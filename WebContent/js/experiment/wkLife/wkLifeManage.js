var wkManageGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'wkCode',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'indexa',
		type : "string"
	});
	fields.push({
		name : 'volume',
		type : "string"
	});
	fields.push({
		name : 'concentration',
		type : "string"
	});
	fields.push({
		name : 'result',
		type : "string"
	});
	fields.push({
		name : 'nextFlow',
		type : "string"
	});
	fields.push({
		name : 'nextFlowId',
		type : "string"
	});
	fields.push({
		name : 'method',
		type : "string"
	});
	fields.push({
		name : 'isExecute',
		type : "string"
	});
	fields.push({
		name : 'backTime',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	// fields.push({
	// name:'phone',
	// type:"string"
	// });
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'patientName',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'inspectDate',
		type : "string"
	});
	fields.push({
		name : 'acceptDate',
		type : "string"
	});
	// fields.push({
	// name:'idCard',
	// type:"string"
	// });
	fields.push({
		name : 'sequenceFun',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'classify',
		type : "string"
	});
	fields.push({
		name : 'labCode',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.name,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});

	cm.push({
		dataIndex : 'wkCode',
		header : biolims.wk.wkCode,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'code',
		header : biolims.common.code,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'sampleCode',
		header : biolims.common.sampleCode,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'labCode',
		header : biolims.common.labCode,
		width : 20 * 6,

		sortable : true,
	});
	cm.push({
		dataIndex : 'patientName',
		hidden : true,
		header : biolims.common.patientName,
		width : 20 * 6
	});
	// cm.push({
	// dataIndex:'idCard',
	// hidden : true,
	// header:biolims.common.idCard,
	// width:20*6
	// });
	// cm.push({
	// dataIndex:'phone',
	// hidden : true,
	// header:biolims.common.phone,
	// width:20*6
	// });

	cm.push({
		dataIndex : 'productId',
		hidden : true,
		header : biolims.common.productId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header : biolims.common.testProject,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'inspectDate',
		hidden : true,
		header : biolims.common.inspectDate,
		width : 50 * 6
	});
	cm.push({
		dataIndex : 'acceptDate',
		hidden : true,
		header : biolims.common.acceptDate,
		width : 20 * 6
	});

	cm.push({
		dataIndex : 'sequenceFun',
		hidden : true,
		header : biolims.common.sequencingFun,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'reportDate',
		hidden : false,
		header : biolims.common.reportDate,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'orderId',
		header : biolims.common.orderId,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'indexa',
		header : 'Blend Code',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'volume',
		header : biolims.common.volume,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'concentration',
		header : biolims.common.concentration,
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'nextFlowId',
		hidden : true,
		header : biolims.common.nextFlowId,
		width : 15 * 10,
		sortable : true
	});
	var nextFlowCob = new Ext.form.TextField({
		allowBlank : false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex : 'nextFlow',
		header : biolims.common.nextFlow,
		width : 15 * 10,
		sortable : true,
	// editor : nextFlowCob
	});
	var resultDecide = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.qualified
			}, {
				id : '0',
				name : biolims.common.disqualified
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'result',
		hidden : false,
		header : biolims.common.result,
		width : 20 * 6,
		sortable : true,
		renderer : Ext.util.Format.comboRenderer(resultDecide),
	// editor: resultDecide
	});
	cm.push({
		dataIndex : 'method',
		header : biolims.common.method,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'isExecute',
		header : biolims.common.confirm2Execute,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'backTime',
		header : biolims.common.backDate,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note,
		width : 30 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/experiment/wkLife/wkManage/showWkManageListJson.action";
	var opts = {};
	opts.tbar = [];
	opts.title = biolims.wk.wkManage;
	opts.height = document.body.clientHeight - 200;
	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};
	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	// opts.tbar.push({
	// text : '检索',
	// handler : search
	// });
	// opts.tbar.push({
	// text : "批量下一步",
	// handler : function() {
	// var options = {};
	// options.width = 400;
	// options.height = 300;
	// loadDialogPage($("#bat_next2_div"), "批量下一步", null, {
	// "确定" : function() {
	// var records = wkManageGrid.getSelectRecord();
	// if (records && records.length > 0) {
	// var nextFlow2 = $("#nextFlow2").val();
	// wkManageGrid.stopEditing();
	// $.each(records, function(i, obj) {
	// obj.set("nextFlow", nextFlow2);
	// });
	// wkManageGrid.startEditing(0, 0);
	// }
	// $(this).dialog("close");
	// }
	// }, true, options);
	// }
	// });
	// opts.tbar.push({
	// text : "批量结果",
	// handler : function() {
	// var options = {};
	// options.width = 400;
	// options.height = 300;
	// loadDialogPage($("#bat_result2_div"), "批量结果", null, {
	// "确定" : function() {
	// var records = wkManageGrid.getSelectRecord();
	// if (records && records.length > 0) {
	// var result2 = $("#result2").val();
	// wkManageGrid.stopEditing();
	// $.each(records, function(i, obj) {
	// obj.set("result", result2);
	// });
	// wkManageGrid.startEditing(0, 0);
	// }
	// $(this).dialog("close");
	// }
	// }, true, options);
	// }
	// });
	// opts.tbar.push({
	// iconCls : 'save',
	// text : '保存',
	// handler : save
	// });
	//取消按钮
//	opts.tbar.push({
//		text : '取消',
//		handler : libraryRollback
//	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	wkManageGrid = gridEditTable("show_wkManage_div", cols, loadParam, opts);
	$("#show_wkManage_div").data("wkManageGrid", wkManageGrid);
});
// 保存
function save() {
	var selectRecord = wkManageGrid.getSelectionModel();
	var inItemGrid = $("#show_wkManage_div").data("wkManageGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if (selectRecord.getSelections().length > 0) {

		$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/wkLife/wkManage/saveWkManage.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#show_wkManage_div").data("wkManageGrid").getStore()
							.commitChanges();
					$("#show_wkManage_div").data("wkManageGrid").getStore()
							.reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);
		});

	} else {
		message(biolims.common.noData2Save);
	}
}

/**
 * @TODO:对于有错误实验信息进行回退且没有进行下一步操作
 * @Author:zhiqiang.yang@biolims.cn
 * @Time:2017-8-2
 */
function libraryRollback() {
	
	var fs = wkManageGrid.getSelectionModel();
	var inItemGrid = $("#show_wkManage_div").data("wkManageGrid");
	var recard = fs.getSelections();
	if (fs.getSelections().length > 0) {
		for ( var i = 0; i < fs.getSelections().length; i++) {

//			$.each(fs, function(j, obj) {
				ajax("post",
						"/experiment/wkLife/wkTastModify/libraryRollbackManage.action",
						{
							ID : recard[i].get("id")
						}, function(data) {
							if (data.success) {
								$("#show_wkManage_div").data("wkManageGrid")
										.getStore().commitChanges();
								$("#show_wkManage_div").data("wkManageGrid")
										.getStore().reload();
								message("回退成功！");
							} else {
								message("回退失败！");
							}
						}, null);
//			});
		}
	} else {
		message("请选择一条要回退的数据");
	}
}
function selectWkInfo() {
	commonSearchAction(wkManageGrid);
	$("#wkManage_wkCode").val("");
	$("#wkManage_code").val("");
	$("#wkManage_sampleCode").val("");
	$("#wkManage_indexa").val("");
}
// 检索
// function search() {
// var option = {};
// option.width = 542;
// option.height = 417;
// loadDialogPage($("#jstj"), "搜索", null, {
// "开始检索" : function() {
//		
//			
// commonSearchAction(wkManageGrid);
// $(this).dialog("close");
//
// },
// "清空" : function() {
// form_reset();
//
// }
// }, true, option);
// }
// function add(){
// window.location=window.ctx+'/experiment/wk/wkManage/editWkManage.action';
// }
// function edit(){
// var id="";
// id=document.getElementById("selectId").value;
// if (id==""||id==undefined){
// message("请选择一条记录!");
// return false;
// }
// window.location=window.ctx+'/experiment/wk/wkManage/editWkManage.action?id='
// + id;
// }
// function view() {
// var id = "";
// id = document.getElementById("selectId").value;
// if (id == "" || id == undefined) {
// message("请选择一条记录!");
// return false;
// }
// window.location = window.ctx +
// '/experiment/wk/wkManage/viewWkManage.action?id=' + id;
// }
// function exportexcel() {
// wkManageGrid.title = '导出列表';
// var vExportContent = wkManageGrid.getExcelXml();
// var x = document.getElementById('gridhtm');
// x.value = vExportContent;
// document.excelfrm.submit();
// }
// $(function() {
// $("#opensearch").click(function() {
// var option = {};
// option.width = 542;
// option.height = 417;
// loadDialogPage($("#jstj"), "搜索", null, {
// "开始检索" : function() {
//			
//				
// commonSearchAction(wkManageGrid);
// $(this).dialog("close");
//
// },
// "清空" : function() {
// form_reset();
//
// }
// }, true, option);
// });
// });
