var techServiceJkTaskGrid;
$(function(){
	var cols={};
	cols.sm = false;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    
	    fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
	    fields.push({
			name:'orderType',
			type:"string"
		});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'experimentUser',
		type:"string"
	});
	    fields.push({
		name:'wkId',
		type:"string"
	});
	    
    	fields.push({
		name:'sequenceBillDate',
		type:"string"
	});
	    fields.push({
		name:'sequenceBillName',
		type:"string"
	});
	    fields.push({
		name:'sequenceType',
		type:"string"
	});
	    fields.push({
		name:'sequenceLength',
		type:"string"
	});
	    fields.push({
		name:'sequencePlatform',
		type:"string"
	});
	    
	    fields.push({
		name:'sampleNum',
		type:"string"
	});
	    fields.push({
		name:'species',
		type:"string"
	});
	    fields.push({
		name:'experimentPeriod',
		type:"string"
	});
	    fields.push({
		name:'experimentEndTime',
		type:"string"
	});
	    fields.push({
		name:'experimentRemarks',
		type:"string"
	});
	    
	    fields.push({
		name:'analysisCycle',
		type:"string"
	});
	    fields.push({
		name:'analysisEndTime',
		type:"string"
	});
	    fields.push({
		name:'analysisRemarks',
		type:"string"
	});
	    fields.push({
		name:'projectDifference',
		type:"string"
	});
	    fields.push({
		name:'projectLeader',
		type:"string"
	});
	    fields.push({
		name:'projectResponsiblePerson',
		type:"string"
	});
	    
	    fields.push({
		name:'mixRatio',
		type:"string"
	});
	    fields.push({
		name:'coefficient',
		type:"string"
	});
	    fields.push({
		name:'readsOne',
		type:"string"
	});
	    fields.push({
		name:'readsTwo',
		type:"string"
	});
	    fields.push({
		name:'index1',
		type:"string"
	});
	    fields.push({
		name:'index2',
		type:"string"
	});
	    fields.push({
		name:'density',
		type:"string"
	});
	    fields.push({
		name:'yw',
		type:"string"
	});
    	
	    
	    fields.push({
		name:'contractName',
		type:"string"
	});
	    fields.push({
		name:'projectName',
		type:"string"
	});
	    fields.push({
		name:'orders',
		type:"string"
	});
	    fields.push({
		name:'testLeader',
		type:"string"
	});
	    fields.push({
		name:'wkType',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'contractId',
		header:biolims.common.contractId,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:biolims.common.projectId,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		header:biolims.common.orderType,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.wk.productId,
		width:20*6,
		sortable:true,
		hidden:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.wk.productName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'experimentUser',
		header:biolims.wk.experimentUser,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'wkId',
		header:biolims.wk.wkId,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceBillDate',
		header:biolims.wk.sequenceBillDate,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceBillName',
		header:biolims.wk.sequenceBillName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sequenceType',
		header:biolims.wk.sequenceType,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sequenceLength',
		header:biolims.wk.sequenceLength,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sequencePlatform',
		header:biolims.wk.sequencePlatform,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleNum',
		header:biolims.common.sampleNum,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'species',
		header:biolims.wk.species,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'experimentPeriod',
		header:biolims.wk.experimentPeriod,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'experimentEndTime',
		header:biolims.wk.experimentEndTime,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'experimentRemarks',
		header:biolims.wk.experimentRemarks,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'analysisCycle',
		header:biolims.wk.analysisCycle,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'analysisEndTime',
		header:biolims.wk.analysisEndTime,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'analysisRemarks',
		header:biolims.wk.analysisRemarks,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'projectDifference',
		header:biolims.wk.projectDifference,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'projectLeader',
		header:biolims.wk.projectLeader,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'projectResponsiblePerson',
		header:biolims.wk.projectResponsiblePerson,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'mixRatio',
		header:biolims.wk.mixRatio,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'coefficient',
		header:biolims.wk.coefficient,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'readsOne',
		header:'ReadsOne',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'readsTwo',
		header:'ReadsTwo',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'index1',
		header:'Index1',
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'index2',
		header:'Index2',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'density',
		header:biolims.wk.density,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'yw',
		header:biolims.wk.yw,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'contractName',
		header:biolims.common.contractName,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'projectName',
		header:biolims.common.projectName,
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orders',
		header:biolims.wk.orders,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'testLeader',
		header:biolims.common.testLeader,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'wkType',
		header:biolims.wk.wkType,
		width:20*6,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/showTechJkTaskListJson.action";
	var opts={};
	opts.title=biolims.common.scienceService;
	opts.height=document.body.clientHeight-400;
	opts.tbar = [];
	opts.rowselect=function(id){
		$("#selectId").val(id);
		show();
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
//	opts.tbar.push({
//		text : '显示',
//		handler : show
//	});
	techServiceJkTaskGrid=gridTable("show_techServiceJkTask_div",cols,loadParam,opts);
});


function show(){
	var selRecord = techServiceJkTaskGrid.getSelectionModel().getSelections();
	var getRecord = techServiceJkTaskItemGrid.store;//填充到当前的明细中
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var taskId = obj.get("id");
			ajax("post","/experiment/wkLife/showSampleByTechJkTaskId.action",{
				taskId : taskId
			},function(data){
				if(data.success){
					techServiceJkTaskItemGrid.store.removeAll();
					var ob = techServiceJkTaskItemGrid.getStore().recordType;
					techServiceJkTaskItemGrid.stopEditing();
					
					$.each(data.data, function(i, o) {
						$.each(selRecord,function(i, obj){
							var isRepeat = false;
							for(var j=0; j<getRecord.getCount();j++){
								var getData = getRecord.getAt(j).get("id");
								if(getData==obj.get("id")){
									message(biolims.common.haveDuplicate);
									isRepeat = true;
									break;
								}
							}
							if(!isRepeat){
								var p= new ob({});
								p.isNew = true;
								p.set("id",o.id);
								p.set("name",o.name);
								p.set("sampleCode",o.sampleCode);
								p.set("state",o.state);
								p.set("techJkServiceTask-id",o.techJkServiceTaskId);
								
								p.set("classify",o.classify);
								p.set("techJkServiceTask-name",o.techJkServiceTaskName);
								p.set("techJkServiceTask-projectId",o.projectId);
								p.set("techJkServiceTask-contractId",o.contractId);
								p.set("techJkServiceTask-orderType",o.orderType);
								techServiceJkTaskItemGrid.getStore().add(p);		
							}
						});
					});
					techServiceJkTaskItemGrid.startEditing(0, 0);	
					
				}else {
					message(biolims.common.anErrorOccurred);
				}
			},null);
		});
	}else{
		message(biolims.common.pleaseSelect);
	}
}


var techServiceJkTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    
	    
	    fields.push({
		name:'techJkServiceTask-projectId',
		type:"string"
	});
	    fields.push({
		name:'techJkServiceTask-contractId',
		type:"string"
	});
	    fields.push({
		name:'techJkServiceTask-orderType',
		type:"string"
	});
	   fields.push({
		name:'experimentUser',
		type:"string"
	});
	   fields.push({
		name:'endDate',
		type:"string"
	});
	    fields.push({
		name:'species',
		type:"string"
	});
	    
	    fields.push({
		name:'techJkServiceTask-id',
		type:"string"
	});
	    fields.push({
		name:'techJkServiceTask-name',
		type:"string"
	});
	    fields.push({
		name:'classify',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden : true
	});
	
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:biolims.common.stateName,
		width:20*10
	});
	cm.push({
		dataIndex:'techJkServiceTask-projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*10
	});
	
	cm.push({
		dataIndex:'techJkServiceTask-contractId',
		header:biolims.common.contractId,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'techJkServiceTask-orderType',
		header:biolims.common.orderType,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'experimentUser',
		hidden : true,
		header:biolims.wk.experimentUser,
		width:20*6
	});
	cm.push({
		dataIndex:'endDate',
		hidden : true,
		header:biolims.common.endDate,
		width:20*10
	});
	cm.push({
		dataIndex:'species',
		hidden : true,
		header:biolims.wk.species,
		width:20*10
	});
	
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/showTechJkServiceTaskItemListJson.action";
	var opts={};
	opts.title=biolims.wk.scienceServiceDetail;
	opts.height =  document.body.clientHeight-320;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/technology/techServiceTaskItem/delTechServiceTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	
	
	
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItems
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = techServiceJkTaskItemGrid.getAllRecord();
							var store = techServiceJkTaskItemGrid.store;
							var isOper = true;
							var buf = [];
							techServiceJkTaskItemGrid.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("sampleCode")){
										buf.push(store.indexOfId(obj1.get("id")));
									}
								});
							});
							techServiceJkTaskItemGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								message(biolims.common.samplecodeComparison);
							}else{
								
								//message("样本号核对完毕！");
								addItems();
							}
							techServiceJkTaskItemGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	techServiceJkTaskItemGrid=gridEditTable("techServiceJkTaskItemdiv",cols,loadParam,opts);
	$("#techServiceJkTaskItemdiv").data("techServiceJkTaskItemGrid", techServiceJkTaskItemGrid);
});


//从左边添加到右边的明细中
function addItems(){
	var selRecord = techServiceJkTaskItemGrid.getSelectionModel().getSelections();//从左边获取数据
	var getRecord = wKItemGrid.store;//填充到当前的明细中
	var count = 1;
	var max=0;
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var isRepeat = false;
			for(var j=0; j<getRecord.getCount();j++){
				var getData = getRecord.getAt(j).get("sampleCode");
				if(getData==obj.get("sampleCode")){
					message(biolims.common.haveDuplicate);
					isRepeat = true;
					break;
				}
			}
			for(var i=0; i<getRecord.getCount();i++){
				var a=getRecord.getAt(i).get("orderNumber");
				if(a>max){
					max=a;
				}
			}
			if(!isRepeat){
				var ob = wKItemGrid.getStore().recordType;
				wKItemGrid.stopEditing();
				var p= new ob({});
				
				var str=obj.get("sampleCode");
				//var s1=str.substring(1,2);
//				var s3=str.substring(0,1)+"D"+str.substring(2);
//				var s3=s1+"D"+s2;
				//
//				if(str.substring(1,2)=="B"||str.substring(1,2)=="D"||str.substring(1,2)=="P"){
					var s3=str.substring(0,1)+"L"+str.substring(2);
//					var orderType = obj.get("orderType");
					ajax("post", "/experiment/wkLife/selectCodeCount.action", {
						code : str
					}, function(data) {
						if (data.success) {
							if(data.data==0){
								p.set("wkId",s3+"-1");
								
							}else if(data.data==1){
								p.set("wkId",s3+"-2");
								
							}else if(data.data==2){
								p.set("wkId",s3+"-3");
								
							}else if(data.data==3){
								p.set("wkId",s3+"-4");
								
							}else{
								message(biolims.common.pleaseResample);
								return;
							}
							p.set("code",obj.get("sampleCode"));
							p.set("sampleCode",obj.get("sampleCode"));
							p.set("name",obj.get("name"));
							p.set("patientName",obj.get("patientName"));
							p.set("productId",obj.get("productId"));
							p.set("productName",obj.get("productName"));
							p.set("inspectDate",obj.get("inspectDate"));
							p.set("acceptDate",obj.get("acceptDate"));
							p.set("idCard",obj.get("idCard"));
							p.set("phone",obj.get("phone"));
							p.set("orderId",obj.get("orderId"));
							p.set("sequenceFun",obj.get("sequenceFun"));
							p.set("reportDate",obj.get("reportDate"));
							if(a>0)
							p.set("orderNumber",parseInt(max)+count);
							else
								p.set("orderNumber",count);	
							p.set("note",obj.get("note"));
							p.set("volume",obj.get("volume"));
							p.set("unit",obj.get("unit"));
								
							p.set("projectId",obj.get("techJkServiceTask-projectId"));//项目编号
							p.set("contractId",obj.get("techJkServiceTask-contractId"));//合同编号
							p.set("orderType",obj.get("techJkServiceTask-orderType"));//任务单类型
							p.set("classify",obj.get("classify"));
							p.set("jkTaskId",obj.get("techJkServiceTask-id"));//科技服务任务单
//								var index = $("#experimentDnaGet_indexs").val();
//								if(index!=""||index!=0){
//									
//									p.set("indexs", Number(index)+count);
//									
//								}else{
//									
//									p.set("indexs", count+1);
//									
//								}
								wKItemGrid.getStore().add(p);
								count++;
								
						}
					});
				}
				
//			}
		});
		
		wKItemGrid.startEditing(0,0);
	}else{
		message(biolims.common.pleaseSelectSamples);
	}
}