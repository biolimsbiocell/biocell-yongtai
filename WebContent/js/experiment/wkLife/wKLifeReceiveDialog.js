var wKReceiveDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'describe',
		type:"string"
	});
	    fields.push({
		name:'receiver-id',
		type:"string"
	});
	    fields.push({
		name:'receiver-name',
		type:"string"
	});
	    fields.push({
		name:'receiverDate',
		type:"string"
	});
	    fields.push({
		name:'storagePrompt-id',
		type:"string"
	});
	    fields.push({
		name:'storagePrompt-name',
		type:"string"
	});
	    fields.push({
		name:'workState',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'describe',
		header:biolims.common.describe,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'receiver-id',
		header:biolims.common.sampleInfoId,
		width:50*10,
		sortable:true
		});
		cm.push({
		dataIndex:'receiver-name',
		header:biolims.common.receiverName,
		width:50*10,
		sortable:true
		});
	cm.push({
		dataIndex:'receiverDate',
		header:biolims.common.acceptDate,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'storagePrompt-id',
		header:biolims.common.storageLocalId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'storagePrompt-name',
		header:biolims.common.storageLocalName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'workState',
		header:biolims.common.workFlowState,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wKReceive/showWKReceiveListJson.action";
	var opts={};
	opts.title=biolims.plasma.plasmaReceive;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setWKReceiveFun(rec);
	};
	wKReceiveDialogGrid=gridTable("show_dialog_wKReceive_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"),biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(wKReceiveDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
