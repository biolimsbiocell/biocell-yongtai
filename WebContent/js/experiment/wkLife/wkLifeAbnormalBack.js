﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
			if(ui.index==1){
				load("/experiment/wkLife/wkAbnormal/showQualityProductList.action", null, "#qualityProductAbnormaldiv");
			}
		}
	});
});
var wkAbnormalBackGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextflowId',
		type:"string"
	});
	   fields.push({
		name:'nextflow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
//	   fields.push({
//		name:'reason',
//		type:"string"
//	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'isExecute',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
			name:'nextFlowId',
			type:"string"
		});
	   fields.push({
			name:'nextFlow',
			type:"string"
		});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
//   fields.push({
//		name:'phone',
//		type:"string"
//	});
   fields.push({
		name:'orderId',
		type:"string"
	});
//	   fields.push({
//		name:'idCard',
//		type:"string"
//	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'isExecute',
		type:"string"
	});
    fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   fields.push({
			name:'state',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'sampleNum',
			type:"string"
	   });
	   fields.push({
			name:'tempId',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'结果表id',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : true,
		header:biolims.wk.wkCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6,
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'Blend Code',
		width:20*6,
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6,
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:20*6,
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6,
//	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.sampleInfoReportDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.volume,
		width:20*6,
		
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6,
		
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.common.qualified
			}, {
				id : '0',
				name : biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),
		editor: result
	});
	
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		sortable:true,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'method',
		header:biolims.common.method,
		width:30*6,
		hidden : false,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
//		editor : methodCob,
//		renderer : Ext.util.Format.comboRenderer(methodCob)
	});

	var storeisExecuteCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var isExecuteCob = new Ext.form.ComboBox({
		store : storeisExecuteCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:biolims.common.confirm2Execute,
		width:20*6,
		editor : isExecuteCob,
		renderer : Ext.util.Format.comboRenderer(isExecuteCob)
	});

	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:20*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:biolims.common.counts,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wkAbnormal/showWkFeedbackListJson.action";
	var opts={};
	opts.title=biolims.wk.wkAbnormal;
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : function() {
			var records = wkAbnormalBackGrid.getSelectRecord();
			if(records.length>0){
				if(records.length>2){
					var productId = new Array();
					$.each(records, function(j, k) {
						productId[j]=k.get("productId");
					});
					for(var i=0;i<records.length;i++){
						if(i!=0&&productId[i]!=productId[i-1]){
							message(biolims.common.testDifferent);
							return;
						}
					}
					loadTestNextFlowCob();
				}else{
					loadTestNextFlowCob();
				}
				
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.batch2deal,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), biolims.common.batch2deal, null, {
				"Confirm" : function() {
					var records = wkAbnormalBackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						wkAbnormalBackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						wkAbnormalBackGrid.startEditing(0, 0);
					}else{
						message(biolims.common.pleaseSelect);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchExecute,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok_div"), biolims.common.batchExecute, null, {
				"Confirm" : function() {
					var records = wkAbnormalBackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isExecute").val();
						wkAbnormalBackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isExecute", isExecute);
						});
						wkAbnormalBackGrid.startEditing(0, 0);
					}else{
						message(biolims.common.pleaseSelect);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : search
//	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	wkAbnormalBackGrid=gridEditTable("wkAbnormalBackdiv",cols,loadParam,opts);
	$("#wkAbnormalBackdiv").data("wkAbnormalBackGrid", wkAbnormalBackGrid);
});
//保存
function save(){	
	//var selectRecord = wkAbnormalBackGrid.getSelectionModel();
	//var inItemGrid = $("#wkAbnormalBackdiv").data("wkAbnormalBackGrid");
	var itemJson = commonGetModifyRecords(wkAbnormalBackGrid);
	if(itemJson.length>0){
		ajax("post", "/experiment/wkLife/wkAbnormal/saveWkAbnormalBack.action", {
			itemDataJson : itemJson
		}, function(data) {
			if (data.success) {
				wkAbnormalBackGrid.getStore().commitChanges();
				wkAbnormalBackGrid.getStore().reload();
				message(biolims.common.saveSuccess);
			} else {
				message(biolims.common.saveFailed);
			}
		}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
}
function selectWkInfo(){
	commonSearchAction(wkAbnormalBackGrid);
	$("#wkAbnormalBack_wkCode").val("");
	$("#wkAbnormalBack_sampleCode").val("");
	$("#wkAbnormalBack_method").val("");
}
//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(wkAbnormalBackGrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();

		}
	}, true, option);
}
/*function selectInfo(){
	wkAbnormalBackGrid.store.removeAll();
	//DNA或血浆info编号
	var code=$("#wkAbnormalBack_code").val();
	//原始样本编号
	var sampleCode=$("#wkAbnormalBack_sampleCode").val();
	//文库编号
	var wkCode=$("#wkAbnormalBack_wkCode").val();
	//index号
	var indexs=$("#wkAbnormalBack_indexa").val();
	
	ajax("post", "/experiment/wk/wkAbnormal/selectAbnormal.action", {
		code : code,sampleCode:sampleCode,wkCode:wkCode,indexs:indexs
	}, function(data) {
		if (data.success) {
			var ob = wkAbnormalBackGrid.getStore().recordType;
			wkAbnormalBackGrid.stopEditing();
			$.each(data.data, function(i, obj) {
				var p = new ob({});
				p.isNew = true;
				
				p.set("id", obj.id);
				p.set("code", obj.code);
				p.set("wkCode", obj.wkCode);
				p.set("sampleCode", obj.sampleCode);
				p.set("resultDecide", obj.result);
				p.set("nextFlow", obj.nextFlow);
				p.set("note", obj.note);
				p.set("method", obj.methods);
				p.set("handleIdea", obj.handleIdea);
				p.set("backTime", obj.backTime);
				p.set("isExecute", obj.isExecute);
				p.set("indexa", obj.indexs);
				p.set("chorma", obj.sampleCondition);
				p.set("bulk", obj.bulk);

				wkAbnormalBackGrid.getStore().add(p);							
			});
			wkAbnormalBackGrid.startEditing(0, 0);		
		} else {
			message("获取明细数据时发生错误！");
		}
	}, null);
	$("#wkAbnormalBack_code").val("");
	$("#wkAbnormalBack_sampleCode").val("");
	$("#wkAbnormalBack_wkCode").val("");
	$("#wkAbnormalBack_indexa").val("");
}*/

//下一步流向
function loadTestNextFlowCob(){
	var records1 = wkAbnormalBackGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=WkLifeTask&productId="+productId, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = wkAbnormalBackGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
	
