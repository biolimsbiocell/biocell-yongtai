﻿var wKItemGrid;
var flg=true;
$(function(){
	seTypeChange();
});
function seTypeChange(){
	var seType=$("#wk_seType").val();
	if(seType=="2"){//lon torrent平台
		flg=false;
	}
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
			name:'rowCode',
			type:"string"
		});
	   fields.push({
			name:'sampleNum',
			type:"string"
		});
	   fields.push({
			name:'sampleConsume',
			type:"string"
		});
	   fields.push({
			name:'addVolume',
			type:"string"
		});
	   fields.push({
			name:'sumVolume',
			type:"string"
		});
	   fields.push({
			name:'colCode',
			type:"string"
		});
	   fields.push({
			name:'counts',
			type:"string"
		});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkId',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
			name:'splitCode',
			type:"string"
		});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"date",
		dateFormat:"Y-m-d"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
//   fields.push({
//		name:'phone',
//		type:"string"
//	});
   fields.push({
		name:'orderId',
		type:"string"
	});
   
   fields.push({
		name:'orderType',
		type:"string"
	});
   fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
	    type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'wk-id',
		type:"string"
	});
	    fields.push({
		name:'wk-name',
		type:"string"
	});
	    
    fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'jkTaskId',
		type:"string"
	});
	   fields.push({
	   name:'classify',
	   type:"string"
   });
	   fields.push({
			name:'productNum',
			type:"string"
		});
	  fields.push({
			name:'dicSampleType-id',
			type:"string"
		});
	  fields.push({
			name:'dicSampleType-name',
			type:"string"
		});
	  fields.push({
			name:'sampleType',
			type:"string"
		});
	  fields.push({
			name:'i5',
			type:"string"
		});
	  fields.push({
			name:'i7',
			type:"string"
		});
	  fields.push({
			name:'tag',
			type:"string"
		});
	  fields.push({
			name:'loopNum',
			type:"string"
		});
	  fields.push({
			name:'sumTotal',
			type:"string"
		});
	  fields.push({
			name:'pcrRatio',
			type:"string"
		});
	  fields.push({
			name:'expectNum',
			type:"string"
		});
	  fields.push({
			name:'tempId',
			type:"string"
		});
	  fields.push({
			name:'rin',
			type:"string"
		});
	  fields.push({
			name:'labCode',
			type:"string"
		});
	  fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	  fields.push({
			name:'sampleInfo-note',
			type:"string"
		});
	  fields.push({
			name:'sampleInfo-receiveDate',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-reportDate',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-patientName',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-idCard',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	  fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	  // 文库类型
	  fields.push({
			name:'techJkServiceTask-libraryType',
			type:"string"
		});
		// 插入片段
	  fields.push({
			name:'techJkServiceTask-insertSize',
			type:"string"
		});
		// 测序类型
		 fields.push({
				name:'techJkServiceTask-sequenceType',
				type:"string"
			});
	  
	  
	  fields.push({
			name:'isZkp',
			type:"string"
		});
	  fields.push({
			name:'blx',
			type:"string"
		});
	  fields.push({
			name:'blendCode',
			type:"string"
		});
	  
	  fields.push({
			name:'libType',
			type:"string"
		});
	  fields.push({
			name:'insertSize',
			type:"string"
		});
	  fields.push({
			name:'libNum',
			type:"string"
		});
	  fields.push({
			name:'sampleNowNum',
			type:"string"
		});
	  fields.push({
			name:'takeNum',
			type:"string"
		});
	  fields.push({
			name:'breakNum',
			type:"string"
		});
	  fields.push({
			name:'teOrH20num',
			type:"string"
		});
	  fields.push({
			name:'dataNowNum',
			type:"string"
		});
	  fields.push({
			name:'storageLocation',
			type:"string"
		});
	  fields.push({
			name:'seqType',
			type:"string"
		});
	  fields.push({
			name:'primer',
			type:"string"
		});
	  fields.push({
			name:'isInit',
			type:"string"
		});
	  fields.push({
			name:'species',
			type:"string"
		});
	  fields.push({
			name:'isRna',
			type:"string"
		});
	  
	  fields.push({
			name:'sampleVolume',
			type:"string"
		});
	  fields.push({
			name:'inwardCode',
			type:"string"
		}); 
	  fields.push({
			name:'tjItem-id',
			type:"string"
		});
	   fields.push({
			name:'tjItem-inwardCode',
			type:"string"
		});
	   fields.push({
			name:'tjItem-wgcId',
			type:"string"
		});
	   fields.push({
			name:'tjItem-species',
			type:"string"
		});
	   fields.push({
			name:'chipType',
			type:"string"
		}); 
	   fields.push({
			name:'qubitConcentration',
			type:"string"
		}); 
	   fields.push({
			name:'NanodropConcentration',
			type:"string"
		}); 
	   fields.push({
			name:'kit',
			type:"string"
		}); 
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'wkId',
		hidden : true,
		header:biolims.wk.wkId,
		width:20*6
	});
	cm.push({
		dataIndex:'splitCode',
		header:biolims.common.splitAfterCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:biolims.common.labCode,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleInfo-idCard',
		hidden : false,
		header:biolims.common.outCode,
		width:20*6
	});
	
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	
	
	
	cm.push({
		dataIndex:'sampleInfo-patientName',
		hidden : true,
		header:"样本名称",
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-libraryType',
		hidden : true,
		header:"文库类型",
		width:20*6,
	});
	
	cm.push({
		dataIndex:'techJkServiceTask-insertSize',
		hidden : true,
		header:"插入片段",
		width:20*6,
	});
	
//	
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:"片段范围",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6
//	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-id',
		hidden : true,
		header:"科技服务明细id",
		width:30*6
	});
	cm.push({
		dataIndex:'tjItem-inwardCode',
		hidden : true,
		header:"内部项目号",
		width:20*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:50*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:25*6,
		renderer: formatDate
	});

	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*10
	});
	
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'jkTaskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.dicSampleTypeId,
		width:20*6
	});
	var testDicSampleType =new Ext.form.TextField({
        allowBlank: false
	});
	testDicSampleType.on('focus', function() {
		loadTestDicSampleType();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10,
		editor : testDicSampleType
	});
	var wkIndexCob =new Ext.form.TextField({
        allowBlank: false
	});
	wkIndexCob.on('focus', function() {
		loadTestwkIndexCob();
	});
	cm.push({
		dataIndex:'tag',
		hidden : true,
		header:'INDEXId',
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'Blend Code <font color="red">*</font>',
		width:20*6,
		editor : wkIndexCob
		
	});
	cm.push({
		dataIndex:'kit',
		hidden : true,
		header:'原辅料盒id',
		width:20*6
	});

	
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i7',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i5',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'chipType',
		hidden : flg,
		header:'芯片类型',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qubitConcentration',
		hidden : true,
		header:'Qubit浓度ng/ul',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'NanodropConcentration',
		hidden : true,
		header:'Nanodrop浓度ng/ul',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'loopNum',
		hidden : true,
		header:biolims.wk.loopNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		header:biolims.common.concentration,
		hidden : false,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumTotal',
		header:biolims.common.sumNum+"ng",
		hidden : false,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'pcrRatio',
		hidden : true,
		header:biolims.wk.pcrRatio,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'expectNum',
		hidden : true,
		header:biolims.wk.expectNum,//通量
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'rin',
		hidden : true,
		header:'RIN',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleConsume',
		hidden : false,
		header:biolims.common.sampleConsumeng,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'libNum',
		hidden : false,
		header:biolims.common.sampleConsume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'takeNum',
		hidden : true,
		header:"取样量",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:"取样体积",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'breakNum',
		hidden : true,
		header:"打断体积",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'teOrH20num',
		hidden : true,
		header:"TE或H20体积",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'dataNowNum',
//		hidden : false,
//		header:"数据量",
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'storageLocation',
		hidden : true,
		header:"储位",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'primer',
		hidden : true,
		header:"引物",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tjItem-species',
		hidden : true,
		header:"物种",
		width:20*6
	});
	
	cm.push({
		dataIndex:'species',
		hidden : true,
		header:"物种",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'blx',
		hidden : true,
		header:"变量X",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : true,
		header:"补水/RSE/TE体积",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleNowNum',
		hidden : false,
		header:biolims.common.sampleAllNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : true,
		header:biolims.common.totalVolume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : false,
		header:biolims.common.rowCode,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'colCode',
		hidden : false,
		header:biolims.common.colCode,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'counts',
		hidden : false,
		header:biolims.common.counts,
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.qualified
			},{
				id : '0',
				name : biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:biolims.common.result,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),
		editor: result
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:biolims.common.orderNumber,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'wk-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wk-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6
	});
	cm.push({
		dataIndex:'productNum',
		hidden : false,
		header:biolims.common.productNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'blendCode',
		hidden : true,
		header:"混合号",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'inwardCode',
		hidden : true,
		header:"内部项目号",
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequenceType',
		hidden : true,
		header:"测序类型",
		width:20*6
	});
	
	
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.openBoxId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : true,
		header:biolims.common.sampleInfoReceiveDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : true,
		header:biolims.common.sampleInfoReportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : true,
		header:biolims.common.openBox,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/showWKItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.wk.wkDetail;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/wkLife/delWKItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				wKTempGrid.getStore().commitChanges();
				wKTempGrid.getStore().reload();
				wKItemGrid.getStore().commitChanges();
				wKItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text :biolims.common.checkCode,
		handler : function() {
			$("#many_batItem_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_batItem_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_batItem_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = wKItemGrid.getAllRecord();
							var store = wKItemGrid.store;

							var isOper = true;
							var buf = [];
							wKItemGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										isOper = true;
									}
									
								});
							});
							
							if(isOper){
								message("编码核对完毕！");
							}
							
							
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("code"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message(biolims.common.noMatchSample+nolist);
							}
							//TA1611080002
							
							
							
							
							
							wKItemGrid.getSelectionModel().selectRows(buf);
							
							wKItemGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		text : biolims.common.productType,
		handler : loadTestDicSampleType
	});
//	opts.tbar.push({
//		text : "生成index",
//		handler : index
//	});
	opts.tbar.push({
		text : biolims.common.sampleConsume,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_sampleConsume_div"), biolims.common.sampleConsume, null, {
				"Confirm" : function() {
					var records = wKItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var sampleConsume = $("#sampleConsume").val();
						wKItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("sampleConsume", sampleConsume);
						});
						wKItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : biolims.wk.expectNum,
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_expectNum_div"), biolims.wk.expectNum, null, {
//				"Confirm" : function() {
//					var records = wKItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var expectNum = $("#expectNum").val();
//						wKItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("expectNum", expectNum);
//						});
//						wKItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	
	opts.tbar.push({
		text : biolims.common.productBatchNumber,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_productNum_div"), biolims.common.productBatchNumber, null, {
				"Confirm" : function() {
					var records = wKItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var productNum = $("#productNum").val();
						wKItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("productNum", productNum);
						});
						wKItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "批量板号",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_counts_div"),"批量板号", null, {
//				"确定" : function() {
//					var records = wKItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var counts = $("#counts").val();
//						wKItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("counts", counts);
//						});
//						wKItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.batchInLib,//入库
		handler : ruku
	});
	if($("#wk_id").val()&&$("#wk_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	
	opts.tbar.push({
		text :biolims.common.exportList,//导出
		handler : exportexcel
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	}
	wKItemGrid=gridEditTable("wKItemdiv",cols,loadParam,opts);
	$("#wKItemdiv").data("wKItemGrid", wKItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
}

//导出
function exportexcel() {
	wKItemGrid.title = "文库构建明细样本信息";
	var vExportContent = wKItemGrid.getExcelXml();
	var x = document.getElementById('gridhtmi');
	x.value = vExportContent;
	document.excelfrmi.submit();
}
//入库
function ruku() {
	var selectRecord = wKItemGrid.getSelectionModel().getSelections();
	if(selectRecord.length>0){
		var records = [];
		for ( var i = 0; i < selectRecord.length; i++) {
			if(selectRecord[i].get("state")=="1"){
				message(biolims.wk.pleaseChooseAgain);
				return;
			}
			records.push(selectRecord[i].get("id"));
		}
		ajax("post", "/experiment/wkLife/rukuItem.action", {
			ids : records
		}, function(data) {
			if (data.success) {
				wKItemGrid.getStore().commitChanges();
				wKItemGrid.getStore().reload();
				message(biolims.plasma.warehousingSuccess);
				
			} else {
				message(biolims.plasma.warehousingFailed);
			}
		}, null);
	}else{
		message(biolims.plasma.pleaseSelectWarehousing);
	}
}

//开始实验
function startTest(){
	var selectRecord=wKItemGrid.getSelectionModel();
	var selRecord=wKTemplateGrid.getSelectRecord();
	if (selectRecord.getSelections().length > 0) {
		var codes = "";
		$.each(selectRecord.getSelections(), function(i, obj) {
			codes += obj.get("code")+",";
		});
		if (!selRecord.length) {
			selRecord = wKTemplateGrid.getAllRecord();
		}
		if (selRecord&&selRecord.length>0) {
			wKTemplateGrid.stopEditing();
			$.each(selRecord, function(i, obj) {
				obj.set("sampleCodes", codes);
			});
			wKTemplateGrid.startEditing(0, 0);
		}
			
	}
}

function addQc(num){
	var selRecord=wKItemGrid.store;
	var max=0;
	var orderId = '';
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		orderId = selRecord.getAt(i).get("orderId");
		if(parseInt(a)>parseInt(max)){
			max=a;
		}
	}
	ajax("post", "/system/quality/qualityProduct/setQcItem.action", {
		code : num
		}, function(data) {
			if (data.success) {
				var ob = wKItemGrid.getStore().recordType;
				wKItemGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("code",obj.id);
					p.set("note",obj.note);
					p.set("orderId",orderId);
					p.set("orderNumber",(parseInt(max)+1));
					wKItemGrid.getStore().add(p);							
				});
				
				wKItemGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
}
function selectwkFun(){
	var win = Ext.getCmp('selectwk');
	if (win) {win.close();}
	var selectwk= new Ext.Window({
	id:'selectwk',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKSelect.action?flag=wk' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectwk.close(); }  }]  });     selectwk.show(); }
	function setwk(id,name){
		var gridGrid = $("#wKItemdiv").data("wKItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('wk-id',id);
			obj.set('wk-name',name);
		});
		var win = Ext.getCmp('selectwk');
		if(win){
			win.close();
		}
	}
	var loadwkIndex;
	function loadTestwkIndexCob(){
		 var options = {};
			options.width = 800;
			options.height = 600;
			loadwkIndex=loadDialogPage(null, biolims.wk.selectINDEX, "/storage/showSpecSelectList.action?type=index&p_type=1", {
				"Confirm" : function() {
					var operGrid = $("#showSpecSelectdiv").data("showSpecSelectGrid");
//					$("#wk_indexa").val(operGrid.store.getCount());
					var selectRecord = operGrid.getSelectionModel().getSelections();
					var records = wKItemGrid.getSelectRecord();
					if (selectRecord.length > 0) {
						$.each(records, function(i, obj) {
							$.each(selectRecord, function(a, b) {
								obj.set("tag", b.get("id"));
								obj.set("indexa", b.get("name"));
								obj.set("kit", b.get("kit-id"));
								obj.set("i5", b.get("i5"));
								obj.set("i7", b.get("i7"));
							});
						});
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
	}
	function setspecFun(){
		var operGrid = $("#showSpecSelectdiv").data("showSpecSelectGrid");
//		$("#wk_indexa").val(operGrid.store.getCount());
		var selectRecord = operGrid.getSelectionModel().getSelections();
		var records = wKItemGrid.getSelectRecord();
		if (selectRecord.length > 0) {
			$.each(records, function(i, obj) {
				$.each(selectRecord, function(a, b) {
					obj.set("tag", b.get("id"));
					obj.set("indexa", b.get("name"));
					obj.set("kit", b.get("kit-id"));
					obj.set("i5", b.get("i5"));
					obj.set("i7", b.get("i7"));
				});
			});
		}else{
			message(biolims.common.selectYouWant);
			return;
		}
		loadwkIndex.dialog("close");
	}
	
	
var loadDicSampleType;	
//查询样本类型
function loadTestDicSampleType(){
	var options = {};
	options.width = document.body.clientWidth-800;
	options.height = document.body.clientHeight-40;
	loadDicSampleType=loadDialogPage(null, biolims.common.sampleType, "/sample/dicSampleType/dicSampleTypeSelect.action", {
		"Confirm" : function() {
			var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = wKItemGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					$.each(records, function(a, b) {
						b.set("dicSampleType-id", obj.get("id"));
						b.set("dicSampleType-name", obj.get("name"));
					});
				});
			}else{
				message(biolims.common.selectYouWant);
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}

function setDicSampleType(){
	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = wKItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			$.each(records, function(a, b) {
				b.set("dicSampleType-id", obj.get("id"));
				b.set("dicSampleType-name", obj.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadDicSampleType.dialog("close");
}


//生成index 
function index(){
	//保存根据实验排序号后台赋值idnex
	if(wKItemGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var selectRecord = wKItemGrid.getSelectionModel().getSelections();
	if(selectRecord.length==1){
		if(selectRecord[0].get("indexa")!=""){
			ajax("post", "/experiment/wkLife/index.action", {
				id:selectRecord[0].get("id")
//				indexId:selectRecord[0].get("tag"),
//				kitId:selectRecord[0].get("kit")
				}, function(data) {
					if (data.success) {
						wKItemGrid.getStore().commitChanges();
						wKItemGrid.getStore().reload();
					} 
				}, null); 
		}else{
			message("请先选择index！");
		}
	}else{
		message("请选择一条数据！");
	}
}
	

	