﻿
var wkRepeatGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
//	   fields.push({
//		name:'unit',
//		type:"string"
//	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'isExecute',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
   fields.push({
		name:'phone',
		type:"string"
	});
   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
   fields.push({
		name:'techTaskId',
		type:"string"
	});
   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
   		name:'classify',
   		type:"string"
   	});
	   fields.push({
	   		name:'state',
	   		type:"string"
	   	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:biolims.wk.wkCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'Blend Code',
		width:20*6,
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : false,
		header:biolims.common.idCard,
		width:20*6,
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:biolims.common.phone,
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : false,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.volume,
		width:20*6,
		
	});
//	cm.push({
//		dataIndex:'unit',
//		hidden : true,
//		header:'单位',
//		width:20*6,
//		
//	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.common.qualified
			}, {
				id : '0',
				name : biolims.common.unqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),
		editor: result
	});
//	var next = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [{
//				id : '0',
//				name : '文库质控'
//			}, {
//				id : '1',
//				name : '2100 or Caliper'
//			}, {
//				id : '2',
//				name : '终止'
//			}, {
//				id : '3',
//				name : '入库'
//			}, {
//				id : '4',
//				name : '异常反馈至项目管理'
//			}, {
//				id : '5',
//				name : '暂停'
//			}, {
//				id : '7',
//				name : '重建库'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : true,
//		header:'下一步流向',
//		width:25*6,
//		renderer: Ext.util.Format.comboRenderer(next),
////		editor: next
//	});

//	var storemethodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '文库质控' ],[ '1', '2100 or Caliper' ],[ '2', '重建库' ],['3','终止'],['4','入库'],['5','暂停']]
//	});
//	var methodCob = new Ext.form.ComboBox({
//		store : storemethodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'method',
		header:biolims.common.method,
		width:30*6,
		hidden : false,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
//		editor : methodCob,
//		renderer : Ext.util.Format.comboRenderer(methodCob)
	});
//	cm.push({
//		dataIndex:'method',
//		hidden : false,
//		header:'处理意见',
//		width:20*6
//	});
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.common.yes
			}, {
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:biolims.common.confirm2Execute,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(submit),
		editor: submit
	});

	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techTaskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6,
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6,
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*6,
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6,
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/repeat/showWKRepeatListJson.action";
	var opts={};
	opts.title=biolims.wk.rebuildLib;
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.batchExecute,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_method_div"), biolims.common.batchExecute, null, {
				"Confirm" : function() {
					var records = wkRepeatGrid.getSelectRecord();
					if (records && records.length > 0) {
						var run = $("#run").val();
						wkRepeatGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isExecute", run);
						});
						wkRepeatGrid.startEditing(0, 0);
					}else{
						message(biolims.common.pleaseSelect);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.retrieve,
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	wkRepeatGrid=gridEditTable("wkRepeatdiv",cols,loadParam,opts);
	$("#wkRepeatdiv").data("wkRepeatGrid", wkRepeatGrid);
});
//保存
function save(){	
	var itemJson = commonGetModifyRecords(wkRepeatGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/wkLife/repeat/saveWkRepeat.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#wkRepeatdiv").data("wkRepeatGrid").getStore().commitChanges();
					$("#wkRepeatdiv").data("wkRepeatGrid").getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
}
//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(wkRepeatGrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();

		}
	}, true, option);
}


