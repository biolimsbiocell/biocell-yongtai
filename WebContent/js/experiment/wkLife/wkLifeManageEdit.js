﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/experiment/wkLife/wkManage/editWkManage.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/wkLife/wkManage/showWkManageList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#wkManage", {
					userId : userId,
					userName : userName,
					formId : $("#wkManage_id").val(),
					title : $("#wkManage_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#wkManage_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/wkLife/wkManage/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/wkLife/wkManage/copyWkManage.action?id=' + $("#wkManage_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#wkManage_id").val() + "&tableId=wkManage");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#wkManage_id").val());
	nsc.push(biolims.wk.wkIdIsEmpty);
	fs.push($("#wkManage_index").val());
	nsc.push(biolims.wk.wkINDEXIsEmpty);
	fs.push($("#wkManage_bulk").val());
	nsc.push(biolims.wk.wkBulkIsEmpty);
	fs.push($("#wkManage_chroma").val());
	nsc.push(biolims.wk.wkConcentrationIsEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.wk.wkManage,
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);