var wkManageDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'index',
		type:"string"
	});
	    fields.push({
		name:'bulk',
		type:"string"
	});
	    fields.push({
		name:'chroma',
		type:"string"
	});
	    fields.push({
		name:'resultDecide',
		type:"string"
	});
	    fields.push({
		name:'nextFlow',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.wk.wkId,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'index',
		header:'index',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'bulk',
		header:biolims.common.bulk,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'chroma',
		header:biolims.common.concentrationc,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'resultDecide',
		header:biolims.plasma.resultDecide,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:biolims.wk.wkCode,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wkManage/showWkManageListJson.action";
	var opts={};
	opts.title=biolims.wk.wkManage;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setWkManageFun(rec);
	};
	wkManageDialogGrid=gridTable("show_dialog_wkManage_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(wkManageDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
