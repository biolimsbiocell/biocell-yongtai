﻿var wKItemManageGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkId',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});	   
	  fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
	   name:'stepNum',
	   type:"string"
   });
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
//	   fields.push({
//		name:'idCard',
//		type:"string"
//	});
//   fields.push({
//		name:'phone',
//		type:"string"
//	});
   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'wk-id',
		type:"string"
	});
	    fields.push({
		name:'wk-name',
		type:"string"
	});
	    
    fields.push({
		name:'techTaskId',
		type:"string"
	});
   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
   		name:'classify',
   		type:"string"
   	});
	   fields.push({
	   		name:'state',
	   		type:"string"
	   	});
	   fields.push({
	   		name:'labCode',
	   		type:"string"
	   	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkId',
		hidden : true,
		header:biolims.wk.wkId,
		width:20*6,
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:20*7,
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*6,
		
		sortable:true,
	});
	cm.push({
		dataIndex:'labCode',
		header:biolims.common.labCode,
		width:20*6,
		
		sortable:true,
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6,
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:20*6,
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6,
//	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6,
	});

	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:20*6,
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6,
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'Blend Code',
		width:20*6,
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6
//		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});

//	var nextFlow = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '0',
//				name : '2100质控'
//			}, {
//				id : '6',
//				name : '2100 or Caliper'
//			}, {
//				id : '1',
//				name : 'QPCR质控'
//			},{
//				id : '2',
//				name : '终止'
//			},{
//				id : '3',
//				name : '入库'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:20*6,
//		sortable:true,
//		renderer: Ext.util.Format.comboRenderer(nextFlow),editor: nextFlow
//	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.qualified
			},{
				id : '0',
				name : biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),
		editor: result
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6,
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'wk-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wk-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	
	cm.push({
		dataIndex:'techTaskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6,
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:biolims.common.contractId,
		width:20*6,
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:biolims.common.projectId,
		width:20*6,
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:biolims.common.orderType,
		width:20*6,
	});
	cm.push({
		dataIndex : 'classify',
		header : biolims.common.classify,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wkManage/showWkItemManageListJson.action";
	var opts={};
	opts.title=biolims.wk.wkDetailManage;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
//	opts.tbar.push({
//		iconCls : 'save',
//		text : '保存',
//		handler : save1
//	});
	opts.tbar.push({
		text : biolims.common.batchInLib,
		handler : ruku
	});
	opts.tbar.push({
		text : biolims.wk.buildToDo,
		handler : jianku
	});
	wKItemManageGrid=gridEditTable("wKItemManagediv",cols,loadParam,opts);
	$("#wKItemManagediv").data("wKItemManageGrid", wKItemManageGrid);

});	
function save1(){	
	var selectRecord = wKItemManageGrid.getSelectionModel();
	var inItemGrid = $("#wKItemManagediv").data("wKItemManageGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if(selectRecord.getSelections().length>0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/wkLife/wkManage/saveWkItemManage.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#wKItemManagediv").data("wKItemManageGrid").getStore().commitChanges();
					$("#wKItemManagediv").data("wKItemManageGrid").getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
		});
	}else{
		message(biolims.common.noData2Save);
	}
}
function selectWkItemInfo(){
	commonSearchAction(wKItemManageGrid);
	$("#wKItemManage_wkId").val("");
	$("#wKItemManage_code").val("");
	$("#wKItemManage_sampleCode").val("");
	$("#wKItemManage_indexa").val("");
}

function ruku(){
	var selectRcords=wKItemManageGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		var ids="";
		$.each(selectRcords,function(i,obj){
			ids+=obj.get("id")+",";
		});
		ajax("post", "/experiment/wkLife/wkManage/wkManageItemRuku.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				wKItemManageGrid.getStore().reload();
				message(biolims.plasma.warehousingSuccess);
			} else{
				message(biolims.plasma.warehousingFailed);
			}
		}, null);
	}else{
		message(biolims.plasma.pleaseSelectWarehousing);
	}
}
function jianku(){
	var selectRcords=wKItemManageGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		for(var i=0;i<selectRcords.length;i++){
			ajax("post", "/experiment/wkLife/wkManage/wkManageItemJK.action", {
				id : selectRcords[i].get("id")
			}, function(data) {
				if (data.success) {
					wKItemManageGrid.getStore().reload();
					message(biolims.common.backlogSuccess);
				} else{
					message(biolims.common.backlogFailed);
				}
			}, null);
		}
	}else{
		message(biolims.common.pleaseSelectBacklog);
	}
}