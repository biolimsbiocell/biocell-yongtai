﻿var wKTastModifyItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'splitCode',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
    });
//   fields.push({
//		name:'phone',
//		type:"string"
//	});
   fields.push({
		name:'orderId',
		type:"string"
	});
//	   fields.push({
//		name:'idCard',
//		type:"string"
//	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'wk-id',
		type:"string"
	});
	    fields.push({
		name:'wk-name',
		type:"string"
	});
    fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   
	   fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
    fields.push({
		name:'orderType',
		type:"string"
	});
    fields.push({
		name:'jkTaskId',
		type:"string"
	});
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'i5',
		type:"string"
	});
    fields.push({
		name:'i7',
		type:"string"
	});
    fields.push({
		name:'concentration',
		type:"string"
	});
    fields.push({
		name:'loopNum',
		type:"string"
	});
  fields.push({
		name:'sumTotal',
		type:"string"
	});
  fields.push({
		name:'pcrRatio',
		type:"string"
	});
  fields.push({
		name:'expectNum',
		type:"string"
	});
  fields.push({
		name:'sampleNum',
		type:"string"
	});
  fields.push({
		name:'tempId',
		type:"string"
	});
  
  fields.push({
		name:'chpsConcentration',
		type:"string"
	});
  fields.push({
		name:'chVolume',
		type:"string"
	});
  fields.push({
		name:'chSumTotal',
		type:"string"
	});
  fields.push({
		name:'indexConcentration',
		type:"string"
	});
  fields.push({
		name:'dxpdConcentration',
		type:"string"
	});
  
  fields.push({
		name:'dxpdVolume',
		type:"string"
	});
  fields.push({
		name:'dxpdSumTotal',
		type:"string"
	});
  fields.push({
		name:'wkConcentration',
		type:"string"
	});
  fields.push({
		name:'wkVolume',
		type:"string"
	});
  fields.push({
		name:'wkSumTotal',
		type:"string"
	});
  fields.push({
		name:'labCode',
		type:"string"
	});
  fields.push({
		name:'sampleInfo-id',
		type:"string"
	});
fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
fields.push({
		name:'sampleInfo-receiveDate',
		type:"string"
	});
fields.push({
	name:'sampleInfo-idCard',
	type:"string"
});
fields.push({
	name:'sampleInfo-orderNum',
	type:"string"
});
fields.push({
		name:'sampleInfo-reportDate',
		type:"string"
	});
fields.push({
	name:'techJkServiceTask-id',
	type:"string"
});
fields.push({
	name:'techJkServiceTask-name',
	type:"string"
});
//文库类型
fields.push({
		name:'techJkServiceTask-libraryType',
		type:"string"
	});
	// 插入片段
fields.push({
		name:'techJkServiceTask-insertSize',
		type:"string"
	});
	// 测序类型
	 fields.push({
			name:'techJkServiceTask-sequenceType',
			type:"string"
		});

	 fields.push({
			name:'libType',
			type:"string"
		});
	  fields.push({
			name:'insertSize',
			type:"string"
		});
	  fields.push({
			name:'libNum',
			type:"string"
		});
	  fields.push({
			name:'sampleNowNum',
			type:"string"
		});
	  fields.push({
			name:'takeNum',
			type:"string"
		});
	  fields.push({
			name:'breakNum',
			type:"string"
		});
	  fields.push({
			name:'teOrH20num',
			type:"string"
		});
	  fields.push({
			name:'dataNowNum',
			type:"string"
		});
	  fields.push({
			name:'storageLocation',
			type:"string"
		});
	  fields.push({
			name:'seqType',
			type:"string"
		});
	  fields.push({
			name:'primer',
			type:"string"
		});
	  fields.push({
			name:'isInit',
			type:"string"
		});
	  fields.push({
			name:'species',
			type:"string"
		});
	  fields.push({
			name:'isRna',
			type:"string"
		});
	  
	  fields.push({
			name:'sampleVolume',
			type:"string"
		});
	fields.push({
		name:'isZkp',
		type:"string"
	});
	fields.push({
		name:'inwardCode',
		type:"string"
	});
	fields.push({
		name:'tjItem-id',
		type:"string"
	});
   fields.push({
		name:'tjItem-inwardCode',
		type:"string"
	});
	fields.push({
		name:'unitGroup-id',
		type:"string"
	});
	fields.push({
		name:'unitGroup-name',
		type:"string"
	});
	fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
  fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : true,
		header:biolims.wk.wkCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.wk.wkCode,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'splitCode',
		header:'拆分编号',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:biolims.common.labCode,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleInfo-idCard',
		hidden : true,
		header:'外部样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-orderNum',
		hidden : true,
		header:'订单号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6,
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.dicSampleTypeId,
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10
	});
	cm.push({
		dataIndex:'inwardCode',
		hidden : true,
		header:"内部项目号",
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:biolims.common.sampleNum,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'Blend Code',
		width:20*6,
	});
	cm.push({
		dataIndex:'indexConcentration',
		hidden : true,
		header:biolims.common.indexConcentration,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'i5',
		hidden : true,
		header:'i7',
		width:20*6
	});
	cm.push({
		dataIndex:'i7',
		hidden : true,
		header:'i5',
		width:20*6
	});
	cm.push({
		dataIndex:'chpsConcentration',
		hidden : true,
		header:biolims.wk.chpsConcentration,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'chVolume',
		hidden : true,
		header:biolims.wk.chVolume,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'chSumTotal',
		hidden : true,
		header:biolims.wk.chSumTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		header:biolims.wk.concentration,
		width:20*6,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.wk.volume,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : true,
		header:biolims.wk.sumTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'dxpdConcentration',
		header:biolims.wk.dxpdConcentration,
		width:20*6,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'dxpdVolume',
		header:biolims.wk.dxpdVolume,
		width:20*6,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'dxpdSumTotal',
		hidden : false,
		header:biolims.wk.dxpdSumTotal,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'loopNum',
		hidden : false,
		header:biolims.wk.loopNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkConcentration',
		header:biolims.wk.wkConcentration,//文库浓度
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'wkVolume',
		hidden : false,
		header:biolims.wk.wkVolume,//文库体积
		width:20*6,
		editor : new Ext.form.NumberField({
			allowBlank : true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'wkSumTotal',
		header:biolims.wk.wkTotal,//文库总量
		width:20*6
	});
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:"单位组ID",
		width:15*10,
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: "单位组",
		width:15*10,
		hidden:true,
//		sortable:true,
		editor : testUnitGroup
	});

	
	

	
	cm.push({
		dataIndex:'pcrRatio',
		hidden : true,
		header:biolims.wk.pcrRatio,
		width:20*6
	});
	cm.push({
		dataIndex:'expectNum',
		header:biolims.wk.expectNum,
		width:20*6,
		hidden : true,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'techJkServiceTask-libraryType',
		hidden : true,
		header:"文库类型",
		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});

	cm.push({
		dataIndex:'insertSize',
		hidden : false,
		header:"文库片段大小（bp）",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'libNum',
		hidden : true,
		header:"文库数量",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleNowNum',
		hidden : true,
		header:"样品现有量",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'takeNum',
		hidden : true,
		header:"取样量",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:"取样体积",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'breakNum',
		hidden : true,
		header:"打断体积",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'teOrH20num',
		hidden : true,
		header:"TE或H20体积",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//cm.push({
	//dataIndex:'dataNowNum',
	//hidden : false,
	//header:"数据量",
	//width:20*6,
	//editor : new Ext.form.TextField({
	//	allowBlank : true
	//})
	//});
	cm.push({
		dataIndex:'storageLocation',
		hidden : true,
		header:"储位",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequenceType',
		hidden : true,
		header:"测序类型",
		width:20*6,
		//editor : new Ext.form.TextField({
		//	allowBlank : true
		//})
	});
	
	
	cm.push({
		dataIndex:'primer',
		hidden : true,
		header:"引物",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'species',
		hidden : true,
		header:"物种",
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', "请选择"], 
		         [ '1', biolims.common.yes], 
		         [ '2', biolims.common.no ],
		        ]
	});
	var isRnaCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isRna',
		hidden : true,
		header:"是否有RNA污染",
		width:20*6,
		editor : isRnaCob,
		renderer : Ext.util.Format.comboRenderer(isRnaCob)
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', "请选择"], 
		         [ '1', biolims.common.yes], 
		         [ '2', biolims.common.no ],
		        ]
	});
	var isInitCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isInit',
		hidden : true,
		header:"是否重建库",
		width:20*6,
		editor : isInitCob,
		renderer : Ext.util.Format.comboRenderer(isInitCob)
	});

	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6,
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:biolims.common.idCard,
//		width:20*6,
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:biolims.common.phone,
//		width:20*6,
//	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.workOrderId,
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.common.qualified
			}, {
				id : '0',
				name : biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		editor : nextFlowCob
	});
	
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.common.yes
			}, {
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.toSubmit,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(submit)
		//editor: submit
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'biolims.common.reason',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contractId',
		header:biolims.common.contractId,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:biolims.common.projectId,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		header:biolims.common.orderType,
		width:20*6,
		sortable:true,
		hidden : true
	});
	cm.push({
		dataIndex:'jkTaskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:20*6
	});
	cm.push({
		dataIndex:'true',
		hidden : true,
		header:biolims.common.counts,
		width:20*6
	});
	cm.push({
		dataIndex:'wk-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wk-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.openBoxId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : true,
		header:biolims.common.sampleInfoReceiveDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : true,
		header:biolims.common.sampleInfoReportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : true,
		header:biolims.common.openBox,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'tjItem-id',
		hidden : true,
		header:"科技服务明细id",
		width:30*6
	});
	cm.push({
		dataIndex:'tjItem-inwardCode',
		hidden : true,
		header:"内部项目号",
		width:20*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wkTastModify/showWKTastModifyItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit=200;
	var opts={};
	opts.title=biolims.wk.wkFFPE;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/wkLife/delWKResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				wKTastModifyItemGrid.getStore().commitChanges();
				wKTastModifyItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
	
//	 opts.tbar.push({
//			text : "批量粘贴导入",
//			handler : function() {
//				
//			
//				//$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br />字段：接收样本号、体积、浓度、260/230、260/280");
//				$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：样本号、体积、浓度、260/230、260/280");
//				$("#many_bat_text").val("");
//				$("#many_bat_text").attr("style", "width:465px;height: 339px");
//				var options = {};
//				options.width = 494;
//				options.height = 508;
//				loadDialogPage($("#many_bat_div"), "批量导入", null, {
//					"确定" : function() {
//						var positions = $("#many_bat_text").val();
//						if (!positions) {
//							message("请填写信息！");
//							return;
//						}
//						var posiObj = {};
//						var posiObj1 = {};
//						var posiObj2 = {};
//						var posiObj3 = {};
//						var array = formatData(positions.split("\n"));
//						$.each(array, function(i, obj) {
//							var tem = obj.split("\t");
//							posiObj[tem[0]] = tem[1];
//							posiObj1[tem[0]] = tem[2];
//							posiObj2[tem[0]] = tem[3];
//							posiObj3[tem[0]] = tem[4];
//						});
//						var records = wKTastModifyItemGrid.getAllRecord();
//						wKTastModifyItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							if (posiObj[obj.get("code")]) {
//								obj.set("volume", posiObj[obj.get("code")]);
//							}
//							if (posiObj1[obj.get("code")]) {
//								obj.set("contraction", posiObj1[obj.get("code")]);
//							}
//							if (posiObj2[obj.get("code")]) {
//								obj.set("od260", posiObj2[obj.get("code")]);
//							}
//							if (posiObj3[obj.get("code")]) {
//								obj.set("od280", posiObj3[obj.get("code")]);
//							}
//							//obj.set("unit-id","ng");
//							//obj.set("unit-name","ng");
//							//obj.set("result","1");
//							//obj.set("method","1");
//									
//						});
//						wKTastModifyItemGrid.startEditing(0, 0);
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}
//		});	
	
	
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), biolims.common.batchResult, null, {
				"Confirm" : function() {
					var records = wKTastModifyItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						wKTastModifyItemGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						wKTastModifyItemGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : loadTestNextFlowCob
	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = wKTastModifyItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						wKTastModifyItemGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						wKTastModifyItemGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text :biolims.common.exportList,
		handler : exportexcela
	});

	
	

	if($("#wk_id").val()&&$("#wk_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	
	
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submitSample1
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
//	opts.tbar.push({
//		text : "单位组",
//		handler : function() {
//			var records = wKTastModifyItemGrid.getSelectRecord();
//			if(records.length>0){
//				loadUnitGroup();
//			}else{
//				message("请选择样本");
//			}	
//		}
//	});

	}
	
	wKTastModifyItemGrid=gridEditTable("wKSampleInfodiv",cols,loadParam,opts);
	$("#wKSampleInfodiv").data("wKTastModifyItemGrid", wKTastModifyItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function exportexcela() {
	wKTastModifyItemGrid.title = biolims.common.exportList;
	var vExportContent = wKTastModifyItemGrid.getExcelXml();
	var x = document.getElementById('gridhtma');
	x.value = vExportContent;
	document.excelfrma.submit();
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = wKTastModifyItemGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=WkTask&productId="+productId, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = wKTastModifyItemGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = wKTastModifyItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}	
	
//提交样本
function submitSample1(){
	var id=$("#wk_id").val();  
	if(wKTastModifyItemGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var grid=wKTastModifyItemGrid.store;
	var record = wKTastModifyItemGrid.getSelectionModel().getSelections();
	var flg=false;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(!record[i].get("submit")){
				flg=true;
			}
			if(record[i].get("nextFlowId")==""){
				message(biolims.common.notFillNextStep);
				return;
			}
		}
	}else{
		for(var i=0;i<grid.getCount();i++){
			if(grid.getAt(i).get("submit")==""){
				flg=true;
			}
			if(grid.getAt(i).get("nextFlowId")==""){
				message(biolims.common.notFillNextStep);
				return;
			}
		}
	}
	
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
	
		
		var records = [];
		
		
		
		for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
		}
		
		ajax("post", "/experiment/wkLife/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				wKTastModifyItemGrid.getStore().commitChanges();
				wKTastModifyItemGrid.getStore().reload();
				message(biolims.common.submitSuccess);
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);	
	}else{
		message(biolims.common.noData2Submit);
	}
		
}
//查询单位组
function loadUnitGroup(){
var win = Ext.getCmp('loadUnitGroup');
if (win) {win.close();}
var loadUnitGroup= new Ext.Window({
id:'loadUnitGroup',modal:true,title:'选择单位组',layout:'fit',width:600,height:600,closeAction:'close',
plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
collapsible: true,maximizable: true,
items: new Ext.BoxComponent({id:'maincontent', region: 'center',
html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=unitGroup' frameborder='0' width='100%' height='100%' ></iframe>"}),
buttons: [
{ text: '关闭',
 handler: function(){
	 loadUnitGroup.close(); }  }]  });   
loadUnitGroup.show(); }





function setunitGroup(id,name){
	var gridGrid = $("#wKSampleInfodiv").data("wKTastModifyItemGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		obj.set('unitGroup-id',id);
		obj.set('unitGroup-name',name);
	});
	var win = Ext.getCmp('loadUnitGroup');
	if(win){
		win.close();
	}
}
