var wKReceiveGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'describe',
		type:"string"
	});
	    fields.push({
		name:'receiver-id',
		type:"string"
	});
	    fields.push({
		name:'receiver-name',
		type:"string"
	});
	    fields.push({
		name:'receiverDate',
		type:"string"
	});
	    fields.push({
		name:'storagePrompt-id',
		type:"string"
	});
	    fields.push({
		name:'storagePrompt-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'describe',
		header:biolims.common.describe,
		width:20*6,
		hidden:true,
		sortable:true
	});
		cm.push({
		dataIndex:'receiver-id',
		hidden:true,
		header:biolims.common.receiverId,
		width:10*6,
		sortable:true
		});
	cm.push({
		dataIndex:'receiver-name',
		header:biolims.common.receiverName,
		width:10*6,
		sortable:true
		});
	cm.push({
		dataIndex:'receiverDate',
		header:biolims.common.receiveDate,
		width:30*6,
		sortable:true
	});
		cm.push({
		dataIndex:'storagePrompt-id',
		hidden:true,
		header:biolims.common.storageLocalId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'storagePrompt-name',
		header:biolims.common.storageLocalName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:biolims.common.workFlowState,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wKReceive/showWKReceiveListJson.action";
	var opts={};
	opts.title=biolims.common.sampleReceive;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	wKReceiveGrid=gridTable("show_wKReceive_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/wkLife/wKReceive/editWKReceive.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/experiment/wkLife/wKReceive/editWKReceive.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/experiment/wkLife/wKReceive/viewWKReceive.action?id=' + id;
}
function exportexcel() {
	wKReceiveGrid.title = biolims.common.exportList;
	var vExportContent = wKReceiveGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startreceiverDate").val() != undefined) && ($("#startreceiverDate").val() != '')) {
					var startreceiverDatestr = ">=##@@##" + $("#startreceiverDate").val();
					$("#receiverDate1").val(startreceiverDatestr);
				}
				if (($("#endreceiverDate").val() != undefined) && ($("#endreceiverDate").val() != '')) {
					var endreceiverDatestr = "<=##@@##" + $("#endreceiverDate").val();

					$("#receiverDate2").val(endreceiverDatestr);

				}
				
				
				commonSearchAction(wKReceiveGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
