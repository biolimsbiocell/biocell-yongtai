﻿function changeWidth(){
	
	$("#wKTastModifyTempPage").css("width","40%");
	$("#markup").css("width","60%");
	wKTempGrid.getStore().reload();
	
	
}
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#wk_state").val();;
	var stateName = $("#wk_stateName").val();
	var method = $("#wk_method").val();
	if(id =="3"||stateName==biolims.common.toModify){
		if(method=='window'){
			$("#wKTastModifyTempPage").remove();
			$("#markup").css("width","100%");
			selectTemp();
		}else{
			load("/experiment/wkLife/wkTastModify/showWKTastModifyTempList.action", null, "#wKTastModifyTempPage");
			$("#markup").css("width","75%");
		}
	}else{
		$("#showTemplateFun").css("display","none");
		$("#showAcceptUser").css("display","none");
		$("#wKTastModifyTempPage").remove();
	}
	changeInfo();
	
	var mttf = $("#wk_template_templateFieldsCode").val();
	var mttfItem = $("#wk_template_templateFieldsItemCode").val();
	reloadWkSampleTaskItem(mttf, mttfItem);
});

function reloadWkSampleTaskItem(mttf, mttfItem) {
	setTimeout(
			function() {
				if (mttf == null || mttf == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = wKItemGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttf.indexOf(colName) >= 0) {
							colArray2.push(colName);
							wKItemGrid.getColumnModel().setHidden(i,
									false);
						} else {
							wKItemGrid.getColumnModel().setHidden(i,
									true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
				if (mttfItem == null || mttfItem == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = wKSampleInfoGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttfItem.indexOf(colName) >= 0) {
							colArray2.push(colName);
							wKSampleInfoGrid.getColumnModel().setHidden(
									i, false);
						} else {
							wKSampleInfoGrid.getColumnModel().setHidden(
									i, true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
			}, 1000);
}

function add() {
	window.location = window.ctx + "/experiment/wkLife/wkTastModify/showWKTastModifyTempEdit.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});

$("#toolbarbutton_list").click(function() {
	list();
});
function list() {
	window.location = window.ctx + '/experiment/wkLife/wkTastModify/showWKTastModifyList.action';
}
function newSave(){
	save();
}
setTimeout(function() {
	if($("#wk_template").val()){
		var maxNum = $("#wk_maxNum").val();
		if(maxNum>0){
			load("/storage/container/sampleContainerTest.action", {
				id : $("#wk_template").val(),
				type :$("#type").val(),
				maxNum : 0
			}, "#3d_image0", function(){
				if(maxNum>0){
					load("/storage/container/sampleContainerTest.action", {
						id : $("#wk_template").val(),
						type :"wkInfo",
						maxNum : 1
					}, "#3d_image1", null);
				}
			});
		}
	}
}, 100);
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_print").click(function(){
	var url = '__report=WkTask.rptdesign&WKID=' + $("#wk_id").val();
	commonPrint(url);
});
$("#toolbarbutton_sp").click(function() {
	if($("#wk_confirmUser_name").val()==""){
		message("审核人不能为空！");
		return;
	}
	var type=$("#wk_type").val();
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#wk_id").val();
	
	var options = {};
	options.width = 929;
	options.height = 534;
	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}

	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null, biolims.common.approvalTask, url, {
		"Confirm" : function() {
			
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};
				
				
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);
				
			}else if(operVal=="1"){
				if(taskName==biolims.plasma.buildLib){
					
					
					var codeList = new Array();
					var codeList1 = new Array();
					var selRecord1 = wKItemGrid.store;
					var flag=true;
					var flag1=true;
						if (wKSampleInfoGrid.getAllRecord().length > 0) {
							var selRecord = wKSampleInfoGrid.store;
							for(var j=0;j<selRecord.getCount();j++){
								var oldv = selRecord.getAt(j).get("submit");
								var code = new Array();
								code = selRecord.getAt(j).get("tempId").split(",");
								for(var i=0;i<code.length;i++){
									codeList.push(code[i]);
								}
								if(selRecord.getAt(j).get("nextFlowId")==""){
									message(biolims.common.notFillNextStep);
									return;
								}
							}
							for(var j=0;j<selRecord1.getCount();j++){
								if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
									codeList1.push(selRecord1.getAt(j).get("code"));
									flag1=false;
									message(biolims.common.unfinishedExperiments);
								};
							}
							if(wKSampleInfoGrid.getModifyRecord().length > 0){
								message(biolims.common.pleaseSaveRecord);
								return;
							}
								if(flag){
									var myMask1 = new Ext.LoadMask(Ext.getBody(), {
										msg : biolims.common.pleaseWait
									});
									myMask1.show();
									Ext.MessageBox.confirm(biolims.common.makeSure, biolims.common.pleaseMakeSure2Deal, function(button, text) {
										if (button == "yes") {
											var paramData =  {};
											paramData.oper = $("#oper").val();
											paramData.info = $("#opinion").val();

											var reqData = {
												data : JSON.stringify(paramData),
												formId : formId,
												taskId : taskId,
												userId : window.userId
											};
											
											
											_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
												location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
											}, dialogWin);
										}
									});
									myMask1.hide();
								}else{
								}
						}
				}else{

					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();

					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};
					
					
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				
					
					
				
				}
			}
		},
		"查看流程图(Check flow chart)" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
});






function save() {
if(checkSubmit()==true){    
		Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var wKTastModifyItemDivData = wKTastModifyItemGrid;
		document.getElementById('wKTastModifyItemJson').value = commonGetModifyRecords(wKTastModifyItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/wkLife/wkTastModify/save.action";
		form1.submit();
	
		}
}		

function saveItem() {
	var id = $("#wk_id").val();
	if (!id) {
		return;
	}
	
		
		
		//Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });

	    var wKItemDivData = $("#wKItemdiv").data("wKItemGrid");
		var wKItemJson = commonGetModifyRecords(wKItemDivData);
	    var wKTemplateDivData = $("#wKTemplatediv").data("wKTemplateGrid");
		var wKTemplateJson = commonGetModifyRecords(wKTemplateDivData);
	    var wKReagentDivData = $("#wKReagentdiv").data("wKReagentGrid");
		var wKReagentJson = commonGetModifyRecords(wKReagentDivData);
	    var wKCosDivData = $("#wKCosdiv").data("wKCosGrid");
		var wKCosJson = commonGetModifyRecords(wKCosDivData);
	    var wKSampleInfoDivData = $("#wKSampleInfodiv").data("wKSampleInfoGrid");
		var wKSampleInfoJson  = commonGetModifyRecords(wKSampleInfoDivData);
		
		
	
	
	ajax("post", "/experiment/wkLife/saveAjax.action", {
		wKItemJson:wKItemJson,
		wKTemplateJson:wKTemplateJson,
		wKReagentJson:wKReagentJson,
		wKCosJson:wKCosJson,
		wKSampleInfoJson:wKSampleInfoJson,
		id : id
	}, function(data) {
		if (data.success) {
			//message("保存成功！");
			if(data.equip!=0&&typeof(data.equip)!='undefined'){
				message(data.equip+"条设备数据保存成功！");
			}
			if(data.re!=0&&typeof(data.re)!='undefined'){
				message(data.re+"条原辅料数据保存成功！");
			}
			wKItemDivData.getStore().reload();
			wKTemplateDivData.getStore().reload();
			wKReagentDivData.getStore().reload();
			wKCosDivData.getStore().reload();
			wKSampleInfoDivData.getStore().reload();
		} else {
			message(biolims.common.saveFailed);
		}
	}, null);
}

function editCopy() {
	window.location = window.ctx + '/experiment/wkLife/copyWK.action?id=' + $("#wk_id").val();
}
$("#toolbarbutton_status").click(function(){
	commonChangeState("formId=" + $("#wk_id").val() + "&tableId=WKTastModify");
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#wk_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#wk_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.plasma.buildLib,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/wkLife/wkTastModify/showWKTastModifyItemList.action", {
				id : $("#wk_id").val()
			}, "#wKTastModifyItempage");
load("/experiment/wkLife/showWKTemplateList.action", {
				id : $("#wk_id").val()
			}, "#wKTemplatepage");
load("/experiment/wkLife/showWKReagentList.action", {
				id : $("#wk_id").val()
			}, "#wKReagentpage");
load("/experiment/wkLife/showWKCosList.action", {
				id : $("#wk_id").val()
			}, "#wKCospage");
load("/experiment/wkLife/showWKResultList.action", {
				id : $("#wk_id").val()
			}, "#wKSampleInfopage");
load("/experiment/wkLife/showWKResultCtDNAList.action", {
				id : $("#wk_id").val()
			}, "#wKSampleInfoCtDNApage");
load("/experiment/wkLife/showWKResultrRNAList.action", {
				id : $("#wk_id").val()
			}, "#wKSampleInforRNApage");
load("/experiment/wkLife/showWKResultmRNAList.action", {
				id : $("#wk_id").val()
			}, "#wKSampleInfomRNApage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.plasma.copy
						});
	item.on('click', editCopy);
	//调用模板
	function TemplateFun(){
		var type="doWk";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:biolims.common.selectTemplate,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
	function setTemplateFun(rec){
		if($("#wk_acceptUser_name").val()==""){
			document.getElementById('wk_acceptUser').value=rec.get('acceptUser-id');
			document.getElementById('wk_acceptUser_name').value=rec.get('acceptUser-name');
		}
		var itemGrid=wKTastModifyItemGrid.store;
		if(itemGrid.getCount()>0){
			for(var i=0;i<itemGrid.getCount();i++){
				if(itemGrid.getAt(i).get("dicSampleType-id")==null){
					itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
					itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
				}
				if(itemGrid.getAt(i).get("productNum")==null){
					itemGrid.getAt(i).set("productNum",rec.get('productNum'));
				}
				if(itemGrid.getAt(i).get("sampleConsume")==null){
					itemGrid.getAt(i).set("sampleConsume",rec.get('sampleNum'));
				}
			}
		}
		
		
		var code=$("#wk_template").val();
		if(code==""){
					document.getElementById('wk_template').value=rec.get('id');
					document.getElementById('wk_template_name').value=rec.get('name');
					var win = Ext.getCmp('TemplateFun');
					if(win){win.close();}
					var id=rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {
								var ob = wKTemplateGrid.getStore().recordType;
								wKTemplateGrid.stopEditing();
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									
									p.set("note",obj.note);
									wKTemplateGrid.getStore().add(p);							
								});
								
								wKTemplateGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = wKReagentGrid.getStore().recordType;
								wKReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									wKReagentGrid.getStore().add(p);							
								});
								
								wKReagentGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = wKCosGrid.getStore().recordType;
								wKCosGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id",obj.typeId);
									p.set("type-name",obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("state",obj.state);
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									wKCosGrid.getStore().add(p);							
								});			
								wKCosGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null);
			
		}else{
			var flag = true;
			if(rec.get('id')==code){
				flag = confirm(biolims.common.whetherOrNot2Load);
 			 }
			if(flag==true){
						var ob1 = wKTemplateGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id");
								//=====================2015-11-30 ly======================//
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/wkLife/delWKTemplateOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null);
								}else{								
									wKTemplateGrid.store.removeAll();
								}
							}
							wKTemplateGrid.store.removeAll();
		 				}
		 				//===========================================//
						var ob2 = wKReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");
								//=====================2015-11-30 ly======================//
								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/wkLife/delWKReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message(biolims.common.deleteSuccess);
									} else {
										message(biolims.common.deleteFailed);
									}
								}, null); 
								}else{
									wKReagentGrid.store.removeAll();
								}
							}
							wKReagentGrid.store.removeAll();
		 				}
						//=========================================
						var ob3 = wKCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");
								//=====================2015-11-30 ly======================//
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/wkLife/delWKCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null); 
								}else{
									wKCosGrid.store.removeAll();
								}
							}
							wKCosGrid.store.removeAll();
		 				}
						document.getElementById('wk_template').value=rec.get('id');
		 				document.getElementById('wk_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = wKTemplateGrid.getStore().recordType;
									wKTemplateGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										
										p.set("note",obj.note);
										wKTemplateGrid.getStore().add(p);							
									});
									
									wKTemplateGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = wKReagentGrid.getStore().recordType;
									wKReagentGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tReagent",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("oneNum",obj.num);
										p.set("note",obj.note);
										wKReagentGrid.getStore().add(p);							
									});
									
									wKReagentGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = wKCosGrid.getStore().recordType;
									wKCosGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tCos",obj.id);
										p.set("type-id",obj.typeId);
										p.set("type-name",obj.typeName);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										p.set("state",obj.state);
										p.set("temperature",obj.temperature);
										p.set("speed",obj.speed);
										p.set("time",obj.time);
										p.set("note",obj.note);
										wKCosGrid.getStore().add(p);							
									});			
									wKCosGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
						}
					}
//				}, null);
//
// 			 }	 				
//		}
		reloadWkSampleTaskItem(rec.get("templateFieldsCode"), rec
				.get("templateFieldsItemCode"));

}
	
//按条件加载原辅料
function showReagent(){

	//获取全部数据
	var allRcords=wKTemplateGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=wKTemplateGrid.getSelectionModel().getSelections();
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#wk_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/wkLife/showWKReagentList.action", {
			id : $("#wk_id").val()
		}, "#wKReagentpage");
	}else if(length1==1){
		wKReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");

		ajax("post", "/experiment/wkLife/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = wKReagentGrid.getStore().recordType;
				wKReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("expireDate",obj.expireDate);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("sn",obj.sn);
					p.set("tReagent",obj.tReagent);
					p.set("wk-id",obj.tId);
					p.set("wk-name",obj.tName);
					
					wKReagentGrid.getStore().add(p);							
				});
				wKReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});	
	}else{
		message(biolims.common.selectRecord);
		return;
	}
	
}

//按条件加载设备
function showCos(){
	//获取全部数据
	var allRcords=wKTemplateGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=wKTemplateGrid.getSelectionModel().getSelections();
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#wk_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/wkLife/showWKCosList.action", {
			id : $("#wk_id").val()
		}, "#wKCospage");
	}else if(length1==1){
		var code="";
		$.each(selectRcords, function(i, obj) {
			code=obj.get("code");
			load("/experiment/wkLife/showWKCosList.action", {
				id : $("#wk_id").val(),
				itemId : code
			}, "#wKCospage");
		});
		wKCosGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");

		ajax("post", "/experiment/wkLife/setCos.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = wKCosGrid.getStore().recordType;
				wKCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("type-id",obj.typeId);
					p.set("type-name",obj.typeName);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("note",obj.note);
					p.set("state",obj.state);
					p.set("time",obj.time);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tCos",obj.tCos);
					p.set("wk-id",obj.tId);
					p.set("wk-name",obj.tName);
					
					wKCosGrid.getStore().add(p);							
				});
				wKCosGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.selectRecord);
		return;
	}
	
}

//加载info页面
function changeInfo(){
	var type=$("#wk_type").val();
	if(type=="0"){
		$("#wKSampleInfopage").css("display", "");
		$("#wKSampleInfoCtDNApage").css("display", "none");
		$("#wKSampleInforRNApage").css("display", "none");
		$("#wKSampleInfomRNApage").css("display", "none");
	}else if(type=="1"){
		$("#wKSampleInfopage").css("display", "none");
		$("#wKSampleInfoCtDNApage").css("display", "");
		$("#wKSampleInforRNApage").css("display", "none");
		$("#wKSampleInfomRNApage").css("display", "none");
	}else if(type=="2"){
		$("#wKSampleInfopage").css("display", "none");
		$("#wKSampleInfoCtDNApage").css("display", "none");
		$("#wKSampleInforRNApage").css("display", "");
		$("#wKSampleInfomRNApage").css("display", "none");
	}else if(type=="3"){
		$("#wKSampleInfopage").css("display", "none");
		$("#wKSampleInfoCtDNApage").css("display", "none");
		$("#wKSampleInforRNApage").css("display", "none");
		$("#wKSampleInfomRNApage").css("display", "");
	}
}

function change(){
	var selected = $('#wk_type option:selected') .val();
	if(selected=="0"){
		$("#wKSampleInfopage").css("display", "");
		$("#wKSampleInfoCtDNApage").css("display", "none");
		$("#wKSampleInforRNApage").css("display", "none");
		$("#wKSampleInfomRNApage").css("display", "none");
	}else if(selected=="1"){
		$("#wKSampleInfopage").css("display", "none");
		$("#wKSampleInfoCtDNApage").css("display", "");
		$("#wKSampleInforRNApage").css("display", "none");
		$("#wKSampleInfomRNApage").css("display", "none");
	}else if(selected=="2"){
		$("#wKSampleInfopage").css("display", "none");
		$("#wKSampleInfoCtDNApage").css("display", "none");
		$("#wKSampleInforRNApage").css("display", "");
		$("#wKSampleInfomRNApage").css("display", "none");
	}else if(selected=="3"){
		$("#wKSampleInfopage").css("display", "none");
		$("#wKSampleInfoCtDNApage").css("display", "none");
		$("#wKSampleInforRNApage").css("display", "none");
		$("#wKSampleInfomRNApage").css("display", "");
	}
	changeInfo();
}


Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.common.rollback
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm(biolims.common.prompt,biolims.common.initTaskList, function(button, text) {
		if (button == "yes") {
			var selRecord = wKSampleInfoGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message(biolims.common.canNotInit);
					return;
				}
			}
			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message(biolims.common.backSuccess);
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message(biolims.common.rollbackFailed);
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.common.save
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text:biolims.common.handleRollback
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: biolims.common.handlingRollback, progressText: biolims.common.handling, width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "WK_TASK",id : $("#wk_id").val()
	}, function(data) {
		if (data.success) {	
			message(biolims.common.handleRollbackSuccess);
		} else {
			message(biolims.common.handleRollbackFailed);
		}
	}, null);
}

	
//var loadtestUser;
//选择实验组用户
function testUser(type){
	var gid=$("#wk_acceptUser").val();
	if(gid!=""){
		var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
			"Confirm" : function() {
				var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					var id=[];
					var name=[];
					for(var i=0;i<selectRecord.length;i++){
						id.push(selectRecord[i].get("user-id"));
						name.push(selectRecord[i].get("user-name"));
					}
					if(type==1){//第一批实验员
						$("#wk_testUserOneId").val(id);
						$("#wk_testUserOneName").val(name);
					}else if(type==2){//第二批实验员
						$("#wk_testUserTwoId").val(id);
						$("#wk_testUserTwoName").val(name);
					}else if(type==3){//审核人
						$("#wk_confirmUser").val(id);
						$("#wk_confirmUser_name").val(name);
					}
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}else{
		message(biolims.common.pleaseSelectGroup);
	}
	
}

function selectTemp(){
	var options = {};
	options.width = document.body.clientWidth*0.9;
	options.height = document.body.clientHeight*0.9;
	var url = "/experiment/wkLife/showWKTempList.action";
	loadDialogPage(null, biolims.common.approvalTask, url, {}, true, options);
}


//查看附件图片
function showimg(model){
	var id=$("#wk_id").val();
	if(id!="NEW"){
		window.open(window.ctx+"/file/fileInfo/loadPic.action?id="
				+id+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
	}else{
		message("请先保存！");
	}
}