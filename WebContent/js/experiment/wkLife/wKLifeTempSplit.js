var wKTempSplitGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'splitCode',
		type:"string"
	});

	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"date",
		dateFormat:"Y-m-d"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'qbcontraction',
		type:"string"
	});	   
	   fields.push({
		name:'concentration',
		type:"string"
	});	
	
	   fields.push({
		name:'sumVolume',
		type:"string"
	});	    
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'wkReceive-id',
		type:"string"
	});
	    fields.push({
		name:'wkReceive-name',
		type:"string"
	});
    fields.push({
		name:'volume',
		type:"string"
	});
    fields.push({
		name:'unit',
		type:"string"
	});
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'labCode',
		type:"string"
	});
    
    fields.push({
		name:'RIN',
		type:"string"
	});
    fields.push({
		name:'concentration',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-id',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-receiveDate',
		type:"string"
	});
   fields.push({
		name:'sampleInfo-reportDate',
		type:"string"
	});
   fields.push({
		name:'techJkServiceTask-id',
		type:"string"
	});
  fields.push({
		name:'techJkServiceTask-name',
		type:"string"
	});
  fields.push({
		name:'techJkServiceTask-sequenceBillDate',
		type:"string"
	});
  fields.push({
		name:'techJkServiceTask-sequencePlatform',
		type:"string"
	});
  fields.push({
		name:'rowCode',
		type:"string"
	});
	fields.push({
		name:'colCode',
		type:"string"
	});
 fields.push({
		name:'counts',
		type:"string"
	});
  fields.push({
		name:'isZkp',
		type:"string"
	});
  fields.push({
		name:'tjItem-id',
		type:"string"
	});
 fields.push({
		name:'tjItem-inwardCode',
		type:"string"
	});
 fields.push({
		name:'tjItem-wgcId',
		type:"string"
	});
 fields.push({
		name:'tjItem-species',
		type:"string"
	});
 fields.push({
		name:'tjItem-dataNum',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'splitCode',
		hidden : false,
		header:'拆分后编号',
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		sortable:true,
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6,
	});
	cm.push({
		dataIndex:'tjItem-inwardCode',
		hidden : true,
		header:"内部项目号",
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		header:biolims.common.sampleNum,
		width:20*6,
	});
	cm.push({
		dataIndex:'RIN',
		header:'RIN',
		width:20*6,
	});
	cm.push({
		dataIndex:'concentration',
		header:biolims.common.concentration,
		width:20*6,
	});
	
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'qbconcentration',
		hidden : false,
		header:"Qubit浓度",
		width:20*6
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : false,
		header:"总量",
		width:20*6
	});

	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:"片段范围",
		width:20*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:"质控品",
		width:30*6,
	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:25*6,
		renderer: formatDate
	});

	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6,
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:20*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:biolims.common.counts,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'wkReceive-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:50*10
	});
	cm.push({
		dataIndex:'wkReceive-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.openBoxId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : true,
		header:biolims.common.sampleInfoReceiveDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : true,
		header:biolims.common.sampleInfoReportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : true,
		header:biolims.common.openBox,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'tjItem-id',
		hidden : true,
		header:"科技服务明细id",
		width:30*6
	});
	
	cm.push({
		dataIndex:'tjItem-dataNum',
		hidden : true,
		header:"数据量",
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-wgcId',
		hidden : false,
		header:"混合号",
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-species',
		hidden : true,
		header:"物种",
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequencePlatform',
		hidden : false,
		header:"测序平台",
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequenceBillDate',
		hidden : true,
		header:"截止日期",
		width:30*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/showWKTempListJson1.action";
	loadParam.limit=200;
	var opts={};
	opts.title='拆分的DNA样本';
	opts.height =  document.body.clientHeight-160;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '按产品拆分样本',
		handler : splitSamples
	});
	wKTempSplitGrid=gridEditTable("wKTempSplitdiv",cols,loadParam,opts);
	$("#wKTempSplitdiv").data("wKTempSplitGrid", wKTempSplitGrid);
});
function splitSamples(){
	var records = wKTempGrid.getSelectRecord();
	var grid=wKTempSplitGrid.store;
	var isRepeat = false;
	if (records.length>0) {
		$.each(records, function(i, obj) {
			for(var k=0;k<grid.getCount();k++){
				var sc = grid.getAt(k).get("splitCode");
				if (sc == obj.get("splitCode")) {
					isReapet=true;
					message("数据重复，请重新选择！");
					return;
				}
			}
			var productIds=obj.get("productId");
			var productNames=obj.get("productName");
			//alert(productIds);
			var array=productIds.split(",");
			var array1=productNames.split(",");
			//alert(array.length);
			if(array.length>1){
				for(var i=0;i<array.length;i++){
					var ob = wKTempSplitGrid.getStore().recordType;
					wKTempSplitGrid.stopEditing();
					var p = new ob({});
					p.isNew = true;
					p.set("splitCode",obj.get("splitCode"));
					p.set("code",obj.get("code"));
					p.set("sampleCode",obj.get("sampleCode"));
					p.set("labCode",obj.get("labCode"));
					p.set("sampleType",obj.get("sampleType"));
					p.set("sampleNum",obj.get("sampleNum"));
					p.set("RIN",obj.get("RIN"));
					p.set("concentration",obj.get("concentration"));
					p.set("volume",obj.get("volume"));
					p.set("qbconcentration",obj.get("qbconcentration"));
					p.set("sumVolume",obj.get("sumVolume"));
					p.set("name",obj.get("name"));
					p.set("patientName",obj.get("patientName"));
					p.set("idCard",obj.get("idCard"));
					p.set("isZkp",obj.get("isZkp"));
					p.set("inspectDate",obj.get("inspectDate"));
					p.set("acceptDate",obj.get("acceptDate"));
					p.set("sequenceFun",obj.get("sequenceFun"));
					p.set("reportDate",obj.get("reportDate"));
					p.set("orderId",obj.get("orderId"));
					p.set("rowCode",obj.get("rowCode"));
					p.set("colCode",obj.get("colCode"));
					p.set("counts",obj.get("counts"));
					p.set("state",obj.get("state"));
					p.set("note",obj.get("note"));
					p.set("unit",obj.get("unit"));
					p.set("classify",obj.get("classify"));
					p.set("sampleInfo-id",obj.get("sampleInfo-id"));
					p.set("sampleInfo-receiveDate",obj.get("sampleInfo-receiveDate"));
					p.set("sampleInfo-reportDate",obj.get("sampleInfo-reportDate"));
					p.set("sampleInfo-note",obj.get("sampleInfo-note"));
					p.set("techJkServiceTask-id",obj.get("techJkServiceTask-id"));
					p.set("techJkServiceTask-name",obj.get("techJkServiceTask-name"));
					p.set("tjItem-id",obj.get("tjItem-id"));
					p.set("tjItem-dataNum",obj.get("tjItem-dataNum"));
					p.set("tjItem-wgcId",obj.get("tjItem-wgcId"));
					p.set("isZkp",obj.get("isZkp"));
					p.set("productId",array[i]);
					p.set("productName",array1[i]);
					wKTempSplitGrid.getStore().add(p);
				}
			}else{
				message("样本是单个检测项目！");
			}
		});
	}else{
		message("请选择要拆分的样本！");
	}
}