var qualityProductAbnormalGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    	fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkId',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
//	   fields.push({
//		name:'idCard',
//		type:"string"
//	});
//   fields.push({
//		name:'phone',
//		type:"string"
//	});
   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	   fields.push({
		name:'wk-id',
		type:"string"
	});
	    fields.push({
		name:'wk-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.common.code,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.qcCode,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : false,
//		header:biolims.common.idCard,
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : false,
//		header:biolims.common.phone,
//		width:20*6
//	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:50*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:20*6
	});

	cm.push({
		dataIndex:'sequenceFun',
		hidden : false,
		header:biolims.common.sequencingFun,
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:biolims.common.orderId,
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'Blend Code',
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6,
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.qualified
			},{
				id : '0',
				name : biolims.common.qualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),
//		editor: result
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:biolims.common.orderNumber,
		width:10*7,
		sortable:true,
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.reason,
		width:20*6,
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkId',
		hidden : false,
		header:biolims.wk.wkCode,
		width:20*6
	});
	cm.push({
		dataIndex:'wk-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wk-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wkAbnormal/showQualityProductListJson.action";
	var opts={};
	opts.title=biolims.common.qcAbnormal;
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	/*opts.tbar.push({
		text : "批量下一步",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_next_div"), "批量下一步", null, {
				"确定" : function() {
					var records = wkAbnormalBackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var nextFlow = $("#nextFlow").val();
						wkAbnormalBackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("nextFlow", nextFlow);
						});
						wkAbnormalBackGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "批量执行",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok_div"), "批量执行", null, {
				"确定" : function() {
					var records = wkAbnormalBackGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isExecute").val();
						wkAbnormalBackGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isExecute", isExecute);
						});
						wkAbnormalBackGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});*/
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : search
//	});
	opts.tbar.push({
		iconCls : 'save',
		text :biolims.common.save,
		handler : save
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	qualityProductAbnormalGrid=gridEditTable("qualityProductAbnormaldiv",cols,loadParam,opts);
	$("#qualityProductAbnormaldiv").data("qualityProductAbnormalGrid", qualityProductAbnormalGrid);
});
//保存
function save(){	
	//var selectRecord = qualityProductAbnormalGrid.getSelectionModel();
	//var inItemGrid = $("#qualityProductAbnormaldiv").data("qualityProductAbnormalGrid");
	var itemJson = commonGetModifyRecords(qualityProductAbnormalGrid);
	if(itemJson.length>0){
		//$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/wkLife/repeat/saveWkRepeat.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#qualityProductAbnormaldiv").data("qualityProductAbnormalGrid").getStore().commitChanges();
					$("#qualityProductAbnormaldiv").data("qualityProductAbnormalGrid").getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
		//});
	}else{
		message(biolims.common.noData2Save);
	}
}
function selectWkqualityProductInfo(){
	commonSearchActionByMo(qualityProductAbnormalGrid,"1");
	$("#qualityProduct_sampleCode").val("");
	$("#qualityProduct_code").val("");
	//$("#qualityProduct_result").val("");
	$("#qualityProduct_wkId").val("");
}
//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(qualityProductAbnormalGrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();

		}
	}, true, option);
}


