﻿
var wKAcceptTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   
	   
	   
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
			name:'patientName',
			type:"string"
		});
	   fields.push({
			name:'sequenceFun',
			type:"string"
		});
	   fields.push({
			name:'productId',
			type:"string"
		});
	   fields.push({
			name:'productName',
			type:"string"
		});
	   fields.push({
			name:'reportDate',
			type:"string"
		});
	   fields.push({
			name:'inspectDate',
			type:"string"
		});
	   fields.push({
			name:'phone',
			type:"string"
		});
	   fields.push({
			name:'idCard',
			type:"string"
		});
	   fields.push({
			name:'orderId',
			type:"string"
		});
	   
	   
	   
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
	});
	cm.push({
		dataIndex:'code',
		header:biolims.common.code,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*6,
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : false,
		header:biolims.common.idCard,
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : false,
		header:biolims.common.phone,
		width:30*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : false,
		header:biolims.common.inspectDate,
		width:30*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:30*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : false,
		header:biolims.common.sequencingFun,
		width:30*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:30*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:30*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.totalVolume,
		width:50*6,
	});

	cm.push({
		dataIndex:'unit',
		hidden : false,
		header:biolims.common.unit,
		width:20*6,
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wKReceive/showWKAcceptTempListJson.action";
	var opts={};
	opts.title=biolims.dna.forExperimental;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = wKAcceptTempGrid.getAllRecord();
							var store = wKAcceptTempGrid.store;

							var isOper = true;
							var buf = [];
							wKAcceptTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								})
							});
							wKAcceptTempGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message(biolims.common.samplecodeComparison);
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							wKAcceptTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	wKAcceptTempGrid=gridEditTable("wKAcceptTempdiv",cols,loadParam,opts);
});

function addItem(){
	var selectRecord=wKAcceptTempGrid.getSelectionModel();
	var selRecord=wKReceiveItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("code");
				if(oldv == obj.get("code")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					break;					
				}
			}
			if(!isRepeat){
			var ob = wKReceiveItemGrid.getStore().recordType;
			wKReceiveItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
			p.set("code",obj.get("code"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("endDate",obj.get("endDate"));
			p.set("state",'1');
			
			wKReceiveItemGrid.getStore().add(p);
		}
			
		});
		wKReceiveItemGrid.startEditing(0, 0);
		}
	
}
