var wkAbnormalGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wkAbnormal/showWkAbnormalListJson.action";
	var opts={};
	opts.title=biolims.wk.wkAbnormal;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	wkAbnormalGrid=gridTable("show_wkAbnormal_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/wkLife/wkAbnormal/editWkAbnormal.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/experiment/wkLife/wkAbnormal/editWkAbnormal.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/experiment/wkLife/wkAbnormal/viewWkAbnormal.action?id=' + id;
}
function exportexcel() {
	wkAbnormalGrid.title = biolims.common.exportList;
	var vExportContent = wkAbnormalGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(wkAbnormalGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
