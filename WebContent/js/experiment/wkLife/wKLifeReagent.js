﻿
var wKReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'count',
		type:"string"
	});
	   fields.push({
			name:'oneNum',
			type:"string"
		});
		   fields.push({
			name:'sampleNum',
			type:"string"
		});
		   fields.push({
			name:'num',
			type:"string"
		});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	    fields.push({
		name:'wk-id',
		type:"string"
	});
	    fields.push({
		name:'wk-name',
		type:"string"
	});
	    fields.push({
		name:'itemId',
		type:"string"
	});
    fields.push({
		name:'tReagent',
		type:"string"
	});
    fields.push({
		name:'sn',
		type:"string"
	});
    fields.push({
		name:'expireDate',
		type:"string"
	});
    fields.push({
		name:'reagentCode',
		type:"string"
	});
    fields.push({
		name:'isRunout',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	
	var codes =new Ext.form.TextField({
        allowBlank: false
	});
	codes.on('focus', function() {
		var selectRecord = wKReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
//				var code=obj.get("code");
				var tid = $("#wk_template").val();
				loadReagentItemByCode(tid);
			});
		}
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:biolims.common.reagentNo,
		width:20*6,
		editor : codes
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.reagentName,
		width:45*6
	});
	cm.push({
		dataIndex:'sn',editor : new Ext.form.TextField({ allowBlank : true }),
		hidden : false,
		header:'sn',
		width:20*10
	});
	//鼠标单击触发事件 
	var batchs =new Ext.form.TextField({
            allowBlank: false
    });
	batchs.on('focus', function() {
		var selectRecord = wKReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("code");
				loadStorageReagentBuy(code);
			});
		}
	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:biolims.common.batch,
		width:20*6,
		editor:batchs
	});
	cm.push({
		dataIndex:'expireDate',
		hidden : false,
		header:biolims.common.expirationDate,
		width:20*6
	});
	var isGood = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name :biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isGood,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(isGood),editor: isGood
	});
	cm.push({
		dataIndex:'count',
		hidden : true,
		header:biolims.common.count,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'factNum',
		hidden : false,
		header:biolims.common.actualReactionNumber,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'oneNum',
		hidden : false,
		header:biolims.common.singleDose,//单个用量
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.dose,//用量
		width:20*6
	});
	
	cm.push({
		dataIndex:'reagentCode',
		hidden : true,
		header:'原辅料编码',
		width:20*6
	});
	var isRunout = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : biolims.common.yes
			},{
				id : '0',
				name :biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isRunout',
		hidden : false,
		header:biolims.common.doYouUseIt,
		width:20*5,
		renderer: Ext.util.Format.comboRenderer(isRunout),
		editor: isRunout
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*10
	});
	cm.push({
		dataIndex:'wk-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wk-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.itemId,
		width:20*10
	});
	cm.push({
		dataIndex:'tReagent',
		hidden : true,
		header:biolims.common.templateReagentId,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/showWKReagentListJson.action?id="+ $("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/wkLife/delWKReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = wKReagentGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							wKReagentGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//		text : biolims.common.fillDetail,
//		handler : function (){
//			//获取选择的数据
//			var selectRcords=wKTemplateGrid.getSelectionModel().getSelections();
//			//获取全部数据
//			var allRcords=wKTemplateGrid.store;
//			//选中的数量
//			var length1=selectRcords.length;
//			//全部数据量
//			var length2=allRcords.getCount();
//			if(length2>0){
//				if(length1==1){
//					var code="";
//					$.each(selectRcords, function(i, obj) {
//						code=obj.get("code");
//					});
//					if(code!=""){
//						showStorageList(code);
//					}else{
//						message(biolims.common.addTemplateDetail);
//						return;
//					}								
//				}else if(length1>1){
//					message(biolims.common.onlyChooseOne);
//					return;
//				}else{
//					message(biolims.common.pleaseSelectData);
//					return;
//				}
//			}else{
//				message(biolims.common.theDataIsEmpty);
//				return;
//			}
//		}
//	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	})
	opts.tbar.push({
		text : biolims.common.copy,
		handler : function() {
			var ob = wKReagentGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			var records = wKReagentGrid.getSelectRecord();
			if (records.length==1) {
				$.each(records, function(i, obj) {
					p.set("code",obj.get("code"));
					p.set("name",obj.get("name"));
					p.set("batch",obj.get("batch"));
					p.set("oneNum",obj.get("oneNum"));
					p.set("expireDate",obj.get("expireDate"));
					p.set("sampleNum",obj.get("sampleNum"));
					p.set("isGood",obj.get("isGood"));
					p.set("itemId",obj.get("itemId"));
					p.set("tReagent",obj.get("tReagent"));
					p.set("wk-id",obj.get("wk-id"));
					p.set("wk-name",obj.get("wk-name"));
					p.set("note",obj.get("note"));
					wKReagentGrid.stopEditing();
					wKReagentGrid.getStore().add(p);
					wKReagentGrid.startEditing(0, 0);
				});
			}else{
				message(biolims.common.pleaseChooseCopyData);
			}
		}
	});
	if($("#wk_id").val()&&$("#wk_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	
	}
	wKReagentGrid=gridEditTable("wKReagentdiv",cols,loadParam,opts);
	$("#wKReagentdiv").data("wKReagentGrid", wKReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//加载采购原辅料
function loadStorageReagentBuy(code){
	var win = Ext.getCmp('loadStorageReagentBuy');
	if (win) {win.close();}
	var loadStorageReagentBuy= new Ext.Window({
	id:'loadStorageReagentBuy',modal:true,title:biolims.common.chosePurchasingReagents,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/template/showStorageReagentBuyList.action?flag=StorageReagentBuy&codes="+code+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 loadStorageReagentBuy.close(); }  }]  });     loadStorageReagentBuy.show();
}
function setStorageReagentBuy(rec){
	
	var gridGrid = $("#wKReagentdiv").data("wKReagentGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		
		obj.set('batch',rec.get("code"));
	});
	var win = Ext.getCmp('loadStorageReagentBuy')
	if(win){
		win.close();
	}
}
//加载库存主数据
function showStorageList(code){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, biolims.common.theInventoryMasterData, url, {
		"Confirm" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					wKReagentGrid.stopEditing();
					var ob = wKReagentGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;

					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
					p.set("itemId",code);
				
					wKReagentGrid.getStore().add(p);	
				});
				wKReagentGrid.startEditing(0, 0);
				options.close();
			}else{
				message(biolims.common.pleaseSelect);
			}
		}
	}, true, options);
}
function selectwkFun(){
	var win = Ext.getCmp('selectwk');
	if (win) {win.close();}
	var selectwk= new Ext.Window({
	id:'selectwk',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKSelect.action?flag=wk' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectwk.close(); }  }]  });     selectwk.show(); }
	function setwk(id,name){
		var gridGrid = $("#wKReagentdiv").data("wKReagentGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('wk-id',id);
			obj.set('wk-name',name);
		});
		var win = Ext.getCmp('selectwk')
		if(win){
			win.close();
		}
	}
	
	//根据原辅料编号查询原辅料明细
	function loadReagentItemByCode(tid){
			var options = {};
			options.width = 900;
			options.height = 460;
//			var url="/system/template/template/showReagentItemByCodeList.action?code="+code+"&tid="+tid;
			var url="/system/template/template/showReagentItemByCodeList.action?tid="+tid;
			loadDialogPage(null, biolims.common.selectedDetail, url, {
				 "Confirm": function() {
					 selRecord = reagentItem1Grid.getSelectionModel();
						if (selRecord.getSelections().length > 0) {
							$.each(selRecord.getSelections(), function(i, obj) {
								wKReagentGrid.stopEditing();
								var ob = wKReagentGrid.getStore().recordType;
								var p = new ob({});
								p.isNew = true;
								p.set("code",obj.get("code"));
								p.set("name",obj.get("name"));
								p.set("batch",obj.get("batch"));
								p.set("isTestSuccess",obj.get("isGood"));
								p.set("itemId",obj.get("itemId"));
								
								p.set("oneNum",obj.get("num"));
								p.set("note",obj.get("note"));
								wKReagentGrid.getStore().add(p);	
							});
							wKReagentGrid.startEditing(0, 0);
							options.close();
						}else{
							message(biolims.common.pleaseSelect);
						}
					 options.close();
				}
			}, true, options);
	}