
var wKTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'insertSize',
		type:"string"
	});
    fields.push({
  		name:'dxpdVolume',
  		type:"string"
  	});
    fields.push({
		name:'wkVolume',
		type:"string"
	});
  fields.push({
		name:'wkSumTotal',
		type:"string"
	});
    fields.push({
		name:'loopNum',
		type:"string"
	});
    fields.push({
		name:'dxpdSumTotal',
		type:"string"
	});
    fields.push({
		name:'pcrRatio',
		type:"string"
	});
    fields.push({
		name:'indexa',
		type:"string"
	});
    fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'splitCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"date",
		dateFormat:"Y-m-d"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'qbcontraction',
		type:"string"
	});	   
	   fields.push({
		name:'concentration',
		type:"string"
	});	
	
	   fields.push({
		name:'sumVolume',
		type:"string"
	});	    
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'wkReceive-id',
		type:"string"
	});
	    fields.push({
		name:'wkReceive-name',
		type:"string"
	});
    fields.push({
		name:'volume',
		type:"string"
	});
    fields.push({
		name:'unit',
		type:"string"
	});
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
  
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'labCode',
		type:"string"
	});
    
    fields.push({
		name:'RIN',
		type:"string"
	});
    fields.push({
		name:'concentration',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-id',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-receiveDate',
		type:"string"
	});
   fields.push({
		name:'sampleInfo-reportDate',
		type:"string"
	});
   fields.push({
		name:'techJkServiceTask-id',
		type:"string"
	});
  fields.push({
		name:'techJkServiceTask-name',
		type:"string"
	});
  fields.push({
		name:'techJkServiceTask-sequenceBillDate',
		type:"string"
	});
  fields.push({
		name:'techJkServiceTask-sequencePlatform',
		type:"string"
	});
  fields.push({
		name:'rowCode',
		type:"string"
	});
	fields.push({
		name:'colCode',
		type:"string"
	});
 fields.push({
		name:'counts',
		type:"string"
	});
  fields.push({
		name:'isZkp',
		type:"string"
	});
  fields.push({
		name:'tjItem-id',
		type:"string"
	});
 fields.push({
		name:'tjItem-inwardCode',
		type:"string"
	});
 fields.push({
		name:'tjItem-wgcId',
		type:"string"
	});
 fields.push({
		name:'tjItem-species',
		type:"string"
	});
 fields.push({
		name:'tjItem-dataNum',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		width:15*10
	});
	
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'splitCode',
		header:'拆分编号',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'loopNum',
		header:biolims.wk.loopNum,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:biolims.common.labCode,
		width:20*6
	});
	  cm.push({
			dataIndex:'pcrRatio',
			hidden : true,
			header:biolims.wk.pcrRatio,
			width:20*6
		});
	    cm.push({
			dataIndex:'dxpdSumTotal',
			header:biolims.wk.dxpdSumTotal,
			width:20*6
		});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10
	});
	cm.push({
		dataIndex:'reason',
		header:'原因',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		header:biolims.common.sampleType,
		width:20*6,
	});
	cm.push({
		dataIndex:'insertSize',
		hidden : false,
		header:"文库片段大小（bp）",
		width:20*6
		
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:"检测项目",
		width:20*6,
	});
	cm.push({
		dataIndex:'tjItem-inwardCode',
		hidden : true,
		header:"内部项目号",
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		header:biolims.common.sampleNum,
		width:20*6,
	});
	cm.push({
		dataIndex:'RIN',
		header:'RIN',
		width:20*6,
	});
	cm.push({
		dataIndex:'concentration',
		header:biolims.common.concentration,
		width:20*6,
	});
	
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'qbconcentration',
		hidden : false,
		header:"Qubit浓度",
		width:20*6
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : false,
		header:"总量",
		width:20*6
	});

	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:"片段范围",
		width:20*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:"质控品",
		width:30*6,
	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:25*6,
		renderer: formatDate
	});

	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6,
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:20*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:biolims.common.counts,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'wkReceive-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:50*10
	});
	cm.push({
		dataIndex:'wkReceive-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.openBoxId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : true,
		header:biolims.common.sampleInfoReceiveDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : true,
		header:biolims.common.sampleInfoReportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : true,
		header:biolims.common.openBox,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'tjItem-id',
		hidden : true,
		header:"科技服务明细id",
		width:30*6
	});
	
	cm.push({
		dataIndex:'tjItem-dataNum',
		hidden : true,
		header:"数据量",
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-wgcId',
		hidden : false,
		header:"混合号",
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-species',
		hidden : true,
		header:"物种",
		width:20*6
	});
	cm.push({
		dataIndex:'wkVolume',
		header:biolims.wk.wkVolume,//文库体积
		width:20*6
	});
	cm.push({
		dataIndex:'dxpdVolume',
		header:biolims.wk.dxpdVolume,
		width:20*6,
		
	});
	cm.push({
		dataIndex:'wkSumTotal',
		header:biolims.wk.wkTotal,//文库总量
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequencePlatform',
		hidden : false,
		header:"测序平台",
		width:30*6
	});
	cm.push({
		dataIndex:'indexa',
		header:'Blend Code',
		width:20*6,
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequenceBillDate',
		hidden : true,
		header:"截止日期",
		width:30*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/wkTastModify/showWKTastModifyTempList1Json.action";
	loadParam.limit=200;
	var opts={};
	opts.title=biolims.wk.wkBuildingSample;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text :biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = wKTempGrid.getAllRecord();
							var store = wKTempGrid.store;

							var isOper = true;
							var buf = [];
							wKTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("splitCode")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							
							
							
							
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("splitCode"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message(biolims.common.noMatchSample+nolist);
							}
							
							
							
							
							
							
							wKTempGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
							//	message("样本号核对不符，请检查！");
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							wKTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		text : '拆分样本',
		handler : splitSample
	});
	opts.tbar.push({
		text : '样本窗口',
		handler : function() {
			$("#wKTempPage").css("width","60%");
			$("#markup").css("width","40%");
			wKTempGrid.getStore().reload();
		}
	});
	opts.tbar.push({
		text : biolims.wk.addQuality,
		handler : addQuality
	});
	opts.tbar.push({
		text : biolims.common.retrieve,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_js_div"), biolims.common.retrieve, null, {
				"Confirm": function() {
					commonSearchAction(wKTempGrid);
					$(this).dialog("close");
				}
			}, true, options);
			
		}
	});
	/*opts.tbar.push({
		text : biolims.common.sampleWindow,
		handler : changeWidth
	});
	
	opts.tbar.push({
		text : biolims.common.scienceService,
		handler : techDNAService
	});*/
	
	opts.tbar.push({
		text : biolims.common.save,
		handler : saveQuality
	});
	wKTempGrid=gridEditTable("wKTempdiv",cols,loadParam,opts);
	$("#wKTempdiv").data("wKTempGrid", wKTempGrid);
});


//保存添加到临时表的质控品
function saveQuality(){
	var itemJson = commonGetModifyRecords(wKTempGrid);
//	alert(itemJson.length);
	if(itemJson.length>0){
			ajax("post", "/experiment/wkLife/saveQuality.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#wKTempdiv").data("wKTempGrid").getStore().commitChanges();
					$("#wKTempdiv").data("wKTempGrid").getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);	
		
	}else{
		message(biolims.common.noData2Save);
	}
}

/**
 * 拆分样本
 */
function splitSample(){
	var records = wKTempGrid.getSelectRecord();
	var productIds="";
	if (records.length>0) {
		$.each(records, function(i, obj) {
			productIds=obj.get("productId");
		});
		var array=productIds.split(",");
		if(array.length>1){
			var options = {};
			options.width = 940;
			options.height = 580;
			loadDialogPage(null,"拆分样本", "/experiment/wkLife/showWKTempSplitList.action", {
				"确定" : function() {
					var grid=wKTempSplitGrid.store;
					if(grid.getCount()>0){
						$.each(records, function(i, obj) {
							ajax("post","/experiment/wkLife/wkTempSplit.action",
							{id : obj.get("id")},
							function(data) {
								if (data.success) {
									message("拆分成功！");
									wKTempGrid.getStore().reload();
								}
							}, null);
						});
					}else{
						message("请先拆分样本！");
					}
					options.close();
				}
			}, true, options);
		}else{
			message("样本是单个检测项目！");
		}
	}else{
		message("请选择要拆分的样本！");
	}
}
/*function splitSample(){
	var records = wKTempGrid.getSelectRecord();
	if (records.length>0) {
		$.each(records, function(i, obj) {
			var productIds=obj.get("productId");
			var productNames=obj.get("productName");
			//alert(productIds);
			var array=productIds.split(",");
			var array1=productNames.split(",");
			//alert(array.length);
			for(var i=0;i<array.length;i++){
				var ob = wKTempSplitGrid.getStore().recordType;
				wKTempSplitGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("code",obj.get("code"));
				p.set("sampleCode",obj.get("sampleCode"));
				p.set("productId",array[i]);
				p.set("productName",array1[i]);
				wKTempSplitGrid.getStore().add(p);
			}
		});
	}else{
		message("请选择要拆分的样本！");
	}
}*/

//添加质控品
function addQuality(){
	var options = {};
	options.width = 900;
	options.height = 460;
		var url="/system/quality/qualityProduct/showSelectQualityList.action";
		loadDialogPage(null, biolims.common.selectedDetail, url, {
			"Confirm": function() {
				var getQuality = selectQualityGrid.getSelectionModel().getSelections();
				var count=1;
				if(getQuality.length > 0){
					$.each(getQuality, function(i, obj) {
						if(obj.get("num")==""||obj.get("num")==0){
							message(biolims.common.pleaseInputNum);
						}else{
							for(var i=0; i<obj.get("num");i++){
								var ob = wKTempGrid.getStore().recordType;
								wKTempGrid.stopEditing();
								var p = new ob({});
								p.isNew = true;
								p.set("code",obj.get("id")+"-"+count);
								p.set("name",obj.get("name"));
								p.set("state",'1');
								p.set("isZkp",'1');
								wKTempGrid.getStore().add(p);
								count++;
							}
						}
					});
					
					wKTempGrid.startEditing(0, 0);
					
				}else{
					message(biolims.common.pleaseSelectQuality);
				}
				
				options.close();
			}
		}, true, options);
}
//添加任务到子表
function addItem(){
	var selectRecord=wKTempGrid.getSelectionModel();
	var selRecord=wKTastModifyItemGrid.store;
	var count=1;
	var max=0;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("splitCode")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					return;					
				}
			}
			if(!isRepeat){
			var ob = wKTastModifyItemGrid.getStore().recordType;
			wKTastModifyItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
			p.set("tempId",obj.get("id"));
			p.set("name",obj.get("name"));
			p.set("code",obj.get("code"));
			p.set("wkCode",obj.get("wkCode"));
			p.set("indexa",obj.get("indexa"));
			p.set("orderNumber",Number(max)+count);
			p.set("state","1");
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("splitCode",obj.get("splitCode"));
			p.set("volume",obj.get("volume"));
			p.set("unit",obj.get("unit"));
			p.set("result",obj.get("result"));
			p.set("nextFlowId",obj.get("nextFlowId"));
			p.set("nextFlow",obj.get("nextFlow"));
			p.set("reason",obj.get("reason"));
			p.set("note",obj.get("note"));
			p.set("patientName",obj.get("patientName"));
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("inspectDate",obj.get("inspectDate"));
			p.set("acceptDate",obj.get("acceptDate"));
			p.set("orderId",obj.get("orderId"));
			p.set("sequenceFun",obj.get("sequenceFun"));
			p.set("reportDate",obj.get("reportDate"));
			p.set("wk-id",obj.get("wk-id"));
			p.set("wk-name",obj.get("wk-name"));
			p.set("rowCode",obj.get("rowCode"));
			p.set("colCode",obj.get("colCode"));
			p.set("counts",obj.get("counts"));
			p.set("contractId",obj.get("contractId"));
			p.set("projectId",obj.get("projectId"));
			p.set("orderType",obj.get("orderType"));
			p.set("jkTaskId",obj.get("jkTaskId"));
			p.set("classify",obj.get("classify"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("i5",obj.get("i5"));
			p.set("i7",obj.get("i7"));
			p.set("concentration",obj.get("concentration"));
			p.set("loopNum",obj.get("loopNum"));
			p.set("sumTotal",obj.get("sumTotal"));
			p.set("pcrRatio",obj.get("pcrRatio"));
			p.set("expectNum",obj.get("expectNum"));
			p.set("sampleNum",obj.get("sampleNum"));
			p.set("chpsConcentration",obj.get("chpsConcentration"));
			p.set("chVolume",obj.get("chVolume"));
			p.set("chSumTotal",obj.get("chSumTotal"));
			p.set("indexConcentration",obj.get("indexConcentration"));
			p.set("dxpdConcentration",obj.get("dxpdConcentration"));
			p.set("dxpdVolume",obj.get("dxpdVolume"));
			p.set("dxpdSumTotal",obj.get("dxpdSumTotal"));
			p.set("wkConcentration",obj.get("wkConcentration"));
			p.set("wkVolume",obj.get("wkVolume"));
			p.set("wkSumTotal",obj.get("wkSumTotal"));
			p.set("labCode",obj.get("labCode"));
			p.set("sampleInfo-id",obj.get("sampleInfo-id"));
			p.set("sampleInfo-note",obj.get("sampleInfo-note"));
			p.set("sampleInfo-receiveDate",obj.get("sampleInfo-receiveDate"));
			p.set("sampleInfo-idCard",obj.get("sampleInfo-idCard"));
			p.set("sampleInfo-orderNum",obj.get("sampleInfo-orderNum"));
			p.set("sampleInfo-reportDate",obj.get("sampleInfo-reportDate"));
			p.set("techJkServiceTask-id",obj.get("techJkServiceTask-id"));
			p.set("wkConcentration",obj.get("concentration"));
			p.set("techJkServiceTask-name",obj.get("techJkServiceTask-name"));
			p.set("techJkServiceTask-insertSize",obj.get("techJkServiceTask-insertSize"));
			p.set("techJkServiceTask-libraryType",obj.get("techJkServiceTask-libraryType"));
			p.set("techJkServiceTask-sequenceType",obj.get("techJkServiceTask-sequenceType"));
			p.set("techJkServiceTask-sequenceType",obj.get("techJkServiceTask-sequenceType"));
			p.set("insertSize",obj.get("insertSize"));
			p.set("indexa",obj.get("indexa"));
			p.set("pcrRatio",obj.get("pcrRatio"));
			p.set("dxpdSumTotal",obj.get("dxpdSumTotal"));
			p.set("loopNum",obj.get("loopNum"));
			p.set("wkSumTotal",obj.get("wkSumTotal"));
			p.set("wkVolume",obj.get("wkVolume"));
			p.set("dxpdVolume",obj.get("dxpdVolume"));
			wKTastModifyItemGrid.getStore().add(p);
			count++;
		//}
			
			wKTastModifyItemGrid.startEditing(0, 0);
			
		}
			
	});
	}else{
		message(biolims.common.pleaseSelectSamples);
	}
	
}

//科技服务
function techDNAService(){
	var options = {};
 	options.width = document.body.clientWidth - 470;
 	options.height = document.body.clientHeight - 80;
	loadDialogPage(null, biolims.common.scienceServiceTest, "/technology/wkLife/techJkServiceTask/showTechJkServiceTaskDialogList.action?type=1&state=1", {
		"Confirm" : function() {
			var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					var id1=obj.get("id");
					$("#techJkServiceTask_id").val(id1);
					commonSearchAction(wKTempGrid);
				});
				
			}else{
				message(biolims.common.selectYouWant);
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}

function selectTemp(){
	if(window.location.href.indexOf("?")>0){
		location.href=window.location.href+"&method=window";
	}else{
		location.href=window.location.href+"?method=window";	
	}

}
