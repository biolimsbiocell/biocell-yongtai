﻿var wKSampleInfomRNAGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
    });
   fields.push({
		name:'phone',
		type:"string"
	});
   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'wk-id',
		type:"string"
	});
	    fields.push({
		name:'wk-name',
		type:"string"
	});
    fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   
	   fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
    fields.push({
		name:'orderType',
		type:"string"
	});
    fields.push({
		name:'jkTaskId',
		type:"string"
	});
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'i5',
		type:"string"
	});
    fields.push({
		name:'i7',
		type:"string"
	});
    fields.push({
		name:'concentration',
		type:"string"
	});
    fields.push({
		name:'loopNum',
		type:"string"
	});
  fields.push({
		name:'sumTotal',
		type:"string"
	});
  fields.push({
		name:'pcrRatio',
		type:"string"
	});
  fields.push({
		name:'expectNum',
		type:"string"
	});
  fields.push({
		name:'sampleNum',
		type:"string"
	});
  fields.push({
		name:'tempId',
		type:"string"
	});
  
  fields.push({
		name:'rin',
		type:"string"
	});
  fields.push({
		name:'nsConcentration',
		type:"string"
	});
  fields.push({
		name:'nsVolume',
		type:"string"
	});
  fields.push({
		name:'nsSumTotal',
		type:"string"
	});
  fields.push({
		name:'qcConcentration',
		type:"string"
	});
  fields.push({
		name:'qcVolume',
		type:"string"
	});
  fields.push({
		name:'qcSumTotal',
		type:"string"
	});
  fields.push({
		name:'qcYield',
		type:"string"
	});
  fields.push({
		name:'temperature',
		type:"string"
	});
  fields.push({
		name:'time',
		type:"string"
	});
  fields.push({
		name:'wkConcentration',
		type:"string"
	});
  fields.push({
		name:'wkVolume',
		type:"string"
	});
  fields.push({
		name:'wkSumTotal',
		type:"string"
	});
  fields.push({
		name:'labCode',
		type:"string"
	});
  fields.push({
		name:'sampleInfo-id',
		type:"string"
	});
fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
fields.push({
		name:'sampleInfo-receiveDate',
		type:"string"
	});
fields.push({
		name:'sampleInfo-reportDate',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : true,
		header:biolims.wk.wkCode,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:biolims.common.labCode,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.testProject,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'Blend Code',
		width:20*6
	});
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i5',
		width:20*6
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i7',
		width:20*6
	});
	cm.push({
		dataIndex:'rin',
		hidden : false,
		header:'RIN',
		width:20*6
	});
	cm.push({
		dataIndex:'nsConcentration',
		header:biolims.wk.nsConcentration,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'nsVolume',
		hidden : false,
		header:biolims.wk.nsVolume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'nsSumTotal',
		header:biolims.wk.nsSumTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'qcConcentration',
		header:biolims.wk.qcConcentration,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'qcVolume',
		hidden : false,
		header:biolims.wk.qcVolume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'qcSumTotal',
		header:biolims.wk.qcSumTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'qcYield',
		header:biolims.wk.qcYield,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.wk.temperature,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.wk.time,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		header:biolims.wk.cDNAConcentration,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.wk.cDNAVolume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'sumTotal',
		header:biolims.wk.cDNASumTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'loopNum',
		hidden : false,
		header:biolims.wk.loopNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkConcentration',
		header:biolims.wk.wkConcentration,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'wkVolume',
		hidden : false,
		header:biolims.wk.wkVolume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'wkSumTotal',
		header:biolims.wk.wkTotal,
		width:20*6
	});
	cm.push({
		dataIndex:'pcrRatio',
		hidden : false,
		header:biolims.wk.pcrRatio,
		width:20*6
	});
	cm.push({
		dataIndex:'expectNum',
		header:biolims.wk.expectNum,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:5
		})
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.common.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:biolims.common.idCard,
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:biolims.common.phone,
		width:20*6
	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:biolims.common.inspectDate,
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:biolims.common.acceptDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.orderId,
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:biolims.common.sequencingFun,
		width:20*6,
	});
	
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:biolims.common.unit,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.common.qualified
			}, {
				id : '0',
				name : biolims.common.disqualified
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCobm();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		editor : nextFlowCob
	});
	
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.common.yes
			}, {
				id : '0',
				name : biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.toSubmit,
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(submit)
		//editor: submit
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contractId',
		header:biolims.common.contractId,
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:biolims.common.projectId,
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderType',
		header:biolims.common.orderType,
		width:20*6,
		sortable:true,
		hidden : true
	});
	cm.push({
		dataIndex:'jkTaskId',
		hidden : true,
		header:biolims.common.taskId,
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.sampleInfoReportDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:biolims.common.rowCode,
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:biolims.common.colCode,
		width:20*6
	});
	cm.push({
		dataIndex:'true',
		hidden : true,
		header:biolims.common.counts,
		width:20*6
	});
	cm.push({
		dataIndex:'wk-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wk-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:biolims.common.classify,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:biolims.common.openBoxId,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : false,
		header:biolims.common.sampleInfoReceiveDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : false,
		header:biolims.common.sampleInfoReportDate,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:biolims.common.openBox,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/wkLife/showWKResultmRNAListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit=200;
	var opts={};
	opts.title=biolims.wk.mRNA;
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#wk_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/wkLife/delWKResultm.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				wKSampleInfomRNAGrid.getStore().commitChanges();
				wKSampleInfomRNAGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
//
//	opts.tbar.push({
//		text : "批量数据",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_datam_div"), "批量数据", null, {
//				"确定" : function() {
//					var records = wKSampleInfomRNAGrid .getSelectRecord();
//					if (records && records.length > 0) {
//						var volume = $("#wkVolumem").val();
//						var sampleNum = $("#sampleNumm").val();
//						wKSampleInfomRNAGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("volume", volume);
//							obj.set("sampleNum", sampleNum);
//						});
//						wKSampleInfomRNAGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	
	
//	 opts.tbar.push({
//			text : "批量粘贴导入",
//			handler : function() {
//				
//			
//				//$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br />字段：接收样本号、体积、浓度、260/230、260/280");
//				$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：样本号、体积、浓度、260/230、260/280");
//				$("#many_batm_text").val("");
//				$("#many_batm_text").attr("style", "width:465px;height: 339px");
//				var options = {};
//				options.width = 494;
//				options.height = 508;
//				loadDialogPage($("#many_batm_div"), "批量导入", null, {
//					"确定" : function() {
//						var positions = $("#many_batm_text").val();
//						if (!positions) {
//							message("请填写信息！");
//							return;
//						}
//						var posiObj = {};
//						var posiObj1 = {};
//						var posiObj2 = {};
//						var posiObj3 = {};
//						var array = formatData(positions.split("\n"));
//						$.each(array, function(i, obj) {
//							var tem = obj.split("\t");
//							posiObj[tem[0]] = tem[1];
//							posiObj1[tem[0]] = tem[2];
//							posiObj2[tem[0]] = tem[3];
//							posiObj3[tem[0]] = tem[4];
//						});
//						var records = wKSampleInfomRNAGrid.getAllRecord();
//						wKSampleInfomRNAGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							if (posiObj[obj.get("code")]) {
//								obj.set("volume", posiObj[obj.get("code")]);
//							}
//							if (posiObj1[obj.get("code")]) {
//								obj.set("contraction", posiObj1[obj.get("code")]);
//							}
//							if (posiObj2[obj.get("code")]) {
//								obj.set("od260", posiObj2[obj.get("code")]);
//							}
//							if (posiObj3[obj.get("code")]) {
//								obj.set("od280", posiObj3[obj.get("code")]);
//							}
//							//obj.set("unit-id","ng");
//							//obj.set("unit-name","ng");
//							//obj.set("result","1");
//							//obj.set("method","1");
//									
//						});
//						wKSampleInfomRNAGrid.startEditing(0, 0);
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}
//		});	
//	
	
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_resultm_div"), biolims.common.batchResult, null, {
				"Confirm" : function() {
					var records = wKSampleInfomRNAGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#resultm").val();
						wKSampleInfomRNAGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						wKSampleInfomRNAGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : loadTestNextFlowCobm
	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submitm_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = wKSampleInfomRNAGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submitm").val();
//						wKSampleInfomRNAGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						wKSampleInfomRNAGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.exportList,
		handler : exportexcelm
	});
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submitSample3
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	}
	wKSampleInfomRNAGrid=gridEditTable("wKSampleInfomRNAdiv",cols,loadParam,opts);
	$("#wKSampleInfomRNAdiv").data("wKSampleInfomRNAGrid", wKSampleInfomRNAGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function exportexcelm() {
	wKSampleInfomRNAGrid.title = biolims.common.exportList;
	var vExportContent = wKSampleInfomRNAGrid.getExcelXml();
	var x = document.getElementById('gridhtmm');
	x.value = vExportContent;
	document.excelfrmm.submit();
}
var loadNextFlow3;
//下一步流向
function loadTestNextFlowCobm(){
	var records1 = wKSampleInfomRNAGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow3=loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=WkTask&n=3&productId="+productId, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = wKSampleInfomRNAGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow3(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = wKSampleInfomRNAGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow3.dialog("close");
}	
	
//提交样本
function submitSample3(){
	var id=$("#wk_id").val();  
	if(wKSampleInfomRNAGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var grid=wKSampleInfomRNAGrid.store;
	var record = wKSampleInfomRNAGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("submit")){
			flg=true;
		}
		if(record[i].get("nextFlowId")==""){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
	}
	for(var i=0;i<grid.getCount();i++){
		if(grid.getAt(i).get("submit")==""){
			flg=true;
		}
		if(grid.getAt(i).get("nextFlowId")==""){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		var records = [];
		for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
		}
		ajax("post", "/experiment/wkLife/submitSamplemRNA.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				wKSampleInfomRNAGrid.getStore().commitChanges();
				wKSampleInfomRNAGrid.getStore().reload();
				message(biolims.common.submitSuccess);
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);	
	}else{
		message(biolims.common.noData2Submit);
	}
		
}