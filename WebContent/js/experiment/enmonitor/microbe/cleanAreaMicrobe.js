var settlingMicrobeTab;
$(function() {
	var options = table(
			true,
			"",
			"/experiment/enmonitor/microbe/cleanAreaMicrobe/showCleanAreaMicrodeTableJsonList.action",
			[ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "workshopDepartment",
				"title" : "车间",
			}, {
				"data" : "region",
				"title" : "区域",
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "testDate",
				"title" : "测试时间",
			}, {
				"data" : "testUser",
				"title" : "测试人",
			}, {
				"data" : "nameCultureMedium",
				"title" : "培养基名称",
			}, {
				"data" : "batchNumberMedium",
				"title" : "培养基批号",
			},
			// {
			// "data" : "mediumCode",
			// "title" : "培养基编号",
			// },
			{
				"data" : "mediumTemperature",
				"title" : "培养温度",
			}, {
				"data" : "observerUser",
				"title" : "观察人",
			}, {
				"data" : "notesObservationDate",
				"title" : "观察日期",
			}, {
				"data" : "stateName",
				"title" : "状态",
			} ], null)
	settlingMicrobeTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(settlingMicrobeTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/enmonitor/microbe/cleanAreaMicrobe/showCleanAreaMicrodeEdit.action?sampleStyle='
			+ $("#sampleStyle").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/enmonitor/microbe/cleanAreaMicrobe/showCleanAreaMicrodeEdit.action?id='
			+ id + "&sampleStyle=" + $("#sampleStyle").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/enmonitor/microbe/cleanAreaMicrobe/showCleanAreaMicrodeEdit.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : "车间",
		"type" : "input",
		"searchName" : "workshopDepartment",
	}, {
		"txt" : "区域",
		"type" : "input",
		"searchName" : "region",
	}, {
		"txt" : "创建人",
		"type" : "input",
		"searchName" : "createUser-name",
	}, {
		"txt" : "测试人",
		"type" : "input",
		"searchName" : "testUser",
	}, {
		"txt" : "培养基名称",
		"type" : "input",
		"searchName" : "nameCultureMedium",
	}, {
		"txt" : "培养基批号",
		"type" : "input",
		"searchName" : "batchNumberMedium",
	}, {
		"txt" : "培养基温度",
		"type" : "input",
		"searchName" : "mediumTemperature",
	}, {
		"txt" : "观察人",
		"type" : "input",
		"searchName" : "observerUser",
	}, {
		"txt" : "状态",
		"type" : "input",
		"searchName" : "stateName",
	}, {
		"txt" : "观察日期(开始)",
		"type" : "dataTime",
		"searchName" : "notesObservationDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "观察日期(结束)",
		"type" : "dataTime",
		"searchName" : "notesObservationDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : "测试时间（开始）",
		"type" : "dataTime",
		"searchName" : "testDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "测试时间（结束）",
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "testDate##@@##2"
	}, {
		"type" : "table",
		"table" : settlingMicrobeTab
	} ];
}
