﻿var settlingMicrobeAllotTab;
$(function() {
	var colOpts=[]
	colOpts.push({
		"data" : "id",
		"visible" : false,
		"title" : biolims.common.id,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data" : "roomNumber",
		"title" : '房间号',
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "roomNumber");
		}
	})
	colOpts.push({
		"data" : "samplingNumber",
		"title" : '采样点数',
		"createdCell" : function(td) {
			$(td).attr("saveName", "samplingNumber");
		}
	})
	colOpts.push({
		"data" : "cultureDishNumber",
		"title" : '培养皿数',
		"createdCell" : function(td) {
			$(td).attr("saveName", "cultureDishNumber");
		}
	})
	colOpts.push({
		"data" : "clumpCount",
		"title" : '沉降菌菌落数',
		"createdCell" : function(td) {
			$(td).attr("saveName", "clumpCount");
		}
	})
	var tbarOpts=[]
	tbarOpts.push({
		text : biolims.common.fillDetail,
		action : function() {
			addItem($("#main"));
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	var options = table(true,$("#settlingMicrobe_id").text(),"/experiment/enmonitor/microbe/settlingMicrobe/showSettlingMicrobeItemTableJson.action",colOpts, tbarOpts)
	//渲染样本的table
	settlingMicrobeAllotTab=renderData($("#main"),options);
	
	
	//提示样本选中数量
	var selectednum=$(".selectednum").text();
		if(!selectednum){
			selectednum=0;
	}
	settlingMicrobeAllotTab.on('draw', function() {
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			$(".selectednum").text(index+parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	//选中模板的实验组的实验员显示隐藏
	if($(".sopChosed").length){
		var userGroup=$(".sopChosed").attr("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).show();
			}else{
				$(v).hide();
			}
		});
	}
	//sop和实验员的点击操作
	sopAndUserHandler(selectednum);
	//保存操作
	allotSave();
});
//sop和实验员的点击操作
function sopAndUserHandler(selectednum) {
	//执行sop点击选择操作
	$(".sopLi").click(function() {
		var length = $(".selected").length;
		$(this).addClass("sopChosed").siblings("li").removeClass("sopChosed");
		$(".label-warning").removeClass("selectednum").html("");
		$(this).children(".label-warning").addClass("selectednum").text(length+parseInt(selectednum));
		if($(this).children(".label-primary").text()) {
			if($("#maxNum").val() == "0") {
				$("#maxNum").val("1");
			}
		} else {
			$("#maxNum").val("0");
		}
		//显示隐藏模板里配置的实验员
		var userGroup=this.getAttribute("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).slideDown();
			}else{
				$(v).slideUp().children("img").removeClass("userChosed");
			}
		});
	});
	//执行实验员点击选择操作
	$(".userLi").click(function() {
		var length = $(".selected").length;
			var img = $(this).children("img");
			if(img.hasClass("userChosed")) {
				img.removeClass("userChosed");
			} else {
				img.addClass("userChosed");
			}		
	});
}
//保存操作
function allotSave() {
	$("#save").click(function() {
		var changeLog = "";
		var itemData = saveItemjson($("#main"));
		if(!$(".sopChosed").length) {
			top.layer.msg(biolims.common.selectTaskModel);
			return false;
		}
		if(!$(".userChosed").length) {
			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
			return false;
		}
		//获得选中样本的ID
		/*var sampleId = [];
		$(".selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
		});*/
		//获得选中实验员的ID
		var userId = [];
		$(".userChosed").each(function(i, val) {
			userId.push($(val).parent("li").attr("userid"));
		});
		changeLog = getChangeLog(userId, changeLog);
		if(changeLog !=""){
			changeLogs=changeLog;
		}
		
		//拼接主表的信息
		var main = {
			id: $("#settlingMicrobe_id").text(),
			name: $("#settlingMicrobe_name").val(),
			"createUser-id": $("#settlingMicrobe_createUser").attr("userId"),
			createDate: $("#settlingMicrobe_createDate").text(),
			state: $("#settlingMicrobe_state").attr("state"),
			stateName: $("#settlingMicrobe_state").text(),
			sampleNum:$(".selectednum").text(),
			maxNum:$("#maxNum").val()
		};
		var data = JSON.stringify(main);
		//拼接保存的data
		var info = {
//			temp: sampleId,
			template: $(".sopChosed").attr("sopid"),
			user: userId.join(","),
			main: data,
			logInfo: changeLogs,
			itemData:itemData
		}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
		type : 'post',
		url:ctx+"/experiment/enmonitor/microbe/settlingMicrobe/saveAllot.action",
		data:info,
		success:function(data){
			var data = JSON.parse(data);
			if(data.success){	
				top.layer.closeAll();			
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx
			+ '/experiment/enmonitor/microbe/settlingMicrobe/editSettlingMicrobe.action?id=' + data.id;
			}
		}
		})
	});
}
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//下一步操作
$("#next").click(function () {
	var settlingMicrobe_id=$("#settlingMicrobe_id").text();
	if(settlingMicrobe_id=="NEW"){
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/enmonitor/microbe/settlingMicrobe/showSettlingMicrobeSteps.action?id="+settlingMicrobe_id+"&bpmTaskId="+$("#bpmTaskId").val();
});
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"txt":biolims.common.projectId,
		"type": "input",
		"searchName": "sampleInfo-project-id",
	}, {
		"txt":biolims.common.orderCode,
		"type": "input",
		"searchName": "orderCode",
	}, {
		"txt":biolims.common.sampleType,
		"type": "input",
		"searchName": "sampleType",
	}, {
		"txt":biolims.common.productName,
		"type": "input",
		"searchName": "productName",
	},  {
		"type" : "table",
		"table" : settlingMicrobeAllotTab
	} ];
}
//核对编码
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layero) {
			var arr = $("#checkCode",parent.document).val().split("\n");
			settlingMicrobeAllotTab.settings()[0].ajax.data = {"codes":arr};
			settlingMicrobeAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});

}
//获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { //获取待处理样本的修改日志
	var sampleNew = [];
	$(".selected").each(function(i, val) {
		sampleNew.push($(val).children("td").eq(1).text());
	});
	changeLog += '"待处理样本"新增有：' + sampleNew + ";";
	//获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	//获取模板sop的修改日志
	if($(".sopChosed").attr("sopid") != $("#sopChangelogId").val()) {
		changeLog += '实验模板:由"' + $("#sopChangelogName").val() + '"变为"' + $(".sopChosed").children('.text').text() + '";';
		changeLog += '样本数量:由"' + $("#sopChangelogSampleNum").val() + '"变为"' + $(".sopChosed").children('.badge').text() + '";';
	}
	//获取实验员的修改日志
	var oldUserId = [],
		userName = [],
		oldUserName = [];
	$('input[class="changelogUserid"]').each(function(i, v) {
		oldUserId.push(v.value);
	});
	if(userId != oldUserId) {
		$(".userChosed").each(function(i, val) {
			userName.push($(val).next("a").text());
		});
		$('input[class="changelogUsername"]').each(function(i, v) {
			oldUserName.push(v.value);
		});
		changeLog += '实验员:由"' + oldUserName + '"变为"' + userName + '";';
	}

	return changeLog;
}
