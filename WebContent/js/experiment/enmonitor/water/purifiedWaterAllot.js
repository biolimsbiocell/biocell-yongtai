﻿var purifiedWaterAllotTab;
$(function() {
	var colOpts=[]
	colOpts.push({
		"data" : "id",
		"visible" : false,
		"title" : biolims.common.id,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data" : "roomNumber",
		"title" : '房间号',
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "roomNumber");
		}
	})
	colOpts.push({
		"data" : "characters",
		"title" : '性状',
		"createdCell" : function(td) {
			$(td).attr("saveName", "characters");
		}
	})
	colOpts.push({
		"data" : "phValue",
		"title" : '酸碱度',
		"createdCell" : function(td) {
			$(td).attr("saveName", "phValue");
		}
	})
	colOpts.push({
		"data" : "nitrate",
		"title" : '硝酸盐',
		"createdCell" : function(td) {
			$(td).attr("saveName", "nitrate");
		}
	})
	colOpts.push({
		"data" : "nitrite",
		"title" : '亚硝酸盐',
		"createdCell" : function(td) {
			$(td).attr("saveName", "nitrite");
		}
	})
	colOpts.push({
		"data" : "ammonia",
		"title" : '氨',
		"createdCell" : function(td) {
			$(td).attr("saveName", "ammonia");
		}
	})
	colOpts.push({
		"data" : "conductivity",
		"title" : '电导率',
		"createdCell" : function(td) {
			$(td).attr("saveName", "conductivity");
		}
	})
	colOpts.push({
		"data" : "toc",
		"title" : 'TOC',
		"createdCell" : function(td) {
			$(td).attr("saveName", "toc");
		}
	})
	colOpts.push({
		"data" : "heavyMetal",
		"title" : '重金属',
		"createdCell" : function(td) {
			$(td).attr("saveName", "heavyMetal");
		}
	})
	colOpts.push({
		"data" : "microbialLimit",
		"title" : '微生物限度',
		"createdCell" : function(td) {
			$(td).attr("saveName", "microbialLimit");
		}
	})
	var tbarOpts=[]
	tbarOpts.push({
		text : biolims.common.fillDetail,
		action : function() {
			addItem($("#main"));
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	var options = table(true,$("#purifiedWater_id").text(),"/experiment/enmonitor/water/purifiedWater/showPurifiedWaterItemTableJson.action",colOpts, tbarOpts)
	//渲染样本的table
	purifiedWaterAllotTab=renderData($("#main"),options);
	
	
	//提示样本选中数量
	var selectednum=$(".selectednum").text();
		if(!selectednum){
			selectednum=0;
	}
	purifiedWaterAllotTab.on('draw', function() {
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			$(".selectednum").text(index+parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	//选中模板的实验组的实验员显示隐藏
	if($(".sopChosed").length){
		var userGroup=$(".sopChosed").attr("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).show();
			}else{
				$(v).hide();
			}
		});
	}
	//sop和实验员的点击操作
	sopAndUserHandler(selectednum);
	//保存操作
	allotSave();
});
//sop和实验员的点击操作
function sopAndUserHandler(selectednum) {
	//执行sop点击选择操作
	$(".sopLi").click(function() {
		var length = $(".selected").length;
		$(this).addClass("sopChosed").siblings("li").removeClass("sopChosed");
		$(".label-warning").removeClass("selectednum").html("");
		$(this).children(".label-warning").addClass("selectednum").text(length+parseInt(selectednum));
		if($(this).children(".label-primary").text()) {
			if($("#maxNum").val() == "0") {
				$("#maxNum").val("1");
			}
		} else {
			$("#maxNum").val("0");
		}
		//显示隐藏模板里配置的实验员
		var userGroup=this.getAttribute("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).slideDown();
			}else{
				$(v).slideUp().children("img").removeClass("userChosed");
			}
		});
	});
	//执行实验员点击选择操作
	$(".userLi").click(function() {
		var length = $(".selected").length;
			var img = $(this).children("img");
			if(img.hasClass("userChosed")) {
				img.removeClass("userChosed");
			} else {
				img.addClass("userChosed");
			}		
	});
}
//保存操作
function allotSave() {
	$("#save").click(function() {
		var changeLog = "";
		var itemData = saveItemjson($("#main"));
		if(!$(".sopChosed").length) {
			top.layer.msg(biolims.common.selectTaskModel);
			return false;
		}
		if(!$(".userChosed").length) {
			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
			return false;
		}
		//获得选中样本的ID
		/*var sampleId = [];
		$(".selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
		});*/
		//获得选中实验员的ID
		var userId = [];
		$(".userChosed").each(function(i, val) {
			userId.push($(val).parent("li").attr("userid"));
		});
		changeLog = getChangeLog(userId, changeLog);
		if(changeLog !=""){
			changeLogs=changeLog;
		}
		
		//拼接主表的信息
		var main = {
			id: $("#purifiedWater_id").text(),
			name: $("#purifiedWater_name").val(),
			"createUser-id": $("#purifiedWater_createUser").attr("userId"),
			createDate: $("#purifiedWater_createDate").text(),
			state: $("#purifiedWater_state").attr("state"),
			stateName: $("#purifiedWater_state").text(),
			sampleNum:$(".selectednum").text(),
			maxNum:$("#maxNum").val()
		};
		var data = JSON.stringify(main);
		//拼接保存的data
		var info = {
//			temp: sampleId,
			template: $(".sopChosed").attr("sopid"),
			user: userId.join(","),
			main: data,
			logInfo: changeLogs,
			itemData:itemData
		}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
		type : 'post',
		url:ctx+"/experiment/enmonitor/water/purifiedWater/saveAllot.action",
		data:info,
		success:function(data){
			var data = JSON.parse(data);
			if(data.success){	
				top.layer.closeAll();			
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx
			+ '/experiment/enmonitor/water/purifiedWater/editPurifiedWater.action?id=' + data.id;
			}
		}
		})
	});
}
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//下一步操作
$("#next").click(function () {
	var purifiedWater_id=$("#purifiedWater_id").text();
	if(purifiedWater_id=="NEW"){
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/enmonitor/water/purifiedWater/showPurifiedWaterSteps.action?id="+purifiedWater_id+"&bpmTaskId="+$("#bpmTaskId").val();
});
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"txt":biolims.common.projectId,
		"type": "input",
		"searchName": "sampleInfo-project-id",
	}, {
		"txt":biolims.common.orderCode,
		"type": "input",
		"searchName": "orderCode",
	}, {
		"txt":biolims.common.sampleType,
		"type": "input",
		"searchName": "sampleType",
	}, {
		"txt":biolims.common.productName,
		"type": "input",
		"searchName": "productName",
	},  {
		"type" : "table",
		"table" : purifiedWaterAllotTab
	} ];
}
//核对编码
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layero) {
			var arr = $("#checkCode",parent.document).val().split("\n");
			purifiedWaterAllotTab.settings()[0].ajax.data = {"codes":arr};
			purifiedWaterAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});

}
//获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { //获取待处理样本的修改日志
	var sampleNew = [];
	$(".selected").each(function(i, val) {
		sampleNew.push($(val).children("td").eq(1).text());
	});
	changeLog += '"待处理样本"新增有：' + sampleNew + ";";
	//获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	//获取模板sop的修改日志
	if($(".sopChosed").attr("sopid") != $("#sopChangelogId").val()) {
		changeLog += '实验模板:由"' + $("#sopChangelogName").val() + '"变为"' + $(".sopChosed").children('.text').text() + '";';
		changeLog += '样本数量:由"' + $("#sopChangelogSampleNum").val() + '"变为"' + $(".sopChosed").children('.badge').text() + '";';
	}
	//获取实验员的修改日志
	var oldUserId = [],
		userName = [],
		oldUserName = [];
	$('input[class="changelogUserid"]').each(function(i, v) {
		oldUserId.push(v.value);
	});
	if(userId != oldUserId) {
		$(".userChosed").each(function(i, val) {
			userName.push($(val).next("a").text());
		});
		$('input[class="changelogUsername"]').each(function(i, v) {
			oldUserName.push(v.value);
		});
		changeLog += '实验员:由"' + oldUserName + '"变为"' + userName + '";';
	}

	return changeLog;
}
