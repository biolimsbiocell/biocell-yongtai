var roomTemplateTab;
$(function() {
	var options = table(
			true,
			"",
			"/experiment/enmonitor/template/roomTemplate/showRoomTemplateTableJsonListsNew.action",
			[ {
				"data" : "id",
				"title" : "编号",
				"createdCell" : function(td) {
					$(td).attr("saveName", "id");
				}
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
				"createdCell" : function(td) {
					$(td).attr("saveName", "createUser-name");
				}
			},{
				"data" : "confirmUser-name",
				"title" : "审核人",
				"createdCell" : function(td) {
					$(td).attr("saveName", "confirmUser-name");
				}
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
				"createdCell" : function(td) {
					$(td).attr("saveName", "createDate");
				}
			}, /*{
				"data" : "finishDate",
				"title" : "完成日期",
				"createdCell" : function(td) {
					$(td).attr("saveName", "finishDate");
				}
			},*/ {
				"data" : "note",
				"title" : "备注",
			}, {
				"data" : "stateName",
				"title" : "状态",
			} ], null)
	roomTemplateTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(roomTemplateTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/enmonitor/template/roomTemplate/showRoomTemplateEdit.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/enmonitor/template/roomTemplate/showRoomTemplateEdit.action?id='
			+ id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
		+ '/experiment/enmonitor/template/roomTemplate/showRoomTemplateView.action?id='
		+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : "编号",
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	},/* {
		"txt" : "完成时间(开始)",
		"type" : "dataTime",
		"searchName" : "finishDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "完成时间(结束)",
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "finishDate##@@##2"
	},*/{
		"txt" : "创建人",
		"type" : "input",
		"searchName" : "createUser-name",
	},{
		"txt" : "审核人",
		"type" : "input",
		"searchName" : "confirmUser-name",
	}, {
		"txt" : "状态",
		"type" : "input",
		"searchName" : "stateName",
	}, {
		"type" : "table",
		"table" : roomTemplateTab
	} ];
}
