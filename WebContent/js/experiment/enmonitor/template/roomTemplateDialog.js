var roomTemplateTab;
$(function() {
	var options = table(
			false,
			"",
			"/experiment/enmonitor/template/roomTemplate/showRoomTemplateTableJsonList.action?region="+$("#region").val(),
			[ {
				"data" : "id",
				"title" : "编号",
				"createdCell" : function(td) {
					$(td).attr("saveName", "id");
				}
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
				"createdCell" : function(td) {
					$(td).attr("saveName", "createUser-name");
				}
			},{
				"data" : "confirmUser-name",
				"title" : "审核人",
				"createdCell" : function(td) {
					$(td).attr("saveName", "confirmUser-name");
				}
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
				"createdCell" : function(td) {
					$(td).attr("saveName", "createDate");
				}
			}, {
				"data" : "region",
				"title" : "区域",
				"createdCell" : function(td) {
					$(td).attr("saveName", "region");
				}
			},{
				"data" : "note",
				"title" : "备注",
			}, {
				"data" : "stateName",
				"title" : "状态",
			} ], null)
	roomTemplateTab = renderData($("#main"), options);
	$('#main').on('init.dt', function(e, settings) {
			// 清除操作按钮
			$("#main_wrapper .dt-buttons").empty();
			$('#main_wrapper').css({
				"padding": "0 16px"
			});
		var trs = $("#main tbody tr");
		roomTemplateTab.ajax.reload();
		roomTemplateTab.on('draw', function() {
			trs = $("#main tbody tr");
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
});

function query(){
	var id = $.trim($("#id").val());
	var searchItemLayerValue = {};
	var k1 = $("#id").attr("searchname");
	if(id != null && id != "") {
		searchItemLayerValue[k1] = "%" + id + "%";
	} else {
		searchItemLayerValue[k1] = id;
	}
	
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	//sessionStorage.setItem("searchContent", query);
	roomTemplateTab.settings()[0].ajax.data = param;
	roomTemplateTab.ajax.reload();
}