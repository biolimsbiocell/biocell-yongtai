var DicTypeTable;
$(function() {
	Array.prototype.remove = function(val) {
		var index = this.indexOf(val);
		if(index > -1) {
			this.splice(index, 1);
		}
	};
	$("body").prepend("<input class='hidden' id='userid'/><input class='hidden' id='username'/>已选中数据<textarea rows='2' cols='30' id='usersname' readonly='readonly'></textarea>");
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
	});
	colOpts.push({
		"data" : "name",
		"title" :"姓名",
	});
	var tbarOpts = [];
//	tbarOpts.push({
//		
//	});
	var addDicTypeOptions = table(true, null,
			'/experiment/enmonitor/dust/cleanAreaDust/selUsersTableListJson.action',
			colOpts, tbarOpts);//传值
	 DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions);
//	$("#addDicTypeTable").on(
//			'init.dt',
//			function(e, settings) {
//				// 清除操作按钮
//				//$("#addDicTypeTable_wrapper .dt-buttons").empty();
//				$('#addDicTypeTable_wrapper').css({
//					"padding": "0 16px"
//				});
//			var trs = $("#addDicTypeTable tbody tr");
//			DicTypeTable.ajax.reload();
//			DicTypeTable.on('draw', function() {
//				trs = $("#addDicTypeTable tbody tr");
//				trs.click(function() {
//					$(this).addClass("chosed").siblings("tr")
//						.removeClass("chosed");
//				});
//			});
//			trs.click(function() {
//				$(this).addClass("chosed").siblings("tr")
//					.removeClass("chosed");
//			});
//			});
	 var id;
	 var name;
	if($("#maincontentframe", parent.document).contents().find("#dif_notifierId").val()==null
			||$("#maincontentframe", parent.document).contents().find("#dif_notifierId").val()==""){
		if($("#maincontentframe", parent.document).contents().find("#cam_notifierId").val()==null
				||$("#maincontentframe", parent.document).contents().find("#cam_notifierId").val()==""){
			if($("#maincontentframe", parent.document).contents().find("#cad_notifierId").val()==null
					||$("#maincontentframe", parent.document).contents().find("#cad_notifierId").val()==""){
				id=[];
				name=[];
			}else{
				id = $("#maincontentframe", parent.document).contents().find("#cad_notifierId").val().split(",");
				name = $("#maincontentframe", parent.document).contents().find("#cad_notifierName").val().split(",");
			}
		}else{
			id = $("#maincontentframe", parent.document).contents().find("#cam_notifierId").val().split(",");
			name = $("#maincontentframe", parent.document).contents().find("#cam_notifierName").val().split(",");
		}
	}else{
		id = $("#maincontentframe", parent.document).contents().find("#dif_notifierId").val().split(",");
		name = $("#maincontentframe", parent.document).contents().find("#dif_notifierName").val().split(",");
	}
//	var id = $("#maincontentframe", parent.document).contents().find("#dif_notifierId").val().split(",");
//	var name = $("#maincontentframe", parent.document).contents().find("#dif_notifierName").val().split(",");
//	if(id.length==0){
//		id = $("#maincontentframe", parent.document).contents().find("#cam_notifierId").val().split(",");
//		name = $("#maincontentframe", parent.document).contents().find("#cam_notifierName").val().split(",");
//	}
//	
	
	$("#userid").val(id);
	$("#username").val(name);
	$("#usersname").val(name);
	$("#addDicTypeTable").on('init.dt', function(e, settings) {
		// 清除操作按钮
		//	$("#addUserTable_wrapper .dt-buttons").empty();
		$('#addDicTypeTable_wrapper').css({
			"padding": "0 16px"
		});
		DicTypeTable.ajax.reload();
		returnChecked(id, name);
		DicTypeTable.on('draw', function() {
			returnChecked(id, name);
		});
	});
});
//回选放方法
function returnChecked(id, name) {
	var trs = $("#addDicTypeTable tbody tr");
	id.forEach(function(vv, ii) {
		trs.each(function(i, v) {
			var userid = $(v).children("td").eq(1).text();
			if(vv == userid) {
				$(v).find('.icheck').iCheck('check');
			}
		})
	});
	$("#addDicTypeTable tbody .icheck").on('ifChanged', function(e) {
		if($(this).is(':checked')) {
			id.push($(this).parents("tr").children("td").eq(1).text());
			name.push($(this).parents("tr").children("td").eq(2).text());
			for(var i =0;i<id.length;i++){
				if(id[i]==""){
					id.remove(id[i]);
					break;
				}
			}
			for(var i =0;i<name.length;i++){
				if(name[i]==""){
					name.remove(name[i]);
					break;
				}
			}
			$("#userid").val(id);
			$("#username").val(name);
			$("#usersname").val(name);
		} else {
			id.remove($(this).parents("tr").children("td").eq(1).text());
			name.remove($(this).parents("tr").children("td").eq(2).text());
			for(var i =0;i<id.length;i++){
				if(id[i]==""){
					id.remove(id[i]);
					break;
				}
			}
			for(var i =0;i<name.length;i++){
				if(name[i]==""){
					name.remove(name[i]);
					break;
				}
			}
			$("#userid").val(id);
			$("#username").val(name);
			$("#usersname").val(name);
		}
	})
}

function searchData(){
	var id=$.trim($("#id").val());
	var name=$.trim($("#name").val());
	var searchItemLayerValue = {};
	var k1 = $("#id").attr("searchname");
	if(id != null && id != "") {
		searchItemLayerValue[k1] = "%" + id + "%";
	} else {
		searchItemLayerValue[k1] = id;
	}
	var k2 = $("#name").attr("searchname");
	if(name != null && name != "") {
		searchItemLayerValue[k2] = "%" + name + "%";
	} else {
		searchItemLayerValue[k2] = name;
	}
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	sessionStorage.setItem("searchContent", query);
	DicTypeTable.settings()[0].ajax.data = param;
	DicTypeTable.ajax.reload();
}