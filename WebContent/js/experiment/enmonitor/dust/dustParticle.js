var dustParticleTab;
$(function() {
	var options = table(
			true,
			"",
			"/experiment/enmonitor/dust/cleanAreaDust/showCleanAreaBacteriaTableJsonList.action",
			[ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "workshopDepartment",
				"title" : "车间部门",
			}, {
				"data" : "region",
				"title" : "区域",
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "createUser-name");
				}
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "testDate",
				"title" : "测试时间",
			}, {
				"data" : "testUser",
				"title" : "测试人",
			}, {
				"data" : "measuringInstrument",
				"title" : "测量设备",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "measuringInstrument");
				}
			}, {
				"data" : "cleanZoneStatus",
				"title" : "洁净区状态",
				"render" : function(data, type, full, meta) {
					if (data == "0") {
						return "动态";
					}
					if (data == "1") {
						return "静态";
					} else {
						return "";
					}
				}
			}, {
				"data" : "maximumAllowableDust",
				"title" : "洁净区尘埃最大允许数等级",
				"render" : function(data, type, full, meta) {
					if (data == "0") {
						return "A";
					}
					if (data == "1") {
						return "B";
					}
					if (data == "2") {
						return "C";
					}
					if (data == "3") {
						return "D";
					} else {
						return "";
					}
				}
			}, {
				"data" : "dustZeropointFive",
				"title" : "最大允许数大于等于0.5um",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "dustZeropointFive");
				}
			}, {
				"data" : "maxFivedust",
				"title" : "最大允许数大于等于5um",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "maxFivedust");
				}
			}, {
				"data" : "note",
				"title" : "备注",
			}, {
				"data" : "stateName",
				"title" : "状态",
			} ], null)
	dustParticleTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(dustParticleTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/enmonitor/dust/cleanAreaDust/showCleanAreaDustEdit.action?sampleStyle='
			+ $("#sampleStyle").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/enmonitor/dust/cleanAreaDust/showCleanAreaDustEdit.action?id='
			+ id + "&sampleStyle=" + $("#sampleStyle").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/enmonitor/dust/cleanAreaDust/showCleanAreaDustEdit.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : "车间部门",
		"type" : "input",
		"searchName" : "workshopDepartment",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : "测试时间(开始)",
		"type" : "dataTime",
		"searchName" : "testDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "测试时间(结束)",
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "testDate##@@##2"
	}, {
		"txt" : "区域",
		"type" : "input",
		"searchName" : "region",
	}, {
		"txt" : "测试人",
		"type" : "input",
		"searchName" : "testUser",
	}, {
		"txt" : "状态",
		"type" : "input",
		"searchName" : "stateName",
	}, {
		"type" : "table",
		"table" : dustParticleTab
	} ];
}
