var dustParticleTab;
$(function() {
	var options = table(
			true,
			"",
			"/experiment/enmonitor/microorganism/showMicroorganismTableJson.action",
			[ {
				"data" : "id",
				"title" : "编号",
			}, {
				"data" : "workshopDepartment",
				"title" : "车间部门",
			}, {
				"data" : "region",
				"title" : "区域",
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "createUser-name");
				}
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			},
			// {
			// "data" : "measuringInstrumentCode",
			// "title" : "测量设备及编号",
			// },
			{
				"data" : "testUser",
				"title" : "测试人",
			}, {
				"data" : "testTime",
				"title" : "测试时间",
			}, {
				"data" : "medium",
				"title" : "培养基名称及规格",
			}, {
				"data" : "mediumBatch",
				"title" : "培养基批号",
			}, {
				"data" : "cultivateAddr",
				"title" : "培养地点或设备名称",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "cultivateAddr");
				}
			}, {
				"data" : "equipmentNo",
				"title" : "设备编号",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "equipmentNo");
				}
			}, {
				"data" : "cultivateStart",
				"title" : "培养开始时间",
			}, {
				"data" : "cultivateEnd",
				"title" : "培养结束时间",
			}, {
				"data" : "observeUser",
				"title" : "观察人",
			}, {
				"data" : "observeDate",
				"title" : "观察日期",
			}, {
				"data" : "confirmUser-name",
				"title" : "复核人",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "confirmUser-name");
				}
			}, {
				"data" : "confirmDate",
				"title" : "复核日期",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "confirmDate");
				}
			}, {
				"data" : "stateName",
				"title" : "状态",
			} ], null)
	dustParticleTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(dustParticleTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/enmonitor/microorganism/showMicroorganismEdit.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/enmonitor/microorganism/showMicroorganismEdit.action?id='
			+ id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/enmonitor/microorganism/showMicroorganismEdit.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : "车间部门",
		"type" : "input",
		"searchName" : "workshopDepartment",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : "测试时间(开始)",
		"type" : "dataTime",
		"searchName" : "testTime##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "测试时间(结束)",
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "testTime##@@##2"
	}, {
		"txt" : "培养时间(开始)",
		"type" : "dataTime",
		"searchName" : "cultivateStart##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "培养时间(结束)",
		"type" : "dataTime",
		"mark" : "s##@@##",
		"searchName" : "cultivateEnd##@@##1"
	}, {
		"txt" : "观察日期(开始)",
		"type" : "dataTime",
		"searchName" : "observeDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "观察日期(结束)",
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "observeDate##@@##2"
	}, {
		"txt" : "区域",
		"type" : "input",
		"searchName" : "region",
	}, {
		"txt" : "测试人",
		"type" : "input",
		"searchName" : "testUser",
	}, {
		"txt" : "培养基名称及规格",
		"type" : "input",
		"searchName" : "medium",
	}, {
		"txt" : "培养基批号",
		"type" : "input",
		"searchName" : "mediumBatch",
	}, {
		"txt" : "观察人",
		"type" : "input",
		"searchName" : "observeUser",
	}, {
		"type" : "table",
		"table" : dustParticleTab
	} ];
}
