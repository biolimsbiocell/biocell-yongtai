var dustParticleTab;
$(function() {
	var options = table(
			true,
			"",
			"/experiment/enmonitor/differentialpressure/showDifferentialpressureTableJson.action",
			[ {
				"data" : "id",
				"title" : "编号",
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "measuringInstrumentCode",
				"title" : "测量设备及编号",
			}, {
				"data" : "testUser",
				"title" : "测试人",
			}, {
				"data" : "testTime",
				"title" : "测试时间",
			}, {
				"data" : "cleanlinessClass",
				"title" : "洁净级别",
				"render" : function(data, type, full, meta) {
					if (data == "0") {
						return "A";
					}
					if (data == "1") {
						return "B";
					}
					if (data == "2") {
						return "C";
					}
					if (data == "3") {
						return "D";
					} else {
						return "";
					}
				}
			}, {
				"data" : "stateName",
				"title" : "状态",
			}
			// {
			// "data" : "note",
			// "title" : "备注",
			// }
			], null)
	dustParticleTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(dustParticleTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/enmonitor/differentialpressure/showDifferentialpressureEdit.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/enmonitor/differentialpressure/showDifferentialpressureEdit.action?id='
			+ id + "&sampleStyle=" + $("#sampleStyle").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/enmonitor/differentialpressure/showDifferentialpressureEdit.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : "创建人",
		"type" : "input",
		"searchName" : "createUser-name",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : "测试时间（开始）",
		"type" : "dataTime",
		"searchName" : "testTime##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "测试时间（结束）",
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "testTime##@@##2"
	}, {
		"txt" : "测量设备及编号",
		"type" : "input",
		"searchName" : "measuringInstrumentCode",
	}, {
		"txt" : "测试人",
		"type" : "input",
		"searchName" : "testUser",
	}, {
		"txt" : "状态",
		"type" : "input",
		"searchName" : "stateName",
	}, {
		"type" : "table",
		"table" : dustParticleTab
	} ];
}
