/* 
 * 
 * 
 */
var differentialpressurechangeLogItem;
$(function() {
	
	if($("#stateName").val()=="完成"){
		var bbtn = `<button id="btn_changeState1" type="button" class="btn btn-info" onclick="changeState()">
		<i class="glyphicon glyphicon-adjust"></i> 状态
		</button>`
		$("#btn_list").parent().append(bbtn)
	}
	
	if($("#stateName").val()=="已下达"||$("#stateName").val()=="审批中"){
		$("#btn_save").hide();
	}
	
	$("#form1 :input").change(function(){ 
	     $("#form1").data("changed",true);   
	  });
	
	var id = $("#id").val();
	if (id == "") {
		$("#id").val("NEW");
	}
	$("#cad_testTime").datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd" // 日期格式，详见
	});
	var type = $("#type").val()
	if (type == "0") {
		var hideRoom = "hiddle";
		var hideEquipment = "";
		var roomName = "房间名称";
		var roomNum = "房间编号";
	} else {
		var hideRoom = "";
		var hideEquipment = "hiddle";
		var roomName = "设备名称";
		var roomNum = "设备编号";
	}

	// $("#cad_checkTheDate").datepicker({
	// format: 'yyyy-mm-dd',
	// autoclose: true,
	// language: 'zh-CN',
	// });

	// // 上传附件
	fileInput('cad', 'cleanAreaD', $("#id").val());
	// 机构用户
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : 'roomName',
		"title" : roomName,
		"createdCell" : function(td) {
			$(td).attr("saveName", "roomName");
		}
	});
	colOpts.push({
		"data" : 'roomNum',
		"title" : roomNum,
		"createdCell" : function(td) {
			$(td).attr("saveName", "roomNum");
		}
	});
	colOpts.push({
		"data" : 'centiare',
		"title" : "房间面积m²",
		"className" : "edit",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "centiare");
		}
	});
	colOpts.push({
		"data" : 'stere',
		"title" : "房间体积m³",
		"className" : "edit",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "stere");
		}
	});
	colOpts.push({
		"data" : 'relativeRoom',
		"title" : "相对房间",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "relativeRoom");
		}
	});
	colOpts.push({
		"data" : 'relativePa',
		"title" : "相对压差Pa",
		"className" : "edit",
		"createdCell" : function(td, data, rowdata) {
			$(td).attr("saveName", "relativePa");
			if (rowdata.relativePa != null && rowdata.relativePa != ""
					&& rowdata.aql != null
					&& rowdata.aql != "") {
				if (Number(rowdata.relativePa) < Number(rowdata.aql)) {
					$(td).parent("tr").css({
						'background-color' : 'red'
					}); // rgb(144, 238, 144)

				} /*else if (Number(rowdata.relativePa) > Number($("#cad_cordon")
						.val())
						&& Number(rowdata.relativePa) < Number($(
								"#cad_deviationCorrectionLine").val())) {
					$(td).parent("tr").css({
						'background-color' : 'yellow'
					});
				}*/
			}
		}
	});
	colOpts.push({
		"data" : 'aql',
		"title" : "合格标准Pa",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "aql");
		}
	});
	colOpts.push({
		"data" : 'result',
		"title" : "结果是否合格",
		"className" : "select",
		"name" : "是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "result");
			$(td).attr("selectOpt", "是|否");
		},
		"render" : function(data, type, full, meta) {
			return data;
		}
	});
	colOpts.push({
		"data" : 'note',
		"title" : "备注",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	});
	if (handlemethod != "view") {
/*		tbarOpts.push({
			text : "选择房间",
			className : "btn btn-sm btn-success choseRoom hiddle" + hideRoom,
			action : function() {
				selRoom($("#documentTable"));
			}
		});*/
		
		if($("#stateName").val()!="已下达"&&$("#stateName").val()!="审批中"){
		
		tbarOpts.push({
			text : "房间模板",
			className : "btn btn-sm btn-success choseRoom hiddle" + hideRoom,
			action : function() {
				if($("#id").val()=="NEW"||$("#id").val()==""){
					top.layer.msg("请保存后选择");
				    return false;
				}
				selRoomTemplate($("#documentTable"));
			}
		});
		tbarOpts.push({
			text : "选择设备",
			className : "btn btn-sm btn-success choseEquipment hiddle"
					+ hideEquipment,
			action : function() {
				selEquipment($("#documentTable"));
			}
		});

		tbarOpts
				.push({
					text : biolims.common.delSelected,
					action : function() {
						removeChecked(
								$("#documentTable"),
								"/experiment/enmonitor/differentialpressure/delRoom.action",
								"删除文档：", id,myTable);
					}
				});
		tbarOpts.push({
			text : biolims.common.uploadCSV,
			action : function() {
				$("#uploadCsv").modal("show");
				uploadCsv();
			}
		});
		tbarOpts.push({
			text: biolims.common.downloadCsvTemplet,
			action: function() {
				downCsv()
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#documentTable"))
			}
		});
		tbarOpts.push({
			text: biolims.common.search,
			action: function() {
				search();
			}
		});
		
		}
	}
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "modify") {
		$("#dif_fileName").prop("readonly", "readonly");
	}
	var sampleInfoOptions = table(
			true,
			id,
			'/experiment/enmonitor/differentialpressure/showDiffPressureItemTableJsonList.action',
			colOpts, tbarOpts)
	myTable = renderDatass($("#documentTable"), sampleInfoOptions);
	myTable.on('draw', function() {
		differentialpressurechangeLogItem = myTable.ajax.json(); 
	});
})

//下载csv
function downCsv(){
//	window.location.href=ctx+"/js/experiment/enmonitor/differentialpresure/cleanAreaDiffpresureItem.csv";
	var mainTableId=$("#id").val();
	if(mainTableId!=""&&mainTableId!='NEW'){
		window.location.href=ctx+"/experiment/enmonitor/differentialpressure/downloadCsvFile.action?id="+mainTableId;
	}else{
		top.layer.msg("请保存后再下载模板！");
	}
}
//弹框模糊查询参数
function searchOptions() {
	var type = $("#type").val()
	if (type == "0") {
		var hideRoom = "hiddle";
		var hideEquipment = "";
		var roomName = "房间名称";
		var roomNum = "房间编号"
	} else {
		var hideRoom = "";
		var hideEquipment = "hiddle";
		var roomName = "设备名称";
		var roomNum = "设备编号";
	}
	return [
		{	
			"txt": "编号",
			"type": "select",
			"searchName": "id",
			"options":$("#id").val(),
			"changeOpt":$("#id").val()
		},
		{	
			"txt": roomName,
			"type": "input",
			"searchName": "roomName"
		},{
			"txt": roomNum,
			"type": "input",
			"searchName": "roomNum"
		},
/*		{
			"txt": "监测点",
			"type": "input",
			"searchName": "monitoringPoint",
		},*/
		{
			"txt": "相对房间",
			"type": "input",
			"searchName": "relativeRoom"
		},
		{
			"txt": "相对压差Pa",
			"type": "input",
			"searchName": "relativePa"
		},
		{
			"txt": "合格标准Pa",
			"type": "input",
			"searchName": "aql",
		},
		{
			"txt": "结果是否合格",
			"type": "select",
			"searchName": "result",
			"options":"请选择"+"|"+"是"+"|"+"否",
			"changeOpt":"''|是|否"
		},
		{
			"txt": "备注",
			"type": "input",
			"searchName": "note",
		},
		{
			"type":"table",
			"table":myTable
		}];
}

// 切换类型
function checkType() {
	$(".choseRoom").toggle();
	$(".choseEquipment").toggle();
	var type = $("#type").val()
	if (type == "0") {
		$(".dataTables_scrollHead").find("th[savename='roomName']")
				.text("房间名称")
		$(".dataTables_scrollHead").find("th[savename='roomNum']").text("房间编号")
	}
	if (type == "1") {
		$(".dataTables_scrollHead").find("th[savename='roomName']")
				.text("设备名称")
		$(".dataTables_scrollHead").find("th[savename='roomNum']").text("设备编号")
	}
}
function selRoom(ele) {
	top.layer
			.open({
				title : "选择房间",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/experiment/enmonitor/differentialpressure/selectRoomTable.action",
						"" ],
				yes : function(index, layero) {
					$('.layui-layer-iframe', parent.document)
							.find("iframe")
							.contents()
							.find("#addRoomTable .selected")
							.each(
									function(i, v) {
										$(".dataTables_scrollHead th").off();
										// 禁用固定列
										$("#fixdeLeft2").hide();
										// 禁用列显示隐藏
										$(".colvis").hide();
										// 清除没有内容选项
										ele.find(".dataTables_empty").parent(
												"tr").remove();
										// 添加明细
										var ths = ele.find("th");
										var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
										tr.height(32);
										for (var i = 1; i < ths.length; i++) {
											var edit = $(ths[i]).attr("key");
											var saveName = $(ths[i]).attr(
													"saveName");
											if (edit == "select") {
												var selectOpt = $(ths[i]).attr(
														"selectopt");
												tr.append("<td class=" + edit
														+ " saveName="
														+ saveName
														+ " selectopt="
														+ selectOpt + "></td>");
											} else if (saveName == 'roomName') {
												tr.append("<td class="
														+ edit
														+ " saveName="
														+ saveName
														+ ">"
														+ $(v).children("td")
																.eq(2).text()
														+ "</td>");
											} else if (saveName == 'roomNum') {
												tr.append("<td class="
														+ edit
														+ " saveName="
														+ saveName
														+ ">"
														+ $(v).children("td")
																.eq(1).text()
														+ "</td>");
											} else if (saveName == 'centiare') {
												tr.append("<td class="
														+ edit
														+ " saveName="
														+ saveName
														+ ">"
														+ $(v).children("td")
																.eq(7).text()
														+ "</td>");
											} else if (saveName == 'stere') {
												tr.append("<td class="
														+ edit
														+ " saveName="
														+ saveName
														+ ">"
														+ $(v).children("td")
																.eq(8).text()
														+ "</td>");
											}else {
												tr.append("<td class=" + edit
														+ " saveName="
														+ saveName + "></td>");
											}

										}
										tr.addClass("editagain");
										ele.find("tbody").prepend(tr);
										checkall(ele);

									});
					top.layer.close(index);
				},
			});
}
// 选择设备
function selEquipment(ele) {
	top.layer
			.open({
				title : "选择设备",
				type : 2,
				offset : [ '10%', '10%' ],
				area : [ document.body.clientWidth - 300,
						document.body.clientHeight - 100 ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/equipment/main/showInstrumentDialogList.action",
						"" ],
				yes : function(index, layero) {
					$('.layui-layer-iframe', parent.document)
							.find("iframe")
							.contents()
							.find("#addInstrument .selected")
							.each(
									function(i, v) {
										$(".dataTables_scrollHead th").off();
										// 禁用固定列
										$("#fixdeLeft2").hide();
										// 禁用列显示隐藏
										$(".colvis").hide();
										// 清除没有内容选项
										ele.find(".dataTables_empty").parent(
												"tr").remove();
										// 添加明细
										var ths = ele.find("th");
										var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
										tr.height(32);
										for (var i = 1; i < ths.length; i++) {
											var edit = $(ths[i]).attr("key");
											var saveName = $(ths[i]).attr(
													"saveName");
											if (edit == "select") {
												var selectOpt = $(ths[i]).attr(
														"selectopt");
												tr.append("<td class=" + edit
														+ " saveName="
														+ saveName
														+ " selectopt="
														+ selectOpt + "></td>");
											} else if (saveName == 'roomName') {
												tr.append("<td class="
														+ edit
														+ " saveName="
														+ saveName
														+ ">"
														+ $(v).children("td")
																.eq(2).text()
														+ "</td>");
											} else if (saveName == 'roomNum') {
												tr.append("<td class="
														+ edit
														+ " saveName="
														+ saveName
														+ ">"
														+ $(v).children("td")
																.eq(1).text()
														+ "</td>");
											} else {
												tr.append("<td class=" + edit
														+ " saveName="
														+ saveName + "></td>");
											}

										}
										tr.addClass("editagain");
										ele.find("tbody").prepend(tr);
										checkall(ele);

									});
					top.layer.close(index);
				},
			});
}
// 上传CSV文件
function uploadCsv() {
	var csvFileInput = fileInputCsv("");
	csvFileInput
			.on(
					"fileuploaded",
					function(event, data, previewId, index) {
						$
								.ajax({
									type : "post",
									url : ctx
											+ "/experiment/enmonitor/differentialpressure/uploadCsvFile.action",
									data : {
										id : $("#id").val(),
										fileId : data.response.fileId
									},
									success : function(data) {
										var data = JSON.parse(data);
										if (data.success) {
											top.layer.msg("上传成功");
											$(".close").click();
											myTable.ajax.reload();
										} else {
											top.layer
													.msg(biolims.common.uploadFailed);
										}
									}
								});
					});
}
// 保存
function save() {
	
	
	//子表
	var datas = saveItemjson($("#documentTable"));
	var changeLogItem = "压差明细：";
	changeLogItem = getChangeLog(datas, $("#documentTable"), changeLogItem);
	var changeLogItemLast = "";
	if (changeLogItem != "压差明细：") {
		changeLogItemLast = changeLogItem
	}
	
	
	
	var changeLog = "";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	// 必填验证
	var requiredField = requiredFilter();
	if (!requiredField) {
		return false;
	}
	if($("#cad_confirmUser_name").val()==""||$("#cad_confirmUser_name").val()==null){
		top.layer.msg("请选择复核人！");
		
		return false;
	}
	
	document.getElementById("changeLog").value = changeLog;
	document.getElementById("changeLogItem").value = changeLogItemLast;
	var index = top.layer.load(4, {
		shade : 0.3
	});
	$("#form1").attr("action",
			ctx + "/experiment/enmonitor/differentialpressure/save.action?bpmTaskId="+$("#bpmTaskId").val());
	$("#documentInfoItemJson").val(saveItemjson($("#documentTable")));
	$("#form1").submit();
	top.layer.close(index);
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			if (k == "attach-fileName") {
				json["attach-id"] = $(tds[j]).attr("attach-id");
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

// 新建
function add() {
	window.location = window.ctx
			+ '/experiment/enmonitor/differentialpressure/showDifferentialpressureEdit.action';
}
// 列表
function list() {
	window.location = window.ctx
			+ '/experiment/enmonitor/differentialpressure/showDifferentialpressureTable.action';
}

function getChangeLog(data, ele, changeLog) {
	debugger
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		differentialpressurechangeLogItem.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function dayin() {
	var da =document.getElementById('gdate').value;
	if(da==""){
		top.layer.msg("请先保存再打印报表！");
		return false;
	}
	$.ajax({
		type : "post",
		data : {
			id : $("#sampleReveice_id").text(),
			confirmDate : da,
			modelId:"yc"
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirt.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				var url = '__report='+data.reportN+'&id=' + $("#id").val();
				commonPrint(url);
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
//	var url = '__report=differentialpressureNew.rptdesign&id=' + $("#id").val();
//	commonPrint(url);
}
function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '_blank', '');
}
function fileUp() {
	if ($("#id").val() == "NEW") {
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
function fileView() {
	top.layer
			.open({
				title : biolims.common.attachment,
				type : 2,
				skin : 'layui-layer-lan',
				area : [ "650px", "400px" ],
				content : window.ctx
						+ "/operfile/initFileList.action?flag=cad&modelType=cleanAreaD&id="
						+ $("#id").val(),
				cancel : function(index, layer) {
					top.layer.close(index)
				}
			})
}
function showConfirmUser() {
	
	
	 $("#form1").data("changed",true);
	
	top.layer.open({
		title : "请选择复核人",
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=QA002",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#cad_confirmUser_id").val(id);
			$("#cad_confirmUser_name").val(name);
		},
	})
}
function showApprovalUser1() {
	
	
	 $("#form1").data("changed",true);
	
	top.layer.open({
		title : "请选择批准人",
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=QA002",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#cad_approver_id").val(id);
			$("#cad_approver_name").val(name);
		},
	})
}
// 提交审批
function tjsp() {
	
	if($("#id").val()=="NEW"){
		top.layer.msg("请保存后提交");
	    return false;
	}
	
	if($("#form1").data("changed")){ 
	    top.layer.msg("请保存后提交");
	    return false;
	   }
	
	 if($("#cad_confirmUser_name").val()==$("#cad_createUser_name").val()){
			top.layer.msg(("创建人与复核人相同请重新选择"));
			functionLock = true;
			return false;
		}
	
	top.layer.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.submit,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=CleanAreaDiffPressure",
									yes : function(index, layero) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : $("#id").val(),
											title : "压差记录",// $("#changePlan_name").val(),
											formName : 'CleanAreaDiffPressure'
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
														top.layer.closeAll();
														location.href = window.ctx
																+ "/lims/pages/dashboard/dashboard.jsp";
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layero) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});
}
// 审批
function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#id").val();
	top.layer.open({
		title : biolims.common.approvalProcess,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#opinionVal").val();
			var opinion = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#opinion").val();
			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinion;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}

	});
}

function ck() {
	top.layer
			.open({
				title : biolims.common.checkFlowChart,
				type : 2,
				anim : 2,
				area : [ '800px', '500px' ],
				btn : biolims.common.selected,
				content : window.ctx
						+ "/workflow/processinstance/toTraceProcessInstanceView.action?formId="
						+ $("#id").val(),
				yes : function(index, layero) {
					top.layer.close(index)
				},
				cancel : function(index, layero) {
					top.layer.close(index)
				}
			});
}

function findUsers() {
	
	 $("#form1").data("changed",true);
	
	top.layer.open({
				title : "选择通知人",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : window.ctx
						+ "/experiment/enmonitor/dust/cleanAreaDust/selUsersTableList.action",
				yes : function(index, layer) {
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#userid").val();
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find("#username").val();
					$("#cad_notifierName").val(name);
					$("#cad_notifierId").val(id);
					top.layer.close(index)	
				},
				cancel : function(index, layer) {
					top.layer.close(index)
				}
			});
}

// 选择区域
//function choseArea() {
//	
//	 $("#form1").data("changed",true);
//	
//	top.layer.open({
//		title : "选择区域",
//		type : 2,
//		area : [ "650px", "400px" ],
//		btn : biolims.common.selected,
//		content : [
//				window.ctx + "/dic/type/dicTypeSelectTable.action?flag=area",
//				'' ],
//		yes : function(index, layer) {
//			var name = $('.layui-layer-iframe', parent.document).find("iframe")
//					.contents().find("#addDicTypeTable .chosed").children("td")
//					.eq(1).text();
//			var id = $('.layui-layer-iframe', parent.document).find("iframe")
//					.contents().find("#addDicTypeTable .chosed").children("td")
//					.eq(0).text();
//			var code = $('.layui-layer-iframe', parent.document).find("iframe")
//					.contents().find("#addDicTypeTable .chosed").children("td")
//					.eq(3).text();
//			var sysCode = $('.layui-layer-iframe', parent.document).find("iframe")
//					.contents().find("#addDicTypeTable .chosed").children("td")
//					.eq(2).text();
//			top.layer.close(index);
//			$("#region").val(name);
//			$("#regionId").val(id);
//			$("#cad_cordon").val(code);
//			$("#cad_deviationCorrectionLine").val(sysCode);
//		},
//	})
//}


//选择区域
function choseArea() {
	$("#form1").data("changed",true);  
	var modelN="2";
	top.layer.open({
		title : "选择区域",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/experiment/regionalManagement/regionalManagement/showRegionalTableJson.action",''],
		yes: function(index, layer) {
			var qId = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addregionalManagementItem .chosed").children("td").eq(0).text();
			var qName = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addregionalManagementItem .chosed").children("td").eq(1).text();
			top.layer.close(index);
			$("#region").val(qName);
			$("#regionId").val(qId);
			$.ajax({
				type: "post",
				url: window.ctx + '/experiment/regionalManagement/regionalManagement/findByModel.action',
				async: false,
				data: {
					modelN: modelN,
					qId: qId,
				},
				success: function(data) {
					var obj = JSON.parse(data);
					$("#cam_cordon").val(obj.cordon);
					$("#cam_deviationCorrectionLine").val(obj.deviationCorrectionLine);
//					$("#dif_cordon2").val(obj.cordonf);
//					$("#dif_deviationCorrectionLine2").val(obj.deviationCorrectionLinef);
				}
			});
		},
	})
}
function changeState() {
	var paraStr = "formId=" + $("#id").val() +
		"&tableId=CleanAreaDiffPressure";
	top.top.layer.confirm("状态完成之前请先保存", {
		icon: 3,
		title: biolims.common.prompt,
		btn: biolims.common.selected
	}, function(index) {
		top.layer.open({
			title: biolims.common.approvalProcess,
			type: 2,
			anim: 2,
			area: ['400px', '400px'],
			btn: biolims.common.selected,
			content: window.ctx +
				"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
				"&flag=changeState'",
			yes: function(index, layer) {
				top.layer.confirm(biolims.common.approve, {
					icon: 3,
					title: biolims.common.prompt,
					btn: biolims.common.selected
				}, function(index) {
					var stateName=$('.layui-layer-iframe', parent.document).find("iframe").contents().find('div .checked').find("input").val();
					ajax("post", "/applicationTypeAction/exeFun.action", {
							applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
							formId: $("#id").val()
						}, function(response) {
							var respText = response.message;
							if(respText == '') {
								window.location.reload();
							} else {
								top.layer.msg(respText);
							}
						}, null)
					top.layer.closeAll();
				})
	
			},
			cancel: function(index, layer) {
				top.layer.closeAll();
	
			}
	
		});
		top.layer.close(index);
	});
}
function selRoomTemplate(){
	var len=$("#documentTable tbody tr").length;
	if(len<=1){
		top.layer
		.open({
			title : "选择房间模板",
			type : 2,
			offset : [ '10%', '10%' ],
			area : [ document.body.clientWidth - 300,
					document.body.clientHeight - 100 ],
			btn : biolims.common.selected,
			content : [
					window.ctx
					+ "/experiment/enmonitor/template/roomTemplate/selRoomTemplateList.action?region="+$("#regionId").val(),
					"" ],
			yes : function(index, layero) {
				
				var roomTemplate_id=$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#main .chosed").children("td").eq(0).text();
						
				$.ajax({
					url:window.ctx+"/experiment/enmonitor/differentialpressure/useTemplateAddItem.action",
					type:"post",
					data:{
						id:roomTemplate_id,
						diffPressureId:$("#id").val()
					},
					success:function(){
						myTable.ajax.reload();
					}
				});
				top.layer.close(index);
			},
		});
	}else{
		top.layer.msg("请不要重复选择！");
	}
}
function renderDatass(ele, options) {
	return ele.DataTable({
		serverSide: true,
		processing: true, //载入数据的时候是否显示“载入中”
		order: [
			[1, 'desc']
		],
		dom: "<'row tablebtns'<'col-sm-12'B>>" +
			"<'row'<'col-xs-2'l>>" +
			"<'row'<'col-sm-12 table-responsive'tr>>" +
			"<'row'<'col-sm-5'i><'col-sm-7'p>>",
		buttons: options.buttons,
		pageLength: 200, //首次加载的数据条数
		searching: false,
		responsive: false,
		autoWidth: false,
		scrollX: true,
		//autoFill: true,
		//fixedColumns: true,
		colReorder: {
			fixedColumnsLeft: 1
		},
		ajax: {
			type: "POST",
			url: options.ajax.url,
			data: options.ajax.data
		},
		columns: options.columns,
		language: {
			lengthMenu: '<select class="form-control input-xsmall">' + '<option value="10">10</option>' + '<option value="15">15</option>' + '<option value="20">20</option>' + '<option value="30">30</option>' + '<option value="40">40</option>' + '<option value="200">200</option>' + '</select>' + biolims.common.data,
			processing: biolims.common.loading, //处理页面数据的时候的显示
			paginate: { //分页的样式文本内容。
				previous: biolims.common.prevPage,
				next: biolims.common.nextPage,
				first: biolims.common.firstPage,
				last: biolims.common.lastPage
			},
			zeroRecords: biolims.common.notDataList, //table tbody内容为空时，tbody的内容。
			//下面三者构成了总体的左下角的内容。
			info: biolims.order.page, //左下角的信息显示，大写的词为关键字，筛选之后得到 _TOTAL_ 条。
			infoEmpty: biolims.common.noRecord, //筛选为空时左下角的显示。
		},
		headerCallback: function() {
			var columns = options.columns;
			var header = new $.fn.dataTable.Api(ele).columns().header();
			columns.forEach(function(val, i) {
				var kkey = val.className;
				var width = val.width;
				if(kkey) {
					if(kkey == "select") {
						$(header[i]).attr("selectopt", val.name);
					}
					//设置当前列是否可编辑
					$(header[i]).attr("key", kkey);
					$(header[i]).css("background-color", "#02C39A");
				}
				if(width) {
					$(header[i]).css({"max-width":width,"min-width":width});
				}
				//为添加明细设置保存的键
				$(header[i]).attr("saveName", val.data);
				
			});
		},
		drawCallback: function() {
			checkall(ele);
			$(".newhour").bind('click',function(event){newTimePacker($(this),event)});
		},
		initComplete: function() {
			
			checkall(ele);
			ele.on('column-sizing.dt', function() {
				checkall(ele);
			});
			autoFill(ele);
			$(".dataTables_scrollBody").css("padding-bottom", "10px");
			new $.fn.dataTable.Api(ele).draw();
			
			//调整按钮样式
			$(".dt-buttons").css("margin-bottom", "10px");
			$(".dt-buttons .btn").css({
				"margin-right": "3px",
				"margin-top": "10px",
				"border-radius": 0,
				"color": "#fff"
			})
		}
	});

}