﻿/* 
 * 文件名称 :productNew.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/02/06
 * 文件描述: 新建产品数据操作函数
 * 
 */
$(function() {
	$(".Wdate1").datepicker({todayHighlight: true,
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd ",//日期格式，详见 
	
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonly();
	}
	if (handlemethod == "modify") {
		$("#qualityProduct_id").prop("readonly", "readonly");
	}
	if (handlemethod == "add") {
		$("#qualityProduct_id").prop("readonly", "readonly");
	}
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'qualityProduct', $("#qualityProduct_id").val());
	
	fieldCustomFun();
	checktable();
})

function settextreadonly() {
	jQuery(":text, textarea").each(function() {
		var _vId = jQuery(this).attr('id');
		jQuery(this).css("background-color", "#B4BAB5").attr("readonly", "readOnly");
		if(_vId == 'actiondropdown_textbox')
			settextread(_vId);
	});
}

function settextread(name) {
	document.getElementById(name).className = "input_parts text input default  readonlyfalse false";
	document.getElementById(name).readOnly = false;
}

//自定义模块
function fieldCustomFun() {

	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();

	//查找自定义列表的订单模块的此检测项目的相关内容
	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByModuleValue.action",
		data: {
			moduleValue: "QualityProduct"
		},
		async: false,
		success: function(data) {
			var objValue = JSON.parse(data);
			if(objValue.success) {
				$.each(objValue.data, function(i, n) {
					var inputs = '';
					var disabled = n.readOnly ? ' ' : "disabled";
					var defaultValue = n.defaultValue ? n.defaultValue : ' ';
					if(n.fieldType == "checkbox") {
						var checkboxs = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
					} else if(n.fieldType == "radio") {
						var options = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
					} else if(n.fieldType == "date") {
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
					} else {
						if(n.isRequired != "false") {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '<img class="requiredimage" src="/images/required.gif" /></span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						} else {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}
					}

					$("#fieldItemDiv").append(inputs);
				});

			} else {
				top.layer.msg(biolims.customList.updateFailed);
			}
		}
	});

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
						inp.setAttribute("changelog", inp.value);
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
							inp.setAttribute("changelog", inp.value);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({todayHighlight: true,
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});
}

// 保存
function save() {
	//自定义字段
	//拼自定义字段儿（实验记录）
	var inputs = $("#fieldItemDiv input");
	var options = $("#fieldItemDiv option");
	var contentData = {};
	var checkboxArr = [];
	//必填验证
	var requiredField=requiredFilter();
		if($("#qualityProduct_name").val()==""){
			top.layer.msg("请填写描述!");
			return false;
		}
		if($("#qualityProduct_batch").val()==""){
			top.layer.msg("请填写批次!");
			return false;
		}
	$("#fieldItemDiv .checkboxs").each(function(i, v) {
		$(v).find("input").each(function(ii, inp) {
			var k = inp.name;
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		});
	});
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type != "checkbox") {
			contentData[k] = inp.value;
			
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
	document.getElementById("fieldContent").value = JSON.stringify(contentData);

	var changeLog = "质控品：";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + biolims.common.from+'"' + val + '"'+biolims.common.to+'"' + valnew + '";';		}
	});
	var changeLogs="";
	if(changeLog !="质控品："){
		changeLogs=changeLog;
		$("#changeLog").val(changeLogs);
	}
	/*var jsonStr = JSON.stringify($("#form1").serializeObject());
	var info = {
		id : $("#qualityProduct_id").val(),
		obj : 'QualityProduct',
		main : jsonStr,
		logInfo : changeLog
	}
	$.ajax({
		type : 'post',
		url : ctx + "/system/quality/qualityProduct/save.action",
		data : info,
		success : function(data) {
			var da = JSON.parse(data);
			if (da.success) {
//				tableRefresh();
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx
						+ '/system/quality/qualityProduct/editQualityProduct.action?id=' + da.id;
			} 
		}
	});*/
	var handlemethod = $("#handlemethod").val();
	var jsonStr = JSON.stringify($("#form1").serializeObject());
	/*var info = {
		id : $("#qualityProduct_id").val(),
		obj : 'QualityProduct',
		main : jsonStr,
		logInfo : changeLog
	}*/
	if(handlemethod == "modify") {
//		top.layer.load(4, {shade:0.3}); 
			$("#form1").attr("action", "/system/quality/qualityProduct/save.action);
			$("#form1").submit();
//			top.layer.closeAll();
	} else {
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id : $("#qualityProduct_id").val(),
				obj : 'QualityProduct'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
//					top.layer.load(4, {shade:0.3}); 
					$("#form1").attr("action", "/system/quality/qualityProduct/save.action);
					$("#form1").submit();
//					top.layer.closeAll();
				}
			}
		});
	}
	
}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if(o[this.name]) {
			if(!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function list() {
	window.location = window.ctx + '/system/quality/qualityProduct/showQualityProductList.action';
}

// 上传附件模态框出现
function fileUp() {
	if(!$("#qualityProduct_id").val()){
		top.layer.msg(biolims.common.pleaseHold)
		return false;
	}
	$("#uploadFile").modal("show");
}
// 查看附件
function fileView() {
	top.layer.open({
		title : "附件",
		type : 2,
		skin : 'layui-top.layer-lan',
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		content : window.ctx
				+ "/operfile/initFileList.action?flag=1&modelType=qualityProduct&id="
				+ $("#qualityProduct_id").val(),
		cancel : function(index, layer) {
			top.layer.close(index);
		}
	});
}
//实验类型
function sylxCheck() {
	top.layer.open({
		title: biolims.master.selectTestType,
		type: 2,
		area: top.screeProportion,
		btn: biolims.common.selected,
		content: [window.ctx + "/system/nextFlow/nextFlow/selectNextFlow.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(1).text();
			var sys_code = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(2).text();
			top.layer.close(index);
			$("#qualityProduct_experType").val(id);
			$("#qualityProduct_experType_name").val(name);
		},
	})
}

function detectionProject() {
	//var inputt=$(that).parents(".input-group").children("input");
	top.layer.open({
		title: biolims.crm.selectProduct,
		type: 2,
		area: top.screeProportion,
		btn: biolims.common.selected,
		content: window.ctx +
			"/com/biolims/system/product/showProductSelTree.action",
		yes: function(index, layer) {
			var name = [];
				id = [];
				department =[];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
				department.push($(v).children("td").eq(3).text())
			});
			//			rows.addClass("editagain");
//			inputt.val(name.join(","));
//			inputt.parents(".col-sm-12").prev().find("input").val(id.join(","));
//			top.layer.close(index)
			debugger;
			$("#productname").val(name);
			$("#productid").val(id);
			top.layer.close(index)
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

function checktable() {
	var cols = [];
	cols.push({
		"data": "id",
		"title":"编号",
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
/*	cols.push({
		"data": "qualityProduct-name",
		"title":"质控品",
		"createdCell": function(td) {
			$(td).attr("saveName", "qualityProduct-name");
		}
	})*/
	cols.push({
		"data": "batch",
		"title": "批号",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "batch");
		}
	})
	
		cols.push({
		"data": "num",
		"title": "数量",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "num");
		}
	})
	/*cols.push({
		"data": "createUser-name",
		"title":"创建人",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-name");
		}
	})
	cols.push({
		"data": "createDate",
		"title":"创建时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
		}
	})*/
	/*cols.push({
		"data": "product-name",
		"title": biolims.common.testProject + '<img src="/images/required.gif"/>',
		"width": "250px",
		"className": "method",
		"method": "detectionProjectRight",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "product-name");
			$(td).attr("product-id", rowData['product-id']);
		}
	})
	*/

	cols.push({
		"data": "state",
		"title": '状态' + '<img src="/images/required.gif"/>',
		"className": "select",
		"name": "有效"+ "|" + "失效",
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
			$(td).attr("selectOpt","有效"+ "|" + "失效");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "失效";
			}
			if(data == "1") {
				return "有效";
			} else {
				return '';
			}
		}
	})
	

	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem123($("#storageReagentBuySerialTable"));
		}

	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem();
		}
	});
	var id=$("#qualityProduct_id").val();
	var bloodSplitReceiveOpts = table(true, null, "/system/quality/qualityDetail/qualityDetailListJson.action?id="+id, cols, tbarOpts)
	//渲染样本的table
	
	bloodSplitReceiveTab=renderData($("#storageReagentBuySerialTable"),bloodSplitReceiveOpts);
    //dwb 日志功能 2018-05-11 12:31:59
	/*bloodSplitReceiveTab.on('draw', function() {
		oldChangeLog = bloodSplitReceiveTab.ajax.json();
	});*/
	//批量结果
	//btnChangeDropdown($('#storageReagentBuySerialTable'), $(".resultsBatchBtn"), [biolims.common.qualified, biolims.common.disqualified], "result");
	//恢复搜索状态
	/*$('#main').on('init.dt', function() {
		recoverSearchContent(bloodSplitReceiveTab);
	});
	$('#main').on('init.dt', function() {
		checkedUpdata($("#main"));
	});*/
	bloodSplitReceiveTab.on('draw', function() {
		oldChangeLog = bloodSplitReceiveTab.ajax.json();
		checkedUpdata($("#storageReagentBuySerialTable"));
	});
}
function addItem123(ss) {
	var id=$("#qualityProduct_id").val();
	if(id==null || id=="NEW"){
		alert("请先保存质控品!!!")
	}else{
		addItem1(ss);
	}
	
}
function addItem1(ele) {
	var id=$("#qualityProduct_id").val();
	var name=$("#qualityProduct_name").val();
	$(".dataTables_scrollHead th").off();
	//禁用固定列
	$("#fixdeLeft2").hide();
	//禁用列显示隐藏
	$(".colvis").hide();
	//清除没有内容选项
	ele.find(".dataTables_empty").parent("tr").remove();
	//添加明细
	var ths = ele.find("th");
	var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
	tr.height(32);
	for(var i = 1; i < ths.length; i++) {
		var edit = $(ths[i]).attr("key");
		var saveName = $(ths[i]).attr("saveName");
		if(edit == "select") {
			var selectOpt = $(ths[i]).attr("selectopt");
			tr.append("<td class=" + edit + " saveName=" + saveName + " selectopt=" + selectOpt + "></td>");
		} else {
			if(saveName=="qualityProduct-name"){
				tr.append("<td class=" + edit + " saveName=" + saveName + ">"+name+"</td>");
			}else{
				tr.append("<td class=" + edit + " saveName=" + saveName + "></td>");
			}
		}

	}
	ele.find("tbody").prepend(tr);
	checkall(ele);
}
//保存
function saveItem() {
	var id=$("#qualityProduct_id").val();
	var ele=$("#storageReagentBuySerialTable");
	var changeLog = biolims.common.containerMent+"：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog != biolims.common.containerMent+"："){
		changeLogs=changeLog;
	}
	console.log(data)
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/system/quality/qualityDetail/changeLocation.action',
		data: {
            id: id,
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed);
			};
		}
	});
}
//获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断是否有效并转换为数字
			if(k == "state") {
				var state = $(tds[j]).text();
				if(state == biolims.master.invalid) {
					json[k] = "0";
				} else if(state == biolims.master.valid) {
					json[k] = "1";
				}
				continue;
			}
			json[k] = $(tds[j]).text();
		}
	
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID为"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title +'"'+ biolims.common.from+'"' + vv[k] + '"'+biolims.common.to+'"' + v[k] + '";';					}
				}
				return false;
			}
		});
	});
	return changeLog;
}