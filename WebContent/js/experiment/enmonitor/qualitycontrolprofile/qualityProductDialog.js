/*var qualityProductDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/system/quality/qualityProduct/showQualityProductListJson.action";
	var opts={};
	opts.title=biolims.common.qualityProduct;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setQualityProductFun(rec);
	};
	qualityProductDialogGrid=gridTable("show_dialog_qualityProduct_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(qualityProductDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
*/
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "name",
		"title": biolims.common.patientName,
	});
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	
	colOpts.push({
		"data": "state",
		"title": biolims.common.state,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.master.valid;
			}
			else if(data == "0") {
				return biolims.master.invalid;
			}else {
				return "";
			}
		}
	});
  
	var tbarOpts = [];
	var addDicTypeOptions = table(true, null,
		'/system/quality/qualityProduct/qualityProductSelectJson.action?gssys='+$("#gssys").val(),
		colOpts, tbarOpts);
	var DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions,200);
	$("#addDicTypeTable").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addDicTypeTable_wrapper .dt-buttons").empty();
			$('#addDicTypeTable_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addDicTypeTable tbody tr");
			DicTypeTable.ajax.reload();
		});
})