var messReport;
// var markLineOpt;
// var showQualityProductList;
// var allData;
// var xValue;
// var sdValue;
// var cvValue;
// var xs1Value;
// var fuxs1Value;
// var xs2Value;
// var fuxs2Value;
// var xs3Value;
// var fuxs3Value;
// var xs6Value;
// var fuxs6Value;
// var maps=new Map();
// var X3=6.5;
$(function() {
	$(".Wdate1").datepicker({
		todayHighlight : true,
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd ",// 日期格式，详见

	});
	var colData = [ {
		"data" : "id",
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	}, {
		"data" : "testDate",
		"title" : "质控日期",
		// "className": "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "testDate");
		}
	}, {
		"data" : "roomName",
		"title" : "房间/设备",
		// "className": "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "roomName");
		}
	},
	// {
	// "data": "rooNum",
	// "title": "房间/设备编号",
	// // "className": "date",
	// "createdCell": function(td) {
	// $(td).attr("saveName", "rooNum");
	// }
	// },
	{
		"data" : "averageNum",
		"title" : "平均值",
		// "className": "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "averageNum");
		}
	}, {
		"data" : "cordon",
		"title" : "警戒线",
		// "className": "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "cordon");

		}
	}, {
		"data" : "deviation",
		"title" : "纠偏线",
		// "className": "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "deviation");
		}
	}, {
		"data" : "realNum",
		"title" : "数值",
		// "className": "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "realNum");
		}
	} ];

	var tbarOpts = [];

	var options = table(
			true,
			"",
			"/experiment/enmonitor/qualitycontrolprofile/showQualityProductListDataJson.action",
			colData, tbarOpts);
	sampleOrderTable = renderRememberData($("#mains"), options);
	// 恢复之前查询的状态
	$('#mains').on('init.dt', function() {
		recoverSearchContent(sampleOrderTable);
	});
	sampleOrderTable.on('draw', function() {
		oldChangeLog = sampleOrderTable.ajax.json();
		checkedUpdata($("#storageReagentBuySerialTable"));
	});

	messReport = echarts.init(document.getElementById("messaryReport"));
	var tuli = "";
	var title = "";
	if ($("#qstmk").val() == "1") {
		tuli = "0.5μm粒子";
		title = "洁净区尘埃粒子0.5μm";
	} else if ($("#qstmk").val() == "2") {
		tuli = "5.0μm";
		title = "洁净区尘埃粒子5.0μm";
	} else if ($("#qstmk").val() == "3") {
		tuli = "菌落数";
		title = "洁净区浮游菌菌落数";
	} else if ($("#qstmk").val() == "4") {
		tuli = "菌落数";
		title = "洁净区沉降菌菌落数";
	} else if ($("#qstmk").val() == "5") {
		tuli = "菌落数";
		title = "表面微生物菌落数";
	}
	// else if($("#qstmk").val()=="6"){
	// tuli = "换气次数";
	// title = "洁净区风量换气次数";
	// }
	// else if($("#qstmk").val()=="7"){
	// tuli = "相对压差";
	// title = "压差相对压差";
	// }
	else {
		tuli = "数据";
		title = "趋势图";
	}
	var room = "";
	if ($("#bianma").val() != "") {
		room = $("#bianma").val();
	} else {
		room = "";
	}
	var qjq = "";
	if ($("#cleanStateSelect").val() == "0") {
		qjq = "动态";
	} else if ($("#cleanStateSelect").val() == "1") {
		qjq = "静态";
	} else {
		qjq = "";
	}

	var option = {
		title : {
			show : true,
			text : room + title + qjq,
		},
		tooltip : {
			trigger : 'axis'
		},
		legend : {
			top:'5%',
			data : [ '平均值', '警戒线', '纠偏线', tuli ]
		// 图例
		},
		toolbox : {
			show : true,
			feature : {
				mark : {
					show : true
				},
				dataView : {
					show : false,
					readOnly : false
				},
				magicType : {
					show : false,
					type : [ 'line' ]
				},
				restore : {
					show : false
				},
				saveAsImage : {
					show : true
				}
			}
		},
		calculable : true,
		xAxis : [ {
			type : 'category',
			boundaryGap : false,
		// data : ['周一','周二','周三','周四','周五','周六','周日']//横坐标数据
		} ],
		yAxis : [ {
			type : 'value'
		} ],
		series : [// 驱动图表生成的数据内容
		{
			name : '平均值',
			type : 'line',
			// showSymbol:false,
			symbol : 'none'
		// data:[120, 132, 101, 134, 90, 230, 210]
		}, {
			name : '警戒线',
			type : 'line',
			// showSymbol:false,
			symbol : 'none'
		// data:[220, 182, 191, 234, 290, 330, 310]
		}, {
			name : '纠偏线',
			type : 'line',
			// showSymbol:false,
			symbol : 'none'
		// data:[150, 232, 201, 154, 190, 330, 410]
		}, {
			name : tuli,
			type : 'line',
			symbol : 'none'
		// data:[320, 332, 301, 334, 390, 330, 320]
		} ]
	};
	messReport.setOption(option);
});
function chaxun() {

	// 刷新表格
	var qstmk = $("#qstmk").val();
	
	var dateStart = document.getElementById("startDate").value;
	var dateEnd = document.getElementById("endDate").value;
	// 类型0房间1设备
	var typeSelect = $("#typeSelect").val();
	// 房间设备
	var bianma = $.trim($("#bianma").val());
	// 洁净区状态0动态1静态
	var cleanStateSelect = $("#cleanStateSelect").val();

	// 监测点
	var monitoringPoint = $.trim($("#monitoringPoint").val());
	var inspectionPoint = $("#inspectionPoint").val();
	// 置信上限
	var uclMaxpointFive = $("#uclMaxpointFive").val();
	var uclMaxFive = $("#uclMaxFive").val();
	// 重加载表格
	sampleOrderTable.settings()[0].ajax.data = {
		startDate : dateStart,
		endDate : dateEnd,
		qstmk : qstmk,
		typeSelect : typeSelect,
		bianma : bianma,
		cleanStateSelect : cleanStateSelect,
		monitoringPoint : monitoringPoint,
		inspectionPoint : inspectionPoint,
		uclMaxpointFive : uclMaxpointFive,
		uclMaxFive : uclMaxFive,
	};
	sampleOrderTable.settings()[0].ajax.async = false;
	sampleOrderTable.ajax.reload();

	// ajax刷新图表
	$
			.ajax({
				url : ctx
						+ '/experiment/enmonitor/qualitycontrolprofile/showQualityControJson.action',
				dataType : "json",
				type : "post",
				async : "false",
				data : {
					startDate : dateStart,
					endDate : dateEnd,
					qstmk : qstmk,
					typeSelect : typeSelect,
					bianma : bianma,
					cleanStateSelect : cleanStateSelect,
					monitoringPoint : monitoringPoint,
					inspectionPoint : inspectionPoint,
					uclMaxFive : uclMaxFive,
					uclMaxpointFive :uclMaxpointFive,
				},
				success : function(data2) {
					console.log(data2)
					if (data2.success) {
						var tuli = "";
						var title = "";
						if ($("#qstmk").val() == "1") {
							tuli = "0.5μm粒子";
							title = "洁净区尘埃粒子0.5μm";
						} else if ($("#qstmk").val() == "2") {
							tuli = "5.0μm";
							title = "洁净区尘埃粒子5.0μm";
						} else if ($("#qstmk").val() == "3") {
							tuli = "菌落数";
							title = "洁净区浮游菌菌落数";
						} else if ($("#qstmk").val() == "4") {
							tuli = "菌落数";
							title = "洁净区沉降菌菌落数";
						} else if ($("#qstmk").val() == "5") {
							tuli = "菌落数";
							title = "表面微生物菌落数";
						}
						// else if($("#qstmk").val()=="6"){
						// tuli = "换气次数";
						// title = "洁净区风量换气次数";
						// }
						// else if($("#qstmk").val()=="7"){
						// tuli = "相对压差";
						// title = "压差相对压差";
						// }
						else {
							tuli = "数据";
							title = "趋势图";
						}
						var room = "";
						if ($("#bianma").val() != "") {
							room = $("#bianma").val();
						} else {
							room = "";
						}
						var qjq = "";
						if ($("#cleanStateSelect").val() == "0") {
							qjq = "动态";
						} else if ($("#cleanStateSelect").val() == "1") {
							qjq = "静态";
						} else {
							qjq = "";
						}
						var option = {
							title : {
								show : true,
								text : room + title + qjq,
							},
							tooltip : {
								trigger : 'axis'
							},
							legend : {
								top:'5%',
								data : [ '平均值', '警戒线', '纠偏线', tuli ]
							// 图例
							},
							toolbox : {
								show : true,
								feature : {
									mark : {
										show : true
									},
									dataView : {
										show : false,
										readOnly : false
									},
									magicType : {
										show : false,
										type : [ 'line' ]
									},
									restore : {
										show : false
									},
									saveAsImage : {
										show : true
									}
								}
							},
							calculable : true,
							xAxis : [ {
								type : 'category',
								boundaryGap : false,
								data : data2.riqi
							// ['周一','周二','周三','周四','周五','周六','周日']//横坐标数据
							} ],
							yAxis : [ {
								type : 'value'
							} ],
							series : [// 驱动图表生成的数据内容
							{
								name : '平均值',
								type : 'line',
								data : data2.pingjun
							// [120, 132, 101, 134, 90, 230, 210]
							}, {
								name : '警戒线',
								type : 'line',
								data : data2.jingjie
							// [220, 182, 191, 234, 290, 330, 310]
							}, {
								name : '纠偏线',
								type : 'line',
								data : data2.jiupian
							// [150, 232, 201, 154, 190, 330, 410]
							}, {
								name : tuli,
								type : 'line',
								data : data2.shuzhi
							// [320, 332, 301, 334, 390, 330, 320]
							} ]
						};
						messReport.setOption(option);
					} else {
						top.layer.msg("图表更新失败！");
					}
				}
			});

}
// 查询房间或设备
function showApprovalUser() {
	if ($("#typeSelect").val() == "0") {// 房间
		top.layer
				.open({
					title : "选择房间",
					type : 2,
					area: top.screeProportion,
					btn : biolims.common.selected,
					content : [
							window.ctx
									+ "/experiment/enmonitor/dust/cleanAreaDust/selectRoomTable.action",
							"" ],
					yes : function(index, layero) {
						var name = "";
						$('.layui-layer-iframe', parent.document)
								.find("iframe").contents().find(
										"#addRoomTable .selected").each(
										function(i, v) {
											name = $(v).children("td").eq(1)
													.text()
										});
						$("#bianma").val(name);
						top.layer.close(index);
					},
				});
	} else if ($("#typeSelect").val() == "1") {// 设备
		top.layer
				.open({
					title : "选择设备",
					type : 2,
					offset : [ '10%', '10%' ],
					area : [ document.body.clientWidth - 300,
							document.body.clientHeight - 100 ],
					btn : biolims.common.selected,
					content : [
							window.ctx
									+ "/equipment/main/showInstrumentDialogList.action",
							"" ],
					yes : function(index, layero) {
						var name = "";
						$('.layui-layer-iframe', parent.document)
								.find("iframe").contents().find(
										"#addInstrument .selected").each(
										function(i, v) {
											name = $(v).children("td").eq(1)
													.text()
										});
						$("#bianma").val(name);
						top.layer.close(index);
					},
				});
	} else {
		top.layer.msg("请选择类型！");
	}
}

// 查询并刷新数据
// function QualityControl() {
// var rows = $("#main .selected");
// var length = rows.length;
// if(!length) {
// top.layer.msg(biolims.common.pleaseSelectData);
// return false;
// }
// window.open(ctx+'/system/quality/qualityProductData/showQualityControlReport.action');
//
//	
//	
// var qstmk=$("#qstmk").val();
// var dateStart = document.getElementById("startDate").value;
// var dateEnd = document.getElementById("endDate").value;
// //类型0房间1设备
// var typeSelect=$("#typeSelect").val();
// //房间设备
// var bianma=$("#bianma").val();
// //洁净区状态0动态1静态
// var cleanStateSelect=$("#cleanStateSelect").val();
//	
//	
// var day = Math.floor((new Date(dateEnd).getTime() - new
// Date(dateStart).getTime())
// / (3600 * 1000 * 24));
// if (dateStart != '' && dateStart != '') {
// xMax = day;
// }
// //更新echarts报表数据
// var options = messReport.getOption();
// options.xAxis[0].max = xMax;
// messReport.hideLoading();
// messReport.setOption(options);
// //ajax异步加载数据,计算参数
// $.ajax({
// url : ctx +
// '/system/quality/qualityProductData/showQualityControJson.action',
// dataType : "json",
// type : "post",
// async : "false",
// data : {
// startDate : $("#startDate").val(),
// endDate : $("#endDate").val(),
//			
// qstmk : qstmk,
// typeSelect : typeSelect,
// bianma : bianma,
// cleanStateSelect : cleanStateSelect,
//			
//			
// page : 1,
// // qualityTypeSelect : $("#qualityTypeSelect").val(),
// limit : 1000,
// pi:pi
// },
// success : function(data) {
// allData=data.data;
// $.each(data.data, function(i, n) {
// debugger;
// var str = n.qualityValue;
// var strDate = n.createDate.substr(5,5);
// var id=n.id;
// var strArray = str.split(",");
// var endDate=$("#startDate").val();
// for(var m=0;m<strArray.length;m++){
// /*strDate = Math.floor((new Date(strDate).getTime() - new
// Date(endDate).getTime())
// / (3600 * 1000 * 24));*/
// reportDate += "['"+strDate+"',"+parseInt(strArray[m]*100, 10)/100+"],";
// reportXDate+="'"+strDate+"',";
// reportId+="'"+id+"',";
// dataQua[number]=strArray[m];
// maps.set(strDate,parseInt(strArray[m]*100, 10)/100);
// number++;
// }
// });
// }
// });
// //重加载表格
// sampleOrderTable.settings()[0].ajax.data = {
// startDate : $("#startDate").val(),
// endDate : $("#endDate").val(),
// qualityTypeSelect : $("#qualityTypeSelect").val(),
// pi:pi
// };
// //$("#sampleReceiveModal").modal("hide");
// sampleOrderTable.settings()[0].ajax.async=false;
// sampleOrderTable.ajax.reload();
//	
//	
// }

