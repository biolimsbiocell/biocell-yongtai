var dustParticleTab;
$(function() {
	var options = table(true, "",
			"/experiment/enmonitor/volume/cleanAreaVolume/showCleanAreaVolumeTableJsonList.action", [ {
				"data" : "id",
				"title" : "编号",
			}, 
//			{
//				"data" : "workshopDepartment",
//				"title" : "车间部门",
//			}, {
//				"data" : "region",
//				"title" : "区域",
//			},
			{
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "testDate",
				"title" : "测试时间",
			}, {
				"data" : "measuringInstrumentCode",
				"title" : "测量设备",
			}, {
				"data" : "stateName",
				"title" : "状态",
			}
//			, 
//			{
//				"data" : "testUser",
//				"title" : "测试人",
//			}, {
//				"data" : "cleanZoneStatus",
//				"title" : "洁净区状态",
//			}, {
//				"data" : "cleanZoneGrade",
//				"title" : "换气次数房间等级",
//			}, {
//				"data" : "cleanZoneRoomrating",
//				"title" : "换气数房间为",
//			}, {
//				"data" : "note",
//				"title" : "备注",
//			}
			], null)
	dustParticleTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(dustParticleTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/enmonitor/volume/cleanAreaVolume/showCleanAreaVolumeEdit.action?sampleStyle='
			+ $("#sampleStyle").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/enmonitor/volume/cleanAreaVolume/showCleanAreaVolumeEdit.action?id=' + id
			+ "&sampleStyle=" + $("#sampleStyle").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/enmonitor/volume/cleanAreaVolume/showCleanAreaVolumeEdit.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : "编号",
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : "创建人",
		"type" : "input",
		"searchName" : "createUser-name",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : "测试时间（开始）",
		"type" : "dataTime",
		"searchName" : "testDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "测试时间（结束）",
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "testDate##@@##2"
	}, {
		"txt" : "测量设备",
		"type" : "input",
		"searchName" : "measuringInstrumentCode",
	}, {
		"txt" : "状态",
		"type" : "input",
		"searchName" : "stateName",
	}, {
		"type" : "table",
		"table" : dustParticleTab
	} ];
}
