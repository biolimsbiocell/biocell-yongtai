/* 
 * 文件描述: 单位组的弹框
 * 
 */
var addSampleOrder;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.orderCode,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.sname,
	});
	colOpts.push({
		"data": "barcode",
		"title": "批次号",
	});
	colOpts.push({
		"data": "gender",
		"title": biolims.common.gender,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.male;
			}
			if(data == "0") {
				return biolims.common.female;
			}
			if(data == "3") {
				return biolims.common.unknown;
			} else {
				return '';
			}
		}
	});
	colOpts.push({
		"data": "age",
		"title": biolims.common.age,
	});
	colOpts.push({
		"data": "productName",
		"title": "产品名称",
	});
	colOpts.push({
		"data": "stateName",
		"title": biolims.common.state,
	});

	var tbarOpts = [];

	tbarOpts.push({
		text:"搜索",
		action: search,
	});
	
	var options = table(false, null,
			'/experiment/qaAudit/qaAudit/showAllSampleOrderDialogListJson.action', colOpts, tbarOpts)
	addSampleOrder = renderData($("#addSampleOrder"), options);
	
	$("#addSampleOrder").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			//$("#addSampleOrder_wrapper .dt-buttons").empty();
			$('#addSampleOrder_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addSampleOrder tbody tr");
			
			addSampleOrder.ajax.reload();
			addSampleOrder.on('draw', function() {
				trs = $("#addSampleOrder tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser"
		},
		{
			"txt": "批次号",
			"type": "input",
			"searchName": "barcode"
		},
		{
			"txt": biolims.sample.attendingDoctor,
			"type": "input",
			"searchName": "attendingDoctor"
		},
		{
			"txt": biolims.sample.medicalInstitutions,
			"type": "input",
			"searchName": "medicalInstitutions"
		},
		{
			"txt": biolims.common.gender,
			"type": "select",
			"options": biolims.common.pleaseChoose+"|"+biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
			"changeOpt": "''|1|0|3",
			"searchName": "gender"
		},
		{
			"type": "table",
			"table": addSampleOrder
		}
	];
}