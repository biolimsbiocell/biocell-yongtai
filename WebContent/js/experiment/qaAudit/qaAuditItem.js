var qaAuditItemTab;
var qaAuditItemChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "productName",
		"title": "产品名称",
//		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	});
	colOpts.push({
		"data" : "productBatchNumber",
		"title" : "产品批号",
//		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "productBatchNumber");
		}
	});
	colOpts.push({
		"data" : "unqualifiedReason",
		"title" : "不合格原因",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "unqualifiedReason");
		}
	});
	colOpts.push({
		"data" : "productSpecifications",
		"title" : "规格",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "productSpecifications");
		}
	});
	colOpts.push({
		"data" : "productNumber",
		"title" : "数量",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "productNumber");
		}
	});
	colOpts.push({
		"data" : "note",
		"title" : "备注",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	});
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#qaAudit_state").text()!=biolims.common.finish){
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				addItem($("#qaAuditItem"));
			}
		});
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#qaAuditItem"),
						"/experiment/qaAudit/qaAudit/delQaAuditItem.action");
				}
		});
		tbarOpts.push({
			text: "保存",
			action: function() {
				saveQaAuditItemTab();
			}
		});
		tbarOpts.push({
			text: "选择产品",
			action: function() {
				selectProduct();
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#qaAuditItem"))
			}
		});
	}
	var qaAuditItemTabOps = table(true,$("#qaAudit_id").val(), "/experiment/qaAudit/qaAudit/showQaAuditItemTableJson.action", colOpts, tbarOpts);
	qaAuditItemTab = renderData($("#qaAuditItem"), qaAuditItemTabOps);
	qaAuditItemTab.on('draw', function() {
		qaAuditItemChangeLog = qaAuditItemTab.ajax.json();
	});
});
//选择产品
function selectProduct(){
	var rows = $("#qaAuditItem .selected");
	if (!rows.length){
		top.layer.msg("请选择数据");
		return false;
	}
	top.layer.open({
		title: "请选择",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/experiment/qaAudit/qaAudit/showSampleOrderDialogList.action", ''],
		yes: function(index, layer) {
			rows.addClass("editagain");
			var orderCode = $('.layui-layer-iframe', parent.document).find("iframe").contents().
				find("#addSampleOrder .chosed").children("td").eq(2).text();
			var productname = $('.layui-layer-iframe', parent.document).find("iframe").contents().
			find("#addSampleOrder .chosed").children("td").eq(5).text();
			rows.find("td[savename='productBatchNumber']").text(orderCode);
			rows.find("td[savename='productName']").text(productname);
			top.layer.close(index);
		}
	})
}
function saveQaAuditItemTab() {
	var ele = $("#qaAuditItem");
	var changeLog = "QA审批子表"+"：";
	var data = saveQaAuditItemJson(ele);
	if(!data){
		return false;
	}
	changeLog = getQaAuditItemChangeLog(data, ele, changeLog);
	var changeLogs = "";
	if(changeLog != "QA审批子表"+"："){
		changeLogs = changeLog
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/experiment/qaAudit/qaAudit/saveQaAuditItem.action',
		data: {
			id: $("#qaAudit_id").val(),
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				qaAuditItemTab.ajax.reload();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
function saveQaAuditItemJson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag =true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	if(flag){
		return JSON.stringify(data);
	}else{
		return false;
	}
}
function getQaAuditItemChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		qaAuditItemChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}