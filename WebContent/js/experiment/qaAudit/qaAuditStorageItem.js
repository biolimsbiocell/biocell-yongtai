var qaAuditStorageItemTab;
var qaAuditStorageItemChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "materielId",
		"title": "物料编号",
//		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "materielId");
		}
	});
	colOpts.push({
		"data": "materielName",
		"title": "物料名称",
//		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "materielName");
		}
	});
	colOpts.push({
		"data" : "materielBatchNumber",
		"title" : "物料批号",
//		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "materielBatchNumber");
		}
	});
	colOpts.push({
		"data" : "unqualifiedReason",
		"title" : "不合格原因",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "unqualifiedReason");
		}
	});
	colOpts.push({
		"data" : "materielSpecifications",
		"title" : "规格",
//		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "materielSpecifications");
		}
	});
	colOpts.push({
		"data" : "materielNumber",
		"title" : "数量",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "materielNumber");
		}
	});
	colOpts.push({
		"data" : "note",
		"title" : "备注",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	});
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#qaAudit_state").text()!=biolims.common.finish){
		tbarOpts.push({
			text: biolims.common.fillDetail,
			action: function() {
				addItem($("#qaAuditStorageItem"));
			}
		});
		tbarOpts.push({
			text: biolims.common.delSelected,
			action: function() {
				removeChecked($("#qaAuditStorageItem"),
						"/experiment/qaAudit/qaAudit/delQaAuditStorageItem.action");
				}
		});
		tbarOpts.push({
			text: "保存",
			action: function() {
				saveQaAuditStorageItemTab();
			}
		});
		tbarOpts.push({
			text: "选择物料",
			action: function() {
				selectProductwuliao();
			}
		});
		tbarOpts.push({
			text: "选择批次",
			action: function() {
				selectProductwuliaopci();
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#qaAuditStorageItem"))
			}
		});
	}
	var qaAuditStorageItemTabOps = table(true,$("#qaAudit_id").val(), "/experiment/qaAudit/qaAudit/showQaAuditStorageItemTableJson.action", colOpts, tbarOpts);
	qaAuditStorageItemTab = renderData($("#qaAuditStorageItem"), qaAuditStorageItemTabOps);
	qaAuditStorageItemTab.on('draw', function() {
		qaAuditStorageItemChangeLog = qaAuditStorageItemTab.ajax.json();
	});
});
//选择物料批次
function selectProductwuliaopci(){
	var rows = $("#qaAuditStorageItem .selected");
	if (!rows.length){
		top.layer.msg("请选择数据");
		return false;
	}
	var infoid = "";
	rows.each(function(i, v) {
		infoid = $(v).find("td[savename='materielId']").text();
	})
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		offset: ['10%', '10%'],
		area: [document.body.clientWidth - 300,
			document.body.clientHeight - 100
		],
		btn: biolims.common.selected,
		content: [
			window.ctx + "/experiment/qaAudit/qaAudit/getStrogeReagent.action?id=" + infoid + "",
			""
		],
		yes: function(index, layero) {
			rows.addClass("editagain");
			var batch = $(".layui-layer-iframe", parent.document)
				.find("iframe").contents().find("#addReagent .chosed")
				.children("td").eq(2).text();
			rows.find("td[savename='materielBatchNumber']").text(batch);
			top.layer.close(index);
		},
	});
//	top.layer.open({
//		title: "请选择",
//		type: 2,
//		area: ["650px", "400px"],
//		btn: biolims.common.selected,
//		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
//		yes: function(index, layer) {
//			rows.addClass("editagain");
//			
//			var orderCode = $('.layui-layer-iframe', parent.document).find("iframe").contents().
//				find("#addSampleOrder .chosed").children("td").eq(0).text();
//			
//			
//			rows.find("td[savename='materielBatchNumber']").text(orderCode);
//			top.layer.close(index);
//		}
//	})
}
//选择物料
function selectProductwuliao(){
	var rows = $("#qaAuditStorageItem .selected");
	if (!rows.length){
		top.layer.msg("请选择数据");
		return false;
	}
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/getStorage.action", ''],
		yes: function(index, layer) {
			rows.addClass("editagain");
			var productname = $('.layui-layer-iframe', parent.document).find("iframe").
			contents().find("#addStorage .chosed").children("td").eq(1).text();
			var code = $('.layui-layer-iframe', parent.document).find("iframe").
			contents().find("#addStorage .chosed").children("td").eq(0).text();
			var gg = $('.layui-layer-iframe', parent.document).find("iframe").
			contents().find("#addStorage .chosed").children("td").eq(2).text();
			rows.find("td[savename='materielName']").text(productname);
			rows.find("td[savename='materielId']").text(code);
			rows.find("td[savename='materielSpecifications']").text(gg);
//			var code = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStorage .chosed").children("td").eq(0).text();
//			var reali = '<li class="reagli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">原辅料名称:<span reagentid=' + code + ' class="reaName">' + name + '</span></small><i class="fa fa-trash pull-right"></i></li>';
//			$(that).parents(".timeline-item").find("#reagentBody").append(reali);
			top.layer.close(index);
//			reagentAndCosRemove();
		},
	})
//	top.layer.open({
//		title: "请选择",
//		type: 2,
//		area: ["650px", "400px"],
//		btn: biolims.common.selected,
//		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
//		yes: function(index, layer) {
//			rows.addClass("editagain");
//			
//			var productname = $('.layui-layer-iframe', parent.document).find("iframe").contents().
//			find("#addSampleOrder .chosed").children("td").eq(4).text();
//			
//			
//			rows.find("td[savename='materielName']").text(productname);
//			top.layer.close(index);
//		}
//	})
}
function saveQaAuditStorageItemTab() {
	var ele = $("#qaAuditStorageItem");
	var changeLog = "QA审批物料子表"+"：";
	var data = saveQaAuditStorageItemJson(ele);
	if(!data){
		return false;
	}
	changeLog = getQaAuditStorageItemChangeLog(data, ele, changeLog);
	var changeLogs = "";
	if(changeLog != "QA审批物料子表"+"："){
		changeLogs = changeLog
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/experiment/qaAudit/qaAudit/saveQaAuditStorageItem.action',
		data: {
			id: $("#qaAudit_id").val(),
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				qaAuditStorageItemTab.ajax.reload();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
function saveQaAuditStorageItemJson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag =true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	if(flag){
		return JSON.stringify(data);
	}else{
		return false;
	}
}
function getQaAuditStorageItemChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '编号为"' + v.id + '":';
		qaAuditStorageItemChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}