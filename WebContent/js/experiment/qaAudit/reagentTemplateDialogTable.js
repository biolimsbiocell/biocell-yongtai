/* 
* 文件名称 :reagentTemplateDialogTable.js
* 创建者 : 郭恒开
* 创建日期: 2018/01/25
* 文件描述:选择实验页面原辅料的datatables
* 
*/
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "storage-id",
		"title": biolims.common.reagentNo,
	});
	colOpts.push({
		"data": "storage-name",
		"title": biolims.common.reagentName,
	});
	colOpts.push({
		"data": "serial",
		"title": biolims.common.batchCoding,
	});
	colOpts.push({
		"data": "expireDate",
		"title": biolims.common.expirationDate,
	});
	colOpts.push({
		"data": "note",
		"title": biolims.common.snCode,
	});
	var tbarOpts = [];
	var addReagentOptions = table(false,$("#reagentId").val(),
			'/experiment/qaAudit/qaAudit/getStrogeReagentJson.action',colOpts , tbarOpts)
		var reagentTable = renderData($("#addReagent"), addReagentOptions);
	$("#addReagent").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addReagent_wrapper .dt-buttons").empty();
		$('#addReagent_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addReagent tbody tr");
	reagentTable.ajax.reload();
	reagentTable.on('draw', function() {
		trs = $("#addReagent tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

