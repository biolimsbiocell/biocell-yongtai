var qaAuditTable;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": "审批编号",
		}, {
			"data": "eventType-name",
			"visible": false,
			"title": "事件类型",
		}, {
			"data": "eventType-name",
			"title": "事件类型",
		}, {
			"data": "productName",
			"title": "产品名称",
		}, {
			"data": "name",
			"title": "备注",
		}, {
			"data": "productBatchNumber",
			"title": "产品批号",
		}, {
			"data": "productSpecifications",
			"title": "产品规格",
		}, {
			"data": "num",
			"title": "数量",
		}, {
			"data": "sampleCode",
			"title": "样本编号",
		}, {
			"data": "createUser-name",
			"title": "创建人",
		}, {
			"data": "createDate",
			"title": "创建日期",
		}, {
			"data": "stateName",
			"title": "状态",
		}];
		
	var options = table(true, "",
		"/experiment/qaAudit/qaAudit/showqaAuditTableJson.action",colData , null)
	qaAuditTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(qaAuditTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/experiment/qaAudit/qaAudit/editQaAudit.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/experiment/qaAudit/qaAudit/editQaAudit.action?id=' + id ;
}
// 查看
function view() {
//	var id = $(".selected").find("input").val();
//	if(id == "" || id == undefined) {
//		top.layer.msg(biolims.common.selectRecord);
//		return false;
//	}
//	window.location = window.ctx +
//		'/experiment/qaAudit/qaaudit/viewSampleOrder.action?id=' + id + '&type=' + $("#order_type").val();
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "QA编号",
			"type": "input",
			"searchName": "id"
		},
		{
			"type": "table",
			"table": qaAuditTable
		}
	];
}