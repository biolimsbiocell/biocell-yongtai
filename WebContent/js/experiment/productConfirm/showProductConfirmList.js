var oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "sampleOrder-barcode",
		"title" : "产品批号",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "sampleOrder-barcode");
			$(td).attr("sampleCode", rowData['sampleCode']);
			$(td).attr("code", rowData['code']);
		},
	});
	colOpts.push({
		"data" : "sampleOrder-ccoi",
		"title" : "cCOI",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-ccoi");
		}
	});
//	colOpts.push({
//		"data" : "sampleCode",
//		"title" : "CCOI",
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "sampleCode");
//		}
//	});
	colOpts.push({
		"data" : "sampleOrder-crmCustomer-name",
		"title" : "医院",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-crmCustomer-name");
		}
	});
	colOpts.push({
		"data" : "harvestDate",
		"title" : "收获开始时间",
		"className":"date",
		width:"150px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "harvestDate");
		}
	});
	colOpts.push({
		"data" : "cellDate",
		"title" : "出细胞时间",
		"className":"date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "cellDate");
		}
	});
	colOpts.push({
		"data" : "endHarvestDate",
		"title" : "收获结束时间",
		"className":"date",
		width:"150px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "endHarvestDate");
		}
	});
	colOpts.push({
		"data": 'templeOperator-name',
		"title": "操作人",
		render:function(data, type, row, meta) {
			if(data!=null&&data!=""){
				return "<button class='btn btn-info btn-xs' code='"+row['code']+"' toid='"+row['templeOperator-id']+"' onclick='seluser(this)'>"+data+"</button>";
			}else{
				return "<button class='btn btn-info btn-xs' toid='' onclick='seluser(this)'>请选择</button>";
			}
		}

	});
	colOpts.push({
		"data": 'operatingRoomName',
		"title": "房间",
		 render:function(data, type, row, meta) {
			if(data!=null&&data!=""){
				return "<button class='btn btn-info btn-xs' code='"+row['code']+"' roomId='"+row['operatingRoomId']+"' onclick='selroom(this)'>"+data+"</button>";
			}else{
				return "<button class='btn btn-info btn-xs' onclick='selroom(this)'>请选择</button>";
			}
		}
	});
	
	
	
	colOpts.push({
		"data" : "transportDate",
		"title" : "运输时间",
//		"className":"date",
		width:"150px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "transportDate");
		}
	
	});
	/*
	 * colOpts.push({ "data" : "dicSampleType-id", "title" : "样本类型ID",
	 * "createdCell" : function(td) { $(td).attr("saveName",
	 * "dicSampleType-id"); }, "visible" : false }); colOpts.push({ "data" :
	 * "dicSampleType-name", "title" : "样本类型", "createdCell" : function(td) {
	 * $(td).attr("saveName", "dicSampleType-name"); } });
	 */
/*	colOpts.push({
		"data" : "numTubes",
		"title" : "细胞数/袋",
		"createdCell" : function(td) {
			$(td).attr("saveName", "numTubes");
		},
		"className":"edit"
	});
	colOpts.push({
		"data" : "transportTemperature",
		"title" : "运输温度",
		"createdCell" : function(td) {
			$(td).attr("saveName", "transportTemperature");
		},
		"className":"edit"
	});*/
	colOpts.push({
		"data":"state",
		"title": "安排状态",
// "className":"select",
		"name":"|未安排|已安排",
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
			$(td).attr("selectOpt", "|未安排|已安排");
	    },
	    "render": function(data, type, full, meta) {
			if(data == "2") {
				return "未安排";
			}if(data == "1") {
				return "已安排";
			}else{
				return "";
			}
		}
	}); 
	var tbarOpts = [];
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#sampleTransportItemDiv"))
		}
	});
	tbarOpts.push({
		text : "未安排",
		action : function() {
			zhuanhuan(2);
		}
	});
	tbarOpts.push({
		text : "已安排",
		action : function() {
			zhuanhuan(1);
		}
	});
	tbarOpts.push({
				text : biolims.common.search,
				action : function() {
					search();
				}
			});
	tbarOpts.push({
				text : biolims.common.save,
				action : function() {
					saveItem();
				}
			});
	tbarOpts.push({
		text : "生产安排确认",
		action : function() {
			saveItemConfirm();
		}
	});
	tbarOpts.push({
		text : "取消确认",
		action : function() {
			cancelItemConfirm();
		}
	});
	
	var ops = table(true, null,
			"/experiment/productConfirm/productConfirm/showProductConfirmItemListJson.action?type="+$("#sampleTransportItem_type").val(), colOpts,
			tbarOpts);
	sampleTransportItemTab = renderData($("#sampleTransportItemDiv"), ops);
	sampleTransportItemTab.on('draw', function() {
				oldChangeLog = sampleTransportItemTab.ajax.json();
			});
	$('#sampleTransportItemDiv').on('init.dt', function() {
				recoverSearchContent(sampleTransportItemTab);
			})
});
function zhuanhuan(aa){
	sampleTransportItemTab.settings()[0].ajax.data = {
		type:aa
	};
	$("#sampleTransportItem_type").val(aa);
	sampleTransportItemTab.ajax.url("/experiment/productConfirm/productConfirm/showProductConfirmItemListJson.action").load();
	oldChangeLog = sampleTransportItemTab.ajax.json();
	if(aa=="2"){
		$("#titleNote").text("未安排");
	}
	if(aa=="1"){
		$("#titleNote").text("已安排");
	}
}

//选择操作人
function seluser(that){
	
	
	$.ajax({
		type: 'post',
//		url: '/experiment/productionPlan/productionPlan/findCprPossibleModify.action',
		url: '/experiment/productConfirm/productConfirm/findCprPossibleModify.action',
		data: {
			code:$(that).attr("code")
		},
		success: function(datas) {
			var data = JSON.parse(datas);
			if(data.success){
				top.layer.msg("该条记录操作人已经开始操作！")
			}else{
				

	top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area: top.screeProportion,		
		btn: biolims.common.selected,
		content:[window.ctx+"/core/user/selectUserTable.action?groupId=SC002",''],
		yes: function(index, layer) {
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
		top.layer.close(index);
		$(that).attr("toid",id);
		$(that).val(name);
		$(that).parent().parent().addClass("editagain");
		$(that).parent().attr("saveName", "templeOperator-name")
	    $(that).parent().attr("templeOperator-id", id);
		$(that).html(name);
		},
	})
	
			}
		}
	})
}
//选择房间
function selroom(that){
	
	$.ajax({
		type: 'post',
//		url: '/experiment/productionPlan/productionPlan/findCprPossibleModify.action',
		url: '/experiment/productConfirm/productConfirm/findCprPossibleModify.action',
		data: {
			code:$(that).attr("code")
		},
		success: function(datas) {
			var data = JSON.parse(datas);
			if(data.success){
				top.layer.msg("该条记录操作人已经开始操作！")
			}else{
				
	
				
			
	top.layer.open({
		title:biolims.common.pleaseChoose,
		type:2,
		area: top.screeProportion,		
		btn: biolims.common.selected,
		content:[window.ctx+ "/experiment/cell/passage/cellPassage/selectRoomTable.action",''],
		yes: function(index, layer) {
		var name = [];
//		var id = [];
		var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addRoomTable .selected").children("td").eq(2).text();
         console.log($('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addRoomTable .selected"))
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addRoomTable .selected").children("td").eq(1).text();
		var state =$('.layui-layer-iframe',parent.document).find("iframe").contents().find("#addRoomTable .selected").children("td").eq(6).text();
		if(name==""){
			top.layer.msg("请选择一条数据！");
			return false;
		}
		if(state=="占用"){
			top.layer.msg("房间已被占用！");
		}
			top.layer.close(index);
			$(that).attr("roomId",id);
			$(that).parent().parent().addClass("editagain")
			$(that).parent().attr("saveName", "operatingRoomName");
			$(that).parent().attr("operatingRoomId", id);
			$(that).val(name);
			$(that).html(name);
		},
	})
			}
		}
	})
}
// 回输计划变更
// function reinfusionChangePlan() {
// if($("#sampleTransportItem_type").val()=="1"){
// var rows = $("#sampleTransportItemDiv .selected");
// if(rows.length!=1){
// top.layer.msg("请选择一条回输计划！");
// return false;
// }
// var data = "";//[]
// rows.each(function(i, val) {
// var tds = $(val).children("td");
// data = $(tds[0]).find("input").val();
// // data.push($(tds[0]).find("input").val());
// });
// top.layer.open({
// title : "填写修改内容",
// type : 1,
// area : ["30%", "60%"],
// btn : "确定提交",
// btnAlign : 'c',
// content : '<textarea id="checkCode" class="form-control" style="width:
// 99%;height: 99%;"></textarea>',
// yes : function(indexx, layer) {
// var array = $("#checkCode", parent.document).val();
// $.ajax({
// type : 'post',
// url :
// '/experiment/reinfusionPlan/reinfusionPlan/saveReinfusionPlanItemChangPlan.action',
// data : {
// dataJson : data,
// changeLog : array
// },
// success : function(data1) {
// var data = JSON.parse(data1)
// if (data.success) {
// top.layer.closeAll();
// top.layer.msg("回输计划变更成功！");
// tableRefresh();
// } else {
// top.layer.msg("回输计划变更失败！")
// };
// }
// });
// }
// ,
// });
//		
// }else{
// top.layer.msg("未安排数据不用提交申请！");
// return false;
// }
// }

// 保存
function saveItem() {
	if($("#sampleTransportItem_type").val()=="1"){
		top.layer.msg("已安排数据，不允许直接保存！");
		return false;
	}else{
		var ele = $("#sampleTransportItemDiv");
		var changeLog = "生产确认：";
		var trsedi = ele.find("tbody").children(".editagain");
		
		
		var data1 = true;
		data1 = saveItemjsonPD(ele);
		if(data1){
			
			var data = saveItemjson(ele);
			console.log(ele)
			changeLog = getChangeLog(data, ele, changeLog);
			var changeLogs = "";
			if (changeLog != "生产确认：") {
				changeLogs = changeLog;
			}
			top.layer.load(4, {
						shade : 0.3
					});
			
			$.ajax({
				type : 'post',
				url : '/experiment/productConfirm/productConfirm/saveProductConfirmItemList.action',
				data : {
					dataJson : data,
					logInfo : changeLogs
					
				},
				success : function(data) {
					var data = JSON.parse(data)
					if (data.success) {
						top.layer.closeAll();
						console.log(trsedi.length)
//						if(trsedi.length>0){
//						top.layer.msg(biolims.common.saveSuccess);
//						}else{
//						top.layer.msg("有数据需要填写");
//						}
						
						tableRefresh();
					} else {
						top.layer.closeAll();
						top.layer.msg(biolims.common.saveFailed)
					};
				}
			});
			console.log(data)

		}else{
			
		}
		
	
	}
}

//取消生产确认
function cancelItemConfirm(){
	if($("#sampleTransportItem_type").val()=="2"){
		top.layer.msg("未安排数据，不用取消安排！");
		return false;
	}
	var rows = $("#sampleTransportItemDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var data = [];
	rows.each(function(i, val) {
		var tds = $(val).children("td");
		data.push($(tds[0]).find("input").val());
	});
	$.ajax({
		type : 'post',
		url : '/experiment/productConfirm/productConfirm/cancelProductConfirmItemConfirmList.action',
		data : {
			ids : data
		},
		success : function(datas) {
			var data = JSON.parse(datas);
			if (data.success) {
				top.layer.msg("取消成功");
				tableRefresh();
			} else {
				top.layer.msg("取消失败")
			};
		}
	});
}

// 生产确认
function saveItemConfirm() {
	if($("#sampleTransportItem_type").val()=="1"){
		top.layer.msg("已安排数据，不需再安排！");
		return false;
	}else{
		var rows = $("#sampleTransportItemDiv .selected");
		var length = rows.length;
		if(!length) {
			top.layer.msg(biolims.common.pleaseSelect);
			return false;
		}
		
		var ele = $("#sampleTransportItemDiv");
		var rq = $("#sampleTransportItemDiv .selected");
		console.log(rq);
		
		var st=true;
		rq.each(function(i,j,k){
			var harvestDate=$(this).find("td[savename='harvestDate']").text();
			if(harvestDate==""){
				st=false;
			}
			var cellDate=$(this).find("td[savename='cellDate']").text();
			if(cellDate==""){
				st=false;
			}
			var endHarvestDate =$(this).find("td[savename='endHarvestDate']").text();
			if(endHarvestDate==""){
				st=false;
			}
		})
			
	    if(st){
	 
	    	//查后台数据
			var codes=[];
			rq.each(function(i,j,k){
			var code=$(this).find("td[savename='sampleOrder-barcode']").text();
	        codes.push(code);
			})
			$.ajax({
				type : 'post',
				url : '/experiment/productConfirm/productConfirm/findC.action',
				data : {
					codes : codes,
					
				},
				success : function(data) {
					var data = JSON.parse(data)
					if (data.success) {
						
						var trs = ele.find("tbody tr").children(".editagain");
						console.log(trs);
						if(trs.length>0){
							
							top.layer.msg("请先保存数据！");
							return false;
						}else{
							var rows = $("#sampleTransportItemDiv .selected");
//							var length = rows.length;
//							if(!length) {
//								top.layer.msg(biolims.common.pleaseSelect);
//								return false;
//							}
							var data = [];
							rows.each(function(i, val) {
								var tds = $(val).children("td");
								data.push($(tds[0]).find("input").val());
							});
							var data2 = true;
							data2 = saveItemConfrinPD(ele);
							if(data2){
								$.ajax({
									type : 'post',
									url : '/experiment/productConfirm/productConfirm/saveProductConfirmItemConfirmList.action',
									data : {
										ids : data
									},
									success : function(datas) {
										var data = JSON.parse(datas);
										if (data.success) {
											top.layer.msg("生产安排完成！");
											tableRefresh();
										} else {
											top.layer.msg(biolims.common.saveFailed)
										};
									}
								});
							}else{
								
							}
							
							
						}
						
						

					} else {
					top.layer.msg("请先保存数据！");

					};
				}
			});
			
			
			
			
			//	

		
	    }else{
	    	top.layer.msg("请填写必填项！保存之后再提交！")
		}
		
	}
}

//获得保存时的json数据
function saveItemjsonPD(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var flag = true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if(k == "harvestDate") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请填写收获开始时间！");
					break;
				}
			}
			if(k == "cellDate") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请填写出细胞时间！");
					break;
				
				}
			}
			if(k == "endHarvestDate") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请填写收获结束时间！");
					break;
				}
			}
		}
	});
	return flag;
}
//生产确认
function saveItemConfrinPD(ele){
	var rows = $("#sampleTransportItemDiv .selected");
//	if (!rows.length) {
//		top.layer.msg("请选择数据！");
//		return false;
//	}
	var trs = rows.find("tbody").children(".editagain");
	var flag = true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			if(k == "harvestDate") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请填写收货开始时间！");
					break;
				}
			}
			if(k == "cellDate") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请填写出细胞时间！");
					break;
				}
			}
			
			if(k == "endHarvestDate") {
				if (!json[k]) {
					console.log(json[k]);
					flag = false;
					top.layer.msg("请填写收货结束时间！");
					break;
				}
			}
		}
	});
	return flag;
}


// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag = true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			if(k == "state") {
				var result = $(tds[j]).text();
				if(result == "未安排") {
					json[k] = "2";
				}else if(result == "已安排") {
					json[k] = "1";
				} else {
					json[k] = "";
				}
				continue;
			}
			
			if(k == "code") {
				json["sampleCode"] = $(tds[j]).attr("sampleCode");
			}
			if(k == "templeOperator-name") {
				json["templeOperator-id"] = $(tds[j]).attr("templeOperator-id");
			}
			if(k == "operatingRoomName") {
				json["operatingRoomId"] = $(tds[j]).attr("operatingRoomId");
			}

			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开 创建日期: 2018/03/09 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
				var id = v.id;
				if (!id) {
					changeLog += '新增记录:';
					for (var k in v) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"为"' + v[k] + '";';
					}
					return true;
				}
				changeLog += /* '样本编号为"' + v.code + */ '":';
				oldChangeLog.data.forEach(function(vv, ii) {
							if (vv.id == id) {
								for (var k in v) {
									if (v[k] != vv[k]) {
										var title = ele.find("th[savename=" + k
												+ "]").text();
										changeLog += '"' + title + '"'
												+ "由" + '"'
												+ vv[k] + '"'
												+ "改为" + '"'
												+ v[k] + '";';
									}
								}
								return false;
							}
						});
			});
	return changeLog;
}
// 弹框模糊查询参数
function searchOptions() {
	return [{
				"txt" : "产品批号",
				"type" : "input",
				"searchName" : "code"
				
			}, 
			{
				"txt" : "操作人",
				"type" : "input",
				"searchName" : "templeOperator-name"
				
			}, 
			/*{
				"txt" : "原始样本编号",
				"type" : "input",
				"searchName" : "sampleCode"
				
			},*/
			{
				"txt" : "医院",
				"type" : "input",
				"searchName" : "sampleOrder-crmCustomer-name"
				
			},
//			{
//				"txt" : "科室",
//				"type" : "input",
//				"searchName" : "inspectionDepartment"
//				
//			}
//			{
//				"txt" : "医生",
//				"type" : "input",
//				"searchName" : "attendingDoctor"
//			},
//			{
//				"txt" : "计划回输日期",
//				"type" : "dataTime",
//				"searchName" : "reinfusionPlanDate",
//			},
			{
				"type" : "table",
				"table" : sampleTransportItemTab
			}];
}

// 报告上传
function fileUp1(that) {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	$("#uploadFileValx").fileinput({
		uploadUrl : ctx
				+ "/attachmentUpload?useType=report&modelType=sampleReportItemNew&contentId="
				+ id,
		uploadAsync : true,
		language : 'zh',
		showPreview : true,
		enctype : 'multipart/form-data',
		elErrorContainer : '#kartik-file-errors'
		,
	});
	// if($("#uploadFile .fileinput-remove").length){
	// $("#uploadFile .fileinput-remove").eq(0).click();
	// }
	$("#uploadFilex").modal("show");
	// var cfileInput = fileInput('1','samplingTaskItem', id);
}
// 报告查看
function fileView1(that) {
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title : biolims.common.attachment,
		type : 2,
		skin : 'layui-layer-lan',
		area : top.screeProportion,
		content : window.ctx
				+ "/operfile/initFileList.action?flag=report&modelType=sampleReportItemNew&id="
				+ id,
		cancel : function(index, layero) {
			top.layer.close(index)
		}
	})
}

// 上传说明
function fileUp2(that) {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	$("#uploadFileValx").fileinput({
		uploadUrl : ctx
				+ "/attachmentUpload?useType=exp&modelType=sampleReportItemNew&contentId="
				+ id,
		uploadAsync : true,
		language : 'zh',
		showPreview : true,
		enctype : 'multipart/form-data',
		elErrorContainer : '#kartik-file-errors'
		,
	});
	// if($("#uploadFile .fileinput-remove").length){
	// $("#uploadFile .fileinput-remove").eq(0).click();
	// }
	$("#uploadFilex").modal("show");
	// var cfileInput = fileInput('1','samplingTaskItem', id);
}
// 说明查看
function fileView2(that) {
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title : biolims.common.attachment,
		type : 2,
		skin : 'layui-layer-lan',
		area : top.screeProportion,
		content : window.ctx
				+ "/operfile/initFileList.action?flag=exp&modelType=sampleReportItemNew&id="
				+ id,
		cancel : function(index, layero) {
			top.layer.close(index)
		}
	})
}

// 上传其他附件
function fileUp3(that) {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	$("#uploadFileValx").fileinput({
		uploadUrl : ctx
				+ "/attachmentUpload?useType=misc&modelType=sampleReportItemNew&contentId="
				+ id,
		uploadAsync : true,
		language : 'zh',
		showPreview : true,
		enctype : 'multipart/form-data',
		elErrorContainer : '#kartik-file-errors'
		,
	});
	// if($("#uploadFile .fileinput-remove").length){
	// $("#uploadFile .fileinput-remove").eq(0).click();
	// }
	$("#uploadFilex").modal("show");
	// var cfileInput = fileInput('1','samplingTaskItem', id);
}
// 其他附件查看
function fileView3(that) {
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title : biolims.common.attachment,
		type : 2,
		skin : 'layui-layer-lan',
		area : top.screeProportion,
		content : window.ctx
				+ "/operfile/initFileList.action?flag=misc&modelType=sampleReportItemNew&id="
				+ id,
		cancel : function(index, layero) {
			top.layer.close(index)
		}
	})
}