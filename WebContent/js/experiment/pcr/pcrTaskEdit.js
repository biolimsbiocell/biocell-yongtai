$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#pcrTask_state").val();
	if(id!="1"){
		load("/experiment/pcr/pcrTask/showPcrTaskTempList.action", null, "#pcrTaskTempPage");
		$("#markup").css("width","75%");
//		$("#tabs1").tabs({
//			select : function(event, ui) {
//				if(ui.index==1){
//					load("/experiment/dna/experimentDnaGet/showDnaTechTaskList.action", { }, "#keji");
//				}
//			}
//		});
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#pcrTaskTempPage").remove();
	}
});	
function add() {
	window.location = window.ctx + "/experiment/pcr/pcrTask/editPcrTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/pcr/pcrTask/showPcrTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#pcrTask", {
					userId : userId,
					userName : userName,
					formId : $("#pcrTask_id").val(),
					title : $("#pcrTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#pcrTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var pcrTaskItemDivData = $("#pcrTaskItemdiv").data("pcrTaskItemGrid");
		document.getElementById('pcrTaskItemJson').value = commonGetModifyRecords(pcrTaskItemDivData);
	    var pcrTaskTemplateDivData = $("#pcrTaskTemplatediv").data("pcrTaskTemplateGrid");
		document.getElementById('pcrTaskTemplateJson').value = commonGetModifyRecords(pcrTaskTemplateDivData);
	    var pcrTaskReagentDivData = $("#pcrTaskReagentdiv").data("pcrTaskReagentGrid");
		document.getElementById('pcrTaskReagentJson').value = commonGetModifyRecords(pcrTaskReagentDivData);
	    var pcrTaskCosDivData = $("#pcrTaskCosdiv").data("pcrTaskCosGrid");
		document.getElementById('pcrTaskCosJson').value = commonGetModifyRecords(pcrTaskCosDivData);
	    var pcrTaskResultDivData = $("#pcrTaskResultdiv").data("pcrTaskResultGrid");
		document.getElementById('pcrTaskResultJson').value = commonGetModifyRecords(pcrTaskResultDivData);

	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/pcr/pcrTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/pcr/pcrTask/copyPcrTask.action?id=' + $("#pcrTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#pcrTask_id").val() + "&tableId=pcrTask");
//}
$("#toolbarbutton_status").click(function(){
//	var pcrResultGrid = pcrTaskResultGrid.getStore();
//	var pcrItemGrid = pcrTaskItemGrid.getStore();
//	var num = 0;
//	for(var i= 0; i<pcrItemGrid.getCount();i++){
//		if(pcrResultGrid.getCount()==0){
//			message("请完成实验！");
//			return;
//		}
//		for(var j=0;j<pcrResultGrid.getCount();j++){
//			if(pcrItemGrid.getAt(i).get("ySampleCode")==pcrResultGrid.getAt(j).get("ySampleCode")){
//				j=pcrResultGrid.getCount();
//			}else{
//				num=num+1;
//				if(num==pcrResultGrid.getCount()){
//					num=0;
//					message("请完成实验！");
//					return;
//				}
//			}
//		}
//	}
	commonChangeState("formId=" + $("#pcrTask_id").val() + "&tableId=PcrTask");
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#pcrTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#pcrTask_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#pcrTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'PCR扩增',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/pcr/pcrTask/showPcrTaskItemList.action", {
				id : $("#pcrTask_id").val()
			}, "#pcrTaskItempage");
load("/experiment/pcr/pcrTask/showPcrTaskTemplateList.action", {
				id : $("#pcrTask_id").val()
			}, "#pcrTaskTemplatepage");
load("/experiment/pcr/pcrTask/showPcrTaskReagentList.action", {
				id : $("#pcrTask_id").val()
			}, "#pcrTaskReagentpage");
load("/experiment/pcr/pcrTask/showPcrTaskCosList.action", {
				id : $("#pcrTask_id").val()
			}, "#pcrTaskCospage");
load("/experiment/pcr/pcrTask/showPcrTaskResultList.action", {
				id : $("#pcrTask_id").val()
			}, "#pcrTaskResultpage");

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	//调用模板
	function TemplateFun(){
		var type="doPcr";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
	
	function setTemplateFun(rec){
		if($("#pcrTask_acceptUser_name").val()==""){
			document.getElementById('pcrTask_acceptUser').value=rec.get('acceptUser-id');
			document.getElementById('pcrTask_acceptUser_name').value=rec.get('acceptUser-name');
		}
		var code=$("#pcrTask_template").val();
		
		if(code==""){
					document.getElementById('pcrTask_template').value=rec.get('id');
					document.getElementById('pcrTask_template_name').value=rec.get('name');
					var win = Ext.getCmp('TemplateFun');
					if(win){win.close();}
					var id=rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {
								var ob = pcrTaskTemplateGrid.getStore().recordType;
								pcrTaskTemplateGrid.stopEditing();
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("code",obj.code);
									p.set("stepName",obj.name);
									
									p.set("note",obj.note);
									pcrTaskTemplateGrid.getStore().add(p);							
								});
								
								pcrTaskTemplateGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = pcrTaskReagentGrid.getStore().recordType;
								pcrTaskReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									pcrTaskReagentGrid.getStore().add(p);							
								});
								
								pcrTaskReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = pcrTaskCosGrid.getStore().recordType;
								pcrTaskCosGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									pcrTaskCosGrid.getStore().add(p);							
								});			
								pcrTaskCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null);

			
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
						var ob1 = pcrTaskTemplateGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id"); 
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/pcr/pcrTask/delPcrTaskTemplateOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null);
								}else{								
									pcrTaskTemplateGrid.store.removeAll();
								}
							}
							pcrTaskTemplateGrid.store.removeAll();
		 				}

						var ob2 = pcrTaskReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");

								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/pcr/pcrTask/delPcrTaskReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
								}else{
									pcrTaskReagentGrid.store.removeAll();
								}
							}
							pcrTaskReagentGrid.store.removeAll();
		 				}
						//=========================================
						var ob3 = pcrTaskCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");
								
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/pcr/pcrTask/delPcrTaskCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null); 
								}else{
									pcrTaskCosGrid.store.removeAll();
								}
							}
							pcrTaskCosGrid.store.removeAll();
		 				}
		 				document.getElementById('pcrTask_template').value=rec.get('id');
						document.getElementById('pcrTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = pcrTaskTemplateGrid.getStore().recordType;
									pcrTaskTemplateGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("stepName",obj.name);
										
										p.set("note",obj.note);
										pcrTaskTemplateGrid.getStore().add(p);							
									});
									
									pcrTaskTemplateGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = pcrTaskReagentGrid.getStore().recordType;
									pcrTaskReagentGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tReagent",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("oneNum",obj.num);
										p.set("note",obj.note);
										pcrTaskReagentGrid.getStore().add(p);							
									});
									
									pcrTaskReagentGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = pcrTaskCosGrid.getStore().recordType;
									pcrTaskCosGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tCos",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("temperature",obj.temperature);
										p.set("speed",obj.speed);
										p.set("time",obj.time);
										p.set("note",obj.note);
										pcrTaskCosGrid.getStore().add(p);							
									});			
									pcrTaskCosGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						}
					}

}
	
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	function ckcrk(){
		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = pcrTaskResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message("有样本未提交，不能初始化！");
						return;
					}
				}
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");
								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message("回滚失败！");
							}
						}, null);
					}
					
				}
			}
		});
	}
	
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '保存'
			});
		item.on('click', ckcrk2);
		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);
		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "PcrTask",id : $("#pcrTask_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
			} else {
				message("办理回滚失败！");
			}
		}, null);
	}
	var loadtestUser;
	//选择实验组用户
	function testUser(){
		var gid=$("#pcrTask_acceptUser").val();
		if(gid!=""){
			var options = {};
			options.width = 500;
			options.height = 500;
			var confirm=biolims.common.confirm;
			loadtestUser=loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				confirm : function() {
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						$("#pcrTask_reciveUser").val(selectRecord[0].get("user-id"));
						$("#pcrTask_reciveUser_name").val(selectRecord[0].get("user-name"));
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
		}else{
			message("请选择实验组");
		}
		
	}
	function setUserGroupUser(){
		var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
				$("#pcrTask_reciveUser").val(selectRecord[0].get("user-id"));
				$("#pcrTask_reciveUser_name").val(selectRecord[0].get("user-name"));
		}else{
			message(biolims.common.selectYouWant);
			return;
		}
		loadtestUser.dialog("close");
	}
