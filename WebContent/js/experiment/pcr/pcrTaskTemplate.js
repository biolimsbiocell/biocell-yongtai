var pcrTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'pcrTask-id',
		type:"string"
	});
	    fields.push({
		name:'pcrTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤编id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:'步骤名称',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		selectreciveUserFun();
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:'实验员',
		width:20*10,
		editor : testUser
	});
	
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:'开始时间',
		width:20*6
		
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:'结束时间',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : true,
		header:'关联样本',
		width:20*6
	});
	cm.push({
		dataIndex:'pcrTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'pcrTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/pcr/pcrTask/showPcrTaskTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="执行步骤";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/pcr/pcrTask/delPcrTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
			text : '选择实验员',
				handler : selectreciveUserFun
		});
	
	
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : '打印执行单',
		handler : stampOrder
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : '开始执行',
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : '执行结束',
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : '生成结果明细',
		handler : addSuccess
	});
	pcrTaskTemplateGrid=gridEditTable("pcrTaskTemplatediv",cols,loadParam,opts);
	$("#pcrTaskTemplatediv").data("pcrTaskTemplateGrid", pcrTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectreciveUserFun(){
	var win = Ext.getCmp('selectreciveUser');
	if (win) {win.close();}
	var selectreciveUser= new Ext.Window({
	id:'selectreciveUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectreciveUser.close(); }  }]  }) ;  
    selectreciveUser.show(); }
	function setreciveUser(id,name){
		var gridGrid = $("#pcrTaskTemplatediv").data("pcrTaskTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reciveUser-id',id);
			obj.set('reciveUser-name',name);
		});
		var win = Ext.getCmp('selectreciveUser');
		if(win){
			win.close();
		}
	}

	//打印执行单
	function stampOrder(){
		var id=$("#pcrTask_template").val();
		if(id==""){
			message("请先选择模板!");
			return;
		}else{
			var url = '__report=pcrTaskTask.rptdesign&id=' + $("#pcrTask_id").val();
			commonPrint(url);}
	}
	//生成结果明细
	function addSuccess(){	
		
		var num =$("#pcrTask_template").val();
		if(num!=""){
			var setNum = pcrTaskReagentGrid.store;
			var selectRecords = pcrTaskItemGrid.store;
				for(var i=0;i<setNum.getCount();i++){
					setNum.getAt(i).set("sampleNum",selectRecords.getCount());
			}



		
		var getRecord = pcrTaskItemGrid.getSelectionModel().getSelections();
		var selectRecord = pcrTaskTemplateGrid.getSelectionModel().getSelections();
		var selRecord = pcrTaskResultGrid.store;
		if(getRecord.length>0){
				if(selectRecord.length>0){
					$.each(selectRecord, function(i, obj) {
						var isRepeat = true;
						var codes = obj.get("sampleCodes");
						var scode = new Array();
						scode = codes.split(",");
						for(var i1=0; i1<scode.length; i1++){
							for(var j1=0;j1<selRecord.getCount();j1++){
								var getv = scode[i1];
								var setv = selRecord.getAt(j1).get("sampleCode");
								if(getv == setv){
									isRepeat = false;
									message("有重复的数据，请重新选择！");
									break;					
								}
							}
						}
						if(isRepeat){
								$.each(getRecord,function(a,b){
										
											
												var ob = pcrTaskResultGrid.getStore().recordType;
												pcrTaskResultGrid.stopEditing();
												var p = new ob({});
												p.isNew = true;
												p.set("tempId",b.get("tempId"));
												p.set("name",b.get("name"));
												p.set("sampleCode",b.get("sampleCode"));
												p.set("note",b.get("note"));
												p.set("patientName",b.get("patientName"));
												p.set("productId",b.get("productId"));
												p.set("productName",b.get("productName"));
												p.set("inspectDate",b.get("inspectDate"));
												p.set("acceptDate",b.get("acceptDate"));
												p.set("idCard",b.get("idCard"));
												p.set("phone",b.get("phone"));
												p.set("orderId",b.get("orderId"));
												p.set("sequenceFun",b.get("sequenceFun"));
												p.set("reportDate",b.get("reportDate"));
												//p.set("submit","0");
												p.set("result","1");
												//p.set("nextFlow","0");
												p.set("volume",100);
												p.set("unit",b.get("unit"));
												p.set("rowCode",b.get("rowCode"));
												p.set("colCode",b.get("colCode"));
												p.set("counts",b.get("counts"));
												p.set("contractId",b.get("contractId"));
												p.set("projectId",b.get("projectId"));
												p.set("orderType",b.get("orderType"));
												p.set("jkTaskId",b.get("jkTaskId"));
												p.set("classify",b.get("classify"));
												p.set("sampleType",obj.get("sampleType"));
												ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
													model : "PcrTask",productId:b.get("productId")
												}, function(data) {
													if (data.success) {
														p.set("nextFlowId",data.data.dnextId);
														p.set("nextFlow",data.data.dnextName);
													}
												}, null);
												message("生成结果成功！");
												pcrTaskResultGrid.getStore().add(p);
												pcrTaskResultGrid.startEditing(0,0);
											
											
										
								});
							
						}
					});
				}else{
					message("请先勾选最后一步！");
				}
		}else{
			message("请选择PCR扩增明细！");
		}

		}else{
			message("请选择实验模板！");
		}
	}

	//获取开始时的时间
	function getStartTime(){
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=pcrTaskTemplateGrid.getSelectionModel();
		var setNum = pcrTaskReagentGrid.store;
		var selectRecords=pcrTaskItemGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
				for(var i=0; i<setNum.getCount();i++){
					var num = setNum.getAt(i).get("itemId");
					if(num==obj.get("code")){
						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
					}
				}
			});
		}else{
			message("请先选择数据！");
		}
		
		//将所选的样本，放到关联样本
		var selRecord=pcrTaskTemplateGrid.getSelectRecord();
		var codes = "";
			$.each(selectRecords.getSelections(), function(i, obj) {
				codes += obj.get("sampleCode")+",";
			});
			$.each(selRecord, function(i, obj) {
				obj.set("sampleCodes", codes);
			});
		
	}
	//获取停止时的时间
	function getEndTime(){
			var setRecord=pcrTaskItemGrid.store;
			var d = new Date();
			var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
			var selectRecord=pcrTaskTemplateGrid.getSelectionModel();
			var getIndex = pcrTaskTemplateGrid.store;
			var getIndexs = pcrTaskTemplateGrid.getSelectionModel().getSelections();
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					obj.set("endTime",str);	
					//将步骤编号，赋值到明细表
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i=0; i<setRecord.getCount(); i++){
						for(var j=0; j<scode.length; j++){
							if(scode[j]==setRecord.getAt(i).get("sampleCode")){
								setRecord.getAt(i).set("stepNum",obj.get("code"));
							}
						}
					}
					//将当前行的关联样本传到下一行
					getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
				});
			}
			
			
	}
	
