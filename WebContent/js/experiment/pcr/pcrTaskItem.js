var pcrTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'pcrTask-id',
		type:"string"
	});
	    fields.push({
		name:'pcrTask-name',
		type:"string"
	});
	   fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'addVolume',
		type:"string"
	});
	   fields.push({
		name:'sumVolume',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'jkTaskId',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
		name:'dataType',
		type:"string"
	});
	   fields.push({
		name:'dataNum',
		type:"string"
	});
	   fields.push({
		name:'productNum',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'tempId',
			type:"string"
		}); 
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:'序号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'index',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'处理结果',
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : false,
		header:'步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目名称',
		width:20*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : false,
		header:'取样日期',
		width:20*6,
		
		renderer: formatDate
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:'接收日期',
		width:20*6,
		
		renderer: formatDate
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : false,
		header:'检测方法',
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6,
		
		renderer: formatDate
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'pcrTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10
	});
	cm.push({
		dataIndex:'pcrTask-name',
		hidden : false,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本用量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : false,
		header:'补充体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : false,
		header:'总体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'unit',
		hidden : false,
		header:'单位',
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'jkTaskId',
		hidden : true,
		header:'建库任务单编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'区分临床还是科技服务 0 临床 1 科技服务',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataType',
		hidden : true,
		header:'数据类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataNum',
		hidden : true,
		header:'数据量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productNum',
		hidden : true,
		header:'中间产物数量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/pcr/pcrTask/showPcrTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="PCR扩增明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/pcr/pcrTask/delPcrTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				pcrTaskItemGrid.getStore().commitChanges();
				pcrTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	pcrTaskItemGrid=gridEditTable("pcrTaskItemdiv",cols,loadParam,opts);
	$("#pcrTaskItemdiv").data("pcrTaskItemGrid", pcrTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectpcrTaskFun(){
	var win = Ext.getCmp('selectpcrTask');
	if (win) {win.close();}
	var selectpcrTask= new Ext.Window({
	id:'selectpcrTask',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectpcrTask.close(); }  }]  }) });  
    selectpcrTask.show(); }
	function setpcrTask(rec){
		var gridGrid = $("#pcrTaskItemdiv").data("pcrTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('pcrTask-id',rec.get('id'));
			obj.set('pcrTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectpcrTask')
		if(win){
			win.close();
		}
	}
	function selectpcrTaskDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/PcrTaskSelect.action?flag=pcrTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selpcrTaskVal(this);
				}
			}, true, option);
		}
	var selpcrTaskVal = function(win) {
		var operGrid = pcrTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#pcrTaskItemdiv").data("pcrTaskItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('pcrTask-id',rec.get('id'));
				obj.set('pcrTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
