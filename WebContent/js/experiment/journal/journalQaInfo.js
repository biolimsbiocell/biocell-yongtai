var journalQaInfoTable;
var oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编码",
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "name",
		"title": "描述",
		"width": "300px",
		"className":"textarea",
		"createdCell": function(td,date) {
			$(td).attr("saveName", "name");
		}
	})
	colOpts.push({
		"title": "上传附件",
		"width": "40px",
		"data": null,
		"createdCell": function(td, data) {},
		"render": function() { 
			return '<input type="button" value="上传附件" onClick="javascript:fileUp1(this);" >' +
				'<input type="button" value="查看附件" onClick="javascript:fileView1(this);">'

		},
	});
	colOpts.push({
		"data": "createUser-id",
		"title": "创建人ID",
		"visible": false,
		"createdCell": function(td,date) {
			$(td).attr("saveName", "createUser-id");
		}
	})
	colOpts.push({
		"data": "createUser-name",
		"title": "创建人",
		"createdCell": function(td,date,rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	})
	colOpts.push({
		"data": "createDate",
		"title": "创建时间",
		"createdCell": function(td,date) {
			$(td).attr("saveName", "createDate");
		}
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#main"));
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#main"),
					"/experiment/journal/journalQaInfo/delJournalQaInfo.action");
			}
	});
	tbarOpts.push({
		text: "保存",
		action: function() {
			saveItem();
		}
	});
	tbarOpts.push({
		text : biolims.common.editwindow,
		action : function() {
			editItemLayer($("#main"))
		}
	});
	var options = table(true, "","/experiment/journal/journalQaInfo/showJournalQaInfoTableJson.action",
			colOpts, tbarOpts)
	journalQaInfoTable = renderData($("#main"), options);
//	$('#main').on('init.dt', function() {
//		recoverSearchContent(journalQaInfoTable);
//	})
	journalQaInfoTable.on('draw', function() {
		oldChangeLog = journalQaInfoTable.ajax.json();
	});
});
// 新建
function add() {
	window.location = window.ctx +
		"/experiment/journal/journalQaInfo/editJournalQaInfo.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/experiment/journal/journalQaInfo/editJournalQaInfo.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/experiment/journal/journalQaInfo/viewJournalQaInfo.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.journalQaInfo.id
		});
	
	fields.push({
		"type":"table",
		"table":journalQaInfoTable
	});
	return fields;
}
function saveItem(){
	var ele = $("#main");
	var changeLog = biolims.sanger.testResult + "：";
	var data = saveItemjson(ele);
	if (!data) {
		return false;
	}
	changeLog = getChangeLog1(data, ele, changeLog);
	var changeLogs = "";
	if (changeLog != "" + "：") {
		changeLogs = changeLog;
	}
	top.layer.load(4, {
		shade : 0.3
	});
	$.ajax({
		type : 'post',
		url : '/experiment/journal/journalQaInfo/saveJournalQaInfo.action',
		data : {
			dataJson : data,
			changeLog:changeLog
		},
		success : function(data) {
			var data = JSON.parse(data)
			if (data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			}
		}
	})
}
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag = true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断男女并转换为数字
			json[k] = $(tds[j]).text();
			if (k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue
			}
		}
		data.push(json);
	});
	if (flag) {
		return JSON.stringify(data);
	} else {
		return false;
	}
}
function fileUp1(that){
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	$("#uploadFileValx").fileinput({
		uploadUrl: ctx + "/attachmentUpload?useType=1&modelType=JournalQaInfo&contentId=" + id,
		uploadAsync: true,
		language: 'zh',
		showPreview: true,
		enctype: 'multipart/form-data',
		elErrorContainer: '#kartik-file-errors',
	});
	$("#uploadFilex").modal("show");
}
function fileView1(that){
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: ["650px", "500px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=JournalQaInfo&id=" + id,
		cancel: function(index, layero) {
			top.layer.close(index)
		}
	})
}

function getChangeLog1(data, ele, changeLog) {
	debugger
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID为"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title +'"'+ biolims.common.from+'"' + vv[k] + '"'+biolims.common.to+'"' + v[k] + '";';					}
				}
				return false;
			}
		});
	});
	return changeLog;
}