var journalQaInfoTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.journalQaInfo.id,
	});
	
	var options = table(true, "","/com/biolims/experiment/journal/journalQaInfo/showJournalQaInfoTableJson.action",
	 fields, null)
	journalQaInfoTable = renderData($("#addJournalQaInfoTable"), options);
	$('#addJournalQaInfoTable').on('init.dt', function() {
		recoverSearchContent(journalQaInfoTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.journalQaInfo.id
		});
	
	fields.push({
		"type":"table",
		"table":journalQaInfoTable
	});
	return fields;
}
