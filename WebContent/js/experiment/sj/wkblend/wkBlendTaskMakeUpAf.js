var wkBlendTaskMakeUpAfTab, oldChangeLog;
var isSeparate = $("#wkBlendTask_template_isSeparate").val();
var flg = false;
if(isSeparate == 1) {
	flg = true;
}

//已排板样本
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "dicSampleTypeId",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "dicSampleTypeId");
		},
	})
	colOpts.push({
		"data": "dicSampleTypeName",
		"title": biolims.common.dicSampleTypeName+'<img src="/images/required.gif"/>',
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleTypeName");
			$(td).attr("dicSampleTypeId", rowData['dicSampleTypeId']);
		}
	})
	colOpts.push({
		"data": "tempId",
		"title": biolims.common.tempId,
		"visible": false,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "tempId");
		},
	})
	colOpts.push({
		"data": "productNum",
		"title": biolims.common.productNum+'<img src="/images/required.gif"/>',
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "productNum");
		},
	})
	colOpts.push({
		"data": "indexa",
		"title": "INDEX",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "indexa");
		},
	})
	colOpts.push({
		"data": "concentration",
		"title": biolims.common.concentration,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "concentration");
		},
	})
	colOpts.push({
		"data": "volume",
		"title": biolims.common.volume,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "volume");
		},
	})
	colOpts.push({
		"data": "sumTotal",
		"title": biolims.common.sumNum,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sumTotal");
		},
	})
	colOpts.push({
		"data": "mixAmount",
		"title": biolims.pooling.mixAmount,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "mixAmount");
		},
	})
	colOpts.push({
		"data": "mixVolume",
		"title": biolims.pooling.mixVolume,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "mixVolume");
		},
	})
	colOpts.push({
		"data": "blendCode",
		"title": biolims.common.mixNumber,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "blendCode");
		},
	})
	colOpts.push({
		"data": "posId",
		"title": biolims.common.location,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})
	colOpts.push({
		"data": "counts",
		"title": biolims.common.counts,
		//"className": "edit",
		"createdCell": function(td, data, rowdata) {
		$(td).parent("tr").css("background-color", rowdata.color);
			$(td).attr("saveName", "counts");
		},
	})
	colOpts.push({
		"data": "chromosomalLocation",
		"title": biolims.sanger.chromosomalLocation,
		"visible": flg,
		"createdCell": function(td) {
			$(td).attr("saveName", "chromosomalLocation");
		}
	})
	
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#wkBlendTask_state").text()!="Complete"){
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#wkBlendTaskMakeUpAfdiv"),
				"/experiment/sj/wkblend/wkBlendTask/delWkBlendTaskItem.action","文库混合删除已排版样本：",$("#wkBlendTask_id").text());
		}
	});
	tbarOpts.push({
		text: biolims.common.Editplay,
		action: function() {
			editItemLayer($("#wkBlendTaskMakeUpAfdiv"))
		}

	})
	tbarOpts.push({
		text: biolims.common.plateAgain,
		action: function() {
			plateLayoutAgain($("#wkBlendTaskMakeUpAfdiv"),
				"/experiment/sj/wkblend/wkBlendTask/delWkBlendTaskItemAf.action")
				removeBlack();
		}
	})
	tbarOpts.push({
		text: biolims.common.productType,
		action: function() {
			addSampleType();
		}
	})
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveWkBlendTaskMakeUpAfTab();
		}
	})
	tbarOpts.push({
		text : biolims.common.productNum,
		action : function() {
			var rows = $("#WkBlendTaskMakeUpAfdiv .selected");
			var length = rows.length;
			if (!length) {
				top.layer.msg(biolims.common.pleaseSelectData);
				return false;
			}
			top.layer.open({
				type : 1,
				title : biolims.common.productNum,
				content : $('#batch_data').html(),
				area:[document.body.clientWidth-600,document.body.clientHeight-200],
				btn: biolims.common.selected,
				yes : function(index, layer) {
					var productNum = $("#productNum",parent.document).val();
					rows.addClass("editagain");
					rows.find("td[savename='productNum']").text(productNum);
					top.layer.close(index);
				}
			});
		}
	})}
	var wkBlendTaskMakeUpAfOps = table(true, wkBlendTask_id, "/experiment/sj/wkblend/wkBlendTask/showWkBlendTaskItemAfTableJson.action", colOpts, tbarOpts);
	wkBlendTaskMakeUpAfTab = renderData($("#wkBlendTaskMakeUpAfdiv"), wkBlendTaskMakeUpAfOps);
	//选择数据并提示
	wkBlendTaskMakeUpAfTab.on('draw', function() {
		var index = 0;
		$("#wkBlendTaskMakeUpAfdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = wkBlendTaskMakeUpAfTab.ajax.json();
		
	});
});
//添加样本类型
function addSampleType() {
	var rows = $("#wkBlendTaskMakeUpAfdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.selectSampleType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/sample/dicSampleType/showDialogDicSampleTypeTable.action", ''],
		yes: function(index, layer) {
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td").eq(
				0).text();
			rows.addClass("editagain");
			rows.find("td[savename='dicSampleTypeName']").attr(
				"dicSampleType-id", id).text(type);
			rows.find("td[savename='dicSampleTypeId']").text(id);

			top.layer.close(index)
		},
	})
}

function plateLayoutAgain(ele, urll) {
	var arr = [];
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.confirm(biolims.common.rearrange + length + biolims.common.record,{icon: 3, title:biolims.common.prompt,
		btn:biolims.common.selected
	 }, function(index) {
		top.layer.close(index);
		rows.each(function(i, val) {
			var id = $(val).find("input[type=checkbox]").val();
			arr.push(id);
		});
		if(arr.length) {
			$.ajax({
				type: "post",
				data: {
					ids: arr
				},
				url: ctx + urll,
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						wkBlendTaskMakeUpAfTab.ajax.reload();
						wkBlendTaskMakeUpTab.ajax.reload();
						$(".mysample").each(function(i, div) {
							var id = $(div).attr("sid");
							arr.forEach(function(val, j) {
								if(id == val) {
									$(div).removeAttr("sid").removeAttr("title").removeClass().css("background-color", "#fff");
								}
							});
						});
					}
				}
			});
		}

	});

}
// 保存
function saveWkBlendTaskMakeUpAfTab() {
    var ele=$("#wkBlendTaskMakeUpAfdiv");
	var changeLog = "文库混合：";
	var data = saveItemjson(ele);
	if(!data){
		return false;
	}
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs = "";
	if(changeLog != "文库混合："){
		changeLogs = changeLog
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/experiment/sj/wkblend/wkBlendTask/saveMakeUp.action',
		data: {
			id: wkBlendTask_id,
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	var flag=true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
			//if(k == "dicSampleTypeName") {
			//	json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
			//	continue;
			//}
			json[k] = $(tds[j]).text();
			if(k=="dicSampleTypeName"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg(biolims.common.pleaseFillDic);
					return false;
				}
			}
			if(k=="productNum"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg(biolims.common.pleaseFillDicNum);
					return false;
				}
			}
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	if(flag){
		return JSON.stringify(data);
	}else{
		return false;
	}
}
//混样
if($("#isBlend").val() == 0) {
	$("#isBlendBtn").hide();
}else{
	 $(window).scroll(function(event){
		if($(window).scrollTop()>400){
			$("#isBlendBtn").fadeOut();
		}else{
			$("#isBlendBtn").fadeIn();
		}
    });
}

function compoundSample() {
	var rows = $("#wkBlendTaskMakeUpAfdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var color = {
			r: 0,
			g: 0,
			b: 0
		},
		min = 380,
		max = 720,
		minHex = parseInt('99', 16),
		maxHex = parseInt('DD', 16);

	while(true) {
		color = getRGB(minHex, maxHex);
		if((color.r + color.g + color.b) >= min && (color.r + color.g + color.b) <= max) {
			break;
		}
	}
	var color = '#' + toHex(color.r) + toHex(color.g) + toHex(color.b);
	rows.attr("background", color).addClass("editagain").css("background-color", color);
	var blendCode = $("#blendCode").val();
	if(!blendCode) {
		$.ajax({
			type: "post",
			url: ctx + "/experiment/sj/wkblend/wkBlendTask/generateBlendCode.action",
			data: {
				id: wkBlendTask_id,
			},
			async: false,
			success: function(data) {
				if(data == "null") {
					blendCode = 0;
				} else {
					blendCode = data;
				}
			}
		});
	}
	rows.each(function(i, v) {
		$(v).children("td[savename='blendCode']").text(parseInt(blendCode) + 1);
	});
	$("#blendCode").val(parseInt(blendCode) + 1);
}
/* 
 * 
 * 描述: 随机生成一个背景色。还需要限制一下颜色区间值，最小值#999999，rgb(153,153,153)，最大值#DDDDDD，rgb(221,221,221)。将RGB三个值相加控制在580-720之间，这样当r=153时，g和b的值就得在214以上。这就是说rgb的每个随机值可以在153-221之间，但是最终的三个值由其他一个或两个值牵制着，颜色就即随机又不会太暗或太亮。
 */
function getRGB(min, max) {
	return {
		r: min + Math.round(Math.random() * 1000) % (max - min),
		g: min + Math.round(Math.random() * 1000) % (max - min),
		b: min + Math.round(Math.random() * 1000) % (max - min)
	};
}

function toHex(val) {
	var hex = '00';
	if(val) {
		hex = parseInt(val).toString(16);
		if(hex.length == 1) {
			hex = '0' + hex;
		}
	}

	return hex;
}
/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function removeBlack(){
	$("#plateModal").find(
			".mysample[sid='" + this.value + "']").css(
			"border", "1px solid gainsboro");
}