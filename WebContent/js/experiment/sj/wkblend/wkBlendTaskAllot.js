var wkBlendTaskAllotTab;
$(function() {
	var colOpts=[]
	colOpts.push({
		"data": "id",
		"visible": false,
		"title": biolims.common.id,
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
	})
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.sampleType,
	})
	colOpts.push({
		"data": "concentration",
		"title": biolims.wk.wkConcentration,
	})
	colOpts.push({
		"data": "volume",
		"title": biolims.wk.wkVolume,
	})
	colOpts.push({
		"data": "sumTotal",
		"title": biolims.wk.wkTotal,
	})
	colOpts.push({
		"data": "indexa",
		"title": "INDEX",
	})
	colOpts.push({
		"data": "insertSize",
		"title": biolims.common.wKsamll,
	})
	colOpts.push({
		"data": "posId",
		"title": biolims.common.location ,
	})
	colOpts.push({
		"data": "counts",
		"title": biolims.common.counts,
	})
	colOpts.push({
		"data": "sampleInfo-project-id",
		"title":biolims.common.projectId,
	})
	colOpts.push({
		"data": "scopeName",
		"title": biolims.purchase.costCenter,
		"createdCell": function(td,data,rowdata) {
			var typeValue=rowdata["sampleInfo-changeType"];
			console.log(rowdata);
			if(typeValue=="0"){
				$(td).parent("tr").children("td").eq(2).css('background-color','EAB9A8');
				console.log($(td).parent("tr").children("td"));
			}else if(typeValue=="1"){
				$(td).parent("tr").children("td").eq(2).css('background-color','30B573');
			}else if(typeValue=="2"){
				$(td).parent("tr").children("td").eq(2).css('background-color','8FAAC6');
			}
		}
	})
	colOpts.push({
		"data": "sampleInfo-changeType",
		"title": biolims.purchase.costCenter,
		"visible":false
	})
	var tbarOpts=[]
	tbarOpts.push({
		text : '<i class="fa fa-exchange"></i> ' + biolims.dna.checkPlateNo,
		action : function() {
			checkCounts();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	var options = table(true,null,"/experiment/sj/wkblend/wkBlendTask/showWkBlendTaskTempTableJson.action",colOpts, tbarOpts)
	//渲染样本的table
	wkBlendTaskAllotTab=renderData($("#main"),options);
	
	
	//提示样本选中数量
	var selectednum=$(".selectednum").text();
		if(!selectednum){
			selectednum=0;
	}
	wkBlendTaskAllotTab.on('draw', function() {
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			$(".selectednum").text(index+parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	//选中模板的实验组的实验员显示隐藏
	if($(".sopChosed").length){
		var userGroup=$(".sopChosed").attr("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).show();
			}else{
				$(v).hide();
			}
		});
	}
	//sop和实验员的点击操作
	sopAndUserHandler(selectednum);
	//保存操作
	allotSave();
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#wkBlendTask_state").text()=="Complete"){
		settextreadonly();
		$("#save").hide()
	}
});
//sop和实验员的点击操作
function sopAndUserHandler(selectednum) {
	//执行sop点击选择操作
	$(".sopLi").click(function() {
		var length = $(".selected").length;
		$(this).addClass("sopChosed").siblings("li").removeClass("sopChosed");
		$(".label-warning").removeClass("selectednum").html("");
		$(this).children(".label-warning").addClass("selectednum").text(length+parseInt(selectednum));
		if($(this).children(".label-primary").text()) {
			if($("#maxNum").val() == "0") {
				$("#maxNum").val("1");
			}
		} else {
			$("#maxNum").val("0");
		}
		//显示隐藏模板里配置的实验员
		var userGroup=this.getAttribute("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).slideDown();
			}else{
				$(v).slideUp().children("img").removeClass("userChosed");
			}
		});
	});
	//执行实验员点击选择操作
	$(".userLi").click(function() {
		var length = $(".selected").length;
			var img = $(this).children("img");
			if(img.hasClass("userChosed")) {
				img.removeClass("userChosed");
			} else {
				img.addClass("userChosed");
			}		
	});
}
//保存操作
function allotSave() {
	$("#save").click(function() {
		var changeLog = "NGS文库混合-待处理样本：";
		if(!$(".sopChosed").length) {
			top.layer.msg(biolims.common.selectTaskModel);
			return false;
		}
		if(!$(".userChosed").length) {
			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
			return false;
		}
		//获得选中样本的ID
		var sampleId = [];
		$(".selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
		});
		//获得选中实验员的ID
		var userId = [];
		$(".userChosed").each(function(i, val) {
			userId.push($(val).parent("li").attr("userid"));
		});
		changeLog = getChangeLog(userId, changeLog);
		//判断是否修改
		var changeLogs="";
		if(changeLog !="NGS文库混合-待处理样本："){
			changeLogs=changeLog;
		}
		
		//拼接主表的信息
		var main = {
			id: $("#wkBlendTask_id").text(),
			name: $("#wkBlendTask_name").val(),
			"createUser-id": $("#wkBlendTask_createUser").attr("userId"),
			createDate: $("#wkBlendTask_createDate").text(),
			state: $("#wkBlendTask_state").attr("state"),
			stateName: $("#wkBlendTask_state").text(),
			sampleNum:$(".selectednum").text(),
			maxNum:$("#maxNum").val()
		};
		var data = JSON.stringify(main);
		//拼接保存的data
		var info = {
			temp: sampleId,
			template: $(".sopChosed").attr("sopid"),
			user: userId.join(","),
			main: data,
			logInfo: changeLogs
		}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
		type : 'post',
		url:ctx+"/experiment/sj/wkblend/wkBlendTask/saveAllot.action",
		data:info,
		success:function(data){
			var data = JSON.parse(data);
			if(data.success){	
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx
			+ '/experiment/sj/wkblend/wkBlendTask/editWkBlendTask.action?id=' + data.id;
			}
		}
		})
	});
}
//下一步操作
$("#next").click(function () {
	var wkBlendTask_id=$("#wkBlendTask_id").text();
	if(wkBlendTask_id=="NEW"){
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/sj/wkblend/wkBlendTask/showWkBlendTaskItemTable.action?id="+wkBlendTask_id+"&bpmTaskId="+$("#bpmTaskId").val();
});
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"txt":biolims.common.projectId,
		"type": "input",
		"searchName": "sampleInfo-project-id",
	}, {
		"type" : "table",
		"table" : wkBlendTaskAllotTab
	} ];
}
//核对板号
function checkCounts() {
	top.layer
			.open({
				title : biolims.common.checkCode,
				type : 1,
				area : [ "30%", "60%" ],
				btn : biolims.common.selected,
				btnAlign : 'c',
				content : '<textarea id="checkCounts" class="form-control" style="width: 99%;height: 99%;"></textarea>',
				yes : function(index, layer) {
					var arr = $("#checkCounts").val().split("\n");
					wkTaskAllotTab.settings()[0].ajax.data = {
						"counts" : arr
					};
					wkTaskAllotTab.ajax.reload();
					top.layer.close(index);
				},
			});

}
//biolims.common.selected
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layer) {
			var arr = $("#checkCode",parent.document).val().split("\n");
			wkBlendTaskAllotTab.settings()[0].ajax.data = {"codes":arr};
			wkBlendTaskAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});

}
//获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { //获取待处理样本的修改日志
	var sampleNew = [];
	$(".selected").each(function(i, val) {
		sampleNew.push($(val).children("td").eq(1).text());
	});
	changeLog += '"待处理样本"新增有：' + sampleNew + ";";
	//获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	//获取模板sop的修改日志
	if($(".sopChosed").attr("sopid") != $("#sopChangelogId").val()) {
		changeLog += '实验模板:由"' + $("#sopChangelogName").val() + '"变为"' + $(".sopChosed").children('.text').text() + '";';
		changeLog += '样本数量:由"' + $("#sopChangelogSampleNum").val() + '"变为"' + $(".sopChosed").children('.badge').text() + '";';
	}
	//获取实验员的修改日志
	var oldUserId = [],
		userName = [],
		oldUserName = [];
	$('input[class="changelogUserid"]').each(function(i, v) {
		oldUserId.push(v.value);
	});
	if(userId != oldUserId) {
		$(".userChosed").each(function(i, val) {
			userName.push($(val).next("a").text());
		});
		$('input[class="changelogUsername"]').each(function(i, v) {
			oldUserName.push(v.value);
		});
		changeLog += '实验员:由"' + oldUserName + '"变为"' + userName + '";';
	}

	return changeLog;
}
