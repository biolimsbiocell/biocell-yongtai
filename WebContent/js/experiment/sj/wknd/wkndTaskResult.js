var wkndTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'sjfz',
		type:"string"
	});
	   fields.push({
		name:'yhhzh',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'sjlane',
		type:"string"
	});
	   fields.push({
		name:'xylr',
		type:"string"
	});
	   fields.push({
		name:'clr',
		type:"string"
	});
	   fields.push({
		name:'flr',
		type:"string"
	});
	   fields.push({
		name:'xzlr',
		type:"string"
	});
	   fields.push({
		name:'qtlr',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'yzlnd',
		type:"string"
	});
	   fields.push({
		name:'ymend',
		type:"string"
	});
	   fields.push({
		name:'sox',
		type:"string"
	});
	   fields.push({
		name:'qy',
		type:"string"
	});
	   fields.push({
		name:'js',
		type:"string"
	});
	   fields.push({
		name:'gz',
		type:"string"
	});
	   fields.push({
		name:'tzzlnd',
		type:"string"
	});
	   fields.push({
		name:'tzmend',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'wkndTask-id',
		type:"string"
	});
	   fields.push({
			name:'wkndTask-name',
			type:"string"
		});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	   fields.push({
			name:'nextFlowId',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : true,
		header:'富集文库',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'sjfz',
		hidden : false,
		header:'上机分组',
		width:20*6
	});
	cm.push({
		dataIndex:'yhhzh',
		hidden : false,
		header:'预上机混合组号',
		width:20*7
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : true,
		header:'混合组号',
		width:20*6
	});
	cm.push({
		dataIndex:'sjlane',
		hidden : true,
		header:'上机lane号',
		width:20*6
	});
	
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:'新鲜组织文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:'其他文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'yzlnd',
		hidden : false,
		header:'原质量浓度（ng/ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ymend',
		hidden : false,
		header:'原摩尔浓度（nm）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:'文库体积(ul)',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : false,
		header:'目标摩尔浓度（nM）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sox',
		hidden : false,
		header:'下调或上调（%）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qy',
		hidden : false,
		header:'取样（ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'js',
		hidden : false,
		header:'加水或干燥（ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'tzzlnd',
		hidden : false,
		header:'调整后质量浓度（ng/ul）',
		width:20*8,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzmend',
		hidden : false,
		header:'调整后摩尔浓度（nm）',
		width:20*7,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'gz',
		hidden : true,
		header:'干燥（min）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xylr',
		hidden : false,
		header:'测序类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : false,
		header:'测序平台',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : false,
		header:'测序读长',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkndTask-id',
		hidden : true,
		header:'相关主表',
		width:20*6
	});
	cm.push({
		dataIndex:'wkndTask-name',
		hidden : true,
		header:'相关主表',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/wknd/wkndTask/showWkndTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="文库浓度调整结果";
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/wknd/wkndTask/delWkndTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	
	opts.tbar.push({
		text : "批量下一步",
		handler : function() {
			var selectRecord = wkndTaskResultGrid.getSelectionModel().getSelections();
			if(selectRecord.length>0){
				loadTestNextFlowCob();
			}else{
				message("请选择数据！");
			}
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_result_div"), "批量下一步", null, {
//				"确定" : function() {
//					var records = wkndTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var result = $("#nextFlow").val();
//						var nextFlowId = $("nextFlowId").val();
//						wkndTaskResultGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("nextFlow", result);
//							obj.set("nextFlowId",nextFlowId);
//						});
//						wkndTaskResultGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
		}
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	wkndTaskResultGrid=gridEditTable("wkndTaskResultdiv",cols,loadParam,opts);
	$("#wkndTaskResultdiv").data("wkndTaskResultGrid", wkndTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = wkndTaskResultGrid.getSelectRecord();
	var sequencingPlatform="";
	$.each(records1, function(j, k) {
		sequencingPlatform=k.get("clr");
	});
	
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/showPoolingTaskNextFlow.action?model=WkndTask&sequencingPlatform="+sequencingPlatform, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = wkndTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = wkndTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}	


