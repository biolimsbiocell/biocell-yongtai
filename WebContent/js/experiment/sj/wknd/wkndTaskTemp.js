var wkndTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'sjfz',
		type:"string"
	});
	   fields.push({
		name:'yhhzh',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'xylr',
		type:"string"
	});
	   fields.push({
		name:'clr',
		type:"string"
	});
	   fields.push({
		name:'flr',
		type:"string"
	});
	   fields.push({
		name:'xzlr',
		type:"string"
	});
	   fields.push({
		name:'qtlr',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'yzlnd',
		type:"string"
	});
	   fields.push({
		name:'ymend',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
		name:'tempid',
		type:"string"
	});
	   fields.push({
			name:'state',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : true,
		header:'富集文库',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'sjfz',
		hidden : false,
		header:'上机分组',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:'预混合组号',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:'混合组号',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'yzlnd',
		hidden : false,
		header:'原质量浓度（ng/ul）',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'ymend',
		hidden : false,
		header:'原摩尔浓度（nm）',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:'文库体积（ul）',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : true,
		header:'各组文库片段长度',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:'血液文库比例%',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:'ctdna文库比例%',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:'FFPE文库比例%',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:'新鲜组织文库比例%',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:'其他文库比例%',
		width:20*6,
		
		 
	});
	
	
	
	cm.push({
		dataIndex:'cxlx',
		hidden : false,
		header:'测序类型',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'cxpt',
		hidden : false,
		header:'测序平台',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : false,
		header:'测序读长',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'tempid',
		hidden : true,
		header:'临时表id',
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'状态',
		width:20*6,
		
		 
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/wknd/wkndTask/showWkndTaskTempListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="文库浓度调整临时表";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/wknd/wkndTask/delWkndTaskTemp.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};

	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = wkndTaskTempGrid.getAllRecord();
							var store = wkndTaskTempGrid.store;

							var isOper = true;
							var buf = [];
							wkndTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							wkndTaskTempGrid.getSelectionModel().selectRows(buf);
							if(isOper==false){
								message("样本号核对不符，请检查！");
								
							}else{
								addItem();
							}
							wkndTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	wkndTaskTempGrid=gridEditTable("wkndTaskTempdiv",cols,loadParam,opts);
	$("#wkndTaskTempdiv").data("wkndTaskTempGrid", wkndTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

//添加任务到子表
function addItem(){
	var selectRecord=wkndTaskTempGrid.getSelectionModel();
	var selRecord=wkndTaskItemGrid.store;
	var count=1;
	var max=0;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					return;					
				}
			}
			if(!isRepeat){
			var ob = wkndTaskItemGrid.getStore().recordType;
			wkndTaskItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("tempId",obj.get("id"));
			p.set("sjfz",obj.get("sjfz"));
			p.set("hhzh",obj.get("hhzh"));
			p.set("yzlnd",obj.get("yzlnd"));
			p.set("ymend",obj.get("ymend"));
			p.set("tl",obj.get("tl"));
			p.set("cxlx",obj.get("cxlx"));
			p.set("cxpt",obj.get("cxpt"));
			p.set("cxdc",obj.get("cxdc"));
			p.set("orderNumber",Number(max)+count);
			p.set("state","1");
			wkndTaskItemGrid.getStore().add(p);
			count++;
			wkndTaskItemGrid.startEditing(0, 0);
		}
			
	});
	}else{
		message("请选择样本！");
	}
	
}

