var wkndTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'sjfz',
		type:"string"
	});
	   fields.push({
		name:'yhhzh',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'sjlane',
		type:"string"
	});
	   fields.push({
		name:'xylr',
		type:"string"
	});
	   fields.push({
		name:'clr',
		type:"string"
	});
	   fields.push({
		name:'flr',
		type:"string"
	});
	   fields.push({
		name:'xzlr',
		type:"string"
	});
	   fields.push({
		name:'qtlr',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'yzlnd',
		type:"string"
	});
	   fields.push({
		name:'ymend',
		type:"string"
	});
	   fields.push({
		name:'zjcwgs',
		type:"string"
	});
	   fields.push({
		name:'zjcwlx',
		type:"string"
	});
	    fields.push({
		name:'wkndTask-id',
		type:"string"
	});
	    fields.push({
		name:'wkndTask-name',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	   fields.push({
			name:'stepNum',
			type:"string"
		});
	   fields.push({
			name:'orderNumber',
			type:"string"
		});  
	   fields.push({
			name:'state',
			type:"string"
		}); 
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		 
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : true,
		header:'富集文库',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sjfz',
		hidden : false,
		header:'上机分组',
		width:20*6
	});
	cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:'预混合组号',
		width:20*6
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:'混合组号',
		width:20*6
	});
	cm.push({
		dataIndex:'yzlnd',
		hidden : false,
		header:'原质量浓度（ng/ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ymend',
		hidden : false,
		header:'原摩尔浓度（nm）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:'文库体积(ul)',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sjlane',
		hidden : true,
		header:'上机lane号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:'血液文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:'ctDNA文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:'FFPE文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:'新鲜组织文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:'其他文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	cm.push({
		dataIndex:'zjcwgs',
		hidden : false,
		header:'中间产物个数',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'zjcwlx',
		hidden : false,
		header:'中间产物类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkndTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkndTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : true,
		header:'各组文库片段长度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cxlx',
		hidden : false,
		header:'测序类型',
		width:20*6
	});
	cm.push({
		dataIndex:'cxpt',
		hidden : false,
		header:'测序平台',
		width:20*6
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : false,
		header:'测序读长',
		width:20*6
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:'实验序号',
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'实验步骤',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'是否入库',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/wknd/wkndTask/showWkndTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="文库调整浓度明细";
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/wknd/wkndTask/delWkndTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '批量入库',
		handler : ruku
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	wkndTaskItemGrid=gridEditTable("wkndTaskItemdiv",cols,loadParam,opts);
	$("#wkndTaskItemdiv").data("wkndTaskItemGrid", wkndTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectwkndTaskFun(){
	var win = Ext.getCmp('selectwkndTask');
	if (win) {win.close();}
	var selectwkndTask= new Ext.Window({
	id:'selectwkndTask',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectwkndTask.close(); }  }]  }) });  
    selectwkndTask.show(); }
	function setwkndTask(rec){
		var gridGrid = $("#wkndTaskItemdiv").data("wkndTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('wkndTask-id',rec.get('id'));
			obj.set('wkndTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectwkndTask');
		if(win){
			win.close();
		}
	}
	function selectwkndTaskDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/WkndTaskSelect.action?flag=wkndTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selwkndTaskVal(this);
				}
			}, true, option);
		}
	var selwkndTaskVal = function(win) {
		var operGrid = wkndTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#wkndTaskItemdiv").data("wkndTaskItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('wkndTask-id',rec.get('id'));
				obj.set('wkndTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
	
	function ruku() {
		var operGrid = wkndTaskItemGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if(selectRecord.length>0){
		var records = [];
		for ( var i = 0; i < selectRecord.length; i++) {
			if(selectRecord[i].get("state")=="1"){
				message("已有样本入库，请重新选择！");
				return;
			}
			records.push(selectRecord[i].get("id"));
		}
		ajax("post", "/experiment/sj/wknd/wkndTask/rukuWkndTaskItem.action", {
			ids : records
		}, function(data) {
			if (data.success) {
				wkndTaskItemGrid.getStore().commitChanges();
				wkndTaskItemGrid.getStore().reload();
				message("入库成功！");
			} else {
				message("入库失败！");
			}
		}, null);
		}else{
			message("请选择入库样本！");
		}
	}
