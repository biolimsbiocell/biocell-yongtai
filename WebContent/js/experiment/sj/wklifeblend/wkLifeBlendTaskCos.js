var wkBlendTaskCosGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'temperature',
		type:"string"
	});
	   fields.push({
		name:'speed',
		type:"string"
	});
	   fields.push({
			name:'state',
			type:"string"
		});
	   fields.push({
			name:'type-id',
			type:"string"
		});
	   fields.push({
			name:'type-name',
			type:"string"
		});
	   fields.push({
		name:'time',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-id',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-name',
		type:"string"
	});
	   fields.push({
		name:'itemId',
		type:"string"
	});
	   fields.push({
		name:'tCos',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		
	});
	
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.instrumentNo,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var instrumentName =new Ext.form.TextField({
        allowBlank: false
	});
	instrumentName.on('focus', function() {
		setinstrumentName();
	 });
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.instrumentName,
		width:20*6,
		editor:instrumentName
		
	});
	cm.push({
		dataIndex:'type-id',
		hidden : true,
		header:"设备类型ID",
		width:20*6
	});
	cm.push({
		dataIndex:'type-name',
		hidden : false,
		header:biolims.common.instrumentType,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.instrumentState,
		width:20*6,
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.common.temperature,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:biolims.common.speed,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.common.time,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isDetecting,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkblendTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkblendTask-name',
		hidden : false,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : false,
		header:biolims.common.itemId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tCos',
		hidden : true,
		header:biolims.common.templateInstrumentId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskCosListJson.action?id="+ $("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title=biolims.common.instrumentDetail;
	opts.height =  document.body.clientHeight-250;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/delWkBlendTaskCos.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
//	opts.tbar.push({
//			text : '选择相关主表',
//				handler : selectwkblendTaskDialogFun
//		});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = wkBlendTaskCosGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
							wkBlendTaskCosGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.copy,
		handler : function() {
			var ob = wkBlendTaskCosGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			var records = wkBlendTaskCosGrid.getSelectRecord();
			if (records.length==1) {
				$.each(records, function(i, obj) {
					p.set("code",obj.get("code"));
					p.set("name",obj.get("name"));
					p.set("temperature",obj.get("temperature"));
					p.set("speed",obj.get("speed"));
					p.set("time",obj.get("time"));
					p.set("isGood",obj.get("isGood"));
					p.set("itemId",obj.get("itemId"));
					p.set("tCos",obj.get("tCos"));
					p.set("type-id",obj.get("type-id"));
					p.set("type-name",obj.get("type-name"));
					p.set("wkblendTask-id",obj.get("wkblendTask-id"));
					p.set("wkblendTask-name",obj.get("wkblendTask-name"));
					p.set("note",obj.get("note"));
					wkBlendTaskCosGrid.stopEditing();
					wkBlendTaskCosGrid.getStore().add(p);
					wkBlendTaskCosGrid.startEditing(0, 0);
				});
			}else{
				message(biolims.common.pleaseChooseCopyData);
			}
		}
	});
	if($("#wkBlendTask_id").val()&&$("#wkBlendTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	}
	wkBlendTaskCosGrid=gridEditTable("wkBlendTaskCosdiv",cols,loadParam,opts);
	$("#wkBlendTaskCosdiv").data("wkBlendTaskCosGrid", wkBlendTaskCosGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectwkblendTaskFun(){
	var win = Ext.getCmp('selectwkblendTask');
	if (win) {win.close();}
	var selectwkblendTask= new Ext.Window({
	id:'selectwkblendTask',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectwkblendTask.close(); }  }]  }) });  
    selectwkblendTask.show(); }
	function setwkblendTask(rec){
		var gridGrid = $("#wkBlendTaskCosdiv").data("wkBlendTaskCosGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('wkblendTask-id',rec.get('id'));
			obj.set('wkblendTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectwkblendTask')
		if(win){
			win.close();
		}
	}
	function selectwkblendTaskDialogFun(){
			var title = '';
			var url = '';
			title = biolims.common.selectRelevantTable;
			url = ctx + "/WkBlendTaskSelect.action?flag=wkblendTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"Confirm" : function() {
						selwkblendTaskVal(this);
				}
			}, true, option);
		}
	var selwkblendTaskVal = function(win) {
		var operGrid = wkBlendTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#wkBlendTaskCosdiv").data("wkBlendTaskCosGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('wkblendTask-id',rec.get('id'));
				obj.set('wkblendTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message(biolims.common.selectYouWant);
			return;
		}
	};
	function setinstrumentName(){
		var record = wkBlendTaskCosGrid.getSelectionModel().getSelected();
		var options = {};
		 options.width = 800;
		 options.height = 500;
		 loadDialogPage(null,biolims.common.selectInstrument, "/equipment/repair/showInstrument.action", {
		 "Confirm" : function() {
		 var operGrid = $("#show_instrument_div").data("showInstrumentGrid");
		 var selectRecord = operGrid.getSelectionModel().getSelections();
		 if (selectRecord.length > 0) {
			 $.each(selectRecord, function(i, obj) {
				 record.set("code", obj.get("id"));
				 record.set("name", obj.get("name"));
				 record.set("state", obj.get("state-name"));
			 });
				
		 }else{
		 message(biolims.common.selectYouWant);
		 return;}
		 $(this).dialog("close");}
		 }, true, options);
	}