var wkBlendTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'sjfz',
		type:"string"
	});
	   fields.push({
		name:'yhhzh',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'xylr',
		type:"string"
	});
	   fields.push({
		name:'clr',
		type:"string"
	});
	   fields.push({
		name:'flr',
		type:"string"
	});
	   fields.push({
		name:'xzlr',
		type:"string"
	});
	   fields.push({
		name:'qtlr',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'wknd',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-id',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-name',
		type:"string"
	});
	    fields.push({
			name:'unitGroup-id',
			type:"string"
		});
		fields.push({
			name:'unitGroup-name',
			type:"string"
		});

	    fields.push({
			name:'tempId',
			type:"string"
		});
	    fields.push({
			name:'mbnd',
			type:"string"
		});
	    fields.push({
			name:'lltj',
			type:"string"
		});
	    fields.push({
			name:'bsgz',
			type:"string"
		});
	    fields.push({
			name:'tzhwknd',
			type:"string"
		});
	    fields.push({
			name:'tzhwktj',
			type:"string"
		});
	    fields.push({
			name:'tzhwkzl',
			type:"string"
		});
	    fields.push({
			name:'nextFlowId',
			type:"string"
		});
	    fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	  fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	  fields.push({
			name:'isZkp',
			type:"string"
		});
	  
	  
	  fields.push({
			name:'sampleName',
			type:"string"
		});
	  fields.push({
			name:'insertSize',
			type:"string"
		});
	  fields.push({
			name:'sumFlux',
			type:"string"
		});
	  fields.push({
			name:'ratioOne',
			type:"string"
		});
	  fields.push({
			name:'ratioNeed',
			type:"string"
		});
	  fields.push({
			name:'species',
			type:"string"
		});
	  fields.push({
			name:'laneNum',
			type:"string"
		});
	  fields.push({
			name:'sampleCode',
			type:"string"
		});
	  fields.push({
			name:'dycxsbs',
			type:"string"
		});
	  fields.push({
			name:'dycbsl',
			type:"string"
		});
	  fields.push({
			name:'decxsbs',
			type:"string"
		});
	  fields.push({
			name:'decbsl',
			type:"string"
		});
	  fields.push({
			name:'pmol',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本号',
		width:20*6
	});
	

	cm.push({
		dataIndex:'sampleName',
		hidden : true,
		header:"样本名称",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumFlux',
		hidden : true,
		header:"通量比例和",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ratioOne',
		hidden : true,
		header:"1比例的量",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ratioNeed',
		hidden : true,
		header:"按比例所需量",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'species',
		hidden : true,
		header:"物种",
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : true,
		header:biolims.pooling.poolingWk,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sjfz',
		hidden : true,
		header:biolims.wk.sjfz,
		width:20*6
	});
	cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:biolims.wk.yhhzh,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:biolims.pooling.code,
		width:20*6
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:biolims.pooling.xylr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:biolims.pooling.clr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:biolims.pooling.flr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:biolims.pooling.xzLr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:biolims.pooling.qtlr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:biolims.pooling.flux,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wknd',
		hidden : true,
		header:biolims.wk.wknd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : true,
		header:biolims.wk.wkpdcd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mbnd',
		hidden : true,
		header:biolims.wk.mbnd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'lltj',
		hidden : true,
		header:biolims.wk.lltj,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bsgz',
		hidden : true,
		header:biolims.wk.bsgz,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzhwknd',
		hidden : true,
		header:biolims.wk.tzhwknd,
		width:20*8,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzhwktj',
		hidden : true,
		header:biolims.wk.tzhwktj,
		width:20*7,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzhwkzl',
		hidden : true,
		header:biolims.wk.tzhwkzl,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'laneNum',
		hidden : false,
		header:"LaneNum",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dycxsbs',
		hidden : false,
		header: biolims.common.oneDilutionFactors,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'dycbsl',
		hidden : false,
		header:biolims.common.oneHydrationAmount,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'decxsbs',
		hidden : false,
		header:biolims.common.twoDilutionFactors,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'decbsl',
		hidden : false,
		header:biolims.common.twoHydrationAmount,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'pmol',
		hidden : false,
		header:biolims.sequencing.upNd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'cxlx',
		hidden : false,
		header:biolims.pooling.sequencingType,
		width:20*6
	});
	
	cm.push({
		dataIndex:'cxpt',
		hidden : false,
		header:biolims.pooling.sequencingPlatform,
		width:20*6
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : false,
		header:biolims.pooling.sequencingReadLong,
		width:20*6
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		editor : nextFlowCob
	});
	
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:"单位组ID",
		width:15*10
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: "单位组",
		width:15*10,
		hidden:true
//		sortable:true,
//		editor : testUnitGroup
	});

	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkblendTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkblendTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.templatePreparationRelust;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/delWkBlendTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};

	opts.tbar.push({
	text : biolims.common.batchNextStep,
	handler : function() {
		var selectRecord = wkBlendTaskResultGrid.getSelectionModel().getSelections();
		if(selectRecord.length>0){
			loadTestNextFlowCob();
		}else{
			message(biolims.common.pleaseSelect);
		}
//		var options = {};
//		options.width = 400;
//		options.height = 300;
//		loadDialogPage($("#bat_result_div"), "批量下一步", null, {
//			"确定" : function() {
//				var records = wkBlendTaskResultGrid.getSelectRecord();
//				if (records && records.length > 0) {
//					var result = $("#nextFlow").val();
//					var nextFlowId = $("#nextFlowId").val();
//					wkBlendTaskResultGrid.stopEditing();
//					$.each(records, function(i, obj) {
//						obj.set("nextFlow", result);
//						obj.set("nextFlowId",nextFlowId);
//					});
//					wkBlendTaskResultGrid.startEditing(0, 0);
//				}
//				$(this).dialog("close");
//			}
//		}, true, options);
	}
});
	if($("#wkBlendTask_id").val()&&$("#wkBlendTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	}
//	opts.tbar.push({
//		text : "单位组",
//		handler : function() {
//			var records = wkBlendTaskResultGrid.getSelectRecord();
//			if(records.length>0){
//				loadUnitGroup();
//			}else{
//				message("请选择样本");
//			}
//			
//		}
//	});
	wkBlendTaskResultGrid=gridEditTable("wkBlendTaskResultdiv",cols,loadParam,opts);
	$("#wkBlendTaskResultdiv").data("wkBlendTaskResultGrid", wkBlendTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = wkBlendTaskResultGrid.getSelectRecord();
	var sequencingPlatform="";
	$.each(records1, function(j, k) {
		sequencingPlatform=k.get("cxpt");
	});
	
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/showPoolingTaskNextFlow.action?model=WkLifeBlendTask&sequencingPlatform="+sequencingPlatform, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = wkBlendTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = wkBlendTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}	

//查询单位组
//function loadUnitGroup(){
//var win = Ext.getCmp('loadUnitGroup');
//if (win) {win.close();}
//var loadUnitGroup= new Ext.Window({
//id:'loadUnitGroup',modal:true,title:'选择单位组',layout:'fit',width:600,height:600,closeAction:'close',
//plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//collapsible: true,maximizable: true,
//items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=unitGroup' frameborder='0' width='100%' height='100%' ></iframe>"}),
//buttons: [
//{ text: '关闭',
// handler: function(){
//	 loadUnitGroup.close(); }  }]  });   
//loadUnitGroup.show(); }
//
//function setunitGroup(id,name){
//	var gridGrid = $("#wkBlendTaskResultdiv").data("wkBlendTaskResultGrid");
//	var selRecords = gridGrid.getSelectionModel().getSelections(); 
//	$.each(selRecords, function(i, obj) {
//		alert(id);
//		obj.set('unitGroup-id',id);
//		obj.set('unitGroup-name',name);
//	});
//	var win = Ext.getCmp('loadUnitGroup');
//	if(win){
//		win.close();
//	}
//}

