var wkBlendTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'sjfz',
		type:"string"
	});
	   fields.push({
		name:'yhhzh',
		type:"string"
	});
	   fields.push({
		name:'wklx',
		type:"string"
	});
	   
	   fields.push({
		name:'kybh',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'tempid',
		type:"string"
	});
	   fields.push({
			name:'state',
			type:"string"
		});
	   fields.push({
			name:'xylr',
			type:"string"
		});
		   fields.push({
			name:'clr',
			type:"string"
		});
		   fields.push({
			name:'flr',
			type:"string"
		});
		   fields.push({
			name:'xzlr',
			type:"string"
		});
		   fields.push({
			name:'qtlr',
			type:"string"
		});
		   fields.push({
			name:'tl',
			type:"string"
		});
		   fields.push({
			name:'tlbl',
			type:"string"
		});
		   fields.push({
			name:'kzxl',
			type:"string"
		});
		   fields.push({
			name:'ect',
			type:"string"
		});
		   fields.push({
			name:'qpcrbl',
			type:"string"
		});
		   fields.push({
			name:'blcd',
			type:"string"
		});
		   fields.push({
				name:'probeCode-id',
				type:"string"
			});
		   fields.push({
				name:'probeCode-name',
				type:"string"
			});
		   fields.push({
				name:'concentration',
				type:"string"
			});
			   fields.push({
				name:'volume',
				type:"string"
			});
			   fields.push({
				name:'sumTotal',
				type:"string"
			});
		   fields.push({
				name:'techJkServiceTask-id',
				type:"string"
			});
		  fields.push({
				name:'techJkServiceTask-name',
				type:"string"
			});
		  fields.push({
				name:'techJkServiceTask-sequenceBillDate',
				type:"string"
			});
		  fields.push({
				name:'isZkp',
				type:"string"
			});
		  fields.push({
				name:'tjItem-id',
				type:"string"
			});
		   fields.push({
				name:'tjItem-inwardCode',
				type:"string"
			});
		   fields.push({
				name:'tjItem-blendLane',
				type:"string"
			});
		   fields.push({
			name:'tjItem-blendFc',
			type:"string"
		   });
		   fields.push({
				name:'tjItem-note',
				type:"string"
		   });
		   fields.push({
				name:'indexa',
				type:"string"
		   });
		   fields.push({
				name:'insertSize',
				type:"string"
		   });
		   fields.push({
				name:'sampleCode',
				type:"string"
		   });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : false,
		header:biolims.wk.wkCode,
		width:20*7
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:"INDEX",
		width:10*5
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-sequenceBillDate',
		hidden : true,
		header:"截止日期",
		width:30*6
	});
	cm.push({
		dataIndex:'sjfz',
		hidden : true,
		header:biolims.wk.sjfz,
		width:20*6
	});
	cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:biolims.wk.yhhzh,
		width:20*6
		 
	});
	cm.push({
		dataIndex:'wklx',
		hidden : true,
		header:biolims.wk.wkType,
		width:20*6
		 
	});
	cm.push({
		dataIndex:'probeCode-id',
		hidden : true,
		header:biolims.pooling.probeCodeId,
		width:20*6
		 
	});
	cm.push({
		dataIndex:'probeCode-name',
		hidden : false,
		header:biolims.pooling.probeCodeName,
		width:20*6
		 
	});
	cm.push({
		dataIndex:'kybh',
		hidden : true,
		header:biolims.pooling.kyId,
		width:20*6
		
		 
	});
	
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:biolims.pooling.xylr,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:biolims.pooling.clr,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:biolims.pooling.flr,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:biolims.pooling.xzLr,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:biolims.pooling.qtlr,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:biolims.pooling.flux,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'tlbl',
		hidden : true,
		header:biolims.pooling.fluxRate,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'kzxl',
		hidden : true,
		header:biolims.pooling.kzxl,
		width:20*6
		
	 
	});
	cm.push({
		dataIndex:'ect',
		hidden : true,
		header:'E^-ΔCT',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'qpcrbl',
		hidden : true,
		header:biolims.pooling.qpcrbl,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'blcd',
		hidden : true,
		header:biolims.pooling.blcd,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.concentration,
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:biolims.common.sumNum,
		width:20*6
	});
	cm.push({
		dataIndex:'insertSize',
		hidden : false,
		header:biolims.common.wKsamll,
		width:20*6
	});
	cm.push({
		dataIndex:'cxlx',
		hidden : false,
		header:biolims.pooling.sequencingType,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'cxpt',
		hidden : false,
		header:biolims.pooling.sequencingPlatform,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : false,
		header:biolims.pooling.sequencingReadLong,
		width:20*6
		
		 
	});

	cm.push({
		dataIndex:'tempid',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6,
		
		 
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'tjItem-id',
		hidden : true,
		header:"科研样本ID",
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-inwardCode',
		hidden : true,
		header:"科研内部项目号",
		width:20*6
	});	
	cm.push({
		dataIndex:'tjItem-blendLane',
		hidden : true,
		header:"科研混Lane",
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-blendFc',
		hidden : true,
		header:"科研混Flowcell",
		width:20*6
	});	
	
	cm.push({
		dataIndex:'tjItem-note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskTempListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.templatePreparationTemporaryTable;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/delWkBlendTaskTemp.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text :biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"Confirm" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = wkBlendTaskTempGrid.getAllRecord();
							var store = wkBlendTaskTempGrid.store;

							var isOper = true;
							var buf = [];
							wkBlendTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("fjwk")){
										buf.push(store.indexOfId(obj1.get("id")));
									}
									
								});
							});
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("fjwk"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message(biolims.common.noMatchSample+nolist);
							}
							wkBlendTaskTempGrid.getSelectionModel().selectRows(buf);
							if(isOper==false){
								message(biolims.common.samplecodeComparison);
							}else{
								addItem();
							}
							wkBlendTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
//	opts.tbar.push({
//		text : biolims.common.scienceService,
//		handler : techDNAService
//	});
	opts.tbar.push({
		text : biolims.common.retrieve,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_js_div"), biolims.common.retrieve, null, {
				"Confirm": function() {
					commonSearchAction(wkBlendTaskTempGrid);
					$(this).dialog("close");
				}
			}, true, options);
			
		}
	});
	opts.tbar.push({
		text : biolims.common.sampleWindow,
		handler : changeWidth
	});
	wkBlendTaskTempGrid=gridEditTable("wkBlendTaskTempdiv",cols,loadParam,opts);
	$("#wkBlendTaskTempdiv").data("wkBlendTaskTempGrid", wkBlendTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//添加任务到子表
function addItem(){
	var selectRecord=wkBlendTaskTempGrid.getSelectionModel();
	var selRecord=wkBlendTaskItemGrid.store;
	var count=1;
	var max=0;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					return;					
				}
			}
			if(!isRepeat){
			var ob = wkBlendTaskItemGrid.getStore().recordType;
			wkBlendTaskItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("tempId",obj.get("id"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("fjwk",obj.get("fjwk"));
//			p.set("sjfz",obj.get("sjfz"));	
			p.set("xylr",obj.get("xylr"));
			p.set("clr",obj.get("clr"));
			p.set("flr",obj.get("flr"));
			p.set("xzlr",obj.get("xzlr"));
			p.set("qtlr",obj.get("qtlr"));
			p.set("tl",obj.get("tl"));
			p.set("ect",obj.get("ect"));
			p.set("qpcrbl",obj.get("qpcrbl"));
			p.set("blcd",obj.get("blcd"));
			p.set("cxlx",obj.get("cxlx"));
			p.set("cxpt",obj.get("cxpt"));
			p.set("cxdc",obj.get("cxdc"));
			p.set("tl",obj.get("tl"));
			p.set("isZkp",obj.get("isZkp"));
			p.set("indexa",obj.get("indexa"));
			p.set("wkmConcentration",obj.get("concentration"));
			p.set("wkhhtl",obj.get("volume"));
			p.set("sumTotal",obj.get("sumTotal"));
			p.set("tjItem-id", obj.get("tjItem-id"));
			p.set("tjItem-inwardCode", obj.get("tjItem-inwardCode"));
			p.set("tjItem-blendLane", obj.get("tjItem-blendLane"));
			p.set("tjItem-blendFc", obj.get("tjItem-blendFc"));
			p.set("tjItem-note", obj.get("tjItem-note"));
			p.set("techJkServiceTask-id",obj.get("techJkServiceTask-id"));
			p.set("techJkServiceTask-name",obj.get("techJkServiceTask-name"));
			p.set("insertSize",obj.get("insertSize"));
			
			p.set("orderNumber",Number(max)+count);
			wkBlendTaskItemGrid.getStore().add(p);
			count++;
			wkBlendTaskItemGrid.startEditing(0, 0);
			
		}
			
	});
	}else{
		message(biolims.common.pleaseAddSample);
	}
	
}

//科技服务
function techDNAService(){
	 	var options = {};
	 	options.width = document.body.clientWidth - 470;
	 	options.height = document.body.clientHeight - 80;
		loadDialogPage(null, biolims.common.scienceServiceTest, "/technology/wk/techJkServiceTask/showTechJkServiceTaskDialogList.action", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				//var records = bloodSplitLeftPagedivGrid.store;
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						var id1=obj.get("id");
						$("#techJkServiceTask_id").val(id1);
						commonSearchAction(wkBlendTaskTempGrid);
//						ajax("post", "/experiment/sj/wkblend/wkBlendTask/selTechJkServicTaskByid.action", {
//						id : id1
//						}, function(data) {
//							if(data.success){
//								$.each(data.list, function(a,b) {
//									var ob = wkBlendTaskItemGrid.getStore().recordType;
//									wkBlendTaskItemGrid.stopEditing();
//									var p= new ob({});
//									var selRecord=wkBlendTaskItemGrid.store;
//									for(var j=0;j<selRecord.getCount();j++){
//										var oldv = selRecord.getAt(j).get("tempId");
//										if(oldv == b.id){
//											return;
//										}
//									}
//									p.set("tempId",b.id);
//									p.set("fjwk",b.fjwk);
//									p.set("sjfz",b.sjfz);
//									p.set("xylr",b.xylr);
//									p.set("clr",b.clr);
//									p.set("flr",b.flr);
//									p.set("xzlr",b.xzlr);
//									p.set("qtlr",b.qtlr);
//									p.set("tl",b.tl);
//									p.set("ect",b.ect);
//									p.set("qpcrbl",b.qpcrbl);
//									p.set("blcd",b.blcd);
//									p.set("cxlx",b.cxlx);
//									p.set("cxpt",b.cxpt);
//									p.set("cxdc",b.cxdc);
//									p.set("tl",b.tl);
//									p.set("concentration",b.concentration);
//									p.set("volume",b.volume);
//									p.set("sumTotal",b.sumTotal);
//									p.set("orderNumber",a+1);
//									if(b.techJkServiceTask!=null){
//										p.set("techJkServiceTask-id",b.techJkServiceTask.id);
//										p.set("techJkServiceTask-name",b.techJkServiceTask.name);
//									}
//									wkBlendTaskItemGrid.getStore().add(p);
//								});
//							}
//						});
					});
					
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function selectTemp(){
	if(window.location.href.indexOf("?")>0){
		location.href=window.location.href+"&method=window";
	}else{
		location.href=window.location.href+"?method=window";	
	}

}
