var wkBlendTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'reciveDate',
		header:biolims.common.testTime,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:biolims.common.templateId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:biolims.common.templateName,
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:biolims.common.acceptUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.common.acceptUserName,
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.common.createUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.common.createUserName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.createDate,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		hidden:true,
		header:biolims.wk.confirmUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:biolims.wk.confirmUserName,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.common.confirmDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:50*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskListJson.action";
	var opts={};
	opts.title=biolims.common.templatePreparation;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	wkBlendTaskGrid=gridTable("show_wkBlendTask_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/sj/wklifeblend/wkBlendTask/editWkBlendTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/experiment/sj/wklifeblend/wkBlendTask/editWkBlendTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/experiment/sj/wklifeblend/wkBlendTask/viewWkBlendTask.action?id=' + id;
}
function exportexcel() {
	wkBlendTaskGrid.title = biolims.common.exportList;
	var vExportContent = wkBlendTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startreciveDate").val() != undefined) && ($("#startreciveDate").val() != '')) {
					var startreciveDatestr = ">=##@@##" + $("#startreciveDate").val();
					$("#reciveDate1").val(startreciveDatestr);
				}
				if (($("#endreciveDate").val() != undefined) && ($("#endreciveDate").val() != '')) {
					var endreciveDatestr = "<=##@@##" + $("#endreciveDate").val();

					$("#reciveDate2").val(endreciveDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
					var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
					$("#confirmDate1").val(startconfirmDatestr);
				}
				if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
					var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();

					$("#confirmDate2").val(endconfirmDatestr);

				}
				
				
				commonSearchAction(wkBlendTaskGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
