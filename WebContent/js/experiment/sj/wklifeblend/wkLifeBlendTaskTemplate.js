var wkBlendTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'testUserId',
		type:"string"
	});
	    fields.push({
		name:'testUserName',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-id',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:biolims.common.templateProcessId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.stepDetail,
		width:100*6,
		editor : new Ext.form.HtmlEditor({
			readOnly : true
		})
	});
	cm.push({
		dataIndex:'testUserId',
		hidden : true,
		header:biolims.common.testUserId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		loadTestUser();
	});
	cm.push({
		dataIndex:'testUserName',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*10,
		editor : testUser
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:biolims.common.startTime,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:biolims.common.endTime,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:biolims.common.relateSample,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkblendTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkblendTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.executionStep;
	opts.height =  document.body.clientHeight*0.8;
	opts.tbar = [];
	var state = $("#limsRightsModify").val();
	if(state&&state=='1'){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/wkblifelend/wkBlendTask/delWkBlendTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : biolims.common.fillDetail,
		handler : templateSelect
	});
	
	opts.tbar.push({
		iconCls : 'application_print',
		text : biolims.common.printList,
		handler : stampOrder
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
	opts.tbar.push({
		text : biolims.common.testUserName,
		handler : loadTestUser
	});
	if($("#wkBlendTask_id").val()&&$("#wkBlendTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
			
		
	}
	}
	wkBlendTaskTemplateGrid=gridEditTable("wkBlendTaskTemplatediv",cols,loadParam,opts);
	$("#wkBlendTaskTemplatediv").data("wkBlendTaskTemplateGrid", wkBlendTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


//查询实验员
function loadTestUser(){
		var gid=$("#wkBlendTask_acceptUser").val();
		var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
			"Confirm" : function() {
				var gridGrid = $("#wkBlendTaskTemplatediv").data("wkBlendTaskTemplateGrid");
				var selRecords = gridGrid.getSelectionModel().getSelections(); 
				var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					var id=[];
					var name=[];
					for(var i=0;i<selectRecord.length;i++){
						id.push(selectRecord[i].get("user-id"));
						name.push(selectRecord[i].get("user-name"));
					}
					for(var j=0;j<selRecords.length;j++){
						selRecords[j].set("testUserId",id.toString());
						selRecords[j].set("testUserName",name.toString());}
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
var selreciveUserVal = function(win) {
	var operGrid = userDialogGrid;
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
		var gridGrid = $("#wkBlendTaskTemplatediv").data("wkBlendTaskTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reciveUser-id',rec.get('id'));
			obj.set('reciveUser-name',rec.get('name'));
		});
		$(win).dialog("close");
		$(win).dialog("remove");
	} else {
		message(biolims.common.selectYouWant);
		return;
	}
};

//打印执行单
function stampOrder(){
	var id=$("#wkBlendTask_template").val();
	if(id==""){
		message(biolims.common.pleaseSelectTemplate);
		return;
	}else{
		var url = '__report=wkBlendTaskTask.rptdesign&id=' + $("#wkBlendTask_id").val();
		commonPrint(url);}
}
//生成结果明细
function addSuccess(){	

	var num =$("#wkBlendTask_template").val();
		if(num!=""){
			var setNum = wkBlendTaskReagentGrid.store;
			var selectRecords = wkBlendTaskItemGrid.store;
			for(var i=0;i<setNum.getCount();i++){
					setNum.getAt(i).set("sampleNum",selectRecords.getCount());
			}
			for(var a=0;a<selectRecords.getCount();a++){
				if(selectRecords.getAt(a).get("sjfz")==""){
					message("请填写文库分组号！");
					return;
				}
			}


	var getRecord = wkBlendTaskItemGrid.getSelectionModel().getSelections();
	var selectRecord = wkBlendTaskTemplateGrid.getSelectionModel().getSelections();
	var selRecord = wkBlendTaskResultGrid.store;
	if(selectRecord.length>0){
				$.each(selectRecord, function(i, obj) {
					var isRepeat = true;
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i1=0; i1<scode.length; i1++){
						for(var j1=0;j1<selRecord.getCount();j1++){
							var getv = scode[i1];
							var setv = selRecord.getAt(j1).get("sjfz");
							if(getv == setv){
								isRepeat = false;
								message(biolims.common.haveDuplicate);
								break;					
							}
						}
					}
					if(isRepeat){
						var blendCode=[];
						var n=[];
						for(var j=0;j<getRecord.length;j++){
							if(getRecord[j].get("sjfz")!=""){
								blendCode.push(getRecord[j].get("sjfz"));
							}
						}
						for(var j=0;j<getRecord.length;j++){
							var a=blendCode.indexOf(getRecord[j].get("sjfz"));
							var flg=true;
							for(var k=0;k<n.length;k++){
								if(a==n[k]){
									flg=false;
								}
							}
							if(flg){
								n.push(a);
							}
						}
						for(var i=0;i<n.length;i++){
							var ob = wkBlendTaskResultGrid.getStore().recordType;
							wkBlendTaskResultGrid.stopEditing();
							var p = new ob({});
							p.isNew = true;
							var sampleCode=[];
							var tl=0;
							for(var j=0;j<getRecord.length;j++){
								if(getRecord[j].get("sjfz")==blendCode[n[i]]){
									sampleCode.push(getRecord[j].get("sampleCode"));
									tl+=Number(getRecord[j].get("tl"));
								}
							}
							p.set("nextFlowId","0068");
							p.set("nextFlow","模板富集");
							p.set("sjfz",getRecord[n[i]].get("sjfz"));
							p.set("cxlx",getRecord[n[i]].get("cxlx"));
							p.set("cxpt",getRecord[n[i]].get("cxpt"));
							p.set("cxdc",getRecord[n[i]].get("cxdc"));
							p.set("tl",tl);
							p.set("sampleCode",sampleCode.toString());
							wkBlendTaskResultGrid.getStore().add(p);
							wkBlendTaskResultGrid.startEditing(0,0);
						}
						message(biolims.common.generateResultsSuccess);
					}
				});
			
	}else{
		var selRecord = wkBlendTaskResultGrid.store;
		var flag;
		
		var getRecord = wkBlendTaskItemGrid.getAllRecord();
		flag = true;
		for(var j1=0;j1<selRecord.getCount();j1++){
			for(var i=0;i<getRecord.length;i++){
				var getv = getRecord[i].get("sjfz");
				var setv = selRecord.getAt(j1).get("sjfz");
				if(getv == setv){
					flag = false;
					message(biolims.common.haveDuplicate);
					break;					
				}
			}
		}
		if(flag){
			
			var blendCode=[];
			var n=[];
			for(var j=0;j<getRecord.length;j++){
				if(getRecord[j].get("sjfz")!=""){
					blendCode.push(getRecord[j].get("sjfz"));
				}
			}
			for(var j=0;j<getRecord.length;j++){
				var a=blendCode.indexOf(getRecord[j].get("sjfz"));
				var flg=true;
				for(var k=0;k<n.length;k++){
					if(a==n[k]){
						flg=false;
					}
				}
				if(flg){
					n.push(a);
				}
			}
			for(var i=0;i<n.length;i++){
				var ob = wkBlendTaskResultGrid.getStore().recordType;
				wkBlendTaskResultGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				var sampleCode=[];
				var tl=0;
				for(var j=0;j<getRecord.length;j++){
					if(getRecord[j].get("sjfz")==blendCode[n[i]]){
						sampleCode.push(getRecord[j].get("sampleCode"));
						tl+=Number(getRecord[j].get("tl"));
					}
				}
				p.set("nextFlowId","0068");
				p.set("nextFlow","模板富集");
				p.set("sjfz",getRecord[n[i]].get("sjfz"));
				p.set("cxlx",getRecord[n[i]].get("cxlx"));
				p.set("cxpt",getRecord[n[i]].get("cxpt"));
				p.set("cxdc",getRecord[n[i]].get("cxdc"));
				p.set("tl",tl);
				p.set("sampleCode",sampleCode.toString());
				wkBlendTaskResultGrid.getStore().add(p);
				wkBlendTaskResultGrid.startEditing(0,0);
			}
			message(biolims.common.generateResultsSuccess);
		}
	}
		}else{
			message(biolims.common.pleaseSelectTemplate);
		}
}

//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=wkBlendTaskTemplateGrid.getSelectionModel();
	var setNum = wkBlendTaskReagentGrid.store;
	var selectRecords=wkBlendTaskItemGrid.getSelectionModel();
	if (selectRecords.getSelections().length > 0) {
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			obj.set("startTime",str);
			//将所选样本的数量，放到原辅料样本数量处
			for(var i=0; i<setNum.getCount();i++){
				var num = setNum.getAt(i).get("itemId");
				if(num==obj.get("code")){
					setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
				}
			}
		});
	}else{
		message(biolims.common.pleaseSelect);
	}
	
	//将所选的样本，放到关联样本
	var selRecord=wkBlendTaskTemplateGrid.getSelectRecord();
	var codes = "";
		$.each(selectRecords.getSelections(), function(i, obj) {
			codes += obj.get("sjfz")+",";
		});
		$.each(selRecord, function(i, obj) {
			obj.set("sampleCodes", codes);
		});
	}else{
		message(biolims.common.pleaseSelectSamples);
	}
}
//获取停止时的时间
function getEndTime(){
		var setRecord=wkBlendTaskItemGrid.store;
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=wkBlendTaskTemplateGrid.getSelectionModel();
		var getIndex = wkBlendTaskTemplateGrid.store;
		var getIndexs = wkBlendTaskTemplateGrid.getSelectionModel().getSelections();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("endTime",str);	
				//将步骤编号，赋值到明细表
				var codes = obj.get("sampleCodes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(scode[j]==setRecord.getAt(i).get("sjfz")){
							setRecord.getAt(i).set("stepNum",obj.get("code"));
						}
					}
				}
				//将当前行的关联样本传到下一行
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
			});
		}
		
		
}


//选择实验步骤
function templateSelect(){
	var option = {};
	option.width = 605;
	option.height = 558;
	loadDialogPage(null, biolims.common.chooseExperimentalSteps, "/experiment/sj/wklifeblend/wkBlendTask/showTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
		"Confirm" : function() {
			var operGrid = $("#template_wait_grid_div").data("grid");
			var ob = wkBlendTaskTemplateGrid.getStore().recordType;
			wkBlendTaskTemplateGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("code", obj.get("code"));
						p.set("name", obj.get("name"));
						wkBlendTaskTemplateGrid.getStore().add(p);
				});
				wkBlendTaskTemplateGrid.startEditing(0, 0);
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);
}