function changeWidth(){
	
	$("#wkBlendTaskTemppage").css("width","40%");
	$("#markup").css("width","60%");
	wkBlendTaskTempGrid.getStore().reload();
	
	
}
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#wkBlendTask_state").val();
	var stateName = $("#wkBlendTask_stateName").val();
	var method = $("#wkBlendTask_method").val();
	if(id =="3"||stateName==biolims.common.toModify){
//		load("/experiment/sj/wkblend/wkBlendTask/showWkBlendTaskTempList.action", null,  "#wkBlendTaskTemppage");
//		$("#markup").css("width","75%");

		if(method=='window'){
			$("#wkBlendTaskTemppage").remove();
			$("#markup").css("width","100%");
			selectTemp();
		}else{
			load("/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskTempList.action", null,  "#wkBlendTaskTemppage");
			$("#markup").css("width","75%");
		}
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#wkBlendTaskTemppage").remove();
	}
	
	var mttf = $("#wkBlendTask_template_templateFieldsCode").val();
	var mttfItem = $("#wkBlendTask_template_templateFieldsItemCode").val();
	reloadWkBlendTaskItem(mttf, mttfItem);
});		

function reloadWkBlendTaskItem(mttf, mttfItem) {
	setTimeout(
			function() {
				if (mttf == null || mttf == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = wkBlendTaskItemGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttf.indexOf(colName) >= 0) {
							colArray2.push(colName);
							wkBlendTaskItemGrid.getColumnModel().setHidden(i,
									false);
						} else {
							wkBlendTaskItemGrid.getColumnModel().setHidden(i,
									true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
				if (mttfItem == null || mttfItem == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = wkBlendTaskResultGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttfItem.indexOf(colName) >= 0) {
							colArray2.push(colName);
							wkBlendTaskResultGrid.getColumnModel().setHidden(
									i, false);
						} else {
							wkBlendTaskResultGrid.getColumnModel().setHidden(
									i, true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
			}, 1000);
}

function add() {
	window.location = window.ctx + "/experiment/sj/wklifeblend/wkBlendTask/editWkBlendTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
	if($("#wkBlendTask_testUserOneName").val()==""){
		message("实验员不能为空！");
		return;
	}
				submitWorkflow("WkLifeBlendTask", {
					userId : userId,
					userName : userName,
					formId : $("#wkBlendTask_id").val(),
					title : $("#wkBlendTask_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});

//审批
$("#toolbarbutton_sp").click(function() {
	if($("#wkBlendTask_confirmUser_name").val()==""){
		message("审核人不能为空！");
		return;
	}
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#wkBlendTask_id").val();
	
	var options = {};
	options.width = 929;
	options.height = 534;
	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}

	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null,biolims.common.approvalTask, url, {
		"Confirm" : function() {
			
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};
				
				
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);
				
			}else if(operVal=="1"){
				if(taskName==biolims.uf.charge2modify){
					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();

					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};
					
					
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				}else{
					var codeList = new Array();
					var codeList1 = new Array();
					var selRecord1 = wkBlendTaskItemGrid.store;
					var flag1=true;
					if (wkBlendTaskResultGrid.getAllRecord().length > 0) {
						var selRecord = wkBlendTaskResultGrid.store;
						for(var j=0;j<selRecord.getCount();j++){
							codeList.push(selRecord.getAt(j).get("tempId"));
							if(selRecord.getAt(j).get("nextFlow")==""){
								message(biolims.common.notFillNextStep);
								return;
							}
						}
						for(var j=0;j<selRecord1.getCount();j++){
//							if(codeList.indexOf(selRecord1.getAt(j).get("sjfz"))==-1){
//								codeList1.push(selRecord1.getAt(j).get("sjfz"));
//								flag1=false;
//								message("有样本未完成实验！");
//							};
						}
						if(wkBlendTaskResultGrid.getModifyRecord().length > 0){
							message(biolims.common.pleaseSaveRecord);
							return;
						}
						if(flag1){
								var myMask1 = new Ext.LoadMask(Ext.getBody(), {
									msg : biolims.common.pleaseWait
								});
								myMask1.show();
								Ext.MessageBox.confirm(biolims.common.makeSure,biolims.common.pleaseMakeSure2Deal, function(button, text) {
									if (button == "yes") {
										var paramData =  {};
										paramData.oper = $("#oper").val();
										paramData.info = $("#opinion").val();

										var reqData = {
											data : JSON.stringify(paramData),
											formId : formId,
											taskId : taskId,
											userId : window.userId
										};
										
										
										_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
											location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
										}, dialogWin);
										
										
									}
								});
								myMask1.hide();
							
						}else{
							message(biolims.common.sampleUnfinished+codeList1);
						}
					}else{
						message(biolims.common.addAndSave);
						return;
					}
				}
			}
				
		},
		"查看流程图(Check flow chart)" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
	
});





function save() {
if(checkSubmit()==true){
	    var wkBlendTaskItemDivData = $("#wkBlendTaskItemdiv").data("wkBlendTaskItemGrid");
		document.getElementById('wkBlendTaskItemJson').value = commonGetModifyRecords(wkBlendTaskItemDivData);
	    var wkBlendTaskTemplateDivData = $("#wkBlendTaskTemplatediv").data("wkBlendTaskTemplateGrid");
		document.getElementById('wkBlendTaskTemplateJson').value = commonGetModifyRecords(wkBlendTaskTemplateDivData);
	    var wkBlendTaskReagentDivData = $("#wkBlendTaskReagentdiv").data("wkBlendTaskReagentGrid");
		document.getElementById('wkBlendTaskReagentJson').value = commonGetModifyRecords(wkBlendTaskReagentDivData);
	    var wkBlendTaskCosDivData = $("#wkBlendTaskCosdiv").data("wkBlendTaskCosGrid");
		document.getElementById('wkBlendTaskCosJson').value = commonGetModifyRecords(wkBlendTaskCosDivData);
	    var wkBlendTaskResultDivData = $("#wkBlendTaskResultdiv").data("wkBlendTaskResultGrid");
		document.getElementById('wkBlendTaskResultJson').value = commonGetModifyRecords(wkBlendTaskResultDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/sj/wklifeblend/wkBlendTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		

function saveItem() {
	var id = $("#wkBlendTask_id").val();
	if (!id) {
		return;
	}
	
		var wkBlendTaskItemDivData = $("#wkBlendTaskItemdiv").data("wkBlendTaskItemGrid");
		var wkBlendTaskItemJson = commonGetModifyRecords(wkBlendTaskItemDivData);
	    var wkBlendTaskTemplateDivData = $("#wkBlendTaskTemplatediv").data("wkBlendTaskTemplateGrid");
		var wkBlendTaskTemplateJson = commonGetModifyRecords(wkBlendTaskTemplateDivData);
	    var wkBlendTaskReagentDivData = $("#wkBlendTaskReagentdiv").data("wkBlendTaskReagentGrid");
		var wkBlendTaskReagentJson = commonGetModifyRecords(wkBlendTaskReagentDivData);
	    var wkBlendTaskCosDivData = $("#wkBlendTaskCosdiv").data("wkBlendTaskCosGrid");
		var wkBlendTaskCosJson = commonGetModifyRecords(wkBlendTaskCosDivData);
	    var wkBlendTaskResultDivData = $("#wkBlendTaskResultdiv").data("wkBlendTaskResultGrid");
		var wkBlendTaskResultJson = commonGetModifyRecords(wkBlendTaskResultDivData);
	
	
	ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/saveAjax.action", {
		wkBlendTaskItemJson:wkBlendTaskItemJson,
		wkBlendTaskTemplateJson:wkBlendTaskTemplateJson,
		wkBlendTaskReagentJson:wkBlendTaskReagentJson,
		wkBlendTaskCosJson:wkBlendTaskCosJson,
		wkBlendTaskResultJson:wkBlendTaskResultJson,
		id : id
	}, function(data) {
		if (data.success) {
			//message("保存成功！");
			if(data.equip!=0&&typeof(data.equip)!='undefined'){
				message(data.equip+"条设备数据保存成功！");
			}
			if(data.re!=0&&typeof(data.re)!='undefined'){
				message(data.re+"条原辅料数据保存成功！");
			}
			wkBlendTaskItemDivData.getStore().reload();
			wkBlendTaskTemplateDivData.getStore().reload();
			wkBlendTaskReagentDivData.getStore().reload();
			wkBlendTaskCosDivData.getStore().reload();
			wkBlendTaskResultDivData.getStore().reload();
		} else {
			message(biolims.common.saveFailed);
		}
	}, null);
}


function editCopy() {
	window.location = window.ctx + '/experiment/sj/wklifeblend/wkBlendTask/copyWkBlendTask.action?id=' + $("#wkBlendTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#wkBlendTask_id").val() + "&tableId=wkBlendTask");
//}
$("#toolbarbutton_status").click(function(){
	commonChangeState("formId=" + $("#wkBlendTask_id").val() + "&tableId=WkLifeBlendTask");

});


function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#wkBlendTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#wkBlendTask_template").val());
	nsc.push(biolims.common.templateEmpty);
	fs.push($("#wkBlendTask_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.wk.wkMix,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskItemList.action", {
				id : $("#wkBlendTask_id").val()
			}, "#wkBlendTaskItempage");
load("/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskTemplateList.action", {
				id : $("#wkBlendTask_id").val()
			}, "#wkBlendTaskTemplatepage");
load("/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskReagentList.action", {
				id : $("#wkBlendTask_id").val()
			}, "#wkBlendTaskReagentpage");
load("/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskCosList.action", {
				id : $("#wkBlendTask_id").val()
			}, "#wkBlendTaskCospage");
load("/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskResultList.action", {
				id : $("#wkBlendTask_id").val()
			}, "#wkBlendTaskResultpage");

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	//调用模板
	function TemplateFun(){
		var type="doWkBlendLife";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:biolims.common.selectTemplate,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
	
	function setTemplateFun(rec){
		var itemGrid=wkBlendTaskItemGrid.store;
		if(itemGrid.getCount()>0){
			for(var i=0;i<itemGrid.getCount();i++){
				itemGrid.getAt(i).set("sampleNum",rec.get('sampleNum'));
			}
		}
		if($("#wkBlendTask_acceptUser_name").val()==""){
			document.getElementById('wkBlendTask_acceptUser').value=rec.get('acceptUser-id');
			document.getElementById('wkBlendTask_acceptUser_name').value=rec.get('acceptUser-name');
		}
		var code=$("#wkBlendTask_template").val();
		if(code==""){
					document.getElementById('wkBlendTask_template').value=rec.get('id');
					document.getElementById('wkBlendTask_template_name').value=rec.get('name');
					var win = Ext.getCmp('TemplateFun');
					if(win){win.close();}
					var id=rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {
								var ob = wkBlendTaskTemplateGrid.getStore().recordType;
								wkBlendTaskTemplateGrid.stopEditing();
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("code",obj.code);
									p.set("stepName",obj.name);
									
									p.set("note",obj.note);
									wkBlendTaskTemplateGrid.getStore().add(p);							
								});
								
								wkBlendTaskTemplateGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = wkBlendTaskReagentGrid.getStore().recordType;
								wkBlendTaskReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									wkBlendTaskReagentGrid.getStore().add(p);							
								});
								
								wkBlendTaskReagentGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = wkBlendTaskCosGrid.getStore().recordType;
								wkBlendTaskCosGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("type-id",obj.typeId);
									p.set("type-name",obj.typeName);
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									wkBlendTaskCosGrid.getStore().add(p);							
								});			
								wkBlendTaskCosGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null);

			
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
						var ob1 = wkBlendTaskTemplateGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id"); 
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/delWkBlendTaskTemplateOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null);
								}else{								
									wkBlendTaskTemplateGrid.store.removeAll();
								}
							}
							wkBlendTaskTemplateGrid.store.removeAll();
		 				}

						var ob2 = wkBlendTaskReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");

								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/delWkBlendTaskReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message(biolims.common.deleteSuccess);
									} else {
										message(biolims.common.deleteFailed);
									}
								}, null); 
								}else{
									wkBlendTaskReagentGrid.store.removeAll();
								}
							}
							wkBlendTaskReagentGrid.store.removeAll();
		 				}
						//=========================================
						var ob3 = wkBlendTaskCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");
								
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/delWkBlendTaskCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null); 
								}else{
									wkBlendTaskCosGrid.store.removeAll();
								}
							}
							wkBlendTaskCosGrid.store.removeAll();
		 				}
						document.getElementById('wkBlendTask_template').value=rec.get('id');
						document.getElementById('wkBlendTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = wkBlendTaskTemplateGrid.getStore().recordType;
									wkBlendTaskTemplateGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("stepName",obj.name);
										
										p.set("note",obj.note);
										wkBlendTaskTemplateGrid.getStore().add(p);							
									});
									
									wkBlendTaskTemplateGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = wkBlendTaskReagentGrid.getStore().recordType;
									wkBlendTaskReagentGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tReagent",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("oneNum",obj.num);
										p.set("note",obj.note);
										wkBlendTaskReagentGrid.getStore().add(p);							
									});
									
									wkBlendTaskReagentGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = wkBlendTaskCosGrid.getStore().recordType;
									wkBlendTaskCosGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tCos",obj.id);
										p.set("type-id",obj.typeId);
										p.set("type-name",obj.typeName);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										p.set("state",obj.state);
										p.set("temperature",obj.temperature);
										p.set("speed",obj.speed);
										p.set("time",obj.time);
										p.set("note",obj.note);
										wkBlendTaskCosGrid.getStore().add(p);							
									});			
									wkBlendTaskCosGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
						}
					}
		reloadWkBlendTaskItem(rec.get("templateFieldsCode"), rec
				.get("templateFieldsItemCode"));


}
	//按条件加载原辅料
	function showReagent(){
		//获取全部数据
		var allRcords=wkBlendTaskTemplateGrid.store;
		var flag=true;
		for(var h=0;h<allRcords.getCount();h++){
			var ida = allRcords.getAt(h).get("id");
			if(ida==undefined){
				flag=false;
			}
		}
		if(!flag){
			message(biolims.common.pleaseHold);
			return;
		}
		//获取选择的数据
		var selectRcords=wkBlendTaskTemplateGrid.getSelectionModel().getSelections();
		//获取全部数据
		var allRcords=wkBlendTaskTemplateGrid.store;
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid =$("#wkBlendTask_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskReagentList.action", {
				id : $("#wkBlendTask_id").val()
			}, "#sangerTaskTemplateReagentpage");
		}else if(length1==1){
			wkBlendTaskReagentGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/setReagent.action", {
				tid:tid,code : code
			}, function(data) {			
				if (data.success) {	
					var ob = wkBlendTaskReagentGrid.getStore().recordType;
					wkBlendTaskReagentGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;					
						p.set("id",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("batch",obj.batch);
						p.set("num",obj.num);
						p.set("expireDate",obj.expireDate);
						p.set("oneNum",obj.oneNum);
						p.set("sampleNum",obj.sampleNum);
						p.set("note",obj.note);
						p.set("sn",obj.sn);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("tReagent",obj.tReagent);
						p.set("wkblendTask-id",obj.tId);
						p.set("wkblendTask-name",obj.tName);					
						wkBlendTaskReagentGrid.getStore().add(p);							
					});
					wkBlendTaskReagentGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
			});		
		}else{
			message(biolims.common.selectRecord);
			return;
		}	
	}

	//按条件加载设备
	function showCos(){
		var allRcords=wkBlendTaskTemplateGrid.store;
		var flag=true;
		for(var h=0;h<allRcords.getCount();h++){
			var ida = allRcords.getAt(h).get("id");
			if(ida==undefined){
				flag=false;
			}
		}
		if(!flag){
			message(biolims.common.pleaseHold);
			return;
		}
		//获取选择的数据
		var selectRcords=wkBlendTaskTemplateGrid.getSelectionModel().getSelections();
		//获取全部数据
		var allRcords=wkBlendTaskTemplateGrid.store;
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid =$("#wkBlendTask_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskCosList.action", {
				id : $("#wkBlendTask_id").val()
			}, "#wkBlendTaskCospage");
		}else if(length1==1){
			wkBlendTaskCosGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/sj/wklifeblend/wkBlendTask/setCos.action", {
				tid:tid,code : code
			}, function(data) {			
				if (data.success) {	
					var ob = wkBlendTaskCosGrid.getStore().recordType;
					wkBlendTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;					
						p.set("id",obj.id);
						p.set("code",obj.code);
						p.set("state",obj.state);
						p.set("name",obj.name);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("type-id",obj.typeId);
						p.set("type-name",obj.typeName);
						p.set("note",obj.note);
						p.set("time",obj.time);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("tCos",obj.tCos);
						p.set("wkblendTask-id",obj.tId);
						p.set("wkblendTask-name",obj.tName);					
						wkBlendTaskCosGrid.getStore().add(p);							
					});
					wkBlendTaskCosGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.pleaseHold);
				}
			}, null);
			});		
		}else{
			message(biolims.common.selectRecord);
			return;
		}
	}		
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.rollback
			});
		item.on('click', ckcrk);
		
		});
	function ckcrk(){
		
		Ext.MessageBox.confirm(biolims.common.prompt, biolims.common.initTaskList, function(button, text) {
			if (button == "yes") {
				var selRecord = wkBlendTaskResultGrid.store;
				
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message(biolims.common.backSuccess);
							} else {
								message(biolims.common.rollbackFailed);
							}
						}, null);
					}
					
				}
			}
		});
	}
	
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.save
			});
		item.on('click', ckcrk2);
		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text:biolims.common.handleRollback
			});
		item.on('click', ckcrk3);
		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: biolims.common.handlingRollback, progressText:biolims.common.handling, width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "WkBlendTask",id : $("#wkBlendTask_id").val()
		}, function(data) {
			if (data.success) {	
				message(biolims.common.handleRollbackSuccess);
			} else {
				message(biolims.common.handleRollbackFailed);
			}
		}, null);
	}



	
	
//	var loadtestUser;
	//选择实验组用户
	function testUser(type){
		var gid=$("#wkBlendTask_acceptUser").val();
		if(gid!=""){
			var options = {};
			options.width = 500;
			options.height = 500;
			loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				"Confirm" : function() {
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						var id=[];
						var name=[];
						for(var i=0;i<selectRecord.length;i++){
							id.push(selectRecord[i].get("user-id"));
							name.push(selectRecord[i].get("user-name"));
						}
						if(type==1){//第一批实验员
							$("#wkBlendTask_testUserOneId").val(id);
							$("#wkBlendTask_testUserOneName").val(name);
						}else if(type==2){//第二批实验员
							$("#wkBlendTask_testUserTwoId").val(id);
							$("#wkBlendTask_testUserTwoName").val(name);
						}else if(type==3){//审核人
							$("#wkBlendTask_confirmUser").val(id);
							$("#wkBlendTask_confirmUser_name").val(name);
						}
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
		}else{
			message(biolims.common.pleaseSelectGroup);
		}
		
	}
//	function setUserGroupUser(){
//		var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
//		var selectRecord = operGrid.getSelectionModel().getSelections();
//		if (selectRecord.length > 0) {
//				$("#wkBlendTask_testUser").val(selectRecord[0].get("user-id"));
//				$("#wkBlendTask_testUser_name").val(selectRecord[0].get("user-name"));
//		}else{
//			message(biolims.common.selectYouWant);
//			return;
//		}
//		loadtestUser.dialog("close");
//	}
	
	function selectTemp(){
		var options = {};
		options.width = document.body.clientWidth*0.9;
		options.height = document.body.clientHeight*0.9;
		var url = "/experiment/sj/wklifeblend/wkBlendTask/showWkBlendTaskTempList.action";
		loadDialogPage(null, biolims.common.approvalTask, url, {}, true, options);
	}
