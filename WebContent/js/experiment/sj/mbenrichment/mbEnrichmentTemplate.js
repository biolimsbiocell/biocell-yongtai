var mbEnrichmentTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'mbEnrichment-id',
		type:"string"
	});
	    fields.push({
		name:'mbEnrichment-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.stepDetail,
		width:100*6,		
		editor : new Ext.form.HtmlEditor({
			readOnly : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*10
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤编id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'步骤编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:biolims.common.startTime,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:biolims.common.endTime,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:biolims.common.relateSample,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mbEnrichment-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mbEnrichment-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.executionStep;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
//       opts.delSelect = function(ids) {
//		ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentTemplate.action", {
//			ids : ids
//		}, function(data) {
//			if (data.success) {
//				mbEnrichmentTemplateGrid.getStore().commitChanges();
//				mbEnrichmentTemplateGrid.getStore().reload();
//				message("删除成功！");
//			} else {
//				message("删除失败！");
//			}
//		}, null);
//	};
//	opts.tbar.push({
//			text : '选择实验员',
//				handler : selectreciveUserFun
//		});
//	opts.tbar.push({
//			text : '选择相关主表',
//				handler : selectmbEnrichmentDialogFun
//		});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = mbEnrichmentTemplateGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//                	if(n>0){
//                		if(this[0]){
//                			var p = new ob({});
//                			p.isNew = true;				
//                			var o;
//                			o= 0-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 1-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 2-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 3-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 4-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 5-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 6-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 7-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 8-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 9-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 10-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 11-1;
//                			p.set("po.fieldName",this[o]);
//							mbEnrichmentTemplateGrid.getStore().insert(0, p);
//                		}
//                	}
//                     n = n +1;
//                	
//                });
//    	}
//	}
//	opts.tbar.push({
//		text : '显示可编辑列',
//		handler : null
//	});
//	opts.tbar.push({
//		text : '取消选中',
//		handler : null
//	});
	var state = $("#mbEnrichment_state").val();
	if(state&&state!='1'){
		opts.delSelect = function(ids) {
			ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentTemplate.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					mbEnrichmentTemplateGrid.getStore().commitChanges();
					mbEnrichmentTemplateGrid.getStore().reload();
					message("删除成功！");
				} else {
					message("删除失败！");
				}
			}, null);
		};
//	opts.tbar.push({
//		iconCls : 'application_print',
//		text : biolims.common.printList,
//		handler : stampOrder
//	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//		iconCls : 'add',
//		text : biolims.common.fillDetail,
//		handler : templateSelect
//	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
	opts.tbar.push({
		text : biolims.common.testUserName,
		handler : loadTestUser
	});
//	if($("#mbEnrichment_id").val()&&$("#mbEnrichment_id").val()!="NEW"){
//		opts.tbar.push({
//			iconCls : 'save',
//			text : biolims.common.save,
//			handler : saveItem
//		});
//			
//		
//	}
	}
	mbEnrichmentTemplateGrid=gridEditTable("mbEnrichmentTemplatediv",cols,loadParam,opts);
	$("#mbEnrichmentTemplatediv").data("mbEnrichmentTemplateGrid", mbEnrichmentTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectreciveUserFun(){
	var win = Ext.getCmp('selectreciveUser');
	if (win) {win.close();}
	var selectreciveUser= new Ext.Window({
	id:'selectreciveUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectreciveUser.close(); 
		 }  
	}]  
	});  
    selectreciveUser.show(); 
    }
    
	function setreciveUser(rec){
		var gridGrid = $("#mbEnrichmentTemplatediv").data("mbEnrichmentTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reciveUser-id',rec.get('id'));
			obj.set('reciveUser-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectreciveUser')
		if(win){
			win.close();
		}
	}
//	function selectreciveUserDialogFun(){
//			var title = '';
//			var url = '';
//			title = "选择实验员";
//			url = ctx + "/UserSelect.action?flag=reciveUser";
//			var option = {};
//			option.width = document.body.clientWidth-30;
//			option.height = document.body.clientHeight-160;
//			loadDialogPage(null, title, url, {
//				"确定" : function() {
//						selreciveUserVal(this);
//				}
//			}, true, option);
//		}
	var selreciveUserVal = function(win) {
		var operGrid = userDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#mbEnrichmentTemplatediv").data("mbEnrichmentTemplateGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('reciveUser-id',rec.get('id'));
				obj.set('reciveUser-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
function selectmbEnrichmentFun(){
	var win = Ext.getCmp('selectmbEnrichment');
	if (win) {win.close();}
	var selectmbEnrichment= new Ext.Window({
	id:'selectmbEnrichment',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectmbEnrichment.close(); }  }]  }) });  
    selectmbEnrichment.show(); }
	function setmbEnrichment(rec){
		var gridGrid = $("#mbEnrichmentTemplatediv").data("mbEnrichmentTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('mbEnrichment-id',rec.get('id'));
			obj.set('mbEnrichment-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectmbEnrichment')
		if(win){
			win.close();
		}
	}
	function selectmbEnrichmentDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/MbEnrichmentSelect.action?flag=mbEnrichment";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selmbEnrichmentVal(this);
				}
			}, true, option);
		}
	var selmbEnrichmentVal = function(win) {
		var operGrid = mbEnrichmentDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#mbEnrichmentTemplatediv").data("mbEnrichmentTemplateGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('mbEnrichment-id',rec.get('id'));
				obj.set('mbEnrichment-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
	//查询实验员
	function loadTestUser(){
			var gid=$("#mbEnrichment_acceptUser").val();
			var options = {};
			options.width = 500;
			options.height = 500;
			loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				"Confirm" : function() {
					var gridGrid = $("#mbEnrichmentTemplatediv").data("mbEnrichmentTemplateGrid");
					var selRecords = gridGrid.getSelectionModel().getSelections(); 
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						var id=[];
						var name=[];
						for(var i=0;i<selectRecord.length;i++){
							id.push(selectRecord[i].get("user-id"));
							name.push(selectRecord[i].get("user-name"));
						}
						for(var j=0;j<selRecords.length;j++){
							selRecords[j].set("reciveUser-id",id.toString());
							selRecords[j].set("reciveUser-name",name.toString());}
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
	}
	
	//获取开始时的时间
	function getStartTime(){
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=mbEnrichmentTemplateGrid.getSelectionModel();
//		var setNum = dnaTemplateReagentGrid.store;
		var selectRecords = mbEnrichmentTemplateGrid.getSelectionModel();
		if(selectRecords.getSelections().length>0){
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
//				for(var i=0; i<setNum.getCount();i++){
//					var num = setNum.getAt(i).get("itemId");
//					if(num==obj.get("code")){
//						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
//					}
//				}
			});		
		}else{
			message(biolims.common.selectStepNum);
		}
		var selectRecord=mbEnrichmentItemGrid.getSelectionModel();
		var selRecord=mbEnrichmentTemplateGrid.getSelectRecord();
		var sampleCodes = "";
		$.each(selectRecord.getSelections(), function(i, obj) {
			sampleCodes += obj.get("hhzh")+",";
		});
		$.each(selRecord, function(i, obj) {
			obj.set("sampleCodes", sampleCodes);
		});
		}else{
		    message(biolims.common.pleaseSelectSamples);
	    }
	}

	//获取停止时的时间
	function getEndTime(){
			var setRecord=mbEnrichmentItemGrid.store;
			var d = new Date();
			var getIndex= mbEnrichmentTemplateGrid.store;
			var getIndexs = mbEnrichmentTemplateGrid.getSelectionModel().getSelections();
			var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
			var selectRecord=mbEnrichmentTemplateGrid.getSelectionModel();
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					obj.set("endTime",str);
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
//					for(var i=0; i<setRecord.getCount(); i++){
//						for(var j=0; j<scode.length; j++){
//							if(scode[j]==setRecord.getAt(i).get("code")){
//								setRecord.getAt(i).set("stepNum",obj.get("code"));
//							}
//						}
//					}
					getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
				});
			}
			else{
				message(biolims.common.selectStepNum);
			}
	}
	function addSuccess(){

		var num =$("#mbEnrichment_template").val();
			if(num!=""){
//				var setNum = sequencingReagentGrid.store;
//				var selectRecords = sequencingItemGrid.store;
//					for(var i=0;i<setNum.getCount();i++){
//						setNum.getAt(i).set("sampleNum",selectRecords.getCount());
//				}


		
		var getRecord1 = mbEnrichmentItemGrid.store;
//		for(var j=0;j<getRecord1.getCount();j++){
//			var productNum=getRecord1.getAt(j).get("dataDemand");
//			if(productNum==""){
//				message("请填写分割数量");
//				return;
//			}
//		}
		var getRecord = mbEnrichmentItemGrid.getSelectionModel().getSelections();
		var selectRecord = mbEnrichmentTemplateGrid.getSelectionModel();
		var selRecord = mbEnrichmentResultGrid.store;
		if(selectRecord.getSelections().length > 0){
			$.each(selectRecord.getSelections(), function(i, obj) {
				var isRepeat = false;
				var codes = obj.get("sampleCodes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<scode.length; i++){
					for(var j=0;j<selRecord.getCount();j++){
						var getv = scode[i];
						var setv = selRecord.getAt(j).get("hhzh");
						if(getv == setv){
							isRepeat = true;
							message(biolims.common.haveDuplicate);
							break;					
						}
					}
				}
				if(!isRepeat){
					$.each(getRecord,function(a,b){

//						var productNum=b.get("dataDemand");
//							for(var k=1;k<=productNum;k++){
								var ob = mbEnrichmentResultGrid.getStore().recordType;
								mbEnrichmentResultGrid.stopEditing();
								var p = new ob({});
								p.isNew = true;
								p.set("tempId",b.get("id"));
								p.set("fjwk",b.get("fjwk"));
								p.set("hhzh",b.get("hhzh"));
								p.set("tl",b.get("tl"));
								p.set("wknd",b.get("wknd"));
								p.set("wkpdcd",b.get("wkpdcd"));
								p.set("cxlx",b.get("cxlx"));
								p.set("cxpt",b.get("cxpt"));
								p.set("cxdc",b.get("cxdc"));
								p.set("note",b.get("note"));
								p.set("tzhwknd",b.get("tzhwknd"));
								p.set("tzhwktj",b.get("tzhwktj"));
								p.set("dycxsbs",b.get("dycxsbs"));
								p.set("dycbsl",b.get("dycbsl"));
								p.set("decxsbs",b.get("decxsbs"));
								p.set("decbsl",b.get("decbsl"));
								p.set("pmol",b.get("pmol"));
								
								p.set("sampleCode",b.get("sampleCode"));
								mbEnrichmentResultGrid.getStore().add(p);
								mbEnrichmentResultGrid.startEditing(0,0);
//							}
							message(biolims.common.generateResultsSuccess);
					
					});
				
			}
			});
		}else{
			var selRecord = mbEnrichmentResultGrid.store;
			var flag;
			
			var getRecord = mbEnrichmentItemGrid.getAllRecord();
			
				flag = true;
				for(var i=0;i<getRecord.length;i++){
					for(var j1=0;j1<selRecord.getCount();j1++){
						var getv = getRecord[i].get("hhzh");
						var setv = selRecord.getAt(j1).get("hhzh");
						if(getv == setv){
							flag = false;
							message(biolims.common.haveDuplicate);
							break;					
						}
					}
				}
				
				if(flag==true){
					$.each(getRecord,function(a,b){
//					var productNum=b.get("dataDemand");
//						for(var k=1;k<=productNum;k++){
							var ob = mbEnrichmentResultGrid.getStore().recordType;
							mbEnrichmentResultGrid.stopEditing();
							var p = new ob({});
							p.isNew = true;
							p.set("tempId",b.get("id"));
							p.set("fjwk",b.get("fjwk"));
							p.set("hhzh",b.get("hhzh"));
							p.set("tl",b.get("tl"));
							p.set("wknd",b.get("wknd"));
							p.set("wkpdcd",b.get("wkpdcd"));
							p.set("cxlx",b.get("cxlx"));
							p.set("cxpt",b.get("cxpt"));
							p.set("cxdc",b.get("cxdc"));
							p.set("note",b.get("note"));
							p.set("tzhwknd",b.get("tzhwknd"));
							p.set("tzhwktj",b.get("tzhwktj"));
							p.set("dycxsbs",b.get("dycxsbs"));
							p.set("dycbsl",b.get("dycbsl"));
							p.set("decxsbs",b.get("decxsbs"));
							p.set("decbsl",b.get("decbsl"));
							p.set("pmol",b.get("pmol"));
							
							p.set("sampleCode",b.get("sampleCode"));
							mbEnrichmentResultGrid.getStore().add(p);
							mbEnrichmentResultGrid.startEditing(0,0);
//						}
						message(biolims.common.generateResultsSuccess);
					});
				}
		
		}

			}else{
				message(biolims.common.pleaseSelectTemplate);
			}
	}
