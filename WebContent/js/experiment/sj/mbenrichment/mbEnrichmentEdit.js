function changeWidth(){
	
	$("#mbEnrichmentTemppage").css("width","40%");
	$("#markup").css("width","60%");
	wkBlendTaskTempGrid.getStore().reload();
	
	
}
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#mbEnrichment_state").val();
	var stateName = $("#mbEnrichment_stateName").val();
	var method = $("#mbEnrichment_method").val();
	if(id =="3"||stateName==biolims.common.toModify){
//		load("/experiment/sj/wkblend/wkBlendTask/showWkBlendTaskTempList.action", null,  "#wkBlendTaskTemppage");
//		$("#markup").css("width","75%");

		if(method=='window'){
			$("#mbEnrichmentTemppage").remove();
			$("#markup").css("width","100%");
			selectTemp();
		}else{
			load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentTempList.action", null,  "#mbEnrichmentTemppage");
			$("#markup").css("width","75%");
		}
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#mbEnrichmentTemppage").remove();
	}
	
	var mttf = $("#mbEnrichment_template_templateFieldsCode").val();
	var mttfItem = $("#mbEnrichment_template_templateFieldsItemCode").val();
	reloadWkBlendTaskItem(mttf, mttfItem);
});		
function reloadWkBlendTaskItem(mttf, mttfItem) {
	setTimeout(
			function() {
				if (mttf == null || mttf == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = mbEnrichmentItemGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttf.indexOf(colName) >= 0) {
							colArray2.push(colName);
							mbEnrichmentItemGrid.getColumnModel().setHidden(i,
									false);
						} else {
							mbEnrichmentItemGrid.getColumnModel().setHidden(i,
									true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
				if (mttfItem == null || mttfItem == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = mbEnrichmentResultGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttfItem.indexOf(colName) >= 0) {
							colArray2.push(colName);
							mbEnrichmentResultGrid.getColumnModel().setHidden(
									i, false);
						} else {
							mbEnrichmentResultGrid.getColumnModel().setHidden(
									i, true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
			}, 1000);
}
function add() {
	window.location = window.ctx + "/experiment/sj/mbenrichment/mbEnrichment/editMbEnrichment.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("MbEnrichment", {
					userId : userId,
					userName : userName,
					formId : $("#mbEnrichment_id").val(),
					title : $("#mbEnrichment_name").val()
				}, function() {
					window.open(window.location,'_self');;
				});
				
});
$("#toolbarbutton_sp").click(function() {
	if($("#mbEnrichment_confirmUser_name").val()==""){
		message(biolims.sample.pleaseSelectReviewer);
		return;
	}
	if(mbEnrichmentResultGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var grid = mbEnrichmentResultGrid.store;
	for(var i=0;i<grid.getCount();i++){
		if(grid.getAt(i).get("result")==""){
			message(biolims.common.taskReults);
			return;
		}
	}
	completeTask($("#mbEnrichment_id").val(), $(this).attr("taskId"), function() {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
	});
});
////审批
//$("#toolbarbutton_sp").click(function() {
//	if($("#mbEnrichment_confirmUser_name").val()==""){
//		message("审核人不能为空！");
//		return;
//	}
//	var taskName=$("#taskName").val();
//	var taskId =  $(this).attr("taskId");
//	var formId=$("#mbEnrichment_id").val();
//	
//	var options = {};
//	options.width = 929;
//	options.height = 534;
//	
//	if (window.ActiveXObject) {
//		// IE浏览器
//		options.height = options.height + "px";
//	}
//
//	options.data = {};
//	options.data.taskId = taskId;
//	options.data.formId = formId;
//	var url = "/workflow/processinstance/toCompleteTaskView.action";
//	var dialogWin = loadDialogPage(null,biolims.common.approvalTask, url, {
//		"Confirm" : function() {
//			
//			var operVal = $("#oper").val();
//			if(operVal=="0"){
//				var paramData = {};
//				paramData.oper = $("#oper").val();
//				paramData.info = $("#opinion").val();
//
//				var reqData = {
//					data : JSON.stringify(paramData),
//					formId : formId,
//					taskId : taskId,
//					userId : window.userId
//				};
//				
//				
//				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
//				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
//			}, dialogWin);
//				
//			}else if(operVal=="1"){
//				if(taskName==biolims.uf.charge2modify){
//					var paramData = {};
//					paramData.oper = $("#oper").val();
//					paramData.info = $("#opinion").val();
//
//					var reqData = {
//						data : JSON.stringify(paramData),
//						formId : formId,
//						taskId : taskId,
//						userId : window.userId
//					};
//					
//					
//					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
//					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
//				}, dialogWin);
//				}else{
//					var codeList = new Array();
//					var codeList1 = new Array();
//					var selRecord1 = mbEnrichmentItemGrid.store;
//					var flag1=true;
//					if (mbEnrichmentResultGrid.getAllRecord().length > 0) {
//						var selRecord = mbEnrichmentResultGrid.store;
//						for(var j=0;j<selRecord.getCount();j++){
//							codeList.push(selRecord.getAt(j).get("tempId"));
//							if(selRecord.getAt(j).get("nextFlow")==""){
//								message(biolims.common.notFillNextStep);
//								return;
//							}
//						}
//						for(var j=0;j<selRecord1.getCount();j++){
////							if(codeList.indexOf(selRecord1.getAt(j).get("sjfz"))==-1){
////								codeList1.push(selRecord1.getAt(j).get("sjfz"));
////								flag1=false;
////								message("有样本未完成实验！");
////							};
//						}
//						if(mbEnrichmentResultGrid.getModifyRecord().length > 0){
//							message(biolims.common.pleaseSaveRecord);
//							return;
//						}
//						if(flag1){
//								var myMask1 = new Ext.LoadMask(Ext.getBody(), {
//									msg : biolims.common.pleaseWait
//								});
//								myMask1.show();
//								Ext.MessageBox.confirm(biolims.common.makeSure,biolims.common.pleaseMakeSure2Deal, function(button, text) {
//									if (button == "yes") {
//										var paramData =  {};
//										paramData.oper = $("#oper").val();
//										paramData.info = $("#opinion").val();
//
//										var reqData = {
//											data : JSON.stringify(paramData),
//											formId : formId,
//											taskId : taskId,
//											userId : window.userId
//										};
//										
//										
//										_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
//											location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
//										}, dialogWin);
//										
//										
//									}
//								});
//								myMask1.hide();
//							
//						}else{
//							message(biolims.common.sampleUnfinished+codeList1);
//						}
//					}else{
//						message(biolims.common.addAndSave);
//						return;
//					}
//				}
//			}
//				
//		},
//		"查看流程图(Check flow chart)" : function() {
//			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
//			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
//		}
//	}, true, options);
//	
//});
//$("#toolbarbutton_sp").click(function() {
//		completeTask($("#mbEnrichment_id").val(), $(this).attr("taskId"), function() {
//			document.getElementById('toolbarSaveButtonFlag').value = 'save';
//			location.href = window.ctx + '/dashboard/toDashboard.action';
//		});
//});






function save() {
if(checkSubmit()==true){
	    var mbEnrichmentItemDivData = $("#mbEnrichmentItemdiv").data("mbEnrichmentItemGrid");
		document.getElementById('mbEnrichmentItemJson').value = commonGetModifyRecords(mbEnrichmentItemDivData);
	    var mbEnrichmentTemplateDivData = $("#mbEnrichmentTemplatediv").data("mbEnrichmentTemplateGrid");
		document.getElementById('mbEnrichmentTemplateJson').value = commonGetModifyRecords(mbEnrichmentTemplateDivData);
	    var mbEnrichmentReagentDivData = $("#mbEnrichmentReagentdiv").data("mbEnrichmentReagentGrid");
		document.getElementById('mbEnrichmentReagentJson').value = commonGetModifyRecords(mbEnrichmentReagentDivData);
	    var mbEnrichmentCosDivData = $("#mbEnrichmentCosdiv").data("mbEnrichmentCosGrid");
		document.getElementById('mbEnrichmentCosJson').value = commonGetModifyRecords(mbEnrichmentCosDivData);
	    var mbEnrichmentResultDivData = $("#mbEnrichmentResultdiv").data("mbEnrichmentResultGrid");
		document.getElementById('mbEnrichmentResultJson').value = commonGetModifyRecords(mbEnrichmentResultDivData);
//	    var mbEnrichmentTempDivData = $("#mbEnrichmentTempdiv").data("mbEnrichmentTempGrid");
//		document.getElementById('mbEnrichmentTempJson').value = commonGetModifyRecords(mbEnrichmentTempDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/sj/mbenrichment/mbEnrichment/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/sj/mbenrichment/mbEnrichment/copyMbEnrichment.action?id=' + $("#mbEnrichment_id").val();
}
$("#toolbarbutton_status").click(function(){
	commonChangeState("formId=" + $("#mbEnrichment_id").val() + "&tableId=MbEnrichment");
});
//function changeState() {
//	commonChangeState("formId=" + $("#mbEnrichment_id").val() + "&tableId=mbEnrichment");
//}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#mbEnrichment_id").val());
	nsc.push(biolims.common.codeNotEmpty);
	fs.push($("#mbEnrichment_template").val());
	nsc.push(biolims.common.templateEmpty);
	fs.push($("#mbEnrichment_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.template1,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentItemList.action", {
				id : $("#mbEnrichment_id").val()
			}, "#mbEnrichmentItempage");
load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentTemplateList.action", {
				id : $("#mbEnrichment_id").val()
			}, "#mbEnrichmentTemplatepage");
load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentReagentList.action", {
				id : $("#mbEnrichment_id").val()
			}, "#mbEnrichmentReagentpage");
load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentCosList.action", {
				id : $("#mbEnrichment_id").val()
			}, "#mbEnrichmentCospage");
load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentResultList.action", {
				id : $("#mbEnrichment_id").val()
			}, "#mbEnrichmentResultpage");
//load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentTempList.action", {
//				id : $("#mbEnrichment_id").val()
//			}, "#mbEnrichmentTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text:biolims.common.copy
						});
	item.on('click', editCopy);
	
	//调用模板
	function TemplateFun(){
		var type="doMbEnrichment";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:biolims.common.selectTemplate,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
	
	function setTemplateFun(rec){
		var itemGrid=mbEnrichmentItemGrid.store;
		if(itemGrid.getCount()>0){
			for(var i=0;i<itemGrid.getCount();i++){
				itemGrid.getAt(i).set("sampleNum",rec.get('sampleNum'));
			}
		}
		if($("#mbEnrichment_acceptUser_name").val()==""){
			document.getElementById('mbEnrichment_acceptUser').value=rec.get('acceptUser-id');
			document.getElementById('mbEnrichment_acceptUser_name').value=rec.get('acceptUser-name');
		}
		var code=$("#mbEnrichment_template").val();
		if(code==""){
					document.getElementById('mbEnrichment_template').value=rec.get('id');
					document.getElementById('mbEnrichment_template_name').value=rec.get('name');
					var win = Ext.getCmp('TemplateFun');
					if(win){win.close();}
					var id=rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {
								var ob = mbEnrichmentTemplateGrid.getStore().recordType;
								mbEnrichmentTemplateGrid.stopEditing();
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("code",obj.code);
									p.set("stepName",obj.name);
									
									p.set("note",obj.note);
									mbEnrichmentTemplateGrid.getStore().add(p);							
								});
								
								mbEnrichmentTemplateGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = mbEnrichmentReagentGrid.getStore().recordType;
								mbEnrichmentReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									mbEnrichmentReagentGrid.getStore().add(p);							
								});
								
								mbEnrichmentReagentGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = mbEnrichmentCosGrid.getStore().recordType;
								mbEnrichmentCosGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("type-id",obj.typeId);
									p.set("type-name",obj.typeName);
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									mbEnrichmentCosGrid.getStore().add(p);							
								});			
								mbEnrichmentCosGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null);

			
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
						var ob1 = mbEnrichmentTemplateGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id"); 
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentTemplate.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null);
								}else{								
									mbEnrichmentTemplateGrid.store.removeAll();
								}
							}
							mbEnrichmentTemplateGrid.store.removeAll();
		 				}

						var ob2 = mbEnrichmentReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");

								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentReagent.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message(biolims.common.deleteSuccess);
									} else {
										message(biolims.common.deleteFailed);
									}
								}, null); 
								}else{
									mbEnrichmentReagentGrid.store.removeAll();
								}
							}
							mbEnrichmentReagentGrid.store.removeAll();
		 				}
						//=========================================
						var ob3 = mbEnrichmentCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");
								
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentCos.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null); 
								}else{
									mbEnrichmentCosGrid.store.removeAll();
								}
							}
							mbEnrichmentCosGrid.store.removeAll();
		 				}
						document.getElementById('wkBlendTask_template').value=rec.get('id');
						document.getElementById('wkBlendTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = mbEnrichmentTemplateGrid.getStore().recordType;
									mbEnrichmentTemplateGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("stepName",obj.name);
										
										p.set("note",obj.note);
										mbEnrichmentTemplateGrid.getStore().add(p);							
									});
									
									mbEnrichmentTemplateGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = mbEnrichmentReagentGrid.getStore().recordType;
									mbEnrichmentReagentGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tReagent",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("oneNum",obj.num);
										p.set("note",obj.note);
										mbEnrichmentReagentGrid.getStore().add(p);							
									});
									
									mbEnrichmentReagentGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = mbEnrichmentCosGrid.getStore().recordType;
									mbEnrichmentCosGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tCos",obj.id);
										p.set("type-id",obj.typeId);
										p.set("type-name",obj.typeName);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										p.set("state",obj.state);
										p.set("temperature",obj.temperature);
										p.set("speed",obj.speed);
										p.set("time",obj.time);
										p.set("note",obj.note);
										mbEnrichmentCosGrid.getStore().add(p);							
									});			
									mbEnrichmentCosGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
						}
					}
		reloadWkBlendTaskItem(rec.get("templateFieldsCode"), rec
				.get("templateFieldsItemCode"));


}
	//按条件加载原辅料
	function showReagent(){
		//获取全部数据
		var allRcords=mbEnrichmentTemplateGrid.store;
		var flag=true;
		for(var h=0;h<allRcords.getCount();h++){
			var ida = allRcords.getAt(h).get("id");
			if(ida==undefined){
				flag=false;
			}
		}
		if(!flag){
			message(biolims.common.pleaseHold);
			return;
		}
		//获取选择的数据
		var selectRcords=mbEnrichmentTemplateGrid.getSelectionModel().getSelections();
		//获取全部数据
		var allRcords=mbEnrichmentTemplateGrid.store;
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid =$("#mbEnrichment_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentReagentList.action", {
				id : $("#mbEnrichment_id").val()
			}, "#mbEnrichmentReagentpage");
		}else if(length1==1){
			mbEnrichmentReagentGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/setReagent.action", {
				tid:tid,code : code
			}, function(data) {			
				if (data.success) {	
					var ob = mbEnrichmentReagentGrid.getStore().recordType;
					mbEnrichmentReagentGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;					
						p.set("id",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("batch",obj.batch);
						p.set("num",obj.num);
						p.set("expireDate",obj.expireDate);
						p.set("oneNum",obj.oneNum);
						p.set("sampleNum",obj.sampleNum);
						p.set("note",obj.note);
						p.set("sn",obj.sn);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("tReagent",obj.tReagent);
						p.set("mbEnrichment-id",obj.tId);
						p.set("mbEnrichment-name",obj.tName);					
						mbEnrichmentReagentGrid.getStore().add(p);							
					});
					mbEnrichmentReagentGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
			});		
		}else{
			message(biolims.common.selectRecord);
			return;
		}	
	}

	//按条件加载设备
	function showCos(){
		var allRcords=mbEnrichmentTemplateGrid.store;
		var flag=true;
		for(var h=0;h<allRcords.getCount();h++){
			var ida = allRcords.getAt(h).get("id");
			if(ida==undefined){
				flag=false;
			}
		}
		if(!flag){
			message(biolims.common.pleaseHold);
			return;
		}
		//获取选择的数据
		var selectRcords=mbEnrichmentTemplateGrid.getSelectionModel().getSelections();
		//获取全部数据
		var allRcords=mbEnrichmentTemplateGrid.store;
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid =$("#mbEnrichment_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentCosList.action", {
				id : $("#mbEnrichment_id").val()
			}, "#mbEnrichmentCospage");
		}else if(length1==1){
			mbEnrichmentCosGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/setCos.action", {
				tid:tid,code : code
			}, function(data) {			
				if (data.success) {	
					var ob = mbEnrichmentCosGrid.getStore().recordType;
					mbEnrichmentCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;					
						p.set("id",obj.id);
						p.set("code",obj.code);
						p.set("state",obj.state);
						p.set("name",obj.name);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("type-id",obj.typeId);
						p.set("type-name",obj.typeName);
						p.set("note",obj.note);
						p.set("time",obj.time);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("tCos",obj.tCos);
						p.set("mbEnrichment-id",obj.tId);
						p.set("mbEnrichment-name",obj.tName);					
						mbEnrichmentCosGrid.getStore().add(p);							
					});
					mbEnrichmentCosGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.pleaseHold);
				}
			}, null);
			});		
		}else{
			message(biolims.common.selectRecord);
			return;
		}
	}	
	
	//选择实验组用户
	function testUser(type){
		var gid=$("#mbEnrichment_acceptUser").val();
		if(gid!=""){
			var options = {};
			options.width = 500;
			options.height = 500;
			loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				"Confirm" : function() {
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						var id=[];
						var name=[];
						for(var i=0;i<selectRecord.length;i++){
							id.push(selectRecord[i].get("user-id"));
							name.push(selectRecord[i].get("user-name"));
						}
						if(type==1){//第一批实验员
							$("#mbEnrichment_testUserOneId").val(id);
							$("#mbEnrichment_testUserOneName").val(name);
						}else if(type==2){//第二批实验员
							$("#mbEnrichment_testUserTwoId").val(id);
							$("#mbEnrichment_testUserTwoName").val(name);
						}else if(type==3){//审核人
							$("#mbEnrichment_confirmUser").val(id);
							$("#mbEnrichment_confirmUser_name").val(name);
						}
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
		}else{
			message(biolims.common.pleaseSelectGroup);
		}
		
	}
	