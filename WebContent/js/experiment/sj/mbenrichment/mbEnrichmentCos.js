var mbEnrichmentCosGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'temperature',
		type:"string"
	});
	   fields.push({
		name:'speed',
		type:"string"
	});
	   fields.push({
		name:'time',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	    fields.push({
		name:'mbEnrichment-id',
		type:"string"
	});
	    fields.push({
		name:'mbEnrichment-name',
		type:"string"
	});
	   fields.push({
		name:'itemId',
		type:"string"
	});
	   fields.push({
		name:'tCos',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.instrumentNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.common.temperature,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:biolims.common.speed,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.common.time,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isDetecting,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mbEnrichment-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mbEnrichment-name',
		hidden : false,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : false,
		header:biolims.common.itemId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tCos',
		hidden : false,
		header:biolims.common.templateInstrumentId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentCosListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.instrumentDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentCos.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				mbEnrichmentCosGrid.getStore().commitChanges();
				mbEnrichmentCosGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//			text : '选择相关主表',
//				handler : selectmbEnrichmentDialogFun
//		});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = mbEnrichmentCosGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//                	if(n>0){
//                		if(this[0]){
//                			var p = new ob({});
//                			p.isNew = true;				
//                			var o;
//                			o= 0-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 1-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 2-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 3-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 4-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 5-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 6-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 7-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 8-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 9-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 10-1;
//                			p.set("po.fieldName",this[o]);
//							mbEnrichmentCosGrid.getStore().insert(0, p);
//                		}
//                	}
//                     n = n +1;
//                	
//                });
//    	}
//	}
//	opts.tbar.push({
//		text : '显示可编辑列',
//		handler : null
//	});
//	opts.tbar.push({
//		text : '取消选中',
//		handler : null
//	});
	mbEnrichmentCosGrid=gridEditTable("mbEnrichmentCosdiv",cols,loadParam,opts);
	$("#mbEnrichmentCosdiv").data("mbEnrichmentCosGrid", mbEnrichmentCosGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectmbEnrichmentFun(){
	var win = Ext.getCmp('selectmbEnrichment');
	if (win) {win.close();}
	var selectmbEnrichment= new Ext.Window({
	id:'selectmbEnrichment',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectmbEnrichment.close(); }  }]  }) });  
    selectmbEnrichment.show(); }
	function setmbEnrichment(rec){
		var gridGrid = $("#mbEnrichmentCosdiv").data("mbEnrichmentCosGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('mbEnrichment-id',rec.get('id'));
			obj.set('mbEnrichment-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectmbEnrichment')
		if(win){
			win.close();
		}
	}
	function selectmbEnrichmentDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/MbEnrichmentSelect.action?flag=mbEnrichment";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selmbEnrichmentVal(this);
				}
			}, true, option);
		}
	var selmbEnrichmentVal = function(win) {
		var operGrid = mbEnrichmentDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#mbEnrichmentCosdiv").data("mbEnrichmentCosGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('mbEnrichment-id',rec.get('id'));
				obj.set('mbEnrichment-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
