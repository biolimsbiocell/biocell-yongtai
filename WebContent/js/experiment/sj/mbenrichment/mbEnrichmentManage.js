 var experimentDnaManageGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'sjfz',
		type:"string"
	});
	   fields.push({
		name:'yhhzh',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'xylr',
		type:"string"
	});
	   fields.push({
		name:'clr',
		type:"string"
	});
	   fields.push({
		name:'flr',
		type:"string"
	});
	   fields.push({
		name:'xzlr',
		type:"string"
	});
	   fields.push({
		name:'qtlr',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'wknd',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-id',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-name',
		type:"string"
	});
	    fields.push({
			name:'unitGroup-id',
			type:"string"
		});
		fields.push({
			name:'unitGroup-name',
			type:"string"
		});

	    fields.push({
			name:'tempId',
			type:"string"
		});
	    fields.push({
			name:'mbnd',
			type:"string"
		});
	    fields.push({
			name:'lltj',
			type:"string"
		});
	    fields.push({
			name:'bsgz',
			type:"string"
		});
	    fields.push({
			name:'tzhwknd',
			type:"string"
		});
	    fields.push({
			name:'tzhwktj',
			type:"string"
		});
	    fields.push({
			name:'tzhwkzl',
			type:"string"
		});
	    fields.push({
			name:'nextFlowId',
			type:"string"
		});
	    fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	  fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	  fields.push({
			name:'isZkp',
			type:"string"
		});
	  
	  
	  fields.push({
			name:'sampleName',
			type:"string"
		});
	  fields.push({
			name:'insertSize',
			type:"string"
		});
	  fields.push({
			name:'sumFlux',
			type:"string"
		});
	  fields.push({
			name:'ratioOne',
			type:"string"
		});
	  fields.push({
			name:'ratioNeed',
			type:"string"
		});
	  fields.push({
			name:'species',
			type:"string"
		});
	  fields.push({
			name:'laneNum',
			type:"string"
		});
	  fields.push({
			name:'sampleCode',
			type:"string"
		});
	  fields.push({
			name:'dycxsbs',
			type:"string"
		});
	  fields.push({
			name:'dycbsl',
			type:"string"
		});
	  fields.push({
			name:'decxsbs',
			type:"string"
		});
	  fields.push({
			name:'decbsl',
			type:"string"
		});
	  fields.push({
			name:'pmol',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本号',
		width:20*6
	});
	

	cm.push({
		dataIndex:'sampleName',
		hidden : true,
		header:"样本名称",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumFlux',
		hidden : true,
		header:"通量比例和",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ratioOne',
		hidden : true,
		header:"1比例的量",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ratioNeed',
		hidden : true,
		header:"按比例所需量",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'species',
		hidden : true,
		header:"物种",
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : true,
		header:biolims.pooling.poolingWk,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sjfz',
		hidden : true,
		header:biolims.wk.sjfz,
		width:20*6
	});
	cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:biolims.wk.yhhzh,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:biolims.pooling.code,
		width:20*6
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:biolims.pooling.xylr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:biolims.pooling.clr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:biolims.pooling.flr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:biolims.pooling.xzLr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:biolims.pooling.qtlr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:biolims.pooling.flux,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'wknd',
		hidden : true,
		header:biolims.wk.wknd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : true,
		header:biolims.wk.wkpdcd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mbnd',
		hidden : true,
		header:biolims.wk.mbnd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'lltj',
		hidden : true,
		header:biolims.wk.lltj,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bsgz',
		hidden : true,
		header:biolims.wk.bsgz,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzhwknd',
		hidden : true,
		header:biolims.wk.tzhwknd,
		width:20*8,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzhwktj',
		hidden : true,
		header:biolims.wk.tzhwktj,
		width:20*7,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzhwkzl',
		hidden : true,
		header:biolims.wk.tzhwkzl,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'laneNum',
		hidden : false,
		header:"LaneNum",
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'dycxsbs',
		hidden : false,
		header:biolims.common.oneDilutionFactors,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});

	cm.push({
		dataIndex:'dycbsl',
		hidden : false,
		header:biolims.common.oneDilutionFactors,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	
	cm.push({
		dataIndex:'decxsbs',
		hidden : false,
		header:biolims.common.twoDilutionFactors,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	
	cm.push({
		dataIndex:'decbsl',
		hidden : false,
		header:biolims.common.twoDilutionFactors,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	
	cm.push({
		dataIndex:'pmol',
		hidden : false,
		header:biolims.sequencing.upNd,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	
	cm.push({
		dataIndex:'cxlx',
		hidden : false,
		header:biolims.pooling.sequencingType,
		width:20*6
	});
	
	cm.push({
		dataIndex:'cxpt',
		hidden : false,
		header:biolims.pooling.sequencingPlatform,
		width:20*6
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : false,
		header:biolims.pooling.sequencingReadLong,
		width:20*6
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10
//		,
//		editor : nextFlowCob
	});
	
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:"单位组ID",
		width:15*10
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: "单位组",
		width:15*10,
		hidden:true
//		sortable:true,
//		editor : testUnitGroup
	});

	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'wkblendTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkblendTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	
	loadParam.url=ctx+"/experiment/sj/mbenrichment/mbEnrichmentManage/showMbEnrichmentManageListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title=biolims.common.templatePreparationRelust;
	opts.height=document.body.clientHeight-200;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	//取消按钮
	opts.tbar.push({
		text : biolims.common.templateEnrichment,
		handler : addsuccess
			//removement
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	experimentDnaManageGrid=gridEditTable("show_experimentDnaManage_div",cols,loadParam,opts);
	$("#show_experimentDnaManage_div").data("experimentDnaManageGrid", experimentDnaManageGrid);

});
//保存
function addsuccess(){
	
	var selectRcords=experimentDnaManageGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		Ext.MessageBox.confirm(biolims.common.prompt, "确认推送数据？", function(button, text) {
			if (button == "yes") {
				for(var i=0;i<selectRcords.length;i++){
					//判断该样本下一步流向是否执行
					ajax("post", "/experiment/sj/mbenrichment/mbEnrichmentManage/serchExcuteByNext.action", {
						id : selectRcords[i].get("id"), code : selectRcords[i].json.hhzh
					}, function(data) {
						if (data.success && data.flag) {
							//pooling 在下一步存在
							ajax("post", "/experiment/sj/mbenrichment/mbEnrichmentManage/RemoveExperimentDnaManageById.action", {
								id : selectRcords[i].get("id"), code : selectRcords[i].json.hhzh,iscz:'1'
							}, function(data1) {
								if (data1.success) {
									experimentDnaManageGrid.getStore().reload();
									message("推送成功");
								} else{
									message("推送失败");
								}
							}, null);
						
						} else{
							//pooling 在下一步不存在
							ajax("post", "/experiment/sj/mbenrichment/mbEnrichmentManage/RemoveExperimentDnaManageById.action", {
								id : selectRcords[i].get("id"), code : selectRcords[i].json.hhzh,iscz:'0'
							}, function(data1) {
								if (data1.success) {
									experimentDnaManageGrid.getStore().reload();
									message("推送成功");
								} else{
									message("推送失败");
								}
							}, null);
						}
					}, null);
						
				}
			}
		});

	}else{
		message("请选择数据！");
	}
}
//保存
function save(){	
	var itemJson = commonGetModifyRecords(experimentDnaManageGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/dna/experimentDnaManage/saveDNAManage.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {			
					experimentDnaManageGrid.getStore().commitChanges();
					experimentDnaManageGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
}

function selectDnaInfo(){
	commonSearchAction(experimentDnaManageGrid);
	$("#experimentDnaManage_code").val("");
//	$("#experimentDnaManage_sampleCode").val("");
}

//下一步流向
function loadTestNextFlowCob(){
	var records1 = experimentDnaManageGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=DnaTask&productId="+productId, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = experimentDnaManageGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

//取消样本
function removement(){
	var selectRcords=experimentDnaManageGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		for(var i=0;i<selectRcords.length;i++){
			//判断该样本下一步流向是否执行
			ajax("post", "/experiment/dna/experimentDnaMoidfy/serchExcuteByNext.action", {
				id : selectRcords[i].get("id"), code : selectRcords[i].json.code
			}, function(data) {
				if (data.success && data.flag) {
					//取消样本
					ajax("post", "/experiment/dna/experimentDnaMoidfy/RemoveExperimentDnaManageById.action", {
						id : selectRcords[i].get("id")
					}, function(data1) {
						if (data1.success) {
							experimentDnaManageGrid.getStore().reload();
							message("取消成功");
						} else{
							message("取消失败");
						}
					}, null);
				
				} else{
					message("该样本在下一步流向中已被执行无法取消");
				}
			}, null);
				
		}
	}else{
		message(biolims.common.pleaseSelectBacklog);
	}
}