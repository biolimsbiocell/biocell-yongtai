var mbEnrichmentResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'wknd',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'mbEnrichment-id',
		type:"string"
	});
	    fields.push({
		name:'mbEnrichment-name',
		type:"string"
	});
	   fields.push({
		name:'tzhwknd',
		type:"string"
	});
	   fields.push({
		name:'tzhwktj',
		type:"string"
	});
	   fields.push({
		name:'dycxsbs',
		type:"string"
	});
	   fields.push({
		name:'dycbsl',
		type:"string"
	});
	   fields.push({
		name:'decxsbs',
		type:"string"
	});
	   fields.push({
		name:'decbsl',
		type:"string"
	});
	   fields.push({
		name:'pmol',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	   fields.push({
		name:'otNum',
		type:"string"
	});
	   fields.push({
		name:'otState',
		type:"string"
	});
	   fields.push({
		name:'esNum',
		type:"string"
	});
	   fields.push({
		name:'esState',
		type:"string"
	});
	   fields.push({
		name:'chefNum',
		type:"string"
	});
	   fields.push({
		name:'chefState',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : true,
		header:'富集文库',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:biolims.pooling.code,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'tl',
		hidden :true,
		header:biolims.pooling.flux,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wknd',
		hidden : true,
		header:'文库浓度（ng/ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : true,
		header:'各组文库片段长度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cxlx',
		hidden : true,
		header:'测序类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cxpt',
		hidden : true,
		header:'测序平台',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : true,
		header:'测序读长',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'mbEnrichment-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mbEnrichment-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'tzhwknd',
		hidden : true,
		header:'浓度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzhwktj',
		hidden : true,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dycxsbs',
		hidden : true,
		header:'第一次稀释倍数',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dycbsl',
		hidden : true,
		header:'第一次补水量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'decxsbs',
		hidden : true,
		header:'第二次稀释倍数',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'decbsl',
		hidden : true,
		header:'第二次补水量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'pmol',
		hidden : true,
		header:'上机浓度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'otNum',
		hidden : false,
		header:biolims.common.instrumentNoot2,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'otState',
		hidden : false,
		header:biolims.common.instrumentStateot2,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'esNum',
		hidden : false,
		header:biolims.common.instrumentNoes,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'esState',
		hidden : false,
		header:biolims.common.instrumentStatees,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'chefNum',
		hidden : false,
		header:biolims.common.instrumentNochef,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'chefState',
		hidden : false,
		header:biolims.common.instrumentStatechef,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.qualified], [ '1', biolims.common.disqualified ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.plasma.resultDecide,
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	
	
	var storeGoodCob1 = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', "上机测序life"], [ '1', "终止" ] ]
	});
	var goodCob1 = new Ext.form.ComboBox({
		store : storeGoodCob1,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:biolims.common.nextFlow,
		width:20*6,
		editor : goodCob1,
		renderer : Ext.util.Format.comboRenderer(goodCob1)
	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.template1Result;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state = $("#sequencing_stateName").val();
	if(state!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				mbEnrichmentResultGrid.getStore().commitChanges();
				mbEnrichmentResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	}
//	opts.tbar.push({
//			text : '选择相关主表',
//				handler : selectmbEnrichmentDialogFun
//		});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = mbEnrichmentResultGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//                	if(n>0){
//                		if(this[0]){
//                			var p = new ob({});
//                			p.isNew = true;				
//                			var o;
//                			o= 0-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 1-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 2-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 3-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 4-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 5-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 6-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 7-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 8-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 9-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 10-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 11-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 12-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 13-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 14-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 15-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 16-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 17-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 18-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 19-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 20-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 21-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 22-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 23-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 24-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 25-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 26-1;
//                			p.set("po.fieldName",this[o]);
//							mbEnrichmentResultGrid.getStore().insert(0, p);
//                		}
//                	}
//                     n = n +1;
//                	
//                });
//    	}
//	}
//	opts.tbar.push({
//		text : '显示可编辑列',
//		handler : null
//	});
//	opts.tbar.push({
//		text : '取消选中',
//		handler : null
//	});
	mbEnrichmentResultGrid=gridEditTable("mbEnrichmentResultdiv",cols,loadParam,opts);
	$("#mbEnrichmentResultdiv").data("mbEnrichmentResultGrid", mbEnrichmentResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectmbEnrichmentFun(){
	var win = Ext.getCmp('selectmbEnrichment');
	if (win) {win.close();}
	var selectmbEnrichment= new Ext.Window({
	id:'selectmbEnrichment',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectmbEnrichment.close(); }  }]  }) });  
    selectmbEnrichment.show(); }
	function setmbEnrichment(rec){
		var gridGrid = $("#mbEnrichmentResultdiv").data("mbEnrichmentResultGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('mbEnrichment-id',rec.get('id'));
			obj.set('mbEnrichment-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectmbEnrichment')
		if(win){
			win.close();
		}
	}
	function selectmbEnrichmentDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/MbEnrichmentSelect.action?flag=mbEnrichment";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selmbEnrichmentVal(this);
				}
			}, true, option);
		}
	var selmbEnrichmentVal = function(win) {
		var operGrid = mbEnrichmentDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#mbEnrichmentResultdiv").data("mbEnrichmentResultGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('mbEnrichment-id',rec.get('id'));
				obj.set('mbEnrichment-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
