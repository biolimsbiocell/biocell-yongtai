var mbEnrichmentTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'wknd',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'tzhwknd',
		type:"string"
	});
	   fields.push({
		name:'tzhwktj',
		type:"string"
	});
	   fields.push({
		name:'dycxsbs',
		type:"string"
	});
	   fields.push({
		name:'dycbsl',
		type:"string"
	});
	   fields.push({
		name:'decxsbs',
		type:"string"
	});
	   fields.push({
		name:'decbsl',
		type:"string"
	});
	   fields.push({
		name:'pmol',
		type:"string"
	});
	   fields.push({
			name:'state',
			type:"string"
		});
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : true,
		header:'富集文库',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:biolims.wk.hhzh,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:biolims.pooling.flux,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'wknd',
		hidden : false,
		header:biolims.wk.wkConcentration,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : false,
		header:biolims.common.wkLong,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'cxlx',
		hidden : false,
		header:biolims.pooling.sequencingType,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'cxpt',
		hidden : false,
		header:biolims.pooling.sequencingPlatform,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : false,
		header:biolims.pooling.sequencingReadLong,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:biolims.common.nextFlow,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'tzhwknd',
		hidden : false,
		header:biolims.user.concentration,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'tzhwktj',
		hidden : false,
		header:biolims.common.bulk,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'dycxsbs',
		hidden : false,
		header:biolims.common.oneDilutionFactors,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'dycbsl',
		hidden : false,
		header:biolims.common.oneHydrationAmount,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'decxsbs',
		hidden : false,
		header:biolims.common.twoDilutionFactors,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'decbsl',
		hidden : false,
		header:biolims.common.twoHydrationAmount,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'pmol',
		hidden : false,
		header:biolims.sequencing.upNd,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentTempListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.templateFJTemp;
	opts.height =  document.body.clientHeight-33;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentTemp.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				mbEnrichmentTempGrid.getStore().commitChanges();
				mbEnrichmentTempGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text :biolims.common.addToTask,
		handler : addItem
	});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = mbEnrichmentTempGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//                	if(n>0){
//                		if(this[0]){
//                			var p = new ob({});
//                			p.isNew = true;				
//                			var o;
//                			o= 0-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 1-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 2-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 3-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 4-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 5-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 6-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 7-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 8-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 9-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 10-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 11-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 12-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 13-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 14-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 15-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 16-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 17-1;
//                			p.set("po.fieldName",this[o]);
//							mbEnrichmentTempGrid.getStore().insert(0, p);
//                		}
//                	}
//                     n = n +1;
//                	
//                });
//    	}
//	}
//	opts.tbar.push({
//		text : '显示可编辑列',
//		handler : null
//	});
//	opts.tbar.push({
//		text : '取消选中',
//		handler : null
//	});
	mbEnrichmentTempGrid=gridEditTable("mbEnrichmentTempdiv",cols,loadParam,opts);
	$("#mbEnrichmentTempdiv").data("mbEnrichmentTempGrid", mbEnrichmentTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//添加任务到子表
function addItem(){
	var selectRecord=mbEnrichmentTempGrid.getSelectionModel();
	var selRecord=mbEnrichmentItemGrid.store;
	var count=1;
	var max=0;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					return;					
				}
			}
			if(!isRepeat){
			var ob = mbEnrichmentItemGrid.getStore().recordType;
			mbEnrichmentItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("tempId",obj.get("id"));
			p.set("fjwk",obj.get("fjwk"));
			p.set("hhzh",obj.get("hhzh"));
			p.set("tl",obj.get("tl"));
			p.set("wknd",obj.get("wknd"));
			p.set("wkpdcd",obj.get("wkpdcd"));
			p.set("cxlx",obj.get("cxlx"));
			p.set("cxpt",obj.get("cxpt"));
			p.set("cxdc",obj.get("cxdc"));
			p.set("note",obj.get("note"));
			p.set("tzhwknd",obj.get("tzhwknd"));
			p.set("tzhwktj",obj.get("tzhwktj"));
			p.set("dycxsbs",obj.get("dycxsbs"));
			p.set("dycbsl",obj.get("dycbsl"));
			p.set("decxsbs",obj.get("decxsbs"));
			p.set("decbsl",obj.get("decbsl"));
			p.set("pmol",obj.get("pmol"));
			
			p.set("orderNumber",Number(max)+count);
			
			p.set("sampleCode",obj.get("sampleCode"));
			mbEnrichmentItemGrid.getStore().add(p);
			count++;
			mbEnrichmentItemGrid.startEditing(0, 0);
			
		}
			
	});
	}else{
		message(biolims.common.pleaseAddSample);
	}
	
}
