//DNA提取明细
var dnaGetItemManagerGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'unitGroup-id',
		type:"string"
	});
	fields.push({
		name:'unitGroup-name',
		type:"string"
	});

	   fields.push({
		name:'sjfz',
		type:"string"
	});
	   fields.push({
		name:'yhhzh',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'xylr',
		type:"string"
	});
	   fields.push({
		name:'clr',
		type:"string"
	});
	   fields.push({
		name:'flr',
		type:"string"
	});
	   fields.push({
		name:'xzlr',
		type:"string"
	});
	   fields.push({
		name:'qtlr',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'tlbl',
		type:"string"
	});
	   fields.push({
		name:'kzxl',
		type:"string"
	});
	   fields.push({
		name:'ect',
		type:"string"
	});
	   fields.push({
		name:'qpcrbl',
		type:"string"
	});
	   fields.push({
		name:'blcd',
		type:"string"
	});
	   fields.push({
		name:'wkhhl',
		type:"string"
	});
	   fields.push({
		name:'wkhhtl',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-id',
		type:"string"
	});
	    fields.push({
		name:'wkblendTask-name',
		type:"string"
	});
	   fields.push({
		name:'hhhnd',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	   fields.push({
			name:'stepNum',
			type:"string"
		});
	   fields.push({
			name:'orderNumber',
			type:"string"
		});   
	   fields.push({
			name:'state',
			type:"string"
		});  
	   fields.push({
			name:'concentration',
			type:"string"
		});
		   fields.push({
			name:'volume',
			type:"string"
		});
		   fields.push({
			name:'sumTotal',
			type:"string"
		});
	   fields.push({
			name:'hhbl',
			type:"string"
		});
	   fields.push({
			name:'techJkServiceTask-id',
			type:"string"
		});
	  fields.push({
			name:'techJkServiceTask-name',
			type:"string"
		});
	  fields.push({
			name:'isZkp',
			type:"string"
		});
	  
	  fields.push({
			name:'sampleName',
			type:"string"
		});
	  fields.push({
			name:'insertSize',
			type:"string"
		});
	  fields.push({
			name:'sumFlux',
			type:"string"
		});
	  fields.push({
			name:'ratioOne',
			type:"string"
		});
	  fields.push({
			name:'ratioNeed',
			type:"string"
		});
	  fields.push({
			name:'species',
			type:"string"
		});
	  fields.push({
			name:'wkmConcentration',
			type:"string"
		});
	  fields.push({
			name:'wkMoleConcentration',
			type:"string"
		});
	  fields.push({
			name:'sampleVolume',
			type:"string"
		});
	  fields.push({
			name:'xszConcentration',
			type:"string"
		});
	  fields.push({
			name:'addRsbVolume',
			type:"string"
		});
	  fields.push({
			name:'xshSumVolume',
			type:"string"
		});
	  fields.push({
			name:'indexa',
			type:"string"
		});
	  fields.push({
			name:'tjItem-id',
			type:"string"
		});
	  fields.push({
			name:'tjItem-note',
			type:"string"
		});
	  fields.push({
			name:'sampleCode',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:20*6,
	 
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	
	cm.push({
		dataIndex:'fjwk',
		hidden : false,
		header:biolims.wk.wkCode,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sjfz',
		hidden : false,
		header:biolims.pooling.wKNum
//			,
//
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:"INDEX",
		width:20*6
	});
	cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:biolims.wk.yhhzh,
		width:20*6
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : true,
		header:biolims.wk.hhzh,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:biolims.pooling.xylr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:biolims.pooling.clr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:biolims.pooling.flr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:biolims.pooling.xzLr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:biolims.pooling.qtlr,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleName',
		hidden : true,
		header:"样本名称",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:biolims.pooling.flux,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'tlbl',
		hidden : true,
		header:biolims.pooling.fluxRate,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumFlux',
		hidden : true,
		header:"通量比例和",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ratioOne',
		hidden : true,
		header:"1比例的量",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ratioNeed',
		hidden : true,
		header:"按比例所需量",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'kzxl',
		hidden : true,
		header:biolims.pooling.kzxl,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ect',
		hidden : true,
		header:'E^-ΔCT',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qpcrbl',
		hidden : true,
		header:biolims.pooling.qpcrbl,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'blcd',
		hidden : true,
		header:biolims.pooling.blcd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:biolims.common.concentration,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:biolims.common.volume,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'unitGroup-id',
		hidden:true,
		header:"单位组ID",
		width:15*10,
//		sortable:true
	});
	var testUnitGroup =new Ext.form.TextField({
        allowBlank: false
	});
	testUnitGroup.on('focus', function() {
		loadUnitGroup();
	});
	cm.push({
		dataIndex:'unitGroup-name',
		header: "单位组",
		width:15*10,
		hidden : true,
//		sortable:true,
		editor : testUnitGroup
	});

	cm.push({
		dataIndex:'sumTotal',
		hidden : true,
		header:biolims.common.sumNum,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'species',
		hidden : true,
		header:"物种",
		width:20*6
	});
	
	cm.push({
		dataIndex:'hhbl',
		hidden : true,
		header:biolims.wk.mixRatio,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkhhl',
		hidden : true,
		header:biolims.wk.wkhhl,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkhhtl',
		hidden : true,
		header:"文库总体积（ul）",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hhhnd',
		hidden : true,
		header:biolims.wk.hhhnd,
		width:20*8,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkmConcentration',
		hidden : true,
		header:"文库质量浓度（ng/ul）",
		width:20*7,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'insertSize',
		hidden : true,
		header:"文库片段大小（bp）",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : true,
		header:"文库长度（bp）",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkMoleConcentration',
		hidden : true,
		header:"文库摩尔浓度（nM）",
		width:20*7,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:"取样体积（ul）",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xszConcentration',
		hidden : true,
		header:"稀释终浓度（nM）",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addRsbVolume',
		hidden : true,
		header:"补加RSB体积（ul）",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xshSumVolume',
		hidden : true,
		header:"稀释后总体积（ul）",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'hhbl',
		hidden : true,
		header:"文库混合比例",
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cxlx',
		hidden : false,
		header:biolims.pooling.sequencingType,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'cxpt',
		hidden : false,
		header:biolims.pooling.sequencingPlatform,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : false,
		header:biolims.pooling.sequencingReadLong,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:biolims.common.expCode,
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.expCode,
		width:20*6
	});
	cm.push({
		dataIndex:'wkblendTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkblendTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.isPutInStorage,
		width:20*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-id',
		hidden : true,
		header:biolims.common.taskId_id,
		width:30*6
	});
	cm.push({
		dataIndex:'techJkServiceTask-name',
		hidden : true,
		header:biolims.common.taskId,
		width:30*6
	});
	cm.push({
		dataIndex:'isZkp',
		hidden : true,
		header:biolims.wk.isZkp,
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-id',
		hidden : true,
		header:"任务单样本id",
		width:20*6
	});
	cm.push({
		dataIndex:'tjItem-note',
		hidden : true,
		header:"科研备注",
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/mbenrichment/mbEnrichmentManage/showDnaGetItemManagerListJson.action";
	var opts={};
	opts.title=biolims.common.templatePreparationDetail;
	opts.height =  document.body.clientHeight-240;
	opts.tbar = [];
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
//	opts.tbar.push({
//		text : biolims.common.batchInLib,
//		handler : ruku
//	});
//	opts.tbar.push({
//		text : biolims.common.extractBacklog,
//		handler : tiqu
//	});
	dnaGetItemManagerGrid=gridEditTable("dnaGetItemManagerdiv",cols,loadParam,opts);
	$("#dnaGetItemManagerdiv").data("dnaGetItemManagerGrid", dnaGetItemManagerGrid);
});

//保存Grid
function save1(){	
	var itemJson = commonGetModifyRecords(dnaGetItemManagerGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/dna/experimentDnaManage/saveDnaGetItemManager.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					dnaGetItemManagerGrid.getStore().commitChanges();
					dnaGetItemManagerGrid.getStore().reload();
					message(biolims.common.saveSuccess);
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);			
	}else{
		message(biolims.common.noData2Save);
	}
}

function selectInfo(){
	commonSearchAction(dnaGetItemManagerGrid);
	$("#dnaGetItemManager_code").val("");
	$("#dnaGetItemManager_sampleCode").val("");
}

function ruku(){
	var selectRcords=dnaGetItemManagerGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		var ids="";
		$.each(selectRcords,function(i,obj){
			ids+=obj.get("id")+",";
		});
		ajax("post", "/experiment/dna/experimentDnaManage/dnaGetManageItemRuku.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				dnaGetItemManagerGrid.getStore().reload();
				message(biolims.plasma.warehousingSuccess);
			} else{
				message(biolims.plasma.warehousingFailed);
			}
		}, null);
	}else{
		message(biolims.plasma.pleaseSelectWarehousing);
	}
}

function tiqu(){
	var selectRcords=dnaGetItemManagerGrid.getSelectionModel().getSelections();
	if(selectRcords.length>0){
		for(var i=0;i<selectRcords.length;i++){
			ajax("post", "/experiment/dna/experimentDnaManage/dnaGetManageItemTiqu.action", {
				id : selectRcords[i].get("id")
			}, function(data) {
				if (data.success) {
					dnaGetItemManagerGrid.getStore().reload();
					message(biolims.common.backlogSuccess);
				} else{
					message(biolims.common.backlogFailed);
				}
			}, null);
		}
	}else{
		message(biolims.common.pleaseSelectBacklog);
	}
}
