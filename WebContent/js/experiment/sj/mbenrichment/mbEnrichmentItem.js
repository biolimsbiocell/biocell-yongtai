var mbEnrichmentItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'fjwk',
		type:"string"
	});
	   fields.push({
		name:'hhzh',
		type:"string"
	});
	   fields.push({
		name:'tl',
		type:"string"
	});
	   fields.push({
		name:'wknd',
		type:"string"
	});
	   fields.push({
		name:'wkpdcd',
		type:"string"
	});
	   fields.push({
		name:'cxlx',
		type:"string"
	});
	   fields.push({
		name:'cxpt',
		type:"string"
	});
	   fields.push({
		name:'cxdc',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'mbEnrichment-id',
		type:"string"
	});
	    fields.push({
		name:'mbEnrichment-name',
		type:"string"
	});
	   fields.push({
		name:'tzhwknd',
		type:"string"
	});
	   fields.push({
		name:'tzhwktj',
		type:"string"
	});
	   fields.push({
		name:'dycxsbs',
		type:"string"
	});
	   fields.push({
		name:'dycbsl',
		type:"string"
	});
	   fields.push({
		name:'decxsbs',
		type:"string"
	});
	   fields.push({
		name:'decbsl',
		type:"string"
	});
	   fields.push({
		name:'pmol',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	   fields.push({
			name:'orderNumber',
			type:"string"
		});
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:'实验室排序号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fjwk',
		hidden : true,
		header:'富集文库',
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:biolims.wk.hhzh,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:biolims.pooling.flux,
		width:20*6
//		,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'wknd',
		hidden : true,
		header:biolims.wk.wkConcentration,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : true,
		header:biolims.common.wkLong,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cxlx',
		hidden : true,
		header:biolims.pooling.sequencingType,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cxpt',
		hidden : true,
		header:biolims.pooling.sequencingPlatform,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'cxdc',
		hidden : true,
		header:biolims.pooling.sequencingReadLong,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : true,
		header:biolims.common.nextFlow,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'mbEnrichment-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mbEnrichment-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'tzhwknd',
		hidden : true,
		header:'浓度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tzhwktj',
		hidden : true,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dycxsbs',
		hidden : false,
		header:biolims.common.oneDilutionFactors,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dycbsl',
		hidden : false,
		header:biolims.common.oneHydrationAmount,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'decxsbs',
		hidden : false,
		header:biolims.common.twoDilutionFactors,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'decbsl',
		hidden : false,
		header:biolims.common.twoHydrationAmount,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'pmol',
		hidden : false,
		header:biolims.sequencing.upNd,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/sj/mbenrichment/mbEnrichment/showMbEnrichmentItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.template1Item;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state = $("#mbEnrichment_state").val();
	if(state&&state!='1'){
		 opts.delSelect = function(ids) {
				ajax("post", "/experiment/sj/mbenrichment/mbEnrichment/delMbEnrichmentItem.action", {
					ids : ids
				}, function(data) {
					if (data.success) {
						mbEnrichmentItemGrid.getStore().commitChanges();
						mbEnrichmentItemGrid.getStore().reload();
						mbEnrichmentTempGrid.getStore().reload();
						message("删除成功！");
					} else {
						message("删除失败！");
					}
				}, null);
			};
//			opts.tbar.push({
//					text : '选择相关主表',
//						handler :null//selectmbEnrichmentDialogFun
//				});
//			opts.tbar.push({
//				text : "批量上传（csv文件）",
//				handler : null
////					function() {
////					var options = {};
////					options.width = 350;
////					options.height = 200;
////					loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
////						"确定":function(){
////							goInExcelcsv();
////							$(this).dialog("close");
////						}
////					},true,options);
////				}
//			});
			opts.tbar.push({
				text : biolims.common.editableColAppear,
				handler : null
			});
			opts.tbar.push({
				text :biolims.common.fillDetail,
				handler : null
			});
			opts.tbar.push({
				text :biolims.common.uncheck,
				handler : null
			});
//			function goInExcelcsv(){
//				var file = document.getElementById("file-uploadcsv").files[0];  
//				var n = 0;
//				var ob = mbEnrichmentItemGrid.getStore().recordType;
//				var reader = new FileReader();  
//				reader.readAsText(file,'GB2312');  
//				reader.onload=function(f){  
//					var csv_data = $.simple_csv(this.result);
//					$(csv_data).each(function() {
//		                	if(n>0){
//		                		if(this[0]){
//		                			var p = new ob({});
//		                			p.isNew = true;				
//		                			var o;
//		                			o= 0-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 1-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 2-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 3-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 4-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 5-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 6-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 7-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 8-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 9-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 10-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 11-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 12-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 13-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 14-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 15-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 16-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 17-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 18-1;
//		                			p.set("po.fieldName",this[o]);
//		                			o= 19-1;
//		                			p.set("po.fieldName",this[o]);
//									mbEnrichmentItemGrid.getStore().insert(0, p);
//		                		}
//		                	}
//		                     n = n +1;
//		                	
//		                });
//		    	}
//			}
//			opts.tbar.push({
//				text : '显示可编辑列',
//				handler : null
//			});
//			opts.tbar.push({
//				text : '取消选中',
//				handler : null
//			});
	}
      
	mbEnrichmentItemGrid=gridEditTable("mbEnrichmentItemdiv",cols,loadParam,opts);
	$("#mbEnrichmentItemdiv").data("mbEnrichmentItemGrid", mbEnrichmentItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectmbEnrichmentFun(){
	var win = Ext.getCmp('selectmbEnrichment');
	if (win) {win.close();}
	var selectmbEnrichment= new Ext.Window({
	id:'selectmbEnrichment',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectmbEnrichment.close(); }  }]  }) });  
    selectmbEnrichment.show(); }
	function setmbEnrichment(rec){
		var gridGrid = $("#mbEnrichmentItemdiv").data("mbEnrichmentItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('mbEnrichment-id',rec.get('id'));
			obj.set('mbEnrichment-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectmbEnrichment')
		if(win){
			win.close();
		}
	}
	function selectmbEnrichmentDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/MbEnrichmentSelect.action?flag=mbEnrichment";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selmbEnrichmentVal(this);
				}
			}, true, option);
		}
	var selmbEnrichmentVal = function(win) {
		var operGrid = mbEnrichmentDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#mbEnrichmentItemdiv").data("mbEnrichmentItemGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('mbEnrichment-id',rec.get('id'));
				obj.set('mbEnrichment-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
