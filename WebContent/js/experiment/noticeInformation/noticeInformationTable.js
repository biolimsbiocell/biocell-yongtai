var noticeInformationTable;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": "公告编号",
		}, {
			"data": "name",
			"title": "公告简述",
		}, {
			"data": "createUser-name",
			"title": "创建人",
		}, {
			"data": "createDate",
			"title": "创建日期",
		}, {
			"data": "stateName",
			"title": "状态",
		},{
			"data": "startDate",
			"title": "公告开始时间",
		},{
			"data": "endDate",
			"title": "公告结束时间",
		}];
		
	var options = table(true, "",
		"/experiment/noticeInformation/noticeInformation/showNoticeInformationTableJson.action",colData , null)
	noticeInformationTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(noticeInformationTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/experiment/noticeInformation/noticeInformation/editNoticeInformation.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/experiment/noticeInformation/noticeInformation/editNoticeInformation.action?id=' + id ;
}
// 查看
function view() {
//	var id = $(".selected").find("input").val();
//	if(id == "" || id == undefined) {
//		top.layer.msg(biolims.common.selectRecord);
//		return false;
//	}
//	window.location = window.ctx +
//		'/experiment/roomManagement/roomManagement/viewRoomManagement.action?id=' + id + '&type=' + $("#order_type").val();
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "公告编号",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "公告简述",
			"type": "input",
			"searchName": "name"
		},
		{
			"type": "table",
			"table": noticeInformationTable
		}
	];
}