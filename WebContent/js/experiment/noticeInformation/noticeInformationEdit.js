$(function() {
	$("#btn_changeState").show();
	$("#noticeInformation_startDate").datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		language: 'zh-CN',
	}); 
	$("#noticeInformation_endDate").datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		language: 'zh-CN',
	}); 
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#noticeInformation_state").text()!="新建") {
		settextreadonly();
	}
	if(handlemethod == "modify") {
		if($("#noticeInformation_id").val()==null||$("#noticeInformation_id").val()==""){
			$("#noticeInformation_id").prop("readonly", false);
		}else{
			$("#noticeInformation_id").prop("readonly", "readonly");
		}
	}else{
		if($("#noticeInformation_id").val()==null||$("#noticeInformation_id").val()==""){
			$("#noticeInformation_id").prop("readonly", false);
		}else{
			$("#noticeInformation_id").prop("readonly", "readonly");
		}
	}
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'noticeInformation', $("#noticeInformation_id").val());
	$("#btn_list").after("<button type=\"button\" class=\"btn btn-info\" id=\"dele\" onclick=\"delect()\">"+
			"<i class=\"glyphicon glyphicon-print\"></i> 删除"+
		"</button>");
	if($("#noticeInformation_state").text()!="新建"){
		$("#btn_save").hide();
		if($("#noticeInformation_state").text()=="作废"){
			$("#btn_changeState").hide();
			//查询当前用户是否人员组
			if(window.groupIds.indexOf("admin") != -1){
				$("#dele").show();
			}else{
				$("#dele").hide();
			}
		}else{
			$("#dele").hide();
		}
	}else{
		$("#dele").hide();
	}
});

function delect() {
	$.ajax({
		type: "post",
		url: window.ctx + '/experiment/noticeInformation/noticeInformation/delect.action',
		async: false,
		data: {
			id: $("#noticeInformation_id").val(),
		},
		success: function(data) {
			var obj = JSON.parse(data);
			if(obj.success) {
				window.location = window.ctx +
				'/experiment/noticeInformation/noticeInformation/showNoticeInformationTable.action';
			} else {
				top.layer.msg("删除失败！");
			}
		}
	});
}


/**
 * onchange（参数）
 * 
 * @param id
 */
function change(id) {
	$("#" + id).css({
		"background-color": "white",
		"color": "black"
	});
}

function add() {
	window.location = window.ctx +
		"/experiment/noticeInformation/noticeInformation/editNoticeInformation.action";
}

function list() {
	window.location = window.ctx +
		'/experiment/noticeInformation/noticeInformation/showNoticeInformationTable.action';
}

function validId() {
	$.ajax({
		type: "post",
		url: window.ctx + '/common/hasId.action',
		async: false,
		data: {
			id: $("#noticeInformation_id").val(),
			obj: 'NoticeInformation'
		},
		success: function(data) {
			var obj = JSON.parse(data);
			if(obj.success) {
				if(obj.bool) {
					bool2 = true;
				} else {
					top.layer.msg(obj.msg);
				}
			} else {
				top.layer.msg(biolims.common.checkingFieldCodingFailure);
			}
		}
	});
}

function tjsp() {
	if($("#sampleOrder_confirmUser_name").val()==null||$("#sampleOrder_confirmUser_name").val()==""){
		top.layer.msg("请添加审核人,保存之后在执行此操作!");
		return false;
	}
		//订单为用户类型才可以提交
		top.top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {
			icon: 3,
			title: biolims.common.prompt,
			btn: biolims.common.selected
		}, function(index) {
			top.layer.open({
				title: biolims.common.approvalProcess,
				type: 2,
				anim: 2,
				area: ['800px', '500px'],
				btn: biolims.common.selected,
				content: window.ctx + "/workflow/processinstance/toStartView.action?formName=SampleOrder",
				yes: function(index, layer) {
					var datas = {
							userId: userId,
							userName: userName,
							formId: $("#sampleOrder_id").val(),
							title: $("#sampleOrder_name").val(),
							formName:'SampleOrder'
					}
					ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
						if(data.success) {
							top.layer.msg(biolims.common.submitSuccess);
							if(typeof callback == 'function') {
								callback(data);
							}
							dialogWin.dialog("close");
						} else {
							top.layer.msg(biolims.common.submitFail);
						}
					}, null);
					top.layer.close(index);
				},
				cancel: function(index, layer) {
					top.layer.close(index)
				}
				
			});
			top.layer.close(index);
		});

}

function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#sampleOrder_id").val();

	top.layer.open({
		title: biolims.common.approvalProcess,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx + "/workflow/processinstance/toCompleteTaskView.action?taskId=" + taskId + "&formId=" + formId,
		yes: function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();

			if(!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if(operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data: JSON.stringify(paramData),
					formId: formId,
					taskId: taskId,
					userId: window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {}
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}

	});
}

function save() {
	if($("#noticeInformation_id").val()==null
			||$("#noticeInformation_id").val()==""){
		top.layer.msg("公告编号不能为空！");
	}else{
		//自定义字段
		//拼自定义字段儿（实验记录）
		var requiredField = requiredFilter();
		if(!requiredField) {
			return false;
		}
		var changeLog = "公告管理-";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});

		document.getElementById("changeLog").value = changeLog;

		var handlemethod = $("#handlemethod").val();
		if(handlemethod == "modify" && checkSubmit() == true) {
			top.layer.load(4, {
				shade: 0.3
			});
			$("#form1").attr("action", "/experiment/noticeInformation/noticeInformation/save.action");
			$("#form1").submit();
			top.layer.closeAll();
		} else {
			$.ajax({
				type: "post",
				url: ctx + '/common/hasId.action',
				data: {
					id: $("#noticeInformation_id").val(),
					obj: 'NoticeInformation'
				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.message) {
						top.layer.msg(data.message);
					} else {
						top.layer.load(4, {
							shade: 0.3
						});
						$("#form1").attr("action", "/experiment/noticeInformation/noticeInformation/save.action");
						$("#form1").submit();
						top.layer.closeAll();
					}
				}
			});
		}
	}
	
	
}

var changeId = $("#changeId").val();



function changeState() {
	var paraStr = "formId=" + $("#noticeInformation_id").val() +
		"&tableId=noticeInformation";
	top.top.layer.confirm("状态完成之前请先保存", {
		icon: 3,
		title: biolims.common.prompt,
		btn: biolims.common.selected
	}, function(index) {
		top.layer.open({
			title: biolims.common.approvalProcess,
			type: 2,
			anim: 2,
			area: ['400px', '400px'],
			btn: biolims.common.selected,
			content: window.ctx +
				"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
				"&flag=changeState'",
			yes: function(index, layer) {
				top.layer.confirm(biolims.common.approve, {
					icon: 3,
					title: biolims.common.prompt,
					btn: biolims.common.selected
				}, function(index) {
					ajax("post", "/applicationTypeAction/exeFun.action", {
						applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
						formId: $("#noticeInformation_id").val()
					}, function(response) {
						var respText = response.message;
						if(respText == '') {
							window.location.reload();
						} else {
							top.layer.msg(respText);
						}
					}, null)
					top.layer.closeAll();
				})
	
			},
			cancel: function(index, layer) {
				top.layer.closeAll();
	
			}
	
		});
		top.layer.close(index);
	});
}


function checkSubmit() {
	if($("#noticeInformation_id").val() == null || $("#noticeInformation_id").val() == "") {
		top.layer.msg(biolims.common.codeNotEmpty);
		return false;
	};
	return true;
}


function fileUp() {
	if($("#noticeInformation_id").val() == "NEW") {
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}

function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=noticeInformation&id=" + $("#noticeInformation_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

//Ai图片识别
function subimtBtn() {
	var form = $("#fileForm");
	var options = {
		url: ctx + '/common/aiUtils/AiTemplateReader.action',
		type: 'post',
		success: function(data) {
			console.log(data);
			var res = JSON.parse(data);
			var wordArray = res.data.ret;
			var str = "";
			for(var i = 0; i < wordArray.length; i++) {
				$("#" + wordArray[i].word_name).val(wordArray[i].word);
			}
		}
	};
	form.ajaxSubmit(options);
}
//Ai图片识别
function subimtBtn2(that) {
	if(that.files[0]) {
		top.layer.open({
			title: "图片裁剪",
			type: 2,
			skin: 'layui-layer-lan',
			area: ["98%", "600px"],
			shadeClose: true,
			content: window.ctx + "/system/sample/sampleOrderChange/casualMethod.action",
			success: function() {
				var files = that.files;
				var reader = new FileReader;
				reader.readAsDataURL(files[0]);
				reader.onload = function() {
					$('.layui-layer-iframe', parent.document).find("iframe").contents().find("img").attr("src", this.result);
				};
			},
			cancel: function(index, layer) {
				top.layer.close(index);
			}
		})
	}

}

//显示图片
function upLoadImg1() {
	top.layer.open({
		type: 2,
		title: "查看图片",
		area: ["35%", "80%"],
		shade: 0,
		content: window.ctx + "/system/sample/sampleOrder/showImg.action?id=" + $("#sampleOrder_id").val(),
	})
}
