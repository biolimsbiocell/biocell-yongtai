﻿var immuneCellProductionMakeUpAfTab, oldChangeLog;
var isSeparate = $("#immuneCellProduction_template_isSeparate").val();
var flg = false;
if(isSeparate == 1) {
	flg = true;
}

//已排板样本
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data": "sampleOrder-id",
		"title":"关联订单号",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "dicSampleTypeId",
		"title": biolims.common.dicSampleTypeId,
		"width": "120px",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "dicSampleTypeId");
		},
	})
	colOpts.push({
		"data": "dicSampleTypeName",
		"title": biolims.common.dicSampleTypeName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleTypeName");
			$(td).attr("dicSampleTypeId", rowData['dicSampleTypeId']);
		}
	})
	colOpts.push({
		"data": "tempId",
		"title": biolims.common.tempId,
		"visible": false,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "tempId");
		},
	})
	colOpts.push({
		"data": "productNum",
		"title": biolims.common.productNum,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "productNum");
		},
	})
	colOpts.push({
		"data": "blendCode",
		"title": biolims.common.mixNumber,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "blendCode");
		},
	})
	colOpts.push({
		"data": "posId",
		"title": biolims.common.location,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})
	colOpts.push({
		"data": "counts",
		"title": biolims.common.counts,
		//"className": "edit",
		"createdCell": function(td, data, rowdata) {
		$(td).parent("tr").css("background-color", rowdata.color);
			$(td).attr("saveName", "counts");
		},
	})
	colOpts.push({
		"data": "chromosomalLocation",
		"title": biolims.sanger.chromosomalLocation,
		"visible": flg,
		"createdCell": function(td) {
			$(td).attr("saveName", "chromosomalLocation");
		}
	})
	
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#bloodSplit_state").text()!=biolims.common.finish){
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#immuneCellProductionMakeUpAfdiv"),
				"/experiment/cell/immunecell/immuneCellProduction/delImmuneCellProductionItem.action","免疫细胞生产删除已排版样本：",$("#immuneCellProduction_id").text());
		}
	});
	tbarOpts.push({
		text: biolims.common.Editplay,
		action: function() {
			editItemLayer($("#immuneCellProductionMakeUpAfdiv"))
		}

	})
	if($("#maxNum").val()>0){
	tbarOpts.push({
		text: biolims.common.plateAgain,
		action: function() {
			plateLayoutAgain($("#immuneCellProductionMakeUpAfdiv"),
				"/experiment/cell/immunecell/immuneCellProduction/delImmuneCellProductionItemAf.action")
		}
	})
	}
	
	tbarOpts.push({
		text: biolims.common.productType,
		action: function() {
			addSampleType();
		}
	})
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveImmuneCellProductionMakeUpAfTab();
		}
	})
	tbarOpts.push({
		text : biolims.common.productNum,
		action : function() {
			var rows = $("#ImmuneCellProductionMakeUpAfdiv .selected");
			var length = rows.length;
			if (!length) {
				top.layer.msg(biolims.common.pleaseSelect);
				return false;
			}
			top.layer.open({
				type : 1,
				title : biolims.common.productNum,
				content : $('#batch_data').html(),
				area:[document.body.clientWidth-600,document.body.clientHeight-200],
				btn: biolims.common.selected,
				yes : function(index, layero) {
					var productNum = $("#productNum",parent.document).val();
					rows.addClass("editagain");
					rows.find("td[savename='productNum']").text(productNum);
					top.layer.close(index);
				}
			});
		}
	})
	//按钮添加质控品
	/*tbarOpts.push({
		text: biolims.wk.addQuality,
		action: function() {
			addRightQuality();
		}
	});*/
	}
	var immuneCellProductionMakeUpAfOps = table(true, immuneCellProduction_id, "/experiment/cell/immunecell/immuneCellProduction/showImmuneCellProductionItemAfTableJson.action", colOpts, tbarOpts);
	immuneCellProductionMakeUpAfTab = renderData($("#immuneCellProductionMakeUpAfdiv"), immuneCellProductionMakeUpAfOps);
	//选择数据并提示
	immuneCellProductionMakeUpAfTab.on('draw', function() {
		var index = 0;
		$("#immuneCellProductionMakeUpAfdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
		oldChangeLog = immuneCellProductionMakeUpAfTab.ajax.json();
		
	});
});
//添加样本类型
function addSampleType() {
	var rows = $("#immuneCellProductionMakeUpAfdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.selectSampleType,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/sample/dicSampleType/selectDicSampleTypeOne.action", ''],
		yes: function(index, layero) {
			var type = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td")
				.eq(1).text();
			var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td").eq(
				0).text();
			rows.addClass("editagain");
			rows.find("td[savename='dicSampleTypeName']").attr(
				"dicSampleType-id", id).text(type);
			rows.find("td[savename='dicSampleTypeId']").text(id);

			top.layer.close(index)
		},
	})
}

function plateLayoutAgain(ele, urll) {
	var arr = [];
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.confirm(biolims.common.rearrange + length + biolims.common.record, function(index) {
		top.layer.close(index);
		rows.each(function(i, val) {
			var id = $(val).find("input[type=checkbox]").val();
			arr.push(id);
		});
		if(arr.length) {
			$.ajax({
				type: "post",
				data: {
					ids: arr
				},
				url: ctx + urll,
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						immuneCellProductionMakeUpAfTab.ajax.reload();
						immuneCellProductionMakeUpTab.ajax.reload();
						$(".mysample").each(function(i, div) {
							var id = $(div).attr("sid");
							arr.forEach(function(val, j) {
								if(id == val) {
									$(div).removeAttr("sid").removeAttr("title").removeClass().css("background-color", "#fff");
								}
							});
						});
					}
				}
			});
		}

	});

}
// 保存
function saveImmuneCellProductionMakeUpAfTab() {
    var ele=$("#immuneCellProductionMakeUpAfdiv");
	var changeLog = "免疫细胞生产：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	if(!data){
		return false;
	}
	var changeLogs = "";
	if(changeLog != ""){
		changeLogs = changeLog
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/experiment/cell/immunecell/immuneCellProduction/saveMakeUp.action',
		data: {
			id: immuneCellProduction_id,
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
			top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
			} else {
			top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
var flag =true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
			//if(k == "dicSampleTypeName") {
			//	json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
			//	continue;
			//}
			json[k] = $(tds[j]).text();
		}
		if(k=="productNum"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg(biolims.common.pleaseFillDicNum);
					return false;
				}
				
			}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}
//混样
if($("#isBlend").val() == 0) {
	$("#isBlendBtn").hide();
}else{
	 $(window).scroll(function(event){
		if($(window).scrollTop()>400){
			$("#isBlendBtn").fadeOut();
		}else{
			$("#isBlendBtn").fadeIn();
		}
    });
}

function compoundSample() {
	var rows = $("#immuneCellProductionMakeUpAfdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var color = {
			r: 0,
			g: 0,
			b: 0
		},
		min = 380,
		max = 720,
		minHex = parseInt('99', 16),
		maxHex = parseInt('DD', 16);

	while(true) {
		color = getRGB(minHex, maxHex);
		if((color.r + color.g + color.b) >= min && (color.r + color.g + color.b) <= max) {
			break;
		}
	}
	var color = '#' + toHex(color.r) + toHex(color.g) + toHex(color.b);
	rows.attr("background", color).addClass("editagain").css("background-color", color);
	var blendCode = $("#blendCode").val();
	if(!blendCode) {
		$.ajax({
			type: "post",
			url: ctx + "/experiment/cell/immunecell/immuneCellProduction/generateBlendCode.action",
			data: {
				id: immuneCellProduction_id,
			},
			async: false,
			success: function(data) {
				if(data == "null") {
					blendCode = 0;
				} else {
					blendCode = data;
				}
			}
		});
	}
	rows.each(function(i, v) {
		$(v).children("td[savename='blendCode']").text(parseInt(blendCode) + 1);
	});
	$("#blendCode").val(parseInt(blendCode) + 1);
}
/* 
 * 
 * 描述: 随机生成一个背景色。还需要限制一下颜色区间值，最小值#999999，rgb(153,153,153)，最大值#DDDDDD，rgb(221,221,221)。将RGB三个值相加控制在580-720之间，这样当r=153时，g和b的值就得在214以上。这就是说rgb的每个随机值可以在153-221之间，但是最终的三个值由其他一个或两个值牵制着，颜色就即随机又不会太暗或太亮。
 */
function getRGB(min, max) {
	return {
		r: min + Math.round(Math.random() * 1000) % (max - min),
		g: min + Math.round(Math.random() * 1000) % (max - min),
		b: min + Math.round(Math.random() * 1000) % (max - min)
	};
}

function toHex(val) {
	var hex = '00';
	if(val) {
		hex = parseInt(val).toString(16);
		if(hex.length == 1) {
			hex = '0' + hex;
		}
	}

	return hex;
}
/*
 * 
 * 创建者 : 郭恒开
 * 创建日期: 2018/03/08
 * 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
//选择添加质控品
function addRightQuality() {
	top.layer.open({
		title: biolims.common.qualityProduct,
		type: 2,
		area: ["60%", "65%"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/quality/qualityProduct/qualityProductSelect.action", ''],
		yes: function(index, layer) {
//			var name = [],
			var ImmuneCellProduction_id=$("#ImmuneCellProduction_id").html();
			
			var	id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .selected").each(function(i, v) {
//				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			
			$.ajax({  
		        url:ctx+"/experiment/cell/immunecell/immuneCellProduction/saveRightQuality.action",  
		        data:{
		        	id:ImmuneCellProduction_id,
			        ids:id
		        },  
		        type:'post',  
		        dataType:"json",  
		        success:function(data){  
		        	window.location.href=ctx+"/experiment/cell/immunecell/immuneCellProduction/showImmuneCellProductionItemTable.action?id="+data.id;
		        	top.layer.close(index);
		        }  
		});
		},
		
	});
}