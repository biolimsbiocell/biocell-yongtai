var masterailsTable;
$(function() {
	var colData = [{
			"data": "id",
			"title": biolims.common.id,
			"visible":false
		},{
			"data": "sampleCode",
			"title": biolims.common.sampleCode,
		},{
			"data": "code",
			"title": biolims.common.code,
		},{
			"data": "name",
			"title": biolims.user.eduName1,
		},{
			"data": "cellType-id",
			"title": biolims.common.cellTypeId,
			"visible":false
		},{
			"data": "cellType-name",
			"title": biolims.common.cellType,
		},{
			"data": "cultureType-id",
			"title": biolims.common.cellTypeId,
			"visible":false
		},{
			"data": "cultureType-name",
			"title": biolims.common.cellType,
		},{
			"data": "pronoun",
			"title": biolims.common.pronoun,
		},{
			"data": "type",
			"title": biolims.common.type,
			"visible":false
		},{
			"data": "strain-id",
			"title": biolims.common.strainId,
			"visible":false
		},{
			"data": "strain-name",
			"title": biolims.common.strainName,
			"visible":false
		},{
			"data": "stockNum",
			"title": biolims.storage.inNum,
		},{
			"data": "inDate",
			"title": biolims.tStorageReagentBuySerial.inDate,
			"visible":false
		},{
			"data": "source",
			"title": biolims.sample.sourceName,
		},{
			"data": "dicState-id",
			"title": biolims.ufTask.state,
			"visible":false
		},{
			"data": "dicState-name",
			"title": biolims.ufTask.stateName,
			"visible":false
		},{
			"data": "materailsName",
			"title": biolims.ufTask.note,
		},{
			"data": "content2",
			"title": "content2",
			"visible":false
		},{
			"data": "content3",
			"title": "content3",
			"visible":false
		},{
			"data": "content4",
			"title": "content4",
			"visible":false
		},{
			"data": "content5",
			"title": "content5",
			"visible":false
		},{
			"data": "content6",
			"title": "content6",
			"visible":false
		},{
			"data": "content7",
			"title": "content7",
			"visible":false
		},{
			"data": "content8",
			"title": "content8",
			"visible":false
		},{
			"data": "content9",
			"title": "content9",
			"visible":false
		},{
			"data": "content10",
			"title": "content10",
			"visible":false
		},{
			"data": "content11",
			"title": "content11",
			"visible":false
		},{
			"data": "content12",
			"title": "content12",
			"visible":false
		}];
	var tbarOpts = [];
	tbarOpts.push({
		text: "showTree",
		action: function() {
			showTree();
		}
	});
	$.ajax({
		type:"post",
		url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
		async:false,
		data:{
			moduleValue : "MaterailsMain"
		},
		success:function(data){
			var objData = JSON.parse(data);
			if(objData.success){
				$.each(objData.data, function(i,n) {
					var str = {
						"data" : n.fieldName,
						"title" : n.label
					}
					colData.push(str);
				});
				
			}else{
				top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
			}
		}
	});
	var options = table(true, "",
			"/experiment/cell/materailsMain/showMaterailsMainListJson.action",colData , tbarOpts)
		masterailsTable = renderRememberData($("#main"), options);
		//恢复之前查询的状态
		$('#main').on('init.dt', function() {
			recoverSearchContent(masterailsTable);
		})
})
// 新建
function add() {
	window.location = window.ctx + "/experiment/cell/materailsMain/editMaterailsMain.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/experiment/cell/materailsMain/editMaterailsMain.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/experiment/cell/materailsMain/viewMasterailsMain.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser"
		},
		{

			"txt": biolims.common.gender,
			"type": "select",
			"options": biolims.common.pleaseChoose+"|"+biolims.common.male+"|"+biolims.common.female+"|"+biolims.common.unknown,
			"changeOpt": "''|1|0|3",
			"searchName": "gender"
		},
		{
			"txt": biolims.wk.confirmUserName,
			"type": "top.layer",
			"searchName": "confirmUser",
			"action": "confirmUser()"
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sample.createDateEnd,
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"txt": biolims.common.confirmDateStart,
			"type": "dataTime",
			"searchName": "confirmDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt":biolims.common.confirmDateEnd,
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "confirmDate##@@##2"
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "table",
			"table": sampleOrderTable
		}
	];
}
function showTree() {
//	var selRecord = masterailsTable.getSelectionModel().getSelections();
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
	}else{
		$(".ipadmini").hide();
		$("#content").css("min-height","0px");
		$(".content").css("min-height","0px");
		$("#cellTree").show();
		var myChart = echarts.init(document.getElementById('cellTree'));
	myChart.showLoading();
	myChart.clear();
	$.get('/experiment/cell/materailsMain/showCellTree.action?sampleCode='
			+ $(".selected").children("td").eq(1).text()+"&code="+ $(".selected").children("td").eq(2).text(), function(data) {
		myChart.hideLoading();
		data = eval("("+data+")");
		myChart.setOption(option = {
			backgroundColor: 'white',
			title:{
				show:true,
				text:'细胞传代关系图'
			},
			tooltip : {
				trigger : 'item',
				triggerOn : 'mousemove'
			},
			series : [ {
				type : 'tree',
				data : data,
				left : '2%',
				right : '2%',
				top : '8%',
				bottom : '20%',
				symbol : 'emptyCircle',
				orient : 'vertical',
				expandAndCollapse : true,
				label : {
					normal : {
						position : 'left',
						rotate : 0,
						verticalAlign : 'middle',
						align : 'right',
						fontSize : 10,
					}
				},
				leaves : {
					label : {
						normal : {
							position : 'bottom',
							rotate : -90,
							verticalAlign : 'middle',
							align : 'left'
						}
					}
				},
				animationDurationUpdate : 750
			} ]
		});
	});}
	myChart.on('click', function (params) {
		console.log(params);
		if(params.dataIndex=="1"){
			top.layer.msg(biolims.common.actionValue);
			return false;
		}
		var code=encodeURIComponent(params.name)
		window.location = window.ctx
		+ '/experiment/cell/materailsMain/editMaterailsMainByCode.action?code='
		+ code;
	});
}