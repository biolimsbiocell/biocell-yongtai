var materailsMainDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'materailsName',
		type:"string"
	});
	    fields.push({
		name:'type',
		type:"string"
	});
	    fields.push({
		name:'project-id',
		type:"string"
	});
	    fields.push({
		name:'project-name',
		type:"string"
	});
	    fields.push({
		name:'stockNum',
		type:"string"
	});
	    fields.push({
		name:'inDate',
		type:"string"
	});
	    fields.push({
		name:'location',
		type:"string"
	});
	    fields.push({
		name:'source',
		type:"string"
	});
	    fields.push({
		name:'dicState-id',
		type:"string"
	});
	    fields.push({
		name:'dicState-name',
		type:"string"
	});
	    fields.push({
		name:'pickUser-id',
		type:"string"
	});
	    fields.push({
		name:'pickUser-name',
		type:"string"
	});
	    fields.push({
		name:'tissue-id',
		type:"string"
	});
	    fields.push({
		name:'tissue-name',
		type:"string"
	});
	    fields.push({
		name:'pickDate',
		type:"string"
	});
	    fields.push({
		name:'content2',
		type:"string"
	});
	    fields.push({
		name:'content3',
		type:"string"
	});
	    fields.push({
		name:'content4',
		type:"string"
	});
	    fields.push({
		name:'content5',
		type:"string"
	});
	    fields.push({
		name:'content6',
		type:"string"
	});
	    fields.push({
		name:'content7',
		type:"string"
	});
	    fields.push({
		name:'content8',
		type:"string"
	});
	    fields.push({
		name:'content9',
		type:"string"
	});
	    fields.push({
		name:'content10',
		type:"string"
	});
	    fields.push({
		name:'content11',
		type:"string"
	});
	    fields.push({
		name:'content12',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'name',
		header:'材料名称',
		width:30*10,
		sortable:true
	});
	var typestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [[ '0', '细胞' ], [ '1', '质粒' ], [ '2', 'BAC' ],[ '4', '精子' ], [ '5', '胚胎' ], [ '6', '组织' ]]
	});
	
	var typeComboxFun = new Ext.form.ComboBox({
		store : typestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	
	cm.push({
		dataIndex:'type',
		header:'类型',
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(typeComboxFun),				
		sortable:true
	});
		cm.push({
		dataIndex:'project-id',
		header:'所属项目ID',hidden:true,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'project-name',
		header:'所属项目',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'stockNum',
		header:'在库数量',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'inDate',
		header:'入库日期',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'source',
		header:'来源',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'dicState-id',
		header:'状态ID',hidden:true,
		width:15*10,
		sortable:true
	});
		cm.push({
		dataIndex:'dicState-name',
		header:'状态',
		width:15*10,
		sortable:true
	});
		cm.push({
			dataIndex:'materailsName',
			header:'备注',
			width:20*10,
			sortable:true
	});
		cm.push({
			dataIndex:'pickUser-id',
			hidden : true,
			header:'摘取人ID',
			width:30*6

	});
		cm.push({
			dataIndex:'pickUser-name',
			hidden : true,
			header:'摘取人',
			width:20*6
	});
		cm.push({
			dataIndex:'tissue-id',
			hidden : true,
			header:'组织类型ID',
			width:15*10
	});
		cm.push({
			dataIndex:'tissue-name',
			hidden : true,
			header:'组织类型',
			width:30*6
	});
		cm.push({
			dataIndex:'pickDate',
			hidden : true,
			header:'摘取日期',
			width:10*6
	});
	/*
	cm.push({
		dataIndex:'content2',
		header:'content2',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content3',
		header:'content3',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content4',
		header:'content4',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content5',
		header:'content5',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content6',
		header:'content6',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content7',
		header:'content7',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content8',
		header:'content8',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content9',
		header:'content9',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content10',
		header:'content10',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content11',
		header:'content11',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'content12',
		header:'content12',
		width:15*10,
		sortable:true
	});*/

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/materials/materailsMain/showDialogMaterailsMainListJson.action?type="+$("#type").val()+"&projectId="+$("#projectId").val();
	var opts={};
	opts.title="实验材料管理";
	opts.height=document.body.clientHeight-200;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
//		window.parent.setMaterailsMainFun(rec);
		setMaterailsMainFun(rec);
	};
	materailsMainDialogGrid=gridTable("show_dialog_materailsMain_div",cols,loadParam,opts);
});

var type=$("#type").val();
if(type=="0"){
//-------------------------细胞明细
var materailsMainCellDialogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
//	   fields.push({
//		name:'id',
//		type:"string"
//	});
	    fields.push({
		name:'materailsMainId',
		type:"string"
	});
	    fields.push({
		name:'materailsMainName',
		type:"string"
	});
	    fields.push({
		name:'cellName',
		type:"string"
	});
	fields.push({
		name:'cellId',
		type:"string"
	});
	fields.push({
		name:'isMy',
		type:"string"
	});
	    fields.push({
		name:'frozenUser-id',
		type:"string"
	});
	    fields.push({
		name:'frozenUser-name',
		type:"string"
	});
	    fields.push({
			name:'strain-id',
			type:"string"
		});
		    fields.push({
			name:'strain-name',
			type:"string"
		});
	    fields.push({
			name:'frozenLocation',
			type:"string"
		});
 	   fields.push({
		name:'frozenCellsNum',
		type:"string"
	});
	   fields.push({
		name:'generation',
		type:"string"
	});
	   fields.push({
		name:'size',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'saveUser',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'materailsBorrow-id',
		type:"string"
	});
	    fields.push({
		name:'materailsBorrow-name',
		type:"string"
	});
	    fields.push({
			name:'cloneCode',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
//	cm.push({
//		dataIndex:'id',
//		hidden : true,
//		header:'编码',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'materailsMainId',
		hidden : true,
		header:'材料主数据ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'materailsMainName',
		hidden : false,
		header:'细胞名称',
		width:15*10
	});
	cm.push({
		dataIndex:'cellId',
		hidden : true,
		header:'细胞ID',
		width:15*10
	});
	/*cm.push({
		dataIndex:'cellName',
		hidden : false,
		header:'细胞名称',
		width:15*10
	});*/
	cm.push({
		dataIndex:'cloneCode',
		hidden : false,
		header:'克隆号',
		width:15*10
	});
	cm.push({
		dataIndex:'isMy',
		hidden : false,
		header:'支原体检测(有/无)',
		width:20*6
	});
	cm.push({
		dataIndex:'frozenUser-id',
		hidden : true,
		header:'冻存人ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'frozenUser-name',
		hidden : false,
		header:'冻存人',
		width:15*10
	});
	cm.push({
		dataIndex:'frozenLocation',
		hidden : true,
		header:'位置ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'starin-id',
		hidden : true,
		header:'品系ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'strain-name',
		hidden : false,
		header:'品系',
		width:15*10
	});
	cm.push({
		dataIndex:'frozenCellsNum',
		hidden : false,
		header:'冻存细胞数',
		width:15*6
	});
	cm.push({
		dataIndex:'generation',
		hidden : false,
		header:'代次',
		width:20*6
	});
	cm.push({
		dataIndex:'size',
		hidden : false,
		header:'规格',
		width:20*6
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'数量',
		width:20*6
	});
	cm.push({
		dataIndex:'saveUser',
		hidden : false,
		header:'保存人',
		width:30*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:50*6
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/materials/materailsMain/showMaterailsMainCellList.action";
	var opts={};
	opts.title="实验材料(Cell)明细";
	opts.height=document.body.clientHeight-300;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
//	opts.tbar.push({
//		text : '生成明细',
//		handler : addCell
//	});
	materailsMainCellDialogGrid=gridEditTable("materailsMainCellDialogdiv",cols,loadParam,opts);
	$("#materailsMainCellDialogdiv").data("materailsMainCellDialogGrid", materailsMainCellDialogGrid);
});
}else if(type=="1"){
//
//
//-------------------------质粒明细
var materailsMainPlasmidDialogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
//	   fields.push({
//		name:'id',
//		type:"string"
//	});
	    fields.push({
		name:'materailsMainId',
		type:"string"
	});
	    fields.push({
		name:'materailsMainName',
		type:"string"
	});
    fields.push({
		name:'plasmidId',
		type:"string"
	});
	    fields.push({
		name:'plasmidName',
		type:"string"
	});
    fields.push({
		name:'type-id',
		type:"string"
	});
	    fields.push({
		name:'typeName',
		type:"string"
	});
	   fields.push({
		name:'shortName',
		type:"string"
	});
	   fields.push({
		name:'property',
		type:"string"
	});
	   fields.push({
		name:'resistance',
		type:"string"
	});
	    fields.push({
		name:'reformUser-id',
		type:"string"
	});
	    fields.push({
		name:'reformUser-name',
		type:"string"
	});
	   fields.push({
		name:'reformDate',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'frozenLocation',
			type:"string"
		});
	   fields.push({
			name:'saveUser',
			type:"string"
		});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'materailsMainId',
		hidden : true,
		header:'材料主数据ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'materailsMainName',
		hidden : false,
		header:'质粒名称',
		width:15*10
	});
	cm.push({
		dataIndex:'plasmidId',
		hidden : true,
		header:'ID',
		width:20*6
	});
	/*cm.push({
		dataIndex:'plasmidName',
		hidden : false,
		header:'名称',
		width:20*6
	});*/
	cm.push({
		dataIndex:'typeName',
		hidden : false,
		header:'类型',
		width:15*10
	});
	cm.push({
		dataIndex:'shortName',
		hidden : false,
		header:'简称',
		width:20*6
	});
	cm.push({
		dataIndex:'property',
		hidden : false,
		header:'特征',
		width:40*6
	});
	cm.push({
		dataIndex:'resistance',
		hidden : false,
		header:'抗性',
		width:40*6
	});
	cm.push({
		dataIndex:'reformUser-name',
		hidden : false,
		header:'改造人',
		width:15*10
	});
	cm.push({
		dataIndex:'reformDate',
		hidden : false,
		header:'改造日期',
		width:15*6
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'管数',
		width:20*6
	});

	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度ng/ul',
		width:20*6
	});
	cm.push({
		dataIndex:'frozenLocation',
		hidden : false,
		header:'位置',
		width:15*10
	});
	cm.push({
		dataIndex:'saveUser',
		hidden : false,
		header:'保存人',
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/materials/materailsMain/showMaterailsMainPlasmidList.action";
	var opts={};
	opts.title="实验材料(质粒)明细";
	opts.height =  document.body.clientHeight-300;
	opts.tbar = []; 
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
//	opts.tbar.push({
//		text : '生成明细',
//		handler : addPlasmid
//	});
	
	
	materailsMainPlasmidDialogGrid=gridEditTable("materailsMainPlasmidDialogdiv",cols,loadParam,opts);
	$("#materailsMainPlasmidDialogdiv").data("materailsMainPlasmidDialogGrid", materailsMainPlasmidDialogGrid);
});
}else  if(type=="2"){

//
//
//-------------------------BAC明细
var materailsMainBACDialogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
//		fields.push({
//			name:'id',
//			type:"string"
//		});
	    fields.push({
			name:'materailsMainId',
			type:"string"
		});
		    fields.push({
			name:'materailsMainName',
			type:"string"
		});
		    fields.push({
			name:'type-id',
			type:"string"
		});
		    fields.push({
			name:'typeName',
			type:"string"
		});
		   fields.push({
			name:'englishName',
			type:"string"
		});
		   fields.push({
			name:'property',
			type:"string"
		});
		   fields.push({
			name:'resistance',
			type:"string"
		});
		    fields.push({
			name:'hostType-id',
			type:"string"
		});
		    fields.push({
			name:'hostTypeName',
			type:"string"
		});
		   fields.push({
			name:'saveCondition',
			type:"string"
		});
		    fields.push({
			name:'specoes-id',
			type:"string"
		});
		    fields.push({
			name:'specoesName',
			type:"string"
		});
		    fields.push({
			name:'unit-id',
			type:"string"
		});
		    fields.push({
			name:'unitName',
			type:"string"
		});
	    fields.push({
			name:'materailsMainBac-num',
			type:"string"
		});
	   fields.push({
		name:'num',
		type:"string"
	   });
	    fields.push({
		name:'materailsBorrow-id',
		type:"string"
	});
	    fields.push({
		name:'materailsBorrow-name',
		type:"string"
	});
    fields.push({
		name:'bacId',
		type:"string"
	});
	    fields.push({
		name:'bacName',
		type:"string"
	});
	    fields.push({
			name:'saveUser',
			type:"string"
		});
		   fields.push({
				name:'volume',
				type:"string"
			});
		   fields.push({
				name:'frozenLocation',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'materailsMainId',
		hidden : true,
		header:'材料主数据ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'materailsMainName',
		hidden : false,
		header:'BAC名称',
		width:15*10
	});
//	cm.push({
//		dataIndex:'id',
//		hidden : true,
//		header:'编码',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'bacId',
		hidden : true,
		header:'BAC编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'bacName',
		hidden : false,
		header:'BAC名称',
		width:20*6

	});*/

	cm.push({
		dataIndex:'typeName',
		hidden : false,
		header:'类型',
		width:10*10
	});
	cm.push({
		dataIndex:'englishName',
		hidden : false,
		header:'英文',
		width:20*6
	});
	cm.push({
		dataIndex:'property',
		hidden : false,
		header:'特征',
		width:30*6

	});
	cm.push({
		dataIndex:'resistance',
		hidden : false,
		header:'抗性',
		width:20*6
	});
	cm.push({
		dataIndex:'hostTypeName',
		hidden : false,
		header:'宿主类型',
		width:15*10
	});
	cm.push({
		dataIndex:'saveCondition',
		hidden : false,
		header:'保存条件',
		width:30*6
	});
	cm.push({
		dataIndex:'specoesName',
		hidden : false,
		header:'种属',
		width:10*10
	});
	cm.push({
		dataIndex:'unitName',
		hidden : false,
		header:'计量单位',
		width:15*10
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'数量',
		width:10*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6
	});
	cm.push({
		dataIndex:'frozenLocation',
		hidden : false,
		header:'位置',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/materials/materailsMain/showMaterailsMainBACList.action";
	var opts={};
	opts.title="实验材料(BAC)明细";
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	cm.push({
		dataIndex:'saveUser',
		hidden : false,
		header:'保存人',
		width:30*6
	});
//	opts.tbar.push({
//		text : '生成明细',
//		handler : addBac
//	});
//			
	materailsMainBACDialogGrid=gridEditTable("materailsMainBACDialogdiv",cols,loadParam,opts);
	$("#materailsMainBACDialogdiv").data("materailsMainBACDialogGrid", materailsMainBACDialogGrid);
});

} else if(type=="6"){
//-------------------------组织明细
var materailsMainTissueDialogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
//		fields.push({
//			name:'id',
//			type:"string"
//		});
	    fields.push({
			name:'materailsMainId',
			type:"string"
		});
		    fields.push({
			name:'materailsMainName',
			type:"string"
		});
		    fields.push({
			name:'materailsMainType',
			type:"string"
		});
		   fields.push({
			name:'materailsMainStockNum',
			type:"string"
		});
	    fields.push({
			name:'materailsMainPickUserId',
			type:"string"
		});
		    fields.push({
			name:'materailsMainPickUserName',
			type:"string"
		});
		    fields.push({
				name:'materailsMainTissueId',
				type:"string"
			});
			    fields.push({
				name:'materailsMainTissueName',
				type:"string"
			});
			    fields.push({
					name:'materailsMainPickDate',
					type:"string"
				});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'materailsMainId',
		hidden : true,
		header:'材料主数据ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'materailsMainName',
		hidden : false,
		header:'组织名称',
		width:15*10
	});

	cm.push({
		dataIndex:'type',
		hidden : false,
		header:'类型',
		width:10*10
	});
	cm.push({
		dataIndex:'stockNum',
		hidden : false,
		header:'在库数量',
		width:20*6
	});
	cm.push({
		dataIndex:'pickUser-id',
		hidden : true,
		header:'摘取人ID',
		width:30*6

	});
	cm.push({
		dataIndex:'pickUser-name',
		hidden : false,
		header:'摘取人',
		width:20*6
	});
	cm.push({
		dataIndex:'tissue-id',
		hidden : true,
		header:'组织类型ID',
		width:15*10
	});
	cm.push({
		dataIndex:'tissue-name',
		hidden : false,
		header:'组织类型',
		width:30*6
	});
	cm.push({
		dataIndex:'pickDate',
		hidden : false,
		header:'摘取日期',
		width:10*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/materials/materailsMain/showMaterailsMainBACList.action";
	var opts={};
	opts.title="实验材料(组织)明细";
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});

	materailsMainTissueDialogGrid=gridEditTable("materailsMainTissueDialogdiv",cols,loadParam,opts);
	$("#materailsMainTissueDialogdiv").data("materailsMainTissueDialogGrid", materailsMainTissueDialogGrid);
});

}else if(type=="4"){
	//-------------------------精子明细
	var materailsMainSemenDialogGrid;
	$(function(){
		var cols={};
		cols.sm = true;
		var fields=[];
		fields.push({
			name:'id',
			type:"string"
		});
		fields.push({
			name:'name',
			type:"string"
		});
		fields.push({
			name:'strawId',
			type:"string"
		});
		fields.push({
			name:'freezingUser-id',
			type:"string"
		});
		fields.push({
			name:'freezingUser-name',
			type:"string"
		});
		fields.push({
			name:'num',
			type:"string"
		});
		fields.push({
			name:'genType',
			type:"string"
		});
		fields.push({
			name:'frozenLocation',
			type:"string"
		});
		fields.push({
			name:'freezingDate',
			type:"string",
//			dateFormat:"Y-m-d"
		});
		fields.push({
			name:'saveCentigrade',
			type:"string"
		});
		fields.push({
			name:'amount',
			type:"string"
		});
		fields.push({
			name:'note',
			type:"string"
		});
		fields.push({
			name:'materailsMain-id',
			type:"string"
		});
		fields.push({
			name:'materailsMain-name',
			type:"string"
		});
		fields.push({
			name:'animalIds',
			type:"string"
		});

		cols.fields=fields;
		var cm=[];
		cm.push({
			dataIndex:'id',
			hidden : true,
			header:'编码',
			width:20*6	
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'name',
			hidden : false,
			header:'精子名称',
			width:20*6
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'animalIds',
			hidden : false,
			header:'动物信息',
			width:30*6
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'genType',
			hidden : false,
			header:'基因型',
			width:10*6
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'strawId',
			hidden : false,
			header:'麦管ID',
			width:10*6
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'frozenLocation',
			hidden : false,
			header:'位置',
			width:15*10
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'num',
			hidden : false,
			header:'数量',
			width:10*6
//			editor : new Ext.form.NumberField({
//				allowDecimals: false, // 不允许小数点 
//				allowNegative: false, // 不允许负数 
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'freezingUser-id',
			hidden : true,
			header:'冻存人ID',
			width:15*10
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'freezingUser-name',
			hidden : false,
			header:'冻存人',
			width:15*10
		});
		cm.push({
			dataIndex:'freezingDate',
			hidden : false,
			header:'冻存日期',
			width:20*6
//			renderer: formatDate,
//			editor: new Ext.form.DateField({format: 'Y-m-d'})
		});
		cm.push({
			dataIndex:'amount',
			hidden : false,
			header:'编号',
			width:10*6
//			editor : new Ext.form.NumberField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'saveCentigrade',
			hidden : false,
			header:'保存温度(℃)',
			width:20*6
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'note',
			hidden : false,
			header:'备注',
			width:50*6
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'materailsMain-id',
			hidden : true,
			header:'相关主表ID',
			width:15*10
//			editor : new Ext.form.TextField({
//				allowBlank : true
//			})
		});
		cm.push({
			dataIndex:'materailsMain-name',
			hidden : true,
			header:'相关主表',
			width:15*10
		});
		cols.cm=cm;
		var loadParam={};
		loadParam.url=ctx+"/tra/materials/materailsMain/showMaterailsMainSemenListJson.action?id="+ $("#id_parent_hidden").val();
		var opts={};
		opts.title="实验材料(精子)明细";
		opts.height =  document.body.clientHeight-100;
		opts.tbar = [];
		opts.delSelect = function(ids) {
			ajax("post", "/tra/materials/materailsMain/delMaterailsMainSemen.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					scpProToGrid.getStore().commitChanges();
					scpProToGrid.getStore().reload();
					message("删除成功！");
				} else {
					message("删除失败！");
				}
			}, null);
		};

		opts.tbar.push({
			text : "批量上传（CSV文件）",
			handler : function() {
				var options = {};
				options.width = 350;
				options.height = 200;
				loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
					"确定":function(){
						goInExcelcsv();
						$(this).dialog("close");
					}
				},true,options);
			}
		});


		function goInExcelcsv(){
			var file = document.getElementById("file-uploadcsv").files[0];  
			var n = 0;
			var ob = materailsMainSemenGrid.getStore().recordType;
			var reader = new FileReader();  
			reader.readAsText(file,'GB2312');  
			reader.onload=function(f){  
				var csv_data = $.simple_csv(this.result);
				$(csv_data).each(function() {
					if(n>0){
						if(this[0]){
							var p = new ob({});
							p.isNew = true;				
							var o;
							o= 0-1;
							p.set("po.fieldName",this[o]);


							o= 1-1;
							p.set("po.fieldName",this[o]);


							o= 2-1;
							p.set("po.fieldName",this[o]);


							o= 3-1;
							p.set("po.fieldName",this[o]);


							o= 4-1;
							p.set("po.fieldName",this[o]);


							o= 5-1;
							p.set("po.fieldName",this[o]);


							o= 6-1;
							p.set("po.fieldName",this[o]);


							o= 7-1;
							p.set("po.fieldName",this[o]);


							o= 8-1;
							p.set("po.fieldName",this[o]);


							o= 9-1;
							p.set("po.fieldName",this[o]);



							materailsMainSemenDialogGrid.getStore().insert(0, p);
						}
					}
					n = n +1;

				});
			};
		}
		opts.tbar.push({
			text : '显示可编辑列',
			handler : null
		});
		opts.tbar.push({
			text : '取消选中',
			handler : null
		});
		opts.tbar.push({
			text : '填加明细',
			handler : null
		});
		materailsMainSemenDialogGrid=gridEditTable("materailsMainSemenDialogdiv",cols,loadParam,opts);
		$("#materailsMainSemenDialogdiv").data("materailsMainSemenGrid", materailsMainSemenDialogGrid);
	});
}

function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(materailsMainDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
