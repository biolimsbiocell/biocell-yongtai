﻿$(function() {
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	//// 上传附件
	fileInput('1', 'materailsMain', $("#materailsMain_id").text());
});
//保存
function save() {
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	var jsonn = {};
	$("#form1 input").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	var data = {
		main: JSON.stringify(jsonn)
	};
	var changeLog = "细胞主数据：";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: "post",
		url: ctx + "/experiment/cell/materailsMain/save.action?changeLog="+changeLog+"&ids="+$("#materailsMain_id").text(),
		data: data,
		async:false,
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx + "/experiment/cell/materailsMain/editMaterailsMain.action?id="+data.id;
			}else{
				top.layer.closeAll();
				top.layer.msg("保存失败");
				}
		}
	});
}
/*
 * 创建者 : dwb
 * 创建日期: 2018-05-10 17:13:12
 * 文件描述: 修改日志
 * 保存增加日志
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		 oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

function list() {
	window.location = window.ctx + '/experiment/cell/materailsMain/showMaterailsMainList.action';
}
//上传附件
function fileUp() {
	if($("#materailsMain_id").text()=="NEW"){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
//查看附件
function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=materailsMain&id=" + $("#materailsMain_id").text(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
//细胞类型
function showCellType() {
	top.layer.open({
		title: biolims.common.cellType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=celltype", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#materailsMain_cellType_id").val(id);
			$("#materailsMain_cellType_name").val(name);
		},
	})
}
//细胞培養方式
function showCellCultureMethod() {
	top.layer.open({
		title: biolims.common.cellCultureMethod,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=cellpeiyang", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#materailsMain_cultureType_id").val(id);
			$("#materailsMain_cultureType_name").val(name);
		},
	})
}
/*
function add() {
	var type=$("#materailsMain_type").val();
	window.location = window.ctx + "/experiment/cell/materailsMain/editMaterailsMain.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/cell/materailsMain/showMaterailsMainList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	setLocation();
});	
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#materailsMain", {
		userId : userId,
		userName : userName,
		formId : $("#materailsMain_id").val(),
		title : $("#materailsMain_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp").click(function() {
	completeTask($("#materailsMain_id").val(), $(this).attr("taskId"), function() {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		location.href = window.ctx + '/dashboard/toDashboard.action';
	});
});

function save() {
	if(checkSubmit()==true){	
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/cell/materailsMain/save.action";
		form1.submit();

	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/cell/materailsMain/copyMaterailsMain.action?id=' + $("#materailsMain_id").val();
}
function changeState() {

	commonChangeState("formId=" + $("#materailsMain_id").val() + "&tableId=materailsMain");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#materailsMain_id").val());
	nsc.push("编码不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function(){
		var tabs=new Ext.TabPanel({
			id:'tabs11',
			renderTo:'maintab',
			height:document.body.clientHeight-30,
			autoWidth:true,
			activeTab:0,
			margins:'0 0 0 0',
			items:[{
				title:'细胞主数据',
				contentEl:'markup'
			} ]
		});
	});



	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

//function showType(){
//	var type=$("#materailsMain_type").val();
//	if(type=="0"){
//		load("/tra/materials/materailsMain/showMaterailsMainCellList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainCellpage");
//		load("/tra/materials/materailsMain/showMaterailsMainCultureList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainCulturepage");
//	}else if(type=="1"){
//		load("/tra/materials/materailsMain/showMaterailsMainPlasmidList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainPlasmidpage");
//	}else if(type=="2"){
//		load("/tra/materials/materailsMain/showMaterailsMainBACList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainBACpage");
//	}else if(type=="4"){
//		load("/tra/materials/materailsMain/showMaterailsMainSemenList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainSemenpage");
//	}else if(type=="5"){
//		load("/tra/materials/materailsMain/showMaterailsMainEmbryoList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainEmbryopage");
//	}else if(type=="6"){
//		load("/tra/materials/materailsMain/showMaterailsMainDNAList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainDNApage");
//	}
//	else if(type=="8"){
//		load("/tra/materials/materailsMain/showMaterailsMainGuineList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainGuinepage");
//	}
//	/*else{
//		load("/tra/materials/materailsMain/showMaterailsMainBACList.action", {
//			id : $("#materailsMain_id").val()
//		}, "#materailsMainBACpage");
//	}*/
//	setTimeout(function() {
//		sumNum();
//	}, 1000);
//}
/*var item = menu.add({
	text: '复制'
});
item.on('click', editCopy);


//设置位置存放记录
function setLocation(){
	var type=$("#materailsMain_type").val();
	var gridGrid = null;
	if(type=="0"){
		gridGrid =  materailsMainCellGrid.getStore();
	}else if(type=="1"){
		gridGrid = materailsMainPlasmidGrid.getStore();
	}else if(type=="2"){
		gridGrid = materailsMainBACGrid.getStore();
	}else if(type=="4"){
		gridGrid = materailsMainSemenGrid.getStore();
	}else if(type=="5"){
		gridGrid = materailsMainEmbryoGrid.getStore();
	}else if(type=="6"){
		gridGrid = materailsMainDNAGrid.getStore();
	}else if(type=="8"){
		gridGrid = materailsMainGuineGrid.getStore();
	}
	else{  //保存组织时的位置设置
		var idstr = "";
		var location = $("#materailsMain_frozenLocation").val();
		var id = $("#materailsMain_id").val();
		if(location!=null&&location!=""){
			idstr = id + "," + location;
			ajax("post", "/storage/position/setPosition.action", {
				idstr : idstr, 
				type : "9"
			}, function(data) {
				if (data.success) {	
					save();

					//message("生成主数据成功！");
				} else {
					message("存储位置设置失败,请重试！");
					return false;
				}
			}, null);
		}else{
			save();
			return ;
		}
	}
	if(type=="0"||type=="1"||type=="2"||type=="8"||type=="4"||type=="5"||type=="6"){
		if(gridGrid!=null){
			var idstr = "";
			var records = gridGrid.getModifiedRecords();
			Ext.each(records,function(record){//遍历行数据数组
				var address = record.get("frozenLocation");
				if(address!=null&&address!="")
					idstr = idstr + record.get("id") + "," + record.get("frozenLocation") + ";";
			});
			if(idstr!=""){
				ajax("post", "/storage/position/setPosition.action", {
					idstr : idstr, 
					type : "1"
				}, function(data) {
					if (data.success) {	
						save();

						//message("生成主数据成功！");
					} else {
						message("存储位置设置失败,请重试！");
						return false;
					}
				}, null);
			}else{
				save();
			}


		}else{
			save();
		}
	}

}

//选择冻存位置
function FrozenLocationFun2(){
	var type = $("#materailsMain_type").val();
	var win = Ext.getCmp('FrozenLocationFun2');
	if (win) {win.close();}
	var ProjectFun= new Ext.Window({
		id:'FrozenLocationFun2',modal:true,title:'选择位置',layout:'fit',width:600,height:580,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/storage/position/showStoragePositionTreeDialog.action?typeId="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			          { text: '关闭',
			        	  handler: function(){
			        		  ProjectFun.close(); }  }]  });     ProjectFun.show(); }

function setFrozenLocationFun(str){
	var ads = str.split(",");
	$("#materailsMain_frozenLocation").val(ads[0]);
	var win = Ext.getCmp('FrozenLocationFun2')
	if(win){win.close();}
}	
*/