$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编号",
	});
	colOpts.push({
		"data": "name",
		"title": "名称",
	});
	colOpts.push({
		"data": "storageContainer-name",
		"title": "容器",
	});
	colOpts.push({
		"data": "storageContainer-rowNum",
		"title": "行",
	});
	colOpts.push({
		"data": "storageContainer-colNum",
		"title": "列",
	});
	colOpts.push({
		"data": "totalLocationsNumber",
		"title": "总位置数",
	});
	colOpts.push({
		"data": "surplusLocationsNumber",
		"title": "剩余位置数",
	});
	var orderNum =$("#orderNum").val();
	var id =$("#id").val();
	var tbarOpts = [];
	var addSampleTypeOptions;
	if(	$("#orderNum").val()!=""&&$("#id").val()!=""){
		addSampleTypeOptions = table(false, null,
				'/experiment/cell/passage/cellPassageNow/showDialogDicSampleTypeTableJson.action?orderNum='+orderNum+'&id='+id,colOpts , tbarOpts)	
	}else{
	    addSampleTypeOptions = table(false, null,
				'/experiment/cell/passage/cellPassageNow/showDialogDicSampleTypeTableJson.action',colOpts , tbarOpts)
	}
	var sampleTypeTable = renderData($("#addDicSampleType"), addSampleTypeOptions);
	$("#addDicSampleType").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addDicSampleType_wrapper .dt-buttons").empty();
		$('#addDicSampleType_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addDicSampleType tbody tr");
	sampleTypeTable.ajax.reload();
	sampleTypeTable.on('draw', function() {
		trs = $("#addDicSampleType tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

