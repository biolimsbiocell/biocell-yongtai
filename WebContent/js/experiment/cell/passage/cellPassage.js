var cellPassageTab;
var childTable;
var userId= window.sessionStorage.getItem("userId")

$(function() {
	var options = table(
			true,
			"",
			"/experiment/cell/passage/cellPassage/showCellPassageTableJson.action",
			[
					{
						"data" : "id",
						"title" : "操作",
						"width" : "60px",
						"render" : function(data) {
							return "<button id="
									+ data
									+ " class='btn btn-info btn-xs plus' onclick='showSubTable(this)'>展开</button>&nbsp;&nbsp;<span class='glyphicon glyphicon-star text-green' style='display:none'></span>";
						},
						"createdCell" : function(td, data, rowdata) {
							if (rowdata.state == 3) {
								if (rowdata.opUser) {
									rowdata.opUser.split(",").forEach(
											function(v, i) {
												if (v == window.userName) {
													$(td).addClass("showTd");
												}
											});
								}
							}
						}
					}, {
						"data" : "id",
						"title" : biolims.common.id,
					}, {
						"data" : "batch",
						"title" : "产品批号",
					}, {
						"data" : "name",
						"title" : biolims.common.name,
					}, {
						"data" : "createUser-name",
						"title" : biolims.sample.createUserName,
					}, {
						"data" : "createDate",
						"title" : biolims.sample.createDate,
					},
					// {
					// "data" : "confirmDate",
					// "title" : biolims.common.confirmDate,
					// },
					{
						"data" : "testUserOneName",
						"title" : biolims.common.testUserName,
					}, {
						"data" : "template-name",
						"title" : biolims.common.experimentModule,
					}, {
						"data" : "scopeName",
						"title" : biolims.purchase.costCenter,
						"visible" : false,
					}, {
						"data" : "stateName",
						"title" : biolims.common.stateName,
					} ], null)
	cellPassageTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(cellPassageTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/cell/passage/cellPassage/editCellPassage.action?sampleStyle='
			+ $("#sampleStyle").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/cell/passage/cellPassage/editCellPassage.action?id='
			+ id + "&sampleStyle=" + $("#sampleStyle").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : "产品批号",
		"type" : "input",
		"searchName" : "batch",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	},/* {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"searchName" : "confirmDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "confirmDate##@@##2"
	},*/ {
		"type" : "table",
		"table" : cellPassageTab
	} ];
}
// 展示样本步骤
function showSubTable(that) {
	if (!$(that).parents("tr").next(".subTable").length) {
		$(that)
				.parents("tr")
				.after(
						'<tr class="subTable" style="display:none"><td colspan="11"><table id="childTab" class="table table-bordered table-condensed" style="font-size: 12px;"></table></td></tr>');
	}
	var corspanTr = $(that).parents("tr").next(".subTable");
	if ($(that).hasClass("plus")) { // 展开
		$(that).removeClass("plus").addClass("minus");
		that.innerText = "关闭";
		corspanTr.slideDown();
		var elemen = corspanTr.find("table");
		if (!elemen.find("tr").length) {
			var id = that.id;
			renderSubTable(elemen, id);
		}
	} else { // 关闭
		corspanTr.slideUp();
		that.innerText = "展开";
		$(that).removeClass("minus").addClass("plus");
	}
}
function renderSubTable(elemen, id) {
	btnData = [];
	colData = [
			{
				"data" : 'id',
				"title" : "ID",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "id");
				}
			},
			{
				"data" : "endTime",
				"title" : "操作",
				"width" : "45px",
				"render": function(data, type, row, meta) {
					console.log(row)
					if (data) {
						return "<button id="
								+ id
								+ " class='btn btn-warning btn-xs' onclick='viewStrps(this)'>查看详情</button>";
					} else {
						if(row['createUser']==userId){
							return "<button id="
							+ id
							+ " class='btn btn-info btn-xs' onclick='viewStrps(this)'>操作</button>&nbsp;&nbsp;<span class='glyphicon glyphicon-star text-green' style='display:none'></span>";
						}else{
							return "<button id="
							+ id
							+ " class='btn btn-info btn-xs' onclick='viewStrps(this)'>查看</button>&nbsp;&nbsp;<span class='glyphicon glyphicon-star text-green' style='display:none'></span>";
						}
						
					}
				},
				"createdCell" : function(td, data, rowdata) {
					if (rowdata.testUserList) {
						rowdata.testUserList.split(",").forEach(function(v, i) {
							if (v == window.userName) {
								$(td).addClass("showTd");
							}
						});
					}
				}

			}, /*
				 * { "data": 'code', "title": "code", "visible": false,
				 * "createdCell": function(td) { $(td).attr("saveName", "code"); } },
				 */{
				"data" : 'content',
				"title" : "content",
				"visible" : false,
				"createdCell" : function(td) {
					$(td).attr("saveName", "content");
				}
			}, /*
				 * { "data": 'contentData', "title": "contentData", "visible":
				 * false, "createdCell": function(td) { $(td).attr("saveName",
				 * "contentData"); } },
				 */{
				"data" : 'orderNum',
				"title" : "序号",
				"width" : "30px",
				"createdCell" : function(td, data, rowdata) {
					$(td).attr("saveName", "orderNum");
					$(td).attr("id", rowdata['id']);
					$(td).attr("code", rowdata['code']);
					$(td).attr("content", rowdata['content']);
					$(td).attr("contentData", rowdata['contentData']);
				}
			}, {
				"data" : 'templeItem-name',
				"title" : "步骤名称",
				"createdCell" : function(td) {
					$(td).attr("saveName", "templeItem-name");
				}
			}, {
				"data" : 'estimatedDate',
				"title" : "预计用时(天)",
				"width" : "50px",
				"createdCell" : function(td) {
					$(td).attr("saveName", "estimatedDate");
				}
			}, {
				"data" : 'planWorkDate',
				"title" : "预计开始时间",
				"width" : "100px",
				"createdCell" : function(td) {
					$(td).attr("saveName", "planWorkDate");
				}
			}, {
				"data" : 'planEndDate',
				"title" : "预计结束时间",
				"width" : "100px",
				"createdCell" : function(td) {
					$(td).attr("saveName", "planEndDate");
				}
			}, {
				"data" : 'createUser',
				"title" : "操作人",
				"createdCell" : function(td) {
					$(td).attr("saveName", "createUser");
				}
			}, {
				"data" : 'createDate',
				"title" : "开始时间",
				"width" : "100px",
				"createdCell" : function(td) {
					$(td).attr("saveName", "createDate");
				}
			},
			// {
			// "data": 'delay',
			// "title": "延时(/小时)",
			// "className": "edit",
			// "width": "60px",
			// "createdCell": function(td) {
			// $(td).attr("saveName", "delay");
			// }
			// },
			{
				"data" : 'endTime',
				"title" : "结束时间",
				"width" : "100px",
				"createdCell" : function(td) {
					$(td).attr("saveName", "endTime");
				}
			}, {
				"data" : 'note',
				"title" : "备注",
//				"className" : "edit",
				"createdCell" : function(td) {
					$(td).attr("saveName", "note");
				}
			}, ];
	// btnData.push({
	// text:"保存",
	// action:function(){
	// saveChildTab(id, elemen);
	// }
	// });
	// btnData.push({
	// text:"选择操作人",
	// action:function(){
	// var childTab = $("#childTab .selected");
	// console.log(childTab);
	// if(!childTab.length){
	// top.layer.msg("请选择数据");
	// return false;
	// }
	// top.layer.open({
	// title: biolims.common.pleaseChoose,
	// type: 2,
	// area: [document.body.clientWidth - 300, document.body.clientHeight -
	// 100],
	// btn: biolims.common.selected,
	// content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin",
	// ''],
	// yes: function(index, layer) {
	// var name = $('.layui-layer-iframe',
	// parent.document).find("iframe").contents().find("#addUserTable
	// .chosed").children("td").eq(1).text();
	// var id = $('.layui-layer-iframe',
	// parent.document).find("iframe").contents().find("#addUserTable
	// .chosed").children("td").eq(0).text();
	// top.layer.close(index);
	// // $("#testUserList").val(name);
	// childTab.addClass("editagain");
	// childTab.children("td").eq(7).text(name);
	// },
	// })
	// }
	// });
	childTable = table(
			true,
			id,
			"/experiment/cell/passage/cellPassageNow/findCellPassageTemplateListPage.action",
			colData, btnData);
	renderData(elemen, childTable);
}
function viewStrps(that) {
	var id = that.id;
	var orderNum = $(that).parent("td").parent("tr").index() + 1;
	sessionStorage.removeItem("dijibu");
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id="
			+ id + "&orderNum=" + orderNum;
}

// 获取table数据信息
function saveChildTabJson(elemen) {
	var data = [];
	var trs = elemen.find("tbody").children(".editagain");
	trs.each(function(i, v) {
		var json = {};
		var tds = $(v).children("td");
		console.log(tds.length);
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("saveName");
			if (!k) {
				continue;
			}
			json[k] = $(tds[j]).text();
			if (k == "orderNum") {
				json["id"] = $(tds[j]).attr("id");
				json["code"] = $(tds[j]).attr("code");
				json["content"] = $(tds[j]).attr("content");
				json["contentData"] = $(tds[j]).attr("contentData");
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		console.log(json);
		data.push(json);
	})
	return JSON.stringify(data);
}

// 保存table的方法
function saveChildTab(id, elemen) {
	var data = saveChildTabJson(elemen);
	top.layer.load(4, {
		shade : 0.3
	});
	$.ajax({
		type : 'post',
		url : '/experiment/cell/passage/cellPassage/saveChildTable.action',
		data : {
			id : id,
			dataJson : data
		},
		success : function(data) {
			var data = JSON.parse(data);
			if (data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				elemen.DataTable().ajax.reload();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			}
			;
		}
	})
}
