var cellPassageAllotTab;
$(function() {
	var colOpts=[];
	colOpts.push({
		"data": "batch",
		"title": "产品批号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "batch");
		}
	});
	colOpts.push({
		"data": "id",
		"visible": false,
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell" : function(td) {
			$(td).attr("saveName", "code");
		}
	});
//	colOpts.push({
//		"data": "sampleCode",
//		"title": biolims.common.sampleCode,
//	})
//	colOpts.push({
//		"data": "sampleInfo-sampleOrder-name",
//		"title": "姓名",
//	});
	colOpts.push({
		"data": "patientName",
		"title": "姓名",
	});
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
		"createdCell": function(td, data, rowData) {
			$(td).attr("class", "product-id").attr("productId", rowData['productId']);
		}
	});
//	colOpts.push({
//		"data": "pronoun",
//		"title": biolims.common.pronoun,
//	})
	/*colOpts.push({
		"data": "concentration",
		"title":biolims.common.concentration,
	})*/
//	colOpts.push({
//		"data": "volume",
//		"title": biolims.common.volume,
//	})
	colOpts.push({
		"data": "orderCode",
		"title": biolims.common.orderCode,
		"visible":false,
	});
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.sampleType,
	});
	colOpts.push({
		"data": "sampleInfo-id",
		"visible":false,
		"title": biolims.sample.sampleMasterData,
	});
//	colOpts.push({
//		"data": "sampleInfo-project-id",
//		"title":biolims.common.projectId,
//	})
//	colOpts.push({
//		"data": "scopeName",
//		"title": biolims.purchase.costCenter,
//		"createdCell": function(td,data,rowdata) {
//			var typeValue=rowdata["sampleInfo-changeType"];
//			if(typeValue=="0"){
//				$(td).parent("tr").children("td").eq(2).css('background-color','EAB9A8');
//			}else if(typeValue=="1"){
//				$(td).parent("tr").children("td").eq(2).css('background-color','30B573');
//			}else if(typeValue=="2"){
//				$(td).parent("tr").children("td").eq(2).css('background-color','8FAAC6');
//			}
//		}
//	})
//	colOpts.push({
//		"data": "sampleInfo-changeType",
//		"title": biolims.purchase.costCenter,
//		"visible":false
//	})
	var tbarOpts=[]
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	tbarOpts.push({
		text: "打印生产条码",
		action: function() {
			downCsvProduct()
		}
	});
	tbarOpts.push({
		text: "打印质检条码",
		action: function() {
			downCsv()
		}
	});
	tbarOpts.push({
		text: "样本退回",
		action:function(){
			backSample()
			//退回样本异常    样本不合格  当前订单作废？相关样本接收作废？   会重新录同轮次订单？
		}
	});
	var options = table(true,null,"/experiment/cell/passage/cellPassage/showCellPassageTempTableJson.action",colOpts, tbarOpts)
	//渲染样本的table
	cellPassageAllotTab=renderData($("#main"),options);
	
	
	//提示样本选中数量
	var selectednum=$(".selectednum").text();
		if(!selectednum){
			selectednum=0;
	}
		cellPassageAllotTab.on('draw', function() {
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				var	proId=$(this).parent().parent().siblings(".product-id").attr("productId");
				$.ajax({
					type: 'post',
					url: ctx + "/experiment/cell/passage/cellPassage/getTemplateId.action",
					data:{
						id: $("#cellPassage_id").text(),
						proId:proId,	
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
						var tpId=data.tpId;
						console.log($("[sopid='"+tpId+"']"))
						var length = $(".selected").length;
						$("[sopid='"+tpId+"']").addClass("sopChosed").siblings("li").removeClass("sopChosed");
						$(".label-warning").removeClass("selectednum").html("");
						$("[sopid='"+tpId+"']").children(".label-warning").addClass("selectednum").text(length + parseInt(selectednum));
						if($("[sopid='"+tpId+"']").children(".label-primary").text()) {
							if($("#maxNum").val() == "0") {
								$("#maxNum").val("1");
							}
						} else {
							$("#maxNum").val("0");
						}
						//显示隐藏模板里配置的实验员
						var userGroup=$("[sopid='"+tpId+"']").attr("suerGroup");
						$(".userLi").each(function (i,v) {
							var userGroupid=v.getAttribute("suerGroup");
							if (userGroup==userGroupid) {
								$(v).slideDown();
							}else{
								$(v).slideUp().children("img").removeClass("userChosed");
							}
						});
					
						}
					}
				});
			} else {
				index--;
			}
			$(".selectednum").text(index+parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	//选中模板的实验组的实验员显示隐藏
	if($(".sopChosed").length){
		var userGroup=$(".sopChosed").attr("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).show();
			}else{
				$(v).hide();
			}
		});
	}
	
	$('#yangben').bind('keydown', function (event) {
		var event = window.event || arguments.callee.caller.arguments[0];
		if (event.keyCode == 13){
			setTimeout(function() {
				//扫码回车
				var yangben = $("#yangben").val();
				$.ajax({
					type: "post",
					url:ctx+"/experiment/cell/passage/cellPassageNow/choseSampleCode.action",
					data:{
						yangben:yangben,
					},
					success: function(dataBack) {
						var data = JSON.parse(dataBack);
						if(data.success){
							top.layer.msg("样本扫描成功!")
							$("#yangben").val("");
							//获取复选框,给复选框赋值
							var rows = $("#main tbody tr");
							var length = rows.length;
							console.log(length)
							if(length!=data.list){
								var parm={
										query:'{"batch":"%'+data.data+'%"}'
									}
									var table = $(".dataTables_scrollBody").children("table").attr("id");
									var cellPassageAllotTab = $('#' + table).DataTable();
									console.log(cellPassageAllotTab)
									cellPassageAllotTab.settings()[0].ajax.data = parm
									cellPassageAllotTab.ajax.reload();
									
									cellPassageAllotTab.on('draw', function() {
										$("#main tbody tr").each(function(i,v){
											var code = $(v).find("td[savename='code']").text();
											if(code==data.code){
												$(v).find("td[savename='code']").parent().find('.icheckbox_square-blue').addClass("checked")
												$(v).find("td[savename='code']").parent().addClass("selected")
											}
										})
									})
									
									
							}else{
								debugger
								$("#main tbody tr").each(function(i,v){
									var code = $(v).find("td[savename='code']").text();
									if(code==data.code){
										$(v).find("td[savename='code']").parent().find('.icheckbox_square-blue').addClass("checked")
										$(v).find("td[savename='code']").parent().addClass("selected")
									}
									
									var	proId=$("#main tbody tr").find("td[savename='code']").siblings(".product-id").attr("productId");
									console.log(proId)
									$.ajax({
										type: 'post',
										url: ctx + "/experiment/cell/passage/cellPassage/getTemplateId.action",
										data:{
											id: $("#cellPassage_id").text(),
											proId:proId,	
										},
										success: function(data) {
											var data = JSON.parse(data);
											if(data.success) {
											var tpId=data.tpId;
											console.log($("[sopid='"+tpId+"']"))
											var length = $(".selected").length;
											$("[sopid='"+tpId+"']").addClass("sopChosed").siblings("li").removeClass("sopChosed");
											$(".label-warning").removeClass("selectednum").html("");
											$("[sopid='"+tpId+"']").children(".label-warning").addClass("selectednum").text(length + parseInt(selectednum));
											if($("[sopid='"+tpId+"']").children(".label-primary").text()) {
												if($("#maxNum").val() == "0") {
													$("#maxNum").val("1");
												}
											} else {
												$("#maxNum").val("0");
											}
											}
										}
									});
									
								})
							}
							
						}else{
							top.layer.msg("样本扫描失败!")
						}
					}
				});
			},500);
		}
	})
	
	
	//sop和实验员的点击操作
	sopAndUserHandler(selectednum);
	//保存操作
	allotSave();
});


//质检下载csv
function downCsv(){
//	//window.location.href=ctx+"/js/experiment/enmonitor/dust/cleanAreaDustItem.csv";
//	var mainTableId=$("#sampleReveice_id").text();
//	if(mainTableId!=""&&mainTableId!='NEW'){
var arr = [];
	
	var rows=$("#main .selected");
	if(rows.length<=0){
		top.layer.msg("请选择一条数据！");
		return false;
	}
	var flag=true
	
	$(".selected").each(function(i, val) {
		arr.push($(val).find("td[savename='batch']").text());
	});
	
    var arrLength = arr.length;
	//去重
	 for(var i=0; i<arr.length; i++){
         for(var j=i+1; j<arr.length; j++){
             if(arr[i]==arr[j]){         //第一个等同于第二个，splice方法删除第二个
                 arr.splice(j,1);
                 j--;
             }
         }
     }
	 if(arr.length>1){
		 top.layer.msg("请选择同一产品批号的数据保存！")
		 flag=false;
	 } 
	 
	
	 if(flag){
			var sampleId =arr[0];   
			window.location.href=ctx+"/experiment/cell/passage/cellPassage/downloadCsvFileTwo.action?sampleId="+sampleId;
	 }
	
//		var sampleId= $("#sampleReceive_sampleOrder").val();
//	}else{
//		top.layer.msg("请保存后再下载模板！");
//	}
}
//生产下载csv
function downCsvProduct(){
	var arr = [];

	var rows=$("#main .selected");
	if(rows.length<=0){
		top.layer.msg("请选择一条数据！");
		return false;
	}
   var flag=true
	
	$(".selected").each(function(i, val) {
		arr.push($(val).find("td[savename='batch']").text());
	});
	
    var arrLength = arr.length;
	//去重
	 for(var i=0; i<arr.length; i++){
         for(var j=i+1; j<arr.length; j++){
             if(arr[i]==arr[j]){         //第一个等同于第二个，splice方法删除第二个
                 arr.splice(j,1);
                 j--;
             }
         }
     }
	 if(arr.length>1){
		 top.layer.msg("请选择同一产品批号的数据保存！")
		 flag=false;
	 } 
	
	 if(flag){
		var sampleId =arr[0];
		window.location.href=ctx+"/experiment/cell/passage/cellPassage/downloadCsvFile.action?sampleId="+sampleId;
	 }
}



//sop和实验员的点击操作
function sopAndUserHandler(selectednum) {
	console.log
	//执行sop点击选择操作
	$(".sopLi").click(function() {
		var length = $(".selected").length;
		$(this).addClass("sopChosed").siblings("li").removeClass("sopChosed");
		$(".label-warning").removeClass("selectednum").html("");
		$(this).children(".label-warning").addClass("selectednum").text(length+parseInt(selectednum));
		if($(this).children(".label-primary").text()) {
			if($("#maxNum").val() == "0") {
				$("#maxNum").val("1");
			}
		} else {
			$("#maxNum").val("0");
		}
		//显示隐藏模板里配置的实验员
		var userGroup=this.getAttribute("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).slideDown();
			}else{
				$(v).slideUp().children("img").removeClass("userChosed");
			}
		});
	});
	//执行实验员点击选择操作
	$(".userLi").click(function() {
		var length = $(".selected").length;
			var img = $(this).children("img");
			if(img.hasClass("userChosed")) {
				img.removeClass("userChosed");
			} else {
				img.addClass("userChosed");
			}		
	});
}
//保存操作
function allotSave() {
	$("#save").click(function() {
		
		
		
		
		var flag =true;

		var changeLog = "";
//		if(!$(".sopChosed").length) {
//			top.layer.msg(biolims.common.selectTaskModel);
//			return false;
//		}
//		if(!$(".userChosed").length) {
//			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
//			return false;
//		}
		//获得选中样本的ID
		var sampleId = [];
		var arr = [];
		$(".selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
			arr.push($(val).find("td[savename='batch']").text());
		});
		
	    var arrLength = arr.length;
		//去重
		 for(var i=0; i<arr.length; i++){
	         for(var j=i+1; j<arr.length; j++){
	             if(arr[i]==arr[j]){         //第一个等同于第二个，splice方法删除第二个
	                 arr.splice(j,1);
	                 j--;
	             }
	         }
	     }
		 if(arr.length>1){
			 top.layer.msg("请选择同一产品批号的数据保存！")
			 flag=false;
		 } 
	
		
		 if(flag){	
			 if(arrLength!=0){
		
			 $.ajax({
					type : 'post',
					url:ctx+"/experiment/cell/passage/cellPassage/findBachIsDifference.action",
					data:{
						batch:arr[0],
						length:arrLength
					},
					success:function(data){
						var data = JSON.parse(data);
						if(data.success){
							//获得选中实验员的ID
							var userId = [];
							$(".userChosed").each(function(i, val) {
								userId.push($(val).parent("li").attr("userid"));
							});
							changeLog = getChangeLog(userId, changeLog);
							if(changeLog !=""){
								changeLogs=changeLog;
							}
							
							//拼接主表的信息
							var main = {
								id: $("#cellPassage_id").text(),
								name: $("#cellPassage_name").val(),
								"createUser-id": $("#cellPassage_createUser").attr("userId"),
								createDate: $("#cellPassage_createDate").text(),
								state: $("#cellPassage_state").attr("state"),
								stateName: $("#cellPassage_state").text(),
								sampleNum:$(".selectednum").text(),
								maxNum:$("#maxNum").val()
							};
							var data = JSON.stringify(main);
							//拼接保存的data
							var info = {
								temp: sampleId,
								template: $(".sopChosed").attr("sopid"),
								user: userId.join(","),
								main: data,
								logInfo: changeLogs,
								place:$("#place").val(),
								infection:$("#infection").val()
							}
							top.layer.load(4, {shade:0.3}); 
							$.ajax({
							type : 'post',
							url:ctx+"/experiment/cell/passage/cellPassageNow/saveAllotNow.action",
							data:info,
							success:function(data){
								var data = JSON.parse(data);
								if(data.success){	
									top.layer.closeAll();
									top.layer.msg(biolims.common.saveSuccess);
									window.location = window.ctx
								+ '/experiment/cell/passage/cellPassage/editCellPassage.action?id=' + data.id;
								}
							}
							})
							
						}else{
							top.layer.msg("请选择所有相同产品批号的数据！")
						}
						
					}
				})	
				
				 
			 }else{
				 top.layer.msg("请选择数据再保存！")
			 }
			 
			 
	
		 }
	});
}
//下一步操作
$("#next").click(function () {
	var cellPassage_id=$("#cellPassage_id").text();
	if(cellPassage_id=="NEW"){
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	//原来的
	//$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/cell/passage/cellPassage/showCellPassageItemTable.action?id="+cellPassage_id+"&bpmTaskId="+$("#bpmTaskId").val();
	//现在的生产
	$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/cell/passage/cellPassageNow/showCellPassageEdit.action?id="+cellPassage_id+"&bpmTaskId="+$("#bpmTaskId").val()+"&orderNum=1";

});

/***
 * 
 * 样本退回到生产异常
 * @returns
 */
function backSample(){
	
	var rows = $("#main .selected");
	if(!rows.length){
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	
	var flag =true;
	var sampleId = [];
	var arr = [];
	$(".selected").each(function(i, val) {
		sampleId.push($(val).children("td").eq(0).find("input").val());
		arr.push($(val).find("td[savename='batch']").text());
	});
    var arrLength = arr.length;
	//去重
	 for(var i=0; i<arr.length; i++){
         for(var j=i+1; j<arr.length; j++){
             if(arr[i]==arr[j]){         //第一个等同于第二个，splice方法删除第二个
                 arr.splice(j,1);
                 j--;
             }
         }
     }
	 if(arr.length>1){
		 top.layer.msg("请选择同一产品批号的数据保存！")
		 flag=false;
	 }
	var batch=[];
	$(".selected").each(function(i,v){
		batch.push($(v).find("td[savename='batch']").text());
	})
	
	if(flag){
		if(arrLength!=0){
			$.ajax({
				type : 'post',
				url:ctx+"/experiment/cell/passage/cellPassage/findBachIsDifference.action",
				data:{
					batch:arr[0],
					length:arrLength
				},
				success:function(data){
					var data = JSON.parse(data);
					if(data.success){
						$.ajax({
							type : 'post',
							url:ctx+"/experiment/cell/passage/cellPassageNow/backSample.action",
							data:{
								batch:batch
							},
							success:function(data){
								var data = JSON.parse(data);
								if(data.success){
									top.layer.msg("样本退回成功!")
									cellPassageAllotTab.ajax.reload();
								}else{
									top.layer.msg("样本退回失败!")
								}
							}
						})
					}else{
						top.layer.msg("请选择所有相同产品批号的数据！")
					}
					
				}
			})	
			
		}
	}
}

//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : "批号",
		"type" : "input",
		"searchName" : "batch",
	},{
		"txt" : "姓名",
		"type" : "input",
		"searchName" : "patientName",
	}
//	,{
//		"txt" : biolims.common.code,
//		"type" : "input",
//		"searchName" : "code",
//	}, {
//		"txt":biolims.common.projectId,
//		"type": "input",
//		"searchName": "sampleInfo-project-id",
//	}, {
//		"txt":biolims.common.orderCode,
//		"type": "input",
//		"searchName": "orderCode",
//	}, {
//		"txt":biolims.common.sampleType,
//		"type": "input",
//		"searchName": "sampleType",
//	}, {
//		"txt":biolims.common.productName,
//		"type": "input",
//		"searchName": "productName",
//	}
	,  {
		"type" : "table",
		"table" : cellPassageAllotTab
	} ];
}
//核对编码
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layero) {
			var arr = $("#checkCode",parent.document).val().split("\n");
			cellPassageAllotTab.settings()[0].ajax.data = {"codes":arr};
			cellPassageAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});

}
//获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { //获取待处理样本的修改日志
	var sampleNew = [];
	$(".selected").each(function(i, val) {
		sampleNew.push($(val).children("td").eq(1).text());
	});
	changeLog += '"待处理样本"新增有：' + sampleNew + ";";
	//获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	//获取模板sop的修改日志
	if($(".sopChosed").attr("sopid") != $("#sopChangelogId").val()) {
		changeLog += '实验模板:由"' + $("#sopChangelogName").val() + '"变为"' + $(".sopChosed").children('.text').text() + '";';
		changeLog += '样本数量:由"' + $("#sopChangelogSampleNum").val() + '"变为"' + $(".sopChosed").children('.badge').text() + '";';
	}
	//获取实验员的修改日志
	var oldUserId = [],
		userName = [],
		oldUserName = [];
	$('input[class="changelogUserid"]').each(function(i, v) {
		oldUserId.push(v.value);
	});
	if(userId != oldUserId) {
		$(".userChosed").each(function(i, val) {
			userName.push($(val).next("a").text());
		});
		$('input[class="changelogUsername"]').each(function(i, v) {
			oldUserName.push(v.value);
		});
		changeLog += '实验员:由"' + oldUserName + '"变为"' + userName + '";';
	}

	return changeLog;
}
function addItemProdution(ele,code){
	$.ajax({
        type: "post",
        url: "/experiment/cell/passage/cellPassageNow/choseSampleCode.action",
        data: {
        	yangben: code,
        },
        async: false,
        success: function (databack) {
        	debugger
            var data = JSON.parse(databack);
            productId = data.productId;
            productName = data.productName;
            batch = data.batch;
            code = data.code;
            patientName = data.patientName;
            sampleType = data.sampleType;
        },
    });
	
	$(".dataTables_scrollHead th").off();
	//禁用固定列
	$("#fixdeLeft2").hide();
	//禁用列显示隐藏
	$(".colvis").hide();
	//清除没有内容选项
	ele.find(".dataTables_empty").parent("tr").remove();
	//添加明细
	var ths = ele.find("th");
	var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
	tr.height(32);
	for(var i = 1; i < ths.length; i++) {
		var edit = $(ths[i]).attr("key");
		var saveName = $(ths[i]).attr("saveName");
		if(edit == "select") {
			var selectOpt = $(ths[i]).attr("selectopt");
			tr.append("<td class=" + edit + " saveName=" + saveName + " selectopt=" + selectOpt + "></td>");
			tr.find("td[savename='productName']").attr("productId", productId).text(productName);
		    tr.find("td[savename='productId']").text(productId);
		    tr.find("td[savename='code']").text(code);
		    tr.find("td[savename='sampleType']").text(sampleType);
            tr.find("td[savename='patientName']").text(patientName);
            tr.find("td[savename='batch']").text(batch);
            var indexa = [];
            $("#main tbody tr").each(function (i, o, v) {
                indexa.push($(this).find("td[savename='index']").text());
            })
            if (indexa == null || indexa == "" || indexa.length < 1) {
                tr.find("td[savename='index']").text("1");
            } else {
                indexa.sort(function (a, b) {
                    return a - b;
                });
                var maxindex = indexa[indexa.length - 1];
                tr.find("td[savename='index']").text(parseInt(maxindex) + 1);
            }
		} else {
			tr.append("<td class=" + edit + " saveName=" + saveName + "></td>");
			tr.find("td[savename='productName']").attr("productId", productId).text(productName);
		    tr.find("td[savename='productId']").text(productId);
		    tr.find("td[savename='code']").text(code);
		    tr.find("td[savename='sampleType']").text(sampleType);
            tr.find("td[savename='patientName']").text(patientName);
            tr.find("td[savename='batch']").text(batch);
            var indexa = [];
            $("#main tbody tr").each(function (i, o, v) {
                indexa.push($(this).find("td[savename='index']").text());
            })
            if (indexa == null || indexa == "" || indexa.length < 1) {
                tr.find("td[savename='index']").text("1");
            } else {
                indexa.sort(function (a, b) {
                    return a - b;
                });
                var maxindex = indexa[indexa.length - 1];
              	tr.find("td[savename='index']").text(parseInt(maxindex) + 1);
            }
		}

	}
	ele.find("tbody").prepend(tr);
	checkall(ele);
}