/* 
 * 文件名称 :cellPassageSteps.js
 * 创建者 : 
 * 创建日期: 
 * 文件描述: 
 * 
 */
var cellPassage_id = $("#cellPassage_id").text();
var isSeparate = $("#cellPassage_isSeparate").val();
var changelogRea, changelogCos;
var oldChangeLog;
var flg = false;
var sUserAgent = navigator.userAgent.toLowerCase(); 
var isIpad = sUserAgent.match(/ipad/i) == "ipad";
var ord;
if (isSeparate == 1) {
	flg = true;
}

// 能否保存
var baocun = true;
// 是不是操作员
var caozuo = true;
// 是不是组长和管理员
var adminUser = true;

$(function() {
$('.box-body').css({
			"position": "relative"
		});
	if(isIpad){
		
		
		
		$('.ipddd').css({
			"height":"700px",
			"overflow-y":"auto"
		});
		
	   
	}
	
	// User user = (User)
	// this.getObjFromSession(SystemConstants.USER_SESSION_KEY);
	// CellProductionRecord cprs = "";
	// 是否有保存 实验步骤是否完成 完成不能保存
	// 是否能操作其他步骤 当前登录人是操作员只能看一步 管理员和组长能看其他步骤（不能操作）
	// 当前登录人是否是操作员 是操作员有权限 不是操作员 是不是管理员 是不是组长（创建人）

	var dijibu1 = sessionStorage.getItem("dijibu");
	if (dijibu1) {
		var orderNums1 = dijibu1;
	} else {
		var orderNums1 = $("#orderNumBy").val();
	}
	$.ajax({
		type : 'post',
		url : ctx + '/experiment/cell/passage/cellPassage/serchInfos.action',
		async : false,
		data : {
			mainId : cellPassage_id,
			orderNums1 : orderNums1
		},
		success : function(data) {
		
			var data = JSON.parse(data);
			if (data.sueccess) {
				// 能否保存
				baocun = data.baocun;
				// 是不是操作员
				caozuo = data.caozuo;
				// 是不是组长和管理员
				adminUser = data.adminUser;
			} else {
				// 查询报错 没想好要干嘛
			}
		}
	});
	// 箭头点击
	arrowClick();
	// 请求步骤的数据
	renderSteps();
	// 上一步下一步点击
	preAndNext();
	bpmTask($("#bpmTaskId").val());
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view"
			|| $("#cellPassage_state").text() == biolims.common.finish) {
		settextreadonly();
		$("#save").hide()
		$("#tjsp").hide()
	}
	// 判断如果是列表页进来的，直接跳到当前步骤
	renderOrderNums();
	
	// $('#oo').click(function(){
	// 	$('#tt').click()
	// })
	// $('#qqq').click(function(){
	// 	$('#qq').click()
	// })
	// $('#www').click(function(){
	// 	$('#ww').click()
	// })
	
	$('.bootstrap-switch-label').css('width',0)
	
	
	
	
});


//下载csv
function downCsv(){
	//window.location.href=ctx+"/js/experiment/enmonitor/dust/cleanAreaDustItem.csv";
	var mainTableId=$("#sampleReceive_sampleOrder").val();
	if(mainTableId!=""&&mainTableId!='NEW'){
		window.location.href=ctx+"/system/sample/sampleOrder/downloadCsvFile.action?id="+mainTableId;
	}else{
		top.layer.msg("请保存后再下载模板！");
	}
}
// 渲染生产模板明细中得内容(开始时间,结束时间)
function renderCellPassageTemplateItem(stepNum) {
	$.ajax({
		type : "post",
		data : {
			orderNum : stepNum,
			id : cellPassage_id
		},
		async : false,
		url : ctx
				+ "/experiment/cell/passage/cellPassage/showCellPassageStepsJson.action",
		success : function(data) {
//			var data = JSON.parse(data);
//			var list = data.template;
//            
//			$("#startTime").html(list[0].startTime).attr("changelog",
//					list[0].startTime);
//			$("#endTime").html(list[0].endTime).attr("changelog",
//					list[0].endTime);
			var data = JSON.parse(data);
			var startTime =data.startTime;
			var endTime=data.endTime;
			$("#startTime").html(startTime).attr("changelog",
					startTime);
			$("#endTime").html(endTime).attr("changelog",
					endTime);
		}
	});
}
// 请求步骤数据
function renderSteps() {
	$.ajax({
				type : "post",
				data : {
					id : cellPassage_id
				},
				async : false,
				url : ctx
						+ "/experiment/cell/passage/cellPassageNow/findTempleItemNumber.action",
				success : function(data) {
					var list = JSON.parse(data).data;
//					var templateItemList = JSON.parse(data).list;
					// 生成步骤
					var stepsLi = "";
					list.forEach(function(v, i) {
						var index = i + 1;
						stepsLi += "<li><a class='disabled step' stepid="
								+ v.id + " code=" + v.orderNum
								+ "><span class='step_no'>"
								+ /* index */v.orderNum
								+ "</span><p class='step_descr'> " + v.name
								+ "</p><p class='estimatedDate'>预计:" + v.today
								+ "天</p><p class='estimatedDateTime'>"
								+ v.startTime + "</p></a></li>";
					});
					$(".wizard_steps").append(stepsLi);
					renderCellPassageTemplateItem(1);
					// 为每个步骤注册点击事件
					$(".wizard_steps .step")
							.click(
									function() {
										
										$(".wizard_steps .step").removeClass(
												"selected")
												.addClass("disabled");
										$(this).removeClass("disabled")
												.addClass("selected");
										// 变步骤名称
										$("#steptiele").text(
												$(this).children(".step_descr")
														.text());
										// 变步骤详情（HTML代码）
										$("#stepcontent").html("xxx");
										$(".bwizard-steps>li:eq(0)>a").click();
										renderCellPassageTemplateItem($(this)
												.children(".step_no").text());
										ord=$(this).attr("code");
										$.ajax({
											type:"post",
											url : ctx
											+ '/experiment/cell/passage/cellPassage/chooseCellPassageSteps.action',
											data:{
												id:cellPassage_id,
												orderNum:$(this).attr("code"),
											},
											success:function(data){
												
											}
										})
									});
				//	$(".wizard_steps .step:eq(0)").click();
					// 为步骤明细的HTML设置动画
					$("#stepContentBtn").click(function() {
						if ($(this).hasClass("xxx")) {
							$(this).removeClass("xxx");
							$("#stepContentModer").animate({
								"height" : "10%"
							}, 800, "swing", function() {
								$(this).animate({
									"width" : "0%",
									"height" : "0%",
								}, 600, "linear");
							});
						} else {
							$(this).addClass("xxx");
							$("#stepContentModer").animate({
								"width" : "100%",
								"height" : "10%"
							}, 500, "swing", function() {
								$(this).animate({
									"height" : "90%"
								}, 600, "linear");
							});
						}
					});
				}
			});

}

var stepIdOld=0;
var stepIdOld1;

// 箭头点击
function arrowClick() {
	$(".bwizard-steps>li")
			.click(
					function() {
						top.layer.load(4, {
							shade: 0.3
						});
						//判断下该步骤是否可以保存，可以保存的，自动保存
						// 判断是否隐藏
						if($("#save").is(":hidden")){
							
						}else{
								saveStepItemZidong();
						}
						var step1State="";
						var step2State="";
						var step3State="";
						var step4State="";
						var stepidVal=$("#wizard_verticle ul.wizard_steps").find("a.selected").attr("stepid");
						
						$.ajax({
							url:ctx+"/experiment/cell/passage/cellPassageNow/selStepState.action",
							type:"post",
							async:false,
							data:{
								id:stepidVal,
							},
							success:function(data){
								var data=JSON.parse(data);
								if(data.success){
									if(data.val.step1State !="1"){
										step1State="0";
									}else{
										step1State=data.val.step1State;
									}
									if(data.val.step2State !="1"){
										step2State="0";
									}else{
										step2State=data.val.step2State;
									}
									if(data.val.step3State !="1"){
										step3State="0";
									}else{
										step3State=data.val.step3State;
									}
									if(data.val.step4State !="1"){
										step4State="0";
									}else{
										step4State=data.val.step4State;
									}
								}
							}
							
						});
						var stepId = $(this).attr("note");
						var orderid = $(".wizard_steps .selected").attr("stepid");
						var orderNum = $(".wizard_steps .selected .step_no").text();
						
						var benbu=$("ul.bwizard-steps").find("li.active").attr("note");
						var flags="";
						if(benbu=="1"){
							if(step1State=="1"){
								if(stepId=="1"){
									flags="true";
								}else if(stepId=="2"){
									if(step3State=="1"){
										flags="true";
									}else{
										if(step2State=="1"){
											flags="true";
										}else{
											flags="false";
										}
									}
								}else if(stepId=="3"){
									flags="true";
								}else if(stepId=="4"){
									flags="true";
								}
							}else if(step1State==""){
								flags="true";
							}else{
								if(stepId=="1"){
									flags="true";
								}else if(stepId=="2"){
									if(step3State=="1"){
										flags="true";
									}else{
										flags="false";
									}
								}else if(stepId=="3"){
									if(step2State=="1"){
										flags="true";
									}else{
										flags="false";
									}
								}else if(stepId=="4"){
									flags="true";
								}
							}
						}else if(benbu=="2"){
							if(step3State=="1"){
								if(stepId=="1"){
									//if(step1State=="1"){
										
										flags="true";
									//}else{
									//	flags="false";
									//}
								}else if(stepId=="2"){
									flags="true";
								}else if(stepId=="3"){
									if(step1State=="1"){
										flags="true";
									}else{
										flags="flase";
									}
								}else if(stepId=="4"){
									flags="true";
								}
							}else{
								if(stepId=="1"){
									if(step1State=="1"){
										flags="true";
									}else{
										flags="false";
									}
								}else if(stepId=="2"){
									flags="true";
								}else if(stepId=="3"){
									if(step2State=="1"){
										flags="true";
									}else{
										flags="false";
									}
								}else if(stepId=="4"){
									flags="true";
								}
							}
						}else if(benbu=="3"){
							if(step2State=="1"){
								if(stepId=="1"){
									flags="true";
								}else if(stepId=="2"){
									flags="true";
								}else if(stepId=="3"){
									if(step1State=="1"){
										flags="true";
									}else{
										flags="false";
									}
									
								}else if(stepId=="4"){
									flags="true";
								}
							}else{
								if(stepId=="1"){
									if(step1State=="1"){
										flags="true";
									}else{
										flags="false";
									}
								}else if(stepId=="2"){
									if(step3State=="1"){
										flags="true";
									}else{
										flags="false";
									}
								}else if(stepId=="3"){
									flags="true";
								}else if(stepId=="4"){
									flags="true";
								}
							}
						}else if(benbu=="4"){
							if(step4State=="1"){
								if(stepId=="1"){
									flags="true";
								}else if(stepId=="2"){
									if(step3State=="1"){
										flags="true";
									}else{
										if(step1State=="1"&&step2State=="1"){
											flags="true";
										}else{
											flags="false";
										}
										
									}
								}else if(stepId=="3"){
									if(step2State=="1"){
										flags="true";
									}else{
										if(step1State=="1"){
											flags="true";
										}else{
											flags="false";
										}
										
									}
								}else if(stepId=="4"){
									flags="true";
								}
							}else{
								if(stepId=="1"){
									flags="true";
								}else if(stepId=="2"){
									if(step3State=="1"){
										flags="true";
									}else{
										if(step1State=="1"&&step2State=="1"){
											flags="true";
										}else{
											flags="false";
										}
									}
								}else if(stepId=="3"){
									if(step2State=="1"){
										flags="true";
									}else{
										if(step1State=="1"){
											flags="true";
										}else{
											flags="false";
										}
									}
								}else if(stepId=="4"){
									flags="true";
								}
							}
						}

						if(flags=="false"){
							top.layer.closeAll();
							top.layer.msg("不能跳转！");
							return false;
						}
						
						$.ajax({
									type : 'post',
									url : ctx
											+ '/experiment/cell/passage/cellPassage/serchInfos.action',
									async : false,
									data : {
										mainId : cellPassage_id,
										orderNums1 : orderNum
									},
									success : function(data) {
										var data = JSON.parse(data);
										if (data.sueccess) {
											if (data.baocun) {
												baocun = true;
											} else {
												baocun = false;
											}
											if (data.caozuo) {
												caozuo = true;
											} else {
												caozuo = false;
											}
											if (data.adminUser) {
												adminUser = true;
											} else {
												adminUser = false;
											}
											// //能否保存
											// baocun = data.baocun;
											// //是不是操作员
											// caozuo = data.caozuo;
											// //是不是组长和管理员
											// adminUser = data.adminUser;

											if (baocun) {
												if (caozuo) {
													$("#save").show();
												} else {
													$("#save").hide();
												}
											} else {
												$("#save").hide();
											}
										} else {
											// 查询报错 没想好要干嘛
											top.layer.closeAll();
										}
									}
								});

						if (stepId == "1") { // 渲染工前准备
							if (baocun) {
								if (caozuo) {
									$("#qrsbfj").show();
									$("#ksgqzb").show();
								} else {
									$("#qrsbfj").hide();
									$("#ksgqzb").hide();
								}
							} else {
								$("#qrsbfj").hide();
								$("#ksgqzb").hide();
							}
							var action = "/experiment/cell/passage/cellPassageNow/findNumberNstructionsListinfo.action";
						} else if (stepId == "2") { // 渲染质检
//							var requiredField = templateRequiredFilter();		
//							if (!requiredField) {
//								top.layer.closeAll();
//								return false;
//							}
//							
//							var fuyong5=true;
//							var mssg5;
//							if($("#thirdTab").find("#productStartTime").text()==""){
//								
//								mssg5="生产开始时间未选择";
//								top.layer.closeAll();
//								top.layer.msg(mssg5);
//								return false;
//							}
//							if($("#thirdTab").find("#productEndTime").text()==""){
//							
//								mssg5="生产结束时间未选择";
//								top.layer.closeAll();
//								top.layer.msg(mssg5);
//								return false;
//							}
//							
//							$("#thirdTab .caozuoItem").find(".time").find("button").each(function(i,v){
//								if($(v).children().eq(1).text()==""){
//									fuyong5 = false;
//									mssg5="生产开始或结束时间未选择";
//									return false;
//								}
//							});
//							if(fuyong5 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg5);
//								return false;
//							}
//							$(".timeline-footer #reagentBody .reagli").each(function(i,v){
//								if($(v).find("input").eq(0).val()==""){
//									
//									fuyong5 = false;
//									mssg5=$(v).find("input").eq(0).prev().text()+"未填写";
//									return false;
//								}
//							});
//							if(fuyong5 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg5);
//								return false;
//							}
//							
//							$("#thirdTab .timeline-item").find("#cosBody .cosli").each(function(i,v){
//								if($(v).find(".label").text()==""){
//									
//									fuyong5 = false;
//									mssg5=$(v).find(".label").prev().text()+"未选择";
//									return false;
//								}
//							});
//							if(fuyong5 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg5);
//								return false;
//							}
//							$("#thirdTab .timeline-item").find("#zhijianBodyNew .input-group").each(function(i,v){
//								if($(v).find("input").val()==""){
//									
//									fuyong5 = false;
//									mssg5=$(v).siblings("small").find(".zhijianName").text()+$(v).find("input").prev().text()+"未填写";
//									return false;
//								}
//							});
//							if(fuyong5 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg5);
//								return false;
//							}
							var action = "/experiment/cell/passage/cellPassageNow/findZhijianItemInfo.action";
						} else if (stepId == "3") { // 渲染BL操作
//							var requiredField = templateRequiredFilter();		
//							if (!requiredField) {
//								top.layer.closeAll();
//								return false;
//							}
//							// 判断是否选择设备
//							var arr = [];
//							var fuyong = true;
//							// 判断是否选择试剂
//							var arr1 = []
//							var fuyong1 = true;
//
//							$("#pageOneChoseYQ .btn-info").each(
//									function(i, v) {
//										var jsonn = {};
//										jsonn.id = v.getAttribute("cosid");
//										jsonn.code = v.getAttribute("code");
//										jsonn.name = v.getAttribute("name");
//
//										if (jsonn.name != null
//												&& jsonn.name != "null"
//												&& jsonn.name != "") {
//										} else {
//											fuyong = false;
//											return false;
//										}
//										arr.push(jsonn);
//									});
//							
//							if (fuyong == false) {
//								top.layer.closeAll();
//								top.layer.msg("请选择设备");
//								return false;
//							}
//							
//							$("#pageOneChoseSJ .btn-info").each(function(i, v) {
//										var jsonn = {};
//										jsonn.ph = v.getAttribute("ph");
//										jsonn.yxq = v.getAttribute("yxq");
//
//										if (jsonn.ph != null
//												&& jsonn.ph != "null"
//												&& jsonn.ph != "") {
//										} else {
//											fuyong1 = false;
//											return false;
//										}
//										arr1.push(jsonn);
//							});
//							
//							if (fuyong1 == false) {
//								top.layer.closeAll();
//								top.layer.msg("请选择试剂");
//								return false;
//							}
//							// 判断是否选择
//							if ($("#operatingRoomName").val() != null
//									&& $("#operatingRoomName").val() != "") {
//							} else {
//								top.layer.closeAll();
//								top.layer.msg("请选择房间");
//								return false;
//							}
//							
//							var fuyong2=true;
//							var mssg2;
//							$(".pageOneRoomConfirm").each(function(i,v){
//								if($(v).attr("state")!="1"){
//									
//									mssg2="房间确认的第"+(i+1)+"个按钮未确认";
//									fuyong2=false;
//									return false;
//								}
//							});
//							if(fuyong2 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg2);
//								return false;
//							}
//							
//							var fuyong3=true;
//							var mssg3;
//							$(".pageOneQRcodesample").each(function(i,v){
//								if($(v).attr("state")!="已确认"){
//									
//									mssg3="样本条码确认的第"+(i+1)+"个核对结果未确认";
//									fuyong3=false;
//									return false;
//								}
//							});
//							if(fuyong3 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg3);
//								return false;
//							}
//							
//							var fuyong4=true;
//							var mssg4;
//							$(".pageOneQRcodeoperation").each(function(i,v){
//								if($(v).attr("state")!="已确认"){
//									
//									mssg4="操作条码确认的第"+(i+1)+"个核对结果未确认";
//									fuyong4=false;
//									return false;
//								}
//							});
//							if(fuyong4 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg4);
//								return false;
//							}
//							$("#pageOneChoseYQ button").each(function(i,v){
//								if($(v).attr("operationstate")!="1"){
//									
//									mssg4="第"+(i+1)+"个选定的设备未确认";
//									fuyong4=false;
//									return false;
//								}
//							});
//							if(fuyong4 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg4);
//								return false;
//							}
//							$(".pageOneOperationalOrder").each(function(i,v){
//								if($(v).attr("state")!="1"){
//									
//									mssg4="操作指令的第"+(i+1)+"个按钮未确认";
//									fuyong4=false;
//									return false;
//								}
//							});
//							if(fuyong4 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg4);
//								return false;
//							}
//							
//							//判断生产记录是否选择完毕
//							$("#firstTab").find("#pageOneProductionRecord .form-group").each(function(i,v){
//								$(v).find("input").each(function(i,vv){
//									if($(vv).val()!=""){
//										
//									}else{
//										mssg4=$(vv).siblings("label").text()+"不能为空";
//										fuyong4=false;
//										console.log($(vv).siblings("label").text());
//										return false;
//									}
//								});
//								if(fuyong4 == false){
//									return false;
//								}
//							});
//							if(fuyong4 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg4);
//								return false;
//							}
							var action = "/experiment/cell/passage/cellPassageNow/findCosAndReagentInfo.action";
						} else if (stepId == "4") { // 渲染完工清场
//							var requiredField = templateRequiredFilter();		
//							if (!requiredField) {
//								top.layer.closeAll();
//								return false;
//							}
//							var fuyong6=true;
//							var mssg6;
							/*$("#blendTable tbody tr").find("td[savename='posId']").each(function(i,v){
								if($(v).text()==""){
									
									mssg6="样本信息表的第"+(i+1)+"条数据的 储位 不能为空";
									fuyong6=false;
									return false;
								}
							});
							if(fuyong6 == false){
								top.layer.closeAll();
								top.layer.msg(mssg6);
								return false;
							}*/
//							$("#zhijianDetails tbody tr").find("td[savename='submit']").each(function(i,v){
//								if($(v).text()!="是"){
//									
//									mssg6="质检明细表的第"+(i+1)+"条数据未提交";
//									fuyong6=false;
//									return false;
//								}
//							});
//							if(fuyong6 == false){
//								top.layer.closeAll();
//								top.layer.msg(mssg6);
//								return false;
//							}
							var action = "/experiment/cell/passage/cellPassageNow/findNstructionsEndListinfo.action";
						}
						
						
						$.ajax({
									type : "post",
									url : action,
									async : false,
									data : {
										id : cellPassage_id,
										orderNum : orderNum,
										stepId : stepId,
										orderid : orderid
									},
									success : function(res) {
										var data = JSON.parse(res);
										if (stepId == "1") {
											// 渲染操作指令
											renderOperationalOrder(
													data.cellPass, stepId);
											//渲染房间确认记录
											renderRoomconfirm(data.cellRoomPass,data.room1state,data.room2state
													,data.room3state,data.room4state,data.room5state,data.room6state);
											//渲染当前样本和生成物条码
											renderBrcodeconfirm(data.checkCodeBefore, data.checkCodeAfter);
											
											console.log(data)
											// // 渲染生产记录
											// renderProductionRecord(data.cellReco.templeItem,
											// data.cellReco, stepId);
											// 渲染生产记录
//											renderProductionRecordNew(
//													data.cellReco.templeItem,
//													data.cellReco, stepId,
//													data.scjz);
											renderProductionRecordNew(
													data.templeContent,
													data.cellRecoId,data.cellRecoContent,stepId,
													data.scjz);
											// 渲染选择设备
											$.ajax({
														type : "post",
														data : {
															id : cellPassage_id,
															stepNum : orderNum,
														},
														url : "/experiment/cell/passage/cellPassage/findProductCos.action",
														success : function(res) {
															var data = JSON
																	.parse(res).CellproductCoss;
															var trs = '';
															if (data.length) {
																data.forEach(function(
																				v,
																				i) {
																			var name = v.instrumentName ? "选择设备:"
																					+ (v.instrumentName == null
																							|| v.instrumentName == undefined
																							|| v.instrumentName == "null" ? ""
																							: v.instrumentName)
																					: "选择设备";
																			var checked = v.operationState == "1" ? 'checked'
																							: '';
																			trs += '<tr><td>'
																				+ v.producingTitle
																					+ '</td><td>'
																					+ v.typeName
																					+ '</td><td>'
																					+ (v.instrumentCode==null?"空":v.instrumentCode)
																					+ '</td><td><button class="btn btn-xs btn-info" id="'
																					+ v.templateCosId
																					+ '" typeid="'
																					+ v.templateCosTypeId
																					+ '" cosid="'
																					+ v.id
																					+ '" code="'
																					+ v.instrumentCode
																					+ '" name="'
																					+ v.instrumentName
																					+ '" operationState="'
																					+ v.operationState
																					+ '" onclick="addCos1(this)">'
																					+ name
																					+ '</button></td><td><input type="checkbox" class="aba mySwitch1"  '
																					+ checked
																					+ '/></td></td></tr>';
																		})
																$("#pageOneChoseYQ").html(trs);
																setTimeout(function(){
																	$("#pageOneChoseYQ .mySwitch1").each(function() {
																		var state = $(this).is(':checked');
																		$(this).bootstrapSwitch({
																			onText : "是",
																			offText : "否",
																			onSwitchChange : function(event, state) {
																				if (state == true) {
																					$(this).parents("tr").find(".btn-info").attr("operationState", "1");
																				} else {
																					$(this).parents("tr").find(".btn-info").attr("operationState", '0');
																				}
																			}
																		}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
																	})
																},0.1);
															} else {
																$("#pageOneChoseYQ").html('<tr><td colspan="4" style="    text-align: center;font-size: 14px; font-weight: 700;">生产模板未配置设备</td></tr>');
															}
															top.layer.closeAll();
														}
													});
											
											
											// 渲染细胞观察
											$.ajax({
													type : "post",
													data : {
														id : cellPassage_id,
														stepNum : orderNum,
													},
													url : "/experiment/cell/passage/cellPassage/findCellPassageObservations.action",
													success : function(res) {
														var datas = JSON.parse(res);
														var data = JSON.parse(res).CellPassageObservations;
														if(datas.success){
															if(datas.CellObservation){
																$("#xbgc").show();
																var trs = '';
																if(datas.xbjs){
																	$("#xbshow").removeClass("xbshow");
																}else{
																	$("#xbshow").addClass("xbshow");
																}
																if (data.length) {
																	data.forEach(function(v,i) {
																		var xbjsstr = "";
																		if(datas.xbjs){
																			xbjsstr = '<td>'
																			+ '细胞计数 1：'+v.cellNum+'<br>'
																			+ '细胞计数 2：'+v.cellNumber+'<br>'
																			+ '活细胞密度：'+v.cellDensity+'×10^6个/ml<br>'
																			+ '</td>';
																		}else{
																			
																		}
																		var checked = v.operationState == "0" ? '否'
																				: '是';
																		var observationResult = v.observationResult == "0" ? '红'
																				: (v.observationResult == "1" ?'微黄':'偏黄');
																				trs += '<tr><td>'
																						+ v.microscopeNo
																						+ '</td><td>'
																						+ format(v.observationDate)
																						+ '</td><td>'
																						+ observationResult
																						+ '</td>'
																						+xbjsstr
																						+'<td>'
																						+ checked
																						+ '</td><td>'
																						+ v.observationUser.name
																						+'</td><td>'
																						+ v.note
																						+ '</td></tr>';
																			});
																	$("#pageOneChoseXbgc").html(trs);
																	
																} else {
																	$("#pageOneChoseXbgc").html('<tr><td colspan="5" '+
																			'style="text-align: center;font-size: 14px; font-weight: 700;">没有数据</td></tr>');
																}
																top.layer.closeAll();
															}else{
																$("#xbgc").hide();
															}
														}else{
															$("#xbgc").hide();
														}
														
													}
												});
											
											// 渲染试剂选择
											$.ajax({
														type : "post",
														data : {
															id : cellPassage_id,
															stepNum : orderNum,
														},
														url : "/experiment/cell/passage/cellPassage/findProductReagent.action",
														success : function(res) {
															var data = JSON
																	.parse(res).CellproductReagent;
															var trs = '';
															if (data.length) {
																data.forEach(function(
																				v,
																				i) {
																			var name = v.reagentNo ? "选择批次:"
																					+ (v.reagentNo == null
																							|| v.reagentNo == undefined
																							|| v.reagentNo == "null" ? ""
																							: v.reagentNo)
																					: "选择批次";
																			trs += '<tr><td>'
																					+ v.reagentName
																					+ '</td><td>'
																					+ v.reagentManufactor
																					+ '</td><td>'
																					+ v.reagentSpecifications
																					+ '</td><td>'
																					+ '<input type="text" class="sjpc">'
																					+ '</td><td><button class="btn btn-xs btn-info" ph="'
																					+ v.reagentNo
																					+ '" sjid="'
																					+ v.reagentId
																					+ '" yxq="'
																					+ v.reagentValidityTime
																					+ '"  id="'
																					+ v.id
																					+ '" onclick="addReagent1(this)">'
																					+ name
																					+ '</button></td><td id="yxq">'
																					+ v.reagentValidityTime
																					+ '</td><td><input type="text" id="sl" value="'+v.reagentNum+'"><input type="hidden" id="storageItemId" value="'+v.storageItemId+'">'
																					+ '</td><td>'
																					+ v.reagentUnit
																					+ '</td></tr>';
																		})
																$("#pageOneChoseSJ").html(trs);
																$('.sjpc').unbind('keydown').bind('keydown', function (event) {
																	var event = window.event || arguments.callee.caller.arguments[0];
																	if (event.keyCode == 13){
																		setTimeout(function() {
																			//扫码回车
																			//判断该批号试剂是否出库，并且是否是该产品用的
																			var pici = "";
																			var sjid = "";
																			var ph = "";
																			$('.sjpc').each(function(i, v) {
																				if(v.value!=null
																						&&v.value!=""){
																					pici = v.value;
																					sjid = $(v).parent().parent().find(".btn-info").attr("id");
																					ph = $(v).parent().parent().find(".btn-info").attr("ph");
																				}
																			});
																			if(ph!=null
																					&&ph!=""
																						&&ph!="null"){
																				$('.sjpc').each(function(i, v) {
																					if(v.value!=null
																							&&v.value!=""){
																						$(v).val("");
																					}
																				});
																				top.layer.msg("该试剂已确认，无需再次确认！");
																			}else{
																				$.ajax({
																					type: "post",
																					url:ctx+"/experiment/cell/passage/cellPassage/checkReagent.action",
																					data:{
																						pici:pici,
																						sjid:sjid,
																						orderNum:$(".wizard_steps .selected .step_no").text(),
																						cellPassageId:$("#cellPassage_id").text().trim()
																					},
																					success: function(dataBack) {
																						var data = JSON.parse(dataBack);
																						if(data.sueccess){
																							if(data.sjcz
																									&&data.sjck
																									&&data.gqrq
																									&&data.yxq){
																								if(data.pyj){
																									if(data.pyjry){
																										$('.sjpc').each(function(i, v) {
																											if(v.value!=null
																													&&v.value!=""){
																												$(v).parent().parent().find(".btn-info").attr("ph",data.reagentNo);
																												$(v).parent().parent().find(".btn-info").text("选择批次:"+data.reagentNo);
																												$(v).parent().parent().find(".btn-info").attr("yxq",data.reagentValidityTime);
																												$(v).parent().parent().find("#yxq").text(data.reagentValidityTime);
																												$(v).val("");
																											}
																										});
																										top.layer.msg("试剂确认！");
																										$(".bwizard-steps .active>a").click();
																									}else{
																										top.layer.msg(data.note);
																									}
																								}else{
																									$('.sjpc').each(function(i, v) {
																										if(v.value!=null
																												&&v.value!=""){
																											$(v).parent().parent().find(".btn-info").attr("ph",data.reagentNo);
																											$(v).parent().parent().find(".btn-info").text("选择批次:"+data.reagentNo);
																											$(v).parent().parent().find(".btn-info").attr("yxq",data.reagentValidityTime);
																											$(v).parent().parent().find("#yxq").text(data.reagentValidityTime);
																											$(v).val("");
																										}
																									});
																									top.layer.msg("试剂确认！");
																									$(".bwizard-steps .active>a").click();

																								}
																							}else{
																								$('.sjpc').each(function(i, v) {
																									if(v.value!=null
																											&&v.value!=""){
																										$(v).val("");
																									}
																								});
																								//top.layer.msg(data.note);
																								var heighttt = $(window).height()*0.25
																								
																								if(isIpad){
																									
																									layer.msg(data.note,{ offset:heighttt});
																								}else{
																									top.layer.msg(data.note);
																								}
																							}
																						}else{
																							top.layer.msg("查询任务失败！");
																						}
																					}
																				});
																			}
																		},500);
																	}
																});
															} else {
																$("#pageOneChoseSJ").html('<tr><td colspan="7" style="    text-align: center;font-size: 14px; font-weight: 700;">生产模板未配置试剂</td></tr>');
															}
															top.layer.closeAll();
														}
													});
										} else if (stepId == "2") {
											// var stepDesc = $(".wizard_steps
											// .selected
											// .step_descr").text();
											// if (stepDesc.indexOf("分袋") > -1)
											// {
											// $(".feedBack").removeClass("hide");
											// } else {
											// $(".feedBack").addClass("hide");
											// }
											// 样本信息
											if ($("#noBlendTable_wrapper").length) {
												$('#noBlendTable').DataTable().destroy();
												// $("#noBlendTable").text("");
												showSampleTableNoBlend(
														$("#noBlendTable"),
														$(".wizard_steps .selected .step_no").text());
												// noBlendTab.ajax.url('/experiment/cell/passage/cellPassage/noBlendPlateSampleTable.action?id='
												// + cellPassage_id +
												// '&stepNum=' +
												// orderNum).load();
											} else {
												showSampleTableNoBlend(
														$("#noBlendTable"),
														$(".wizard_steps .selected .step_no").text());
											}
											// 已提交质检明细
											if ($("#zhijianDetails_wrapper").length) {
												$('#zhijianDetails').DataTable().destroy();
												// zhijianDetails.ajax.url('/experiment/cell/passage/cellPassage/findQualityItem.action?id='
												// + cellPassage_id +
												// '&stepNum=' +
												// orderNum).load();
												// $("#zhijianDetails").text("");
												showZhijianDetails($("#zhijianDetails"),
															$(".wizard_steps .selected .step_no").text());
											} else {
												showZhijianDetails(
														$("#zhijianDetails"),
														$(".wizard_steps .selected .step_no").text());
											}
											// 质检
											var zhiJian = data.temItemList;
											$("#zhijianBody").html("");
											if (zhiJian.length) {
												zhiJian.forEach(function(vvv,
																i) {
															$("#zjType").text(vvv.typeName);
															$("#zjType").attr("zjId",vvv.typeId);
															renderZhijian(vvv);
														});
											} else {
												$("#zjType").text("");
												$("#zjType").attr("");
												$("#zhijianBody").html('<li class="zhijianli zhijianOld" style="text-align:center">没有数据</li>');
											}
											if (data.cpt != null
													&& data.cpt != "") {
//												var blend = data.cpt.blend;
												var blend = data.cpt;
												var stepNum = $(
														".wizard_steps .selected .step_no")
														.text();// data.cpt.orderNum;
												// 混合的显示混合列表
												if (blend == "1") {
													$("#blend_div").show();
													if ($("#blendTable_wrapper").length) {
														// $("#blendTable").text("");
														$('#blendTable').DataTable().destroy();
															
														showSampleTable(
																$("#blendTable"),
																$(".wizard_steps .selected .step_no").text());
														// blendTab.ajax.url('/experiment/cell/passage/cellPassage/blendPlateSampleTable.action?id='
														// + cellPassage_id +
														// '&stepNum=' +
														// stepNum).load();
														
														//混合子表自动生成数据  如果子表没有数据，有数据不管
//														setTimeout(function() {
//															shencheng($(".wizard_steps .selected .step_no").text());
//														},1000);
													} else {
														showSampleTable(
																$("#blendTable"),
																$(".wizard_steps .selected .step_no").text());
														//混合子表自动生成数据  如果子表没有数据，有数据不管
//														setTimeout(function() {
//															shencheng($(".wizard_steps .selected .step_no").text());
//														},1000);
													}
													$(".blendSample").removeClass("hide");
												} else {
													$("#blend_div").hide();
													$(".blendSample").addClass("hide");
												}
											}
											top.layer.closeAll();
										} else if (stepId == '3') {
											//jsonRes 放的是第二次 循环  cell----- 的数据
                                            //data.reagenOrCos, 包括质检  第一次循环的数据
											renderCaozuo(data.reagenOrCos,
													data.jsonRes,
													data.customTest,
													data.cellResult,
													data.temProd,
													data.haveEndTime);
											// 生产步骤的开始时间和结束时间
									
											if (data.cellResult != null) {
												$("#productStartTime")
														.text(
																data.cellResult.productStartTime);
//												if(data.cellResult.productStartTime!=""&&data.cellResult.productStartTime!=null){
//													$("#productStartTime").parents('#btn_startTime').attr("disabled",true);
//												}
												if(data.haveEndTime){
													$("#productStartTime").parents('#btn_startTime').attr("disabled",true);
                                                	$("#productEndTime").parents('#btn_endTime').attr("disabled",true);	

												}
//												$("#productStartTime")
//                                                if(data.cellResult.productEndTime!=""&&data.cellResult.productEndTime!=null){
////                                                	$("#productEndTime").parents('#btn_endTime').attr("disabled",true);	
//												}
												$("#productEndTime").text(data.cellResult.productEndTime);
												
												$("#notes").val(data.cellResult.notes);
											}
											top.layer.closeAll();
										} else if (stepId == '4') {
											// 渲染操作指令
											renderOperationalOrder(
													data.cellPass, stepId);
											// 渲染生产记录
											// renderProductionRecord(data.cellReco.templeItem,
											// data.cellReco, stepId);
//											renderProductionRecordNew(
//													data.cellReco.templeItem,
//													data.cellReco, stepId);
											renderProductionRecordNew(
													data.templeContent,
													data.cellRecoId,data.cellRecoContent, stepId);

											var stepNum = $(".wizard_steps .selected .step_no").text(); // 当前得步骤数
											// 判断按钮显示隐藏 ajax判断 当前步骤是否是该生产的最后一步
											// 最后一步显示填写细胞数量按钮
											// 并且继续生产按钮名称改为完成生产操作
											$.ajax({
														type : 'post',
														async : false,
														url : ctx
																+ '/experiment/cell/passage/cellPassage/changeButtonBlock.action',
														data : {
															mainId : cellPassage_id,
															stepNum : stepNum,
														},
														success : function(data) {
															var data1 = JSON
																	.parse(data);
															if (data1.success) {
																if (data1.zhyb) {
																	$("#jxsc").val("完成生产操作");
																	$("#next").show();
																}else{
																	$("#next").hide();
																}
																// 加判断，如果当前步骤完成其他按钮都不显示
																if (data1.dqbz) {
																	if (baocun) {
																		if (caozuo) {
																			$("#jxsc").show();
																			$("#chongfu").show();
																			if (adminUser) {
																				$("#bhgzz").show();
																			} else {
																				$("#bhgzz").hide();
																			}
																		} else{
																			$("#jxsc").hide();
																			$("#chongfu").hide();
																			if(adminUser){
																				$("#bhgzz").show();
																			}else{
																				$("#bhgzz").hide();
																			}
																		}
																		$("#next").hide();
																	} else{
																		$("#jxsc").hide();
																		$("#chongfu").hide();
																		$("#bhgzz").hide();
																	}
																} else {
																	$("#jxsc").hide();
																	$("#chongfu").hide();
																	$("#bhgzz").hide();
																}
															} else {

															}
															top.layer.closeAll();
														}
													});
										}
									}
								});
						top.layer.closeAll();
					
				});
   }
//子表自动生成结果
//function shencheng(stepNum){
//	if($("#save").is(":hidden")){
//		
//	}else{
//		$.ajax({
//			type : 'post',
//			url : ctx
//					+ '/experiment/cell/passage/cellPassage/blendSampleClick.action',
//			data : {
//				stepNum:stepNum,
//				id : cellPassage_id
//			},
//			success : function(data) {
//				var data = JSON.parse(data);
//				if (data.success) {
//					blendTab.ajax.reload();
//				} else {
//					
//				}
//			}
//		});
//	}
//}
//条码扫码确认
$("#sampleCodeConfirm").keyup(function(event){
	if(event.keyCode ==13){
		setTimeout(function() {
			sampleCodeConfirmMes();
		},500);
	}
 });
function sampleCodeConfirmMes(){
	if($("#save").is(":hidden")){
		top.layer.msg("您无权限操作此功能！");
	}else{
		var sampleCodeConfirm = $("#sampleCodeConfirm").val();
		if(sampleCodeConfirm!=null&&sampleCodeConfirm!=""){
			$.ajax({
				type : 'post',
				url : ctx
						+ '/experiment/cell/passage/cellPassageNow/sampleCodeConfirm.action',
				data : {
					stepNum:$(".wizard_steps .selected .step_no").text(),
					id : cellPassage_id,
					sampleCodeConfirm : sampleCodeConfirm,
				},
				success : function(data) {
					var data = JSON.parse(data);
					if (data.success) {
						if (data.cz) {
							if (data.qr) {
								$(".pageOneQRcodesample").each(function(i, v) {
									if(v.getAttribute("code")==sampleCodeConfirm){
										$(v).attr("state","已确认");
										$(v).find('td')[1].textContent="已确认";
									}
								});
								$("#sampleCodeConfirm").val("");
								top.layer.msg("样本确认成功！");
							} else {
								$("#sampleCodeConfirm").val("");
								top.layer.msg("已确认数据，无需再确认！");
							}
						} else {
							$("#sampleCodeConfirm").val("");
							top.layer.msg(sampleCodeConfirm+"--"+"该条码不是该批次产品，请核实！");
						}
					} else {
						top.layer.msg("条码查询失败，请联系管理员！");
					}
				}
			});
		}else{
			
		}
	}
}
//下一步扫码确认
$("#sampleCodeAfterConfirm").keyup(function(event){
	if(event.keyCode ==13){
		setTimeout(function() {
			sampleCodeAfterConfirmMes();
		},500);
	}
 });
function sampleCodeAfterConfirmMes(){
	if($("#save").is(":hidden")){
		top.layer.msg("您无权限操作此功能！");
	}else{
		var sampleCodeAfterConfirm = $("#sampleCodeAfterConfirm").val();
		if(sampleCodeAfterConfirm!=null&&sampleCodeAfterConfirm!=""){
			$.ajax({
				type : 'post',
				url : ctx
						+ '/experiment/cell/passage/cellPassageNow/sampleCodeAfterConfirm.action',
				data : {
					stepNum:$(".wizard_steps .selected .step_no").text(),
					id : cellPassage_id,
					sampleCodeAfterConfirm : sampleCodeAfterConfirm,
				},
				success : function(data) {
					var data = JSON.parse(data);
					if (data.success) {
						if (data.cz) {
							if (data.qr) {
								$(".pageOneQRcodeoperation").each(function(i, v) {
									if(v.getAttribute("code")==sampleCodeAfterConfirm){
										$(v).attr("state","已确认");
										$(v).find('td')[1].textContent="已确认";
									}
								});
								$("#sampleCodeAfterConfirm").val("");
								top.layer.msg("样本确认成功！");
							} else {
								$("#sampleCodeAfterConfirm").val("");
								top.layer.msg("已确认数据，无需再确认！");
							}
						} else {
							$("#sampleCodeAfterConfirm").val("");
							top.layer.msg(sampleCodeAfterConfirm+"--"+"该条码不是该批次产品，请核实！");
						}
					} else {
						top.layer.msg("条码查询失败，请联系管理员！");
					}
				}
			});
		}else{
			
		}
	}
}


//扫码提交质检
$("#zhiJianCodeTiJiao").keyup(function(event){
	if(event.keyCode ==13){
		setTimeout(function() {
			zhiJianCodeTiJiao();
		},500);
	}
 });
function zhiJianCodeTiJiao(){
	if($("#save").is(":hidden")){
		top.layer.msg("您无权限操作此功能！");
	}else{
		var zhiJianCodeTiJiao = $("#zhiJianCodeTiJiao").val();
		if(zhiJianCodeTiJiao!=""&&zhiJianCodeTiJiao!=null){
			ajax(
					"post",
					"/experiment/cell/passage/cellPassage/submitToQualityTestSample.action",
					{
						zhiJianCode: zhiJianCodeTiJiao,
						orderNum : $(".wizard_steps .selected .step_no").text(),

					},
					function(data) {
						if (data.success) {
							top.layer
									.msg(biolims.common.submitSuccess);
							$("#zhiJianCodeTiJiao").val("");
							zhijianDetails.ajax.reload();
						} else {
							$("#zhiJianCodeTiJiao").val("");
							top.layer
									.msg("条码有误或已提交请检查！");
						}
					}, null);
		}else{
			
		}
	}
}



//选择原辅料批次工前准备
function addReagent1(that) {
	var batch = $("#cellpassagebatch").val();
	var cellPassageId = $("#cellPassage_id").text();
	top.layer.open({
		title : biolims.common.selReagent,
		type : 2,
		area: top.screeProportion,
		btn : biolims.common.selected,
		content : [
				window.ctx + "/storage/getStrogeReagent.action?id=" + $(that).attr("sjid")
						+ "&batch=" + batch +"&cellPassageId="+cellPassageId+"", "" ],
		yes : function(index, layero) {
			var batch = $(".layui-layer-iframe", parent.document)
					.find("iframe").contents().find("#addReagent .chosed")
					.children("td").eq(2).text();
			var id = $(".layui-layer-iframe", parent.document)
			.find("iframe").contents().find("#addReagent .chosed")
			.children("td").eq(4).attr("useid");
			var yxq = $(".layui-layer-iframe", parent.document)
			.find("iframe").contents().find("#addReagent .chosed")
			.children("td").eq(4).text();
			// var sn = $(".layui-layer-iframe", parent.document).find("iframe")
			// .contents().find("#addReagent .chosed").children("td")
			// .eq(4).text();
			$(that).attr("ph",batch).attr("yxq",yxq);
			$(that).text("选择批次："+batch);
//			$(that).parents().find("#yxq").text(yxq);
			$(that).parent().parent().find("#yxq").text(yxq);
			$(that).parent().parent().find("#storageItemId").val(id);
//			item.find(".label-primary span").text(batch);
			// item.find(".label-info span").text(sn);
			top.layer.close(index)
		},
	});
}
function addStates(){debugger
	var benbu=$("ul.bwizard-steps").find("li.active").attr("note");
	var step1State="";
	var step2State="";
	var step3State="";
	var step4State="";
	if(benbu=="1"){
		
		step1State="0";
		var requiredField = templateRequiredFilter(benbu);		
		if (!requiredField) {
			step1State="0";
		}
		// 判断是否选择设备
		var arr = [];
		var fuyong = true;
		// 判断是否选择试剂
		var arr1 = []
		var fuyong1 = true;

		$("#pageOneChoseYQ .btn-info").each(
				function(i, v) {
					var jsonn = {};
					jsonn.id = v.getAttribute("cosid");
					jsonn.code = v.getAttribute("code");
					jsonn.name = v.getAttribute("name");

					if (jsonn.name != null
							&& jsonn.name != "null"
							&& jsonn.name != "") {
					} else {
						fuyong = false;
						return false;
					}
					arr.push(jsonn);
				});
		
		if (fuyong == false) {
			step1State="0";
		}
		
		$("#pageOneChoseSJ .btn-info").each(function(i, v) {
					var jsonn = {};
					jsonn.ph = v.getAttribute("ph");
					jsonn.yxq = v.getAttribute("yxq");

					if (jsonn.ph != null
							&& jsonn.ph != "null"
							&& jsonn.ph != "") {
					} else {
						fuyong1 = false;
						return false;
					}
					arr1.push(jsonn);
		});
		
		if (fuyong1 == false) {
			step1State="0";
		}
		// 判断是否选择
		if ($("#operatingRoomName").val() != null
				&& $("#operatingRoomName").val() != "") {
		} else {
			step1State="0";
		}
		
		var fuyong2=true;
		var mssg2;
		$(".pageOneRoomConfirm").each(function(i,v){
			if($(v).attr("state")!="1"){
				
				mssg2="房间确认的第"+(i+1)+"个按钮未确认";
				fuyong2=false;
				return false;
			}
		});
		if(fuyong2 == false){
			top.layer.closeAll();
			step1State="0";
		}else{
			var fuyong3=true;
			var mssg3;
			$(".pageOneQRcodesample").each(function(i,v){
				if($(v).attr("state")!="已确认"){
					
					mssg3="样本条码确认的第"+(i+1)+"个核对结果未确认";
					fuyong3=false;
					return false;
				}
			});
			if(fuyong3 == false){
				step1State="0"
			}else{
				var fuyong4=true;
				var mssg4;
				$(".pageOneQRcodeoperation").each(function(i,v){
					if($(v).attr("state")!="已确认"){
						
						mssg4="操作条码确认的第"+(i+1)+"个核对结果未确认";
						fuyong4=false;
						return false;
					}
				});
				if(fuyong4 == false){
					step1State="0"
				}else{
					$("#pageOneChoseYQ button").each(function(i,v){
						if($(v).attr("operationstate")!="1"){
							
							mssg4="第"+(i+1)+"个选定的设备未确认";
							fuyong4=false;
							return false;
						}
					});
					if(fuyong4 == false){
						step1State="0"
					}else{
						$(".pageOneOperationalOrder").each(function(i,v){
							if($(v).attr("state")!="1"){
								
								mssg4="操作指令的第"+(i+1)+"个按钮未确认";
								fuyong4=false;
								return false;
							}
						});
						if(fuyong4 == false){
							step1State="0"
						}else{
							//判断生产记录是否选择完毕
							$("#firstTab").find("#pageOneProductionRecord .form-group").each(function(i,v){
								$(v).find("input").each(function(i,vv){
									if($(vv).val()!=""){
										
									}else{
										mssg4=$(vv).siblings("label").text()+"不能为空";
										fuyong4=false;
										return false;
									}
								});
								if(fuyong4 == false){
									return false;
								}
							});
							if(fuyong4 == false){
								step1State="0"
							}else{
								step1State="1"
							}
						}
					}
				}
			}
		}
	}else if(benbu=="3"){
		step2State="0";
		var fuyong5=true;
		var mssg5;
		var requiredField = templateRequiredFilter(benbu);		
		if (!requiredField) {
			fuyong5 = false;
		}
		
		
		if($("#thirdTab").find("#productStartTime").text()==""){
			
			fuyong5 = false;
			mssg5="生产开始时间未选择";
		}
		if($("#thirdTab").find("#productEndTime").text()==""){
		
			fuyong5 = false;
			mssg5="生产结束时间未选择";
		}
		
		$("#thirdTab .caozuoItem").find(".time").find("button").each(function(i,v){
			if($(v).children().eq(1).text()==""){
				fuyong5 = false;
				mssg5="生产开始或结束时间未选择";
				return false;
			}
		});
		if(fuyong5 == false){
			step2State="0";
		}else{
			$(".timeline-footer #reagentBody .reagli").each(function(i,v){
				if($(v).find("input").eq(0).val()==""){
					
					fuyong5 = false;
					mssg5=$(v).find("input").eq(0).prev().text()+"未填写";
					return false;
				}
			});
			if(fuyong5 == false){
				step2State="0";
			}else{
				step2State="1";
			}
		}
	}else if(benbu=="2"){
		step3State="0";
		var fuyong6=true;
		var mssg6;
		var requiredField = templateRequiredFilter(benbu);		
		if (!requiredField) {
			fuyong6=false;
		}
		
		$("#zhijianDetails tbody tr").find("td[savename='submit']").each(function(i,v){
			if($(v).text()!="是"){
				
				mssg6="质检明细表的第"+(i+1)+"条数据未提交";
				fuyong6=false;
				return false;
			}
		});
		if(fuyong6 == false){
			step3State="0";
		}else{
			$("#thirdTab .timeline-item").find("#cosBody .cosli").each(function(i,v){
				if($(v).find(".label").text()==""){
					
					fuyong5 = false;
					mssg5=$(v).find(".label").prev().text()+"未选择";
					return false;
				}
			});
			if(fuyong5 == false){
				step3State="0";
			}else{
				$("#thirdTab .timeline-item").find("#zhijianBodyNew .input-group").each(function(i,v){
					if($(v).find("input").val()==""){
						
						fuyong5 = false;
						mssg5=$(v).siblings("small").find(".zhijianName").text()+$(v).find("input").prev().text()+"未填写";
						//return false;
						return false;
					}
				});
				if(fuyong5 == false){
					step3State="0";
				}else{
					step3State="1";
				}
			}
		}
	}else if(benbu=="4"){
		step4State="0";
		var requiredField = templateRequiredFilter(benbu);		
		if (!requiredField) {
			fuyong6=false;
		}
		//判断操作指令是否选择完毕
		var fuyong2=true;
		var mssg2;
		$(".pageFourOperationalOrder").each(function(i,v){
			if($(v).attr("state")!="1"){
				
				mssg2="操作指令的第"+(i+1)+"个按钮未确认";
				fuyong2=false;
				return false;
			}
		});
		if(fuyong2 == false){
			step4State="0";	
		}else{
			//判断操作指令是否选择完毕
			$("#fourTab").find("#pageFourProductionRecord .form-group").each(function(i,v){
				if($(v).find("input").val()==""){
					
					mssg2=$(v).find("input").prev().text()+"不能为空";
					fuyong2=false;
					$(v).find("input").focus();
					return false;
				}
			});
			if(fuyong2 == false){
				step4State="0";	
			}else{
				step4State="1";	
			}
		}
		
	}
	debugger
	var states= new Array();
	states[0]=step1State;//第一步
	states[1]=step2State;//第二步
	states[2]=step3State;//第三步
	states[3]=step4State;//第四步
	var stepidVal=$("#wizard_verticle ul.wizard_steps").find("a.selected").attr("stepid");
	$.ajax({
		url:ctx+"/experiment/cell/passage/cellPassageNow/saveStepState.action",
		type:"post",
		async:false,
		data:{
			id:stepidVal,
			state:states,
		},
		success:function(){
			
		}
	});
}
//保存数据(点击自动保存)
function saveStepItemZidong() {
	
	addStates();
	
	var stepId = $(".bwizard-steps>.active").attr("note");
	var action;
	var saveData;

	// 判断是否选择设备
	var arr = [];
	var fuyong = true;

	$("#pageOneChoseYQ .btn-info").each(function(i, v) {
		var jsonn = {};
		jsonn.id = v.getAttribute("cosid");
		jsonn.code = v.getAttribute("code");
		jsonn.name = v.getAttribute("name");
		jsonn.operationState = v.getAttribute("operationState");

		if (jsonn.name != null && jsonn.name != "null" && jsonn.name != "") {
		} else {
			fuyong = false;
//			return false;
		}
		arr.push(jsonn);
	});
	var changeLog = "细胞生产：";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	
	var changeLogs = "";
	if(changeLog != "细胞生产：") {
		changeLogs = changeLog;
		$("#changelog").val(changeLogs);
	}
	
	
	if (fuyong == false) {
//		top.layer.msg("请选择设备");
//		return false;
	}

	// 判断是否选择
	if ($("#operatingRoomName").val() != null
			&& $("#operatingRoomName").val() != "") {
	} else {
//		top.layer.msg("请选择房间");
		//return false;
	}
	if (stepId == "1") { // 工前准备
		
		
		//试剂
		var productionReagent = [];
		$("#pageOneChoseSJ button").each(
				function(i, v) {
					var obj = {};
					obj.id = v.id;
					obj.reagentNo = v.getAttribute("ph") ? v
							.getAttribute("ph") : "";
					obj.reagentValidityTime = v.getAttribute("yxq") ? v
							.getAttribute("yxq") : "";
							
					obj.reagentNum = $(v).parent().parent().find("#sl").val();
					obj.storageItemId = $(v).parent().parent().find("#storageItemId").val();
					productionReagent.push(obj);
		})
		
		// 操作指令
		var operationalOrder = [];
		$("#pageOneOperationalOrder .pageOneOperationalOrder").each(
				function(i, v) {
					var obj = {};
					obj.id = v.id;
					obj.productionInspection = v.getAttribute("state") ? v
							.getAttribute("state") : "0";
					obj.operationNotes = v.getAttribute("note") ? v
							.getAttribute("note") : "";
					operationalOrder.push(obj);
				})
		// 生产记录
		var productionRecord = {
			guohkId : $("#pageOneProductionRecord").attr("pid")
		};
		$("#pageOneProductionRecord input").each(function(i, v) {
			productionRecord[v.id] = v.value;
			
		});
		
		// 房间核查保存
		var cellRoomPass = [];
		
		$("#pageOneRoomConfirm .pageOneRoomConfirm").each(function(i, v) {
			
			var jsonn = {};
			jsonn.id = v.getAttribute("id");
			jsonn.state = v.getAttribute("state");
            
			cellRoomPass.push(jsonn);
		});
		
		var template = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			orderNumTwo : ord,
			stepId : stepId,
			temNstr : operationalOrder,
			content : productionRecord,
			productionReagent:productionReagent,
			cellproductCos : arr,
			cellRoomPass : cellRoomPass,
		};
		saveData = JSON.stringify(template);
		action = "/experiment/cell/passage/cellPassageNow/savepreparationProductionRecord.action";
	} else if (stepId == "2") { // 拼质检的数据
		var noBlendTabledata = savenoBlendTablejson($("#noBlendTable"));
		var blendTabledata = savenoBlendTablejson($("#blendTable"));
		//质检日志
		var datas = savenoBlendTablejson($("#blendTable"));
		var changeLogItem = "质检：";
		changeLogItem = getChangeLog(datas, $("#blendTable"), changeLogItem);
		var changeLogItemLast = "";
		if (changeLogItem != "质检：") {
			changeLogItemLast = changeLogItem
		}
		
		var zhijian = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			stepId : stepId,
			noBlendTabledata : noBlendTabledata,
			blendTabledata : blendTabledata,
		};
		saveData = JSON.stringify(zhijian);
		action = "/experiment/cell/passage/cellPassageNow/saveQualityInspectionResults.action";
	} else if (stepId == "3") {
		var arr = [];
		$(".caozuoItem").each(
				function(i, v) {
					var obj = {};
					obj.id = v.getAttribute("czid");
					obj.state = v.getAttribute("state") ? v
							.getAttribute("state") : "0";
					obj.startTime = $(v).find("#btn_startTime .stepsTime")
							.text(); // $("#btn_startTime
					// .stepsTime").text();
					obj.endTime = $(v).find("#btn_endTime .stepsTime").text(); // $("#btn_endTime
					// .stepsTime").text();
					// 拼原辅料的数据
					var reagentli = $(v).find(".reagli");
					var reagent = [];
					reagentli.each(function(i, val) {
						var reagentItem = {};
						reagentItem.id = val.getAttribute("reagentid");
						reagentItem.name = $(val).children(".text").text();
						reagentItem.code = val.getAttribute("code");
						reagentItem.batch = $(val).children(".label-primary")
								.children("span").text();
						reagentItem.sn = "";
						// $(val).children(".label-info")
						// .children("span").text();
						reagentItem.inputVal = $(val).find(".input-sm").val();
						reagentItem.unit = $(val).find(".unit").val();
						reagent.push(reagentItem);
					});
					obj.reagent = reagent;
					// 拼设备的数据
					var cosli = $(v).find(".cosli");
					var cos = [];
					cosli
							.each(function(i, val) {
								var cosItem = {};
								cosItem.id = val.getAttribute("cosid");
								cosItem.name = $(val)
										.children(".label-warning").text();
								cosItem.code = val.getAttribute("code");
								cosItem.typeId = val.getAttribute("typeid");
								cos.push(cosItem);
							});
					obj.cos = cos;

					var result1 = {}; // 自定义字段
					var resultValue = ""; // 自定义字段值
					$("#pageThree" + obj.id + " input").each(
							function(z, x) {
								result1[x.id] = x.value;
								resultValue = resultValue + $(x).prev().text()
										+ ":" + x.value + "\n";
							});
					obj.customTest = result1;
					obj.customTestValue = resultValue;

					arr.push(obj)
				});
		var result = {}; // 自定义字段
		$("#pageThree input").each(function(i, v) {
			result[v.id] = v.value;
		})
		var template = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			stepId : stepId,
			result : result,
			content : arr,
			productStartTime : $("#productStartTime").text(),
			productEndTime : $("#productEndTime").text(),
			notes : $("#notes").val(),
		// startime:$("#btn_startTime .stepsTime").text(),
		// endtime:$("#btn_endTime .stepsTime").text()
		};
		saveData = JSON.stringify(template);
		action = "/experiment/cell/passage/cellPassageNow/saveCosandReagent.action";
	} else if (stepId == "4") {
		// 操作指令
		var operationalOrder = [];
		$("#pageFourOperationalOrder .pageFourOperationalOrder").each(
				function(i, v) {
					var obj = {};
					obj.id = v.id;
					obj.productionInspection = v.getAttribute("state") ? v
							.getAttribute("state") : "0";
					operationalOrder.push(obj);
				})
		// 身产记录
		var productionRecord = {
			guohkId : $("#pageFourProductionRecord").attr("pid")
		};
		$("#pageFourProductionRecord input").each(function(i, v) {
			productionRecord[v.id] = v.value;
		});
		var template = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			stepId : stepId,
			temNstr : operationalOrder,
			content : productionRecord,
		};
		saveData = JSON.stringify(template);
		action = "/experiment/cell/passage/cellPassageNow/savefourthStepCellproductResult.action";
	}
	$.ajax({
		type : "post",
		url : ctx + action,
		data : {
			"saveData" : saveData,
			changeLog:changeLog,
			changeLogItem:changeLogItemLast
		},
		success : function(data) {
			var data = JSON.parse(data);
			if (data.success) {
				
			} else {
				top.layer.closeAll();
			}
		}
	});
}

function add0(m) {
	return m < 10 ? '0' + m : m
}

function format(timestamp) {
	var time = new Date(parseInt(timestamp));
	var year = time.getFullYear();
	var month = time.getMonth() + 1;
	var date = time.getDate();
	var hours = time.getHours();
	var minutes = time.getMinutes();
	var seconds = time.getSeconds();
	return year + '-' + add0(month) + '-' + add0(date);// + ' ' + add0(hours) + ':' + add0(minutes) + ':' + add0(seconds);
}


var panduan2 = true;
// 重复当前步骤
function chongfu() {
	panduan2 = false;
	top.layer
			.confirm(
					"确认重复操作当前步骤？",
					{
						icon : 3,
						title : biolims.common.prompt,
						btn : [ biolims.common.selected, "取消" ],
						cancel : function(indexz, layero) {
							panduan2 = true;
							top.layer.close(indexz);
						}
					},
					function(indexz) {
						var stepNum = $(".wizard_steps .selected .step_no")
								.text(); // 当前得步骤数
						$
								.ajax({
									type : 'post',
									async : false,
									url : ctx
											+ '/experiment/cell/passage/cellPassage/chongfu.action',
									data : {
										mainId : cellPassage_id,
										stepNum : stepNum,
									},
									success : function(data) {
										var data = JSON.parse(data);
										if (data.success) {
											top.layer.msg("重复步骤生成成功！");
											panduan2 = true;
											$("#maincontentframe",
													window.parent.document)[0].src = window.ctx
													+ "/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id="
													+ cellPassage_id
													+ "&orderNum=" + stepNum;
										} else {
											top.layer.msg("重复步骤生成失败！");
											panduan2 = true;
										}
									}
								});
						top.layer.close(indexz);
					});
}
var panduan = true;
// 完成当前步,将样本流入到下一步
function finishStep() {
	var step1State="";
	var step2State="";
	var step3State="";
	var step4State="";
	var stepidVal=$("#wizard_verticle ul.wizard_steps").find("a.selected").attr("stepid");
	$.ajax({
		url:ctx+"/experiment/cell/passage/cellPassageNow/selStepState.action",
		type:"post",
		async:false,
		data:{
			id:stepidVal,
		},
		success:function(data){
			var data=JSON.parse(data);
			if(data.success){
				if(data.val.step1State !="1"){
					step1State="0";
				}else{
					step1State=data.val.step1State;
				}
				if(data.val.step2State !="1"){
					step2State="0";
				}else{
					step2State=data.val.step2State;
				}
				if(data.val.step3State !="1"){
					step3State="0";
				}else{
					step3State=data.val.step3State;
				}
				if(data.val.step4State !="1"){
					step4State="0";
				}else{
					step4State=data.val.step4State;
				}
			}
		}
		
	});
	if(step1State!="1" || step2State!="1" || step3State!="1" || step4State!="1"){
		if(step1State!="1"){
			top.layer.closeAll();
			top.layer.msg("工前准备，有未填写信息，请全部填写完成！");
			return false;
		}else if(step2State!="1"){
			top.layer.closeAll();
			top.layer.msg("生产操作，有未填写信息，请全部填写完成！");
			return false;
		}else if(step3State!="1"){
			top.layer.closeAll();
			top.layer.msg("质检信息，有未填写信息，请全部填写完成！");
			return false;
		}else if(step4State!="1"){
			top.layer.closeAll();
			top.layer.msg("完工清场，有未填写信息，请全部填写完成！");
			return false;
		}else{
			top.layer.closeAll();
			top.layer.msg("有未填写信息，请全部填写完成！");
			return false;
		}
	}
	var bao = false;
	var isstop = false;

	// 判断要不要入库
	var ruku = false;
	
	//判断操作指令是否选择完毕
	var fuyong2=true;
	var mssg2;
	$(".pageFourOperationalOrder").each(function(i,v){
		if($(v).attr("state")!="1"){
			
			mssg2="操作指令的第"+(i+1)+"个按钮未确认";
			fuyong2=false;
			return false;
		}
	});
	if(fuyong2 == false){
		top.layer.closeAll();
		top.layer.msg(mssg2);
		return false;
	}
	//判断操作指令是否选择完毕
	$("#fourTab").find("#pageFourProductionRecord .form-group").each(function(i,v){
		if($(v).find("input").val()==""){
			
			mssg2=$(v).find("input").prev().text()+"不能为空";
			fuyong2=false;
			$(v).find("input").focus();
			return false;
		}
	});
	if(fuyong2 == false){
		top.layer.closeAll();
		top.layer.msg(mssg2);
		return false;
	}
	
	var stepNum = $(".wizard_steps .selected .step_no").text(); // 当前得步骤数
	var blendLength = $("#blendTable .dataTables_empty").length;
	$
			.ajax({
				type : 'post',
				async : false,
				url : ctx
						+ '/experiment/cell/passage/cellPassage/findProductTime.action',
				data : {
					mainId : cellPassage_id,
					stepNum : stepNum,
				},
				success : function(data) {
					var data = JSON.parse(data);

					if (data.success) {
						bao = true;
					} else {
						top.layer.msg("生产开始时间/结束时间未选择，不能收获！");

					}

					// false 不用入库
					if (data.state) {
						// true 入库
						if (data.samplestate) {
							ruku = false;
						} else {
							ruku = true;
						}
					} else {
						ruku = false;
					}

				}
			});
	if (bao) {
		if (panduan) {
			if (ruku) {
				top.layer.msg("结果表还未入库，请先入库！");
			} else {
				panduan = false;
				top.layer
						.confirm(
								"确认继续生产？",
								{
									icon : 3,
									title : biolims.common.prompt,
									btn : [ biolims.common.selected, "取消" ],
									cancel : function(indexz, layero) {
										panduan = true;
										top.layer.close(indexz);
									}
								},
								function(indexz) {

									var id = "";
									var ids = [];
									var blend = false;
									// 是否可以收获
									var harvest = false;
									// 样本结果表数量
									var resultNum = true;
									// 样本结果表状态
									var result = true;
									// 样本结果描述
									var message = "";

									// 是否是生产
									var isHarvest = false;
									// 是否存在未处理的偏差
									var deviation = false;

									// 是否让js继续

									$
											.ajax({
												type : 'post',
												async : false,
												url : ctx
														+ '/experiment/cell/passage/cellPassage/findBlendState.action',
												data : {
													mainId : cellPassage_id,
													stepNum : stepNum,
												},
												success : function(data) {
													var data = JSON.parse(data);
													var bl = data.ti.blend;
													if (bl == "1") {
														blend = true;
													}
													// 判断是否是收获 并是否可收获
													if (data.harvest) {
														harvest = true;
													} else {
														harvest = false;
													}
													// 判断样本结果表是否有数据
													if (data.resultNum) {
														resultNum = true;
													} else {
														resultNum = false;
													}
													// 判断样本结果表是否数据有问题
//													if (data.result) {
//														result = true;
//													} else {
//														result = false;
//													}
													if (data.result) {
														result = true;
													} else {
														result = true;
													}

													// 判断当前步骤是否是收获
													if (data.isHarvest) {
														isHarvest = true;
													} else {
														isHarvest = false;
													}

													// 判断当前样本是否存在未处理的偏差
													if (data.deviation) {
														deviation = true;
													} else {
														deviation = false;
													}

													message = data.message;

												}
											});
									if (isHarvest) {
										if (deviation) {
											isstop = true;
										}
									} else {

									}
									if (isstop) {
										top.layer.msg("存在未处理的偏差，不能收获！");
										panduan = true;
									} else {
										if (harvest) {
											if (resultNum) {
												if (result) {
													if (blend == true) {// }&&
														// blendLength
														// == 0)
														// {
														// //说明此步混合了
														// id =
														// $("#blendTable").find("tbody").children("tr").find("input[type='checkbox']").val();
														var room = $("#operatingRoomName").val();
														var roomid = $("#operatingRoomId").val();
														var rwdid=$("#cellPassage_id").text();
														var bzname=$("#steptiele").text();
														var batch=$("#cellPassage_batch").text();
														var orderNum=$(".wizard_steps .selected .step_no").text();
														$.ajax({
																	type : 'post',
																	url : ctx
																			+ '/experiment/cell/passage/cellPassage/finishPresentStep.action',
																	data : {
																		mainId : cellPassage_id,
																		id : id,
																		ultravioletlamptime:$("#ultravioletlamptime").val(),
																		ids : ids,
																		hhzt : "1",
																		stepNum : stepNum,
																		startTime : $(
																				"#startTime")
																				.text(),
																		endTime : $(
																				"#endTime")
																				.text(),
																		roomName : room,
																		roomId : roomid,
																		rwdid: rwdid,
																		bzname:bzname,
																		batch:batch,
																		orderNum :orderNum,		
																	},
																	success : function(
																			data) {
																		var data = JSON
																				.parse(data);
																		if (data.success) {
																			top.layer
																					.msg("确认完成");
																			$(
																					"#jxsc")
																					.hide();
																			$(
																					"#chongfu")
																					.hide();
																			$(
																					"#bhgzz")
																					.hide();
																			$.ajax({
																				type : 'post',
																				async : false,
																				url : ctx
																						+ '/experiment/cell/passage/cellPassage/changeButtonBlockNew.action',
																				data : {
																					mainId : cellPassage_id,
																					stepNum : stepNum,
																				},
																				success : function(data) {
																					var data1 = JSON.parse(data);
																						if (data1.zhyb) {
																							$("#maincontentframe",
																									window.parent.document)[0].src = window.ctx
																									+ "/experiment/cell/passage/cellPassage/showCellPassageResultTable.action?id="
																									+ cellPassage_id
																									+ "&orderNum=" + stepNum;
																							        +"&bpmTaskId="				
																						}else{
																							$("#maincontentframe",
																									window.parent.document)[0].src = window.ctx
																									+ "/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id="
																									+ cellPassage_id
																									+ "&orderNum=" + stepNum;
																						}
																					}
																			});
//																			$("#maincontentframe",
//																					window.parent.document)[0].src = window.ctx
//																					+ "/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id="
//																					+ cellPassage_id
//																					+ "&orderNum=" + stepNum;
////																			$("#maincontentframe",
////																					window.parent.document)[0].src = window.ctx
////																					+ "/experiment/cell/passage/cellPassage/showCellPassageResultTable.action?id="
////																					+ cellPassage_id
////																					+ "&orderNum=" + stepNum;
////																			        +"&bpmTaskId="																			
																			panduan = true;
																			// noBlendTab.ajax.reload();
																		} else {
																			panduan = true;
																		}
																	}
																});
													} else {
														// var rows =
														// $("#noBlendTable").find("tbody").children("tr");
														// $.each(rows,
														// function(ii, vv){
														// var id =
														// $(vv).find("input[type='checkbox']").val();
														// ids.push(id);
														// });
														$
																.ajax({
																	type : 'post',
																	url : ctx
																			+ '/experiment/cell/passage/cellPassage/finishPresentStep.action',
																	data : {
																		mainId : cellPassage_id,
																		id : id,
																		ids : ids,
																		hhzt : "0",
																		stepNum : stepNum,
																		startTime : $(
																				"#startTime")
																				.text(),
																		endTime : $(
																				"#endTime")
																				.text(),
										

																	},
																	success : function(
																			data) {
																		var data = JSON
																				.parse(data);
																		if (data.success) {
																			top.layer
																					.msg("确认完成");
																			$(
																					"#jxsc")
																					.hide();
																			$(
																					"#chongfu")
																					.hide();
																			$(
																					"#bhgzz")
																					.hide();
//																			$("#maincontentframe",
//																					window.parent.document)[0].src = window.ctx
//																					+ "/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id="
//																					+ cellPassage_id
//																					+ "&orderNum=" + stepNum;
																			$("#maincontentframe",
																					window.parent.document)[0].src = window.ctx
																					+ "/experiment/cell/passage/cellPassage/showCellPassageResultTable.action?id="
																					+ cellPassage_id
																					+ "&orderNum=" + stepNum;
																			        +"&bpmTaskId="
																			panduan = true;
																			// noBlendTab.ajax.reload();
																		} else {
																			panduan = true;
																		}
																	}
																})
													}
												} else {
													top.layer.msg(message);
													panduan = true;
												}
											} else {
												top.layer.msg(message);
												panduan = true;
											}
										} else {
											top.layer.msg("回输计划不通过请确认回输计划！");
											panduan = true;
										}
									}
									top.layer.close(indexz);
								}, function(indexzz) {
									panduan = true;
									top.layer.close(indexzz);
								});
			}

		}
	} else {

	}

}
var panduan1 = true;
// 不合格样本完成
function finishStepFailed() {
	if (panduan1) {
		panduan1 = false;
		top.layer
				.confirm(
						"确认不合格终止？",
						{
							icon : 3,
							title : biolims.common.prompt,
							btn : [ biolims.common.selected, "取消" ],
							cancel : function(indexz, layero) {
								panduan1 = true;
								top.layer.close(indexz);
							}
						},
						function(indexz) {
							var id = "";
							var ids = [];
							var blend = false;
							// 是否可以收获
							var harvest = false;
							var stepNum = $(".wizard_steps .selected .step_no")
									.text(); // 当前得步骤数
							var blendLength = $("#blendTable .dataTables_empty").length;
							$
									.ajax({
										type : 'post',
										async : false,
										url : ctx
												+ '/experiment/cell/passage/cellPassage/findBlendState.action',
										data : {
											mainId : cellPassage_id,
											stepNum : stepNum,
										},
										success : function(data) {
											var data = JSON.parse(data);
											var bl = data.ti.blend;
											if (bl == "1") {
												blend = true;
											}
											// if(data.harvest) {
											// harvest = true;
											// } else {
											// harvest = false;
											// }
										}
									})
							// if(harvest) {
							if (blend == true) {// }&& blendLength == 0) {
								// //说明此步混合了
								// id =
								// $("#blendTable").find("tbody").children("tr").find("input[type='checkbox']").val();
								$
										.ajax({
											type : 'post',
											url : ctx
													+ '/experiment/cell/passage/cellPassage/finishPresentStepFailed.action',
											data : {
												mainId : cellPassage_id,
												id : id,
												ids : ids,
												hhzt : "1",
												stepNum : stepNum,
												startTime : $("#startTime")
														.text(),
												endTime : $("#endTime").text()
											},
											success : function(data) {
												var data = JSON.parse(data);
												if (data.success) {
													top.layer.msg("确认完成");
													$(
															"#maincontentframe",
															window.parent.document)[0].src = window.ctx
															+ "/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id="
															+ cellPassage_id
															+ "&orderNum="
															+ stepNum;
													panduan1 = true;
													// noBlendTab.ajax.reload();
												} else {
													panduan1 = true;
												}
											}
										});
							} else {
								// var rows =
								// $("#noBlendTable").find("tbody").children("tr");
								// $.each(rows, function(ii, vv){
								// var id =
								// $(vv).find("input[type='checkbox']").val();
								// ids.push(id);
								// });
								$
										.ajax({
											type : 'post',
											url : ctx
													+ '/experiment/cell/passage/cellPassage/finishPresentStepFailed.action',
											data : {
												mainId : cellPassage_id,
												id : id,
												ids : ids,
												hhzt : "0",
												stepNum : stepNum,
												startTime : $("#startTime")
														.text(),
												endTime : $("#endTime").text()
											},
											success : function(data) {
												var data = JSON.parse(data);
												if (data.success) {
													top.layer.msg("确认完成");
													$(
															"#maincontentframe",
															window.parent.document)[0].src = window.ctx
															+ "/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id="
															+ cellPassage_id
															+ "&orderNum="
															+ stepNum;
													panduan1 = true;
													// noBlendTab.ajax.reload();
												} else {
													panduan1 = true;
												}
											}
										})
							}
							// } else {
							// top.layer.msg("回输计划不通过请确认回输计划！");
							// panduan1 = true;
							// }
							top.layer.close(indexz);
						});

	}
}
//渲染房间确认记录
function renderRoomconfirm(cellRoomPass,room1state,room2state
		,room3state,room4state,room5state,room6state){
	
	
	$("#enmon1").removeClass("btn-danger");
	$("#enmon1").removeClass("btn-success");
	$("#enmon2").removeClass("btn-danger");
	$("#enmon2").removeClass("btn-success");
	$("#enmon3").removeClass("btn-danger");
	$("#enmon3").removeClass("btn-success");
	$("#enmon4").removeClass("btn-danger");
	$("#enmon4").removeClass("btn-success");
	$("#enmon5").removeClass("btn-danger");
	$("#enmon5").removeClass("btn-success");
	$("#enmon6").removeClass("btn-danger");
	$("#enmon6").removeClass("btn-success");
	
	if(room1state){
		$("#enmon1").addClass("btn-success");
	}else{
		$("#enmon1").addClass("btn-danger");
	}
	
	if(room2state){
		$("#enmon2").addClass("btn-success");
	}else{
		$("#enmon2").addClass("btn-danger");
	}
	
	if(room3state){
		$("#enmon3").addClass("btn-success");
	}else{
		$("#enmon3").addClass("btn-danger");
	}
	
	if(room4state){
		$("#enmon4").addClass("btn-success");
	}else{
		$("#enmon4").addClass("btn-danger");
	}
	
	if(room5state){
		$("#enmon5").addClass("btn-success");
	}else{
		$("#enmon5").addClass("btn-danger");
	}
	
	if(room6state){
		$("#enmon6").addClass("btn-success");
	}else{
		$("#enmon6").addClass("btn-danger");
	}
	
	
	var v1="生产现场不得有与本批生产无关物品。";
	var v2="生产现场有状态标识：包括设备状态标识等。";
	var v3="确认环境温度是否正常：环境温度18.0℃-26.0℃，环境湿度小于75%。";
	var v4="生物安全柜正常开启，设备在校验期内。";
	if (cellRoomPass.length) {
		var trs = "";
		cellRoomPass.forEach(function(v, i) {
			var checked = v.state=="1"?'checked':'';
			var neirong = "";
			if(i==0){
				neirong = v1;
			}else if(i==1){
				neirong = v2;
			}else if(i==2){
				neirong = v3;
			}else if(i==3){
				neirong = v4;
			}
			trs += '<tr class="pageOneRoomConfirm" id="'
				+ v.id
				+ '" " state="'
				+ v.state
				+ '"><td><span class="badge bg-green">'
				+ (i+1)
				+ '</span></td><td class="hh" style="white-space: inherit;word-wrap: break-word;word-break: break-all;">'
				+ neirong
				+ '</td><td><input type="checkbox" class="mySwitch" style="display:none" '
				+ checked
				+ '/></td></tr>';
			
			
//			trs += '<tr class="pageOneQRcodesample" id="'
//				+ v.id
//				+ '" code="'
//				+ v.code
//				+'" state="'
//				+ v.state
//				+ '"><td><span class="badge bg-green">'
//				+ v.code
//				+ '</span></td><td>'
//				+ v.state
//				+'</td></tr>';
			$("#pageOneRoomConfirm").html(trs);
		});
		setTimeout(function(){
			$("#pageOneRoomConfirm .mySwitch").each(function() {
				var state = $(this).is(':checked');
				$(this).bootstrapSwitch({
					onText : "是",
					offText : "否",
					onSwitchChange : function(event, state) {
						if (state == true) {
							$(this).parents("tr").attr("state", "1");
						} else {
							$(this).parents("tr").attr("state", '0');
						}
					}
				}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
			})
		},0.1);
//		var trs = "";
//		trs += '<tr class="pageOneRoomConfirm" id="'
//			+ 1
//			+ '" note="'
//			+ 2
//			+ '" state="'
//			+ 1
//			+ '"><td><span class="badge bg-green">'
//			+ 4
//			+ '</span></td><td class="hh" style="white-space: inherit;word-wrap: break-word;word-break: break-all;">'
//			+ ''
//			+ '</td><td><input type="checkbox" class="mySwitch" style="display:none" '
//			+ 'checked'
//			+ '/></td></tr>'
//			+'<tr class="pageOneRoomConfirm" id="'
//			+ 1
//			+ '" note="'
//			+ 2
//			+ '" state="'
//			+ 1
//			+ '"><td><span class="badge bg-green">'
//			+ 4
//			+ '</span></td><td class="hh" style="white-space: inherit;word-wrap: break-word;word-break: break-all;">'
//			+ 5
//			+ '</td><td><input type="checkbox" class="mySwitch" style="display:none" '
//			+ 'checked'
//			+ '/></td></tr>'
//			+'<tr class="pageOneRoomConfirm" id="'
//			+ 1
//			+ '" note="'
//			+ 2
//			+ '" state="'
//			+ 1
//			+ '"><td><span class="badge bg-green">'
//			+ 4
//			+ '</span></td><td class="hh" style="white-space: inherit;word-wrap: break-word;word-break: break-all;">'
//			+ 5
//			+ '</td><td><input type="checkbox" class="mySwitch" style="display:none" '
//			+ 'checked'
//			+ '/></td></tr>'
//			+'<tr class="pageOneRoomConfirm" id="'
//			+ 1
//			+ '" note="'
//			+ 2
//			+ '" state="'
//			+ 1
//			+ '"><td><span class="badge bg-green">'
//			+ 4
//			+ '</span></td><td class="hh" style="white-space: inherit;word-wrap: break-word;word-break: break-all;">'
//			+ 5
//			+ '</td><td><input type="checkbox" class="mySwitch" style="display:none" '
//			+ 'checked'
//			+ '/></td></tr>';
//		$("#pageOneRoomConfirm").html(trs);
//		setTimeout(function(){
//			$("#pageOneRoomConfirm .mySwitch").each(function() {
//				var state = $(this).is(':checked');
//				$(this).bootstrapSwitch({
//					onText : "是",
//					offText : "否",
//					onSwitchChange : function(event, state) {
//						if (state == true) {
//							$(this).parents("tr").attr("state", "1");
//						} else {
//							$(this).parents("tr").attr("state", '0');
//						}
//					}
//				}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
//			})
//		},0.1);
	}else{
		var trs = "";
		trs = '<tr><td colspan="3" style="text-align: center;">没有数据</td></tr>';
		$("#pageOneRoomConfirm").html(trs);
	}
}
//条形码扫码
function renderBrcodeconfirm(checkCodeBefore, checkCodeAfter){
	
	if (checkCodeBefore.length) {
		var trs = "";
		checkCodeBefore.forEach(function(v, i) {
			trs += '<tr class="pageOneQRcodesample" id="'
				+ v.id
				+ '" code="'
				+ v.code
				+'" state="'
				+ v.state
				+ '"><td><span class="badge bg-green">'
				+ v.code
				+ '</span></td><td>'
				+ v.state
				+'</td></tr>';
			$("#pageOneQRcodesample").html(trs);
		});
	}else{
		var trs = "";
		trs = '<tr><td colspan="2" style="text-align: center;">没有数据</td></tr>';
		$("#pageOneQRcodesample").html(trs);
	}
	
	
	if(checkCodeAfter.length){
		var trs = "";
		checkCodeAfter.forEach(function(v, i) {
			trs += '<tr class="pageOneQRcodeoperation" id="'
				+ v.id
				+ '" code="'
				+ v.code
				+'" state="'
				+ v.state
				+ '"><td><span class="badge bg-green">'
				+ v.code
				+ '</span></td><td>'
				+ v.state
				+'</td></tr>';
		});
		$("#pageOneQRcodeoperation").html(trs);
	}else{
		var trs = "";
		trs = '<tr><td colspan="2" style="text-align: center;">没有数据</td></tr>';
		$("#pageOneQRcodeoperation").html(trs);
	}
	
}
// 渲染操作指令
function renderOperationalOrder(cellPass, stepId) {
	var trs = '';
	if (stepId == "1") {
		if (cellPass.length) {
			
			cellPass
					.forEach(function(v, i) {
//						var zhiling = v.templeInsId;
						var checked = v.productionInspection == "0" ? ''
								: 'checked';
						var note = v.operationNotes ? v.operationNotes : "";
						var wbtn = note ? "btn-warning" : "";
						trs += '<tr class="pageOneOperationalOrder" id="'
								+ v.id
								+ '" note="'
								+ note
								+ '" state="'
								+ v.productionInspection
								+ '"><td><span class="badge bg-green">'
//								+ zhiling.sort
								+ v.sort
								+ '</span></td><td class="hh" style="white-space: inherit;word-wrap: break-word;word-break: break-all;">'
//								+ zhiling.name
								+ v.name
								+ '</td><td><input type="checkbox" class="mySwitch" style="display:none" '
								+ checked
								+ '/></td><td><button class="btn btn-xs '
								+ wbtn
								+ '" onclick="layerNote(this)"> <i class="fa fa-pencil-square-o"></i>备注</button></td></tr>';
					})
					
		} else {
			trs = '<tr><td colspan="4" style="text-align: center;">没有数据</td></tr>';
		}
		$("#pageOneOperationalOrder").html(trs);
		
	} else if (stepId == "4") {
		if (cellPass.length) {
			cellPass.forEach(function(v, i) {
//				var zhiling = v.templeInsId;
				var checked = v.productionInspection == "0" ? '' : 'checked';
				trs += '<tr class="pageFourOperationalOrder" id="' + v.id
						+ '" state="' + v.productionInspection
//						+ '"><td><span class="badge bg-green">' + zhiling.sort
						+ '"><td><span class="badge bg-green">' + v.sort
//						+ '</span></td><td class="hh" style="white-space: inherit;white-space: inherit;word-wrap: break-word;word-break: break-all;">' + zhiling.name
						+ '</span></td><td class="hh" style="white-space: inherit;white-space: inherit;word-wrap: break-word;word-break: break-all;">' + v.name
						+ '</td><td><input type="checkbox" class="mySwitch" style="display:none"'
						+ checked + ' /></td></tr>';
			})
		} else {
			trs = '<tr><td colspan="3" style="text-align: center;">没有数据</td></tr>';
		}
		$("#pageFourOperationalOrder").html(trs);
		
	}

	setTimeout(function(){
		$(".table .mySwitch").each(function() {
			var state = $(this).is(':checked');
			$(this).bootstrapSwitch({
				onText : "是",
				offText : "否",
				onSwitchChange : function(event, state) {
					if (state == true) {
						$(this).parents("tr").attr("state", "1");
					} else {
						$(this).parents("tr").attr("state", '0');
					}
				}
			}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
		})
	},0.1);
}
// 备注信息
function layerNote(that) {
	if ($(that).hasClass("btn-warning")) {
		var txt = $(that).parents("tr").attr("note");
	} else {
		var txt = '';
	}
	top.layer
			.open({
				title : "备注",
				type : 1,
				area : [ "30%", "60%" ],
				btn : biolims.common.selected,
				btnAlign : 'c',
				content : '<textarea id="checkCounts" class="form-control" style="width: 99%;height: 99%;">'
						+ txt + '</textarea>',
				yes : function(index, layero) {
					var content = $("#checkCounts", parent.document).val();
					if (content) {
						$(that).addClass("btn-warning").parents("tr").attr(
								"note", content);
					} else {
						$(that).removeClass("btn-warning").parents("tr").attr(
								"note", "");
					}
					top.layer.close(index);
				},
			});
}
// 渲染生产记录（自定义字段）
//function renderProductionRecordNew(jilu, cellReco, stepId, scjz) {
function renderProductionRecordNew(jilu, cellRecoId,cellRecoContent, stepId, scjz) {
	var ipts = '';
	var content = JSON.parse(jilu);
	content
			.forEach(function(v, i) {
				var txt = "";
				if (v.fieldName == "operatingRoomName") {
					var faIcon = 'fa fa-home';
					if(v.required=="true"){
					ipts += '<div class="form-group"><label class="lab"><i class="'
							+ faIcon
							+ '"></i><img class="requiredimage" src="/images/required.gif"><label class="labTwo">'
							+ v.label
//							+ '<button class="btn btn-warning btn-xs" onclick="selectRoom();">+选择</button></label>'
							+ '</label></label>'
							+ '<input type="hidden" id="operatingRoomId" class="form-control" changelog="" value=""/> <input type="'
							+ v.type + '" id="' + v.fieldName
							+ '" class="form-control" changelog="" value="' + txt
							+ '" readonly="readyOnly" /><span class="input-group-btn"></div>';
					return true;
					}else{
						ipts += '<div class="form-group"><label><i class="'
							+ faIcon
							+ '"></i> '
							+ v.label
//							+ '<button class="btn btn-warning btn-xs" onclick="selectRoom();">+选择</button></label>'
							+ '</label>'
							+ '<input type="hidden" id="operatingRoomId" class="form-control" changelog="" value=""/> <input type="'
							+ v.type + '" id="' + v.fieldName
							+ '" class="form-control" changelog="" value="' + txt
							+ '" readonly="readyOnly" /><span class="input-group-btn"></div>';
						return true;
					}
				} else if (v.fieldName == "templeOperator") {
					var faIcon = 'fa fa-male';
					// txt = window.userName;\
					if(v.required=="true"){
					
					ipts += '<div class="form-group"><label class="lab"><i class="'
							+ faIcon
							+ '"></i> <img class="requiredimage" src="/images/required.gif"><label class="labTwo"> '
							+ v.label
							+ '</label> </label><input type="hidden" id="'
//							+ '<button class="btn btn-warning btn-xs" onclick="selectUserTo();">+选择</button></label> <input type="hidden" id="'
							+ v.fieldName + '" class="form-control" changelog="" value="'
							+ txt + '"/> <input type="' + v.type + '" id="'
							+ v.fieldName
							+ '-name" class="form-control" changelog="" value="' + txt
							+ '" readOnly="readyOnly" /></div>';
					return true;
					
					}else{
						ipts += '<div class="form-group"><label><i class="'
							+ faIcon
							+ '"></i> '
							+ v.label
							+ '</label> <input type="hidden" id="'
//							+ '<button class="btn btn-warning btn-xs" onclick="selectUserTo();">+选择</button></label> <input type="hidden" id="'
							+ v.fieldName + '" class="form-control" changelog="" value="'
							+ txt + '"/> <input type="' + v.type + '" id="'
							+ v.fieldName
							+ '-name" class="form-control" changelog="" value="' + txt
							+ '" readOnly="readyOnly" /></div>';
					return true;
					
					}
				} else if (v.fieldName == "templeReviewer") {
					var faIcon = 'fa fa-user';
					  if(v.required=="true"){
					
					ipts += '<div class="form-group"><label class="lab" ><i class="'
							+ faIcon
							+ '"></i> <img class="requiredimage" src="/images/required.gif"><label class="labTwo">'
							+ v.label
//							+ '<button class="btn btn-warning btn-xs" onclick="selectUserTr();">+选择</button></label> <input type="hidden" id="'
							+ '</label><button class="btn btn-warning btn-xs" onclick="checkU();">复核</button></label> <input type="hidden" id="'
							+ v.fieldName + '" class="form-control" changelog="" value="'
							+ txt + '"/> <input type="' + v.type + '" id="'
							+ v.fieldName
							+ '-name" class="form-control" changelog="" value="' + txt
							+ '" readOnly="readyOnly" /></div>';
					  
					  }else{
							ipts += '<div class="form-group"><label ><i class="'
								+ faIcon
								+ '"></i>'
								+ v.label
//								+ '<button class="btn btn-warning btn-xs" onclick="selectUserTr();">+选择</button></label> <input type="hidden" id="'
								+ '<button class="btn btn-warning btn-xs" onclick="checkU();">复核</button></label> <input type="hidden" id="'
								+ v.fieldName + '" class="form-control" changelog="" value="'
								+ txt + '"/> <input type="' + v.type + '" id="'
								+ v.fieldName
								+ '-name" class="form-control" changelog="" value="' + txt
								+ '" readOnly="readyOnly" /></div>';
					  }
					return true;
				} else if (v.fieldName == "qaTempleReviewer") {
					var faIcon = 'fa fa-user';
					  if(v.required=="true"){
					
					ipts += '<div class="form-group"><label class="lab"><i class="'
							+ faIcon
							+ '"></i><img class="requiredimage" src="/images/required.gif"><label class="labTwo">  '
							+ v.label
							+ '</label><button class="btn btn-warning btn-xs" onclick="selectUserQaTr();">+选择</button></label> <input type="hidden" id="'
							+ v.fieldName + '" class="form-control" changelog="" value="'
							+ txt + '"/> <input type="' + v.type + '" id="'
							+ v.fieldName
							+ '-name" class="form-control" changelog="" value="' + txt
							+ '" readOnly="readyOnly" /></div>';
					return true;
					  
					  }else{
						  ipts += '<div class="form-group"><label><i class="'
								+ faIcon
								+ '"></i> '
								+ v.label
								+ '<button class="btn btn-warning btn-xs" onclick="selectUserQaTr();">+选择</button></label> <input type="hidden" id="'
								+ v.fieldName + '" class="form-control" changelog="" value="'
								+ txt + '"/> <input type="' + v.type + '" id="'
								+ v.fieldName
								+ '-name" class="form-control" changelog="" value="' + txt
								+ '" readOnly="readyOnly" /></div>';
					  }
				}else if (v.fieldName == "ambientTemperature") {
				     var faIcon = 'fa fa-black-tie';
				     var defaultValue = v.defaultValue ? v.defaultValue : '27.0';
				     if(v.required=="true"){
				     ipts += '<div class="form-group"><label class="lab"><i class="'
				       + faIcon
				       + '"></i> <img class="requiredimage" src="/images/required.gif"><label class="labTwo">  '
				       + v.label
				       + '</label></label> <input type="text" id="'
				       + v.fieldName 
				       + '" class="slider form-control" data-slider-min="-20.0" data-slider-max="80.0" data-slider-step="0.1" data-slider-value="'
				       + defaultValue
				       + '" data-slider-tooltip="always" data-slider-id="green"><input class="form-control" type="text" id="ambientTemperatureTem" value="'+defaultValue+'"></div>';
				     return true;
					 
				     }else{
				    	  ipts += '<div class="form-group"><label><i class="'
						       + faIcon
						       + '"></i> '
						       + v.label
						       + '</label> <input type="text" id="'
						       + v.fieldName 
						       + '" class="slider form-control" data-slider-min="-20.0" data-slider-max="80.0" data-slider-step="0.1" data-slider-value="'
						       + defaultValue
						       + '" data-slider-tooltip="always" data-slider-id="green"><input class="form-control" type="text" id="ambientTemperatureTem" value="'+defaultValue+'"></div>';
						     return true;
							 
				     }
				    } else if (v.fieldName == "ambientHumidity") {
				     var faIcon = 'fa fa-flask';
				     var defaultValue = v.defaultValue ? v.defaultValue : '37.0';
				     if(v.required=="true"){
				     ipts += '<div class="form-group"><label class="lab"><i class="'
				       + faIcon
				       + '"></i> <img class="requiredimage" src="/images/required.gif"> <label class="labTwo">  '
				       + v.label
				       + '</label></label> <input type="text" id="'
				       + v.fieldName
				       + '" class="slider form-control" data-slider-min="0.0" data-slider-max="100.0" data-slider-step="0.1" data-slider-value="'
				       + defaultValue
				       + '" data-slider-tooltip="always" data-slider-id="purple"><input class="form-control" type="text" id="ambientHumidityHum" value="'+defaultValue+'"></div>';
				     return true;
				     }else{
				    	 ipts += '<div class="form-group"><label><i class="'
						       + faIcon
						       + '"></i> '
						       + v.label
						       + '</label> <input type="text" id="'
						       + v.fieldName
						       + '" class="slider form-control" data-slider-min="0.0" data-slider-max="100.0" data-slider-step="0.1" data-slider-value="'
						       + defaultValue
						       + '" data-slider-tooltip="always" data-slider-id="purple"><input class="form-control" type="text" id="ambientHumidityHum" value="'+defaultValue+'"></div>'; 
				    	  return true;
				     }
				     //消毒剂
				    } else if (v.fieldName == "disinfectant") {
					var faIcon = 'fa fa-home';
					if(v.required=="true"){
					
					ipts += '<div class="form-group"><label class="lab"><i class="'
						+ faIcon
						+ '"></i> <img class="requiredimage" src="/images/required.gif"><label class="labTwo">   '
						+ v.label
						+ '</label><button class="btn btn-warning btn-xs" onclick="selectDisinFectant();">+选择</button></label> <input type="'
						+ v.type + '" id="' + v.fieldName
						+ '" class="form-control" changelog=""  value="' + txt
						+ '"/><span class="input-group-btn"></div>';
//					ipts += '<div class="form-group"><label><i class="'
//							+ faIcon
//							+ '"></i> '
//							+ v.label
//							+ '</label> <input type="'
//							+ v.type + '" id="' + v.fieldName
//							+ '" class="form-control" value="' + txt
//							+ '"/><span class="input-group-btn"></div>';
					return true;
					
					}else{
						ipts += '<div class="form-group"><label ><i class="'
							+ faIcon
							+ '"></i> '
							+ v.label
							+ '<button class="btn btn-warning btn-xs" onclick="selectDisinFectant();">+选择</button></label> <input type="'
							+ v.type + '" id="' + v.fieldName
							+ '" class="form-control" changelog=""  value="' + txt
							+ '"/><span class="input-group-btn"></div>';
						return true;
						
					}
					//紫外线
				} else if (v.fieldName == "ultravioletlamptime") {
					var faIcon = 'fa fa-home';
					if(v.required=="true"){
					
					ipts += '<div class="form-group"><label class="lab"><i class="'
							+ faIcon
							+ '"></i> <img class="requiredimage" src="/images/required.gif"><label class="labTwo">'
							+ v.label
							+ '</label><button class="btn btn-warning btn-xs" onclick="xzTime();">选择时间</button></label> <input type="'
							+ v.type + '" id="' + v.fieldName
							+ '" class="form-control" changelog="" value="' + txt
							+ '"/><span class="input-group-btn"></div>';
					return true;
					}else{
						ipts += '<div class="form-group"><label><i class="'
							+ faIcon
							+ '"></i> '
							+ v.label
							+ '<button class="btn btn-warning btn-xs" onclick="xzTime();">选择时间</button></label> <input type="'
							+ v.type + '" id="' + v.fieldName
							+ '" class="form-control" changelog="" value="' + txt
							+ '"/><span class="input-group-btn"></div>';
					return true;
					}
				}
				if(v.required=="true"){
					ipts += '<div class="form-group"><label class="lab"><i class="' + faIcon
					+ '"></i><img class="requiredimage" src="/images/required.gif"> ' + v.label + '</label> <input type="'
					+ v.type + '" id="' + v.fieldName
					+ '" class="form-control" changelog="" value="' + txt + '"/></div>';
				}else{
					ipts += '<div class="form-group"><label><i class="' + faIcon
					+ '"></i> ' + v.label + '</label> <input type="'
					+ v.type + '" id="' + v.fieldName
					+ '" class="form-control" changelog="" value="' + txt + '"/></div>';
				}
				
			});
	if (stepId == "1") {
//		$("#pageOneProductionRecord").html(ipts).attr("pid", cellReco.id);
		$("#pageOneProductionRecord").html(ipts).attr("pid", cellRecoId);
	} else if (stepId == "4") {
//		$("#pageFourProductionRecord").html(ipts).attr("pid", cellReco.id);
		$("#pageFourProductionRecord").html(ipts).attr("pid", cellRecoId);
	}
//	var target = $("div[pid=" + cellReco.id + "]");
	var target = $("div[pid=" + cellRecoId + "]");
	// var scjzSJ = JSON.parse(scjz);
	for ( var k in scjz) {
		if (k == "templeOperator") {
			txt = scjz[k] ? scjz[k] : "";
			$("#templeOperator").val(txt);
		} else if (k == "templeOperator-name") {
			txt = scjz[k] ? scjz[k] : "";
			$("#templeOperator-name").val(txt);
		} else if (k == "templeReviewer") {
			txt = scjz[k] ? scjz[k] : "";
			$("#templeReviewer").val(txt);
		} else if (k == "qaTempleReviewer") {
			txt = scjz[k] ? scjz[k] : "";
			$("#qaTempleReviewer").val(txt);
		} else if (k == "qaTempleReviewer-name") {
			txt = scjz[k] ? scjz[k] : "";
			$("#qaTempleReviewer-name").val(txt);
		} else if (k == "templeReviewer-name") {
			txt = scjz[k] ? scjz[k] : "";
			$("#templeReviewer-name").val(txt);
		} else if (k == "disinfectant") {
			txt = scjz[k] ? scjz[k] : "";
			$("#disinfectant").val(txt);
		} else if (k == "ultravioletlamptime") {
			txt = scjz[k] ? scjz[k] : "";
			$("#ultravioletlamptime").val(txt);
		} else if (k == "operatingRoomName") {
			txt = scjz[k] ? scjz[k] : "";
			$("#operatingRoomName").val(txt);
		} else if (k == "operatingRoomId") {
			txt = scjz[k] ? scjz[k] : "";
			$("#operatingRoomId").val(txt);
		}
	}
//	var content = JSON.parse(cellReco.content);
	var content = JSON.parse(cellRecoContent);
	for ( var k in content) {
		if (k == "ambientHumidity" || k == "ambientTemperature") {
			target.find("#" + k).attr("data-slider-value", content[k]);
		} else if (k == "templeOperator" || k == "templeOperator-name") {
			// txt = content[k] ? content[k] : window.userName;
			// target.find("#templeOperator").val(txt);
		} else if (k == "templeReviewer" || k == "templeReviewer-name") {
			// txt = content[k] ? content[k] : window.userName;
			// target.find("#templeOperator").val(txt);
		} else if (k == "qaTempleReviewer" || k == "qaTempleReviewer-name") {
			// txt = content[k] ? content[k] : window.userName;
			// target.find("#templeOperator").val(txt);
		} else if (k == "operatingRoomName") {
//			txt = content[k] ? content[k] : "";
//			target.find("#operatingRoomName").val(txt);
		} else {
			target.find("#" + k).val(content[k]);
		}
	}
	$('.slider').slider();
	 $("#ambientTemperature").on("slide", function(slideEvt) {
		  $("#ambientTemperatureTem").val(slideEvt.value);
		 });
		 $("#ambientTemperature").slider().on("change", function(slideEvt) {
		  $("#ambientTemperatureTem").val($("#ambientTemperature").slider().slider('getValue'));
		 });
		 $("#ambientTemperatureTem").blur(function(){
		  if(!isNaN($("#ambientTemperatureTem").val())){
		   if(-20<=Number($("#ambientTemperatureTem").val())&&Number($("#ambientTemperatureTem").val())<=80){
		    $("#ambientTemperature").slider().slider('setValue', Number($("#ambientTemperatureTem").val()));
		   }else{
			  $("#ambientTemperatureTem").val($("#ambientTemperature").slider().slider('getValue'));
			    top.layer.msg("请输入-20~80之间的数字！");

		   }
		  }else{
			  $("#ambientTemperatureTem").val($("#ambientTemperature").slider().slider('getValue'));
			  $("#ambientTemperatureTem").val($("#ambientTemperature").slider().slider('getValue'));

		  }
		 });
		 $("#ambientHumidity").on("slide", function(slideEvt) {
		  $("#ambientHumidityHum").val(slideEvt.value);
		 });
		 $("#ambientHumidity").slider().on("change", function(slideEvt) {
		  $("#ambientHumidityHum").val($("#ambientHumidity").slider().slider('getValue'));
		 });
		 $("#ambientHumidityHum").blur(function(){
		  if(!isNaN($("#ambientHumidityHum").val())){
		   if(0<=Number($("#ambientHumidityHum").val())&&Number($("#ambientHumidityHum").val())<=100){
		    $("#ambientHumidity").slider().slider('setValue', Number($("#ambientHumidityHum").val()));
		   }else{
			 $("#ambientHumidityHum").val($("#ambientHumidity").slider().slider('getValue'));
		    top.layer.msg("请输入0~100之间的数字！");
		   }
		  }else{
		 $("#ambientHumidityHum").val($("#ambientHumidity").slider().slider('getValue'));

		   top.layer.msg("请输入0~100之间的数字！");
		  }
		 });
	// 日期格式化
	$("#contentData").find('input[type=data]').datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd" // 日期格式，详见
	});

}// 渲染生产记录（自定义字段）
function renderProductionRecord(jilu, cellReco, stepId) {

	var ipts = '';
	var content = JSON.parse(jilu.content);
	content
			.forEach(function(v, i) {
				var txt = "";
				if (v.fieldName == "operatingRoomName") {
					var faIcon = 'fa fa-home';
					ipts += '<div class="form-group"><label><i class="'
							+ faIcon
							+ '"></i> '
							+ v.label
//							+ '<button class="btn btn-warning btn-xs" onclick="selectRoom();">+选择</button></label>'
							+ '</label>'
							+ '<input type="hidden" id="operatingRoomId" class="form-control" value=""/><input type="'
							+ v.type + '" id="' + v.fieldName
							+ '" class="form-control" value="' + txt
							+ '" readonly="readyOnly" /><span class="input-group-btn"></div>';
					return true;
				} else if (v.fieldName == "templeOperator") {
					var faIcon = 'fa fa-male';
					// txt = window.userName;
					ipts += '<div class="form-group"><label><i class="'
							+ faIcon + '"></i> ' + v.label
							+ '</label> <input type="hidden" id="'
							+ v.fieldName + '" class="form-control" value="'
							+ txt + '"/> <input type="' + v.type + '" id="'
							+ v.fieldName
							+ '-name" class="form-control" value="' + txt
							+ '"/></div>';
				} else if (v.fieldName == "templeReviewer") {
					var faIcon = 'fa fa-user';
					ipts += '<div class="form-group"><label><i class="'
							+ faIcon + '"></i> ' + v.label
							+ '</label> <input type="' + v.type + '" id="'
							+ v.fieldName + '" class="form-control" value="'
							+ txt + '"/></div>';
				} else if (v.fieldName == "qaTempleReviewer") {
					var faIcon = 'fa fa-user';
					ipts += '<div class="form-group"><label><i class="'
							+ faIcon + '"></i> ' + v.label
							+ '</label> <input type="' + v.type + '" id="'
							+ v.fieldName + '" class="form-control" value="'
							+ txt + '"/></div>';
				} else if (v.fieldName == "ambientTemperature") {
					var faIcon = 'fa fa-black-tie';
					var defaultValue = v.defaultValue ? v.defaultValue : '27.0';
					ipts += '<div class="form-group"><label><i class="'
							+ faIcon
							+ '"></i> '
							+ v.label
							+ '</label> <input type="text" id="'
							+ v.fieldName
							+ '" class="slider form-control" data-slider-min="-20.0" data-slider-max="80.0" data-slider-step="0.1" data-slider-value="'
							+ defaultValue
							+ '" data-slider-tooltip="always" data-slider-id="green"></div>';
					return true;
				} else if (v.fieldName == "ambientHumidity") {
					var faIcon = 'fa fa-flask';
					var defaultValue = v.defaultValue ? v.defaultValue : '37.0';
					ipts += '<div class="form-group"><label><i class="'
							+ faIcon
							+ '"></i> '
							+ v.label
							+ '</label> <input type="text" id="'
							+ v.fieldName
							+ '" class="slider form-control" data-slider-min="0.0" data-slider-max="100.0" data-slider-step="0.1" data-slider-value="'
							+ defaultValue
							+ '" data-slider-tooltip="always" data-slider-id="purple"></div>';
					return true;
				}
				ipts += '<div class="form-group"><label><i class="' + faIcon
						+ '"></i> ' + v.label + '</label> <input type="'
						+ v.type + '" id="' + v.fieldName
						+ '" class="form-control" value="' + txt + '"/></div>';
			});
	if (stepId == "1") {
		$("#pageOneProductionRecord").html(ipts).attr("pid", cellReco.id);
	} else if (stepId == "4") {
		$("#pageFourProductionRecord").html(ipts).attr("pid", cellReco.id);
	}
	var target = $("div[pid=" + cellReco.id + "]");
	// var content = JSON.parse(cellReco.content);
	// for(var k in content) {
	// if(k == "ambientHumidity" || k == "ambientTemperature") {
	// target.find("#" + k).attr("data-slider-value", content[k]);
	// } else if(k == "templeOperator") {
	// txt = content[k] ? content[k] : window.userName;
	// target.find("#templeOperator").val(txt);
	// } else if(k == "operatingRoomName") {
	// txt = content[k] ? content[k] : "";
	// target.find("#operatingRoomName").val(txt);
	// } else {
	// target.find("#" + k).val(content[k]);
	// }
	// }
	$('.slider').slider();
	// 日期格式化
	$("#contentData").find('input[type=data]').datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd" // 日期格式，详见
	});

}
//选择消毒剂
function selectDisinFectant() {
	top.layer.open({
		title: "消毒剂",
		type: 2,
		area: top.screeProportion,
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=disinfectant", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
//			$("#disinfectant").val(id);
			$("#disinfectant").val(name);
		},
	})
}
// 选择房间
function selectRoom() {
	top.layer
			.open({
				title : "选择房间",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/experiment/cell/passage/cellPassage/selectRoomTable.action",
						"" ],
				yes : function(index, layero) {
					// var name = $(".layui-layer-iframe", parent.document)
					// .find("iframe").contents().find("#addRoomTable
					// .chosed").children("td").eq(1).text();
					var name = [];
					var id=[];
					$('.layui-layer-iframe', parent.document).find("iframe")
							.contents().find("#addRoomTable .selected").each(
									function(i, v) {
										// id.push($(v).children("td").eq(2).text());
										name.push($(v).children("td").eq(2)
												.text());
										id.push($(v).children("td").eq(1)
												.text());
									})
					if (name.length > 1) {
						top.layer.msg("请选择一条数据！");
						return false;
					} else {
						top.layer.close(index);
						$("#operatingRoomName").val(name);
						$("#operatingRoomId").val(id);
					}
				},
			});
}
// 选择操作人
function selectUserTo() {
	top.layer.open({
		title : "选择操作人",
		type : 2,
		area: top.screeProportion,
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=SC002",
				"" ],
		yes : function(index, layero) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#templeOperator").val(id);
			$("#templeOperator-name").val(name);
		},
	});
}
// 选择审核人
function selectUserTr() {
	top.layer.open({
		title : "选择审核人",
		type : 2,
		area: top.screeProportion,
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=SC002",
				"" ],
		yes : function(index, layero) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#templeReviewer").val(id);
			$("#templeReviewer-name").val(name);
		},
	});
}

//复核人复核
function checkU() {
	layer.open({
		  type: 1,
		  title: "复核人验证",
		  closeBtn: 1,
		  offset: '100px',
		  shadeClose: true,
		  skin: 'yourclass',
		  btn: ['验证'],
		  content: `
		            <div class="form-group has-feedback">
						<input type="text" class="form-control name" placeholder="Username" required autofocus>
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" class="form-control psd" placeholder="Password" required>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="col-xs-12">
					<button class="btn btn-primary btn-block" id="checkU">验证</button>
					<script>
					$("#checkU").click(function() {
		        	var user = $(".name").val();
					var pad = $(".psd").val();
		 $.ajax({
			type: "post",
			url: ctx+"/main/checkU.action",
			data: {
				user: user,
				pad: pad,
			},
			datatype: "json",
			success: function(data) {
				var obj = JSON.parse(data);
				if(obj["success"] == 1) {
				top.layer.msg("验证成功！");
			    $("#templeReviewer-name").val(obj.name);
			     $("#templeReviewer").val(obj.id);
			    layer.closeAll(); //疯狂模式，关闭所有层
				} else{ 
				top.layer.msg("验证失败！");
				} 
			}
		});
		        });
					
				</script>
				</div>` ,
		 success: function(layero){
		        var btn = layero.find('.layui-layer-btn');
		        btn.find('.layui-layer-btn0').remove()
      }
		}); 
}




// 选择QA复核人
function selectUserQaTr() {
	top.layer.open({
		title : "选择QA复核人",
		type : 2,
		area: top.screeProportion,
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=QA005",
				"" ],
		yes : function(index, layero) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#qaTempleReviewer").val(id);
			$("#qaTempleReviewer-name").val(name);
		},
	});
}
// 核对编码
function checkCode() {
	top.layer
			.open({
				title : biolims.common.checkCode,
				type : 1,
				area : [ "30%", "60%" ],
				btn : biolims.common.selected,
				btnAlign : 'c',
				content : '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
				yes : function(index, layero) {
					var arr = $("#checkCode", parent.document).val()
							.split("\n");
					noBlendTab.settings()[0].ajax.data = {
						"codes" : arr
					};
					noBlendTab.ajax.reload();
					top.layer.close(index);
				},
			});

}

// 渲染样本的datatables
function showSampleTableNoBlend(eletable, stepNum) {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code,
		"createdCell" : function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data" : "batch",
		"title" : "产品批号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "batch");
		}
	})
	colOpts.push({
		"data" : "patientName",
		"title" : "姓名",
		"createdCell" : function(td) {
			$(td).attr("saveName", "patientName");
		}
	})
	colOpts.push({
		"data" : "sampleOrder-filtrateCode",
		"title" : "筛选号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-filtrateCode");
		}
	})
	colOpts.push({
		"data" : "sampleType",
		"title" : "样本名称",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleType");
		}
	})
	colOpts.push({
		"data" : "countsOld",
		"title" : "二氧化碳培养箱编号(旧)",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "countsOld");
			$(td).attr("instrumentOld-id", rowData['instrumentOld-id']);
			$(td).attr("instrumentOld-storageContainer-rowNum",
					rowData['instrumentOld-storageContainer-rowNum']);
			$(td).attr("instrumentOld-storageContainer-colNum",
					rowData['instrumentOld-storageContainer-colNum']);
		}
	})
	colOpts.push({
		"data" : "posIdOld",
		"title" : "储位(旧)",
		"createdCell" : function(td) {
			$(td).attr("saveName", "posIdOld");
		}
	});
	colOpts.push({
		"data" : "counts",
		"title" : "二氧化碳培养箱编号(新)",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "counts");
			$(td).attr("instrument-id", rowData['instrument-id']);
			$(td).attr("instrument-storageContainer-rowNum",
					rowData['instrument-storageContainer-rowNum']);
			$(td).attr("instrument-storageContainer-colNum",
					rowData['instrument-storageContainer-colNum']);
		}
	})
	colOpts.push({
		"data" : "posId",
		"title" : "储位(新)",
		"createdCell" : function(td) {
			$(td).attr("saveName", "posId");
		}
	});
	colOpts.push({
		"data" : "inventoryStatus",
		"title" : "是否出库",
		"createdCell" : function(td) {
			$(td).attr("saveName", "inventoryStatus");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return biolims.common.yes;
			} else if (data == "0") {
				return biolims.common.no;
			} else {
				return "";
			}
		}
	});
	// colOpts.push({
	// "data": "qualified",
	// "title": biolims.common.isQualified,
	// // "className": "select",
	// "name": biolims.common.qualified + "|" + biolims.common.disqualified +
	// "|" +
	// "警告",
	// "createdCell": function(td) {
	// $(td).attr("saveName", "qualified");
	// $(td).attr("selectOpt",
	// biolims.common.qualified + "|" + biolims.common.disqualified + "|" +
	// "警告");
	// },
	// "render": function(data, type, full, meta) {
	// if(data == "2") {
	// return biolims.common.qualified;
	// }
	// if(data == "1") {
	// return biolims.common.disqualified;
	// }
	// if(data == "3") {
	// return "警告";
	// } else {
	// return '';
	// }
	// }
	// })
	var tbarOpts = [];
	// tbarOpts.push({
	// text: "核对编号",
	// visible : false,
	// className: "btn-sm btn-success",
	// action: function() {
	// checkCode();
	// }
	// })
	// tbarOpts.push({
	// text: "核对编号",
	// className: "btn-sm btn-success",
	// action: function() {
	// checkCode();
	// }
	// })
	// tbarOpts.push({
	// text : "回输",
	// className : "btn-sm btn-success hide feedBack",
	// action : function(){
	// feedBack();
	// }
	// })
	if (baocun) {
		if (caozuo) {
			tbarOpts.push({
				text : "生成结果",// "混合样本",
				className : "btn-sm btn-success hide blendSample",
				action : function() {
					blendSample();
				}
			})
			// tbarOpts.push({
			// text: "取出样本",
			// className: "btn-sm btn-success hide blendSample",
			// action: function() {
			// getblendSample();
			// }
			// })
		} else {

		}
	} else {

	}
	// 提交质检按钮 成功后直接将数据带去质检模块

	// tbarOpts.push({
	// text: '<i class="glyphicon glyphicon-upload"></i>' +"选择质检项"
	// /* biolims.common.submissionQuality */,
	// action: function() {
	// var rows = $("#noBlendTable .selected");
	// if(!rows.length) {
	// top.layer.msg(biolims.common.pleaseSelectData);
	// return false;
	// }
	// var idList = [];
	// rows.each(function(i, v) {
	// idList.push($(v).find(".icheck").val());
	// });
	// // 拼弹出的质检项目
	// var bodyZhijian = '';
	// $("#zhijianBody .zhijianli").each(function(i, v) {
	// bodyZhijian += '<tr>' +
	// '<td>' + $(v).find(".zhijianName").text() + '</td>' +
	// '<td><input type="checkbox" class="icheckzj" value="' +
	// $(v).find(".zhijianName").attr("zhijianid") + '" /></td>' +
	// '<td><input type="text" style="" /></td>' +
	// '<td><input type="button" sampleTypeid="" value="选择样本类型"
	// class="form-control btn-success" onclick="xuanze(this)"/></td>' +
	// '<td><input type="text" style="" /></td>' +
	// '<td><input type="text" style="" /></td>' +
	// '</tr>';
	// });
	// var cheboxZhijian = '<table class="table">' +
	// '<tr>' +
	// '<th>检测项</th>' +
	// '<th>选择</th>' +
	// '<th>样本量</th>' +
	// '<th>样本类型</th>' +
	// '<th>备注</th>' +
	// '<th>单位</th>' +
	// '</tr>' + bodyZhijian +
	// '</table>';
	// var chosedID = [];
	// layer.open({
	// title: "选择质检项目",
	// offset: '80px',
	// type: 1,
	// area: ["90%", "70%"],
	// btn: biolims.common.selected,
	// btnAlign: 'c',
	// content: cheboxZhijian,
	// success: function() {
	// $('.icheckzj').iCheck({
	// checkboxClass: 'icheckbox_square-blue',
	// increaseArea: '20%' // optional
	// }).on('ifChanged', function(event) {
	// // var arr=[];
	// // $("#pageOneChoseYQ .btn-info").each(function (i,v) {
	// // var jsonn={};
	// // jsonn.id=v.getAttribute("cosid");
	// // jsonn.code=v.getAttribute("code");
	// // jsonn.name=v.getAttribute("name");
	// // arr.push(jsonn);
	// // });
	// // $.ajax({
	// // type:"post",
	// // url:"/experiment/cell/passage/cellPassage/saveCosPrepare.action",
	// // data:{data:JSON.stringify(arr)},
	//						
	//						
	// if($(this).is(':checked')) {
	// chosedID.push(this.value);
	// } else {
	// var value = this.value;
	// chosedID = chosedID.filter(function(v) {
	// return v != value;
	// })
	// }
	// });
	// },
	// yes: function(index, layero) {
	// if(chosedID.length>0){
	// var arr=[];
	// var panduanlx = true;
	// for(var j = 0,len = chosedID.length; j < len; j++){
	// var jsonn={};
	// jsonn.jcxmc=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().prev().text();
	// jsonn.jcxid=chosedID[j];
	// jsonn.jcsl=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().next().find("input[type='text']").val();
	// if(jsonn.jcsl==null
	// ||jsonn.jcsl==""
	// ||isNaN(Number(jsonn.jcsl))){
	// top.layer.msg("检测数量填写数字");
	// panduanlx = false;
	// }
	// jsonn.jcxlxmc=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().next().next().find("input[type='button']").val();
	// jsonn.jcxlx=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().next().next().find("input[type='button']").attr("sampletypeid");
	// jsonn.jcbz=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().next().next().next().find("input[type='text']").val();
	// if(jsonn.jcxlx==null
	// ||jsonn.jcxlx==""
	// ||jsonn.jclxmc=="选择样本类型"){
	// top.layer.msg("请选择质检样本类型");
	// panduanlx = false;
	// }
	// arr.push(jsonn);
	// //
	// }
	// if(panduanlx){
	// $.ajax({
	// type: "post",
	// url: ctx +
	// "/experiment/cell/passage/cellPassage/submitToQualityTestType.action",
	// data: {
	// idList: idList,
	// chosedID: chosedID,
	// mark: "CellPassage",
	// stepNum: $(".wizard_steps .selected").children(".step_no").text(),
	// id: cellPassage_id,
	// zj: $("#zjType").attr("zjId"),
	// data:JSON.stringify(arr)
	// },
	// success: function(res) {
	// var data = JSON.parse(res);
	// if(data.success) {
	// // top.layer.msg(biolims.common.submissionofSuccess);
	// top.layer.msg("选择成功，请在质检明细中提交质检！");
	// zhijianDetails.ajax.reload();
	// } else {
	// top.layer.msg("选择失败，请重新选择！");
	// // top.layer.msg(biolims.common.submissionFailure);
	// }
	// }
	// });
	// layer.close(index);
	// }else{
	//							
	// }
	//						
	// }else{
	// top.layer.msg("请选择质检项");
	// }
	// },
	// });
	//
	// // $.ajax({
	// // type: "post",
	// // data: {
	// // stepNum: $(".wizard_steps .selected")
	// // .children(".step_no").text(),
	// // idList: idList,
	// // id: cellPassage_id,
	// // zj: $("#zjType").attr("zjId"),
	// // mark: "CellPassage"
	// // },
	// // url: ctx +
	// // "/experiment/cell/passage/cellPassage/submitToQualityTest.action",
	// // success: function(data) {
	// // var data = JSON.parse(data);
	// // if(data.success) {
	// // top.layer
	// // .msg(biolims.common.submissionofSuccess);
	// //
	// // } else {
	// // top.layer
	// // .msg(biolims.common.submissionFailure);
	// // }
	// //
	// // }
	// // });
	// }
	// });

	// tbarOpts.push({
	// text: '<i class="glyphicon glyphicon-upload"></i>' +"选择质检项"
	// /* biolims.common.submissionQuality */,
	// action: function() {
	// var rows = $("#noBlendTable .selected");
	// if(!rows.length) {
	// top.layer.msg(biolims.common.pleaseSelectData);
	// return false;
	// }
	// var idList = [];
	// rows.each(function(i, v) {
	// idList.push($(v).find(".icheck").val());
	// });
	// // 拼弹出的质检项目
	// var bodyZhijian = '';
	// $("#zhijianBody .zhijianli").each(function(i, v) {
	// bodyZhijian += '<tr>' +
	// '<td>' + $(v).find(".zhijianName").text() + '</td>' +
	// '<td><input type="checkbox" class="icheckzj" value="' +
	// $(v).find(".zhijianName").attr("zhijianid") + '" /></td>' +
	// '<td><input type="text" style="" /></td>' +
	// '<td><input type="button" sampleTypeid="" value="选择样本类型"
	// class="form-control btn-success" onclick="xuanze(this)"/></td>' +
	// '<td><input type="text" style="" /></td>' +
	// '<td><input type="text" style="" /></td>' +
	// '</tr>';
	// });
	// var cheboxZhijian = '<table class="table">' +
	// '<tr>' +
	// '<th>检测项</th>' +
	// '<th>选择</th>' +
	// '<th>样本量</th>' +
	// '<th>样本类型</th>' +
	// '<th>备注</th>' +
	// '<th>单位</th>' +
	// '</tr>' + bodyZhijian +
	// '</table>';
	// var chosedID = [];
	// layer.open({
	// title: "选择质检项目",
	// offset: '80px',
	// type: 1,
	// area: ["90%", "50%"],
	// btn: biolims.common.selected,
	// btnAlign: 'c',
	// content: cheboxZhijian,
	// success: function() {
	// $('.icheckzj').iCheck({
	// checkboxClass: 'icheckbox_square-blue',
	// increaseArea: '20%' // optional
	// }).on('ifChanged', function(event) {
	// // var arr=[];
	// // $("#pageOneChoseYQ .btn-info").each(function (i,v) {
	// // var jsonn={};
	// // jsonn.id=v.getAttribute("cosid");
	// // jsonn.code=v.getAttribute("code");
	// // jsonn.name=v.getAttribute("name");
	// // arr.push(jsonn);
	// // });
	// // $.ajax({
	// // type:"post",
	// // url:"/experiment/cell/passage/cellPassage/saveCosPrepare.action",
	// // data:{data:JSON.stringify(arr)},
	//						
	//						
	// if($(this).is(':checked')) {
	// chosedID.push(this.value);
	// } else {
	// var value = this.value;
	// chosedID = chosedID.filter(function(v) {
	// return v != value;
	// })
	// }
	// });
	// },
	// yes: function(index, layero) {
	// if(chosedID.length>0){
	// var arr=[];
	// var panduanlx = true;
	// for(var j = 0,len = chosedID.length; j < len; j++){
	// var jsonn={};
	// jsonn.jcxmc=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().prev().text();
	// jsonn.jcxid=chosedID[j];
	// jsonn.jcsl=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().next().find("input[type='text']").val();
	// if(jsonn.jcsl==null
	// ||jsonn.jcsl==""
	// ||isNaN(Number(jsonn.jcsl))){
	// top.layer.msg("检测数量填写数字");
	// panduanlx = false;
	// }
	// jsonn.jcxlxmc=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().next().next().find("input[type='button']").val();
	// jsonn.jcxlx=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().next().next().find("input[type='button']").attr("sampletypeid");
	// jsonn.jcbz=$("input[type='checkbox'][value='"+chosedID[j]+"']:checked").parent().parent().next().next().next().find("input[type='text']").val();
	// if(jsonn.jcxlx==null
	// ||jsonn.jcxlx==""
	// ||jsonn.jclxmc=="选择样本类型"){
	// top.layer.msg("请选择质检样本类型");
	// panduanlx = false;
	// }
	// arr.push(jsonn);
	// //
	// }
	// if(panduanlx){
	// $.ajax({
	// type: "post",
	// url: ctx +
	// "/experiment/cell/passage/cellPassage/submitToQualityTestType.action",
	// data: {
	// idList: idList,
	// chosedID: chosedID,
	// mark: "CellPassage",
	// stepNum: $(".wizard_steps .selected").children(".step_no").text(),
	// id: cellPassage_id,
	// zj: $("#zjType").attr("zjId"),
	// data:JSON.stringify(arr)
	// },
	// success: function(res) {
	// var data = JSON.parse(res);
	// if(data.success) {
	// // top.layer.msg(biolims.common.submissionofSuccess);
	// top.layer.msg("选择成功，请在质检明细中提交质检！");
	// zhijianDetails.ajax.reload();
	// } else {
	// top.layer.msg("选择失败，请重新选择！");
	// // top.layer.msg(biolims.common.submissionFailure);
	// }
	// }
	// });
	// layer.close(index);
	// }else{
	//							
	// }
	//						
	// }else{
	// top.layer.msg("请选择质检项");
	// }
	// },
	// });
	//
	// // $.ajax({
	// // type: "post",
	// // data: {
	// // stepNum: $(".wizard_steps .selected")
	// // .children(".step_no").text(),
	// // idList: idList,
	// // id: cellPassage_id,
	// // zj: $("#zjType").attr("zjId"),
	// // mark: "CellPassage"
	// // },
	// // url: ctx +
	// // "/experiment/cell/passage/cellPassage/submitToQualityTest.action",
	// // success: function(data) {
	// // var data = JSON.parse(data);
	// // if(data.success) {
	// // top.layer
	// // .msg(biolims.common.submissionofSuccess);
	// //
	// // } else {
	// // top.layer
	// // .msg(biolims.common.submissionFailure);
	// // }
	// //
	// // }
	// // });
	// }
	// });

	var cellPassageMakeUpAfOps = table(true, cellPassage_id,
			"/experiment/cell/passage/cellPassage/noBlendPlateSampleTable.action?stepNum="
					+ stepNum, colOpts, tbarOpts);
	noBlendTab = renderData(eletable, cellPassageMakeUpAfOps);
}
// 选择样本质检类型
function xuanze(that) {
	top.layer
			.open({
				title : "选择样本名称",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/sample/dicSampleType/selectDicSampleTypeOne.action",
						'' ],
				yes : function(index, layer) {
					var type = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(1)
							.text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(0)
							.text();
					$(that).val(type);
					$(that).attr("sampleTypeid", id);
					top.layer.close(index)
				},
			})
}
// 渲染样本的datatables
function showSampleTable(eletable, stepNum) {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data" : "code",
		"title" : "样本编号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data" : "batch",
		"title" : "产品批号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "batch");
		}
	})
	colOpts.push({
		"data" : "patientName",
		"title" : "姓名",
		"createdCell" : function(td) {
			$(td).attr("saveName", "patientName");
		}
	})
	colOpts.push({
		"data" : "sampleOrder-filtrateCode",
		"title" : "筛选号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-filtrateCode");
		}
	})
	colOpts.push({
		"data" : "sampleType",
		"title" : "样本名称",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleType");
		}
	})
	colOpts.push({
		"data" : "countsOld",
		"title" : "二氧化碳培养箱编号(旧)",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "countsOld");
			$(td).attr("instrumentOld-id", rowData['instrumentOld-id']);
			$(td).attr("instrumentOld-storageContainer-rowNum",
					rowData['instrumentOld-storageContainer-rowNum']);
			$(td).attr("instrumentOld-storageContainer-colNum",
					rowData['instrumentOld-storageContainer-colNum']);
		}
	})
	colOpts.push({
		"data" : "posIdOld",
		"title" : "储位(旧)",
		"createdCell" : function(td) {
			$(td).attr("saveName", "posIdOld");
		}
	});
	colOpts.push({
		"data" : "counts",
		"title" : "二氧化碳培养箱编号(新)",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "counts");
			$(td).attr("instrument-id", rowData['instrument-id']);
			$(td).attr("instrument-storageContainer-rowNum",
					rowData['instrument-storageContainer-rowNum']);
			$(td).attr("instrument-storageContainer-colNum",
					rowData['instrument-storageContainer-colNum']);
		}
	})
	colOpts.push({
		"data" : "posId",
		"title" : "储位(新)",
		"createdCell" : function(td) {
			$(td).attr("saveName", "posId");
		}
	});
	colOpts.push({
		"data" : "qualified",
		"title" : biolims.common.isQualified,
		"className" : "select",
		"visible":false,
		"name" : "2" + "|" + "1" + "|" + "3",
		"createdCell" : function(td) {
			$(td).attr("saveName", "qualified");
			$(td).attr(
					"selectOpt",
					biolims.common.qualified + "|"
							+ biolims.common.disqualified + "|" + "警戒");
		},
		"render" : function(data, type, full, meta) {
			if (data == "2") {
				return biolims.common.qualified;
			}
			if (data == "1") {
				return biolims.common.disqualified;
			}
			if (data == "3") {
				return "警戒";
			} else {
				return '';
			}
		}
	});
	var tbarOpts = [];
	// 提交质检按钮 成功后直接将数据带去质检模块
	// tbarOpts.push({
	// text: '<i class="glyphicon glyphicon-upload"></i>' +
	// biolims.common.submissionQuality,
	// action: function() {
	// var rows = $("#blendTable .selected");
	// if(!rows.length) {
	// top.layer.msg(biolims.common.pleaseSelectData);
	// return false;
	// }
	//
	// // var tr = $(this[0].node).parents("#plateDiv").find(
	// // "#cellPassageSample").find("tbody").find("tr");
	// var idList = [];
	// rows.each(function(i, v) {
	// idList.push($(v).find(".icheck").val());
	// });
	//
	// $.ajax({
	// type: "post",
	// data: {
	// stepNum: $(".wizard_steps .selected")
	// .children(".step_no").text(),
	// idList: idList,
	// id: cellPassage_id,
	// zj: $("#zjType").attr("zjId"),
	// mark: "CellPassage"
	// },
	// url: ctx +
	// "/experiment/cell/passage/cellPassage/submitToQualityTest.action",
	// success: function(data) {
	// var data = JSON.parse(data);
	// if(data.success) {
	// top.layer
	// .msg(biolims.common.submissionofSuccess);
	//
	// } else {
	// top.layer
	// .msg(biolims.common.submissionFailure);
	// }
	//
	// }
	// });
	// }
	// });
    
	if (baocun) {
		if (caozuo) {
			tbarOpts.push({
				
						text : '<i class="glyphicon glyphicon-upload"></i>'
								+ "质检项"
						/* biolims.common.submissionQuality */,
						action : function() {
							// $("#blendTable").each(function(){
							//
							// var a =
							// $(this).find("input[type=checkbox]").attr('checked');
							//
							//			      
							// });
							var rows = $("#blendTable .selected");
							if (!rows.length) {
								top.layer.msg(biolims.common.pleaseSelectData);
								return false;
							}
							var idList = [];
							rows.each(function(i, v) {
								idList.push($(v).find(".icheck").val());
							});
							// var idList = [];

							// var b =a.getElementsByTagName("td");
							//				    
							// var d =b[0].innerText;
							// idList.push(d);

							// //拼弹出的质检项目
							// var bodyZhijian = '';
							// $("#zhijianBody .zhijianli").each(function(i, v)
							// {
							// bodyZhijian += '<tr>' +
							// '<td>' + $(v).find(".zhijianName").text() +
							// '</td>' +
							// '<td><input type="checkbox" class="icheckzj"
							// value="' +
							// $(v).find(".zhijianName").attr("zhijianid") + '"
							// /></td>' +
							// '<td><input type="text" style="border: none;"
							// /></td>' +
							// '</tr>';
							// });
							// var cheboxZhijian = '<table class="table">' +
							// '<tr>' +
							// '<th>检测项</th>' +
							// '<th>选择</th>' +
							// '<th>样本量</th>' +
							// '</tr>' + bodyZhijian +
							// '</table>';
							// var chosedID = [];
							// top.layer.open({
							// title: "选择质检项目",
							// type: 1,
							// area: ["40%", "50%"],
							// btn: biolims.common.selected,
							// btnAlign: 'c',
							// content: cheboxZhijian,
							// success: function() {
							// $('.icheckzj', parent.document).iCheck({
							// checkboxClass: 'icheckbox_square-blue',
							// increaseArea: '20%' // optional
							// }).on('ifChanged', function(event) {
							// if($(this).is(':checked')) {
							// chosedID.push(this.value);
							// } else {
							// var value = this.value;
							// chosedID = chosedID.filter(function(v) {
							// return v != value;
							// })
							// }
							// });
							// },
							// yes: function(index, layero) {
							// $.ajax({
							// type: "post",
							// url: ctx +
							// "/experiment/cell/passage/cellPassage/submitToQualityTestType.action",
							// data: {
							// idList: idList,
							// chosedID: chosedID,
							// mark: "CellPassage",
							// stepNum: $(".wizard_steps
							// .selected").children(".step_no").text(),
							// id: cellPassage_id,
							// zj: $("#zjType").attr("zjId"),
							// },
							// success: function(res) {
							// var data = JSON.parse(res);
							// if(data.success) {
							// top.layer.msg(biolims.common.submissionofSuccess);
							// zhijianDetails.ajax.reload();
							// } else {
							// top.layer.msg(biolims.common.submissionFailure);
							// }
							// }
							// });
							// top.layer.close(index);
							// 拼弹出的质检项目
							var bodyZhijian = '';
							if($("#zhijianBody .zhijianli").length!=0){
								var zhijianlikong = true;
								$("#zhijianBody .zhijianli").each(
									function(i, v) {
										if($(v).attr("id")==undefined){
											top.layer.msg("该步骤没有配置质检项！");
											zhijianlikong = false;
										}
									}
									
								);
								if(zhijianlikong){
									
								}else{
									return false;
								}
							}
							$("#zhijianBody .zhijianli").each(
											function(i, v) {
												bodyZhijian += '<tr>'
														+ '<td>'
														+ $(v).find(".zhijianName").text()
														+ '</td>'
														+ '<td><input type="checkbox" class="icheckzj" value="'
														+ $(v).find(".zhijianName").attr("zhijianid")
														+ '" /></td>'
														+ '<td><input type="button" sampleTypeid="" value="选择样本名称" class="form-control btn-success" onclick="xuanze(this)"/></td>'
														+ '<td><input type="text" style="width:100%" /></td>'
														+ '<td><input type="text" style="width:100%" /></td>'
														+ '<td><input type="text" style="width:100%" /></td>'
														+ '</tr>';
											});
							var cheboxZhijian ='<div class="ipadmini">'+'<table class="table">'
									+ '<tr>' + '<th>检测项</th>' + '<th>选择</th>'
									+ '<th>样本名称</th>' + '<th>样本量</th>' + '<th>单位</th>'
									+ '<th>备注</th>' + '</tr>'
									+ bodyZhijian + '</table>'+'</div>';
							var chosedID = [];
							layer.open({
										title : "选择质检项目",
										offset: '80px',
										btn : biolims.common.selected,
										type : 1,
										area :  [ document.body.clientWidth - 300,
											'500px' ],																			
										btnAlign : 'c',
										content : cheboxZhijian,
										success : function() {
											$('.layui-layer-btn-c').css({
												'position': 'absolute',
												'top':'0',
												'left': '50%',
												'transform': 'translateX(-50%)'
											})
											$('.layui-layer-page').css({
												'max-height': '400px',
												
											})
											
											$('.icheckzj').iCheck({
																checkboxClass : 'icheckbox_square-blue',
																increaseArea : '20%' // optional
															}).on('ifChanged',function(event) {
																// var arr=[];
																// $("#pageOneChoseYQ
																// .btn-info").each(function
																// (i,v) {
																// var jsonn={};
																// jsonn.id=v.getAttribute("cosid");
																// jsonn.code=v.getAttribute("code");
																// jsonn.name=v.getAttribute("name");
																// arr.push(jsonn);
																// });
																// $.ajax({
																// type:"post",
																// url:"/experiment/cell/passage/cellPassage/saveCosPrepare.action",
																// data:{data:JSON.stringify(arr)},

																if ($(this).is(':checked')) {
																	chosedID.push(this.value);
																} else {
																	var value = this.value;
																	chosedID = chosedID.filter(function(v) {
																				return v != value;
																			})
																}
															});
										},
										yes : function(index, layero) {
											if (chosedID.length > 0) {
												var arr = [];
												var panduanlx = true;
												for (var j = 0, len = chosedID.length; j < len; j++) {
													var jsonn = {};
													jsonn.jcxmc = $("input[type='checkbox'][value='"
																	+ chosedID[j]
																	+ "']:checked").parent().parent().prev().text();
													jsonn.jcxid = chosedID[j];
													jsonn.jcsl = $("input[type='checkbox'][value='"
																	+ chosedID[j]
																	+ "']:checked").parent().parent().next().next().find("input[type='text']").val();
													jsonn.jcxlxmc = $("input[type='checkbox'][value='"
																	+ chosedID[j]
																	+ "']:checked").parent().parent().next().next().next().find("input[type='text']").val();
													jsonn.jcxlx = $("input[type='checkbox'][value='"
																	+ chosedID[j]
																	+ "']:checked").parent().parent().next().find("input[type='button']").attr("sampletypeid");
													jsonn.jcbz = $("input[type='checkbox'][value='"
																	+ chosedID[j]
																	+ "']:checked").parent().parent().next().next().next().next().find("input[type='text']").val();
													jsonn.jcxsldw = $("input[type='checkbox'][value='"
																	+ chosedID[j]
																	+ "']:checked").parent().parent().next().next().next().find("input[type='text']").val();

													if (jsonn.jcxlx == null
															|| jsonn.jcxlx == ""
															|| jsonn.jclxmc == "选择样本类型") {
														top.layer
																.msg("请选择质检样本类型");
														panduanlx = false;
													}
													arr.push(jsonn);
												}
												if (panduanlx) {
													
													$.ajax({
															type : "post",
															url : ctx+ "/experiment/cell/passage/cellPassage/submitToQualityTestType.action",
															data : {
																idList : idList,
																chosedID : chosedID,
																mark : "CellPassage",
																stepNum : $(".wizard_steps .selected").children(".step_no").text(),
																stepName : $(".wizard_steps .selected").children(".step_descr").text(),
																id : cellPassage_id,
																zj : $("#zjType").attr("zjid"),
																data : JSON.stringify(arr)
																},
															success : function(res) {
																var data = JSON.parse(res);
																	if (data.success) {
																		// top.layer.msg(biolims.common.submissionofSuccess);
																		top.layer.msg("选择成功，请在质检明细中提交质检！");
																		zhijianDetails.ajax.reload();
																	} else {
																		top.layer.msg("选择失败，请重新选择！");
																		// top.layer.msg(biolims.common.submissionFailure);
																	}
																}
															});
													layer.close(index);
												} else {

												}

											} else {
												top.layer.msg("请选择质检项");
											}
										},
									});
						}
					});
			tbarOpts.push({
						text : "培养箱",
						action : function() {
							var rows = $("#blendTable .selected");
							var length = rows.length;
							if (!length) {
								top.layer.msg(biolims.common.pleaseSelect);
								return false;
							}
						    var orderNum=$(".wizard_steps .selected .step_no").text();
						    var id =$("#cellPassage_id").text();
							top.layer.open({
										title : "选择培养箱",
										type : 2,
										area: top.screeProportion,
										btn : biolims.common.selected,
										content : [
												window.ctx
														+ "/experiment/cell/passage/cellPassageNow/selectInstrumentOne.action?orderNum="+orderNum+"&id="+id,
												'' ],
										yes : function(index, layer) {
											var id = $('.layui-layer-iframe',
													parent.document)
													.find("iframe")
													.contents()
													.find(
															"#addDicSampleType .chosed")
													.children("td").eq(0)
													.text();
											var num = $('.layui-layer-iframe',
													parent.document)
													.find("iframe")
													.contents()
													.find(
															"#addDicSampleType .chosed")
													.children("td").eq(6)
													.text();
											if (num != "" && num > 0) {
												rows.addClass("editagain");
												rows
														.find(
																"td[savename='counts']")
														.attr("instrument-id",
																id).text(id);
												rows.find("td[savename='posId']").text("");
												top.layer.close(index);
											} else {
												top.layer
														.msg("该培养箱空位不足，请选择其他培养箱");
											}

										},
									});
						}
					});
			
			//样本名称按钮
			tbarOpts.push({
				text : "样本名称",
				action : function() {
					var rows = $("#blendTable .selected");
					var length = rows.length;
					if (!length) {
						top.layer.msg(biolims.common.pleaseSelect);
						return false;
					}
					top.layer.open({
									title : "选择样本名称",
									type : 2,
									area: top.screeProportion,
									btn : biolims.common.selected,
									content : [
											window.ctx
													+ "/sample/dicSampleType/selectDicSampleTypeOne.action",
											'' ],
									yes : function(index, layer) {
										var type = $('.layui-layer-iframe', parent.document).find(
												"iframe").contents().find(
												"#addDicSampleType .chosed").children("td").eq(1)
												.text();
										var id = $('.layui-layer-iframe', parent.document).find(
												"iframe").contents().find(
												"#addDicSampleType .chosed").children("td").eq(0)
												.text();
										rows.addClass("editagain");
//										rows.find("td[savename='sampleType-name']").attr(
//												"sampletype-id", id).text(type);
										rows.find("td[savename='sampleType']").attr(
												"sampletype-id", id).text(type);
										/*rows.find("td[savename='sampleType']").text(type);*/
										top.layer.close(index)
									},
								})
					}
					//
			});
		}
	}

	var cellPassageMakeUpAfOps = table(true, cellPassage_id,
			"/experiment/cell/passage/cellPassage/blendPlateSampleTable.action?stepNum="
					+ stepNum, colOpts, tbarOpts);
	blendTab = renderData(eletable, cellPassageMakeUpAfOps);
	blendTab.on('draw', function() {
			 $('#blendTable').find('.odd').addClass('selected');
	$('#blendTable').find('.icheckbox_square-blue').addClass('checked');
		$("#blendTable tbody tr").each(
				function(i, val) {
					if ($(val).find("td[savename='counts']").text() != "") {
						counts = $(val).find("td[savename='counts']").text();
						rowNum = $(val).find("td[savename='counts']").attr(
								"instrument-storagecontainer-rownum");
						colNum = $(val).find("td[savename='counts']").attr(
								"instrument-storagecontainer-colNum");
					}
				});
		$("#blendTable tbody tr").each(function(i, val) {
			if ($(val).find("td[savename='counts']").length == 0) {
				counts = "";
			} else {

			}
		});
		if (counts != "") {
			$("#plateModal").show();
			// 根据上一步选择页面变化生成孔板
			// rendrModalDiv($("#maxNum").val(), $("#temRow").val(),
			// $("#temCol").val());
			$("#plateNum .plate").text("");
			$("#plateNum").attr("plateNum", counts);
			rendrModalDiv(1, rowNum, colNum);
			// 填充孔板
			createShelf(rowNum, colNum, $(".plate"), renderChosedSample);
			showPlate();
		} else {
			$("#plateModal").hide();
		}
		oldChangeLog = blendTab.ajax.json();
	});
}
// if($("#maxNum").val()>0){
var counts = "";
var rowNum = "";
var colNum = "";
if (counts != "") {
	$("#plateModal").show()
	// 根据上一步选择页面变化生成孔板
	// rendrModalDiv($("#maxNum").val(), $("#temRow").val(),
	// $("#temCol").val());
	$("#plateNum .plate").text("");
	$("#plateNum").attr("plateNum", counts);
	rendrModalDiv(1, rowNum, colNum);
	// 填充孔板
	createShelf(rowNum, colNum, $(".plate"), renderChosedSample);
	showPlate();
} else {
	$("#plateModal").hide();
}
// 根据上一步选择页面变化生成孔板
function rendrModalDiv(num, row, col) {
	if (row > 8) {
		for (var i = 1; i < num; i++) {
			var clonePlateDiv = $(".plateDiv").eq(0).clone();
			clonePlateDiv.find("#plateNum").attr("plateNum", counts);// "p" +
			// (i +
			// 1)
			$("#plateModal").append(clonePlateDiv);
		}
	} else {
		if (num > 1) {
			$(".plateDiv").removeClass("col-xs-12").addClass("col-xs-6");
			for (var i = 1; i < num; i++) {
				var clonePlateDiv = $(".plateDiv").eq(0).clone();
				clonePlateDiv.find("#plateNum").attr("plateNum", counts);// "p"
				// + (i
				// + 1)
				$("#plateModal").append(clonePlateDiv);
			}
		}
	}
	$(".plateDiv").find(".btn-sm").click(function() {
		$(this).addClass("active").siblings(".btn-sm").removeClass("active");
	});
}
/**
 * 根据要求生成要求规格的架子或盒子
 * 
 * @param m =>
 *            行
 * @param n =>
 *            列
 */
function createShelf(m, n, element, callback) {
	var m = parseInt(m);
	var n = parseInt(n);
	// $(element).empty();
	var arr = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L",
			"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y",
			"Z" ];
	var tr0 = document.createElement("tr");
	var num = "";
	for (var i = 0; i <= n; i++) {
		num += '<td>' + i + '</td>';
	}
	tr0.innerHTML = num;
	tr0.children[0].innerHTML = "X";
	$(element).append(tr0);
	var h = 0;
	for (var i = 0; i < m; i++) {
		var tr = document.createElement("tr");
		var tds = "";
		var x = i + 1;
		for (var j = 0; j <= n; j++) {
			var jj = j < 10 ? "0" + j : j;
			tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m)
					+ ' coord=' + arr[i] + jj + '></div></td>';
			x += m;
		}
		tr.innerHTML = tds;
		tr.children[0].innerHTML = arr[i];
		$(element).append(tr);
	}
	var divs = $(element).find("div");
	for (var i = 0; i < divs.length; i++) {
		var h = i + 1;
		divs[i].setAttribute("h", h);
	}

	callback();
}
// 把选择的样本放在孔板上
function renderChosedSample() {
	var data = [];
	// 在孔板上悬浮提示
	$(".plate div").mouseover(function() {
		var plateDiv = $(this).parents(".plateDiv");
		var plate = plateDiv.find(".plate");
		var order = plateDiv.find(".active").attr("order");
		var that = this;
		if (order == "h") {
			// 样本横向排列悬浮提示
			orientationSampleHover(that, plate);
		} else if (order == "z") {
			// 样本纵向排列悬浮提示
			portraitSampleHover(that, plate);
		}

	});
	$(".plate div").mouseout(function() {
		$(".plate div").css('box-shadow', "none");
	});
	// 在孔板上点击赋值坐标
	var clickIndex = 0;
	$(".plate div")
			.click(
					function() {
						var bcpd = $("#blendTable .editagain");
						if(bcpd.length=="1"){
							top.layer.msg("请先保存数据");
							return false;
						}
						
						var sampleId = [];
						$("#blendTable .selected").each(
								function(i, val) {
									sampleId.push($(val).children("td").eq(0)
											.find("input").val());
								});
						if (sampleId.length > 0) {
							if (baocun) {
								if (caozuo) {
									if (this.title) {
										top.layer.msg("当前位置有样本，请换位置存储");
										return false;
									}
									var plateDiv = $(this).parents(".plateDiv");
									var plate = plateDiv.find(".plate");
									var order = plateDiv.find(".active").attr(
											"order");
									var that = this;
									if (order == "h") {
										// 样本横向排列
										data = orientationSampleClick(that,
												plate);
									} else if (order == "z") {
										// 样本纵向排列
										clickIndex++;
										data = portraitSampleClick(that, plate,
												clickIndex);
									}
									$
											.ajax({
												type : "post",
												url : ctx
														+ "/experiment/cell/passage/cellPassageNow/plateLayout.action",
												data : {
													data : data
												},
												success : function(data) {
													$("#plateModal").find(
															".sel").removeAttr(
															"sid");
													$("#plateModal").find(
															".sel").removeAttr(
															"title");
													$("#plateModal").find(
															".sel").css({
														"background-color" : ""
													});
													$("#plateModal").find(
															".sel")
															.removeClass("sel");
													blendTab.ajax.reload();
													// qualityTestMakeUpAfTab.ajax.reload();
												}

											});
								} else {
									top.layer.msg("您无权限使用该样本");
									return false;
								}
							} else {
								top.layer.msg("您无权限使用该样本");
								return false;
							}

						} else {
							if ($(this).attr("sId")) {
								$("#plateModal").find(".sel")
										.removeClass("sel");
								$(this).addClass("sel");
								var sid = $(this).attr("sId");
								$("#blendTable tbody .icheck").each(
										function(i, val) {
											$(val).iCheck('uncheck');
											if (val.value == sid) {
												$(val).iCheck('check');
											}
										});
								$("#plateModal .mysample").css("border",
										"1px solid gainsboro");
							} else {
								top.layer.msg("当前样本不是该步骤样本，无法移动该样本！");
								// var plateDiv = $(this).parents(".plateDiv");
								// var plate = plateDiv.find(".plate");
								// if($("#plateModal").find(".sel").length > 0)
								// {
								// var sId =
								// $("#plateModal").find(".sel").attr("sId");
								// var sampleCode =
								// $("#plateModal").find(".sel").attr("title");
								// $("#plateModal").find(".sel").removeAttr("sid");
								// $("#plateModal").find(".sel").removeAttr("title");
								// $("#plateModal").find(".sel").css({"background-color":""});
								// $("#plateModal").find(".sel").removeClass("sel");
								// this.style.backgroundColor = "#007BB6";
								// this.setAttribute("title", sampleCode);
								// this.setAttribute("sId", sId);
								// this.className = "mysample";
								// var posId = this.getAttribute("coord");
								// var plateNum =
								// plate.parent("#plateNum")[0].getAttribute("platenum");
								// data.push(sId + "," + posId + "," +
								// plateNum);
								// $.ajax({
								// type: "post",
								// url: ctx +
								// "/experiment/cell/passage/cellPassageNow/plateLayout.action",
								// data: {
								// data: data
								// },
								// success: function(data) {
								// blendTab.ajax.reload();
								// }
								//
								// })
								// }
							}
						}
					});
}
// 样本横向排列悬浮提示
function orientationSampleHover(that, plate) {
	var hh = parseInt(that.getAttribute("h"));
	var cc = $("#blendTable .selected").length;
	var holes = plate.find("div");
	for (var j = 0; j < holes.length; j++) {
		if (hh <= holes[j].getAttribute("h")
				&& holes[j].getAttribute("h") < hh + cc) {
			holes[j].style.boxShadow = "0 0 3px #007BB6";
		}
	}
}
// 样本横向排列点击渲染
function orientationSampleClick(that, plate) {
	var hh = parseInt(that.getAttribute("h"));
	var samples = $("#blendTable .selected");
	var cc = samples.length;
	var holes = plate.find("div");
	var positionArr = [];
	var data = [];
	var flag = 0;
	var first;
	for (var j = 0; j < holes.length; j++) {
		if (hh <= holes[j].getAttribute("h")
				&& holes[j].getAttribute("h") < hh + cc) {
			if (flag == 0) {
				first = j;
				flag++;
			}
			var sampleCode = samples.eq(j - first).children(
					"td[savename='code']").text();
			var id = samples.eq(j - first).children("td").eq(0).find(".icheck")
					.val();
			var posId = holes[j].getAttribute("coord");
			var plateNum = plate.parent("#plateNum")[0]
					.getAttribute("platenum");
			// samples.eq(j-first).children("td[savename='posId']").text(holes[j].getAttribute("coord"));
			data.push(id + "," + posId + "," + plateNum)
			holes[j].setAttribute("title", sampleCode);
			holes[j].setAttribute("sId", id);
			holes[j].className = "mysample";
			holes[j].style.backgroundColor = "#007BB6";
		}
	}
	return data;
}
// 渲染已保存到孔板上的样本
function showPlate() {
	$
			.ajax({
				type : "post",
				url : ctx
						+ "/experiment/cell/passage/cellPassageNow/showWellPlate.action",
				data : {
					id : counts
				},
				success : function(data) {
					var data = JSON.parse(data);
					for (var i = 0; i < data.data.length; i++) {
						var platePoint = $("#plateModal").find(
								"div[plateNum='" + counts + "']").find(
								"div[coord='" + data.data[i].location + "']")[0];
						platePoint.style.backgroundColor = "#007BB6";
						platePoint.setAttribute("title", data.data[i].batch);
						platePoint.setAttribute("sId", data.data[i].sId);
						platePoint.className = "mysample";
					}
				}
			})
}
// 渲染已提交质检明细datatables
function showZhijianDetails(eletable, stepNum) {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
//	colOpts.push({
//		"data": "productCode",
//		"title": "条码号",
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("saveName", "productCode");
//		}
//	})
	colOpts.push({
		"data": "SampleNumber",
		"title": "样本编号",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "SampleNumber");
		}
	})
	colOpts.push({
		"data" : "code",
		"title" : "样本编号",
		"visible":false,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "code");
			$(td).attr("qualityInfoId", rowData['qualityInfoId']);
		}
	})
	colOpts.push({
		"data" : "sampleType",
		"title" : "样本名称",
	})
	colOpts.push({
		"data" : "sampleNum",
		"title" : "检验量",
	})
	colOpts.push({
		"data" : "sampleNumUnit",
		"title" : "单位",
	})
	colOpts.push({
		"data" : "batch",
		"title" : "产品批号 ",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "batch");
			$(td).attr("qualityInfoId", rowData['qualityInfoId']);
			$(td).attr("code", rowData['code']);
		}
	})
	colOpts.push({
		"data" : "sampleOrder-name",
		"title" : "姓名 ",
	})
	colOpts.push({
		"data" : "sampleOrder-filtrateCode",
		"title" : "筛选号 ",
	})
	colOpts.push({
		"data" : "orderCode",
		"title" : "订单编号",
	})
	colOpts.push({
		"data" : "productName",
		"title" : "检测项目",
	})
	colOpts.push({
		"data" : "cellType",
		"title" : "检测方式",
		// "className":"select",
		"name" : "|自主检测|第三方检测",
		"createdCell" : function(td) {
			$(td).attr("saveName", "cellType");
			$(td).attr("selectOpt", "|自主检测|第三方检测");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return "自主检测";
			}
			if (data == "5") {
				return "第三方检测";
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data" : "sampleDeteyion-name",
		"title" : "检测项",
	});
	colOpts.push({
		"data" : "stepNum",
		"title" : "步骤",
	});
	colOpts.push({
		"data" : "experimentalStepsName",
		"title" : "步骤名称",
	});
	colOpts.push({
		"data" : "submit",
		"title" : "质检是否提交",
		// "className":"select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "submit");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			}
			if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data" : "qualitySubmitTime",
		"title" : "质检提交时间",
	});
	colOpts.push({
		"data" : "qualitySubmitUser",
		"title" : "质检提交人",
	})
	colOpts.push({
		"data" : "qualityReceiveTime",
		"title" : "质检接收时间",
	});
	colOpts.push({
		"data" : "qualityFinishTime",
		"title" : "质检完成时间",
	});
	colOpts.push({
		"data" : "qualified",
		"title" : biolims.common.isQualified,
		"visible":false,
		// "className": "select",
		"name" : biolims.common.qualified + "|" + biolims.common.disqualified
				+ "|" + "警戒",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "qualified");
			$(td).attr(
					"selectOpt",
					biolims.common.qualified + "|"
							+ biolims.common.disqualified + "|" + "警戒");
			if (rowData['qualified'] == "1") {
				$(td).parent().addClass("qualified");
			}
			if (rowData['qualified'] == "3") {
				$(td).parent().addClass("qualified");// .css('color','red');
			}
		},
		"render" : function(data, type, full, meta) {
			if (data == "2") {
				return biolims.common.qualified;
			}
			if (data == "1") {
				return biolims.common.disqualified;
			}
			if (data == "3") {
				return "警戒";
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "qualityInfoId",
		"title" : "质检结果id",
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "qualityInfoId");
		},
	})
	colOpts.push({
		"data" : "note",
		"title" : "备注",
	})
	var tbarOpts = [];
	if (baocun) {
		if (caozuo) {
//			tbarOpts
//					.push({
//						text : '删除',
//						action : function() {
//							var panduan = true;
//							var rows = $("#zhijianDetails .selected");
//							var length = rows.length;
//							if (!length) {
//								top.layer.msg("请选择数据");
//								return false;
//							}
//							var ids = [];
//							$.each(rows,
//									function(j, k) {
//										ids.push($(k).find(
//												"input[type=checkbox]").val());
//										if ($(k).find("td[savename=submit]")
//												.text() == "是") {
//											panduan = false;
//										}
//									});
//							if (panduan) {
//								ajax(
//										"post",
//										"/experiment/cell/passage/cellPassage/delQualityTestSample.action",
//										{
//											ids : ids
//										},
//										function(data) {
//											if (data.success) {
//												top.layer.msg("删除成功！");
//												zhijianDetails.ajax.reload();
//											} else {
//												top.layer
//														.msg(biolims.common.submitFail);
//											}
//										}, null);
//							} else {
//								top.layer.msg("所选数据存在已提交数据，不允许删除！");
//							}
//						}
//					});
//			tbarOpts.push({
//						text : '提交质检',
//						action : function() {
//							var panduan = true;
//							var rows = $("#zhijianDetails .selected");
//							var length = rows.length;
//							if (!length) {
//								top.layer.msg("请选择数据");
//								return false;
//							}
//							var ids = [];
//							$.each(rows,
//									function(j, k) {
//										ids.push($(k).find(
//												"input[type=checkbox]").val());
//										if ($(k).find("td[savename=submit]")
//												.text() == "是") {
//											panduan = false;
//										}
//									});
//							if (panduan) {
//								ajax(
//										"post",
//										"/experiment/cell/passage/cellPassage/submitToQualityTestSample.action",
//										{
//											ids : ids
//										},
//										function(data) {
//											if (data.success) {
//												top.layer
//														.msg(biolims.common.submitSuccess);
//												zhijianDetails.ajax.reload();
//											} else {
//												top.layer
//														.msg(biolims.common.submitFail);
//											}
//										}, null);
//							} else {
//								top.layer.msg("所选数据存在已提交数据，请重新选择！");
//							}
//						}
//					});
		}
	}
	tbarOpts
			.push({
				text : '查看质检结果',
				action : function() {
					var rows = $("#zhijianDetails .selected");
					var length = rows.length;
					if (length != 1) {
						top.layer.msg("请选择一条数据");
						return false;
					}
					var infoid = "";
					rows.each(function(i, v) {
						infoid = $(v).find("td[savename='batch']").attr('qualityinfoid');

					})
					// $.each(rows, function(j, k) {
					// alert($(k).find("td[savename='id']").text())
					// infoid =
					// $(k).find("td[savename='qualityInfoId']").text();
					// });
					if (infoid == "") {
						top.layer.msg("该条记录还没有质检结果请稍后再试！");
						return false;
					}
					top.layer
							.open({
								title : "质检结果",
								type : 2,
								area: top.screeProportion,
								btn : "关闭",
								content : [
										window.ctx
												+ "/experiment/cell/passage/cellPassage/findQualityInfoByid.action?infoid="
												+ infoid, '' ],
								yes : function(index, layer) {
									top.layer.close(index)
								},
							})
				}
			});
	tbarOpts.push({
		text : '打印请验单',
		action : function() {
			var rows = $("#zhijianDetails .selected");
			var length = rows.length;
			if (!length) {
				top.layer.msg("请选择数据！");
				return false;
			} else {
				var result = "";
				$("#zhijianDetails .selected").each(
						function(i, o) {
							if (result == "") {
								result += $(this).find("input[type=checkbox]")
										.val();
							} else {
								result += ","
										+ $(this).find("input[type=checkbox]")
												.val();
							}
						})
						
						$.ajax({
						type : "post",
						data : {
							id : $("#sampleReveice_id").text(),
							confirmDate : $("#cellPassage_createDate").text(),
							modelId:"dyqyd"
						},
						url : ctx
								+ "/stamp/birtVersion/selectBirt.action",
						success : function(data) {
							var data = JSON.parse(data)
							if (data.reportN) {
								
								var url = '__report='+data.reportN+'&id=' + result;
								commonPrint(url);
								
							} else {
								top.layer.msg("没有报表信息！");
							}
						}
					});
			
			}
		}
	})
	var options = table(true, cellPassage_id,
			"/experiment/cell/passage/cellPassage/findQualityItem.action",
			colOpts, tbarOpts);
	zhijianDetails = renderData(eletable, options);
	if(isIpad){
		$('#zhijianDetails_info').parent().removeClass('col-sm-5');
		$('#zhijianDetails_info').parent().addClass('col-sm-8');
		$('#zhijianDetails_paginate').parent().removeClass('col-sm-7');
		$('#zhijianDetails_paginate').parent().addClass('col-sm-9');
	}
}

function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '_blank', '');
}
// 分袋回输
function feedBack() {
	var id = $("#noBlendTable").find("tbody").children("tr").find(
			"input[type='checkbox']").val();
	$.ajax({
		type : 'post',
		url : ctx + '/experiment/cell/passage/cellPassage/feedBack.action',
		data : {
			id : id
		},
		success : function(data) {
			var data = JSON.parse(data);
			if (data.success) {
				top.layer.msg("回输成功");
				blendTab.ajax.reload();
			}
		}
	})
}
// 样本出库
function getblendSample() {
	var ids = [];
	var rows = $("#noBlendTable").find("tbody").children("tr");
	var blendRows = $("#blendTable").find("tbody").children("tr");
	var panduan = true;
	$.each(blendRows, function(ii, vv) {
		var inventoryStatus = $(vv).find("td[savename='inventoryStatus']")
				.text();
		if (inventoryStatus != null && inventoryStatus == "1") {
			top.layer.msg("样本已经出库,请勿再执行此操作");
			panduan = false;
			return false;
		}
	})
	if (panduan) {
		$.each(rows, function(i, v) {
			ids.push($(v).find("input[type='checkbox']").val());
		})
		$
				.ajax({
					type : 'post',
					url : ctx
							+ '/experiment/cell/passage/cellPassage/getblendSample.action',
					data : {
						ids : ids
					},
					success : function(data) {
						var data = JSON.parse(data);
						if (data.success) {
							top.layer.msg("样本出库成功");
							noBlendTab.ajax.reload();
							blendTab.ajax.reload();
						}
					}
				})
	}
}

// 生成结果的乐观锁
var blendSamplelgs = true;
// 混合样本
function blendSample() {
	if (blendSamplelgs) {
		blendSamplelgs = false;

		var ids = [];
		var rows = $("#noBlendTable").find("tbody").children("tr");
		var blendRows = $("#blendTable").find("tbody").children("tr");
		var panduan = true;
		$.each(blendRows, function(ii, vv) {
			var id = $(vv).find("input[type='checkbox']").val();
			if (id != null && id != "" && id != undefined) {
				top.layer.msg("样本已经操作,请勿再执行此操作");
				panduan = false;
				return false;
			}
		});

		// $.each(rows, function(xx, aa) {
		// var zz = $(aa).find("[savename='inventoryStatus']").text();
		// var oldid = $(aa).find("input[type='checkbox']").val();
		// if(zz != null && zz != "" ) {
		//				
		// }else{
		// // $.ajax({
		// // type: 'post',
		// // url: ctx +
		// '/experiment/cell/passage/cellPassage/blendSample.action',
		// // data: {
		// // ids: ids
		// // },
		// // success: function(data) {
		// // var data = JSON.parse(data);
		// // if(data.success) {
		// // top.layer.msg("样本混合成功");
		// // top.layer.msg("样本还未出库，请出库后生成结果");
		// // panduan = false;
		// // return false;
		// // }else{
		// //
		// // }
		// // }
		// // })
		// }
		// });

		if (panduan) {
			$.each(rows, function(i, v) {
				ids.push($(v).find("input[type='checkbox']").val());
			});
			$
					.ajax({
						type : 'post',
						url : ctx
								+ '/experiment/cell/passage/cellPassage/blendSample.action',
						data : {
							ids : ids
						},
						success : function(data) {
							var data = JSON.parse(data);
							if (data.success) {
								top.layer.msg("样本混合成功");
								noBlendTab.ajax.reload();
								blendTab.ajax.reload();
								blendSamplelgs = true;
							} else {
								blendSamplelgs = true;
							}
						}
					})
		} else {
			blendSamplelgs = true;
		}
	}

}

// 渲染质检
function renderZhijian(zhijian) {
	var zhijianLis = "";
	var id = zhijian.id ? zhijian.id : "";
	var code = zhijian.code ? zhijian.code : "";
	var name = zhijian.name ? zhijian.name : "";
	var nextId = zhijian.nextId ? zhijian.nextId : "";
	var itemId = zhijian.itemId ? zhijian.itemId : "";
	zhijianLis += '<li class="zhijianli zhijianOld" id='
			+ id
			+ '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span nextid=\''
			+ nextId
			+ '\' zhijianid=\''
			+ code
			+ '\' class="zhijianName">'
			+ name
			+ '</span></small><small><i testId=\''
			+ code
			+ '\' stepNum=\''
			+ itemId
			+ '\' onclick="showResult(this)" class="fa fa-search-plus pull-right"></i></small></li>';
	$("#zhijianBody").append(zhijianLis);
}

// 展示质检提交到哪个质检的结果内容(改成查看该样本所有质检的结果)
function showResult(self) {
	// var stepNum = $(self).attr("stepNum");
	// var testId = $(self).attr("testId");
	// 任务单号
	var testId = $("#cellPassage_id").text();
	var mark = "CellPassage";
	top.layer
			.open({
				title : "质检结果",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/experiment/quality/qualityTest/showQualityTestResultListAllForzj.action?testId="
								+ testId // " +
						// "stepNum=" + stepNum + "&testId=" + testId +
						// "&mark=" + mark
						, '' ],
				yes : function(index, layer) {
					top.layer.closeAll();
				},
			})
}

// 渲染BL操作
function renderCaozuo(caozuo, caozuoResult, customTest, customTestresult,
		temProduc,haveEndTime) {
	var caozuo = JSON.parse(caozuo);
	var max = caozuo.length;
	if (max) {
		// 渲染模板
		$(".timeline")
				.html(
						'<li class="time-label" id="startCaozuo"><span class="bg-red">严格执行有关文件的操作规范 </span><span class="time"><button id="btn_startTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this,'+true+','+true+')"><i class="fa fa-clock-o"></i> 开始时间: <i id="productStartTime" class="stepsTime"></i></button></span></li>');// <button
		// id="btn_endTime"
		// class="btn
		// btn-sm
		// btn-box-tool"
		// onclick="watchTime(this)"><i
		// class="fa
		// fa-clock-o"></i>结束时间:
		// <i
		// id="productEndTime"
		// class="stepsTime"></i></button>
		var lipts = '';
		var startpits='';
		var endpits='';
		caozuo
				.forEach(function(v, i) {
					var startTime = temProduc.length ? temProduc[i].startTime
							: '';
					var endTime = temProduc.length ? temProduc[i].endTime : '';
					if (startTime == undefined) {
						startTime = "";
					}
					if (endTime == undefined) {
						endTime = "";
					}
					// var li = $('<li id="' + v.id + '" class="caozuoItem"><i
					// class="fa
					// fa-balance-scale bg-aqua"></i><div
					// class="timeline-item"><span
					// class="time"><input type="checkbox"
					// class="mySwitch"></span><h3
					// class="timeline-header"><a href="####">' +
					// v.producingTitle + '
					// </a></h3><div class="timeline-body">' + v.producingName +
					// '<button class="btn btn-success btn-xs pull-right"
					// onclick="showLineFoot (this)">收起</button></div><div
					// class="timeline-footer box-body"><div
					// class="col-xs-6"><ul
					// class="todo-list" id="reagentBody"></ul></div><div
					// class="col-xs-6"><ul class="todo-list"
					// id="cosBody"></ul></div></div></div></li>');
//					if(temProduc[i].templeCell.hideSmalleTime=="1"){
//						alert(1)
//					}else if(temProduc[i].templeCell.hideSmalleTime=="0"){
//						alert(2)
//					}
					if(temProduc[i].templeCell.hideSmalleTime=="1"){
						if(haveEndTime){
						startpits='<button disabled="true" id="btn_startTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this,'+false+','+true+')"><i class="fa fa-clock-o"></i> 开始时间: <i id="startTime" class="stepsTime">'
							+ startTime
							+ '</i></button>'
						}else{
						startpits='<button  id="btn_startTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this,'+false+','+true+')"><i class="fa fa-clock-o"></i> 开始时间: <i id="startTime" class="stepsTime">'
						+ startTime	
						+ '</i></button>'
						}
						if(haveEndTime){
						endpits='<button disabled="true" id="btn_endTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this,'+false+','+false+')"><i class="fa fa-clock-o"></i> 结束时间:   <i id="endTime" class="stepsTime">'
						+ endTime
						+ '</i></button>'	
						}else{
						endpits='<button id="btn_endTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this,'+false+','+false+')"><i class="fa fa-clock-o"></i> 结束时间:   <i id="endTime" class="stepsTime">'
						+ endTime
						+ '</i></button>'	
						}
						lipts='<li id="'
							+ v.id
							+ '" class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><input type="checkbox" class="mySwitch" style="display:none">'
//							+ '<button id="btn_startTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this,'+false+','+true+')"><i class="fa fa-clock-o"></i> 开始时间: <i id="startTime" class="stepsTime">'
//							+ startTime
							+startpits
							+endpits
							+ '</span><h3 class="timeline-header"><a href="####">'
							+ v.producingTitle
							+ ' </a></h3><div class="timeline-body">'
							+ v.producingName
							+ '<button class="btn btn-success btn-xs pull-right" onclick="showLineFoot (this)">收起</button></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div> <div class="col-xs-6"> <ul class="todo-list" id="zhijianBodyNew"></ul></div><div class="col-xs-12" id="pageThree'
							+ temProduc[i].id + '"></div></div></div></li>';
					}else if(temProduc[i].templeCell.hideSmalleTime=="0"){
						lipts='<li id="'
							+ v.id
							+ '" class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><input type="checkbox" class="mySwitch" style="display:none">'
							+ '</span><h3 class="timeline-header"><a href="####">'
							+ v.producingTitle
							+ ' </a></h3><div class="timeline-body">'
							+ v.producingName
							+ '<button class="btn btn-success btn-xs pull-right" onclick="showLineFoot (this)">收起</button></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div>   <div class="col-xs-6"> <ul class="todo-list" id="zhijianBodyNew"></ul></div><div class="col-xs-12" id="pageThree'
							+ temProduc[i].id + '"></div></div></div></li>';
					}
					
//					var li = $(
//							'<li id="'
//							+ v.id
//							+ '" class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><input type="checkbox" class="mySwitch" style="display:none">'
////							if(temProduc[i].templeCell.hideSmalleTime=="1"){
//								+ '<button id="btn_startTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 开始时间: <i id="startTime" class="stepsTime">'
//								+ startTime
//								+ '</i></button><button id="btn_endTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 结束时间:   <i id="endTime" class="stepsTime">'
//								+ endTime
//								+ '</i></button>'	
////							}
//							+ '</span><h3 class="timeline-header"><a href="####">'
//							+ v.producingTitle
//							+ ' </a></h3><div class="timeline-body">'
//							+ v.producingName
//							+ '<button class="btn btn-success btn-xs pull-right" onclick="showLineFoot (this)">收起</button></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div><div class="col-xs-12" id="pageThree'
//							+ temProduc[i].id + '"></div></div></div></li>');
					var li = $(lipts);
					var reagent = v.reagentList;
					var cos = v.cosList;
					var cpo = v.cpoList;
					var zhiJian = v.zhiJianList;
					reagent.forEach(function(vv, ii) {
						renderReagent(li, vv)
					})
					cos.forEach(function(vvv, iii) {
						renderCos(li, vvv)
					})
					zhiJian.forEach(function(vvv, iii) {
						renderZhiJianNew(li, vvv)
					})
					/*
					 * cpo.forEach(function(vvvv, iiii) {
					 * renderStartAndEndTime(li, vvvv) })
					 */
					$(".timeline").append(li);

					var result = temProduc.length ? temProduc[i].customTest
							: '';
					if (result == "") {

					} else {
						var resultz = JSON.parse(caozuo[i].customTest);

						var result1 = JSON.parse(temProduc[i].customTest);
						var ipts = '';

						resultz.forEach(function(z, x) {
									if(z.readOnly=="true"){
										var v=z.defaultValue.toString();
										if(z.required=="true"){
											ipts += '<div class="col-xs-6"><div class="input-group"><span class="input-group-addon">'
												+ z.label
												+ '<img class="requiredimage" src="/images/required.gif"></span>'
	                                            +'<input id="'
	    										+ z.fieldName
	    										+ '" readonly="readonly" type="'
	    										+ z.type
	    								        + '" required="'
	    								        + z.required
	    								        + '" value="'
	    										+ z.defaultValue
	    										+ '" class="form-control">'	
												+'</div></div>';
										}else{
											ipts += '<div class="col-xs-6"><div class="input-group"><span class="input-group-addon">'
												+ z.label
												+ '</span>'
	                                            +'<input id="'
	    										+ z.fieldName
	    										+ '" readonly="readonly" type="'
	    										+ z.type
	    								        + '" required="'
	    								        + z.required
	    								        + '" value="'
	    										+ z.defaultValue
	    										+ '" class="form-control">'	
												+'</div></div>';
										}
										
										
									}else{
										if(z.required=="true"){
											ipts += '<div class="col-xs-6"><div class="input-group"><span class="input-group-addon">'
												+ z.label
												+ '<img class="requiredimage" src="/images/required.gif"></span>'
												+'<input id="'
												+ z.fieldName
												+ '" type="'
												+ z.type
												+ '" required="'
		    								    + z.required
												+ '" class="form-control">'
												+'</div></div>';
										}else{
											ipts += '<div class="col-xs-6"><div class="input-group"><span class="input-group-addon">'
												+ z.label
												+ '</span>'
												+'<input id="'
												+ z.fieldName
												+ '" type="'
												+ z.type
												+ '" required="'
		    								    + z.required
												+ '" class="form-control">'
												+'</div></div>';
										}
										
									}
									
											
								});
						$("#pageThree" + temProduc[i].id).html(ipts);

						for ( var k in result1) {
							$("#pageThree" + temProduc[i].id).find("#" + k)
									.val(result1[k]);
						}

					}

				});
		$(".timeline")
				.append(
						'<li><i class="fa fa-balance-scale bg-yellow"></i><div class="timeline-item">备注：<input type="text" id="notes" /><span class="time"><button id="btn_endTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this,'+true+','+false+')"><i class="fa fa-clock-o"></i> 结束时间: <i id="productEndTime" class="stepsTime"></i></button></span></div></li><li><i class="fa fa-clock-o bg-gray"></i></li>');
		// $(".timeline").append('<li><i class="fa fa-balance-scale
		// bg-yellow"></i><div class="timeline-item"><span class="time"><button
		// id="btn_endTime" class="btn btn-sm btn-box-tool"
		// onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 结束时间: <i
		// id="endTime" class="stepsTime">'+endTime+'</i></button></span><h3
		// class="timeline-header"><a href="####">操作-End</a></h3><div
		// class="timeline-body">生产结果</div><div class="timeline-footer box-body"
		// id="pageThree"></div></div></li><li> <i class="fa fa-clock-o
		// bg-gray"></i></li>');
		// $(".timeline .caozuoItem:eq(0) .time").html('<button
		// id="btn_startTime" class="btn btn-sm btn-box-tool"
		// onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 开始时间: <i
		// id="startTime" class="stepsTime">'+startTime+'</i></button><button
		// id="btn_endTime" class="btn btn-sm btn-box-tool"
		// onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 结束时间: <i
		// id="endTime" class="stepsTime">'+endTime+'</i></button>');
		// 赋值
		var caozuoResult = JSON.parse(caozuoResult);
	
		caozuoResult.forEach(function(v, i) {
			var targetli = $("#" + v.templeId);
			if (v.state && v.state == "1") {
				var checked = true;
			} else {
				var checked = false;
			}
			targetli.attr({
				"czid" : v.id,
				"state" : v.state
			}).find(".mySwitch").prop("checked", checked);
			v.reagent.forEach(function(vv, ii) {
				var thisreagent = targetli.find("#" + vv.templateReagentId);
				thisreagent.attr("reagentid", vv.id)
						.find(".label-primary span").text(vv.batch);
				// thisreagent.find(".label-info span").text(vv.cellPassSn);
				thisreagent.find("input ").find(".sum").val(vv.reagentDosage);
				thisreagent.find("input ").find(".unit").val(vv.unit);
				targetli.find("#" + vv.templateReagentId).find(".sum")
						.val(vv.reagentDosage);
				targetli.find("#" + vv.templateReagentId).find(".unit")
				.val(vv.unit);
				
			});
			v.cosList.forEach(function(vv, ii) {
				targetli.find("#" + vv.templateCosId).attr({
					"cosid" : vv.id,
					"code" : vv.instrumentCode
				}).find(".label-warning").text(vv.instrumentName);
			});
		});

		setTimeout(function(){
			$(".timeline .mySwitch").each(function(i,v) {
				var state = $(this).is(':checked');
				$(this).bootstrapSwitch({
					onText : "已完成",
					offText : "未完成",
					onSwitchChange : function(event, state) {
						var num = $(this).parents('li').index();
						if(isIpad){
							// $('.ipddd').animate({
								
							// 	scrollTop: num*60
							// });
							 $('.ipddd').scrollTop(num*160)
						}else{
							$('html,body').animate({
								scrollTop: $(this).parents('.timeline-item').find('.timeline-body').offset().top+20
							});
						}
						if (state == true) {
							$(this).parents(".caozuoItem").attr("state", "1");
						} else {
							$(this).parents(".caozuoItem").attr("state", '0');
						}
					}
				}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
			})
		},0.1);
		
	//    if(isIpad){
	// 	   $('.timeline').css({
	// 		   "height":"600px",
	// 		   "overflow-y":"auto"
	// 	   });
	// 	  // 
		  
	//    }
		// var result = JSON.parse(customTest);
		// var ipts = '';
		// result.forEach(function(v, i) {
		// ipts += '<div class="col-xs-4"><div class="input-group"><span
		// class="input-group-addon">' + v.label + '</span> <input id="' +
		// v.fieldName + '" type="' + v.type + '"
		// class="form-control"></div></div>';
		// })
		// $("#pageThree").html(ipts);
		// if(customTestresult.length) {
		// var customTestresult = JSON.parse(customTestresult[0].content);
		// for(var k in customTestresult) {
		// $("#pageThree").find("#" + k).val(customTestresult[k]);
		// }
		// }
		// 选择原辅料和设备
		reagentAndCosChose();
	} else {
		$(".timeline").html("没有数据");
	}
}
// 渲染原辅料
function renderReagent(li, reagent) {
	var reagentLis = "";
	var code = reagent.code ? reagent.code : "";
	reagentLis += '<li class="reagli" id='
			+ reagent.id
			+ ' code='
			+ code
			+ ' ><span><i class="fa fa-ban"></i></span><p class="text">'
			+ reagent.name
			+ '</p><small class="label label-primary">批次:<span></span></small></br><div class="input-group"><span class="input-group-addon">原辅料用量</span> <input style="width:65%" type="text" class="form-control input-sm sum"><input readonly=readonly style="width:25%" type="text" class="form-control input-sm unit"></div></li>';// <small
																																																						// class="label
																																																						// label-info">sn:<span></span></small>
	li.find("#reagentBody").append(reagentLis);
}
//渲染质检
function renderZhiJianNew(li, zhijan) {
	var reali = "";
	var id = zhijan.id ? zhijan.id : "";
	var code = zhijan.code ? zhijan.code : "";
	var name = zhijan.name ? zhijan.name : "";
	var num = zhijan.num ? zhijan.num : "";
	var unit = zhijan.unit ? zhijan.unit : "";
	var sname = zhijan.sname ? zhijan.sname : "";
	reali += '<li class="zhijianli zhijianOld" id=' + id +' ><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary">检测项:<span zhijianid=\'' + code + '\' class="zhijianName">' + name + '</span></small><div class="input-group"><span class="input-group-addon">样本量</span> <input readyonly="readyonly" style="width:90%" type="text" class="form-control input-sm sum" value='+num+'></div><div class="input-group"><span class="input-group-addon">单位</span> <input readonly="readonly" style="width:90%" type="text" class="form-control input-sm unit" value='+unit+'></div><div class="input-group"><span class="input-group-addon">样本名称</span> <input style="width:90%" type="text" readonly=readonly class="form-control input-sm sname" value='+sname+'></div></li>';
	li.find("#zhijianBodyNew").append(reali);
}


// 渲染设备
function renderCos(li, cos) {
	var cosLis = "";
	var name = cos.name ? cos.name : "";
	var code = cos.code ? cos.code : "";
	cosLis += '<li class="cosli" id=' + cos.id + '  typeid=' + cos.typeId
			+ '><span><i class="fa fa-search-plus"></i></span><p class="text">'
			+ cos.typeName
			+ '</p><small class="label label-warning"></small></li>';
	li.find("#cosBody").append(cosLis);
}

function renderStartAndEndTime(li, cpo) {
	var startTime = cpo.startTime ? cpo.startTime : "";
	var endTime = cpo.endTime ? cpo.endTime : "";
}
// 获取当前时间
function watchTime(that,state,startOrEnd) {
	var time = $(that).children(".stepsTime").text();

//	if($(that).children(".stepsTime").text()!=""&&$(that).children(".stepsTime").text()!=null){
//		$(that).children(".stepsTime").text(
//				parent.moment(new Date()).format("YYYY-MM-DD  HH:mm"));
//	}
//
//	
	if($(that).attr("id")=="btn_endTime"){
		if($(that).children(".stepsTime").attr("id")=="endTime"){
			var stime=$(that).prev().children(".stepsTime").text();
			if(stime==""||stime==null){
				top.layer.msg("请先选择开始时间!");		
				return false
			}	
		}else if($(that).children(".stepsTime").attr("id")=="productEndTime"){
			if($("#productStartTime").text()==""||$("#productStartTime").text()==null){
				top.layer.msg("请先选择开始时间!");		
				return false;
			}
		}
			
	}
	
	var startOrEnd;
	var caozuoId;
	var smallTime;
	var stepName;
	if(state){
		//大开始结束
		if(startOrEnd){
			//大开始
//			smallTime=$(that).children(".stepsTime").text();
			startOrEnd="start"		
		}else{
			//大结束
//			smallTime=$(that).children(".stepsTime").text();
			startOrEnd="end";
		}
		
		
	}else{
		//小开始结束
		if(startOrEnd){
			//小开始
			caozuoId  =$(that).parents('.caozuoItem').attr('czid');
//			smallTime =$(that).children(".stepsTime").text();
			stepName=$(that).parent().parent().find("a").text();
			startOrEnd="start"
		}else{
			//小结束
			caozuoId  =$(that).parents('.caozuoItem').attr('czid');
//			smallTime =$(that).children(".stepsTime").text();
			stepName=$(that).parent().parent().find("a").text();
			startOrEnd="end";
		}
		
		
	}
	if($(that).children(".stepsTime").text()!=""&&$(that).children(".stepsTime").text()!=null){
		$(that).children(".stepsTime").text(
				parent.moment(new Date()).format("YYYY-MM-DD  HH:mm"));
		
		
		smallTime =$(that).children(".stepsTime").text();
		layer.open({
			  type: 1,
			  title: "复核人验证",
			  closeBtn: 1,
			  offset: '100px',
			  shadeClose: true,
			  skin: 'yourclass',
			  btn: ['验证'],
			  content: `
		            <div class="form-group has-feedback">
						<input type="text" id="yzname" class="form-control name" placeholder="Username" required>
						<span class="glyphicon glyphicon-user form-control-feedback"></span>
					</div>
					<div class="form-group has-feedback">
						<input type="password" id="yzpassword" class="form-control psd" placeholder="Password" required>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
					</div>
					<div class="col-xs-12">
				</div>` ,
		 yes: function(index,layero){
		     var user = layero.find('#yzname').val();
		     var pad = layero.find('#yzpassword').val();
		     
//		     if(startOrEnd="start"){
//		    	var t= $(that).next().children(".stepsTime").text();
//		    	if(t!=""&&t!=null){
//		    		$(that).next().children(".stepsTime").text("")
//		    		
//		    	}
//		     }
		     
		     $.ajax({
					type: "post",
					url: ctx+"/main/checkU.action",
					data: {
						user: user,
						pad: pad,
					},
					datatype: "json",
					success: function(data) {
						var obj = JSON.parse(data);
						if(obj["success"] == 1) {
					    if(startOrEnd=="start"){
					    	if(state==true){
					    		$("#productEndTime").text("");
					    	}else{
						    	$(that).next().children(".stepsTime").text("");
	
					    	}
					    	$.ajax({
								type : "post",
								url : "/experiment/cell/passage/cellPassageNow/saveStartTimeOrEndTime.action",
								data : {
									id:$("#cellPassage_id").text().trim(),
									orderNum : $(".wizard_steps .selected .step_no").text(),
									caozuoId : caozuoId,
									smallTime : smallTime,
									startOrEnd : startOrEnd,
									stepName:stepName,
									modify:"yes",
									user:user,
									time:time,
									start:"yesStart",
								},
								success : function(res) {
									var data = JSON.parse(res);
									if (data.success) {
										top.layer.msg("保存成功");
//										$(that).attr("disabled",true);
									} else {
										top.layer.msg("保存失败");
									}
								}
							});
					    }else{
					    
						$.ajax({
							type : "post",
							url : "/experiment/cell/passage/cellPassageNow/saveStartTimeOrEndTime.action",
							data : {
								id:$("#cellPassage_id").text().trim(),
								orderNum : $(".wizard_steps .selected .step_no").text(),
								caozuoId : caozuoId,
								smallTime : smallTime,
								startOrEnd : startOrEnd,
								stepName:stepName,
								modify:"yes",
								user:user,
								time:time
							},
							success : function(res) {
								var data = JSON.parse(res);
								if (data.success) {
									top.layer.msg("保存成功");
//									$(that).attr("disabled",true);
								} else {
									top.layer.msg("保存失败");
								}
							}
						});
						
					    }
						
					    layer.closeAll(); //疯狂模式，关闭所有层
						} else{ 
						top.layer.msg("验证失败！");
						} 
					}
				});
		 },
		 cancel: function(index, layero){ 
			  $(that).children(".stepsTime").text(time);
			  layer.close(index);
			  return false; 
		}    
	});
}else{
	$(that).children(".stepsTime").text(
			parent.moment(new Date()).format("YYYY-MM-DD  HH:mm"));
   smallTime =$(that).children(".stepsTime").text();
   top.layer.msg('你确定要选择时间么？', {
		  time: 0, //不自动关闭
		  btn: ['确定', '取消'],
		  btnAlign: 'c'	,
		  shade:0.3, 
		  yes: function(index){
				$.ajax({
					type : "post",
					url : "/experiment/cell/passage/cellPassageNow/saveStartTimeOrEndTime.action",
					data : {
						id:$("#cellPassage_id").text().trim(),
						orderNum : $(".wizard_steps .selected .step_no").text(),
						caozuoId : caozuoId,
						smallTime : smallTime,
						startOrEnd : startOrEnd,
						stepName:stepName,
					},
					success : function(res) {
						var data = JSON.parse(res);
						if (data.success) {
							top.layer.msg("保存成功");
//							$(that).attr("disabled",true);
						} else {
							top.layer.msg("保存失败");
						}
					}
				});
			
				
		    top.layer.close(index);
		  },
		  btn2: function(index,layer){
			  $(that).children(".stepsTime").text("")  
			 }
		});

	
	}
}
// 设备原辅料展开关闭
function showLineFoot(that) {
	
	var target = $(that).parent(".timeline-body").next(".timeline-footer");
	if (target.hasClass("xxxxx")) {
		$(that).text("收起");
		target.slideDown().removeClass("xxxxx");
	} else {
		$(that).text("展开");
		target.slideUp().addClass("xxxxx");
	}
	// if(isIpad){
	// 	$('.timeline').animate({
	// 		scrollTop: $(that).offset().top-60
	// 	});
	// }else{
	// 	$('html,body').animate({
	// 		scrollTop: $(that).offset().top-60
	// 	});
	// }
}
// 原辅料和设备选择操作
function reagentAndCosChose() {
	$(".fa-search-plus").unbind("click").click(function() {
		var item = $(this).parents("li");
		if (item.hasClass("reagli")) {
			addReagent(item.attr("code"), item);
		}
		if (item.hasClass("cosli")) {
			addCos(item.attr("typeid"), item);
		}
	});
}
// 选择原辅料批次
function addReagent(id, item) {
	var batch = $("#cellpassagebatch").val();
	var url="";
	pici=item.find("[code='"+id+"']").find(".label-primary span").text();
	if(pici!=""&&pici!=null){
		url="/storage/getStrogeReagent.action?state=state&id=" + id+ "&batch=" + batch + "&pici="+pici+""
	}else{
		url="/storage/getStrogeReagent.action?id=" + id+ "&batch=" + batch + ""
	}

	top.layer.open({
		title : biolims.common.selReagent,
		type : 2,
		area: top.screeProportion,
		btn : biolims.common.selected,
		content : [
				window.ctx + url, "" ],
		yes : function(index, layero) {
			var batch = $(".layui-layer-iframe", parent.document)
					.find("iframe").contents().find("#addReagent .chosed")
					.children("td").eq(2).text();
			var unit = $(".layui-layer-iframe", parent.document)
			.find("iframe").contents().find("#addReagent .chosed")
			.children("td").eq(6).text();
			// var sn = $(".layui-layer-iframe", parent.document).find("iframe")
			// .contents().find("#addReagent .chosed").children("td")
			// .eq(4).text();
//			item.find(".label-primary span").text(batch);
			item.find("[code='"+id+"']").find(".label-primary span").text(batch);
			item.find("[code='"+id+"']").find(".unit").val(unit);

			// item.find(".label-info span").text(sn);
			top.layer.close(index)
		},
	});
}
// 第三页选择设备
function addCos(id, item) {
	top.layer.open({
		title : biolims.common.selectInstrument,
		type : 2,
		offset : [ '10%', '10%' ],
		area: top.screeProportion,
		btn : biolims.common.selected,
		content : [
				window.ctx + "/equipment/main/selectCos.action?typeId=" + id
						+ "", "" ],
		yes : function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe")
					.contents().find("#addCos .chosed").children("td").eq(1)
					.text();
			var code = $(".layui-layer-iframe", parent.document).find("iframe")
					.contents().find("#addCos .chosed").children("td").eq(0)
					.text();
			item.find(".label-warning").text(name);
			item.attr("code", code);
			top.layer.close(index)
		},
	})
}
// 第一页选择设备
function addCos1(that) {
	var roomName = $("#operatingRoomName").val();
	top.layer
			.open({
				title : biolims.common.selectInstrument,
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
					
					window.ctx
					+ "/experiment/cell/passage/cellPassage/selectCos.action?typeId="
					+ that.getAttribute("typeId")
					+ "&roomName="
					+ encodeURIComponent(encodeURIComponent(roomName))
					+ "", "" ],
//						window.ctx
//								+ "/equipment/main/selectCos.action?typeId="
//								+ that.getAttribute("typeId")
//								+ "&roomName="
//								+ encodeURIComponent(encodeURIComponent(roomName))
//								+ "", "" ],
				yes : function(index, layero) {
					var name = $(".layui-layer-iframe", parent.document).find(
							"iframe").contents().find("#addCos .chosed")
							.children("td").eq(1).text();
					var code = $(".layui-layer-iframe", parent.document).find(
							"iframe").contents().find("#addCos .chosed")
							.children("td").eq(0).text();
					that.innerText = "选择设备：" + name;
					that.setAttribute("code", code);
					that.setAttribute("name", name);
					$(that).parent().prev().text(code);
					top.layer.close(index)
				},
			})
}
// 第一页设备保存
function savePage1Cos() {
	var arr = [];
	$("#pageOneChoseYQ .btn-info").each(function(i, v) {
		var jsonn = {};
		jsonn.id = v.getAttribute("cosid");
		jsonn.code = v.getAttribute("code");
		jsonn.name = v.getAttribute("name");
		arr.push(jsonn);
	});
	var changeLog = "细胞生产:";
	$('button[class="btn btn-xs btn-info"]').each(function(i, v) {
		var valnew = $(v).attr("name");
		var val = $(v).attr("code");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	
	$.ajax({
		type : "post",
		url : "/experiment/cell/passage/cellPassage/saveCosPrepare.action",
		data : {
			data : JSON.stringify(arr),
			roomName : $("#operatingRoomName").val(),
			roomId : $("#operatingRoomId").val(),
			zbid : $("#cellPassage_id").text(),
			bzs : $(".wizard_steps .selected .step_no").text(),
			changeLog:changeLog
		},
		success : function(res) {
			var data = JSON.parse(res);
			if (data.success) {
//				top.layer.msg("保存成功");
				startPrepare();
			} else {
				top.layer.msg("保存失败");
			}
		}
	});
}
function startPrepare() {
	var arr = [];
	$("#pageOneChoseYQ .btn-info").each(function(i, v) {
		var jsonn = {};
		jsonn.id = v.getAttribute("cosid");
		jsonn.code = v.getAttribute("code");
		jsonn.name = v.getAttribute("name");

		jsonn.rwdid = $("#cellPassage_id").text();
		jsonn.batch = $("#cellPassage_batch").text();
		jsonn.bzid = $(".wizard_steps .selected").attr("stepid");
		jsonn.bzname = $("#steptiele").text();
		jsonn.bzorderNum = $(".wizard_steps .selected .step_no").text();
		arr.push(jsonn);
	});
	var room = $("#operatingRoomName").val();
	var roomid = $("#operatingRoomId").val();
	var rwdid=$("#cellPassage_id").text();
	var bzname=$("#steptiele").text();
    var batch=$("#cellPassage_batch").text();
    var orderNum=$(".wizard_steps .selected .step_no").text();
    if(roomid!=""){
    		$.ajax({
		type : "post",
		url : "/experiment/cell/passage/cellPassage/startPrepare.action",
		data : {
			data : JSON.stringify(arr),
			roomName : room,
			roomId : roomid,
			rwdid: rwdid,
			bzname:bzname,
            batch:batch,
	        orderNum :orderNum,
		},
		success : function(res) {
			var data = JSON.parse(res);
			if (data.success) {
				if (data.zy) {
					top.layer.msg("仪器/房间已开启占用！");
				} else {
					top.layer.msg("仪器或房间已被占用请检查！");
				}
			} else {
				top.layer.msg("占用出现异常！");
			}
		}
	});
    }else{
    	top.layer.msg("请选择房间");
    	return false;
    }
}
// 原辅料和设备复制操作
// function reagentAndCosCopy() {
// $(".fa-copy").unbind("click").click(function() {
// var li = $(this).parent("li").clone();
// // 复制的是原辅料
// if(li.hasClass("reagli")) {
// li.attr("reagentid", ""); // id为空
// li.find(".label-primary span").text(""); // 批次为空
// li.find(".label-info span").text(""); // sn为空
// }
// // 复制的是设备
// if(li.hasClass("cosli")) {
// li.attr({
// "cosid": "",
// "code": ""
// }); // id为空
// li.children(".label-default").text(""); // 设备名称为空
// }
// $(this).parent("li").after(li);
// reagentAndCosCopy();
// reagentAndCosRemove();
// reagentAndCosChose();
// });
// }

// 原辅料和设备删除操作
// function reagentAndCosRemove() {
// $(".fa-trash").unbind("click").click(function() {
// var li = $(this).parent("li");
// if(li.hasClass("reagli")) {
// var id = li.attr("reagentid");
// if(id) {
// $.ajax({
// type: "post",
// url: ctx
// +"/experiment/cell/passage/cellPassage/delCellPassageReagent.action",
// data: {
// id: id,
// mainId: $("#cellPassage_id").text(),
// del: "细胞传代实验实验步骤：" +$(".wizard_steps .selected").find(".step_descr").text()
// +"删除原辅料："
// },
// success: function(data) {
// var data = JSON.parse(data);
// if(data.success) {
// top.layer.msg(biolims.common.deleteSuccess);
// }
// }
// });
// } else {
// li.remove();
// }
// }
// if(li.hasClass("cosli")) {
// var id = li.attr("cosid");
// if(id) {
// $.ajax({
// type: "post",
// url: ctx +"/experiment/cell/passage/cellPassage/delCellPassageCos.action",
// data: {
// id: id,
// mainId: $("#cellPassage_id").text(),
// del: "细胞传代实验实验步骤：" +
// $(".wizard_steps .selected").find(".step_descr").text() +"删除设备："
// },
// success: function(data) {
// var data = JSON.parse(data);
// if(data.success) {
// top.layer.msg(biolims.common.deleteSuccess);
// }
// }
// });
// } else {
// li.remove();
// }
// }
// });
// }

// 保存数据

function saveStepItem() {

	var benbu=$("ul.bwizard-steps").find("li.active").attr("note");
	var step1State="";
	var step2State="";
	var step3State="";
	var step4State="";
	if(benbu=="1"){
		
		
		step1State="0";
		var requiredField = templateRequiredFilter(benbu);		
		if (!requiredField) {
			top.layer.closeAll();
			return false;
		}
		// 判断是否选择设备
		var arr = [];
		var fuyong = true;
		// 判断是否选择试剂
		var arr1 = []
		var fuyong1 = true;
	
	
		$("#pageOneChoseYQ .btn-info").each(
				function(i, v) {
					var jsonn = {};
					jsonn.id = v.getAttribute("cosid");
					jsonn.code = v.getAttribute("code");
					jsonn.name = v.getAttribute("name");

					if (jsonn.name != null
							&& jsonn.name != "null"
							&& jsonn.name != "") {
					} else {
						fuyong = false;
						return false;
					}
					arr.push(jsonn);
				});
		
		if (fuyong == false) {
			top.layer.closeAll();
			top.layer.msg("请选择仪器！");
			return false;
		}
		
		$("#pageOneChoseSJ .btn-info").each(function(i, v) {
					var jsonn = {};
					jsonn.ph = v.getAttribute("ph");
					jsonn.yxq = v.getAttribute("yxq");

					if (jsonn.ph != null
							&& jsonn.ph != "null"
							&& jsonn.ph != "") {
					} else {
						fuyong1 = false;
						return false;
					}
					arr1.push(jsonn);
		});
		
		if (fuyong1 == false) {
			top.layer.closeAll();
			top.layer.msg("请扫描试剂！");
			return false;
		}
		// 判断是否选择
		if ($("#operatingRoomName").val() != null
				&& $("#operatingRoomName").val() != "") {
		} else {
			top.layer.closeAll();
			top.layer.msg("请选择房间！");
			return false;
		}
		
		var fuyong2=true;
		var mssg2;
		$(".pageOneRoomConfirm").each(function(i,v){
			if($(v).attr("state")!="1"){
				
				mssg2="房间确认的第"+(i+1)+"个按钮未确认";
				fuyong2=false;
				return false;
			}
		});
		if(fuyong2 == false){
			top.layer.closeAll();
			top.layer.msg(mssg2);
			return false;
		}
		var fuyong3=true;
		var mssg3;
		$(".pageOneQRcodesample").each(function(i,v){
			if($(v).attr("state")!="已确认"){
				
				mssg3="样本条码确认的第"+(i+1)+"个核对结果未确认";
				fuyong3=false;
				return false;
			}
		});
		if(fuyong3 == false){
			top.layer.closeAll();
			top.layer.msg(mssg3);
			return false;
		}
		
		var fuyong4=true;
		var mssg4;
		$(".pageOneQRcodeoperation").each(function(i,v){
			if($(v).attr("state")!="已确认"){
				
				mssg4="操作条码确认的第"+(i+1)+"个核对结果未确认";
				fuyong4=false;
				return false;
			}
		});
		if(fuyong4 == false){
			top.layer.closeAll();
			top.layer.msg(mssg4);
			return false;
		}
		$("#pageOneChoseYQ button").each(function(i,v){
			if($(v).attr("operationstate")!="1"){
				
				mssg4="第"+(i+1)+"个选定的设备未确认";
				fuyong4=false;
				return false;
			}
		});
		if(fuyong4 == false){
			top.layer.closeAll();
			top.layer.msg(mssg4);
			return false;
		}
		$(".pageOneOperationalOrder").each(function(i,v){
			if($(v).attr("state")!="1"){
				
				mssg4="操作指令的第"+(i+1)+"个按钮未确认";
				fuyong4=false;
				return false;
			}
		});
		if(fuyong4 == false){
			top.layer.closeAll();
			top.layer.msg(mssg4);
			return false;
		}
		
		//判断生产记录是否选择完毕
		$("#firstTab").find("#pageOneProductionRecord .form-group").each(function(i,v){
			$(v).find("input").each(function(i,vv){
				if($(vv).val()!=""){
					
				}else{
					mssg4=$(vv).siblings("label").text()+"不能为空";
					fuyong4=false;
					return false;
				}
			});
			if(fuyong4 == false){
				return false;
			}
		});
		if(fuyong4 == false){
			top.layer.closeAll();
			top.layer.msg(mssg4);
			return false;
		}
		step1State="1";
	}else if(benbu=="3"){
		step2State="0";
		var requiredField = templateRequiredFilter(benbu);		
		if (!requiredField) {
			top.layer.closeAll();
			return false;
		}
		
		var fuyong5=true;
		var mssg5;
		if($("#thirdTab").find("#productStartTime").text()==""){
			
			mssg5="生产开始时间未选择";
			top.layer.closeAll();
			top.layer.msg(mssg5);
			return false;
		}
		if($("#thirdTab").find("#productEndTime").text()==""){
		
			mssg5="生产结束时间未选择";
			top.layer.closeAll();
			top.layer.msg(mssg5);
			return false;
		}
		
		$("#thirdTab .caozuoItem").find(".time").find("button").each(function(i,v){
			if($(v).children().eq(1).text()==""){
				fuyong5 = false;
				mssg5="生产开始或结束时间未选择";
				return false;
			}
		});
		if(fuyong5 == false){
			top.layer.closeAll();
			top.layer.msg(mssg5);
			return false;
		}
		
		$(".timeline-footer #reagentBody .reagli").each(function(i,v){
			if($(v).find("input").eq(0).val()==""){
				
				fuyong5 = false;
				mssg5=$(v).find("input").eq(0).prev().text()+"未填写";
				return false;
			}
		});
		if(fuyong5 == false){
			top.layer.closeAll();
			top.layer.msg(mssg5);
			return false;
		}
		
		$("#thirdTab .timeline-item").find("#cosBody .cosli").each(function(i,v){
			if($(v).find(".label").text()==""){
				
				fuyong5 = false;
				mssg5=$(v).find(".label").prev().text()+"未选择";
				return false;
			}
		});
		if(fuyong5 == false){
			top.layer.closeAll();
			top.layer.msg(mssg5);
			return false;
		}
		$("#thirdTab .timeline-item").find("#zhijianBodyNew .input-group").each(function(i,v){
			if($(v).find("input").val()==""){
				
				fuyong5 = false;
				mssg5=$(v).siblings("small").find(".zhijianName").text()+$(v).find("input").prev().text()+"未填写";
				return false;
			}
		});
		if(fuyong5 == false){
			top.layer.closeAll();
			top.layer.msg(mssg5);
			return false;
		}
		step2State="1";
		
	}else if(benbu=="2"){
		step3State="0";
	
		var requiredField = templateRequiredFilter(benbu);		
		if (!requiredField) {
			top.layer.closeAll();
			return false;
		}
		var fuyong6=true;
		var mssg6;
		$("#zhijianDetails tbody tr").find("td[savename='submit']").each(function(i,v){
			if($(v).text()!="是"){
				
				mssg6="质检明细表的第"+(i+1)+"条数据未提交";
				fuyong6=false;
				return false;
			}
		});
		if(fuyong6 == false){
			top.layer.closeAll();
			top.layer.msg(mssg6);
			return false;
		}	
		step3State="1";
	}else if(benbu=="4"){
		step4State="0";
		//判断操作指令是否选择完毕
		var fuyong2=true;
		var mssg2;
		$(".pageFourOperationalOrder").each(function(i,v){
			if($(v).attr("state")!="1"){
				
				mssg2="操作指令的第"+(i+1)+"个按钮未确认";
				fuyong2=false;
				return false;
			}
		});
		if(fuyong2 == false){
			top.layer.closeAll();
			top.layer.msg(mssg2);
			return false;
		}
		//判断操作指令是否选择完毕
		$("#fourTab").find("#pageFourProductionRecord .form-group").each(function(i,v){
			if($(v).find("input").val()==""){
				
				mssg2=$(v).find("input").prev().text()+"不能为空";
				fuyong2=false;
				$(v).find("input").focus();
				return false;
			}
		});
		if(fuyong2 == false){
			top.layer.closeAll();
			top.layer.msg(mssg2);
			return false;
		}
		step4State="1";	
	}
	var states= new Array();
	states[0]=step1State;//第一步
	states[1]=step2State;//第二步
	states[2]=step3State;//第三步
	states[3]=step4State;//第四步
	var stepidVal=$("#wizard_verticle ul.wizard_steps").find("a.selected").attr("stepid");
	$.ajax({
		url:ctx+"/experiment/cell/passage/cellPassageNow/saveStepState.action",
		type:"post",
		async:false,
		data:{
			id:stepidVal,
			state:states,
		},
		success:function(){
			
		}
	});
	
	var stepId = $(".bwizard-steps>.active").attr("note");
	var action;
	var saveData;

	
	if (stepId == "1") { // 工前准备
		// 判断是否选择设备
		var arr = [];
		var fuyong = true;
		// 判断是否选择试剂
		var arr1 = [];
		var fuyong1 = true;
		
		//试剂数量
		var reg = /^[0-9]*$/;
		if($("#sl").val()!=""&&$("#sl").val()!=null){
		
		if(!reg.test($("#sl").val()) ){
			top.layer.msg("请填写正确的试剂数量(数字)!");
			return false;
		}
		
		}

		$("#pageOneChoseYQ .btn-info").each(function(i, v) {
			var jsonn = {};
			jsonn.id = v.getAttribute("cosid");
			jsonn.code = v.getAttribute("code");
			jsonn.name = v.getAttribute("name");

			if (jsonn.name != null && jsonn.name != "null" && jsonn.name != "") {
			} else {
				fuyong = false;
				return false;
			}
			arr.push(jsonn);
		});
		var requiredField = templateRequiredFilter(stepId);		
		if (!requiredField) {	
			return false;
		}	
		var changeLog = "细胞生产：";
		$('input[class="form-control"]').each(function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
			}
		});
		
		var changeLogs = "";
		if(changeLog != "细胞生产：") {
			changeLogs = changeLog;
			$("#changelog").val(changeLogs);
		}
		
		
		if (fuyong == false) {
			top.layer.msg("请选择设备");
			return false;
		}
		

		$("#pageOneChoseSJ .btn-info").each(
				function(i, v) {
					var jsonn = {};
					jsonn.ph = v.getAttribute("ph");
					jsonn.yxq = v.getAttribute("yxq");

					if (jsonn.ph != null
							&& jsonn.ph != "null"
							&& jsonn.ph != "") {
					} else {
						fuyong1 = false;
						return false;
					}
					arr1.push(jsonn);
		});
		if (fuyong1 == false) {
			top.layer.msg("请选择试剂");
			return false;
		}

		// 判断是否选择
		if ($("#operatingRoomName").val() != null
				&& $("#operatingRoomName").val() != "") {
		} else {
			top.layer.msg("请选择房间");
			return false;
		}
		// 操作指令
		var stepanduan=true;
		var requiredField = templateRequiredFilter(stepId);		
		if (!requiredField) {	
			return false;
		}
		
		var operationalOrder = [];
		$("#pageOneOperationalOrder .pageOneOperationalOrder").each(
				function(i, v) {
					var obj = {};
					obj.id = v.id;
					obj.productionInspection = v.getAttribute("state") ? v
							.getAttribute("state") : "0";
					obj.operationNotes = v.getAttribute("note") ? v
							.getAttribute("note") : "";
					operationalOrder.push(obj);
				})
		// 生产记录
		var productionRecord = {
			guohkId : $("#pageOneProductionRecord").attr("pid")
		};
		$("#pageOneProductionRecord input").each(function(i, v) {
			productionRecord[v.id] = v.value;
		});
		var productionReagent = [];
		$("#pageOneChoseSJ button").each(
				function(i, v) {
					var obj = {};
					obj.id = v.id;
					obj.reagentNo = v.getAttribute("ph") ? v
							.getAttribute("ph") : "";
					obj.reagentValidityTime = v.getAttribute("yxq") ? v
							.getAttribute("yxq") : "";
							
					obj.reagentNum = $(v).parent().parent().find("#sl").val();
					obj.storageItemId = $(v).parent().parent().find("#storageItemId").val();
					productionReagent.push(obj);
		});
		// 判断是否选择设备
		var cellproductCos = [];

		$("#pageOneChoseYQ .btn-info").each(function(i, v) {
			var jsonn = {};
			jsonn.id = v.getAttribute("cosid");
			jsonn.code = v.getAttribute("code");
			jsonn.name = v.getAttribute("name");
			jsonn.operationState = v.getAttribute("operationstate");
            
			cellproductCos.push(jsonn);
		});
		
		// 房间核查保存
		var cellRoomPass = [];
		
		$("#pageOneRoomConfirm .pageOneRoomConfirm").each(function(i, v) {
			
			var jsonn = {};
			jsonn.id = v.getAttribute("id");
			jsonn.state = v.getAttribute("state");
            
			cellRoomPass.push(jsonn);
		});
		
		
		var template = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			stepId : stepId,
			temNstr : operationalOrder,
			content : productionRecord,
			productionReagent : productionReagent,
			cellproductCos :cellproductCos,
			cellRoomPass : cellRoomPass
		};
		saveData = JSON.stringify(template);
		action = "/experiment/cell/passage/cellPassageNow/savepreparationProductionRecord.action";
	} else if (stepId == "2") {
		// 拼质检的数据
		var requiredField = templateRequiredFilter(stepId);		
		if (!requiredField) {	
			return false;
		}
		var noBlendTabledata = savenoBlendTablejson($("#noBlendTable"));
		var blendTabledata = savenoBlendTablejson($("#blendTable"));
		//质检日志
		var datas = savenoBlendTablejson($("#blendTable"));
		var changeLogItem = "质检：";
		changeLogItem = getChangeLog(datas, $("#blendTable"), changeLogItem);
		var changeLogItemLast = "";
		if (changeLogItem != "质检：") {
			changeLogItemLast = changeLogItem
		}
		
		var zhijian = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			stepId : stepId,
			noBlendTabledata : noBlendTabledata,
			blendTabledata : blendTabledata,
		};
		saveData = JSON.stringify(zhijian);
		action = "/experiment/cell/passage/cellPassageNow/saveQualityInspectionResults.action";
	} else if (stepId == "3") { 
		// 生产操作
		var requiredField = templateRequiredFilter(stepId);		
		if (!requiredField) {	
			return false;
		}
		var arr = [];
		$(".caozuoItem").each(
				function(i, v) {
					var obj = {};
					obj.id = v.getAttribute("czid");
					obj.state = v.getAttribute("state") ? v
							.getAttribute("state") : "0";
					obj.startTime = $(v).find("#btn_startTime .stepsTime")
							.text(); // $("#btn_startTime
					// .stepsTime").text();
					obj.endTime = $(v).find("#btn_endTime .stepsTime").text(); // $("#btn_endTime
					// .stepsTime").text();
					// 拼原辅料的数据
					var reagentli = $(v).find(".reagli");
					var reagent = [];
					reagentli.each(function(i, val) {
						var reagentItem = {};
						reagentItem.id = val.getAttribute("reagentid");
						reagentItem.name = $(val).children(".text").text();
						reagentItem.code = val.getAttribute("code");
						reagentItem.batch = $(val).children(".label-primary")
								.children("span").text();
						reagentItem.sn = "";
						// $(val).children(".label-info")
						// .children("span").text();
						reagentItem.inputVal = $(val).find(".input-sm").val();
						reagentItem.unit = $(val).find(".unit").val();
						reagent.push(reagentItem);
					});
					obj.reagent = reagent;
//					var reg = /^[0-9]*$/;
//					if(!reg.test(obj.reagent[0].inputVal) && obj.reagent[0].inputVal!=""){
//						top.layer.msg("请填写正确的试剂数量!");
//						return false;
//					}
					// 拼设备的数据
					var cosli = $(v).find(".cosli");
					var cos = [];
					cosli
							.each(function(i, val) {
								var cosItem = {};
								cosItem.id = val.getAttribute("cosid");
								cosItem.name = $(val)
										.children(".label-warning").text();
								cosItem.code = val.getAttribute("code");
								cosItem.typeId = val.getAttribute("typeid");
								cos.push(cosItem);
							});
					obj.cos = cos;

					var result1 = {}; // 自定义字段
					var resultValue = ""; // 自定义字段值
					$("#pageThree" + obj.id + " input").each(
							function(z, x) {
								result1[x.id] = x.value;
								resultValue = resultValue + $(x).prev().text()
										+ ":" + x.value + "\n";
							});
					obj.customTest = result1;
					obj.customTestValue = resultValue;

					arr.push(obj)
				});
		var result = {}; // 自定义字段
		$("#pageThree input").each(function(i, v) {
			result[v.id] = v.value;
		})
		var template = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			stepId : stepId,
			result : result,
			content : arr,
			productStartTime : $("#productStartTime").text(),
			productEndTime : $("#productEndTime").text(),
			notes : $("#notes").val(),
		// startime:$("#btn_startTime .stepsTime").text(),
		// endtime:$("#btn_endTime .stepsTime").text()
		};
		saveData = JSON.stringify(template);
		action = "/experiment/cell/passage/cellPassageNow/saveCosandReagent.action";
	} else if (stepId == "4") {
		var requiredField = templateRequiredFilter(stepId);		
		if (!requiredField) {	
			return false;
		}
		// 操作指令
		var operationalOrder = [];
		$("#pageFourOperationalOrder .pageFourOperationalOrder").each(
				function(i, v) {
					var obj = {};
					obj.id = v.id;
					obj.productionInspection = v.getAttribute("state") ? v
							.getAttribute("state") : "0";
					operationalOrder.push(obj);
				})
		// 身产记录
		var productionRecord = {
			guohkId : $("#pageFourProductionRecord").attr("pid")
		};
		$("#pageFourProductionRecord input").each(function(i, v) {
			productionRecord[v.id] = v.value;
		});
		var template = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			stepId : stepId,
			temNstr : operationalOrder,
			content : productionRecord,
		};
		saveData = JSON.stringify(template);
		action = "/experiment/cell/passage/cellPassageNow/savefourthStepCellproductResult.action";
	}
	
	
	
	$.ajax({
		type : "post",
		url : ctx + action,
		data : {
			"saveData" : saveData,
			changeLog:changeLog,
			changeLogItem:changeLogItemLast
		},
		success : function(data) {
			var data = JSON.parse(data);
			if (data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				$(".bwizard-steps .active>a").click();
			} else {
				top.layer.closeAll();
			}
		}
	});
}
// 获得保存时的json数据
function savenoBlendTablejson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");

			// 是否合格
			if (k == "qualified") {
				var qualified = $(tds[j]).text();
				if (qualified == "合格") {
					json[k] = "2";
				} else if (qualified == "不合格") {
					json[k] = "1";
				} else if (qualified == "警戒") {
					json[k] = "3";
				} else {
					json[k] = "";
				}
				continue;
			}

			// 板子
			if (k == "counts") {
				json["instrument-id"] = $(tds[j]).attr("instrument-id");
			}
			
			if (k == "countsOld") {
				json["instrumentOld-id"] = $(tds[j]).attr("instrumentOld-id");
			}
			
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
// 上一步下一步操作
function preAndNext() {
	// 上一步操作
	$("#prev")
			.click(
					function() {
						$("#maincontentframe", window.parent.document)[0].src = window.ctx
								+ "/experiment/cell/passage/cellPassage/editCellPassage.action?id="
								+ cellPassage_id
								+ "&bpmTaskId="
								+ $("#bpmTaskId").val();
					});
	// 下一步操作
	$("#next")
			.click(
					function() {
						$("#maincontentframe", window.parent.document)[0].src = window.ctx
								+ "/experiment/cell/passage/cellPassage/showCellPassageResultTable.action?id="
								+ cellPassage_id
								+ "&orderNum="
								+$(".wizard_steps .selected .step_no").text()
								+ "&bpmTaskId="
								+ $("#bpmTaskId").val();
					});
}

function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#cellPassage_id").text();

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "1") {
				top.layer.msg(biolims.dna.pleaseFinish);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}

	});
}

// 判断如果是列表页进来的，直接跳到当前步骤
function renderOrderNums() {
	

	var dijibu = sessionStorage.getItem("dijibu");
	if (dijibu) {
		var orderNums = dijibu;
	} else {
		var orderNums = $("#orderNumBy").val();
	}

	if (orderNums && orderNums != 1) {
		$(".wizard_steps .step").each(function(i, v) {
			if ($(v).children(".step_no").text() == orderNums) {
				$(v).click();
			} else {
				if (adminUser) {

				} else {
					$(v).unbind("click");
				}
			}
		});
	} else {
		$(".wizard_steps .step").each(function(i, v) {
			if ($(v).children(".step_no").text() == orderNums) {
				$(v).click();
			} else {
				if (adminUser) {

				} else {
					$(v).unbind("click");
				}
			}
		});
	}
}

// 选择实验员
function showdutyManId() {
	top.layer.open({
		title : biolims.common.pleaseChoose,
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/core/user/selectUserTable.action?groupId=admin",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addUserTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index);
			$("#testUserList").val(name);
		},
	})
}
// 选择检测项
function showSampledetecyion() {
	top.layer
			.open({
				title : biolims.common.pleaseChoose,
				type : 2,
				area : [ document.body.clientWidth - 300,
						document.body.clientHeight - 100 ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/system/detecyion/sampleDeteyion/showSampledetecyion.action?",
						'' ],
				yes : function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addSampleDeteyionTable .chosed").children("td")
							.eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addSampleDeteyionTable .chosed").children("td")
							.eq(0).text();
					top.layer.close(index);
					$("#sample_detecyion_id").val(id);
					$("#sample_detecyion_name").val(name);
				},
			})
}

// 打印
function printTempalte() {
	var id = $("#cellPassage_id").text();
	var orderNum = $(".wizard_steps .selected .step_no").text();
	
	// 判断当前步骤是否是重复步骤
	$.ajax({
		type : "post",
		url : ctx
				+ "/experiment/cell/passage/cellPassage/getStepChongfu.action",
		data : {
			id : id,
			orderNum : orderNum
		},
		success : function(data) {
			var data = JSON.parse(data);
			if (data.success) {
				if (data.chongfu) {
					$.ajax({
						type : "post",
						data : {
							id : $("#sampleReveice_id").text(),
							confirmDate : $("#cellPassage_createDate").text(),
							modelId:"scrwlbcf",
						},
						url : ctx
								+ "/stamp/birtVersion/selectBirt.action",
						success : function(data) {
							var data = JSON.parse(data)
							if (data.reportN) {
								var url = '__report='+data.reportN+'&id=' + id;	
								commonPrint(url);
							} else {
								top.layer.msg("没有报表信息！");
							}
						}
					});

				} else {
					
					$.ajax({
						type : "post",
						data : {
							id : $("#sampleReveice_id").text(),
							confirmDate : $("#cellPassage_createDate").text(),
							modelId:"scrwlb",
							oNum : orderNum,
						},
						url : ctx
								+ "/stamp/birtVersion/selectBirt.action",
						success : function(data) {
							var data = JSON.parse(data)
							if (data.reportN) {
								var url = '__report='+data.reportN+'&id=' + id;	
								commonPrint(url);
							} else {
								top.layer.msg("没有报表信息！");
							}
						}
					});
//					var url = '__report=templateitem.rptdesign&id=' + id
//							+ '&ordernum=' + orderNum;
			
				}
			} else {
				top.layer.msg("当前步骤数据存在问题，请联系管理员");
			}
		}
	});

}

function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '', '');
}
// 上传附件
function fileInput1() {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $("#pageFourProductionRecord").attr("pid");
	$("#uploadFileValx")
			.fileinput(
					{
						uploadUrl : ctx
								+ "/attachmentUpload?useType=ddfj&modelType=cellProductionCompleted&contentId="
								+ id,
						uploadAsync : true,
						language : 'zh',
						showPreview : true,
						enctype : 'multipart/form-data',
						elErrorContainer : '#kartik-file-errors',
					});
	$("#uploadFilex").modal("show");
}
// 查看附件
function fileView() {
	top.layer
			.open({
				title : biolims.common.attachment,
				type : 2,
				skin : 'layui-layer-lan',
				area : [ document.body.clientWidth - 300,
						document.body.clientHeight - 100 ],
				content : window.ctx
						+ "/operfile/initFileList.action?flag=ddfj&modelType=cellProductionCompleted&id="
						+ $("#pageFourProductionRecord").attr("pid"),
				cancel : function(index, layer) {
					top.layer.close(index)
				}
			})
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function xzTime(){
	var sDate =new Date()
	sDate =dateFtt("hh:mm",sDate);
	var hour = sDate.split(':')[0];
    var min = sDate.split(':')[1];
 
    var n = Number(hour*3600) + Number(min*60)+Number(30*60);
    var endDate =arrive_timer_format(n)
    var a =sDate+"-"+endDate
   $("#ultravioletlamptime").val(a)
	 

}
function dateFtt(fmt,date) {
 var o = { 
 "M+" : date.getMonth()+1,     //月份 
 "d+" : date.getDate(),     //日 
 "h+" : date.getHours(),     //小时 
 "m+" : date.getMinutes(),     //分 
 "s+" : date.getSeconds(),     //秒 
 "q+" : Math.floor((date.getMonth()+3)/3), //季度 
 "S" : date.getMilliseconds()    //毫秒 
 }; 
 if(/(y+)/.test(fmt)) 
 fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length)); 
 for(var k in o) 
 if(new RegExp("("+ k +")").test(fmt)) 
 fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length))); 
 return fmt; 
}
function arrive_timer_format(s) {
	var t;
	if(s > -1){
	    hour = Math.floor(s/3600);
	    min = Math.floor(s/60) % 60;
	    sec = s % 60;
	    day = parseInt(hour / 24);
	    if (day > 0) {
	        hour = hour - 24 * day;
	        t = day + "day " + hour + ":";
	        }
	    else t = hour + ":";   
	    if(min < 10){t += "0";}
	        t += min;
	}
	return t;
	}


//必填验证
function templateRequiredFilter(step) {
	var flag=true;
	var a;
	if(step=="1"){
		a= $("#firstTab .requiredimage")
	}else if(step=="2"){
		a= $("#secondTab .requiredimage")
	}else if(step=="3"){
		a= $("#thirdTab .requiredimage")
	}else if(step=="4"){
		a= $("#fourTab .requiredimage")
	}
	console.log(a)
	a.each(function(i, v) {
		var ipt = $(v).parent(".input-group-addon").siblings("input[type='text']");
		var ipt2 = $(v).parent(".input-group-addon").siblings("input[type='number']");
		var ipt3 = $(v).parent(".input-group-addon").siblings("input[type='date']");
		var ipt1 = $(v).parent(".input-group-addon").siblings("select");
		var ipt4 = $(v).parent(".lab").siblings("input[type='text']");

		if(ipt1.length) {
			if(!ipt1.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				ipt1.focus();
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
		if(ipt4.length) {
			if(!ipt4.val()) {
				var title = $(v).parent(".lab").find(".labTwo").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				ipt1.focus();
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
		
		if(ipt.length) {
			if(!ipt.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				ipt.focus();
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
		if(ipt3.length) {
			if(!ipt3.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				ipt3.focus();
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
		if(ipt2.length) {
			if(!ipt2.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				ipt2.focus();
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
	});
	return flag;
}
function templateRequiredFilter1() {
	var flag=true;
	$(".requiredimage").each(function(i, v) {
		var ipt = $(v).parent(".input-group-addon").siblings("input[type='text']");
		var ipt2 = $(v).parent(".input-group-addon").siblings("input[type='number']");
		var ipt3 = $(v).parent(".input-group-addon").siblings("input[type='date']");
		var ipt1 = $(v).parent(".input-group-addon").siblings("select");

		if(ipt1.length) {
			if(!ipt1.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}

		
		if(ipt.length) {
			if(!ipt.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
		if(ipt3.length) {
			if(!ipt3.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
		if(ipt2.length) {
			if(!ipt2.val()) {
				var title = $(v).parent(".input-group-addon").text();
				var setTop = $(v).offset().top;
				$("body").animate({
					scrollTop: setTop-200
				}, 500);
				top.layer.msg(biolims.common.pleaseFillInfo1 + title);
				flag=false;
				return false;
			}
		}
	});
	return flag;
}
//环境监测查看
function enmonitor(type){
	if($("#operatingRoomId").val()==null||$("#operatingRoomId").val()==""){
		top.layer.msg("请先选择房间！");
	}else{
		$.ajax({
			type : "post",
			url : ctx
					+ "/experiment/cell/passage/cellPassageNow/findEnmonitor.action",
			data : {
				operatingRoomId : $("#operatingRoomId").val(),
				estimatedDateTime : $(".wizard_steps .selected .estimatedDateTime").text(),
				type:type
			},
			success : function(data) {
				var data = JSON.parse(data);
				if (data.success) {
					if(data.roomstate){
						if(type=="1"){
							//查询该生产步骤时间最近一次尘埃粒子监测记录
							window.open(window.ctx+'/experiment/enmonitor/dust/cleanAreaDust/showCleanAreaDustEdit.action?id='+data.taskid);
//							window.open(window.ctx+'/experiment/cell/passage/cellPassage/showCleanAreaDustEdit.action?id='+data.taskid);
						}else if(type=="2"){
							//查询该生产步骤时间最近一次浮游菌监测记录
							window.open(window.ctx+'/experiment/enmonitor/microbe/cleanAreaMicrobe/showCleanAreaMicrodeEdit.action?id='+data.taskid);
//							window.open(window.ctx+'/experiment/cell/passage/cellPassage/showCleanAreaMicrodeEdit.action?id='+data.taskid);
						}else if(type=="3"){
							//查询该生产步骤时间最近一次风量监测记录
							window.open(window.ctx+'/experiment/enmonitor/volume/cleanAreaVolume/showCleanAreaVolumeEdit.action?id='+data.taskid);
//							window.open(window.ctx+'/experiment/cell/passage/cellPassage/showCleanAreaVolumeEdit.action?id='+data.taskid);
						}else if(type=="4"){
							//查询该生产步骤时间最近一次沉降菌监测记录
//							window.open(window.ctx+'/experiment/enmonitor/celanAreaBacteria/showCleanAreaBacteriaEdit.action?id='+data.taskid);
							window.open(window.ctx+'/experiment/cell/passage/cellPassage/showCleanAreaBacteriaEdit.action?id='+data.taskid);
						}else if(type=="5"){
							//查询该生产步骤时间最近一次表面微生物监测记录
//							window.open(window.ctx+'/experiment/enmonitor/microorganism/showMicroorganismEdit.action?id='+data.taskid);
							window.open(window.ctx+'/experiment/cell/passage/cellPassage/showMicroorganismEdit.action?id='+data.taskid);
						}else if(type=="6"){
							//查询该生产步骤时间最近一次压差监测记录
//							window.open(window.ctx+'/experiment/enmonitor/differentialpressure/showDifferentialpressureEdit.action?id='+data.taskid);
							window.open(window.ctx+'/experiment/cell/passage/cellPassage/showDifferentialpressureEdit.action?id='+data.taskid);
						}
					}else{
						if(type=="1"){
							//查询该生产步骤时间最近一次尘埃粒子监测记录
							top.layer.msg("未查到该房间的尘埃粒子监测记录！");
						}else if(type=="2"){
							//查询该生产步骤时间最近一次浮游菌监测记录
							top.layer.msg("未查到该房间的浮游菌监测记录！");
						}else if(type=="3"){
							//查询该生产步骤时间最近一次风量监测记录
							top.layer.msg("未查到该房间的风量监测记录！");
						}else if(type=="4"){
							//查询该生产步骤时间最近一次沉降菌监测记录
							top.layer.msg("未查到该房间的风量监测记录！");
						}else if(type=="5"){
							//查询该生产步骤时间最近一次表面微生物监测记录
							top.layer.msg("未查到该房间的表面微生物监测记录！");
						}else if(type=="6"){
							//查询该生产步骤时间最近一次压差监测记录
							top.layer.msg("未查到该房间的压差监测记录！");
						}
					}
				} else {
					if(type=="1"){
						//查询该生产步骤时间最近一次尘埃粒子监测记录
						top.layer.msg("未查到该房间的尘埃粒子监测记录！");
					}else if(type=="2"){
						//查询该生产步骤时间最近一次浮游菌监测记录
						top.layer.msg("未查到该房间的浮游菌监测记录！");
					}else if(type=="3"){
						//查询该生产步骤时间最近一次风量监测记录
						top.layer.msg("未查到该房间的风量监测记录！");
					}else if(type=="4"){
						//查询该生产步骤时间最近一次沉降菌监测记录
						top.layer.msg("未查到该房间的风量监测记录！");
					}else if(type=="5"){
						//查询该生产步骤时间最近一次表面微生物监测记录
						top.layer.msg("未查到该房间的表面微生物监测记录！");
					}else if(type=="6"){
						//查询该生产步骤时间最近一次压差监测记录
						top.layer.msg("未查到该房间的压差监测记录！");
					}
				}
			}
		});
	}
}
