
$(function() {
	var searchModel = '<div class="input-group"><input type="text" class="form-control" placeholder="Search..." id="searchvalue" onkeydown="entersearch()"><span class="input-group-btn"> <button class="btn btn-info" type="button" onclick="searchModel()">按名称搜索</button></span> </div>';	
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "生产单号",
	});
	colOpts.push({
		"data": "batch",
		"title": "产品批号",
	});
	colOpts.push({
		"data": "stateName",
		"title": "状态",
	});
	
	var tbarOpts = [];
	var options =table(false, null,'/experiment/cell/passage/cellPassageNow/showCellPassageJson.action', colOpts, tbarOpts)
	
	
	addUnitGroup = renderData($("#showCellPassage"), options);
	$("#showCellPassage").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#showCellPassage_wrapper .dt-buttons").html(searchModel);
//			$("#showCellPassage_wrapper .dt-buttons").empty();
			$('#showCellPassage_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#showCellPassage tbody tr");
			addUnitGroup.ajax.reload();
			addUnitGroup.on('draw', function() {
				trs = $("#showCellPassage tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});
	
	

})

//回车搜索
function entersearch(){  
    var event = window.event || arguments.callee.caller.arguments[0];  
    if (event.keyCode == 13)  
    {  
        searchModel();
    }  
} 
//搜索方法
function searchModel() {
	addUnitGroup.settings()[0].ajax.data = {
		query: JSON.stringify({
			id:"%"+$("#searchvalue").val()+"%"
		})
	};
	addUnitGroup.ajax.reload();
}
