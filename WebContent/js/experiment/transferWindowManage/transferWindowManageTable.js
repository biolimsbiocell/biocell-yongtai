var transferWindowManageTable;
$("#btn_view").hide();
$(function() {
	$("#btn_view").hide();
	var colData = [{
			"data": "id",
			"title": "传递窗编号",
		}, {
			"data": "transferWindowName",
			"title": "传递窗名称",
		}, {
			"data": "name",
			"title": "备注",
		}, {
			"data": "transferWindowState",
			"title": "传递窗状态",
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return "可以使用";
				}
				if(data == "0") {
					return "不可以使用";
				}
				else {
					return '';
				}
			}
		}, /* {
			"data": "isFull",
			"title": "占用状态",
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return "占用";
				}
				if(data == "0") {
					return "未占用";
				}
				else {
					return data;
				}
			}
		},*/ {
			"data": "describe",
			"title": "传递窗描述",
		}, {
			"data": "regionalManagement-name",
			"title": "区域",
		}, {
			"data": "createUser-name",
			"title": "创建人",
		}, {
			"data": "createDate",
			"title": "创建日期",
		}, {
			"data": "stateName",
			"title": "状态",
		},/*{
			"data": "testingTime",
			"title": "测试时间",
		},*/{
			"data": "validityPeriod",
			"title": "结果时间的有效期",
		}];
		
	var options = table(true, "",
		"/experiment/transferWindowManage/transferWindowManage/showTransferWindowManageTableJson.action",colData , null)
	transferWindowManageTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(transferWindowManageTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/experiment/transferWindowManage/transferWindowManage/editTransferWindowManage.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/experiment/transferWindowManage/transferWindowManage/editTransferWindowManage.action?id=' + id ;
}
// 查看
function view() {
//	var id = $(".selected").find("input").val();
//	if(id == "" || id == undefined) {
//		top.layer.msg(biolims.common.selectRecord);
//		return false;
//	}
//	window.location = window.ctx +
//		'/experiment/transferWindowManage/transferWindowManage/viewTransferWindowManage.action?id=' + id + '&type=' + $("#order_type").val();

}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "传递窗编号",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "传递窗名称",
			"type": "input",
			"searchName": "transferWindowName"
		},
		{
			"txt": "创建人",
			"type": "input",
			"searchName": "createUser-name"
		},
		{
			"type": "table",
			"table": transferWindowManageTable
		}
	];
}