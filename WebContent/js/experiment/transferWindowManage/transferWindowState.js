var transferWindowStateTable;
var oldtransferWindowStateChangeLog;
$(function() {
	// 加载子表
	var id = $("#transferWindowManage_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data":"id",
		"title": "id",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false,	
		
	});
	colOpts.push({
		"data":"code",
		"title": "检验样本编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
	    }
	});
	colOpts.push({
		"data":"batch",
		"title": "批次号",
		"createdCell": function(td) {
			$(td).attr("saveName", "batch");
	    },
	});
	colOpts.push({
		"data":"sampleDetecyion-id",
		"title": "sampleDetecyionId",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleDeteyion-id");
	    },
		"visible": false,	
		
	});
	colOpts.push({
		"data":"sampleDetecyion-name",
		"title": "检测项",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleDetecyion-name");
	    }
	});
	colOpts.push({
		"data":"qualitySubmitTime",
		"title": "提交时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "qualitySubmitTime");
	    }
	});
	colOpts.push({
		"data":"qualitySubmitUser",
		"title": "提交人",
		"createdCell": function(td) {
			$(td).attr("saveName", "qualitySubmitUser");
	    }
	});
	colOpts.push({
		"data":"qualityReceiveTime",
		"title": "QC扫码时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "qualityReceiveTime");
	    }
	});
	colOpts.push({
		"data":"qualityReceiveUser",
		"title": "QC扫码人",
		"createdCell": function(td) {
			$(td).attr("saveName", "qualityReceiveUser");
	    }
	});
	// 样本状态  1合格0不合格  是否超过30分钟 
	colOpts.push({
		"data":"sampleState",
		"title": "样本状态",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleState");
	    },
	    "render":function(data){
	    	if(data=="1"){
	    		return "合格";
	    	}else if(data=="0"){
	    		return "不合格";
	    	}else{
	    		return "";
	    	}
	    }
	});
	// 数据显示 1显示0隐藏
	colOpts.push({
		"data":"state",
		"title": "数据显示",
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
	    "render":function(data){
	    	if(data=="1"){
	    		return "显示";
	    	}else if(data=="0"){
	    		return "隐藏";
	    	}else{
	    		return "";
	    	}
	    }
	});
/*	colOpts.push({
		"data":"transferWindowManage-id",
		"title": "传递窗编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "TransferWindowManage-id");
	    },
		"visible": false,	
		
	});*/
	/*colOpts.push({
		"data":"stageName",
		"title": "占用实验名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "stageName");
	    },
	    "visible": false,	
	});
	colOpts.push({
		"data":"tableTypeId",
		"title": "占用实验单号",
		"createdCell": function(td) {
			$(td).attr("saveName", "tableTypeId");
	    },
	    "visible": false,	
	});*/

/*	colOpts.push({
		"data":"name",
		"title": "患者姓名",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
	});*/
	/*colOpts.push({
		"data":"note",
		"title": "占用实验阶段",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
	});*/
	/*colOpts.push({
		"data":"note2",
		"title": "占用实验阶段名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "note2");
	    },
	});
	colOpts.push({
		"data":"acceptUser",
		"title": "操作人",
		"createdCell": function(td) {
			$(td).attr("saveName", "acceptUser");
	    },
	});*/
/*	colOpts.push({
		"data":"startDate",
		"title": "开始时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "startDate");
	    },
	});
	colOpts.push({
		"data":"endDate",
		"title": "结束时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "endDate");
	    },
	});
	colOpts.push({
		"data":"transferWindowState",
		"title": "设备状态",
		"createdCell": function(td) {
			$(td).attr("saveName", "transferWindowState");
	    },
	});*/
	if (handlemethod != "完成") {
		tbarOpts.push({
			text: biolims.common.search,
			action: function() {
				search();
			}
		});
	}
	var handlemethod = $("#handlemethod").val();
	
	var transferWindowStateOptions = table(true,
		id,
		'/experiment/transferWindowManage/transferWindowManage/showTransferWindowStateTableJson.action', colOpts, tbarOpts)
	transferWindowStateTable = renderData($("#transferWindowStateTable"), transferWindowStateOptions);
	transferWindowStateTable.on('draw', function() {
		oldtransferWindowStateChangeLog = transferWindowStateTable.ajax.json();
	});

});
//弹框模糊查询参数
function searchOptions() {
	return [
		{
			"txt": "传递窗编号",
			"type": "select",
			"searchName": "id",
			"options":$("#transferWindowManage_id").val(),
			"changeOpt":$("#transferWindowManage_id").val()
		},
		{	
			"txt": "检验样本编号",
			"type": "input",
			"searchName": "code"
		},
		{	
			"txt": "检测项",
			"type": "input",
			"searchName": "sampleDetecyion.name"
		}/*,{
			"txt": "提交时间(开始)",
			"type": "dataTime",
			"searchName": "qualitySubmitTime##@@##1",
			"mark" : "s##@@##",
		},
		{
			"txt": "提交时间(结束)",
			"type": "dataTime",
			"searchName": "qualitySubmitTime##@##2",
			"mark" : "e##@@##",
		},
		{
			"txt": "扫码时间(开始)",
			"type": "dataTime",
			"searchName": "qualityReceiveTime##@##1",
			"mark" : "s##@@##",
		},
		{
			"txt": "扫码时间(结束)",
			"type": "dataTime",
			"searchName": "qualityReceiveTime##@##2",
			"mark" : "e##@@##",
		}*/,
		{
			"txt": "提交人",
			"type": "input",
			"searchName": "qualitySubmitUser",
		},
		{
			"txt": "QC扫码人",
			"type": "input",
			"searchName": "qualityReceiveUser",
		},
		{
			"txt": "样本状态",
			"type": "select",
			"searchName": "sampleState",
			"options":"请选择"+"|"+"合格"+"|"+"不合格",
			"changeOpt":"''|1|0"
		},
		{
			"txt": "是否扫码",
			"type": "select",
			"searchName": "scancode",
			"options":"请选择"+"|"+"是"+"|"+"否",
			"changeOpt":"''|1|0"
		},
		{
			"type":"table",
			"table":transferWindowStateTable
		}];
}