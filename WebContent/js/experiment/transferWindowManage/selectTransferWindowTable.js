var TransferWindowTable;
$(function() {
		var checkstate = true;
		console.log($("#maincontentframe",parent.document).contents().find(".box-title").text().trim());
		if($("#maincontentframe",parent.document).contents().find(".box-title").text().trim()=="生产计划"){
			checkstate = false;
		}
		if($("#maincontentframe",parent.document).contents().find(".box-title").text().trim()=="生产确认未确认"){
			checkstate = false;
		}
		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : "传递窗编号",
		});
		colOpts.push({
			"data" : "transferWindowName",
			"title" : "传递窗名称",
		});
		colOpts.push({
			"data" : "regionalManagement-name",
			"title" : "所在区域",
		});
		colOpts.push({
			"data" : "createUser-name",
			"title" : "创建人",
		});
		colOpts.push({
			"data": "transferWindowState",
			"title": "传递窗状态",
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return "可以使用";
				}
				if(data == "0") {
					return "不可以使用";
				}else {
					return '';
				}
			}
		});
		colOpts.push({
			"data": "isFull",
			"title": "占用状态",
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return "未占用";
				}
				if(data == "1") {
					return "占用";
				}
				else {
					return data;
				}
			}
		});
		var tbarOpts = [];
		var addUserOptions = table(checkstate, null,'/experiment/cell/passage/cellPassage/selectTransferWindowTableJson.action',
				colOpts, tbarOpts)
		TransferWindowTable = renderData($("#addTransferWindowTable"), addUserOptions);
		$("#addTransferWindowTable").on(
				'init.dt',
				function(e, settings) {
					// 清除操作按钮
					$("#addTransferWindowTable_wrapper .dt-buttons").empty();
					$('#addTransferWindowTable_wrapper').css({
						"padding": "0 16px"
					});
				var trs = $("#addTransferWindowTable tbody tr");
				TransferWindowTable.ajax.reload();
				TransferWindowTable.on('draw', function() {
					trs = $("#addTransferWindowTable tbody tr");
					trs.click(function() {
						$(this).addClass("chosed").siblings("tr").removeClass("chosed");
					});
				});
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
				});
	
})
function query(){
	var transferWindowName = $.trim($("#transferWindowName").val());
	var searchItemLayerValue = {};
	var k1 = $("#transferWindowName").attr("searchname");
	if(transferWindowName != null && transferWindowName != "") {
		searchItemLayerValue[k1] = "%" + transferWindowName + "%";
	} else {
		searchItemLayerValue[k1] = transferWindowName;
	}
	var regional = $.trim($("#regional").val());
	var k2 = $("#regional").attr("searchname");
	if(regional != null && regional != "") {
		searchItemLayerValue[k2] = "%" + regional + "%";
	} else {
		searchItemLayerValue[k2] = regional;
	}
	var createUser = $.trim($("#createUser").val());
	var k3 = $("#createUser").attr("searchname");
	if(createUser != null && createUser != "") {
		searchItemLayerValue[k3] = "%" + createUser + "%";
	} else {
		searchItemLayerValue[k3] = createUser;
	}
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	sessionStorage.setItem("searchContent", query);
	TransferWindowTable.settings()[0].ajax.data = param;
	TransferWindowTable.ajax.reload();
}
