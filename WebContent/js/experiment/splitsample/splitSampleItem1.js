var splitSampleItemGrid1;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'splitCode',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'labCode',
		type : "string"
	});
	fields.push({
		name : 'dnaCode',
		type : "string"
	});
	fields.push({
		name : 'sampleType',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'endDate',
		type : "string"
	});
	fields.push({
		name : 'contraction',
		type : "string"
	});
	fields.push({
		name : 'qbContraction',
		type : "string"
	});
	fields.push({
		name : 'od280',
		type : "string"
	});
	fields.push({
		name : 'od260',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
	fields.push({
		name : 'volume',
		type : "string"
	});
	fields.push({
		name : 'nextFlowId',
		type : "string"
	});
	fields.push({
		name : 'nextFlow',
		type : "string"
	});

	fields.push({
		name : 'isSplitPlatform',
		type : "string"
	});
	fields.push({
		name : 'isSplitProduct',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'stateName',
		type : "string"
	});
	fields.push({
		name : 'sequencePlatform',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : '编码',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'splitCode',
		header : '拆分后编号',
		width : 30 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'code',
		header : '拆分前编号1',
		width : 25 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'sampleCode',
		header : '原始样本编号',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'labCode',
		header : '实验室样本号',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'dnaCode',
		header : 'dna编号',
		width : 25 * 6,
		hieen : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'sampleType',
		header : '样本类型',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'productId',
		header : '检测项目id',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'sequencePlatform',
		header : '测序平台',
		width : 20 * 6,
		sortable : true
	});
	cm.push({
		dataIndex : 'productName',
		header : '检测项目',
		width : 30 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'endDate',
		header : '结束日期',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'contraction',
		header : '浓度(ng/ul)',
		width : 20 * 6,

		sortable : true,
	// editor : new Ext.form.TextField({
	// allowDecimals:true,
	// })
	});
	cm.push({
		dataIndex : 'qbContraction',
		header : 'qubit浓度(ng/ul)',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'od280',
		header : 'od260/280',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'od260',
		header : 'od260/230',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'sampleNum',
		header : '总量',
		width : 20 * 6,

		sortable : true
	});
	cm.push({
		dataIndex : 'volume',
		header : '体积',
		width : 20 * 6,

		sortable : true,
		editor : new Ext.form.TextField({
			allowDecimals : true,
		})
	});

	cm.push({
		dataIndex : 'nextFlowId',
		hidden : true,
		header : "下一步流向ID",
		width : 15 * 10
	});
	var nextFlowCob = new Ext.form.TextField({
		allowBlank : false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex : 'nextFlow',
		header : "下一步流向",
		width : 15 * 10,
		editor : nextFlowCob
	});
	var storeDataBits = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', "是" ], [ '0', "否" ] ]
	});
	var DataBits = new Ext.form.ComboBox({
		store : storeDataBits,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isSplitPlatform',
		hidden : false,
		header : "是否已拆平台",
		width : 20 * 6,
		// editor : DataBits,
		renderer : Ext.util.Format.comboRenderer(DataBits)
	});
	var storePutCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', "是" ], [ '0', "否" ] ]
	});
	var putCob = new Ext.form.ComboBox({
		store : storePutCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isSplitProduct',
		hidden : false,
		header : "是否已拆产品",
		width : 20 * 6,
		renderer : Ext.util.Format.comboRenderer(putCob)
	});
	cm.push({
		dataIndex : 'state',
		header : '状态ID',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'stateName',
		header : '状态',
		width : 20 * 6,

		sortable : true
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/experiment/splitsample/splitSampleItem/showSplitSampleItem1ListJson.action";
	var opts = {};
	opts.title = "多平台拆分后分管操作";
	opts.height = document.body.clientHeight - 34;

	opts.tbar = [];

	opts.tbar.push({
		text : '拆分产品',
		handler : splitSample
	});

	opts.tbar.push({
		text : "下一步流向",
		handler : loadTestNextFlowCob
	});
	opts.tbar.push({
		text : '保存',
		handler : save
	});
	
	opts.tbar.push({
		text:'提交',
		handler :submit
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	splitSampleItemGrid1 = gridEditTable("splitSampleItem1div", cols,
			loadParam, opts);
	$("#splitSampleItem1div")
			.data("splitSampleItemGrid1", splitSampleItemGrid1);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});



function submit(){

	if(splitSampleItemGrid1.getModifyRecord().length > 0){
		message("修改样本后请保存！");
		return;
	}
	var records = splitSampleItemGrid1.getSelectRecord();
	var ids = "";
	var nextFlowId="";
	var flg = true;
	var splitCode="";
	$.each(records, function(i, obj) {
		nextFlowId = obj.get("nextFlowId");
		splitCode = obj.get("splitCode");
		if(nextFlowId==""){
			message(splitCode+"下一步流向为空，请选择下一步流向!");
			flg=false;
			return;
		}
		ids += obj.get("id")+",";
	});
	if(!flg){
		message("请选择下一步流向!");
		return;
	}else{
		ajax("post", "/experiment/splitsample/splitSampleItem/submitSample.action", {
			ids : ids
		}, function(data) {
			if (data.success) {					
				message("提交成功");
				splitSampleItemGrid1.getStore().reload();
			} else {
				message("保存失败");
			}
		}, null);	
	}
		
						

}
function save() {
	var itemJson = commonGetModifyRecords(splitSampleItemGrid1);
	if (itemJson.length > 0) {
		ajax(
				"post",
				"/experiment/splitsample/splitSampleItem/saveSampleItem.action",
				{
					itemDataJson : itemJson
				}, function(data) {
					if (data.success) {
						splitSampleItemGrid1.getStore().commitChanges();
						splitSampleItemGrid1.getStore().reload();
						message("保存成功");
					} else {
						message("保存失败");
					}
				}, null);
	} else {
		message("没有需要保存的数据");
	}
}

/**
 * 拆分样本
 */
function splitSample() {
	var records = splitSampleItemGrid1.getSelectRecord();
	var productIds = "";
	if (records.length > 0) {
		$.each(records, function(i, obj) {
			productIds = obj.get("productId");
		});
		var array = productIds.split(",");
		if (array.length > 1) {
			var options = {};
			options.width = 940;
			options.height = 580;
			loadDialogPage(
					null,
					"拆分产品",
					"/experiment/splitsample/splitSampleItem/showSplitProductItemList.action",
					{
						"确定" : function() {
							var grid = splitSampleItemGrid1.store;
							var getRecord = splitSampleItemGrid1
									.getSelectionModel().getSelections();
							var selectRecords = splitProductItemGrid.store;
							var blendCode = [];
							var n = [];
							if (selectRecords.getCount() == 0) {
								message("请先按产品拆分样本！");
								return;
							}
							if (grid.getCount() > 0) {
								for ( var a = 0; a < selectRecords.getCount(); a++) {
									if (selectRecords.getAt(a).get("groupNum") == "") {
										message("请填写分管号！");
										return;
									}
								}
								var groupNums = [];
								
								for ( var i = 0; i < selectRecords.getCount(); i++) {
									groupNums.push(selectRecords.getAt(i).get(
											"groupNum"));
									// productIds.push(selectRecords.getAt(i).get("productId"));
									// productNames.push(selectRecords.getAt(i).get("productName"));

								}

								var s = [];
								var json = {};
								for ( var i = 0; i < groupNums.length; i++) {
									if (!json[groupNums[i]]) {
										s.push(groupNums[i]);
										json[groupNums[i]] = 1;
									}
								}
								for ( var u = 0; u < s.length; u++) {
									var productIds = [];
									var productNames = [];
									var y = parseInt(u+1);
									for ( y; y < groupNums.length; y++) {
										if (s[u] == groupNums[y]) {
											productIds.push(selectRecords.getAt(y).get("productId"));
											productNames.push(selectRecords.getAt(y).get("productName"));
										}
									}
									productIds.push(selectRecords.getAt(u).get("productId"));
									productNames.push(selectRecords.getAt(u).get("productName"));
									
									var ob = splitSampleItemGrid1.getStore().recordType;
									splitSampleItemGrid1.stopEditing();
									var p = new ob({});
									p.isNew = true;
									ajax(
											"post",
											"/experiment/splitsample/splitSampleItem/SplitProductItem.action",
											{
												dnaCode : selectRecords.getAt(u).get("dnaCode"),
												splitCode : selectRecords.getAt(u).get("code")
											},
											function(data) {
												if (data.success) {
													// DA1706230001WK01P01
													var maxSplitCode = data.data;
													// 1
													var lastNumber = maxSplitCode.substr(maxSplitCode.length - 1,1);
													var startNumber = parseInt(lastNumber)+ parseInt((u + 1));
													// DA1706230001WK01P0
													var number = maxSplitCode.substring(0,maxSplitCode.length - 1);
													var splitCode = number+ startNumber;
													p.set("splitCode",splitCode);
												}
											}, null);
									p.set("code", selectRecords.getAt(u).get(
											"code"));
									p.set("sampleCode", selectRecords.getAt(u)
											.get("sampleCode"));
									p.set("labCode", selectRecords.getAt(u)
											.get("labCode"));
									p.set("dnaCode", selectRecords.getAt(u)
											.get("dnaCode"));
									p.set("sampleType", selectRecords.getAt(u)
											.get("sampleType"));
									p.set("endDate", selectRecords.getAt(u)
											.get("endDate"));
									p.set("contraction", selectRecords.getAt(u)
											.get("contraction"));
									p.set("qbContraction", selectRecords.getAt(
											u).get("qbContraction"));
									p.set("od280", selectRecords.getAt(u).get(
											"od280"));
									p.set("od260", selectRecords.getAt(u).get(
											"od260"));
									p.set("isSplitPlatform", selectRecords
											.getAt(u).get("isSplitPlatform"));
									p.set("isSplitProduct", selectRecords
											.getAt(u).get("isSplitProduct"));
									p.set("state", "3");
									p.set("stateName", "已拆产品");
									p.set("sequencePlatform", selectRecords
											.getAt(u).get("sequencePlatform"));
									p.set("productId", productIds.toString());
									p.set("productName", productNames.toString());
									splitSampleItemGrid1.getStore().add(p);
									splitSampleItemGrid1.startEditing(0, 0);
								}
								message("拆分产品成功，请保存！");
								for ( var i = 0; i < getRecord.length; i++) {
									splitSampleItemGrid1.stopEditing();
									getRecord[i].set("state", "4");
									splitSampleItemGrid1.startEditing(0, 0);
								}
								// $.each(records, function(i, obj) {
								// ajax("post","/experiment/splitsample/splitSampleItem/SplitProductItem.action",
								// {id : obj.get("id")},
								// function(data) {
								// if (data.success) {
								// message("拆分成功！");
								// splitSampleItemGrid1.getStore().reload();
								// }
								// }, null);
								// });
							} else {
								message("请先拆分样本！");
							}
							options.close();
						}
					}, true, options);
		} else {
			message("样本是单个检测项目！");
		}
	} else {
		message("请选择要拆分的样本！");
	}
}

var loadNextFlow;
// 下一步流向
function loadTestNextFlowCob() {
	var records1 = splitSampleItemGrid1.getSelectRecord();
	var productId = "";
	$.each(records1, function(j, k) {
		productId = k.get("productId");
	});
	var options = {};
	options.width = 500;
	options.height = 500;
	loadNextFlow = loadDialogPage(
			null,
			"选择下一步流向",
			"/system/nextFlow/nextFlow/shownextFlowDialog.action?model=ExperimentDnaGet&productId="
					+ productId, {
				"Confirm" : function() {
					var operGrid = $("#show_dialog_nextFlow_div1").data(
							"shownextFlowDialogGrid");
					var selectRecord = operGrid.getSelectionModel()
							.getSelections();
					var records = splitSampleItemGrid1.getSelectRecord();
					if (selectRecord.length > 0) {
						$.each(records, function(i, obj) {
							$.each(selectRecord, function(a, b) {
								obj.set("nextFlowId", b.get("id"));
								obj.set("nextFlow", b.get("name"));
							});
						});
					} else {
						message("请选择数据");
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
}

function setNextFlow() {
	var operGrid = $("#show_dialog_nextFlow_div1").data(
			"shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = splitSampleItemGrid1.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	} else {
		message("请选择数据");
		return;
	}
	loadNextFlow.dialog("close");
}
