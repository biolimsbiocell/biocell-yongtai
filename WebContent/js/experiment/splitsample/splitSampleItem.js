var oldChangeLog;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "splitCode",
		"title": biolims.common.splitAfterCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "splitCode");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.splitBeforeCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "sampleOrder-id",
		"title":"关联订单",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": "productId",
		"title": biolims.master.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	});
	//SampleInfo
	colOpts.push({
		"data": "sampleInfo-id",
		"title": "SampleInfo",
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleInfo-id");
		}
	})
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.originalSampleType,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleType");
		}
	})
	
	colOpts.push({
		"data": "dicSampleType-id",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	})
	colOpts.push({
		"data": "dicSampleType-name",
		"title":biolims.common.dicSampleTypeName,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-name");
		}
	})
	colOpts.push({
		"data": "contraction",
		"className":"edit",
		"title": biolims.common.concentration,
		"createdCell": function(td) {
			$(td).attr("saveName", "contraction");
		}
	})
	colOpts.push({
		"data": "volume",
		"title": biolims.common.volume,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "volume");
		}
	})
	colOpts.push({
		"data": "sampleNum",
		"title": biolims.common.sumNum,
//		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleNum");
		}
	})
	colOpts.push({
		"data": "qbContraction",
		"title": biolims.common.qbConcentration,
		"createdCell": function(td) {
			$(td).attr("saveName", "qbContraction");
		}
	})
	colOpts.push({
		"data": "od280",
		"title": "od260/280",
		"createdCell": function(td) {
			$(td).attr("saveName", "od280");
		}
	})
	colOpts.push({
		"data": "od260",
		"title": "od260/230",
		"createdCell": function(td) {
			$(td).attr("saveName", "od260");
		}
	})
	colOpts.push({
		"data": "nextFlowId",
		"title": biolims.common.nextFlowId,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlowId");
		}
	})
	colOpts.push({
		"data": "nextFlow",
		"title": biolims.common.nextFlow,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlow");
		}
	})
	colOpts.push({
		"data": "isSplitProduct",
		"title": biolims.common.isSplit,
		"name":biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "isSplitProduct");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.no;
			}else if(data == "1") {
				return biolims.common.yes;
			}else{
				return "";
			}
		}
	})
	colOpts.push({
		"data": "state",
		"title": biolims.common.state,
		"name":biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			else if(data == "0") {
				return biolims.common.no;
			}else {
				return "";
			}
		}
	})
	colOpts.push({
		"data": "stateName",
		"title": biolims.common.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
		}
	})
	colOpts.push({
		"data": "sampleInfo-orderNum",
		"title": biolims.common.orderCode,
		"createdCell": function(td,data,rowData) {
			$(td).attr("saveName", "sampleInfo-orderNum");
			$(td).attr("sampleInfo-id", rowData['sampleInfo-id']);
		}
	})
	
	var tbarOpts = [];
	//添加浓度按钮
	tbarOpts.push({
		text: biolims.common.concentration,
		className: 'btn btn-sm btn-success btnConcentration',
		action: function() {
			submitConcentration();
		}
	});
	//添加体积按钮
	tbarOpts.push({
		text: biolims.common.volume,
		className: 'btn btn-sm btn-success btnVolume',
		action: function() {
			submitVolume();
		}
	});
	tbarOpts.push({
		text: biolims.common.splitSample,
		action: function() {
			splitSample();
		}
	});
	tbarOpts.push({
		text: biolims.common.splitProdect,
		action: function() {
			splitProduct();
		}
	});
	tbarOpts.push({
		text: biolims.common.allSample,
		action: function() {
			allSample();
		}
	});
	tbarOpts.push({
		text: biolims.master.clinicalSample,
		action: function() {
			clinicalSample();
		}
	});
	tbarOpts.push({
		text: biolims.common.scienceService,
		action: function() {
			techDNAService();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-paypal"></i> '+biolims.common.nextFlow,
		action: function() {
			nextFlow();
		}
	});
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem($("#splitSampleDiv"));
		}
	});
	tbarOpts.push({
		text: biolims.common.submitSample,
		action: function() {
			submitSample();
		}
	});
	var splitSampleOps = table(true, null, "/experiment/splitsample/splitSampleItem/showSplitSampleItemListJson.action", colOpts, tbarOpts);
	splitSampleTab = renderData($("#splitSampleDiv"), splitSampleOps);
	splitSampleTab.on('draw', function() {
		oldChangeLog = splitSampleTab.ajax.json();
	});
});
//下一步流向
function nextFlow() {
	var rows = $("#splitSampleDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var productId = "";
	var sampleType="";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
		sampleType = $(k).find("td[savename='dicSampleType-id']").text();
	});
	top.layer.open({
		title:biolims.common.selectNextFlow,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=SplitSampleItem&productId="
					+ productIds+"&sampleType="+sampleType,''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
			.eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
			0).text();
		rows.addClass("editagain");
		rows.find("td[savename='nextFlow']").text(name);
		rows.find("td[savename='nextFlowId']").text(id);

		top.layer.close(index)
		},
	})
}
// 保存
function saveItem() {
	var ele=$("#splitSampleDiv");
	var changeLog = "拆分平台：";
	var data = saveItemjson(ele);
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !="拆分平台："){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		type: 'post',
		url: '/experiment/splitsample/splitSampleItem/saveSampleItem.action',
		data: {
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断男女并转换为数字
			if(k == "result") {
				var result = $(tds[j]).text();
				if(result == biolims.common.disqualified) {
					json[k] = "0";
				} else if(result == biolims.common.qualified) {
					json[k] = "1";
				}
				continue;
			}
			if(k == "sampleInfo-orderNum") {
				json["sampleInfo-id"] = $(tds[j]).attr("sampleInfo-id");
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if(!id){
			changeLog += '新增记录:';
			for(var k in v) {
					var title = ele.find("th[savename=" + k + "]").text();
					changeLog += '"' + title + '"为"' + v[k] + '";';
				}
			return true;
		}
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function submitSample(){
	var rows = $("#splitSampleDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids=[];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val())
	});
	ajax("post", "/experiment/splitsample/splitSampleItem/submitSample.action", {
		ids: ids,
	}, function(data) {
		if(data.success) {
			top.layer.msg(biolims.common.submitSuccess);
			tableRefresh();
		} else {
			top.layer.msg(biolims.common.submitFail);
		}
	}, null)
}
//拆分样本
function splitSample() {
	var rows = $("#splitSampleDiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg("请选择拆分样本");
		return false;
	}
	var id="";
	var splitCode="";
	
	$.each(rows, function(j, k) {
		id = $(k).find("input[type=checkbox]").val();
		splitCode=$(k).find("td[savename='splitCode']").text();
	});
	if(splitCode){
		top.layer.msg("该样本已拆分，无法再次拆分");
		return;
	}
	var num = 1;
	top.layer.open({
		title: "选择拆分数量",
		type: 1,
		area: ["30%", "30%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: ' <div class="input-group"><span class="input-group-btn"><button class="btn btn-default" type="button" id="subSampleNum"> - </button></span><input type="text" readOnly="readOnly" value="1" class="form-control" id="sampleNum"><span class="input-group-btn"><button class="btn btn-default" id="addSampleNum" type="button"> + </button></span></div>',
		success: function() {
			$("#subSampleNum", parent.document).click(function() {
				num = parseInt($("#sampleNum", parent.document).val()) - 1;
				$("#sampleNum", parent.document).val(num);
			});
			$("#addSampleNum", parent.document).click(function() {
				num = parseInt($("#sampleNum", parent.document).val()) + 1;
				$("#sampleNum", parent.document).val(num);
			});
		},
		yes: function(index, layer) {
			ajax(
					"post",
					"/experiment/splitsample/splitSampleItem/splitNum.action",
					{
						splitNum : num,
						id : id
					},
					function(data) {
						if (data.success) {
								top.layer.msg("拆分完成");
								tableRefresh();
//								$('#splitSampleDiv').dataTable()
//								    .on( 'stateLoaded.dt', function (e, settings, data) {
//									var splitCodes=data.splitCodes;
//									for(var i in splitCodes){
//										splitCodeData=splitCodes[i];
//										$("#splitSampleDiv tbody .icheck").each(
//												function(i, val) {
//													$(val).iCheck('uncheck');
//													if (val.value == splitCodeData) {
//														$(val).iCheck('check');
//													}
//												});
//									}
//								    } );
						}
					}, null);
			top.layer.close(index)
		},
	})
}
//所有样本
function allSample() {
	window.location = window.ctx
			+ '/experiment/splitsample/splitSampleItem/showSplitSampleItemList.action';
}
//临床样本
function clinicalSample() {
	window.location = window.ctx
			+ '/experiment/splitsample/splitSampleItem/showSplitSampleItemList.action?id='
			+ "1";
}
//拆分产品
function splitProduct(){
	var rows = $("#splitSampleDiv .selected");
	var length = rows.length;
	if(length!=1) {
		top.layer.msg(biolims.common.PleasSelSampleSplit);
		return false;
	}
	var orderNum="";
	$.each(rows, function(j, k) {
		orderNum = $(k).find("td[savename='sampleInfo-orderNum']").text();
	});
	var orderNums =orderNum.split(",");
	if(orderNums.length>1){
		top.layer.open({
			title: biolims.sample.pleaseSelectReviewer,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderSelList.action?orderNum="+orderNum, ''],
			yes: function(index, layer) {
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td").eq(3).text();
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td").eq(2).text();
				rows.find("td[savename='productId']").text(name);
				rows.find("td[savename='productName']").text(id);
				top.layer.close(index);
			},
			end:function(){
				splitProductView();
			}
		})
	}
}

function splitProductView(){
	var rows = $("#splitSampleDiv .selected");
//	var length = rows.length;
//	if(length!=1) {
//		top.layer.msg(biolims.common.PleasSelSampleSplit);
//		return false;
//	}
	var productIds="";
	var id="";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
		id=$(k).find("input[type=checkbox]").val();
	});
	var array = productIds.split(",");
//	if (array.length > 1) {
	top.layer.open({
		title:biolims.common.splitProdect,
		type:2,
		area:[document.body.clientWidth-200,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/experiment/splitsample/splitSampleItem/showSplitProductItemList1.action?id="+id,''],
		yes: function(index, layer) {
			var seRows=$('.layui-layer-iframe', parent.document).find("iframe").contents().find("tbody").find("tr");
			var boo=true;
			$.each(seRows, function(j, k) {
				if($(k).children().eq(1).text()==""||$(k).children().eq(1).text()==null){
					boo=false;
				}
			});
			if(seRows.length>0){
				if(boo){
					var groupNums = [];
					for ( var i = 0; i < seRows.length; i++) {
						groupNums.push($(seRows[i]).children().eq(1).text());
					}
					var s = [];
					var json = {};
					for ( var i = 0; i < groupNums.length; i++) {
						if (!json[groupNums[i]]) {
							s.push(groupNums[i]);
							json[groupNums[i]] = 1;
						}
					}	
					for ( var u = 0; u < s.length; u++) {
						var productIds = [];
						var productNames = [];
						var y = parseInt(u + 1);
						for (y; y < groupNums.length; y++) {
							if (s[u] == groupNums[y]) {
								productIds.push($(seRows[y]).find("td[savename='productId']").text());
								productNames.push($(seRows[y]).find("td[savename='productName']").text());
							}
						}
						productIds.push($(seRows[u]).find("td[savename='productId']").text());
						productNames.push($(seRows[u]).find("td[savename='productName']").text());
						var splitCode;
						if ($(seRows[u]).find("td[savename='splitCode']").text() == "") {
							if (u < 9) {
								splitCode = $(seRows[u]).find("td[savename='code']").text()
										+ "P010" + (u + 1);
							} else if (u = 9) {
								splitCode = $(seRows[u]).find("td[savename='code']").text()
										+ "P01" + (u + 1);
							} else {
								splitCode = $(seRows[u]).find("td[savename='code']").text()
										+ "P01" + u;
							}
						} else {
							if (u < 9) {
								splitCode = $(seRows[u]).find("td[savename='splitCode']").text()
										+ "0" + (u + 1);
							} else if (u = 9) {
								splitCode = $(seRows[u]).find("td[savename='splitCode']").text()
										+ "" + (u + 1);
							} else {
								splitCode = $(seRows[u]).find("td[savename='splitCode']").text()
										+ "" + u;
							}
						}
						var ele=$("#splitSampleDiv");
						//禁用固定列
						$("#fixdeLeft2").hide();
						//禁用列显示隐藏
						$(".colvis").hide();
						//添加明细
						var ths = ele.find("th");
						var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
						tr.height(32);
						console.log(productIds)
						for(var i = 1; i < ths.length; i++) {
							var edit = $(ths[i]).attr("key");
							var saveName = $(ths[i]).attr("saveName");
							if(saveName=="splitCode"){
								tr.append("<td class=" + edit + " saveName=" + saveName + ">"+splitCode+"</td>");
							}else if(saveName=="productId"){
								tr.append("<td class=" + edit + " saveName=" + saveName + ">"+productIds+"</td>");
							}else if(saveName=="productName"){
								tr.append("<td class=" + edit + " saveName=" + saveName + ">"+productNames+"</td>");
							}else{
								tr.append("<td class=" + edit + " saveName=" + saveName + ">"+$(rows[0]).find("td[savename='"+saveName+"']").text()+"</td>");
							}
						tr.addClass("editagain");
						ele.find("tbody").prepend(tr);
						}
						checkall(ele);
						ajax(
								"post",
								"/experiment/splitsample/splitSampleItem/selIsSplitProductSplitCode.action",
								{
									id : id
								}, function(data) {
									if (data.success) {
										if (data.data != 1) {
											fla = false;
											flas = false;
										} else {
											fla = true;
											flas = true;
										}
									}
								}, null);
					}
				ele.find(".selected").remove();	
				top.layer.close(index)	
				}else{
					top.layer.msg("请填写分管号");
				}
			}else{
				top.layer.msg("请点击按产品拆分样本");
			}
		},
	})
//	}else{
//		top.layer.msg(biolims.common.sampleIsDPriject)
//	}
}
//赋值勾选的浓度
function submitConcentration(){
	var rows = $("#splitSampleDiv .selected");
	var length = rows.length;
	if(!length) {
		layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//勾选得chekbox得id数组集合
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val());
	});
	var concentrationList = [];
	$.each(rows, function(i, v) {
		concentrationList.push($(v).find("td[savename=contraction]").text());
	});
	//获取勾选的浓度得第一个值,最后要用的值
	var concentrationValue=concentrationList[0];
	$.each(rows, function(i, v) {
		$(v).find("td[savename=contraction]").text(concentrationValue);
		$(v).addClass("editagain");
	});
}

//赋值勾选的体积
function submitVolume(){
	var rows = $("#splitSampleDiv .selected");
	var length = rows.length;
	if(!length) {
		layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//勾选得chekbox得id数组集合
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val());
	});
	var concentrationList = [];
	$.each(rows, function(i, v) {
		concentrationList.push($(v).find("td[savename=volume]").text());
		//获取勾选的浓度得第一个值,最后要用的值
		var volumeValue=concentrationList[0];
		
		$(v).find("td[savename=volume]").text(volumeValue);
		$(v).addClass("editagain");
	});
}
