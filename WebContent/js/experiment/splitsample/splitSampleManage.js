var splitSampleManageTab;
$(function() {
	var options = table(true, "",
			"/experiment/splitsample/splitSampleManage/showSplitSampleManageListJson.action", [ {
				"data" : "id",
				"visible":false,
				"title" : biolims.common.id,
			}, {
				"data" : "splitCode",
				"title" : biolims.common.splitAfterCode,
			}, {
				"data" : "code",
				"title" : biolims.common.splitBeforeCode,
			}, {
				"data" : "sampleCode",
				"title" : biolims.common.sampleCode,
			}, {
				"data" : "labCode",
				"title" : biolims.common.labCode,
			}, {
				"data" : "sampleType",
				"title" : biolims.common.sampleType,
			}, {
				"data" : "productId",
				"title" : biolims.master.productId,
			}, {
				"data" : "sequencePlatform",
				"title" : biolims.wk.sequencePlatform,
			}, {
				"data" : "productName",
				"title" : biolims.master.product,
			}, {
				"data" : "contraction",
				"title" : biolims.common.concentration,
			}, {
				"data" : "qbContraction",
				"title" : biolims.common.qbConcentration,
			}, {
				"data" : "od280",
				"title" : "od260/280",
			}, {
				"data" : "od260",
				"title" : "od260/230",
			}, {
				"data" : "sampleNum",
				"title" : biolims.common.sumNum,
			}, {
				"data" : "volume",
				"title" : biolims.common.bulk,
			}], null)
	splitSampleManageTab = renderData($("#splitSampleManageDiv"), options);
	$('#splitSampleManageDiv').on('init.dt', function() {
		recoverSearchContent(splitSampleManageTab);
	})
});
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"searchName" : "confirmDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "confirmDate##@@##2"
	}, {
		"type" : "table",
		"table" : splitSampleManageTab
	} ];
}
