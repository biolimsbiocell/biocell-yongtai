var splitProductItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'splitCode',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'labCode',
		type:"string"
	});
	    fields.push({
		name:'dnaCode',
		type:"string"
	});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'endDate',
		type:"string"
	});
	    fields.push({
		name:'contraction',
		type:"string"
	});
	    fields.push({
		name:'qbContraction',
		type:"string"
	});
	    fields.push({
		name:'od280',
		type:"string"
	});
	    fields.push({
		name:'od260',
		type:"string"
	});
	    fields.push({
		name:'sampleNum',
		type:"string"
	});
	    fields.push({
		name:'volume',
		type:"string"
	});
	    fields.push({
			name:'nextFlowId',
			type:"string"
		});
	    fields.push({
			name:'nextFlow',
			type:"string"
		});
	    
	    fields.push({
			name:'isSplitPlatform',
			type:"string"
		});
	    fields.push({
			name:'isSplitProduct',
			type:"string"
		});   
	    fields.push({
			name:'state',
			type:"string"
		});
	    fields.push({
			name:'stateName',
			type:"string"
		}); 
	    fields.push({
			name:'sequencePlatform',
			type:"string"
		}); 
	    fields.push({
			name:'groupNum',
			type:"string"
		}); 
	    
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cm.push({
		dataIndex:'groupNum',
		header:'分管号',
		width:10*6,
		
		sortable:true,
		editor : new Ext.form.TextField({
		allowDecimals:true,
	})
	});
	cm.push({
		dataIndex:'splitCode',
		header:'拆分后编号',
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:'拆分前编号',
		width:25*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'原始样本编号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'labCode',
		header:'实验室样本号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'dnaCode',
		header:'dna编号',
		width:20*6,
		hidden :true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		
		sortable:true
	});
	 cm.push({
			dataIndex:'sequencePlatform',
			header:'测序平台',
			width:30*6,
			sortable:true
		});

	cm.push({
		dataIndex:'sampleType',
		header:'样本类型',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目id',
		width:20*6,
		
		sortable:true
	});

	cm.push({
		dataIndex:'endDate',
		header:'结束日期',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'contraction',
		header:'浓度(ng/ul)',
		width:20*6,
		
		sortable:true,
//		editor : new Ext.form.TextField({
//			allowDecimals:true,
//		})
	});
	cm.push({
		dataIndex:'qbContraction',
		header:'qubit浓度(ng/ul)',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'od280',
		header:'od260/280',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'od260',
		header:'od260/230',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleNum',
		header:'总量',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'volume',
		header:'体积',
		width:20*6,
		
		sortable:true,
//		editor : new Ext.form.TextField({
//			allowDecimals:true,
//		})
	});
	
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:"下一步流向ID",
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:"下一步流向",
		width:15*10,
		hidden:true,
		editor : nextFlowCob
	});
	var storeDataBits = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', "是"], [ '0',"单平台"] ]
	});
	var DataBits = new Ext.form.ComboBox({
		store : storeDataBits,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isSplitPlatform',
		hidden : true,
		header:"是否已拆平台",
		width:20*6,
//		editor : DataBits,
		renderer : Ext.util.Format.comboRenderer(DataBits)
	});
	var storePutCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', "是"], [ '0',"否"] ]
	});
	var putCob = new Ext.form.ComboBox({
		store : storePutCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isSplitProduct',
		hidden : true,
		header:"是否已拆产品",
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(putCob)
	});
	cm.push({
		dataIndex:'state',
		header:'状态ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态',
		width:20*6,
		hidden:true,
		sortable:true
	});

	
	
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/splitsample/splitSampleItem/showSplitProductItemListJson.action";
	var opts={};
	opts.title="拆分检测项目";
	opts.height=document.body.clientHeight-34;
	opts.tbar = [];
	
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '按产品拆分样本',
		handler : splitSamples
	});
	
//	opts.tbar.push({
//		text : '拆分检测项目',
//		handler : splitProduct
//	});
	
//	opts.tbar.push({
//		text :"下一步流向",
//		handler : loadTestNextFlowCob
//	});

//	opts.tbar.push({
//		text : '保存',
//		handler : save
//	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : "删除选中",
		handler : null
	});
	splitProductItemGrid=gridEditTable("splitProductItemdiv",cols,loadParam,opts);
	$("#splitProductItemdiv").data("splitProductItemGrid", splitProductItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


function splitSamples(){
	var records = splitSampleItemGrid1.getSelectRecord();
	var getRecord = splitProductItemGrid.store;
	var isRepeat = false;
	if (records.length>0) {
		$.each(records, function(i, obj) {
			for(var j=0; j<getRecord.getCount();j++){
				var id = getRecord.getAt(j).get("id");
				if(id==obj.get("id")){
					message("已按产品拆分样本，请勿重复拆分！");
					isRepeat = true;
					break;
				}
			}
			if(!isRepeat){
				var productIds=obj.get("productId");
				var productNames=obj.get("productName");
				var array=productIds.split(",");
				var array1=productNames.split(",");
				if(array.length>1){
					var groupNum=1;
					for(var i=0;i<array.length;i++){
						var ob = splitProductItemGrid.getStore().recordType;
						splitProductItemGrid.stopEditing();
						var p = new ob({});
						p.isNew = true;
						p.set("id",obj.get("id"));
						p.set("code",obj.get("splitCode"));
						p.set("sampleCode",obj.get("sampleCode"));
						p.set("labCode",obj.get("labCode"));
						p.set("dnaCode",obj.get("dnaCode"));
						p.set("sampleType",obj.get("sampleType"));
						p.set("groupNum",groupNum);
						p.set("endDate",obj.get("endDate"));
						p.set("contraction",obj.get("contraction"));
						p.set("qbContraction",obj.get("qbContraction"));
						p.set("od280",obj.get("od280"));
						p.set("od260",obj.get("od260"));
	//					p.set("sampleNum",obj.get("sampleNum"));
	//					p.set("volume",obj.get("volume"));
						p.set("isSplitPlatform",obj.get("isSplitPlatform"));
						p.set("isSplitProduct","1");
						p.set("state","4");
						p.set("stateName","已拆产品");
						p.set("sequencePlatform",obj.get("sequencePlatform"));
						p.set("productId",array[i]);
						p.set("productName",array1[i]);
						splitProductItemGrid.getStore().add(p);
						groupNum++;
					}
				}else{
					message("样本是单个检测项目！");
				}
			}
		});
	}else{
		message("请选择要拆分的样本！");
	}
}
