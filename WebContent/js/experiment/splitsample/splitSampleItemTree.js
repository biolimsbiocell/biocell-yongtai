function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/splitsample/splitSampleItem/editSplitSampleItem.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/splitsample/splitSampleItem/viewSplitSampleItem.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : '编码',
		dataIndex : 'id',
		width:50*6,
		hidden:true
	}, 
	
	   
	{
		header : '拆分后编号',
		dataIndex : 'spiltCode',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '样本编号',
		dataIndex : 'code',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '原始样本编号',
		dataIndex : 'sampleCode',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '实验室样本号',
		dataIndex : 'labCode',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : 'dna编号',
		dataIndex : 'dnaCode',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '样本类型',
		dataIndex : 'sampleType',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '检测项目id',
		dataIndex : 'productId',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '检测项目',
		dataIndex : 'productName',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '结束日期',
		dataIndex : 'endDate',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '浓度(ng/ul)',
		dataIndex : 'contraction',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : 'qubit浓度(ng/ul)',
		dataIndex : 'qbContraction',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : 'od260/280',
		dataIndex : 'od280',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : 'od260/230',
		dataIndex : 'od260',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '总量',
		dataIndex : 'sampleNum',
		width:50*6,
		hidden:false
	}, 
	
	   
	{
		header : '体积',
		dataIndex : 'volume',
		width:50*6,
		hidden:false
	}, 
	
			
			
			
			{
				header : '上级编码',
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#splitSampleItemTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
