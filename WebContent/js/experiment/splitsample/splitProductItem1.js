$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "groupNum",
		"title": biolims.common.splitCode,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "groupNum");
		}
	});
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	});
	colOpts.push({
		"data": "splitCode",
		"title": biolims.common.splitAfterCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "splitCode");
		}
	});
	colOpts.push({
		"data": "orderCode",
		"title": biolims.common.orderCode ,
		"createdCell": function(td) {
			$(td).attr("saveName", "orderCode");
		}
	});
	colOpts.push({
		"data": "sampleInfo-idCard",
		"title": biolims.common.outCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleInfo-idCard");
		}
	});
	colOpts.push({
		"data": "barCode",
		"title": biolims.common.barCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "barCode");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.splitBeforeCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": "labCode",
		"title": biolims.common.labCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "labCode");
		}
	});
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.originalSampleType,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleType");
		}
	});
	colOpts.push({
		"data": "dicsampleType-name",
		"title": biolims.common.sampleType,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicsampleType-name");
		}
	});
	colOpts.push({
		"data": "productId",
		"title": biolims.common.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	});
	colOpts.push({
		"data": "contraction",
		"title": biolims.common.concentration,
		"createdCell": function(td) {
			$(td).attr("saveName", "contraction");
		}
	});
	colOpts.push({
		"data": "qbContraction",
		"title": biolims.common.qbConcentration,
		"createdCell": function(td) {
			$(td).attr("saveName", "qbContraction");
		}
	});
	colOpts.push({
		"data": "od280",
		"title": "od260/280",
		"createdCell": function(td) {
			$(td).attr("saveName", "od280");
		}
	});
	colOpts.push({
		"data": "od260",
		"title": "od260/230",
		"createdCell": function(td) {
			$(td).attr("saveName", "od260");
		}
	});
	colOpts.push({
		"data": "sampleNum",
		"title": biolims.common.sumNum,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleNum");
		}
	});
	colOpts.push({
		"data": "volume",
		"title": biolims.common.volume,
		"createdCell": function(td) {
			$(td).attr("saveName", "volume");
		}
	});
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.splitProdect,
		action: function() {
			splitProduct($("#plitProductDiv"));
		}
	});
	var options = table(true, $("#splitSampleId").val(),
			'/experiment/splitsample/splitSampleItem/showSplitProductItemList1Json.action',colOpts , tbarOpts)
		var splitProductTable = renderData($("#plitProductDiv"), options);
})
function splitProduct(ele){
	var rows = $('#maincontentframe', parent.document).contents().find("#splitSampleDiv .selected");
	var productIds="";
	var productNames="";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
		productNames = $(k).find("td[savename='productName']").text();
	});
	console.log(productIds)
	var array = productIds.split(",");
	var arrayName=productNames.split(",");
	//禁用固定列
	$("#fixdeLeft2").hide();
	//禁用列显示隐藏
	$(".colvis").hide();
	//清除没有内容选项
	ele.find(".dataTables_empty").parent("tr").remove();
	//添加明细
	for(var j=0;j<array.length;j++){
	var ths = ele.find("th");
	var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
	tr.height(32);
	for(var i = 1; i < ths.length; i++) {
		var edit = $(ths[i]).attr("key");
		var saveName = $(ths[i]).attr("saveName");
		if(saveName=="productId"){
			tr.append("<td class=" + edit + " saveName=" + saveName + ">"+array[j]+"</td>");
		}else if(saveName=="productName"){
			tr.append("<td class=" + edit + " saveName=" + saveName + ">"+arrayName[j]+"</td>");
		}else{
			tr.append("<td class=" + edit + " saveName=" + saveName + ">"+$(rows[0]).find("td[savename='"+saveName+"']").text()+"</td>");
		}

	}
	ele.find("tbody").prepend(tr);}
	checkall(ele);
}
