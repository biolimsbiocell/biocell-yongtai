﻿$(function() {
	$(".step").click(
			function() {
					var index = $(".wizard_steps .step").index($(this));
					$(".wizard_steps .selected").removeClass("selected")
						.addClass("done");
					$(this).removeClass("done").addClass("selected");
					$(".HideShowPanel").eq(index).slideDown().siblings(
						'.HideShowPanel').slideUp();
					//table刷新和操作隐藏
					console.log(index);
					if(index == 0) {
						$(".box-tools").hide();
					} else {
						$(".box-tools").hide();
					}
					$('body,html').animate({
						scrollTop: 0
					}, 500, function() {
						$(".HideShowPanel").each(function() {
							if($(this).css("display") == "block") {
								var table = $(this).find(".dataTables_scrollBody").children("table").attr("id");
								$("#" + table).DataTable().ajax.reload();
							}
						});
					});
			});
});
		