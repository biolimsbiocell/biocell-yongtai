var regionalManagementTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.regionalManagement.id,
	});
	  fields.push({
			"data":"name",
			"title":"名称",
		});
	
	var options = table(true, "","/com/biolims/experiment/regionalManagement/regionalManagement/showRegionalManagementTableJson.action",
	 fields, null)
	regionalManagementTable = renderData($("#addRegionalManagementTable"), options);
	$('#addRegionalManagementTable').on('init.dt', function() {
		recoverSearchContent(regionalManagementTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.regionalManagement.id
		});
	
	fields.push({
		"type":"table",
		"table":regionalManagementTable
	});
	return fields;
}
