var regionalManagementTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":"区域编号",
	    });
	    fields.push({
			"data":"name",
			"title":"区域名称",
		});
	    fields.push({
			"data":"regionalState",
			"title":"区域状态",
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return "可用";
				}else if(data == "0") {
					return "不可用";
				}else {
					return data;
				}
			}
		});
	    fields.push({
			"data":"createUser-name",
			"title":"创建人",
		});
	    fields.push({
			"data":"createDate",
			"title":"创建时间",
		});
	    fields.push({
			"data":"state",
			"title":"状态",
			"visible":false,
		});
	    fields.push({
			"data":"stateName",
			"title":"状态",
		});

//		$.ajax({
//			type:"post",
//			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
//			async:false,
//			data:{
//				moduleValue : "RegionalManagement"
//			},
//			success:function(data){
//				var objData = JSON.parse(data);
//				if(objData.success){
//					$.each(objData.data, function(i,n) {
//						var str = {
//							"data" : n.fieldName,
//							"title" : n.label
//						}
//						colData.push(str);
//					});
//					
//				}else{
//					layer.msg("表格解析错误，请重新刷新界面！");
//				}
//			}
//		});

	var options = table(true, "","/experiment/regionalManagement/regionalManagement/showRegionalManagementTableJson.action",
	 fields, null)
	regionalManagementTable = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(regionalManagementTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/experiment/regionalManagement/regionalManagement/editRegionalManagement.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/experiment/regionalManagement/regionalManagement/editRegionalManagement.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/com/biolims/experiment/regionalManagement/regionalManagement/viewRegionalManagement.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":"区域编号"
		});
	
	fields.push({
		"type":"table",
		"table":regionalManagementTable
	});
	return fields;
}
