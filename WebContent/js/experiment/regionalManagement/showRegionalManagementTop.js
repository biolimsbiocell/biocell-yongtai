var DicTypeTable;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "区域编号",
	});
	colOpts.push({
		"data" : "name",
		"title" : "区域名称",
	});
	colOpts.push({
		"data" : "createUser-name",
		"title" : "创建人",
	});
	colOpts.push({
		"data" : "createDate",
		"title" : "创建时间",
	});
	colOpts.push({
		"data" : "regionalState",
		"title" : "区域状态",
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "可用";
			}else if(data == "0") {
				return "不可用";
			}else {
				return data;
			}
		}
	});
	var tbarOpts = [];
	var addDicTypeOptions = table(false, null,
			'/experiment/regionalManagement/regionalManagement/showRegionalManagementTopTable.action',
			colOpts, tbarOpts)//传值
DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions);
	$("#addDicTypeTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮,弹窗搜索就隐藏下一行
				$("#addDicTypeTable_wrapper .dt-buttons").empty();
				$('#addDicTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addDicTypeTable tbody tr");
			DicTypeTable.ajax.reload();
			DicTypeTable.on('draw', function() {
				trs = $("#addDicTypeTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})