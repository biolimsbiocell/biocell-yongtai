var regionalManagementItem;
$(function() {
	var searchModel = '<div class="input-group"><input type="text" class="form-control" placeholder="Search..." id="searchvalue" onkeydown="entersearch()"><span class="input-group-btn"> <button class="btn btn-info" type="button" onclick="searchModel()">按名称搜索</button></span> </div>';
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "区域ID",
//		"visible" :false,
	});
	colOpts.push({
		"data" : "name",
		"title" : "名称",
	});
	var tbarOpts = [];
	var addregionalManagementItem = table(false, null,

			'/experiment/regionalManagement/regionalManagement/showRegionalMTableJson.action',
			colOpts, tbarOpts)
	regionalManagementItem = renderData($("#addregionalManagementItem"), addregionalManagementItem);
	$("#addregionalManagementItem").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮
				$("#addDicTypeTable_wrapper .dt-buttons").html(searchModel);
				//$("#addDicTypeTable_wrapper .dt-buttons").empty();
				$('#addDicTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addregionalManagementItem tbody tr");
			regionalManagementItem.ajax.reload(); 
			regionalManagementItem.on('draw', function() {
				trs = $("#addregionalManagementItem tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})
