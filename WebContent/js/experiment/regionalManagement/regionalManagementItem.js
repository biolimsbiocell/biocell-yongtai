var regionalManagementItem;
var oldRegionalManagementTable;
$(function() {
	// 加载子表
	var id = document.getElementById('regionalManagement_id').value;
	
	
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data":"id",
		"title": "编码 ",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false,	
		
	});
	colOpts.push({
		"data":"cordon",
		"title": "警戒线/0.5um",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "cordon");
	    },
	});
	colOpts.push({
		"data":"deviationCorrectionLine",
		"title": "纠偏线/0.5um",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "deviationCorrectionLine");
	    },
	});
	colOpts.push({
		"data":"cordonf",
		"title": "警戒线5um",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "cordonf");
	    },
	});
	colOpts.push({
		"data":"deviationCorrectionLinef",
		"title": "警戒线5um",
		"className": "edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "deviationCorrectionLinef");
	    },
	});
	colOpts.push({
		"data" : "model",
		"title" : "所属模块",
		"className" : "select",
		"name" : "洁净区尘埃粒子"+ "|洁净区浮游菌" + "|洁净区沉降菌"+"|表面微生物",
		"createdCell" : function(td) {
			$(td).attr("saveName", "model");
			$(td).attr("selectOpt",
					"洁净区尘埃粒子"+ "|洁净区浮游菌" +"|洁净区沉降菌"+"|表面微生物");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "洁净区尘埃粒子";
			}
			if (data == "1") {
				return "洁净区浮游菌";
			} 
			if (data == "2") {
				return "洁净区沉降菌";
			}
			if (data == "3") {
				return "表面微生物";
			}else {
				return '';
			}
		}
	})
	var handlemethod = $("#handlemethod").val();
//按钮
	if(handlemethod != "view") {
		tbarOpts.push({
			text: "添加明细",
			action: function() {
				addItem($("#regionalManagementTable"))
			}
		});
		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeChecked(
						$("#regionalManagementTable"),
						"/experiment/regionalManagement/regionalManagement/delbv.action",
						"删除文档：", id,regionalManagementItem);
			}
		});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#regionalManagementTable"))
			}
		});
		
	}
	
	var regionalMItem = table(true,
		id,
		'/experiment/regionalManagement/regionalManagement/showRegionalManagementItemTableJson.action', colOpts, tbarOpts)
	regionalManagementTable = renderData($("#regionalManagementTable"), regionalMItem);
	regionalManagementTable.on('draw', function() {
		oldRegionalManagementTable = regionalManagementTable.ajax.json();
	});
});	
	
//json
function savejson(ele) {

		var trs = ele.find("tbody").children(".editagain");
		var data = [];
		var flag = true;

		var trss = ele.find("tbody tr");
		trss.each(function(i, val) {
			var json = {};
			var tds = $(val).children("td");
			json["id"] = $(tds[0]).find("input").val();
			for (var j = 1; j < tds.length; j++) {
				var k = $(tds[j]).attr("savename");
				json[k] = $(tds[j]).text();
				
				if (k == "model") {
					var model = $(tds[j]).text();
					if (model == "洁净区尘埃粒子") {
						json[k] = "0";
					} else if (model == "洁净区浮游菌") {
						json[k] = "1";
					} else if (model == "洁净区沉降菌") {
						json[k] = "2";
					}else if (model == "表面微生物") {
						json[k] = "3";
					}else {
						json[k] = "";
					}
					continue;
				}
				}
			data.push(json);
			})
			return JSON.stringify(data);
		}

