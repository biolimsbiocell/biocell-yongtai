﻿var affymetrixTaskTemplateCosGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
	    name:'id',
	    type:"string"
    });
    fields.push({
	    name:'code',
	    type:"string"
	});
    fields.push({
		name:'itemId',
	    type:"string"
	});
    fields.push({
		name:'tCos',
	    type:"string"
    });
    fields.push({
	    name:'name',
	    type:"string"
    });
    fields.push({
	    name:'state',
	    type:"string"
    });
    fields.push({
		name:'temperature',
		type:"string"
	});
    fields.push({
		name:'speed',
		type:"string"
	});
    fields.push({
		name:'time',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
	    name:'isGood',
	    type:"string"
    });
    fields.push({
	    name:'affymetrixTask-id',
	    type:"string"
    });
    fields.push({
	    name:'affymetrixTask-name',
	    type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'设备id',
		width:20*6
	});
	cm.push({
		dataIndex:'tCos',
		hidden : true,
		header:'模板设备id',
		width:20*6
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'模板步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'设备编号',
		width:20*6,		
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'设备名称',
		width:20*6,		
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'设备状态',
		width:20*6,		
	});
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '否' ], [ '1', '是' ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:'是否通过检验',
		width:20*6,
		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:'温度',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:'转速',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:'时间',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'affymetrixTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'affymetrixTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/affymetrix/affymetrixTask/showAffymetrixTaskTemplateCosListJson.action?id="+$("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title="设备明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state=$("#affymetrixTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/affymetrix/affymetrixTask/delaffymetrixTaskTemplateCos.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '填加明细',
		handler : function (){
			//获取选择的数据
			var selectRcords=affymetrixTaskTemplateItemGrid.getSelectionModel().getSelections();
			//获取全部数据
			var allRcords=affymetrixTaskTemplateItemGrid.store;
			//选中的数量
			var length1=selectRcords.length;
			//全部数据量
			var length2=allRcords.getCount();			
			if(length2>0){
				if(length1==1){
					var code="";
					$.each(selectRcords, function(i, obj) {
						code=obj.get("code");
					});
					if(code!=null){
						var ob = affymetrixTaskTemplateCosGrid.getStore().recordType;
						var p = new ob({});
						p.isNew = true;
						p.set("itemId", code);
						affymetrixTaskTemplateCosGrid.stopEditing();
						affymetrixTaskTemplateCosGrid.getStore().insert(0, p);
						affymetrixTaskTemplateCosGrid.startEditing(0, 0);
					}else{
						message("请先添加模板明细数据！");
						return;
					}				
				}else if(length1>1){
					message("模板明细中只能选择一条数据！");
					return;
				}else{
					message("请先选择模板明细中数据！");
					return;
				}
			}else{
				message("模板明细中数据为空！");
				return;
			}
		}
    });
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});	
	}
	affymetrixTaskTemplateCosGrid=gridEditTable("affymetrixTaskTemplateCosdiv",cols,loadParam,opts);
	$("#affymetrixTaskTemplateCosdiv").data("affymetrixTaskTemplateCosGrid", affymetrixTaskTemplateCosGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
	
