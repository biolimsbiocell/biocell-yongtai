﻿var affymetrixTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
	    name:'patientName',
	    type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'idCard',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
    fields.push({
	    name:'location',
	    type:"string"
	});
    fields.push({
		name:'qbcontraction',
		type:"string"
	});	   
	fields.push({
		name:'volume',
		type:"string"
	});
	fields.push({
		name:'unit',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'projectId',
		type:"string"
	});
	fields.push({
		name:'contractId',
		type:"string"
	});
	fields.push({
		name:'orderType',
		type:"string"
	});
	fields.push({
		name:'taskId',
		type:"string"
	});
    fields.push({
		name:'submit',
		type:"string"
	});
	fields.push({
		name:'affymetrixTask-id',
		type:"string"
	});
	fields.push({
		name:'affymetrixTask-name',
		type:"string"
	});
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'sumVolume',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'isToProject',
		type:"string"
	});
    fields.push({
		name:'nextFlowId',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'advice',
		type:"string"
	});
    fields.push({
		name:'rin',
		type:"string"
	});
    fields.push({
		name:'contraction',
		type:"string"
	});
    fields.push({
		name:'tempId',
		type:"string"
	});
    fields.push({
	    name:'classify',
	    type:"string"
	});
    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'dataBits',
		type:"string"
	});
    fields.push({
		name:'labCode',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:40*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:biolims.common.labCode,
		width:20*6
	});
	cm.push({
		dataIndex:'affymetrixTaskCode',
		hidden : true,
		header:biolims.user.affymetrixExperimentNo,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:biolims.common.idCard,
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		header:biolims.user.mobile,
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:biolims.common.sequencingFun,
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'productName',
		header:biolims.common.productName,
		width:20*6,
		hidden:false
	});
	cm.push({
		dataIndex:'productId',
		header:biolims.common.productId,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden:true,
		header:biolims.common.dicSampleTypeId,
		width:15*10,
		sortable:true
	});
	var testDicType2 =new Ext.form.TextField({
        allowBlank: false
	});
	testDicType2.on('focus', function() {
		loadTestDicType2();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName,
		width:15*10,
		editor : testDicType2
	});
	cm.push({
		dataIndex:'inspectDate',
		header:biolims.sample.sampleTime,
		width:30*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:biolims.user.associatedorderId,
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.volume,
		width:20*6,		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'contraction',
		hidden : false,
		header:'文库浓度ng/ul',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'文库片段大小',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'qbcontraction',
		hidden : false,
		header:lims.common.qbConcentration,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'rin',
		hidden : true,
		header:'RIN',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同编号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified ], [ '0', biolims.common.disqualified ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'琼脂糖凝胶结果',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		editor : nextFlowCob
	});
	var storePutCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var putCob = new Ext.form.ComboBox({
		store : storePutCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.toSubmit,
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(putCob)
	});
	var storeDataBits = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.dna.germline ], [ '0', biolims.dna.variation ] ]
	});
	var DataBits = new Ext.form.ComboBox({
		store : storeDataBits,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'dataBits',
		hidden : false,
		header:biolims.dna.dataBits,
		width:20*6,
		editor : DataBits,
		renderer : Ext.util.Format.comboRenderer(DataBits)
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:biolims.common.reason,
		width:40*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'affymetrixTask-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'affymetrixTask-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cm.push({
		dataIndex:'taskId',
		hidden : true,
		header:'科技服务任务单',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:'临床/科技服务 ',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:biolims.common.openBox,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/affymetrix/affymetrixTask/showAffymetrixTaskInfoListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.user.affymetrixExperimentResultManagement;
	opts.height =  document.body.clientHeight*0.8;
	opts.tbar = [];
	var state=$("#affymetrixTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/affymetrix/affymetrixTask/delAffymetrixTaskInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				affymetrixTaskResultGrid.getStore().commitChanges();
				affymetrixTaskResultGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.productType,
		handler : loadTestDicType2
	});
	opts.tbar.push({
		text : biolims.common.batchData,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_infos_div"), biolims.common.batchData, null, {
				"Confirm" : function() {
					var records = affymetrixTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var od230 = $("#od230").val();
						var od260 = $("#od260").val();
						var contraction = $("#contraction").val();
						var volume = $("#volume").val();
						var rin = $("#rin").val();
						var qbcontraction = $("#qbcontraction").val();
						affymetrixTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("od260", od230);
							obj.set("od280", od260);
							obj.set("contraction", contraction);
							obj.set("volume", volume);
							obj.set("rin", rin);
							obj.set("qbcontraction", qbcontraction);
						});
						affymetrixTaskResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
		
	 opts.tbar.push({
			text : biolims.common.batchimport,
			handler : function() {				
				$(".jquery-ui-warning").html(biolims.common.pleasePaste);
				$("#many_bat_text").val("");
				$("#many_bat_text").attr("style", "width:465px;height: 339px");
				var options = {};
				options.width = 494;
				options.height = 508;
				loadDialogPage($("#many_bat_div"), biolims.common.batchimport, null, {
					"Confirm" : function() {
						var positions = $("#many_bat_text").val();
						if (!positions) {
							message(biolims.common.pleaseFillInfo);
							return;
						}
						var posiObj = {};
						var posiObj1 = {};
						var posiObj2 = {};
						var posiObj3 = {};
						var posiObj4 = {};
						var posiObj5 = {};
						var array = formatData(positions.split("\n"));
						$.each(array, function(i, obj) {
							var tem = obj.split("\t");
							posiObj[tem[0]] = tem[1];
							posiObj1[tem[0]] = tem[2];
							posiObj2[tem[0]] = tem[3];
							posiObj3[tem[0]] = tem[4];
							posiObj4[tem[0]] = tem[5];
							posiObj5[tem[0]] = tem[6];
						});
						var records = affymetrixTaskResultGrid.getAllRecord();
						affymetrixTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							if (posiObj[obj.get("code")]) {
								obj.set("volume", posiObj[obj.get("code")]);
							}
							if (posiObj1[obj.get("code")]) {
								obj.set("contraction", posiObj1[obj.get("code")]);
							}
							if (posiObj2[obj.get("code")]) {
								obj.set("od260", posiObj2[obj.get("code")]);
							}
							if (posiObj3[obj.get("code")]) {
								obj.set("od280", posiObj3[obj.get("code")]);
							}
							if (posiObj4[obj.get("code")]) {
								obj.set("qbcontraction", posiObj4[obj.get("code")]);
							}
							if (posiObj5[obj.get("code")]) {
								obj.set("rin", posiObj5[obj.get("code")]);
							}																
						});
						affymetrixTaskResultGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}
		});			 	 
	 opts.tbar.push({
			text : biolims.common.batchResult,
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_result_div"), biolims.common.batchResult, null, {
					"Confirm" : function() {
						var records = affymetrixTaskResultGrid.getSelectRecord();
						if (records && records.length > 0) {
							var result = $("#result").val();
							affymetrixTaskResultGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("result", result);
							});
							affymetrixTaskResultGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : loadTestNextFlowCob
	});

	opts.tbar.push({
		text : biolims.common.exportList,
		handler : exportexcel
	});
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submit
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	}
	affymetrixTaskResultGrid=gridEditTable("affymetrixTaskResultdiv",cols,loadParam,opts);
	$("#affymetrixTaskResultdiv").data("affymetrixTaskResultGrid", affymetrixTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function exportexcel() {
	affymetrixTaskResultGrid.title = biolims.common.exportList;
	var vExportContent = affymetrixTaskResultGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

var loadDicType2;	
//查询样本类型
function loadTestDicType2(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadDicType2=loadDialogPage(null, biolims.common.sampleType, "/sample/dicSampleType/dicSampleTypeSelect.action?a=2", {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = affymetrixTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("dicSampleType-id", b.get("id"));
							obj.set("dicSampleType-name", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function setDicType2(){
	var operGrid = $("#show_dialog_dicType_div").data("dicTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = affymetrixTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			$.each(records, function(a, b) {
				b.set("dicType-id", obj.get("id"));
				b.set("dicType-name", obj.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadDicType2.dialog("close");

}

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = affymetrixTaskResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=AffymetrixTask&productId="+productId, {
			"Confirm" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = affymetrixTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = affymetrixTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}	
	
//提交样本
function submit(){
	var id=$("#affymetrixTask_id").val();  
	if(affymetrixTaskResultGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var record = affymetrixTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("submit")){
			flg=true;
		}
		if(record[i].get("nextFlowId")==""){
			message(biolims.common.notFillNextStep);
			return;
		}
	}
	var grid=affymetrixTaskResultGrid.store;
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();				
		var records = [];						
		for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
		}		
		ajax("post", "/experiment/affymetrix/affymetrixTask/submit.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				affymetrixTaskResultGrid.getStore().commitChanges();
				affymetrixTaskResultGrid.getStore().reload();
				message(biolims.common.submitSuccess);
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);
	}else{
		message(biolims.common.noData2Submit);
	}
}
