﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state = $("#affymetrixTask_state").val();
	var stateName = $("#affymetrixTask_stateName").val();
	if(state =="3"||stateName==biolims.common.toModify){
		load("/experiment/affymetrix/affymetrixTask/showAffymetrixTaskTempList.action", null, "#affymetrixTaskTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#affymetrixTaskTempPage").remove();
	}
});	

function add() {
	window.location = window.ctx + "/experiment/affymetrix/affymetrixTask/editAffymetrixTask.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/affymetrix/affymetrixTask/showAffymetrixTaskList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});	

$("#toolbarbutton_print").click(function(){
	var url = '__report=AffymetrixTask.rptdesign&id=' + $("#affymetrixTask_id").val();
	commonPrint(url);
});

setTimeout(function() {
	if($("#affymetrixTask_template").val()){
		var maxNum = $("#affymetrixTask_maxNum").val();
		if(maxNum>0){
			load("/storage/container/sampleContainerTest.action", {
				id : $("#affymetrixTask_template").val(),
				type :$("#type").val(),
				maxNum : 0
			}, "#3d_image0", function(){
				if(maxNum>1){
					load("/storage/container/sampleContainerTest.action", {
						id : $("#affymetrixTask_template").val(),
						type :$("#type").val(),
						maxNum : 1
					}, "#3d_image1", null);
				}
			});
		}
	}
}, 100);

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("AffymetrixTask", {
					userId : userId,
					userName : userName,
					formId : $("#affymetrixTask_id").val(),
					title : $("#affymetrixTask_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {	
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#affymetrixTask_id").val();	
	var options = {};
	options.width = 929;
	options.height = 534;	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}
	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null, biolims.common.approvalTask, url, {
		"Confirm" : function() {			
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();
				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};											
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);				
			}else if(operVal=="1"){
				if(taskName=="Affymetrix实验"){
				var codeList = new Array();
				var codeList1 = new Array();
				var selRecord1 = affymetrixTaskItemGrid.store;
				var flag=true;
				var flag1=true;
				if (affymetrixTaskResultGrid.getAllRecord().length > 0) {
					var selRecord = affymetrixTaskResultGrid.store;
					for(var j=0;j<selRecord.getCount();j++){
						var oldv = selRecord.getAt(j).get("submit");
						codeList.push(selRecord.getAt(j).get("affymetrixTaskCode"));
						if(oldv==""){
							flag=false;
							message(biolims.common.sampleUncommited);
							return;
						}
						if(selRecord.getAt(j).get("nextFlowId")==""){
							message(biolims.common.notFillNextStep);
							return;
						}
					}
					for(var j=0;j<selRecord1.getCount();j++){						
						if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
							codeList1.push(selRecord1.getAt(j).get("code"));
							flag1=false;
							message(biolims.common.unfinishedExperiments);
						};
					}
					if(affymetrixTaskResultGrid.getModifyRecord().length > 0){
						message(biolims.common.pleaseSaveRecord);
						return;
					}
					if(flag1){
							var myMask1 = new Ext.LoadMask(Ext.getBody(), {
								msg : biolims.common.pleaseWait
							});
							myMask1.show();
							Ext.MessageBox.confirm("确认(Confirm)", biolims.common.pleaseMakeSure2Deal, function(button, text) {
								if (button == "yes") {
									var paramData =  {};
									paramData.oper = $("#oper").val();
									paramData.info = $("#opinion").val();
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									};																		
									_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
										location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
									}, dialogWin);
																										}
							});
							myMask1.hide();
					}else{
						message(biolims.common.sampleUnfinished+codeList1);
					}
				}else{
					message(biolims.common.addAndSave);
					return;
				}
				}else{
					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();
					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};										
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				}
			}				
		},
		"查看流程图(Check flow chart)" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var affymetrixTaskItemDivData = $("#affymetrixTaskItemdiv").data("affymetrixTaskItemGrid");
		document.getElementById('affymetrixTaskItemJson').value = commonGetModifyRecords(affymetrixTaskItemDivData);
	    var affymetrixTaskResultDivData = $("#affymetrixTaskResultdiv").data("affymetrixTaskResultGrid");
		document.getElementById('affymetrixTaskResultJson').value = commonGetModifyRecords(affymetrixTaskResultDivData);
		var affymetrixTaskTemplateItemDivData = $("#affymetrixTaskTemplateItemdiv").data("affymetrixTaskTemplateItemGrid");
		document.getElementById('affymetrixTaskTemplateItemJson').value = commonGetModifyRecords(affymetrixTaskTemplateItemDivData);
		var affymetrixTaskTemplateReagentDivData = $("#affymetrixTaskTemplateReagentdiv").data("affymetrixTaskTemplateReagentGrid");
		document.getElementById('affymetrixTaskTemplateReagentJson').value = commonGetModifyRecords(affymetrixTaskTemplateReagentDivData);
		var affymetrixTaskTemplateCosDivData = $("#affymetrixTaskTemplateCosdiv").data("affymetrixTaskTemplateCosGrid");
		document.getElementById('affymetrixTaskTemplateCosJson').value = commonGetModifyRecords(affymetrixTaskTemplateCosDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/affymetrix/affymetrixTask/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();	
		}
}	

function editCopy() {
	window.location = window.ctx + '/experiment/affymetrix/affymetrixTask/copyAffymetrixTask.action?id=' + $("#affymetrixTask_id").val();
}

$("#toolbarbutton_status").click(function(){
	var AffymetrixTaskInfoGrid = affymetrixTaskResultGrid.getStore();
	var AffymetrixTaskItemGrid = affymetrixTaskItemGrid.getStore();
	var num = 0;
	for(var i= 0; i<AffymetrixTaskItemGrid.getCount();i++){		
		if(AffymetrixTaskInfoGrid.getCount()==0){
			message(biolims.common.pleaseComplete);
			return;
		}
		for(var j=0;j<AffymetrixTaskInfoGrid.getCount();j++){
			if(AffymetrixTaskItemGrid.getAt(i).get("yCode")==AffymetrixTaskInfoGrid.getAt(j).get("yCode")){
				j=AffymetrixTaskInfoGrid.getCount();
			}else{
				num=num+1;
				if(num==AffymetrixTaskInfoGrid.getCount()){
					num=0;
					message(biolims.common.pleaseComplete);
					return;
				}
			}
		}		
	}
	commonChangeState("formId=" + $("#affymetrixTask_id").val() + "&tableId=AffymetrixTask");
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#affymetrixTask_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#affymetrixTask_template").val());
	nsc.push(biolims.common.templateEmpty);
	fs.push($("#affymetrixTask_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.user.affymetrixExperiment,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/affymetrix/affymetrixTask/showAffymetrixTaskItemList.action", {
				id : $("#affymetrixTask_id").val()
			}, "#affymetrixTaskItempage");
load("/experiment/affymetrix/affymetrixTask/showTemplateItemList.action", {
	id:$("#affymetrixTask_id").val()
}, "#affymetrixTaskTemplateItempage");
load("/experiment/affymetrix/affymetrixTask/showAffymetrixTaskTemplateReagentList.action",{
				id:$("#affymetrixTask_id").val(),
			}, "#affymetrixTaskTemplateReagentpage");
load("/experiment/affymetrix/affymetrixTask/showAffymetrixTaskTemplateCosList.action", {
	id:$("#affymetrixTask_id").val()
}, "#affymetrixTaskTemplateCospage");
load("/experiment/affymetrix/affymetrixTask/showAffymetrixTaskInfoList.action", {
				id : $("#affymetrixTask_id").val()
			}, "#affymetrixTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
//调用模板
function TemplateFun(){
			var type="doAffymetrixTask";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:biolims.common.selectTemplate,layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: biolims.common.close,
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}

//双击加载模板数据
function setTemplateFun(rec){	
	if($('#affymetrixTask_acceptUser_name').val()==""){
		document.getElementById('affymetrixTask_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('affymetrixTask_acceptUser_name').value=rec.get('acceptUser-name');
	}		
	var itemGrid=affymetrixTaskItemGrid.store;
	if(itemGrid.getCount()>0){
		for(var i=0;i<itemGrid.getCount();i++){
			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
			itemGrid.getAt(i).set("sampleConsume",rec.get('sampleNum'));
		}
	}
		var code=$("#affymetrixTask_template").val();
		if(code==""){
			var cid=rec.get('id');
						document.getElementById('affymetrixTask_template').value=rec.get('id');
						document.getElementById('affymetrixTask_template_name').value=rec.get('name');
						var win = Ext.getCmp('TemplateFun');
						if(win){win.close();}
						var id=rec.get('id');
						//加载模板明细
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {
									var ob = affymetrixTaskTemplateItemGrid.getStore().recordType;
									affymetrixTaskTemplateItemGrid.stopEditing();
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										affymetrixTaskTemplateItemGrid.getStore().add(p);							
									});									
									affymetrixTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
						//加载原辅料明细
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = affymetrixTaskTemplateReagentGrid.getStore().recordType;
								affymetrixTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									affymetrixTaskTemplateReagentGrid.getStore().add(p);							
								});								
								affymetrixTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						//加载实验设备明细
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = affymetrixTaskTemplateCosGrid.getStore().recordType;
								affymetrixTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("state",obj.state);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									affymetrixTaskTemplateCosGrid.getStore().add(p);							
								});			
								affymetrixTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null);
		}else{
			var flag = true;
			if(rec.get('id')==code){
				flag = confirm(biolims.common.whetherOrNot2Load);
 			 }
			if(flag==true){
 				//判断设备是否占用
 				var cid=rec.get('id');
						var ob1 = affymetrixTaskTemplateItemGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id");				
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/affymetrix/affymetrixTask/delTemplateItemOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null);
								}else{								
									affymetrixTaskTemplateItemGrid.store.removeAll();
								}
							}
							affymetrixTaskTemplateItemGrid.store.removeAll();
		 				}		 				
						var ob2 = affymetrixTaskTemplateReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");								
								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/affymetrix/affymetrixTask/delTemplateReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message(biolims.common.deleteSuccess);
									} else {
										message(biolims.common.deleteFailed);
									}
								}, null); 
								}else{
									affymetrixTaskTemplateReagentGrid.store.removeAll();
								}
							}
							affymetrixTaskTemplateReagentGrid.store.removeAll();
		 				}
						var ob3 = affymetrixTaskTemplateCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");							
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/affymetrix/affymetrixTask/delTemplateCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message(biolims.common.deleteSuccess);
										} else {
											message(biolims.common.deleteFailed);
										}
									}, null); 
								}else{
									affymetrixTaskTemplateCosGrid.store.removeAll();
								}
							}
							affymetrixTaskTemplateCosGrid.store.removeAll();
		 				}
						document.getElementById('affymetrixTask_template').value=rec.get('id');
		 				document.getElementById('affymetrixTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	
									var ob = affymetrixTaskTemplateItemGrid.getStore().recordType;
									affymetrixTaskTemplateItemGrid.stopEditing();									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										affymetrixTaskTemplateItemGrid.getStore().add(p);							
									});									
									affymetrixTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message(biolims.common.anErrorOccurred);
								}
							}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = affymetrixTaskTemplateReagentGrid.getStore().recordType;
								affymetrixTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									affymetrixTaskTemplateReagentGrid.getStore().add(p);							
								});								
								affymetrixTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = affymetrixTaskTemplateCosGrid.getStore().recordType;
								affymetrixTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("isGood",obj.isGood);
									p.set("state",obj.state);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									affymetrixTaskTemplateCosGrid.getStore().add(p);							
								});			
								affymetrixTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message(biolims.common.anErrorOccurred);
							}
						}, null); 
					}			
	}				
}
	
//按条件加载原辅料
function showReagent(){
	//获取全部数据
	var allRcords=affymetrixTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=affymetrixTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=affymetrixTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#affymetrixTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/affymetrix/affymetrixTask/showAffymetrixTaskTemplateReagentList.action", {
			id : $("#affymetrixTask_id").val()
		}, "#affymetrixTaskTemplateReagentpage");
	}else if(length1==1){
		affymetrixTaskTemplateReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/affymetrix/affymetrixTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = affymetrixTaskTemplateReagentGrid.getStore().recordType;
				affymetrixTaskTemplateReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("affymetrixTask-id",obj.tId);
					p.set("affymetrixTask-name",obj.tName);					
					affymetrixTaskTemplateReagentGrid.getStore().add(p);							
				});
				affymetrixTaskTemplateReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.selectRecord);
		return;
	}	
}

//按条件加载设备
function showCos(){
	var allRcords=affymetrixTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=affymetrixTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=affymetrixTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#affymetrixTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/affymetrix/affymetrixTask/showAffymetrixTaskTemplateCosList.action", {
			id : $("#affymetrixTask_id").val()
		}, "#affymetrixTaskTemplateCospage");
	}else if(length1==1){
		affymetrixTaskTemplateCosGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/affymetrix/affymetrixTask/setCos.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = affymetrixTaskTemplateCosGrid.getStore().recordType;
				affymetrixTaskTemplateCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("note",obj.note);
					p.set("time",obj.time);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tCos",obj.tCos);
					p.set("affymetrixTask-id",obj.tId);
					p.set("affymetrixTask-name",obj.tName);					
					affymetrixTaskTemplateCosGrid.getStore().add(p);							
				});
				affymetrixTaskTemplateCosGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.selectRecord);
		return;
	}
}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.rollback
			});
		item.on('click', ckcrk);
		
		});
	
	function ckcrk(){		
		Ext.MessageBox.confirm(biolims.common.prompt, biolims.common.initTaskList, function(button, text) {
			if (button == "yes") {
				var selRecord = affymetrixTaskResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message(biolims.common.canNotInit);
						return;
					}
				}
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message(biolims.common.backSuccess);
								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message(biolims.common.rollbackFailed);
							}
						}, null);
					}
					
				}
			}
		});
	}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.save
			});
		item.on('click', ckcrk2);		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.handleRollback
			});
		item.on('click', ckcrk3);		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: biolims.common.handlingRollback, progressText: biolims.common.handling, width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "AffymetrixTask",id : $("#affymetrixTask_id").val()
		}, function(data) {
			if (data.success) {	
				message(biolims.common.handleRollbackSuccess);
			} else {
				message(biolims.common.handleRollbackFailed);
			}
		}, null);
	
}
		



