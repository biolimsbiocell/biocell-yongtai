var affymetrixTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'createUser-id',
		type:"string"
	});
	fields.push({
		name:'createUser-name',
		type:"string"
	});
	fields.push({
		name:'createDate',
		type:"string"
	});
    fields.push({
		name:'testUser-id',
		type:"string"
	});
	fields.push({
		name:'testUser-name',
		type:"string"
	});
	fields.push({
		name:'receiveDate',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.user.itemNo,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.describe,
		width:50*10,
		sortable:true
	});
	cm.push({
	    dataIndex:'createUser-id',
	    header:biolims.common.createUserId,
	    width:15*10,
	    sortable:true
	});
	cm.push({
		dataIndex:'createUser-name',
		header:biolims.common.createUserName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.createDateName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'testUser-id',
		header:biolims.common.testUserId,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'testUser-name',
		header:biolims.common.testUserName,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'receiveDate',
		header:biolims.common.acceptDate,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.code,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:40*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/affymetrix/affymetrixTask/showAffymetrixTaskListJson.action";
	var opts={};
	opts.title=biolims.user.affymetrixExperiment;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setAffymetrixTaskFun(rec);
	};
	affymetrixTaskDialogGrid=gridTable("show_dialog_affymetrixTask_div",cols,loadParam,opts);
});

function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(affymetrixTaskDialogGrid);
				$(this).dialog("close");
			},
			"清空(Empty)" : function() {
				form_reset();
			}
		}, true, option);
}
