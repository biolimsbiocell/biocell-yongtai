var snpFirstInstanceGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'chipNum',
		type:"string"
	});
	   fields.push({
		name:'lcjy',
		type:"string"
	});
	   fields.push({
		name:'ycbg',
		type:"string"
	});
	   fields.push({
		name:'jg',
		type:"string"
	});
	   fields.push({
		name:'jgjs',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
        fields.push({
		name:'reportInfo-id',
		type:"string"
	});
	    fields.push({
		name:'reportInfo-name',
		type:"string"
	});
	    fields.push({
		name:'reportInfoId',
		type:"string"
	});
	    fields.push({
		name:'reportInfoName',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-fileName',
		type:"string"
	});
	    fields.push({
		name:'fileNum',
		type:"string"
	});
    fields.push({
		name:'taskType',
		type:"string"
	});
	   fields.push({
		name:'taskId',
		type:"string"
	});
	   fields.push({
		name:'taskResultId',
		type:"string"
	});
	   fields.push({
		name:'upTime',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:25*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6
	});

//	var storerCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
//	});
//	var rCob = new Ext.form.ComboBox({
//		store : storerCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		xtype : 'actioncolumn',
		hidden : false,
		width : 120,
		header : '上传附件',
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : '附件',
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')){
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					rec.set("upTime",(new Date()).toString());
					var doc= new Ext.Window({
					id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+ rec.get('taskResultId')+"&modelType=snpAnalysisItem' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: '关闭',
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				}else{
					message("请先保存记录。");
				}
			}
		}]
	});
	cm.push({
		dataIndex:'reportInfoId',
		hidden : true,
		header:'模板ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfoName',
		hidden : true,
		header:'报告模板',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfo-id',
		hidden : true,
		header:'模板ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfo-name',
		hidden : false,
		header:'报告模板',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:'附件ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'template-fileName',
		hidden : true,
		header:'附件名称',
		sortable:true,
		width:15*10
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交<font color="red" size="4">*</font>',
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'jg',
		hidden : false,
		header:'结果<font color="red" size="4">*</font>',
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'jgjs',
		hidden : false,
		header:'结果解释<font color="red" size="4">*</font>',
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'lcjy',
		hidden : false,
		header:'临床建议',
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});*/
	var storelcjyCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '门诊随访' ], [ '0', '遗传门诊随访' ] ]
	});
	var lcjyCob = new Ext.form.ComboBox({
		store : storelcjyCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'lcjy',
		hidden : false,
		header:'临床建议<font color="red" size="4">*</font>',
		width:20*6,
		editor : lcjyCob,
		renderer : Ext.util.Format.comboRenderer(lcjyCob)
	});
	var storeexCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '正常' ], [ '0', '异常' ], [ '2', '多态' ] ]
	});
	var exCob = new Ext.form.ComboBox({
		store : storeexCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	
	cm.push({
		dataIndex:'ycbg',
		hidden : false,
		header:'是否异常报告<font color="red" size="4">*</font>',
		width:20*6,
		
		editor : exCob,
		renderer : Ext.util.Format.comboRenderer(exCob)
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'是否合格<font color="red" size="4">*</font>',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向<font color="red" size="4">*</font>',
		width:15*10,
		sortable:true,
		hidden:true,
		editor : nextFlowCob
	});


	cm.push({
		dataIndex:'taskType',
		hidden : true,
		header:'相关实验',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'chipNum',
		hidden : false,
		header:'芯片号',
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'taskId',
		hidden : true,
		header:'相关实验ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'taskResultId',
		hidden : true,
		header:'相关实验结果ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'upTime',
		hidden : true,
		header:'附件修改时间',
		width:20*10
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/first/snpFirst/showSnpFirstInstanceListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="SNP-Array数据审核";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snpjc/first/snpFirst/delSnpFirstInstance.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				snpFirstInstanceGrid.getStore().commitChanges();
				snpFirstInstanceGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//		text : '选择报告模板',
//		handler : showsampleReportSelectList
//	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
	opts.tbar.push({
		text : "编辑数据",
		handler : function() {
			var records = snpFirstInstanceGrid.getSelectRecord();
			if (records && records.length==1) {
				$.each(records, function(i, obj) {
				if(obj.get("jgjs")!=null && obj.get("jgjs")!=""){
					$("#re").val(obj.get("jgjs"));
				}
				if(obj.get("lcjy")!=null && obj.get("lcjy")!=""){
					$("#lc").val(obj.get("lcjy"));
				}
				if(obj.get("jg")!=null && obj.get("jg")!=""){
					$("#jg").val(obj.get("jg"));
				}
				if(obj.get("ycbg")!=null && obj.get("ycbg")!=""){
					$("#yc").val(obj.get("ycbg"));
				}
				});
				var options = {};
				options.width = 700;
				options.height = 500;
				loadDialogPage($("#bat_area1_div"), "编辑数据", null, {
					"确定" : function() {
							var re = $("#re").val();
							var lc = $("#lc").val();
							var jg = $("#jg").val();
							var yc = $("#yc").val();
							snpFirstInstanceGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("jgjs", re);
								obj.set("lcjy", lc);
								obj.set("jg", jg);
								obj.set("ycbg", yc);
							});
							snpFirstInstanceGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}else if(records.length==0){
				message("请选择要编辑的样本！");
			}else{
				message("只能选择一条样本编辑！");
			}
		}
	});
//	opts.tbar.push({
//		text : "批量编辑数据",
//		handler : function() {
//			var records = snpFirstInstanceGrid.getSelectRecord();
//			if (records && records.length>0) {
//				var options = {};
//				options.width = 700;
//				options.height = 500;
//				loadDialogPage($("#bat_area2_div"), "批量编辑数据", null, {
//					"确定" : function() {
//							var re = $("#re1").val();
//							var lc = $("#lc1").val();
//							var jg = $("#jg1").val();
//							var yc = $("#yc1").val();
//							snpFirstInstanceGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								/*obj.set("jgjs", re);
//								obj.set("lcjy", lc);
//								obj.set("jg", jg);
//								obj.set("ycbg", yc);*/
//								if(re!=null && re!=""){
//									obj.set("jgjs", re);
//								}
//								if(lc!=null && lc!=""){
//									obj.set("lcjy", lc);
//								}
//								if(lc!=null && lc!=""){
//									obj.set("jg", jg);
//								}
//								if(lc!=null && lc!=""){
//									obj.set("ycbg", yc);
//								}
//							});
//							snpFirstInstanceGrid.startEditing(0, 0);
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}else{
//				message("请选择样本！");
//			}
//		}
//	});
	opts.tbar.push({
		text : '查看附件',
		handler : loadPic
	});
	opts.tbar.push({
		text : '预览报告',
		handler : lookReport
	});

//	opts.tbar.push({
//		text : "批量合格",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_result_div"), "批量合格", null, {
//				"确定" : function() {
//					var records = snpFirstInstanceGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var result = $("#result").val();
//						snpFirstInstanceGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("result", result);
//						});
//						snpFirstInstanceGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : "批量下一步",
//		handler : function() {
//			var records = SnpFirstInstanceGrid.getSelectRecord();
//			if(records.length>0){
//					loadTestNextFlowCob();
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});

	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = snpFirstInstanceGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						snpFirstInstanceGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						snpFirstInstanceGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : '提交样本',
		handler : submitSample
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	snpFirstInstanceGrid=gridEditTable("snpFirstInstancediv",cols,loadParam,opts);
	$("#snpFirstInstancediv").data("snpFirstInstanceGrid", snpFirstInstanceGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//提交样本
function submitSample(){
if(snpFirstInstanceGrid.getModifyRecord().length > 0){
	message("请先保存记录！");
	return;
}
var record = snpFirstInstanceGrid.getSelectionModel().getSelections();
var flg=false;
if(record.length>0){
	for(var i=0;i<record.length;i++){
		if(!record[i].get("submit")){
			flg=true;
		}
		if(record[i].get("jg")==""){
			message("结果为必填项！");
			return;
		}
		if(record[i].get("jgjs")==""){
			message("结果解释不能为空！");
			return;
		}
		if(record[i].get("lcjy")==""){
			message("临床建议不能为空！");
			return;
		}
		if(record[i].get("ycbg")==""){
			message("是否异常报告不能为空！");
			return;
		}
	}
}else{
	var grid=snpFirstInstanceGrid.store;
	for(var i=0;i<grid.getCount();i++){
		if(grid.getAt(i).get("submit")==""){
			flg=true;
		}
	}
}
if(flg){
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
			{
			        msg : '正在处理，请稍候。。。。。。',
			        removeMask : true// 完成后移除
			    });
	loadMarsk.show();
	var records = [];
	for ( var i = 0; i < record.length; i++) {
		records.push(record[i].get("id"));
	}
	ajax("post", "/experiment/snpjc/first/snpFirst/submitSample.action", {
		ids : records
	}, function(data) {
		if (data.success) {
			loadMarsk.hide();
			snpFirstInstanceGrid.getStore().commitChanges();
			snpFirstInstanceGrid.getStore().reload();
			message("提交成功！");
		} else {
			loadMarsk.hide();
			message("提交失败！");
		}
	}, null);
}else{
	message("没有需要提交的样本！");
}
}

//保存
function save(){
	var itemJson = commonGetModifyRecords(snpFirstInstanceGrid);
	if(itemJson.length>0){
		ajax("post", "/experiment/snpjc/first/snpFirst/saveSnpFirstInstance.action", {
			itemDataJson : itemJson
		}, function(data) {
			if (data.success) {
				snpFirstInstanceGrid.getStore().commitChanges();
				snpFirstInstanceGrid.getStore().reload();
				message("保存成功！");
			} else {
				message("保存失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}


function lookReport(){
	var ids=[];
	var idStr="";
	var idStr1="";
	var model="SnpFirstInstance";
	var task="snpAnalysisItem";
	var productId="";
	var selectGrid=snpFirstInstanceGrid.getSelectionModel().getSelections();
	var getGrid=snpFirstInstanceGrid.store;
	if(getGrid.getCount()>0){
		if (selectGrid.length==1) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				idStr+=obj.get("id");
				idStr1+=obj.get("taskResultId");
				productId+=obj.get("productId");
			});
			//idStr=ids.substring(0, ids.length-1);
			for ( var i = 0; i < selectGrid.length; i++) {
				if (!selectGrid[i].isNew) {
					ids.push(selectGrid[i].get("id"));
				}
			}
			window.open(window.ctx+"/reports/createReport/loadSnpReport.action?id="
					+idStr+"&taskId="+idStr1+"&model="+model+"&task="+task+"&productId="+productId,'','height=650,width=1050,scrollbars=yes,resizable=yes');
		}else{
			message("请选择一条数据！");
		}
	}else{
		message("列表中无数据！");
	}
}

function loadPic(){
	var ids="";
	var idStr="";
	var model="snpAnalysisItem";
	var selectGrid=snpFirstInstanceGrid.getSelectionModel().getSelections();
	var getGrid=snpFirstInstanceGrid.store;
	if(getGrid.getCount()>0){
		if (selectGrid.length > 0) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				ids+="'"+obj.get("taskResultId")+"',";
			});
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}else{
			//没有勾选明细查看全部附件
			
			for(var j=0;j<getGrid.getCount();j++){
				ids+="'"+getGrid.getAt(j).get("taskResultId")+"',";
			}
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}
	}else{
		message("列表中无数据！");
	}
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = snpFirstInstanceGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=SnpFisrtInstance&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = snpFirstInstanceGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = snpFirstInstanceGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}
	

//选择报告模板
function showsampleReportSelectList() {
	var selected=snpFirstInstanceGrid.getSelectionModel().getSelections();
	var productId="";
	var type="2";
	if(selected.length>0){
		if(selected.length>1){
			var productIds = new Array();
			$.each(selected, function(j, k) {
				productIds[j]=k.get("productId");
			});
			for(var i=0;i<selected.length;i++){
				if(i!=0 && productIds[i]!=productIds[i-1]){
					message("检测项目不同！");
					return;
				}else{
					productId=productIds[i];
				}
			}
		}else{
			$.each(selected, function(i, a) {
				productId=a.get("productId");
			});
		}
	}else{
		productId="";
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
	var showsampleReportSelectList = new Ext.Window(
			{
				id : 'showsampleReportSelectList',
				modal : true,
				title : '选择模版',
				layout : 'fit',
				width : 480,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sysmanage/report/selReportTemplateList.action?productId="+productId+"&type="+type+"' frameborder='0' width='780' height='500' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						showsampleReportSelectList.close();
					}
				} ]
			});
	showsampleReportSelectList.show();
}

function setReportFun(rec) {
	var selected=snpFirstInstanceGrid.getSelectionModel().getSelections();
	if(selected.length>0){
		$.each(selected,function(i,b){
			b.set("reportInfo-id",rec.get('id'));
			b.set("reportInfo-name",rec.get('name'));
			b.set("template-id",rec.get('attach-id'));
			b.set("template-fileName",rec.get('attach-fileName'));
		});
	}else{
		message("请选择要出报告的样本！");
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
}

//生成并下载报告文件
function createReport(ids) {
	var selected=snpFirstInstanceGrid.getSelectionModel().getSelections();
	var ids = [];
	if(snpFirstInstanceGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	if(selected.length>0){
		var flag=true;
		var id="";//选中的Item的id
		var code="";//选中的样本号
		var fnames="";//样本号不对应的图片
		$.each(selected,function(i,b){
			code=b.get("code");
			id=b.get("taskResultId");
		});
		ajax("post", "/common/comsearch/com/compareCode.action", {
			id : id,model:"snpAnalysisItem"
		}, function(data) {
			if (data.success) {
				$.each(data.data, function(i, obj) {
					var str=obj.fileName;
					var scode1 = new Array();
					scode1=str.split("-");
					if(scode1[0]==code){
						flag=true;
					}else{
						flag=false;
						fnames+="【"+obj.fileName+"】,";
					}
				});
			} else {
				message("请上传图片！");
				return;
			}
		}, null);
		if(fnames==""){
			for ( var i = 0; i < selected.length; i++) {
				if (!selected[i].isNew) {
					ids.push(selected[i].get("id"));
				}
			}
			ajax("post", "/experiment/snpjc/first/snpFirst/createReportFile.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					snpFirstInstanceGrid.getStore().commitChanges();
					snpFirstInstanceGrid.getStore().reload();
					//message("生成报告成功！");
				} else {
					message("预览报告失败！");
				}
			}, null);
			downFiles();
		}else{
			message("样本号不对应的图片有："+fnames);
			return;
		}
	}else{
		message("请选择要预览的样本！");
		return;
	}
}

//下载文件
function downFiles(){
	var selectGrid=snpFirstInstanceGrid.getSelectionModel().getSelections();
	var getGrid=snpFirstInstanceGrid.store;
	if(getGrid.getCount()>0){
		if(selectGrid.length>0){
			$.each(selectGrid, function(i, obj) {
				var fileName="PDF"+obj.get("code");
				window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'.pdf','','');
			});
//		if(selectGrid.length>0){
//			$.each(selectGrid, function(i, obj) {
//				ajax("post", "/common/comsearch/com/downloadReportFile.action", {
//					id : obj.get("taskResultId"),model:"snpAnalysisItem"
//				}, function(data) {
//					if (data.success) {
//						var num=data.num;
//						var fileName="PDF"+obj.get("code");
//						if(num>0){
//							if(num<=1){
//								window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'-1.pdf','','');
//							}else{
//								for(var i=0;i<num;i++){
//									window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'-'+(i+1)+'.pdf','','');
//								}
//							}
//						}
//					} else {
//						message("预览报告失败！");
//					}
//				}, null);
//			});
		}else{
			message("请选择样本！");
		}
	}else{
		message("列表中没有数据！");
	}
}

function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
		
			
			commonSearchAction(snpFirstInstanceGrid);
			$(this).dialog("close");

		},
		"清空" : function() {
			form_reset();

		}
	}, true, option);
}