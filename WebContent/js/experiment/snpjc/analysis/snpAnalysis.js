var snpAnalysisGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'fxDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'snpSj-id',
		type:"string"
	});
	    fields.push({
		name:'snpSj-name',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.user.itemNo,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*10,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'fxDate',
		header:biolims.common.analysisData,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:biolims.common.experimentalTemplateId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:biolims.common.experimentalTemplate,
		hidden:true,
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:biolims.common.acceptUserId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.common.acceptUserName,
		
		width:20*6,
		sortable:true
		});
		cm.push({
		dataIndex:'snpSj-id',
		hidden:true,
		header:biolims.common.sjCodeId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'snpSj-name',
		header:biolims.common.sjCode,
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'note',
		header:biolims.common.note,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.stateName,
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/analysis/snpAnalysis/showSnpAnalysisListJson.action";
	var opts={};
	opts.title=biolims.common.snpAnalysis;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	snpAnalysisGrid=gridTable("show_snpAnalysis_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/snpjc/analysis/snpAnalysis/editSnpAnalysis.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/snpjc/analysis/snpAnalysis/editSnpAnalysis.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/snpjc/analysis/snpAnalysis/viewSnpAnalysis.action?id=' + id;
}
function exportexcel() {
	snpAnalysisGrid.title = '导出列表';
	var vExportContent = snpAnalysisGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startfxDate").val() != undefined) && ($("#startfxDate").val() != '')) {
					var startfxDatestr = ">=##@@##" + $("#startfxDate").val();
					$("#fxDate1").val(startfxDatestr);
				}
				if (($("#endfxDate").val() != undefined) && ($("#endfxDate").val() != '')) {
					var endfxDatestr = "<=##@@##" + $("#endfxDate").val();

					$("#fxDate2").val(endfxDatestr);

				}
				
				
				commonSearchAction(snpAnalysisGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
