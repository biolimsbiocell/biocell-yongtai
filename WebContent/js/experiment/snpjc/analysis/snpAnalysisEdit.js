$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#snpAnalysis_state").val();
	if(id=="3"){
		load("/experiment/snpjc/analysis/snpAnalysis/showSnpAnalysisTempList.action", null, "#snpAnalysisTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#snpAnalysisTempPage").remove();
	}
});	
function add() {
	window.location = window.ctx + "/experiment/snpjc/analysis/snpAnalysis/editSnpAnalysis.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/snpjc/analysis/snpAnalysis/showSnpAnalysisList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
//	var selRecord = snpAnalysisItemGrid.store;
//	if (selRecord.getCount()> 0) {
//		for(var j=0;j<selRecord.getCount();j++){
//			var oldv = selRecord.getAt(j).get("submit");
//			var rid = selRecord.getAt(j).get("reportInfo-id");
//			if(oldv=="1"){
//				if(rid=="" || rid==null){
//					message("请上传附件！");
//					return;
//				}
//			}
//		}
//	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
			var selRecord = snpAnalysisItemGrid.store;
			if (selRecord.getCount()> 0) {
				if(snpAnalysisItemGrid.getModifyRecord().length > 0){
					message("请先保存记录！");
					return;
				}
				submitWorkflow("SnpAnalysis", {
					userId : userId,
					userName : userName,
					formId : $("#snpAnalysis_id").val(),
					title : $("#snpAnalysis_name").val()
				}, function() {
					window.location.reload();
				});
			}else{
				message("请添加样本！");
			}
				
});
$("#toolbarbutton_sp").click(function() {
	//var taskName=$("#taskName").val();
	//if(taskName=="SNP分析二审"){
	var selRecord = snpAnalysisItemGrid.store;
	if (selRecord.getCount()> 0) {
		for(var j=0;j<selRecord.getCount();j++){
			var oldv = selRecord.getAt(j).get("submit");
			if(oldv!=1){
				flag=false;
				message("有样本未提交！");
				return;
			}
			if(selRecord.getAt(j).get("result")==""){
				message("是否合格不能为空！");
				return;
			}
			//if(selRecord.getAt(j).get("nextFlow")==""){
			//	message("请填写下一步！");
			//	return;
			//}
		}
		if(snpAnalysisItemGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
		completeTask($("#snpAnalysis_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}else{
		completeTask($("#snpAnalysis_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
	//}else{
	//	completeTask($("#snpAnalysis_id").val(), $(this).attr("taskId"), function() {
	//		document.getElementById('toolbarSaveButtonFlag').value = 'save';
	//		location.href = window.ctx + '/dashboard/toDashboard.action';
	//	});
	//}
});






function save() {
if(checkSubmit()==true){
	    var snpAnalysisItemDivData = $("#snpAnalysisItemdiv").data("snpAnalysisItemGrid");
		document.getElementById('snpAnalysisItemJson').value = commonGetModifyRecords(snpAnalysisItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/snpjc/analysis/snpAnalysis/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/snpjc/analysis/snpAnalysis/copySnpAnalysis.action?id=' + $("#snpAnalysis_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#snpAnalysis_id").val() + "&tableId=snpAnalysis");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#snpAnalysis_id").val()){
		commonChangeState("formId=" + $("#snpAnalysis_id").val() + "&tableId=SnpAnalysis");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#snpAnalysis_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#snpAnalysis_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.snpAnalysis,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/snpjc/analysis/snpAnalysis/showSnpAnalysisItemList.action", {
				id : $("#snpAnalysis_id").val()
			}, "#snpAnalysisItempage");

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
			    	text: '复制'
					});
item.on('click', editCopy);
	
function SnpSjFun(){
	var win = Ext.getCmp('SnpSjFun');
	if (win) {win.close();}
	var SnpSjFun= new Ext.Window({
	id:'SnpSjFun',modal:true,title:'选择洗染扫描',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/experiment/snpjc/sj/snpSj/snpSjSelect.action?flag=SnpSjFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 SnpSjFun.close(); }  }]  });     SnpSjFun.show(); }
function setSnpSjFun(rec){
	var code=$("#snpAnalysis_snpSj").val();
	if(code==""){
		document.getElementById('snpAnalysis_snpSj').value=rec.get('id');
		document.getElementById('snpAnalysis_snpSj_name').value=rec.get('name');
		var win = Ext.getCmp('SnpSjFun');
		if(win){win.close();}
		var id=rec.get('id');
		ajax("post", "/experiment/snpjc/sj/snpSj/setSnpSjResult.action", {
			code : id,
			}, function(data) {
				if (data.success) {
					var ob = snpAnalysisItemGrid.getStore().recordType;
					snpAnalysisItemGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("code",obj.code);
						p.set("sampleCode",obj.sampleCode);
						p.set("chipNum",obj.chipNum);
						p.set("sampleType",obj.sampleType);
						p.set("productId",obj.productId);
						p.set("productName",obj.productName);
						p.set("note",obj.note);
						snpAnalysisItemGrid.getStore().add(p);							
					});
					snpAnalysisItemGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
	}else{
		if(rec.get('id')==code){
			var win = Ext.getCmp('SnpSjFun');
			if(win){win.close();}
		}else{
			var ob1 = snpAnalysisItemGrid.store;
			if (ob1.getCount() > 0) {
			for(var j=0;j<ob1.getCount();j++){
				var oldv = ob1.getAt(j).get("id");
				//根据ID删除
				if(oldv!=null){
					ajax("post", "/experiment/snpjc/analysis/snpAnalysis/delSnpAnalysisItemOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null);
				}else{								
					snpAnalysisItemGrid.store.removeAll();
				}
			}
			snpAnalysisItemGrid.store.removeAll();
			}
			document.getElementById('snpAnalysis_snpSj').value=rec.get('id');
			document.getElementById('snpAnalysis_snpSj_name').value=rec.get('name');
			var win = Ext.getCmp('SnpSjFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/experiment/snpjc/sj/snpSj/setSnpSjResult.action", {
				code : id,
				}, function(data) {
					if (data.success) {
						var ob = snpAnalysisItemGrid.getStore().recordType;
						snpAnalysisItemGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.code);
							p.set("sampleCode",obj.sampleCode);
							p.set("chipNum",obj.chipNum);
							p.set("sampleType",obj.sampleType);
							p.set("productId",obj.productId);
							p.set("productName",obj.productName);
							p.set("note",obj.note);
							snpAnalysisItemGrid.getStore().add(p);							
						});
						snpAnalysisItemGrid.startEditing(0, 0);		
					} else {
						message("获取明细数据时发生错误！");
					}
				}, null); 
		}
	}
}


Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = snpAnalysisItemGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}

			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "SnpAnalysis",id : $("#snpAnalysis_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

