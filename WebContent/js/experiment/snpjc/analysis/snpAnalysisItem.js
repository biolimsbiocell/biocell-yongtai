var snpAnalysisItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'chipNum',
		type:"string"
	});
	   fields.push({
		name:'lcjy',
		type:"string"
	});
	   fields.push({
		name:'ycbg',
		type:"string"
	});
	   fields.push({
		name:'jg',
		type:"string"
	});
	   fields.push({
		name:'jgjs',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'snpAnalysis-id',
		type:"string"
	});
	    fields.push({
		name:'snpAnalysis-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
        fields.push({
		name:'reportInfo-id',
		type:"string"
	});
	    fields.push({
		name:'reportInfo-name',
		type:"string"
	});
	    fields.push({
		name:'reportInfoId',
		type:"string"
	});
	    fields.push({
		name:'reportInfoName',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-fileName',
		type:"string"
	});
	    fields.push({
		name:'fileNum',
		type:"string"
	});
	    fields.push({
		name:'upTime',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	


	/*var storerCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var rCob = new Ext.form.ComboBox({
		store : storerCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});*/
	cm.push({
		xtype : 'actioncolumn',
		hidden : false,
		width : 120,
		header : biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : '附件',
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')){
					var win = Ext.getCmp('doc');
					rec.set("upTime",(new Date()).toString());
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+ rec.get('id')+"&modelType=snpAnalysisItem' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: '关闭',
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				}else{
					message("请先保存记录。");
				}
			}
		}]
	});
	cm.push({
		dataIndex:'reportInfo-id',
		hidden : true,
		header:biolims.common.templateId,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfo-name',
		hidden : false,
		header:biolims.common.templateName,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfoId',
		hidden : true,
		header:biolims.common.templateId,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfoName',
		hidden : true,
		header:biolims.report.ReportTemplate,
		sortable:true,
		width:15*10
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1',biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.toSubmit+'<font color="red" size=4"">*</font>',
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:biolims.report.reportFileId,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'template-fileName',
		hidden : true,
		header:biolims.report.reportFileName,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'jg',
		hidden : false,
		header:biolims.common.analysisResult,
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'jgjs',
		hidden : false,
		header:biolims.common.resultsCommentate,
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'lcjy',
//		hidden : false,
//		header:'临床建议',
//		width:30*6,
//		
//		editor : new Ext.form.TextArea({
//			allowBlank : true
//		})
//	});
	var storelcjyCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.outpatientVisits ], [ '0', biolims.common.ycoutpatientVisits ] ]
	});
	var lcjyCob = new Ext.form.ComboBox({
		store : storelcjyCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'lcjy',
		hidden : false,
		header:biolims.common.clinicalNote,
		width:20*6,
		editor : lcjyCob,
		renderer : Ext.util.Format.comboRenderer(lcjyCob)
	});
	var storeexCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1',biolims.common.regular ], [ '0', biolims.common.abnormal ], [ '2', biolims.common.polymorphic ] ]
	});
	var exCob = new Ext.form.ComboBox({
		store : storeexCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	
	cm.push({
		dataIndex:'ycbg',
		hidden : false,
		header:biolims.common.exceptionReported,
		width:20*6,
		
		editor : exCob,
		renderer : Ext.util.Format.comboRenderer(exCob)
	});
	
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified ], [ '0', biolims.common.disqualified ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.isQualified+'<font color="red" size=4"">*</font>',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	/*var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向<font color="red" size=4"">*</font>',
		width:15*10,
		sortable:true,
		editor : nextFlowCob
	});*/


	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:60*6
	});
	cm.push({
		dataIndex:'snpAnalysis-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10
	});
	cm.push({
		dataIndex:'snpAnalysis-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'upTime',
		hidden : true,
		header:'附件修改时间',
		width:20*10
	});
	cm.push({
		dataIndex:'chipNum',
		hidden : false,
		header:biolims.common.chipBar,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
	dataIndex : 'fileNum',
	header : biolims.common.tempId,
	hidden : true,
	width : 60
//	handler: requestScope.fileNum
});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/analysis/snpAnalysis/showSnpAnalysisItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.snpAnalysisDetail;
	opts.height =  document.body.clientHeight-148;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snpjc/analysis/snpAnalysis/delSnpAnalysisItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//		text : '选择报告模板',
//		handler : showsampleReportSelectList
//	});

//	opts.tbar.push({
//		text : "批量合格",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_result_div"), "批量合格", null, {
//				"确定" : function() {
//					var records = snpAnalysisItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var result = $("#result").val();
//						snpAnalysisItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("result", result);
//						});
//						snpAnalysisItemGrid.startEditing(0, 0);
//					}else{
//						message("请选择样本！");
//					}
//					$(this).dialog("close");
//					
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : "批量下一步",
//		handler : function() {
//			var records = snpAnalysisItemGrid.getSelectRecord();
//			if(records.length>0){
//					loadTestNextFlowCob();
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.report.checkFile,
		handler : loadPic
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.vim,
		handler : function() {
			var records = snpAnalysisItemGrid.getSelectRecord();
			if (records && records.length==1) {
				$.each(records, function(i, obj) {
				if(obj.get("jgjs")!=null && obj.get("jgjs")!=""){
					$("#re").val(obj.get("jgjs"));
				}
				if(obj.get("lcjy")!=null && obj.get("lcjy")!=""){
					$("#lc").val(obj.get("lcjy"));
				}
				if(obj.get("jg")!=null && obj.get("jg")!=""){
					$("#jg").val(obj.get("jg"));
				}
				if(obj.get("ycbg")!=null && obj.get("ycbg")!=""){
					$("#yc").val(obj.get("ycbg"));
				}
				});
				var options = {};
				options.width = 700;
				options.height = 500;
				loadDialogPage($("#bat_area_div"), "编辑数据", null, {
					"确定" : function() {
							var re = $("#re").val();
							var lc = $("#lc").val();
							var jg = $("#jg").val();
							var yc = $("#yc").val();
							snpAnalysisItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("jgjs", re);
								obj.set("lcjy", lc);
								obj.set("jg", jg);
								obj.set("ycbg", yc);
							});
							snpAnalysisItemGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}else if(records.length==0){
				message("请选择要编辑的样本！");
			}else{
				message("只能选择一条样本编辑！");
			}
		}
	});
//	opts.tbar.push({
//		text : "批量编辑数据",
//		handler : function() {
//			var records = snpAnalysisItemGrid.getSelectRecord();
//			if (records && records.length>0) {
//				var options = {};
//				options.width = 700;
//				options.height = 500;
//				loadDialogPage($("#bat_area1_div"), "批量编辑数据", null, {
//					"确定" : function() {
//							var re = $("#re1").val();
//							var lc = $("#lc1").val();
//							var jg = $("#jg1").val();
//							var yc = $("#yc1").val();
//							snpAnalysisItemGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								/*obj.set("jgjs", re);
//								obj.set("lcjy", lc);
//								obj.set("jg", jg);
//								obj.set("ycbg", yc);*/
//								if(re!=null && re!=""){
//									obj.set("jgjs", re);
//								}
//								if(lc!=null && lc!=""){
//									obj.set("lcjy", lc);
//								}
//								if(lc!=null && lc!=""){
//									obj.set("jg", jg);
//								}
//								if(lc!=null && lc!=""){
//									obj.set("ycbg", yc);
//								}
//							});
//							snpAnalysisItemGrid.startEditing(0, 0);
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}else{
//				message("请选择样本！");
//			}
//		}
//	});
	opts.tbar.push({
		text : biolims.report.checkReport,
		handler : lookReport
	});
	
//opts.tbar.push({
//	text : "批量提交",
//	handler : function() {
//		var options = {};
//		options.width = 400;
//		options.height = 300;
//		loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//			"确定" : function() {
//				var records = snpAnalysisItemGrid.getSelectRecord();
//				if (records && records.length > 0) {
//					var submit = $("#submit").val();
//					snpAnalysisItemGrid.stopEditing();
//					$.each(records, function(i, obj) {
//						obj.set("submit", submit);
//					});
//					snpAnalysisItemGrid.startEditing(0, 0);
//				}else{
//					message("请选择样本！");
//				}
//				$(this).dialog("close");
//			}
//		}, true, options);
//	}
//});
	
opts.tbar.push({
	text : biolims.common.submitSample,
	handler : submitSample
});
opts.tbar.push({
	iconCls : 'save',
	text : biolims.common.save,
	handler : saveInfo
});
	
	snpAnalysisItemGrid=gridEditTable("snpAnalysisItemdiv",cols,loadParam,opts);
	$("#snpAnalysisItemdiv").data("snpAnalysisItemGrid", snpAnalysisItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(snpAnalysisItemGrid);
	var id=$("#snpAnalysis_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/snpjc/analysis/snpAnalysis/saveSnpAnalysisItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					snpAnalysisItemGrid.getStore().commitChanges();
					snpAnalysisItemGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

//提交样本
function submitSample(){
	var id=$("#snpAnalysis_id").val();
	if(snpAnalysisItemGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	var record = snpAnalysisItemGrid.getSelectionModel().getSelections();
	var flg=false;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(!record[i].get("submit")){
				flg=true;
			}
		}
	}else{
		var grid=snpAnalysisItemGrid.store;
		for(var i=0;i<grid.getCount();i++){
			if(grid.getAt(i).get("submit")==""){
				flg=true;
			}
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		var records = [];
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		ajax("post", "/experiment/snpjc/analysis/snpAnalysis/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				snpAnalysisItemGrid.getStore().commitChanges();
				snpAnalysisItemGrid.getStore().reload();
				message("提交成功！");
			} else {
				loadMarsk.hide();
				message("提交失败！");
			}
		}, null);
	}else{
		message("没有需要提交的样本！");
	}
}

/*//保存
function save1(){
	var id=$("#snpAnalysis_id").val();
	var itemJson = commonGetModifyRecords(snpAnalysisItemGrid);
	if(itemJson.length>0){
		ajax("post", "/experiment/snpjc/analysis/snpAnalysis/saveSnpAnalysisResult.action", {
			itemDataJson : itemJson,id : id
		}, function(data) {
			if (data.success) {
				snpAnalysisItemGrid.getStore().commitChanges();
				snpAnalysisItemGrid.getStore().reload();
				message("提交成功！");
			} else {
				message("提交失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}*/


function lookReport(){
	var ids=[];
	var idStr="";
	var idStr1="";
	var model="SnpAnalysisItem";
	var task="snpAnalysisItem";
	var productId="";
	var code="";//选中的样本号
	var fnames="";//样本号不对应的图片
	var selectGrid=snpAnalysisItemGrid.getSelectionModel().getSelections();
	var getGrid=snpAnalysisItemGrid.store;
	if(getGrid.getCount()>0){
		if (selectGrid.length==1) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				code=obj.get("code");
				idStr+=obj.get("id");
				idStr1+=obj.get("id");
				productId+=obj.get("productId");
			});
			//idStr=ids.substring(0, ids.length-1);
			ajax("post", "/common/comsearch/com/compareCode.action", {
				id : idStr, model:"snpAnalysisItem"
			}, function(data) {
				if (data.success) {
					$.each(data.data, function(i, obj) {
						var str=obj.fileName;
						var scode1 = new Array();
						scode1=str.split("-");
						if(scode1[0]==code){
							flag=true;
						}else{
							flag=false;
							fnames+="【"+obj.fileName+"】,";
						}
					});
				} else {
					message("请上传图片！");
					return;
				}
			}, null);
			if(fnames==""){
				for ( var i = 0; i < selectGrid.length; i++) {
					if (!selectGrid[i].isNew) {
						ids.push(selectGrid[i].get("id"));
					}
				}
				window.open(window.ctx+"/reports/createReport/loadSnpReport.action?id="
					+idStr+"&taskId="+idStr1+"&model="+model+"&task="+task+"&productId="+productId,'',
					'height=650,width=1050,scrollbars=yes,resizable=yes');
			}else{
				message("样本号不对应的图片有："+fnames);
				return;
			}
		}else{
			message("请选择一条数据！");
		}
	}else{
		message("列表中无数据！");
	}
}


function loadPic(){
	var ids="";
	var idStr="";
	var model="snpAnalysisItem";
	var selectGrid=snpAnalysisItemGrid.getSelectionModel().getSelections();
	var getGrid=snpAnalysisItemGrid.store;
	if(getGrid.getCount()){
		if (selectGrid.length > 0) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				ids+="'"+obj.get("id")+"',";
			});
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}else{
			//没有勾选明细查看全部附件
			
			for(var j=0;j<getGrid.getCount();j++){
				ids+="'"+getGrid.getAt(j).get("id")+"',";
			}
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}
	}else{
		message("列表没有数据");
	}
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = snpAnalysisItemGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=SnpAnalysis&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = snpAnalysisItemGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = snpAnalysisItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}
	

//选择报告模板
function showsampleReportSelectList() {
	var selected=snpAnalysisItemGrid.getSelectionModel().getSelections();
	var productId="";
	var type="2";
	if(selected.length>0){
		if(selected.length>1){
			var productIds = new Array();
			$.each(selected, function(j, k) {
				productIds[j]=k.get("productId");
			});
			for(var i=0;i<selected.length;i++){
				if(i!=0 && productIds[i]!=productIds[i-1]){
					message("检测项目不同！");
					return;
				}else{
					productId=productIds[i];
				}
			}
		}else{
			$.each(selected, function(i, a) {
				productId=a.get("productId");
			});
		}
	}else{
		productId="";
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
	var showsampleReportSelectList = new Ext.Window(
			{
				id : 'showsampleReportSelectList',
				modal : true,
				title : '选择模版',
				layout : 'fit',
				width : 480,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sysmanage/report/selReportTemplateList.action?productId="+productId+"&type="+type+"' frameborder='0' width='780' height='500' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						showsampleReportSelectList.close();
					}
				} ]
			});
	showsampleReportSelectList.show();
}

function setReportFun(rec) {
	var selected=snpAnalysisItemGrid.getSelectionModel().getSelections();
	if(selected.length>0){
		$.each(selected,function(i,b){
			b.set("reportInfo-id",rec.get('id'));
			b.set("reportInfo-name",rec.get('name'));
			b.set("template-id",rec.get('attach-id'));
			b.set("template-fileName",rec.get('attach-fileName'));
		});
	}else{
		message("请选择要出报告的样本！");
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
}

//生成并下载报告文件
function createReport(ids) {
	var selected=snpAnalysisItemGrid.getSelectionModel().getSelections();
	var ids = [];
	if(snpAnalysisItemGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	if(selected.length>0){
		var flag=true;
		var id="";//选中的Item的id
		var code="";//选中的样本号
		var fnames="";//样本号不对应的图片
		$.each(selected,function(i,b){
			code=b.get("code");
			id=b.get("id");
			/*if(b.get("template-id")=="" || b.get("template-id")==null
					|| b.get("template-id")==undefined){
				flag=false;
				message("请选择报告模板！");
			}else{
				flag=true;
			}*/
		});
		ajax("post", "/common/comsearch/com/compareCode.action", {
			id : id,model:"snpAnalysisItem"
		}, function(data) {
			if (data.success) {
				$.each(data.data, function(i, obj) {
					var str=obj.fileName;
					var scode1 = new Array();
					scode1=str.split("-");
					if(scode1[0]==code){
						flag=true;
					}else{
						flag=false;
						fnames+="【"+obj.fileName+"】,";
					}
				});
			} else {
				message("请上传图片！");
				return;
			}
		}, null);
		if(fnames==""){
			for ( var i = 0; i < selected.length; i++) {
				if (!selected[i].isNew) {
					ids.push(selected[i].get("id"));
				}
			}
			ajax("post", "/experiment/snpjc/analysis/snpAnalysis/createReportFile.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					snpAnalysisItemGrid.getStore().commitChanges();
					snpAnalysisItemGrid.getStore().reload();
				} else {
					message("预览报告失败！");
				}
			}, null);
			downFiles();
		}else{
			message("样本号不对应的图片有："+fnames);
			return;
		}
	}else{
		message("请选择要预览的样本！");
		return;
	}
}

//下载文件
function downFiles(){
	var selectGrid=snpAnalysisItemGrid.getSelectionModel().getSelections();
	if(selectGrid.length>0){
		$.each(selectGrid, function(i, obj) {
			var fileName="PDF"+obj.get("code");
			window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'.pdf','','');
		});
	}
//	var selectGrid=snpAnalysisItemGrid.getSelectionModel().getSelections();
//	if(selectGrid.length>0){
//		$.each(selectGrid, function(i, obj) {
//			ajax("post", "/common/comsearch/com/downloadReportFile.action", {
//				id : obj.get("id"),model:"snpAnalysisItem"
//			}, function(data) {
//				if (data.success) {
//					var num=data.num;
//					var fileName="PDF"+obj.get("code");
//					if(num>0){
//						if(num<=1){
//							window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'-1.pdf','','');
//						}else{
//							for(var i=0;i<num;i++){
//								window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'-'+(i+1)+'.pdf','','');
//							}
//						}
//					}
//				} else {
//					message("预览报告失败！");
//				}
//			}, null);
//		});
//	}
}