						$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#snpCross_state").val();
	if(id=="3"){
		load("/experiment/snpjc/cross/snpCross/showSnpCrossTempList.action", null, "#snpCrossTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#snpCrossTemppage").remove();
	}
	setTimeout(function() {
		var getGrid=snpCrossTemplateGrid.store;
		if(getGrid.getCount()==0){
			//alert(getGrid.getCount());
			loadTemplate($("#snpCross_template").val());
		}
	}, 2000);
});	
function add() {
	window.location = window.ctx + "/experiment/snpjc/cross/snpCross/editSnpCross.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/snpjc/cross/snpCross/showSnpCrossList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("SnpCross", {
					userId : userId,
					userName : userName,
					formId : $("#snpCross_id").val(),
					title : $("#snpCross_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	if(snpCrossResultGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
		completeTask($("#snpCross_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
});






function save() {
if(checkSubmit()==true){
	    var snpCrossItemDivData = $("#snpCrossItemdiv").data("snpCrossItemGrid");
		document.getElementById('snpCrossItemJson').value = commonGetModifyRecords(snpCrossItemDivData);
	    var snpCrossTemplateDivData = $("#snpCrossTemplatediv").data("snpCrossTemplateGrid");
		document.getElementById('snpCrossTemplateJson').value = commonGetModifyRecords(snpCrossTemplateDivData);
	    var snpCrossReagentDivData = $("#snpCrossReagentdiv").data("snpCrossReagentGrid");
		document.getElementById('snpCrossReagentJson').value = commonGetModifyRecords(snpCrossReagentDivData);
	    var snpCrossCosDivData = $("#snpCrossCosdiv").data("snpCrossCosGrid");
		document.getElementById('snpCrossCosJson').value = commonGetModifyRecords(snpCrossCosDivData);
	    var snpCrossResultDivData = $("#snpCrossResultdiv").data("snpCrossResultGrid");
		document.getElementById('snpCrossResultJson').value = commonGetModifyRecords(snpCrossResultDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/snpjc/cross/snpCross/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/snpjc/cross/snpCross/copySnpCross.action?id=' + $("#snpCross_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#snpCross_id").val() + "&tableId=snpCross");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#snpCross_id").val()){
		commonChangeState("formId=" + $("#snpCross_id").val() + "&tableId=SnpCross");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#snpCross_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#snpCross_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#snpCross_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.snpHybridization,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/snpjc/cross/snpCross/showSnpCrossItemList.action", {
				id : $("#snpCross_id").val()
			}, "#snpCrossItempage");
load("/experiment/snpjc/cross/snpCross/showSnpCrossTemplateList.action", {
				id : $("#snpCross_id").val()
			}, "#snpCrossTemplatepage");
load("/experiment/snpjc/cross/snpCross/showSnpCrossReagentList.action", {
				id : $("#snpCross_id").val()
			}, "#snpCrossReagentpage");
load("/experiment/snpjc/cross/snpCross/showSnpCrossCosList.action", {
				id : $("#snpCross_id").val()
			}, "#snpCrossCospage");
load("/experiment/snpjc/cross/snpCross/showSnpCrossResultList.action", {
				id : $("#snpCross_id").val()
			}, "#snpCrossResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	//调用模板
	function TemplateFun(){
				var type="doSnpCross";
				var win = Ext.getCmp('TemplateFun');
				if (win) {win.close();}
				var TemplateFun= new Ext.Window({
				id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
				plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
				collapsible: true,maximizable: true,
				items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
				html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
				buttons: [
				{ text: '关闭',
				 handler: function(){
				 TemplateFun.close(); }  }]  }); 
				 TemplateFun.show(); 
			}
			
			function setTemplateFun(rec){
				document.getElementById('snpCross_acceptUser').value=rec.get('acceptUser-id');
				document.getElementById('snpCross_acceptUser_name').value=rec.get('acceptUser-name');
				var code=$("#snpCross_template").val();
				if(code==""){
							document.getElementById('snpCross_template').value=rec.get('id');
							document.getElementById('snpCross_template_name').value=rec.get('name');
							var win = Ext.getCmp('TemplateFun');
							if(win){win.close();}
							var id=rec.get('id');
							ajax("post", "/system/template/template/setTemplateItem.action", {
								code : id,
								}, function(data) {
									if (data.success) {
										var ob = snpCrossTemplateGrid.getStore().recordType;
										snpCrossTemplateGrid.stopEditing();
										$.each(data.data, function(i, obj) {
											var p = new ob({});
											p.isNew = true;
											p.set("tItem",obj.id);
											p.set("code",obj.code);
											p.set("stepName",obj.name);
											
											p.set("note",obj.note);
											snpCrossTemplateGrid.getStore().add(p);							
										});
										
										snpCrossTemplateGrid.startEditing(0, 0);		
									} else {
										message("获取明细数据时发生错误！");
									}
								}, null); 
								ajax("post", "/system/template/template/setTemplateReagent.action", {
								code : id,
								}, function(data) {
									if (data.success) {

										var ob = snpCrossReagentGrid.getStore().recordType;
										snpCrossReagentGrid.stopEditing();
										
										$.each(data.data, function(i, obj) {
											var p = new ob({});
											p.isNew = true;
											p.set("tReagent",obj.id);
											p.set("code",obj.code);
											p.set("name",obj.name);
											p.set("batch",obj.batch);
											p.set("isGood",obj.isGood);
											p.set("itemId",obj.itemId);
											p.set("saveCondition",obj.saveCondition);
											p.set("oneNum",obj.num);
											p.set("note",obj.note);
											p.set("sn",obj.sn);
											p.set("xishu",obj.xishu);
											snpCrossReagentGrid.getStore().add(p);							
										});
										
										snpCrossReagentGrid.startEditing(0, 0);		
									} else {
										message("获取明细数据时发生错误！");
									}
								}, null); 
								ajax("post", "/system/template/template/setTemplateCos.action", {
								code : id,
								}, function(data) {
									if (data.success) {

										var ob = snpCrossCosGrid.getStore().recordType;
										snpCrossCosGrid.stopEditing();
										
										$.each(data.data, function(i, obj) {
											var p = new ob({});
											p.isNew = true;
											p.set("tCos",obj.id);
											p.set("code",obj.code);
											p.set("name",obj.name);
											p.set("isGood",obj.isGood);
											p.set("itemId",obj.itemId);
											
											p.set("temperature",obj.temperature);
											p.set("speed",obj.speed);
											p.set("time",obj.time);
											p.set("note",obj.note);
											snpCrossCosGrid.getStore().add(p);							
										});			
										snpCrossCosGrid.startEditing(0, 0);		
									} else {
										message("获取明细数据时发生错误！");
									}
								}, null);

					
				}else{
					if(rec.get('id')==code){
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
		 			 }else{
								var ob1 = snpCrossTemplateGrid.store;
				 				if (ob1.getCount() > 0) {
									for(var j=0;j<ob1.getCount();j++){
										var oldv = ob1.getAt(j).get("id"); 
										alert(oldv);
										//根据ID删除
										if(oldv!=null){
											ajax("post", "/experiment/snpjc/cross/snpCross/delOne.action", {
												ids : oldv,model:"1"
											}, function(data) {
												if (data.success) {
													message("删除成功！");
												} else {
													message("删除失败！");
												}
											}, null);
										}else{								
											snpCrossTemplateGrid.store.removeAll();
										}
									}
									snpCrossTemplateGrid.store.removeAll();
				 				}

								var ob2 = snpCrossReagentGrid.store;
								if (ob2.getCount() > 0) {
									for(var j=0;j<ob2.getCount();j++){
										var oldv = ob2.getAt(j).get("id");
										alert(oldv);
										//根据ID删除
										if(oldv!=null){
										ajax("post", "/experiment/snpjc/cross/snpCross/delOne.action", {
											ids : oldv,model:"2"
										}, function(data) {
											if (data.success) {
												message("删除成功！");
											} else {
												message("删除失败！");
											}
										}, null); 
										}else{
											snpCrossReagentGrid.store.removeAll();
										}
									}
									snpCrossReagentGrid.store.removeAll();
				 				}
								//=========================================
								var ob3 = snpCrossCosGrid.store;
								if (ob3.getCount() > 0) {
									for(var j=0;j<ob3.getCount();j++){
										var oldv = ob3.getAt(j).get("id");
										alert(oldv);
										//根据ID删除
										if(oldv!=null){
											ajax("post", "/experiment/snpjc/cross/snpCross/delOne.action", {
												ids : oldv,model:"3"
											}, function(data) {
												if (data.success) {
													message("删除成功！");
												} else {
													message("删除失败！");
												}
											}, null); 
										}else{
											snpCrossCosGrid.store.removeAll();
										}
									}
									snpCrossCosGrid.store.removeAll();
				 				}
								document.getElementById('snpCross_template').value=rec.get('id');
								document.getElementById('snpCross_template_name').value=rec.get('name');
				 				var win = Ext.getCmp('TemplateFun');
				 				if(win){win.close();}
								var id = rec.get('id');
								ajax("post", "/system/template/template/setTemplateItem.action", {
									code : id,
									}, function(data) {
										if (data.success) {	

											var ob = snpSampleTemplateGrid.getStore().recordType;
											snpSampleTemplateGrid.stopEditing();
											
											$.each(data.data, function(i, obj) {
												var p = new ob({});
												p.isNew = true;
												p.set("tItem",obj.id);
												p.set("code",obj.code);
												p.set("stepName",obj.name);
												
												p.set("note",obj.note);
												snpSampleTemplateGrid.getStore().add(p);							
											});
											
											snpSampleTemplateGrid.startEditing(0, 0);		
										} else {
											message("获取明细数据时发生错误！");
										}
									}, null); 
									ajax("post", "/system/template/template/setTemplateReagent.action", {
									code : id,
									}, function(data) {
										if (data.success) {	

											var ob = snpSampleReagentGrid.getStore().recordType;
											snpSampleReagentGrid.stopEditing();
											
											$.each(data.data, function(i, obj) {
												var p = new ob({});
												p.isNew = true;
												p.set("tReagent",obj.id);
												p.set("code",obj.code);
												p.set("name",obj.name);
												p.set("batch",obj.batch);
												p.set("isGood",obj.isGood);
												p.set("itemId",obj.itemId);
												p.set("saveCondition",obj.saveCondition);
												p.set("oneNum",obj.num);
												p.set("note",obj.note);
												p.set("xishu",obj.xishu);
												snpSampleReagentGrid.getStore().add(p);							
											});
											
											snpSampleReagentGrid.startEditing(0, 0);		
										} else {
											message("获取明细数据时发生错误！");
										}
									}, null); 
									ajax("post", "/system/template/template/setTemplateCos.action", {
									code : id,
									}, function(data) {
										if (data.success) {	

											var ob = snpSampleCosGrid.getStore().recordType;
											snpSampleCosGrid.stopEditing();
											
											$.each(data.data, function(i, obj) {
												var p = new ob({});
												p.isNew = true;
												p.set("tCos",obj.id);
												p.set("code",obj.code);
												p.set("name",obj.name);
												p.set("isGood",obj.isGood);
												p.set("itemId",obj.itemId);
												
												p.set("temperature",obj.temperature);
												p.set("speed",obj.speed);
												p.set("time",obj.time);
												p.set("note",obj.note);
												snpSampleCosGrid.getStore().add(p);							
											});			
											snpSampleCosGrid.startEditing(0, 0);		
										} else {
											message("获取明细数据时发生错误！");
										}
									}, null); 
								}
							}
		}
			

			Ext.onReady(function(){
				var item = menu.add({
				    	text: '回滚'
					});
				item.on('click', ckcrk);
				
				});
			function ckcrk(){
				
				Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
					if (button == "yes") {
								ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
									code : $("#snpCross_id").val(), nextFlowId : "SnpCross"
								}, function(data) {
									if (data.success) {	
										message("回滚成功！");
									} else {
										message("回滚失败！");
									}
								}, null);
							
					}
				});
			}	
			Ext.onReady(function(){
				var item = menu.add({
				    	text: '保存'
					});
				item.on('click', ckcrk2);
				
				});
			function ckcrk2(){
				save();
			}
			Ext.onReady(function(){
				var item = menu.add({
				    	text: '办理回滚结果'
					});
				item.on('click', ckcrk3);
				
				});
			function ckcrk3(){
				Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
				ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
					model : "SnpCross",id : $("#snpCross_id").val()
				}, function(data) {
					if (data.success) {	
						message("办理回滚成功！");
					} else {
						message("办理回滚失败！");
					}
				}, null);
			}
			
			
			
//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = snpCrossTemplateGrid.getStore().recordType;
				snpCrossTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("stepName",obj.name);
					
					p.set("note",obj.note);
					snpCrossTemplateGrid.getStore().add(p);							
				});
				
				snpCrossTemplateGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = snpCrossReagentGrid.getStore().recordType;
				snpCrossReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("saveCondition",obj.saveCondition);
					p.set("oneNum",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					p.set("xishu",obj.xishu);
					snpCrossReagentGrid.getStore().add(p);							
				});
				
				snpCrossReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = snpCrossCosGrid.getStore().recordType;
				snpCrossCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					snpCrossCosGrid.getStore().add(p);							
				});			
				snpCrossCosGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
}
