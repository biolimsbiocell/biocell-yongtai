var snpSjItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'chipNum',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'snpSj-id',
		type:"string"
	});
	    fields.push({
		name:'snpSj-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'chipNum',
		hidden : false,
		header:biolims.common.chipNumber,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow,
		width:15*10,
		hidden:true,
		sortable:true,
		editor : nextFlowCob
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified ], [ '0', biolims.common.disqualified ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.isQualified+'<font color="red" size=4"">*</font>',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.Submitted+'<font color="red" size=4"">*</font>',
		width:20*6,
//		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'snpSj-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSj-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/sj/snpSj/showSnpSjItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.andScanningResult;
	opts.height =  document.body.clientHeight-170;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snpjc/sj/snpSj/delSnpSjItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				snpSjItemGrid.getStore().commitChanges();
				snpSjItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text :biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = snpSjItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						snpSjItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						snpSjItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'save',
		text :biolims.common.save,
		handler : saveInfo
	});
	snpSjItemGrid=gridEditTable("snpSjItemdiv",cols,loadParam,opts);
	$("#snpSjItemdiv").data("snpSjItemGrid", snpSjItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(snpSjItemGrid);
	var id=$("#snpSj_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/snpjc/sj/snpSj/saveSnpSjItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					snpSjItemGrid.getStore().commitChanges();
					snpSjItemGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

//提交样本
function submitSample(){
	var id=$("#snpSj_id").val();  
	if(snpSjItemGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	var record = snpSjItemGrid.getSelectionModel().getSelections();
	var flg=false;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(!record[i].get("submit")){
				flg=true;
			}
			if(record[i].get("result")==""){
				message("结果不能为空！");
				return;
			}
			
		}
	}else{
		var grid=snpSjItemGrid.store;
		for(var i=0;i<grid.getCount();i++){
			if(grid.getAt(i).get("submit")==""){
				flg=true;
			}
			if(grid.getAt(i).get("result")==""){
				message("结果不能为空！");
				return;
			}
		
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		
		
		var records = [];
		
		
		
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		
		ajax("post", "/experiment/snpjc/sj/snpSj/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				snpSjItemGrid.getStore().commitChanges();
				snpSjItemGrid.getStore().reload();
				message("提交成功！");
			} else {
				loadMarsk.hide();
				message("提交失败！");
			}
		}, null);
	}else{
		message("没有需要提交的样本！");
	}
}
	
