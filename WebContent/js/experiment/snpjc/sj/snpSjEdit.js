$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/experiment/snpjc/sj/snpSj/editSnpSj.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/snpjc/sj/snpSj/showSnpSjList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("SnpSj", {
					userId : userId,
					userName : userName,
					formId : $("#snpSj_id").val(),
					title : $("#snpSj_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	var codeList = new Array();
	var codeList1 = new Array();
	var selRecord1 = snpSjItemGrid.store;
	var flag=true;
	var flag1=true;
	if (snpSjItemGrid.getAllRecord().length > 0) {
		var selRecord = snpSjItemGrid.store;
		for(var j=0;j<selRecord.getCount();j++){

			codeList.push(selRecord.getAt(j).get("sampleCode"));
			if(selRecord.getAt(j).get("result")==""){
				message("结果不能为空！");
				return;
			}
	if(snpSjItemGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
		completeTask($("#snpSj_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
		}
	}
});





function save() {
if(checkSubmit()==true){
	 // var snpSjItemDivData = $("#snpSjItemdiv").data("snpSjItemGrid");
		document.getElementById('snpSjItemJson').value = commonGetModifyRecords(snpSjItemGrid);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/snpjc/sj/snpSj/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/snpjc/sj/snpSj/copySnpSj.action?id=' + $("#snpSj_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#snpSj_id").val() + "&tableId=snpSj");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#snpSj_id").val()){
		commonChangeState("formId=" + $("#snpSj_id").val() + "&tableId=SnpSj");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#snpSj_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#snpSj_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.andScanning,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/snpjc/sj/snpSj/showSnpSjItemList.action", {
				id : $("#snpSj_id").val()
			}, "#snpSjItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	//选择SNP杂交实验
/*	function ProducerTaskFun(){
		var win = Ext.getCmp('ProducerTaskFun');
		if (win) {win.close();}
		var ProducerTaskFun= new Ext.Window({
		id:'ProducerTaskFun',modal:true,title:'选择SNP杂交编号',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/experiment/snpjc/cross/snpCross/snpCrossSelect.action?flag=SnpCrossFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
		 ProducerTaskFun.close(); }  }]  });     
		 ProducerTaskFun.show(); 
	 }

	function setSnpCrossFun(rec){
		var code=$("#snpSj_snpCross").val();
		if(code==""){
			document.getElementById('snpSj_snpCross').value=rec.get('id');
			document.getElementById('snpSj_snpCross_name').value=rec.get('name');
			var win = Ext.getCmp('ProducerTaskFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/experiment/snpjc/cross/snpCross/setSnpCrossResult.action", {
				code : id,
				}, function(data) {
					if (data.success) {
						var ob = snpSjItemGrid.getStore().recordType;
						snpSjItemGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.code);
							p.set("sampleCode",obj.sampleCode);
							p.set("chipNum",obj.chipNum);
							p.set("sampleType",obj.sampleType);
							p.set("productId",obj.productId);
							p.set("productName",obj.productName);
							p.set("note",obj.note);
							snpSjItemGrid.getStore().add(p);							
						});
						snpSjItemGrid.startEditing(0, 0);		
					} else {
						message("获取明细数据时发生错误！");
					}
				}, null); 
		}else{
			if(rec.get('id')==code){
				var win = Ext.getCmp('ProducerTaskFun');
				if(win){win.close();}
			}else{
				var ob1 = snpSjItemGrid.store;
				if (ob1.getCount() > 0) {
				for(var j=0;j<ob1.getCount();j++){
					var oldv = ob1.getAt(j).get("id");
					//根据ID删除
					if(oldv!=null){
						ajax("post", "/experiment/snpjc/sj/snpSj/delSnpSjItemOne.action", {
							ids : oldv
						}, function(data) {
							if (data.success) {
								message("删除成功！");
							} else {
								message("删除失败！");
							}
						}, null);
					}else{								
						snpSjItemGrid.store.removeAll();
					}
				}
					snpSjItemGrid.store.removeAll();
				}
				document.getElementById('snpSj_snpCross').value=rec.get('id');
				document.getElementById('snpSj_snpCross_name').value=rec.get('name');
				var win = Ext.getCmp('ProducerTaskFun');
				if(win){win.close();}
				var id=rec.get('id');
				ajax("post", "/experiment/snpjc/cross/snpCross/setSnpCrossResult.action", {
					code : id,
					}, function(data) {
						if (data.success) {
							var ob = snpSjItemGrid.getStore().recordType;
							snpSjItemGrid.stopEditing();
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("code",obj.code);
								p.set("sampleCode",obj.sampleCode);
								p.set("chipNum",obj.chipNum);
								p.set("sampleType",obj.sampleType);
								p.set("productId",obj.productId);
								p.set("productName",obj.productName);
								p.set("note",obj.note);
								snpSjItemGrid.getStore().add(p);							
							});
							snpSjItemGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
			}
		}
	}
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});*/
	//我的js
	function ProducerTaskFun(){
		var code=$("#snpSj_snpCrossId").val();
		var options = {};
		options.width = 600;
		options.height = 600;
		var url="/experiment/snpjc/cross/snpCross/showSnpCrossList1.action";
		loadDialogPage(null, "选择样本处理", url, {
			 "确定": function() {
				 var selected=crossReasonGrid.getSelectionModel().getSelections();
				 var srId="";
				 var srName="";
				 var idStr="";
				 var idStr1="";
				 if(selected.length>0){
					 $.each(selected, function(i, obj) {
						 srId += obj.get("id")+",";
						 srName += obj.get("name")+",";
						 idStr += "'"+obj.get("id")+"',";
					 });
					 idStr1=idStr.substring(0,idStr.length-1); 
					 if(code==""||code==null){
						 $("#snpSj_snpCrossId").val(srId);
						 $("#snpSj_snpCrossName").val(srName);
						 ajax("post","/experiment/snpjc/cross/snpCross/setSnpCrossResult.action",
									{code:idStr1},function(data){
									if(data.success){
										var ob = snpSjItemGrid.getStore().recordType;
										snpSjItemGrid.stopEditing();
										$.each(data.data,function(i,obj){
											if(obj.result=="1"){
												var p = new ob({});
												p.isNew = true;
												p.set("code",obj.code);
												p.set("sampleCode",obj.sampleCode);
												p.set("chipNum",obj.chipNum);
												p.set("sampleType",obj.sampleType);
												p.set("productId",obj.productId);
												p.set("productName",obj.productName);
												p.set("note",obj.note);
												snpSjItemGrid.getStore().add(p);
												snpSjItemGrid.startEditing(0, 0);
											}
										});
									}else {
										message("获取明细数据时发生错误！");
									}
								},null);
						 options.close();
					 }else{
						var ob1 = snpSjItemGrid.store;
						if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id");
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/snpjc/sj/snpSj/delSnpSjItemOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null);
								}else{								
									snpSjItemGrid.store.removeAll();
								}
							}
							snpSjItemGrid.store.removeAll();
						}
						 $("#snpSj_snpCrossId").val(srId);
						 $("#snpSj_snpCrossName").val(srName);
						
						 ajax("post","/experiment/bobs/sample/bobsSample/setResultToCross.action",
									{code:idStr1},function(data){
									if(data.success){
										var ob = snpSjItemGrid.getStore().recordType;
										snpSjItemGrid.stopEditing();
										$.each(data.data,function(i,obj){
											if(obj.result=="1"){
												var p = new ob({});
												p.isNew = true;
												p.set("code",obj.code);
												p.set("sampleCode",obj.sampleCode);
												p.set("chipNum",obj.chipNum);
												p.set("sampleType",obj.sampleType);
												p.set("productId",obj.productId);
												p.set("productName",obj.productName);
												p.set("note",obj.note);
												snpSjItemGrid.getStore().add(p);
												
											}
											snpSjItemGrid.startEditing(0, 0);
										});
									}else {
										message("获取明细数据时发生错误！");
									}
								},null);
						 options.close();
					 }
				 }else{
					 options.close();
				 }
			}
		}, true, options);
	}

	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	
	
	//123456
	function ckcrk(){
		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = snpSjItemGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message("有样本未提交，不能初始化！");
						return;
					}
				}

				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : "SnpSj"
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");

								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message("回滚失败！");
							}
						}, null);
					
				}
			}
		});
	}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '保存'
			});
		item.on('click', ckcrk2);
		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);
		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "SnpSj",id : $("#snpSj_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
				window.location.reload();
			} else {
				message("办理回滚失败！");
			}
		}, null);
	}
