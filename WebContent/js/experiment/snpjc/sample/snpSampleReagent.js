var snpSampleReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'xishu',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'count',
		type:"string"
	});
	   fields.push({
		name:'oneNum',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'snpSample-id',
		type:"string"
	});
	    fields.push({
		name:'snpSample-name',
		type:"string"
	});
	   fields.push({
		name:'itemId',
		type:"string"
	});
	   fields.push({
		name:'tReagent',
		type:"string"
	});
	   fields.push({
		name:'saveCondition',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.reagentNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xishu',
		hidden : false,
		header:biolims.common.floatationCoefficient,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:biolims.common.batch,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'count',
		hidden : true,
		header:'数量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'oneNum',
		hidden : false,
		header:biolims.common.singleDose,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.dose,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'saveCondition',
		hidden : false,
		header:biolims.common.saveCondition,
		width:20*6
	});
//	var storesaveCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '常温' ], [ '1', '4℃' ], [ '1', '-20℃' ] ]
//	});
//	var saveCob = new Ext.form.ComboBox({
//		store : storesaveCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'saveCondition',
//		hidden : false,
//		header:'储存条件<font size="4" color="red">*</font>',
//		width:20*6,
//		editor : saveCob,
//		renderer : Ext.util.Format.comboRenderer(saveCob)
//	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : true,
		header:biolims.common.ifPassTest,
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'关联步骤id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tReagent',
		hidden : true,
		header:'模板原辅料id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/sample/snpSample/showSnpSampleReagentListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight-400;
	opts.tbar = [];
	if($("#snpSample_stateName").val()!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snpjc/sample/snpSample/delSnpSampleReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.exportList,
		handler : exportexcelc1
	});
	}
	snpSampleReagentGrid=gridEditTable("snpSampleReagentdiv",cols,loadParam,opts);
	$("#snpSampleReagentdiv").data("snpSampleReagentGrid", snpSampleReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function exportexcelc1() {
	snpSampleReagentGrid.title = biolims.common.exportList;
	var vExportContent = snpSampleReagentGrid.getExcelXml();
	var x = document.getElementById('gridhtmc1');
	x.value = vExportContent;
	document.excelfrmc1.submit();
}