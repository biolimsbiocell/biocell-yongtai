var snpSampleItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'checkCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'xsyVolume',
		type:"string"
	});
	   fields.push({
		name:'addVolume',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'snpSample-id',
		type:"string"
	});
	    fields.push({
		name:'snpSample-name',
		type:"string"
	});
	   fields.push({
		name:'productNum',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	   fields.push({
		name:'productNum',
		type:"string"
	});
	   fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
	   fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
   //新加的字段患者名字
   fields.push({
		name:'patientName',
		type:"string"
	});
   fields.push({
		name:'od280',
		type:"string"
	});
   fields.push({
		name:'od260',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.expCode,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'checkCode',
		hidden : false,
		header:biolims.common.checkedSampleCode,
		width:25*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6,
		sortable:true
	});
	//新加的字段
	cm.push({
		dataIndex:'od280',
		hidden : false,
		header:'OD260/280',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'od260',
		hidden : false,
		header:'OD260/230',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.nucleicConcentration,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.sampleVolume,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'xsyVolume',
		hidden : false,
		header:biolims.common.xsyVolume,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : false,
		header:biolims.common.xhbzjyVolume,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});

//	cm.push({
//		dataIndex:'dicSampleType-id',
//		hidden : true,
//		header:'中间产物编号',
//		width:20*6,
//		sortable:true
//	});
//	var testDicSampleType2 =new Ext.form.TextField({
//        allowBlank: false
//	});
//	testDicSampleType2.on('focus', function() {
//		loadTestDicSampleType2();
//	});
//	cm.push({
//		dataIndex:'dicSampleType-name',
//		header:'中间产物类型<font color="red" size="4px">*</font>',
//		width:15*10,
//		sortable:true,
//		hidden:false,
//		editor : testDicSampleType2
//	});
//	cm.push({
//		dataIndex:'productNum',
//		hidden : false,
//		header:'中间产物数量<font color="red" size="4px">*</font>',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'步骤编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'snpSample-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/sample/snpSample/showSnpSampleItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.snpSampleHandleDetail;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	if($("#snpSample_stateName").val()!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snpjc/sample/snpSample/delSnpSampleItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				snpSampleItemGrid.getStore().commitChanges();
				snpSampleItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//		text : "批量中间产物类型",
//		handler : function() {
//			var options = {};
//			options.width = document.body.clientWidth-800;
//			options.height = document.body.clientHeight-40;
//			loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
//				"确定" : function() {
//					var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
//					var selectRecord = operGrid.getSelectionModel().getSelections();
//					var records = snpSampleItemGrid.getSelectRecord();
//					if (selectRecord.length > 0) {
//						$.each(selectRecord, function(i, obj) {
//							$.each(records, function(a, b) {
//								b.set("dicSampleType-id", obj.get("id"));
//								b.set("dicSampleType-name", obj.get("name"));
//							});
//						});
//					}else{
//						message("请选择您要选择的数据");
//						return;
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	
//	opts.tbar.push({
//		text : "批量产物数量",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_productNum_div"), "批量产物数量", null, {
//				"确定" : function() {
//					var records = snpSampleItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var productNum = $("#productNum").val();
//						snpSampleItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("productNum", productNum);
//						});
//						snpSampleItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	
	 opts.tbar.push({
			text : biolims.common.batchCopyImport,
			handler : function() {
				$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：样本号、核酸浓度、样本体积、稀释液体积、消化步骤加样体积");
				$("#many_bat_text2").val("");
				$("#many_bat_text2").attr("style", "width:465px;height: 339px");
				var options = {};
				options.width = 494;
				options.height = 508;
				loadDialogPage($("#many_bat_div2"), "批量导入", null, {
					"确定" : function() {
						var positions = $("#many_bat_text2").val();
						if (!positions) {
							message("请填写信息！");
							return;
						}
						var posiObj = {};
						var posiObj1 = {};
						var posiObj2 = {};
						var posiObj3 = {};
						var array = formatData(positions.split("\n"));
						$.each(array, function(i, obj) {
							var tem = obj.split("\t");
							posiObj[tem[0]] = tem[1];
							posiObj1[tem[0]] = tem[2];
							posiObj2[tem[0]] = tem[3];
							posiObj3[tem[0]] = tem[4];
						});
						var records = snpSampleItemGrid.getAllRecord();
						snpSampleItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							if (posiObj[obj.get("code")]) {
								obj.set("concentration", posiObj[obj.get("code")]);
							}
							if (posiObj1[obj.get("code")]) {
								obj.set("volume", posiObj1[obj.get("code")]);
							}
							if (posiObj2[obj.get("code")]) {
								obj.set("xsyVolume", posiObj2[obj.get("code")]);
							}
							if (posiObj3[obj.get("code")]) {
								obj.set("addVolume", posiObj3[obj.get("code")]);
							}
									
						});
						snpSampleItemGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}
	});	
 //批量上传CSV
 opts.tbar.push({
	text : biolims.common.batchUploadCSV,
	handler : function() {
		var options = {};
		options.width = 350;
		options.height = 200;
		loadDialogPage($("#bat_uploadcsvItem_div"),"批量上传",null,{
			"确定" : function() {
				goInExcelcsvItem();
				$(this).dialog("close");
			}
		}, true, options);
	}
});
 function goInExcelcsvItem(){
 var file = document.getElementById("file-uploadcsvItem").files[0];  
	var n = 0;
	//var ob = snpSampleItemGrid.getStore().recordType;
	var ob1 = snpSampleItemGrid.store;
	var reader = new FileReader();  
	reader.readAsText(file,'GB2312');  
	reader.onload=function(f){  
		var csv_data = $.simple_csv(this.result);
		for(var k=0;k<ob1.getCount();k++){
			$(csv_data).each(function() {
        	if(n>0){
        		if(this[0]==ob1.getAt(k).get("code")){
        			ob1.getAt(k).set("patientName",this[1]);
        			ob1.getAt(k).set("od280",this[7]);
        			ob1.getAt(k).set("od260",this[8]);
        			ob1.getAt(k).set("concentration",this[9]);
        			ob1.getAt(k).set("volume",this[11]);
        			ob1.getAt(k).set("xsyVolume",this[12]);
        		}
        	}
                n = n +1;
            });
		}
	};
 }
	 
	 
	 
//上传CSV文件
opts.tbar.push({
	text : biolims.common.batchUploadCSV,
	handler : function() {
		var options = {};
		options.width = 350;
		options.height = 200;
		loadDialogPage($("#bat_uploadcsvitem_div"),"批量上传",null,{
			"确定" : function() {
				goInExcel3();
				$(this).dialog("close");
			}
		}, true, options);
	}
});
function goInExcel3(){
	var file = document.getElementById("file-uploadcsv3").files[0];  
	var n = 0;
	var ob = snpSampleItemGrid.getStore().recordType;
	var ob1 = snpSampleItemGrid.store;
	var reader = new FileReader();  
	reader.readAsText(file,'GB2312');  
	reader.onload=function(f){  
		var csv_data = $.simple_csv(this.result);
		for(var k=0;k<ob1.getCount();k++){
			$(csv_data).each(function() {
            	if(n>0){
            		if(this[0]==ob1.getAt(k).get("Code")){
            						
            			ob1.getAt(k).set("od280",this[7]);
            			ob1.getAt(k).set("od260",this[8]);
            			ob1.getAt(k).set("contraction",this[9]);
            			
            		}
            	}
                n = n +1;
            });
		}
    	};
	}

//	opts.tbar.push({
//	text : "批量上传（csv文件）",
//	handler : function() {
//		var options = {};
//		options.width = 350;
//		options.height = 200;
//		loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//			"确定":function(){
//				goInExcelcsv();
//				$(this).dialog("close");
//			}
//		},true,options);
//	}
//	});
//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = snpSampleItemGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//	            	if(n>0){
//	            		if(this[0]){
//	            			var p = new ob({});
//	            			p.isNew = true;				
//	            			p.set("code",this[0]);
//	            			p.set("sampleCode",this[1]);
//	            			/*p.set("poolingCode",this[2]);
//	            			p.set("sampleAmount",this[3]);
//	            			p.set("outPut",this[4]);
//	            			p.set("cluster",this[5]);*/
//	            			p.set("concentration",this[6]);
//	            			p.set("volume",this[7]);
//	            			p.set("xysVolume",this[8]);
//	            			p.set("addVolume",this[9]);
//	            			snpSampleItemGrid.getStore().insert(0, p);
//	            		}
//	            	}
//	                 n = n +1;
//	            });
//		};
//	}
	 opts.tbar.push({
		text : biolims.common.exportList,
		handler : exportexcel
	});
	 opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	}
	snpSampleItemGrid=gridEditTable("snpSampleItemdiv",cols,loadParam,opts);
	$("#snpSampleItemdiv").data("snpSampleItemGrid", snpSampleItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(snpSampleItemGrid);
	var id=$("#snpSample_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/snpjc/sample/snpSample/saveSnpSampleItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					snpSampleItemGrid.getStore().commitChanges();
					snpSampleItemGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

function exportexcel() {
	snpSampleItemGrid.title = biolims.common.exportList;
	var vExportContent = snpSampleItemGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
var loadDicSampleType2;
//查询样本类型
function loadTestDicSampleType2(){
	var options = {};
	options.width = document.body.clientWidth-800;
	options.height = document.body.clientHeight-40;
	loadDicSampleType2=loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action?a=2", {
		"确定" : function() {
			var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = snpSampleItemGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					$.each(records, function(a, b) {
						b.set("dicSampleType-id", obj.get("id"));
						b.set("dicSampleType-name", obj.get("name"));
					});
				});
			}else{
				message("请选择您要选择的数据");
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}
function setDicSampleType2(){
	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = snpSampleItemGrid.getSelectRecord();
	
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("dicSampleType-id", b.get("id"));
				obj.set("dicSampleType-name", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadDicSampleType2.dialog("close");

}