$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#snpSample_state").val();
	if(id=="3"){
		load("/experiment/snpjc/sample/snpSample/showSnpSampleTempList.action", null, "#snpSampleTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#snpSampleTemppage").remove();
	}
	setTimeout(function() {
		var getGrid=snpSampleTemplateGrid.store;
		if(getGrid.getCount()==0){
			//alert(getGrid.getCount());
			loadTemplate($("#snpSample_template").val());
		}
	}, 2000);
});
function add() {
	window.location = window.ctx + "/experiment/snpjc/sample/snpSample/editSnpSample.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/snpjc/sample/snpSample/showSnpSampleList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("SnpSample", {
					userId : userId,
					userName : userName,
					formId : $("#snpSample_id").val(),
					title : $("#snpSample_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	var codeList = new Array();
	var codeList1 = new Array();
	var selRecord1 = snpSampleItemGrid.store;
	var flag=true;
	var flag1=true;
	if (snpSamplePdhResultGrid.getAllRecord().length > 0) {
		var selRecord = snpSamplePdhResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){

			codeList.push(selRecord.getAt(j).get("sampleCode"));
			if(selRecord.getAt(j).get("result")==""){
				message("结果不能为空！");
				return;
			}
			if(selRecord.getAt(j).get("nextFlowId")==""){
				message("下一步不能为空！");
				return;
			}
		}
		if(snpSamplePdhResultGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
		completeTask($("#snpSample_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});






function save() {
if(checkSubmit()==true){
	    var snpSampleItemDivData = $("#snpSampleItemdiv").data("snpSampleItemGrid");
		document.getElementById('snpSampleItemJson').value = commonGetModifyRecords(snpSampleItemDivData);
	    var snpSampleTemplateDivData = $("#snpSampleTemplatediv").data("snpSampleTemplateGrid");
		document.getElementById('snpSampleTemplateJson').value = commonGetModifyRecords(snpSampleTemplateDivData);
	    var snpSampleReagentDivData = $("#snpSampleReagentdiv").data("snpSampleReagentGrid");
		document.getElementById('snpSampleReagentJson').value = commonGetModifyRecords(snpSampleReagentDivData);
	    var snpSampleCosDivData = $("#snpSampleCosdiv").data("snpSampleCosGrid");
		document.getElementById('snpSampleCosJson').value = commonGetModifyRecords(snpSampleCosDivData);
	    var snpSamplePcrResultDivData = $("#snpSamplePcrResultdiv").data("snpSamplePcrResultGrid");
		document.getElementById('snpSamplePcrResultJson').value = commonGetModifyRecords(snpSamplePcrResultDivData);
	    var snpSampelChResultDivData = $("#snpSampelChResultdiv").data("snpSampelChResultGrid");
		document.getElementById('snpSampelChResultJson').value = commonGetModifyRecords(snpSampelChResultDivData);
	    var snpSamplePdhResultDivData = $("#snpSamplePdhResultdiv").data("snpSamplePdhResultGrid");
		document.getElementById('snpSamplePdhResultJson').value = commonGetModifyRecords(snpSamplePdhResultDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/snpjc/sample/snpSample/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/snpjc/sample/snpSample/copySnpSample.action?id=' + $("#snpSample_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#snpSample_id").val() + "&tableId=snpSample");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#snpSample_id").val()){
		commonChangeState("formId=" + $("#snpSample_id").val() + "&tableId=SnpSample");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#snpSample_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#snpSample_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#snpSample_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.snpSampleHandle,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/snpjc/sample/snpSample/showSnpSampleItemList.action", {
				id : $("#snpSample_id").val()
			}, "#snpSampleItempage");
load("/experiment/snpjc/sample/snpSample/showSnpSampleTemplateList.action", {
				id : $("#snpSample_id").val()
			}, "#snpSampleTemplatepage");
load("/experiment/snpjc/sample/snpSample/showSnpSampleReagentList.action", {
				id : $("#snpSample_id").val()
			}, "#snpSampleReagentpage");
load("/experiment/snpjc/sample/snpSample/showSnpSampleCosList.action", {
				id : $("#snpSample_id").val()
			}, "#snpSampleCospage");
load("/experiment/snpjc/sample/snpSample/showSnpSamplePcrResultList.action", {
				id : $("#snpSample_id").val()
			}, "#snpSamplePcrResultpage");
load("/experiment/snpjc/sample/snpSample/showSnpSampelChResultList.action", {
				id : $("#snpSample_id").val()
			}, "#snpSampelChResultpage");
load("/experiment/snpjc/sample/snpSample/showSnpSamplePdhResultList.action", {
				id : $("#snpSample_id").val()
			}, "#snpSamplePdhResultpage");
//load("/experiment/snpjc/sample/snpSample/showSnpSampleTempList.action", {
//				id : $("#snpSample_id").val()
//			}, "#snpSampleTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
//调用模板
function TemplateFun(){
		var type="doSnpSample";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
	
	function setTemplateFun(rec){
		//把实验模板中的中间产物类型和数量带入
//		var itemGrid=snpSampleItemGrid.store;
//		if(itemGrid.getCount()>0){
//			for(var i=0;i<itemGrid.getCount();i++){
//				itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
//				itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
//				itemGrid.getAt(i).set("productNum",rec.get('productNum'));
//			}
//		}
		document.getElementById('snpSample_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('snpSample_acceptUser_name').value=rec.get('acceptUser-name');
		var code=$("#snpSample_template").val();
		if(code==""){
		document.getElementById('snpSample_template').value=rec.get('id');
		document.getElementById('snpSample_template_name').value=rec.get('name');
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
		var id=rec.get('id');
		ajax("post", "/system/template/template/setTemplateItem.action", {
			code : id,
			}, function(data) {
				if (data.success) {
					var ob = snpSampleTemplateGrid.getStore().recordType;
					snpSampleTemplateGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tItem",obj.id);
						p.set("code",obj.code);
						p.set("stepName",obj.name);
						
						p.set("note",obj.note);
						snpSampleTemplateGrid.getStore().add(p);							
					});
					
					snpSampleTemplateGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateReagent.action", {
			code : id,
			}, function(data) {
				if (data.success) {

					var ob = snpSampleReagentGrid.getStore().recordType;
					snpSampleReagentGrid.stopEditing();
					
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tReagent",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("batch",obj.batch);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("saveCondition",obj.saveCondition);
						p.set("oneNum",obj.num);
						p.set("note",obj.note);
						p.set("sn",obj.sn);
						p.set("xishu",obj.xishu);
						snpSampleReagentGrid.getStore().add(p);							
					});
					
					snpSampleReagentGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateCos.action", {
			code : id,
			}, function(data) {
				if (data.success) {

					var ob = snpSampleCosGrid.getStore().recordType;
					snpSampleCosGrid.stopEditing();
					
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tCos",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("time",obj.time);
						p.set("note",obj.note);
						snpSampleCosGrid.getStore().add(p);							
					});			
					snpSampleCosGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null);
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
						var ob1 = snpSampleTemplateGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id"); 
								
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/snpjc/sample/snpSample/delOne.action", {
										ids : oldv,model:"1"
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null);
								}else{								
									snpSampleTemplateGrid.store.removeAll();
								}
							}
							snpSampleTemplateGrid.store.removeAll();
		 				}

						var ob2 = snpSampleReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");

								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/snpjc/sample/snpSample/delOne.action", {
									ids : oldv,model:"2"
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
								}else{
									snpSampleReagentGrid.store.removeAll();
								}
							}
							snpSampleReagentGrid.store.removeAll();
		 				}
						//=========================================
						var ob3 = snpSampleCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");
								
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/snpjc/sample/snpSample/delOne.action", {
										ids : oldv,model:"3"
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null); 
								}else{
									snpSampleCosGrid.store.removeAll();
								}
							}
							snpSampleCosGrid.store.removeAll();
		 				}
						document.getElementById('snpSample_template').value=rec.get('id');
						document.getElementById('snpSample_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = snpSampleTemplateGrid.getStore().recordType;
									snpSampleTemplateGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("stepName",obj.name);
										
										p.set("note",obj.note);
										snpSampleTemplateGrid.getStore().add(p);							
									});
									
									snpSampleTemplateGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = snpSampleReagentGrid.getStore().recordType;
									snpSampleReagentGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tReagent",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										p.set("saveCondition",obj.saveCondition);
										p.set("oneNum",obj.num);
										p.set("note",obj.note);
										p.set("sn",obj.sn);
										p.set("xishu",obj.xishu);
										snpSampleReagentGrid.getStore().add(p);							
									});
									
									snpSampleReagentGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = snpSampleCosGrid.getStore().recordType;
									snpSampleCosGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tCos",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("temperature",obj.temperature);
										p.set("speed",obj.speed);
										p.set("time",obj.time);
										p.set("note",obj.note);
										snpSampleCosGrid.getStore().add(p);							
									});			
									snpSampleCosGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						}
					}
}
	
	function loadPic(){
		var model="snpSample";
		window.open(window.ctx+"/experiment/dna/experimentDnaGet/loadPic.action?id="+$("#snpSample_id").val()+"&model="+model,'','height=700,width=1200,scrollbars=yes,resizable=yes');
	}
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	function ckcrk(){
		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = snpSamplePdhResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message("有样本未提交，不能初始化！");
						return;
					}
				}

				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");

								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message("回滚失败！");
							}
						}, null);
					}
					
				}
			}
		});
	}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '保存'
			});
		item.on('click', ckcrk2);
		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);
		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "SnpSample",id : $("#snpSample_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
			} else {
				message("办理回滚失败！");
			}
		}, null);
	}
	
	
//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = snpSampleTemplateGrid.getStore().recordType;
				snpSampleTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("stepName",obj.name);
					
					p.set("note",obj.note);
					snpSampleTemplateGrid.getStore().add(p);							
				});
				
				snpSampleTemplateGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = snpSampleReagentGrid.getStore().recordType;
				snpSampleReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("oneNum",obj.num);
					p.set("saveCondition",obj.saveCondition);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					p.set("xishu",obj.xishu);
					snpSampleReagentGrid.getStore().add(p);							
				});
				
				snpSampleReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = snpSampleCosGrid.getStore().recordType;
				snpSampleCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					snpSampleCosGrid.getStore().add(p);							
				});			
				snpSampleCosGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
}