var snpSampelChResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'checkCode',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'od280',
		type:"string"
	});
	   fields.push({
		name:'od230',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'snpSample-id',
		type:"string"
	});
	    fields.push({
		name:'snpSample-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	   //新加的字段患者名字
	   fields.push({
			name:'patientName',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'左侧表ID',
		width:30*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'checkCode',
		hidden : false,
		header:biolims.common.checkedSampleCode,
		width:25*6,
		sortable:true
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.expCode,
		width:20*6,
		sortable:true
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		sortable:true,
		width:20*6
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		sortable:true,
		width:25*6
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'od280',
		hidden : false,
		header:'260/280',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'od230',
		hidden : false,
		header:'260/230',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.pcrProductsPotency,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeGoodCob1 = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob1 = new Ext.form.ComboBox({
		store : storeGoodCob1,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.isQualified+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : goodCob1,
		renderer : Ext.util.Format.comboRenderer(goodCob1)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob1 =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob1.on('focus', function() {
		loadTestNextFlowCob1();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		sortable:true,
		hidden:true,
		editor : nextFlowCob1
	});
	var storesubmitCob1 = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob1 = new Ext.form.ComboBox({
		store : storesubmitCob1,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : true,
		header:'是否提交',
		width:20*6,
		editor : submitCob1,
		renderer : Ext.util.Format.comboRenderer(submitCob1)
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.reason,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/sample/snpSample/showSnpSampelChResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.purificationResult;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	if($("#snpSample_stateName").val()!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snpjc/sample/snpSample/delSnpSampelChResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				snpSampelChResultGrid.getStore().commitChanges();
				snpSampelChResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div1"), "批量结果", null, {
				"确定" : function() {
					var records = snpSampelChResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result1").val();
						snpSampelChResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						snpSampelChResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "批量下一步",
//		handler : function() {
//			var records = snpSampelChResultGrid.getSelectRecord();
//			if(records.length>0){
//				if(records.length>2){
//					var productId = new Array();
//					$.each(records, function(j, k) {
//						productId[j]=k.get("productId");
//					});
//					for(var i=0;i<records.length;i++){
//						if(i!=0&&productId[i]!=productId[i-1]){
//							message("检测项目不同！");
//							return;
//						}
//					}
//					loadTestNextFlowCob1();
//				}else{
//					loadTestNextFlowCob1();
//				}
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div1"), "批量提交", null, {
//				"确定" : function() {
//					var records = snpSampelChResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						snpSampelChResultGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						snpSampelChResultGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	//上传cvs文件
	
//	 opts.tbar.push({
//			text : "批量粘贴导入",
//			handler : function() {
//				alert(2222);
//				$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：样本编号、260/280、260/230、纯化后PCR产物浓度");
//				$("#many_bat_text").val("");
//				$("#many_bat_text").attr("style", "width:465px;height: 339px");
//				var options = {};
//				options.width = 494;
//				options.height = 508;
//				loadDialogPage($("#many_bat_div"), "批量导入", null, {
//					"确定" : function() {
//						var positions = $("#many_bat_text").val();
//						if (!positions) {
//							message("请填写信息！");
//							return;
//						}
//						var posiObj = {};
//						var posiObj1 = {};
//						var posiObj2 = {};
//						var posiObj3 = {};
//						var array = formatData(positions.split("\n"));
//						$.each(array, function(i, obj) {
//							var tem = obj.split("\t");
//							posiObj[tem[0]] = tem[1];
//							posiObj1[tem[0]] = tem[2];
//							posiObj2[tem[0]] = tem[3];
//							posiObj3[tem[0]] = tem[4];
//						});
//						var records = snpSampelChResultGrid.getAllRecord();
//						snpSampelChResultGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							if (posiObj[obj.get("code")]) {
//								obj.set("od280", posiObj[obj.get("code")]);
//							}
//							if (posiObj1[obj.get("code")]) {
//								obj.set("od230", posiObj1[obj.get("code")]);
//							}
//							if (posiObj2[obj.get("code")]) {
//								obj.set("concentration", posiObj2[obj.get("code")]);
//							}
//						});
//						snpSampelChResultGrid.startEditing(0, 0);
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}
//		});
	
	 //批量上传CSV
 opts.tbar.push({
		text : biolims.common.batchUploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsvsresult_div"),"批量上传",null,{
				"确定" : function() {
					goInExcelcsvResult();
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	 
	 opts.tbar.push({
			text : biolims.common.exportList,
			handler : exportexcelc
		});
	opts.tbar.push({
		text : biolims.common.scfragmentationResults,
		handler : getPdhResult
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save3
	});
	}
	snpSampelChResultGrid=gridEditTable("snpSampelChResultdiv",cols,loadParam,opts);
	$("#snpSampelChResultdiv").data("snpSampelChResultGrid", snpSampelChResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//批量上传CSV的方法
function goInExcelcsvResult(){
	 var file = document.getElementById("file-uploadcsvresult").files[0];  
		var n = 0;
		var ob = snpSampelChResultGrid.getStore().recordType;
		var ob1 = snpSampelChResultGrid.store;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			for(var k=0;k<ob1.getCount();k++){
				$(csv_data).each(function() {
               	if(n>0){
               		if(this[0]==ob1.getAt(k).get("code")){
               			
               			ob1.getAt(k).set("patientName",this[1]);
               			ob1.getAt(k).set("od280",this[7]);
               			ob1.getAt(k).set("od230",this[8]);
               			ob1.getAt(k).set("concentration",this[9]);
               		}
               	}
                   n = n +1;
               });
			}   };
			}

function exportexcelc() {
	snpSampelChResultGrid.title = '导出列表';
	var vExportContent = snpSampelChResultGrid.getExcelXml();
	var x = document.getElementById('gridhtmc');
	x.value = vExportContent;
	document.excelfrmc.submit();
}
//保存
function save3(){
	var id=$("#snpSample_id").val();
	var itemJson = commonGetModifyRecords(snpSampelChResultGrid);
	if(itemJson.length>0){
		ajax("post", "/experiment/snpjc/sample/snpSample/saveSnpSampleCh.action", {
			itemDataJson : itemJson,id : id
		}, function(data) {
			if (data.success) {
				snpSampelChResultGrid.getStore().commitChanges();
				snpSampelChResultGrid.getStore().reload();
				message("保存成功！");
			} else {
				message("保存失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob1(){
	var records1 = snpSampelChResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=UfTask&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = snpSampelChResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = snpSampelChResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}
	

function getPdhResult(){
	//获取选中的PCR结果数据
	var getPcr = snpSampelChResultGrid.getSelectionModel().getSelections();
	var getCh = snpSamplePdhResultGrid.store;
	if(getPcr.length>0){
		var flag=true;
		$.each(getPcr, function(i, obj) {
			for(var i=0;i<getCh.getCount();i++){
				if(getCh.getAt(i).get("code")==obj.get("code")){
					flag=false;
					message("有重复数据生成！");
					return;
				}
			}
			var r=obj.get("result");
			if(r!="" && r!=null && r!=undefined){
//				if(r=="0"){
//					flag=false;
//					message("纯化结果不合格！");
//				}else{
//					flag=true;
//				}
			}else{
				flag=false;
				message("请填写纯化结果！");
			}
		});
		if(flag){
			$.each(getPcr, function(i, obj) {
				var ob = snpSamplePdhResultGrid.getStore().recordType;
				snpSamplePdhResultGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("tempId",obj.get("tempId"));
				p.set("sampleCode",obj.get("sampleCode"));
				p.set("code",obj.get("code"));
				p.set("orderNumber",obj.get("orderNumber"));
				p.set("checkCode",obj.get("checkCode"));
				p.set("productId",obj.get("productId"));
				p.set("productName",obj.get("productName"));
				p.set("sampleType",obj.get("sampleType"));
				p.set("result",obj.get("result"));
				ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
					model : "SnpSample",productId:obj.get("productId")
				}, function(data) {
					p.set("nextFlowId",data.dnextId);
					p.set("nextFlow",data.dnextName);
				}, null);
				
				snpSamplePdhResultGrid.getStore().add(p);
				snpSamplePdhResultGrid.startEditing(0,0);
			});
			message("生成结果成功！");
		}
	}else{
		message("请选择生成纯化结果的PCR样本！");
	}
}