var snpSamplePdhResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'checkCode',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'snpSample-id',
		type:"string"
	});
	    fields.push({
		name:'snpSample-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'左侧表ID',
		width:30*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.expCode,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'checkCode',
		hidden : false,
		header:biolims.common.checkedSampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	var storeGoodCob2 = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob2 = new Ext.form.ComboBox({
		store : storeGoodCob2,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.isQualified+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : goodCob2,
		renderer : Ext.util.Format.comboRenderer(goodCob2)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob2 =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob2.on('focus', function() {
		loadTestNextFlowCob2();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow+'<font color="red" size="4">*</font>',
		width:15*10,
		sortable:true,
		editor : nextFlowCob2
	});
	var storesubmitCob1 = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob1 = new Ext.form.ComboBox({
		store : storesubmitCob1,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.toSubmit+'<font color="red" size="4">*</font>',
		width:20*6,
//		editor : submitCob1,
		renderer : Ext.util.Format.comboRenderer(submitCob1)
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.reason,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/sample/snpSample/showSnpSamplePdhResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.fragmentationResults;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	if($("#snpSample_stateName").val()!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snpjc/sample/snpSample/delSnpSamplePdhResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				snpSamplePdhResultGrid.getStore().commitChanges();
				snpSamplePdhResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.uploadAttachment,
		handler : function() {
			var id=$("#snpSample_id").val();
			if(id!="NEW"){
				var win = Ext.getCmp('doc');
				if (win) {win.close();}
				var doc= new Ext.Window({
				id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
				plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
				collapsible: true,maximizable: true,
				items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
				html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+id+"&modelType=snpSamplePdhResult' frameborder='0' width='100%' height='100%' ></iframe>"}),
				buttons: [
				{ text: '关闭',
				 handler: function(){
				 doc.close(); }  }]  }); 
				 doc.show();
			}else{
				message("请先保存记录。");
			}
		}
});
	opts.tbar.push({
		text :biolims.report.checkFile,
		handler : loadPic
	});
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div2"), "批量结果", null, {
				"确定" : function() {
					var records = snpSamplePdhResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result2").val();
						snpSamplePdhResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						snpSamplePdhResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : function() {
			var records = snpSamplePdhResultGrid.getSelectRecord();
			if(records.length>0){
					loadTestNextFlowCob2();
				
			}else{
				message("请选择数据!");
			}
		}
	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div2"), "批量提交", null, {
//				"确定" : function() {
//					var records = snpSamplePdhResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						snpSamplePdhResultGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						snpSamplePdhResultGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save2
	});
	}
	snpSamplePdhResultGrid=gridEditTable("snpSamplePdhResultdiv",cols,loadParam,opts);
	$("#snpSamplePdhResultdiv").data("snpSamplePdhResultGrid", snpSamplePdhResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//保存
function save2(){
	var id=$("#snpSample_id").val();
	var itemJson = commonGetModifyRecords(snpSamplePdhResultGrid);
	if(itemJson.length>0){
		ajax("post", "/experiment/snpjc/sample/snpSample/saveSnpSamplePdh.action", {
			itemDataJson : itemJson,id : id
		}, function(data) {
			if (data.success) {
				snpSamplePdhResultGrid.getStore().commitChanges();
				snpSamplePdhResultGrid.getStore().reload();
				message("保存成功！");
			} else {
				message("保存失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}
function loadPic(){
	var model="snpSamplePdhResult";
	var id=$("#snpSample_id").val();
	var ids="'"+id+"'";
//	window.open(window.ctx+"/experiment/snpjc/sample/loadPic.action?id="+$("#snpSample_id").val()+"&model="+model,'','height=700,width=1200,scrollbars=yes,resizable=yes');
	window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="+ids+"&model="+model,'','height=700,width=1200,scrollbars=yes,resizable=yes');
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob2(){
	var records1 = snpSamplePdhResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=SnpSample&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = snpSamplePdhResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = snpSamplePdhResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}
//提交样本
function submitSample(){
	var id=$("#snpSample_id").val();  
	if(snpSamplePdhResultGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	var record = snpSamplePdhResultGrid.getSelectionModel().getSelections();
	var flg=false;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(!record[i].get("submit")){
				flg=true;
			}
			if(record[i].get("result")==""){
				message("结果不能为空！");
				return;
			}
			if(record[i].get("nextFlowId")==""){
				message("下一步不能为空！");
				return;
			}
		}
	}else{
		var grid=snpSamplePdhResultGrid.store;
		for(var i=0;i<grid.getCount();i++){
			if(grid.getAt(i).get("submit")==""){
				flg=true;
			}
			if(grid.getAt(i).get("result")==""){
				message("结果不能为空！");
				return;
			}
			if(grid.getAt(i).get("nextFlowId")==""){
				message("下一步不能为空！");
				return;
			}
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		var records = [];
		
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		ajax("post", "/experiment/snpjc/sample/snpSample/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				snpSamplePdhResultGrid.getStore().commitChanges();
				snpSamplePdhResultGrid.getStore().reload();
				message("提交成功！");
			} else {
				loadMarsk.hide();
				message("提交失败！");
			}
		}, null);
	}else{
		message("没有需要提交的样本！");
	}
}
	

	
