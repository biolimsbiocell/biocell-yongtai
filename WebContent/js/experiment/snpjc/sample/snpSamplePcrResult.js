var snpSamplePcrResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'checkCode',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'dyResult',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'snpSample-id',
		type:"string"
	});
	    fields.push({
		name:'snpSample-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'左侧表ID',
		width:30*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.expCode,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'checkCode',
		hidden : false,
		header:biolims.common.checkedSampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6,
		sortable:true
	});
	var storedyResultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var dyResultCob = new Ext.form.ComboBox({
		store : storedyResultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'dyResult',
		hidden : false,
		header:biolims.common.dyIsQualified+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : dyResultCob,
		renderer : Ext.util.Format.comboRenderer(dyResultCob)
	});
//	cm.push({
//		dataIndex:'dyResult',
//		hidden : false,
//		header:'电泳是否合格',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:biolims.common.isQualified+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		sortable:true,
		hidden:true,
		editor : nextFlowCob
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : true,
		header:'是否提交',
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.reason,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'snpSample-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/snpjc/sample/snpSample/showSnpSamplePcrResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.pcrResult;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	if($("#snpSample_stateName").val()!=biolims.common.finish){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/snpjc/sample/snpSample/delSnpSamplePcrResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				snpSamplePcrResultGrid.getStore().commitChanges();
				snpSamplePcrResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text :biolims.common.uploadAttachment,
		handler : function() {
			var id=$("#snpSample_id").val();
			if(id!="NEW"){
				var win = Ext.getCmp('doc');
				if (win) {win.close();}
				var doc= new Ext.Window({
				id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
				plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
				collapsible: true,maximizable: true,
				items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
				html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+id+"&modelType=snpSamplePcrResult' frameborder='0' width='100%' height='100%' ></iframe>"}),
				buttons: [
				{ text: '关闭',
				 handler: function(){
				 doc.close(); }  }]  }); 
				 doc.show();
			}else{
				message("请先保存记录。");
			}
		}
});
	opts.tbar.push({
		text : biolims.report.checkFile,
		handler : loadPic
	});
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = snpSamplePcrResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						snpSamplePcrResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("dyResult", result);
						});
						snpSamplePcrResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "批量下一步",
//		handler : function() {
//			var records = snpSamplePcrResultGrid.getSelectRecord();
//			if(records.length>0){
//				if(records.length>2){
//					var productId = new Array();
//					$.each(records, function(j, k) {
//						productId[j]=k.get("productId");
//					});
//					for(var i=0;i<records.length;i++){
//						if(i!=0&&productId[i]!=productId[i-1]){
//							message("检测项目不同！");
//							return;
//						}
//					}
//					loadTestNextFlowCob();
//				}else{
//					loadTestNextFlowCob();
//				}
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = snpSamplePcrResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						snpSamplePcrResultGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						snpSamplePcrResultGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.resultPurification,
		handler : getChResult
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save1
	});
	}
	snpSamplePcrResultGrid=gridEditTable("snpSamplePcrResultdiv",cols,loadParam,opts);
	$("#snpSamplePcrResultdiv").data("snpSamplePcrResultGrid", snpSamplePcrResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function loadPic(){
		var model="snpSamplePcrResult";
		//alert($("#snpSample_id").val());
		var id=$("#snpSample_id").val();
		var ids="'"+id+"'";
//		window.open(window.ctx+"/experiment/snpjc/sample/loadPic.action?id="+$("#snpSample_id").val()+"&model="+model,'','height=700,width=1200,scrollbars=yes,resizable=yes');
		window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="+ids+"&model="+model,'','height=700,width=1200,scrollbars=yes,resizable=yes');
}

//保存
function save1(){
	var id=$("#snpSample_id").val();
	var itemJson = commonGetModifyRecords(snpSamplePcrResultGrid);
	if(itemJson.length>0){
		ajax("post", "/experiment/snpjc/sample/snpSample/saveSnpSamplePcrResult.action", {
			itemDataJson : itemJson,id : id
		}, function(data) {
			if (data.success) {
				snpSamplePcrResultGrid.getStore().commitChanges();
				snpSamplePcrResultGrid.getStore().reload();
				message("保存成功！");
			} else {
				message("保存失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}

function getChResult(){
	//获取选中的PCR结果数据
	var getPcr = snpSamplePcrResultGrid.getSelectionModel().getSelections();
	var getCh = snpSampelChResultGrid.store;
	if(getPcr.length>0){
		var flag=true;
		$.each(getPcr, function(i, obj) {
			for(var i=0;i<getCh.getCount();i++){
				if(getCh.getAt(i).get("code")==obj.get("code")){
					flag=false;
					message("有重复数据生成！");
					return;
				}
			}
			var r=obj.get("dyResult");
			if(r!="" && r!=null && r!=undefined){
//				if(r=="0"){
//					flag=false;
//					message("电泳结果不合格！");
//				}else{
//					flag=true;
//				}
			}else{
				flag=false;
				message("请填写电泳结果！");
			}
		});
		if(flag){
			$.each(getPcr, function(i, obj) {
				var ob = snpSampelChResultGrid.getStore().recordType;
				snpSampelChResultGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("tempId",obj.get("tempId"));
				p.set("sampleCode",obj.get("sampleCode"));
				p.set("code",obj.get("code"));
				p.set("orderNumber",obj.get("orderNumber"));
				p.set("checkCode",obj.get("checkCode"));
				p.set("productId",obj.get("productId"));
				p.set("productName",obj.get("productName"));
				p.set("sampleType",obj.get("sampleType"));
				p.set("result",obj.get("dyResult"));
				snpSampelChResultGrid.getStore().add(p);
				snpSampelChResultGrid.startEditing(0,0);
			});
			message("生成结果成功！");
		}
	}else{
		message("请选择生成纯化结果的PCR样本！");
	}
}