var cfdnaTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	    fields.push({
		name:'reciveDate',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'indexa',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'maxNum',
		type:"string"
	});
	    fields.push({
		name:'qcNum',
		type:"string"
	});
	    fields.push({
			name:'confirmDate',
			type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:30*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'reciveUser-id',
		hidden:true,
		header:'实验员ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'reciveUser-name',
		header:'实验员',
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'reciveDate',
		header:'实验时间',
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'下达人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'下达人',
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'下达时间',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'confirmDate',
		header:'完成时间',
		width:25*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:'模板ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:'模板',
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'indexa',
		header:'index',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态名称',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*6,
		hidden:true,
		sortable:true
	});
		cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:'实验组ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:'实验组',
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'maxNum',
		header:'容器数量',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'qcNum',
		header:'质控品数量',
		width:20*6,
		hidden:true,
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/cfdna/cfdnaTask/showCfdnaTaskListJson.action";
	var opts={};
	opts.title="cfDNA质量评估";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	cfdnaTaskGrid=gridTable("show_cfdnaTask_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/cfdna/cfdnaTask/editCfdnaTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/cfdna/cfdnaTask/editCfdnaTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/cfdna/cfdnaTask/viewCfdnaTask.action?id=' + id;
}
function exportexcel() {
	cfdnaTaskGrid.title = '导出列表';
	var vExportContent = cfdnaTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startreciveDate").val() != undefined) && ($("#startreciveDate").val() != '')) {
					var startreciveDatestr = ">=##@@##" + $("#startreciveDate").val();
					$("#reciveDate1").val(startreciveDatestr);
				}
				if (($("#endreciveDate").val() != undefined) && ($("#endreciveDate").val() != '')) {
					var endreciveDatestr = "<=##@@##" + $("#endreciveDate").val();

					$("#reciveDate2").val(endreciveDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(cfdnaTaskGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
