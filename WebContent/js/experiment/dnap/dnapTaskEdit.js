$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#dnapTask_state").val();
	if(id!="3"){
		load("/experiment/dnap/dnapTask/showDnapTaskTempList.action", null, "#dnapTaskTempPage");
		$("#markup").css("width","75%");
//		$("#tabs1").tabs({
//			select : function(event, ui) {
//				if(ui.index==1){
//					load("/experiment/dna/experimentDnaGet/showDnaTechTaskList.action", { }, "#keji");
//				}
//			}
//		});
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#dnapTaskTempPage").remove();
	}
});	
function add() {
	window.location = window.ctx + "/experiment/dnap/dnapTask/editDnapTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/dnap/dnapTask/showDnapTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#dnapTask", {
					userId : userId,
					userName : userName,
					formId : $("#dnapTask_id").val(),
					title : $("#dnapTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#dnapTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var dnapTaskItemDivData = $("#dnapTaskItemdiv").data("dnapTaskItemGrid");
		document.getElementById('dnapTaskItemJson').value = commonGetModifyRecords(dnapTaskItemDivData);
	    var dnapTaskTemplateDivData = $("#dnapTaskTemplatediv").data("dnapTaskTemplateGrid");
		document.getElementById('dnapTaskTemplateJson').value = commonGetModifyRecords(dnapTaskTemplateDivData);
	    var dnapTaskReagentDivData = $("#dnapTaskReagentdiv").data("dnapTaskReagentGrid");
		document.getElementById('dnapTaskReagentJson').value = commonGetModifyRecords(dnapTaskReagentDivData);
	    var dnapTaskCosDivData = $("#dnapTaskCosdiv").data("dnapTaskCosGrid");
		document.getElementById('dnapTaskCosJson').value = commonGetModifyRecords(dnapTaskCosDivData);
	    var dnapTaskResultDivData = $("#dnapTaskResultdiv").data("dnapTaskResultGrid");
		document.getElementById('dnapTaskResultJson').value = commonGetModifyRecords(dnapTaskResultDivData);
	 
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/dnap/dnapTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/dnap/dnapTask/copyDnapTask.action?id=' + $("#dnapTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#dnapTask_id").val() + "&tableId=dnapTask");
//}
$("#toolbarbutton_status").click(function(){
	commonChangeState("formId=" + $("#dnapTask_id").val() + "&tableId=DnapTask");
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#dnapTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#dnapTask_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#dnapTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'DNA纯化',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/dnap/dnapTask/showDnapTaskItemList.action", {
				id : $("#dnapTask_id").val()
			}, "#dnapTaskItempage");
load("/experiment/dnap/dnapTask/showDnapTaskTemplateList.action", {
				id : $("#dnapTask_id").val()
			}, "#dnapTaskTemplatepage");
load("/experiment/dnap/dnapTask/showDnapTaskReagentList.action", {
				id : $("#dnapTask_id").val()
			}, "#dnapTaskReagentpage");
load("/experiment/dnap/dnapTask/showDnapTaskCosList.action", {
				id : $("#dnapTask_id").val()
			}, "#dnapTaskCospage");
load("/experiment/dnap/dnapTask/showDnapTaskResultList.action", {
				id : $("#dnapTask_id").val()
			}, "#dnapTaskResultpage");

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	//调用模板
	function TemplateFun(){
		var type="doDnap";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
	
	function setTemplateFun(rec){
		if($("#dnapTask_acceptUser_name").val()==""){
			document.getElementById('dnapTask_acceptUser').value=rec.get('acceptUser-id');
			document.getElementById('dnapTask_acceptUser_name').value=rec.get('acceptUser-name');
		}
		var code=$("#dnapTask_template").val();
		
		if(code==""){
					document.getElementById('dnapTask_template').value=rec.get('id');
					document.getElementById('dnapTask_template_name').value=rec.get('name');
					var win = Ext.getCmp('TemplateFun');
					if(win){win.close();}
					var id=rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {
								var ob = dnapTaskTemplateGrid.getStore().recordType;
								dnapTaskTemplateGrid.stopEditing();
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("code",obj.code);
									p.set("stepName",obj.name);
									
									p.set("note",obj.note);
									dnapTaskTemplateGrid.getStore().add(p);							
								});
								
								dnapTaskTemplateGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = dnapTaskReagentGrid.getStore().recordType;
								dnapTaskReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									dnapTaskReagentGrid.getStore().add(p);							
								});
								
								dnapTaskReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = dnapTaskCosGrid.getStore().recordType;
								dnapTaskCosGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									dnapTaskCosGrid.getStore().add(p);							
								});			
								dnapTaskCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null);

			
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
						var ob1 = dnapTaskTemplateGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id"); 
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/dnap/dnapTask/delDnapTaskTemplateOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null);
								}else{								
									dnapTaskTemplateGrid.store.removeAll();
								}
							}
							dnapTaskTemplateGrid.store.removeAll();
		 				}

						var ob2 = dnapTaskReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");

								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/dnap/dnapTask/delDnapTaskReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
								}else{
									dnapTaskReagentGrid.store.removeAll();
								}
							}
							dnapTaskReagentGrid.store.removeAll();
		 				}
						//=========================================
						var ob3 = dnapTaskCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");
								
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/dnap/dnapTask/delDnapTaskCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null); 
								}else{
									dnapTaskCosGrid.store.removeAll();
								}
							}
							dnapTaskCosGrid.store.removeAll();
		 				}
						document.getElementById('dnapTask_template').value=rec.get('id');
						document.getElementById('dnapTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = dnapTaskTemplateGrid.getStore().recordType;
									dnapTaskTemplateGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("stepName",obj.name);
										
										p.set("note",obj.note);
										dnapTaskTemplateGrid.getStore().add(p);							
									});
									
									dnapTaskTemplateGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = dnapTaskReagentGrid.getStore().recordType;
									dnapTaskReagentGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tReagent",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("oneNum",obj.num);
										p.set("note",obj.note);
										dnapTaskReagentGrid.getStore().add(p);							
									});
									
									dnapTaskReagentGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = dnapTaskCosGrid.getStore().recordType;
									dnapTaskCosGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tCos",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("temperature",obj.temperature);
										p.set("speed",obj.speed);
										p.set("time",obj.time);
										p.set("note",obj.note);
										dnapTaskCosGrid.getStore().add(p);							
									});			
									dnapTaskCosGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						}
					}

}
	
	
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	function ckcrk(){
		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = dnapTaskResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message("有样本未提交，不能初始化！");
						return;
					}
				}
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");
								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message("回滚失败！");
							}
						}, null);
					}
					
				}
			}
		});
	}
	
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '保存'
			});
		item.on('click', ckcrk2);
		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);
		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "DnapTask",id : $("#dnapTask_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
			} else {
				message("办理回滚失败！");
			}
		}, null);
	}

	var loadtestUser;
	//选择实验组用户
	function testUser(){
		var gid=$("#dnapTask_acceptUser").val();
		if(gid!=""){
			var options = {};
			options.width = 500;
			options.height = 500;
			var confirm=biolims.common.confirm;
			loadtestUser=loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				confirm : function() {
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						$("#dnapTask_reciveUser").val(selectRecord[0].get("user-id"));
						$("#dnapTask_reciveUser_name").val(selectRecord[0].get("user-name"));
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
		}else{
			message("请选择实验组");
		}
		
	}
	function setUserGroupUser(){
		var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
				$("#dnapTask_reciveUser").val(selectRecord[0].get("user-id"));
				$("#dnapTask_reciveUser_name").val(selectRecord[0].get("user-name"));
		}else{
			message(biolims.common.selectYouWant);
			return;
		}
		loadtestUser.dialog("close");
	}
