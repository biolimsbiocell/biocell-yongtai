﻿var fishProTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'checked',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'sequencingFun',
		type:"string"
	});
	   fields.push({
	   name:'inspectDate',
	   type:"string"
   });
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'fishProTask-id',
		type:"string"
	});
	    fields.push({
		name:'fishProTask-name',
		type:"string"
	});
	    fields.push({
		name:'storage-id',
		type:"string"
	});
	    fields.push({
		name:'storage-name',
		type:"string"
	});
	    fields.push({
		name:'concentration',
		type:"string"
	});
	    fields.push({
		name:'result',
		type:"string"
	});
	    fields.push({
		name:'reason',
		type:"string"
	});
	    fields.push({
		name:'stepNum',
		type:"string"
	});
	    fields.push({
		name:'volume',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'orderNumber',
		type:"string"
	});
	    fields.push({
		name:'classify',
		type:"string"
	});
	    fields.push({
		name:'productNum',
		type:"string"
	});
	    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
	    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
	    fields.push({
		name:'sampleNum',
		type:"string"
	});
	    fields.push({
		name:'sampleConsume',
		type:"string"
	});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表id',
		width:40*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'checked',
		hidden : false,
		header:biolims.common.checkedSampleCode,
		width:25*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.orderNumber,
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'样本名称',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:30*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证号',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:25*6
	});
	
	cm.push({
		dataIndex:'sequencingFun',
		hidden : true,
		header:'检测方法',
		width:20*6

	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单id',
		width:30*6
	});

	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:30*6
	});

	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:25*6,
		renderer: formatDate
	});
//	cm.push({
//		dataIndex:'dicSampleType-id',
//		hidden : true,
//		header:'中间产物编号',
//		width:20*6,
//		sortable:true
//	});
//	var testDicSampleType2 =new Ext.form.TextField({
//        allowBlank: false
//	});
//	testDicSampleType2.on('focus', function() {
//		loadTestDicSampleType2();
//	});
//	cm.push({
//		dataIndex:'dicSampleType-name',
//		header:'中间产物类型<font color="red" size="4px">*</font>',
//		width:15*10,
//		sortable:true,
//		editor : testDicSampleType2
//	});
//	cm.push({
//		dataIndex:'productNum',
//		hidden : false,
//		header:'中间产物数量<font color="red" size="4px">*</font>',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.probeBitNum,
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sampleConsume',
		hidden : true,
		header:'样本用量',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:'浓度',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'storage-id',
		header:'储位',
		width:30*6,
		hidden : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'storage-name',
		header:'储位',
		width:30*6,
		hidden : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	var result = new Ext.form.ComboBox({
//		store : new Ext.data.JsonStore({
//			fields : [ 'id', 'name' ],
//			data : [ {
//				id : '1',
//				name : '合格'
//			},{
//				id : '0',
//				name : '不合格'
//			}]
//		}),
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'结果',
//		width:15*6,
//		renderer: Ext.util.Format.comboRenderer(result),editor: null
//	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishProTask-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishProTask-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务',
		width:20*6
	});
	
	cols.cm=cm;
	var loadParam={};

	loadParam.url=ctx+"/experiment/fish/fishProTask/showFishProTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.common.flakingSampleDetail;
	opts.height =  document.body.clientHeight-220;
	opts.tbar = [];
	var state=$("#fishProTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/fishProTask/delFishProTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				fishProTaskItemGrid.getStore().commitChanges();
				fishProTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.probeBitNum,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_sampleNum_div"), "批量探针组数量", null, {
				"确定" : function() {
					var records = fishProTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var probe = $("#probe").val();
						fishProTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("sampleNum", probe);
						});
						fishProTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "批量中间产物类型",
//		handler : function() {
//			var options = {};
//			options.width = document.body.clientWidth-800;
//			options.height = document.body.clientHeight-40;
//			loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
//				"确定" : function() {
//					var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
//					var selectRecord = operGrid.getSelectionModel().getSelections();
//					var records = fishProTaskItemGrid.getSelectRecord();
//					if (selectRecord.length > 0) {
//						$.each(selectRecord, function(i, obj) {
//							$.each(records, function(a, b) {
//								b.set("dicSampleType-id", obj.get("id"));
//								b.set("dicSampleType-name", obj.get("name"));
//							});
//						});
//					}else{
//						message("请选择您要选择的数据");
//						return;
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	
//	opts.tbar.push({
//		text : "批量产物数量",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_productNum_div"), "批量产物数量", null, {
//				"确定" : function() {
//					var records = fishProTaskItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var productNum = $("#productNum").val();
//						fishProTaskItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("productNum", productNum);
//						});
//						fishProTaskItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	
	}
	fishProTaskItemGrid=gridEditTable("fishProTaskItemdiv",cols,loadParam,opts);
	$("#fishProTaskItemdiv").data("fishProTaskItemGrid", fishProTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(fishProTaskItemGrid);
	var id=$("#karyoShipTask_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/fish/fishProTask/saveFishProTaskItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					fishProTaskItemGrid.getStore().commitChanges();
					fishProTaskItemGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

var loadDicSampleType2;	
//查询样本类型
function loadTestDicSampleType2(){
	var options = {};
	options.width = document.body.clientWidth-800;
	options.height = document.body.clientHeight-40;
	loadDicSampleType2=loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action?a=2", {
		"确定" : function() {
			var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = fishProTaskItemGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					$.each(records, function(a, b) {
						b.set("dicSampleType-id", obj.get("id"));
						b.set("dicSampleType-name", obj.get("name"));
					});
				});
			}else{
				message("请选择您要选择的数据");
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}

function setDicSampleType2(){
	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = fishProTaskItemGrid.getSelectRecord();
	
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("dicSampleType-id", b.get("id"));
				obj.set("dicSampleType-name", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadDicSampleType2.dialog("close");

}