﻿var samplePreTaskCosGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
   fields.push({
		name:'itemId',
		type:"string"
	});
   fields.push({
		name:'tCos',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	//
	   fields.push({
			name:'temperature',
			type:"string"
		});
	   fields.push({
			name:'speed',
			type:"string"
		});
	   fields.push({
			name:'time',
			type:"string"
		});
	   fields.push({
			name:'note',
			type:"string"
		});
	//
	   fields.push({
		name:'isGood',
		type:"string"
	});
	    fields.push({
		name:'samplePreTask-id',
		type:"string"
	});
	    fields.push({
		name:'samplePreTask-name',
		type:"string"
	});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'设备id',
		width:20*6
	});
	cm.push({
		dataIndex:'tCos',
		hidden : true,
		header:'模板设备id',
		width:20*6
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'模板步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.instrumentNo,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.instrumentName,
		width:20*6
	});
//	cm.push({
//		dataIndex:'isGood',
//		hidden : false,
//		header:'是否通过检验',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '否' ], [ '1', '是' ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isGood,
		width:20*6,
		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	//
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.common.temperature,
		width:20*6
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:biolims.common.speed,
		width:20*6
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.common.time,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'samplePreTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10
	});
	cm.push({
		dataIndex:'samplePreTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/samplePreTask/showSamplePreTaskCosListJson.action?id="+$("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title=biolims.common.instrumentDetail;
	opts.height =  document.body.clientHeight-220;
	opts.tbar = [];
	var state=$("#samplePreTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/samplePreTask/delSamplePreTaskCos.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				samplePreTaskCosGrid.getStore().commitChanges();
				samplePreTaskCosGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : function (){
			//获取选择的数据
			var selectRcords=samplePreTaskTemplateGrid.getSelectionModel().getSelections();
			//获取全部数据
			var allRcords=samplePreTaskTemplateGrid.store;
			//选中的数量
			var length1=selectRcords.length;
			//全部数据量
			var length2=allRcords.getCount();
			//var selRecord = cosItemGrid.store;
			if(length2>0){
				if(length1==1){
					var code="";
					$.each(selectRcords, function(i, obj) {
						code=obj.get("code");
					});
					if(code!=null){
						var ob = samplePreTaskCosGrid.getStore().recordType;
						var p = new ob({});
						p.isNew = true;
						p.set("itemId", code);
						samplePreTaskCosGrid.stopEditing();
						samplePreTaskCosGrid.getStore().insert(0, p);
						samplePreTaskCosGrid.startEditing(0, 0);
					}else{
						message(biolims.common.addTemplateDetail);
						return;
					}				
				}else if(length1>1){
					message(biolims.common.onlyChooseOne);
					return;
				}else{
					message(biolims.common.pleaseSelectData);
					return;
				}
			}else{
				message(biolims.common.theDataIsEmpty);
				return;
			}
		}
});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	}
	samplePreTaskCosGrid=gridEditTable("samplePreTaskCosdiv",cols,loadParam,opts);
	$("#samplePreTaskCosdiv").data("samplePreTaskCosGrid", samplePreTaskCosGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

