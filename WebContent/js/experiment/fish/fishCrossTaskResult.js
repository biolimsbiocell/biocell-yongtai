﻿var fishCrossTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
		fields.push({
		name:'code',
		type:"string"
	});
		fields.push({
		name:'zpCode',
		type:"string"
	});
		fields.push({
		name:'slideCode',
		type:"string"
	});
		fields.push({
		name:'sampleCode',
		type:"string"
	});
   		fields.push({
		name:'productId',
		type:"string"
	});
   		fields.push({
		name:'productName',
		type:"string"
	});
   		fields.push({
		name:'patientName',
		type:"string"
	});
  		fields.push({
		name:'idCard',
		type:"string"
	});
		fields.push({
		name:'sequencingFun',
		type:"string"
	});
		fields.push({
		name:'inspectDate',
		type:"string"
	});
		fields.push({
		name:'orderId',
		type:"string"
	});
		fields.push({
		name:'phone',
		type:"string"
	});
		fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'fishCrossTask-id',
		type:"string"
	});
	    fields.push({
		name:'fishCrossTask-name',
		type:"string"
	});
	    fields.push({
		name:'submit',
		type:"string"
	});
	    fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'reason',
		type:"string"
	});
	    fields.push({
		name:'concentration',
		type:"string"
	});
	    fields.push({
		name:'classify',
		type:"string"
	});
	    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
	    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	    fields.push({
		name:'sampleNum',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表id',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:30*6
	});
	cm.push({
		dataIndex:'zpCode',
		hidden : true,
		sortable:true,
		header:'制片样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:30*6
	});
	cm.push({
		dataIndex:'slideCode',
		hidden : false,
		header:biolims.common.slideCode,
		sortable:true,
		width:25*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden:true,
		header:'中间产物编号',
		width:15*10,
		sortable:true
	});
	var testDicSampleType =new Ext.form.TextField({
        allowBlank: false
	});
	testDicSampleType.on('focus', function() {
		loadTestDicSampleType();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:'样本类型',
		width:15*10,
		hidden:true,
		sortable:true,
		editor : testDicSampleType
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.user.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证号',
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:30*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:30*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:'样本数量',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sequencingFun',
		hidden : true,
		header:'检测方法',
		width:20*6
	});
	
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单Id',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:'浓度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积(μL)',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	var unitstore = new Ext.data.JsonStore({
		root : 'results',
		remoteSort : true,
		fields : [ {name : 'id'}, {name : 'name'}],
		proxy : new Ext.data.HttpProxy({
			url : window.ctx
					+ '/common/comsearch/com/getDicUnit.action',
			method : 'POST'
		})
	});
	unitstore.load();
	var unitCob = new Ext.form.ComboBox({
		store : unitstore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'remote',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex : 'unit',
		header : '单位',
		width : 100,
		hidden:true,
		renderer : Ext.util.Format.comboRenderer(unitCob),
		editor : unitCob
	});
	
	var storeisresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified], [ '0', biolims.common.disqualified ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeisresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result+'<font color="red">*</font>',
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow+'<font color="red">*</font>',
		width:15*10,
		sortable:true,
		hidden:false,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'method',
		hidden : true,
		header:'处理意见',
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1',biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.Submitted+'<font color="red">*</font>',
		width:20*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});

	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'fishCrossTask-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishCrossTask-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务',
		width:20*6
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/fishCrossTask/showFishCrossTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit=200;
	var opts={};
	opts.title=biolims.common.hybridsResult;
	opts.height =  document.body.clientHeight-220;
	opts.tbar = [];
	var state=$("#fishCrossTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/fishCrossTask/delFishCrossTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				fishCrossTaskResultGrid.getStore().commitChanges();
				fishCrossTaskResultGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.batchSlide,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#batccc_slideCode_div"), biolims.common.batchSlide, null, {
				"确定" : function() {
					var records = fishCrossTaskResultGrid .getSelectRecord();
					if (records && records.length > 0) {
						var slideCode = $("#slideCode").val();
						fishCrossTaskResultGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("slideCode", slideCode);
						});
						fishCrossTaskResultGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});

//	opts.tbar.push({
//		text : "批量单位",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_unit_div"), "批量单位", null, {
//				"确定" : function() {
//					var records = fishCrossTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var unit = $("#unit").val();
//						fishCrossTaskResultGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("unit", unit);
//						});
//						fishCrossTaskResultGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), biolims.common.batchResult, null, {
				"确定" : function() {
					var records = fishCrossTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						fishCrossTaskResultGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						fishCrossTaskResultGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "批量下一步",
//		handler : function() {
//			var records = fishCrossTaskResultGrid.getSelectRecord();
//			if(records.length>0){
//				if(records.length>2){
//					var productId = new Array();
//					$.each(records, function(j, k) {
//						productId[j]=k.get("productId");
//					});
//					for(var i=0;i<records.length;i++){
//						if(i!=0&&productId[i]!=productId[i-1]){
//							message("检测项目不同！");
//							return;
//						}
//					}
//					loadTestNextFlowCob();
//				}else{
//					loadTestNextFlowCob();
//				}
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = fishCrossTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						fishCrossTaskResultGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						fishCrossTaskResultGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : "样本类型",
//		handler : function() {
//			var options = {};
//			options.width = document.body.clientWidth-800;
//			options.height = document.body.clientHeight-40;
//			loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
//				"确定" : function() {
//					var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
//					var selectRecord = operGrid.getSelectionModel().getSelections();
//					var records = fishCrossTaskResultGrid.getSelectRecord();
//					if (selectRecord.length > 0) {
//						$.each(records, function(i, obj) {
//							$.each(selectRecord, function(a, b) {
//								obj.set("dicSampleType-id", b.get("id"));
//								obj.set("dicSampleType-name", b.get("name"));
//							});
//						});
//					}else{
//						message(biolims.common.selectYouWant);
//						return;
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text :biolims.common.exportList,
		handler : exportexcel
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	}
	fishCrossTaskResultGrid=gridEditTable("fishCrossTaskResultdiv",cols,loadParam,opts);
	$("#fishCrossTaskResultdiv").data("fishCrossTaskResultGrid", fishCrossTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(fishCrossTaskResultGrid);
	var id=$("#fishCrossTask_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/fish/fishCrossTask/saveFishCrossTaskResult.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					fishCrossTaskResultGrid.getStore().commitChanges();
					fishCrossTaskResultGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveSuccess);
				}
			}, null);
		}
	}else{
		message(biolims.common.noData2Save);
	}
	  }else{
		  message(biolims.storage.infoChange);
	}
}

function exportexcel() {
	fishCrossTaskResultGrid.title = biolims.common.exportList;
	var vExportContent = fishCrossTaskResultGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
var loadDicSampleType;
//查询样本类型
function loadTestDicSampleType(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadDicSampleType=loadDialogPage(null,biolims.common.sampleType, "/sample/dicSampleType/dicSampleTypeSelect.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = fishCrossTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("dicSampleType-id", b.get("id"));
							obj.set("dicSampleType-name", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setDicSampleType(){

	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = fishCrossTaskResultGrid.getSelectRecord();
	
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("dicSampleType-id", b.get("id"));
				obj.set("dicSampleType-name", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadDicSampleType.dialog("close");

}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = fishCrossTaskResultGrid.getSelectRecord();
	var productId1="";
	$.each(records1, function(j, k) {
		productId1=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=FishCrossTask&productId="+productId1, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = fishCrossTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = fishCrossTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}

//提交样本
function submitSample(){
	var id=$("#fishCrossTask_id").val();  
	if(fishCrossTaskResultGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var record = fishCrossTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(!record[i].get("submit")){
				flg=true;
			}
			if(record[i].get("result")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
			if(record[i].get("nextFlowId")==""){
				message(biolims.common.nextStepNotEmpty);
				return;
			}
		}
	}else{
		var grid=fishCrossTaskResultGrid.store;
		for(var i=0;i<grid.getCount();i++){
			if(grid.getAt(i).get("submit")==""){
				flg=true;
			}
			if(grid.getAt(i).get("result")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
			if(grid.getAt(i).get("nextFlowId")==""){
				message(biolims.common.nextStepNotEmpty);
				return;
			}
		}
	} 
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		var records = [];
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		
		ajax("post", "/experiment/fish/fishCrossTask/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				fishCrossTaskResultGrid.getStore().commitChanges();
				fishCrossTaskResultGrid.getStore().reload();
				message(biolims.common.submitSuccess);
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);
	}else{
		message(biolims.common.noData2Submit);
	}
}
	


	

