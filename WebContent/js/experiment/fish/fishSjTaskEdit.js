$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/experiment/fish/fishSjTask/editFishSjTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/fish/fishSjTask/showFishSjTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("FishSjTask", {
					userId : userId,
					userName : userName,
					formId : $("#fishSjTask_id").val(),
					title : $("#fishSjTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	var codeList = new Array();
	if (fishSjTaskResultGrid.getAllRecord().length > 0) {
		var selRecord = fishSjTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){

			codeList.push(selRecord.getAt(j).get("sampleCode"));
			if(selRecord.getAt(j).get("isGood")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
			if(selRecord.getAt(j).get("isCommit")==""){
				message(biolims.common.submitSamplePlease);
				return;
			}
		
		}
		if(fishSjTaskResultGrid.getModifyRecord().length > 0){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
			completeTask($("#fishSjTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});

function save() {
//	var code=$("#fishSjTask_template").val();
//	if(code==""||code==null){
//		message("请选择模板！");
//		return;
//	}
if(checkSubmit()==true){
	    var fishSjTaskResultDivData = fishSjTaskResultGrid;
		document.getElementById('fishSjTaskResultJson').value = commonGetModifyRecords(fishSjTaskResultDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/fish/fishSjTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg :biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/fish/fishSjTask/copyFishSjTask.action?id=' + $("#fishSjTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#fishSjTask_id").val() + "&tableId=fishSjTask");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#fishSjTask_id").val()){
		commonChangeState("formId=" + $("#fishSjTask_id").val() + "&tableId=FishSjTask");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#fishSjTask_id").val());
	nsc.push(biolims.user.NoNull);
	fs.push($("#fishSjTask_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.onBoard,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/fish/fishSjTask/showFishSjTaskResultList.action", {
				id : $("#fishSjTask_id").val()
			}, "#fishSjTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
			    	text: biolims.common.copy
					});
item.on('click', editCopy);

////选择实验模板
//function TemplateFun(){
//	var type="doFishSj";
//	var win = Ext.getCmp('TemplateFun');
//	if (win) {win.close();}
//	var TemplateFun= new Ext.Window({
//	id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//	TemplateFun.close(); }  }]  });     
//	TemplateFun.show();
//}
//function setTemplateFun(rec){
//	document.getElementById('fishSjTask_acceptUser').value=rec.get('acceptUser-id');
//	document.getElementById('fishSjTask_acceptUser_name').value=rec.get('acceptUser-name');
//	document.getElementById('fishSjTask_template').value=rec.get('id');
//	document.getElementById('fishSjTask_template_name').value=rec.get('name');
//	var win = Ext.getCmp('TemplateFun');
//	if(win){win.close();}
//}


////选择杂交洗脱
//function FishCrossTaskFun(){
//	alert(33333);
//	var win = Ext.getCmp('FishCrossTaskFun');
//	if (win) {win.close();}
//	var FishCrossTaskFun= new Ext.Window({
//	id:'FishCrossTaskFun',modal:true,title:'选择杂交洗脱',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/experiment/fish/fishCrossTask/FishCrossTaskSelect.action?flag=fishCrossTaskFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//	FishCrossTaskFun.close(); }  }]  });
//	FishCrossTaskFun.show();
//}

//选择杂交洗脱
//重复加载的删除，删除后重新加载
function FishCrossTaskFun(){
	var code=$("#fishSjTask_fishCrossTaskId").val();
	var options = {};
	options.width = 600;
	options.height = 600;
	var url="/experiment/fish/fishCrossTask/finshSjReason.action";
	loadDialogPage(null, "Selection of hybrid elution", url, {
		 "confirm": function() {
	 var selected=showFishSjDialogGrid.getSelectionModel().getSelections();
	 var srId="";
	 var srName="";
	 var idStr="";
	 var idStrs="";
	 if(selected.length>0){
		 $.each(selected, function(i, obj) {
			 srId += obj.get("id")+",";
			 srName+=obj.get("name")+",";
			 idStr += "'"+obj.get("id")+"',";
		 });
		 //字符串截取
		 idStrs = idStr.substring(0, idStr.length-1);
		//没有数据的时候
		 if(code=="" || code==null){
			 $("#fishSjTask_fishCrossTaskId").val(srId);
			 $("#fishSjTask_fishCrossTaskName").val(srName);
			 //加载列表，用ajax调用根据id查询列表的action
			 ajax("post", "/experiment/fish/fishCrossTask/setResultToFishSJ.action", {
				 code : idStrs
			 	}, function(data) {
			 		if (data.success) {
						var ob = fishSjTaskResultGrid.getStore().recordType;
						fishSjTaskResultGrid.stopEditing();
						$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("code",obj.code);
						p.set("sampleCode",obj.sampleCode);
						p.set("slideCode",obj.slideCode);
						p.set("productId",obj.productId);
						p.set("productName",obj.productName);
						p.set("sampleType",obj.sampleType);
						p.set("note",obj.note);
						p.set("isGood","1");
						ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
							model : "FishSjTask",productId:obj.productId
						}, function(data) {
							p.set("nextFlowId",data.dnextId);
							p.set("nextFlow",data.dnextName);
						}, null);
						fishSjTaskResultGrid.getStore().add(p);							
						});
						fishSjTaskResultGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
					}, null); 
					 options.close();
	 			}else{
	 				//数据一样
	 				if(srId==code){
	 					options.close();
	 				}else{
	 					var ob1 = fishSjTaskResultGrid.store;
	 					if (ob1.getCount() > 0) {
	 					for(var j=0;j<ob1.getCount();j++){
	 						var oldv = ob1.getAt(j).get("id");
	 						//根据ID删除
	 						if(oldv!=null){
	 							ajax("post", "/experiment/fish/fishSjTask/delFishSjTaskResultOne.action", {
	 								ids : oldv
	 							}, function(data) {
	 								if (data.success) {
	 									message(biolims.common.deleteSuccess);
	 								} else {
	 									message(biolims.common.deleteFailed);
	 								}
	 							}, null);
	 						}else{								
	 							fishSjTaskResultGrid.store.removeAll();
	 						}
	 					}
	 						fishSjTaskResultGrid.store.removeAll();
	 					}
	 					//重新加载数据
	 					document.getElementById('fishSjTask_fishCrossTaskId').value=srId;
	 					document.getElementById('fishSjTask_fishCrossTaskName').value=srName;
	 					ajax("post", "/experiment/fish/fishCrossTask/setResultToFishSJ.action", {
	 						code : idStrs,
	 						}, function(data) {
	 							if (data.success) {
	 								var ob = fishSjTaskResultGrid.getStore().recordType;
	 								fishSjTaskResultGrid.stopEditing();
	 								$.each(data.data, function(i, obj) {
	 									var p = new ob({});
	 									p.isNew = true;
	 									p.set("code",obj.code);
	 									p.set("sampleCode",obj.sampleCode);
	 									p.set("slideCode",obj.slideCode);
	 									p.set("productId",obj.productId);
	 									p.set("productName",obj.productName);
	 									p.set("sampleType",obj.sampleType);
	 									p.set("note",obj.note);
	 									fishSjTaskResultGrid.getStore().add(p);							
	 								});
	 								fishSjTaskResultGrid.startEditing(0, 0);		
	 							} else {
	 								message(biolims.common.anErrorOccurred);
	 							}
	 						}, null);
	 					 options.close();
	 				}
	 			}
			 }else{
				 options.close();
			 }
		}
	}, true, options);
}

function setfishCrossTaskFun(rec){
	var code=$("#fishSjTask_fishCrossTaskId").val();
	if(code==""){
		document.getElementById('fishSjTask_fishCrossTaskId').value=rec.get('id');
		document.getElementById('fishSjTask_fishCrossTaskName').value=rec.get('name');
		var win = Ext.getCmp('FishCrossTaskFun');
		if(win){win.close();}
		var id=rec.get('id');
		ajax("post", "/experiment/fish/fishCrossTask/setResultToFishSJ.action", {
			code : id
			}, function(data) {
				if (data.success) {
					var ob = fishSjTaskResultGrid.getStore().recordType;
					fishSjTaskResultGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("code",obj.code);
						p.set("sampleCode",obj.sampleCode);
						p.set("slideCode",obj.slideCode);
						p.set("productId",obj.productId);
						p.set("productName",obj.productName);
						p.set("sampleType",obj.sampleType);
						p.set("note",obj.note);
						p.set("isGood","1");
						ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
							model : "FishSjTask",productId:obj.productId
						}, function(data) {
							p.set("nextFlowId",data.dnextId);
							p.set("nextFlow",data.dnextName);
						}, null);
						fishSjTaskResultGrid.getStore().add(p);							
					});
					fishSjTaskResultGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null); 
	}else{
		if(rec.get('id')==code){
			var win = Ext.getCmp('FishCrossTaskFun');
			if(win){win.close();}
			options.close();
		}else{
			var ob1 = fishSjTaskResultGrid.store;
			if (ob1.getCount() > 0) {
			for(var j=0;j<ob1.getCount();j++){
				var oldv = ob1.getAt(j).get("id");
				if(oldv!=null){
					ajax("post", "/experiment/fish/fishSjTask/delFishSjTaskResultOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message(biolims.common.deleteSuccess);
						} else {
							message(biolims.common.deleteFailed);
						}
					}, null);
				}else{								
					fishSjTaskResultGrid.store.removeAll();
				}
			}
				fishSjTaskResultGrid.store.removeAll();
			}
			document.getElementById('fishSjTask_fishCrossTaskId').value=rec.get('id');
			document.getElementById('fishSjTask_fishCrossTaskName').value=rec.get('name');
			var win = Ext.getCmp('FishCrossTaskFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/experiment/fish/fishCrossTask/setResultToFishSJ.action", {
				code : id,
				}, function(data) {
					if (data.success) {
						var ob = fishSjTaskResultGrid.getStore().recordType;
						fishSjTaskResultGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.code);
							p.set("sampleCode",obj.sampleCode);
							p.set("slideCode",obj.slideCode);
							p.set("productId",obj.productId);
							p.set("productName",obj.productName);
							p.set("sampleType",obj.sampleType);
							p.set("note",obj.note);
							fishSjTaskResultGrid.getStore().add(p);							
						});
						fishSjTaskResultGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
		}
	}
}


Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = fishSjTaskResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("isCommit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}


			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : "FishSjTask"
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("isCommit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "FishSjTask",id : $("#fishSjTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
			window.location.reload();
		} else {
			message("办理回滚失败！");
		}
	}, null);
}
