﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
	var state = $("#fishProTask_state").val();
	if(state =="3"){
		load("/experiment/fish/fishProTask/showFishProTasktempList.action", null, "#fishProTaskTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showAcceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#showworkOrder").css("display","none");
	}
	setTimeout(function() {
		var getGrid=fishProTaskTemplateGrid.store;
		if(getGrid.getCount()==0){
			loadTemplate($("#fishProTask_template").val());
		}
	}, 2000);
});
function add() {
	window.location = window.ctx + "/experiment/fish/fishProTask/editFishProTask.action?maxNum="+$("#fishProTask_maxNum").val();
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/fish/fishProTask/showFishProTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("FishProTask", {
					userId : userId,
					userName : userName,
					formId : $("#fishProTask_id").val(),
					title : $("#fishProTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {

	var codeList = new Array();
	var codeList1 = new Array();
	var selRecord1 = fishProTaskItemGrid.store;
	var flag=true;
	var flag1=true;
	if (fishProTaskResultGrid.getAllRecord().length > 0) {
		var selRecord = fishProTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			codeList.push(selRecord.getAt(j).get("sampleCode"));
			if(selRecord.getAt(j).get("result")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
			if(selRecord.getAt(j).get("nextFlowId")==""){
				message(biolims.common.nextStepNotEmpty);
				return;
			}
		}
		if(fishProTaskResultGrid.getModifyRecord().length > 0){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
		completeTask($("#fishProTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});


function save() {
	var code=$("#fishProTask_template").val();
	if(code==""||code==null){
		message(biolims.common.selectTaskModel);
		return;
	}
if(checkSubmit()==true){    Ext.MessageBox.show({ msg:biolims.common.savingData, progressText:biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });   
	    var fishProTaskItemDivData = $("#fishProTaskItemdiv").data("fishProTaskItemGrid");
		document.getElementById('fishProTaskItemJson').value = commonGetModifyRecords(fishProTaskItemDivData);
	    var fishProTaskResultDivData = $("#fishProTaskResultdiv").data("fishProTaskResultGrid");
		document.getElementById('fishProTaskResultJson').value = commonGetModifyRecords(fishProTaskResultDivData);
		var fishProTaskTemplateDivData = $("#fishProTaskTemplatediv").data("fishProTaskTemplateGrid");
		document.getElementById('fishProTaskTemplateJson').value = commonGetModifyRecords(fishProTaskTemplateDivData);
		var fishProTaskReagentDivData = $("#fishProTaskReagentdiv").data("fishProTaskReagentGrid");
		document.getElementById('fishProTaskReagentJson').value = commonGetModifyRecords(fishProTaskReagentDivData);
		var fishProTaskCosDivData = $("#fishProTaskCosdiv").data("fishProTaskCosGrid");
		document.getElementById('fishProTaskCosJson').value = commonGetModifyRecords(fishProTaskCosDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/fish/fishProTask/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/fish/fishProTask/copyFishProTask.action?id=' + $("#fishProTask_id").val();
}

$("#toolbarbutton_status").click(function(){
//	var bloodInfoGrid = fishProTaskResultGrid.getStore();
//	var bloodItemGrid = fishProTaskItemGrid.getStore();
//	var num = 0;
//	for(var i= 0; i<bloodItemGrid.getCount();i++){
//		for(var j=0;j<bloodInfoGrid.getCount();j++){
//			if(bloodItemGrid.getAt(j).get("sampleCode")==bloodInfoGrid.getAt(i).get("sampleCode")){
//				j=bloodInfoGrid.getCount();
//			}else{
//				num=num+1;
//				if(num==bloodInfoGrid.getCount()){
//					num=0;
//					message("请完成实验！");
//					return;
//				}
//			}
////			if(bloodItemGrid.getAt(j).get("sampleCode")!=bloodInfoGrid.getAt(i).get("sampleCode")){
////				message("请完成实验！");
////				return;
////			}
//		}
//	}

	commonChangeState("formId=" + $("#fishProTask_id").val() + "&tableId=FishProTask");
});
//setTimeout(function() {
//	if($("#fishProTask_template").val()){
//		var maxNum = $("#fishProTask_maxNum").val();
//		if(maxNum>0){
//				load("/storage/container/sampleContainerTest.action", {
//					id : $("#fishProTask_template").val(),
//					type :$("#type").val(),
//					maxNum : 0
//				}, "#gridContainerdiv0", function(){
//					if(maxNum>1){
//					load("/storage/container/sampleContainerTest.action", {
//						id : $("#fishProTask_template").val(),
//						type :$("#type").val(),
//						maxNum : 1
//					}, "#gridContainerdiv1", null)
//					}
//				});
//			
//		}
//	}
//}, 100);
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#fishProTask_id").val());
	nsc.push(biolims.common.codeNotEmpty);
	fs.push($("#fishProTask_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.flaking,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/fish/fishProTask/showFishProTaskItemList.action", {
	id : $("#fishProTask_id").val()
}, "#fishProTaskItempage");
load("/experiment/fish/fishProTask/showFishProTaskTemplateList.action", {
	id : $("#fishProTask_id").val()
}, "#fishProTaskTemplatepage");
load("/experiment/fish/fishProTask/showFishProTaskReagentList.action", {
	id : $("#fishProTask_id").val()
}, "#fishProTaskReagentpage");
load("/experiment/fish/fishProTask/showFishProTaskCosList.action", {
	id : $("#fishProTask_id").val()
}, "#fishProTaskCospage");
load("/experiment/fish/fishProTask/showFishProTaskResultList.action", {
	id : $("#fishProTask_id").val()
}, "#fishProTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);

//调用模板
function TemplateFun(){
		var type="doFishPro";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'Select Template',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: 'close',
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
function setTemplateFun(rec){
	var duringDays=rec.get('duringDays');
	var remindDays=rec.get('remindDays');
	document.getElementById('fishProTask_acceptUser').value=rec.get('acceptUser-id');
	document.getElementById('fishProTask_acceptUser_name').value=rec.get('acceptUser-name');
	var createDate=$('#fishProTask_createDate').val();
	createDate=new Date(createDate);
	var date = new Date(createDate.getTime() + 24*60*60*1000*duringDays);  
	var result = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
	var code=$("#fishProTask_template").val();
		if(code==""){
			document.getElementById('fishProTask_template').value=rec.get('id');
			document.getElementById('fishProTask_template_name').value=rec.get('name');
			var win = Ext.getCmp('TemplateFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/system/template/template/setTemplateItem.action", {
				code : id
				}, function(data) {
					if (data.success) {
						var ob = fishProTaskTemplateGrid.getStore().recordType;
						fishProTaskTemplateGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tItem",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							
							p.set("note",obj.note);
							fishProTaskTemplateGrid.getStore().add(p);							
						});
						
						fishProTaskTemplateGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateReagent.action", {
				code : id
				}, function(data) {
					if (data.success) {	

						var ob = fishProTaskReagentGrid.getStore().recordType;
						fishProTaskReagentGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tReagent",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							p.set("batch",obj.batch);
							p.set("isGood",obj.isGood);
							p.set("itemId",obj.itemId);
							
							p.set("oneNum",obj.num);
							p.set("note",obj.note);
							fishProTaskReagentGrid.getStore().add(p);							
						});
						
						fishProTaskReagentGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateCos.action", {
				code : id
				}, function(data) {
					if (data.success) {	

						var ob = fishProTaskCosGrid.getStore().recordType;
						fishProTaskCosGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tCos",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							p.set("isGood",obj.isGood);
							p.set("itemId",obj.itemId);
							
							p.set("temperature",obj.temperature);
							p.set("speed",obj.speed);
							p.set("time",obj.time);
							p.set("note",obj.note);
							fishProTaskCosGrid.getStore().add(p);							
						});			
						fishProTaskCosGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
 				var ob1 = fishProTaskTemplateGrid.store;
 				if (ob1.getCount() > 0) {
					for(var j=0;j<ob1.getCount();j++){
						var oldv = ob1.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
							ajax("post", "/experiment/fish/fishProTask/delFishProTaskTemplateOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null);
						}else{								
							fishProTaskTemplateGrid.store.removeAll();
						}
					}
					fishProTaskTemplateGrid.store.removeAll();
 				}
				var ob2 = fishProTaskReagentGrid.store;
				if (ob2.getCount() > 0) {
					for(var j=0;j<ob2.getCount();j++){
						var oldv = ob2.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
						ajax("post", "/experiment/fish/fishProTask/delFishProTaskReagentOne.action", {
							ids : oldv
						}, function(data) {
							if (data.success) {
								message(biolims.common.deleteSuccess);
							} else {
								message(biolims.common.deleteFailed);
							}
						}, null); 
						}else{
							fishProTaskReagentGrid.store.removeAll();
						}
					}
					fishProTaskReagentGrid.store.removeAll();
 				}
				var ob3 = fishProTaskCosGrid.store;
				if (ob3.getCount() > 0) {
					for(var j=0;j<ob3.getCount();j++){
						var oldv = ob3.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
							ajax("post", "/experiment/fish/fishProTask/delFishProTaskCosOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null); 
						}else{
							fishProTaskCosGrid.store.removeAll();
						}
					}
					fishProTaskCosGrid.store.removeAll();
 				}
 			 document.getElementById('fishProTask_template').value=rec.get('id');
 			 document.getElementById('fishProTask_template_name').value=rec.get('name');
			 var win = Ext.getCmp('TemplateFun');
			 if(win){win.close();}
				var id = rec.get('id');
				ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = fishProTaskTemplateGrid.getStore().recordType;
							fishProTaskTemplateGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tItem",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("note",obj.note);
								fishProTaskTemplateGrid.getStore().add(p);							
							});
							
							fishProTaskTemplateGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = fishProTaskReagentGrid.getStore().recordType;
							fishProTaskReagentGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tReagent",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("batch",obj.batch);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("oneNum",obj.num);
								p.set("note",obj.note);
								p.set("sn",obj.sn);
								
								fishProTaskReagentGrid.getStore().add(p);							
							});
							
							fishProTaskReagentGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = fishProTaskCosGrid.getStore().recordType;
							fishProTaskCosGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tCos",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("temperature",obj.temperature);
								p.set("speed",obj.speed);
								p.set("time",obj.time);
								p.set("note",obj.note);
								fishProTaskCosGrid.getStore().add(p);							
							});			
							fishProTaskCosGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null);
			 }
			
 		}	
}


//按条件加载原辅料
function showReagent(){
	
	//获取全部数据
	var allRcords=fishProTaskTemplateGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=fishProTaskTemplateGrid.getSelectionModel().getSelections();	
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#fishProTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/fish/fishProTask/showFishProTaskReagentList.action", {
			id : $("#fishProTask_id").val()
		}, "#fishProTaskReagentpage");
	}else if(length1==1){
		fishProTaskReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/fish/fishProTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = fishProTaskReagentGrid.getStore().recordType;
				fishProTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("fishProTask-id",obj.tId);
					p.set("fishProTask-name",obj.tName);
					
					fishProTaskReagentGrid.getStore().add(p);							
				});
				fishProTaskReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.pleaseSelectAPieceOfData);
		return;
	}
	
}

//按条件加载设备
function showCos(){
	//获取全部数据
	var allRcords=fishProTaskTemplateGrid.store;
	var flag="1";
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag="0";
		}else{
			flag="1";
		}
	}
	if(flag=="0"){
		message(biolims.common.pleaseHold);
		return;
	}else{
		//获取选择的数据
		var selectRcords=fishProTaskTemplateGrid.getSelectionModel().getSelections();
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid=$("#fishProTask_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/fish/fishProTask/showFishProTaskCosList.action", {
				id : $("#fishProTask_id").val()
			}, "#fishProTaskCospage");
		}else if(length1==1){
			fishProTaskCosGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/fish/fishProTask/setCos.action", {
				tid:tid,code : code
			}, function(data) {
				
				if (data.success) {	
					var ob = fishProTaskCosGrid.getStore().recordType;
					fishProTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						
						p.set("id",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("sampleNum",obj.sampleNum);
						p.set("time",obj.time);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("tCos",obj.tCos);
						p.set("fishProTask-id",obj.tId);
						p.set("fishProTask-name",obj.tName);
						p.set("note",obj.note);
						fishProTaskCosGrid.getStore().add(p);							
					});
					fishProTaskCosGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
			});		
		}else{
			message(biolims.common.pleaseSelectAPieceOfData);
			return;
		}
	}
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = fishProTaskResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}


			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.common.save
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "FishProTask",id : $("#fishProTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id
		}, function(data) {
			if (data.success) {
				var ob = fishProTaskTemplateGrid.getStore().recordType;
				fishProTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					
					p.set("note",obj.note);
					fishProTaskTemplateGrid.getStore().add(p);							
				});
				
				fishProTaskTemplateGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id
		}, function(data) {
			if (data.success) {	

				var ob = fishProTaskReagentGrid.getStore().recordType;
				fishProTaskReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("oneNum",obj.num);
					p.set("note",obj.note);
					fishProTaskReagentGrid.getStore().add(p);							
				});
				
				fishProTaskReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id
		}, function(data) {
			if (data.success) {	

				var ob = fishProTaskCosGrid.getStore().recordType;
				fishProTaskCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					fishProTaskCosGrid.getStore().add(p);							
				});			
				fishProTaskCosGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
}
