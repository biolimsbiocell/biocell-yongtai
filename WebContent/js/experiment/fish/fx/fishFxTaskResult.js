var fishFxTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'upTime',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'slideCode',
		type:"string"
	});
	   fields.push({
		name:'cellNum',
		type:"string"
	});
	   fields.push({
		name:'lcjy',
		type:"string"
	});
	   fields.push({
		name:'ycbg',
		type:"string"
	});
	    fields.push({
		name:'probeSite',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'resultDetail',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'isCommit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'fishFxTask-id',
		type:"string"
	});
	    fields.push({
		name:'fishFxTask-name',
		type:"string"
	});
	    fields.push({
		name:'reportInfo-id',
		type:"string"
	});
	    fields.push({
		name:'reportInfo-name',
		type:"string"
	});
	    fields.push({
		name:'reportInfoId',
		type:"string"
	});
	    fields.push({
		name:'reportInfoName',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-fileName',
		type:"string"
	});
	    fields.push({
		name:'fileNum',
		type:"string"
	});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		sortable:true,
		width:15*6
	});
	cm.push({
		dataIndex:'slideCode',
		hidden : false,
		header:biolims.common.slideCode,
		width:20*6
	});
	cm.push({
		dataIndex:'probeSite',
		hidden : true,
		header:'使用探针位点数',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:15*6
	});
	cm.push({
		dataIndex:'cellNum',
		hidden : false,
		header:biolims.common.bacteriaCount,
		width:13*6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 120,
		hidden : false,
		header :biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : '附件',
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')){
					rec.set("upTime",(new Date()).toString());
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+ rec.get('id')+"&modelType=fishFxTaskResult' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: '关闭',
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				}else{
					message("请先保存记录。");
				}
			}
		}]
	});
	cm.push({
		dataIndex:'reportInfo-id',
		hidden : true,
		header:'模板ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfo-name',
		hidden : false,
		header:biolims.report.ReportTemplate,
		sortable:true,
		width:15*10
	});	cm.push({
		dataIndex:'reportInfoId',
		hidden : true,
		header:'模板ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfoName',
		hidden : true,
		header:biolims.report.ReportTemplate,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:'附件ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'template-fileName',
		hidden : true,
		header:'附件名称',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isQualified+'<font color="red" size="4">*</font>',
		width:15*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden : true,
		header:'下一步流向id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:biolims.common.nextFlow+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : nextFlowCob
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isCommit',
		hidden : false,
		header:biolims.common.toSubmit+'<font color="red" size="4">*</font>',
		width:15*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result,
		width:30*6,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	var storeexCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '正常' ], [ '0', '异常' ], [ '2', '多态' ] ]
	});
	var exCob = new Ext.form.ComboBox({
		store : storeexCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'ycbg',
		hidden : false,
		header:biolims.common.exceptionReported,
		width:20*6,
		editor : exCob,
		renderer : Ext.util.Format.comboRenderer(exCob)
	});
	cm.push({
		dataIndex:'resultDetail',
		hidden : false,
		header:biolims.common.resultsCommentate,
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex:'lcjy',
		hidden : false,
		header:'临床建议',
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});*/
	var storelcjyCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '门诊随访' ], [ '0', '遗传门诊随访' ] ]
	});
	var lcjyCob = new Ext.form.ComboBox({
		store : storelcjyCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'lcjy',
		hidden : false,
		header:biolims.common.clinicalNote,
		width:20*6,
		editor : lcjyCob,
		renderer : Ext.util.Format.comboRenderer(lcjyCob)
	});
//	cm.push({
//		dataIndex:'ycbg',
//		hidden : false,
//		header:'是否异常报告',
//		width:20*6,
//		editor : exCob,
//		renderer : Ext.util.Format.comboRenderer(exCob)
//	});
//	cm.push({
//		dataIndex:'result',
//		hidden : false,
//		header:'结果',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	var storereCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
//	});
//	var reCob = new Ext.form.ComboBox({
//		store : storereCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});

	
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'upTime',
		hidden : true,
		header:'附件修改时间',
		width:20*10
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishFxTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishFxTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	
	cm.push({
	dataIndex : 'fileNum',
	header : '临时表id',
	hidden : true,
	width : 60
});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/fx/fishFxTask/showFishFxTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.fishAnalysisDetail;
	opts.height =  document.body.clientHeight-138;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/fx/fishFxTask/delFishFxTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
opts.tbar.push({
	text : biolims.common.vim,
	handler : function() {
var records = fishFxTaskResultGrid.getSelectRecord();
if (records.length==1) {
	$.each(records, function(i, obj) {
		if(obj.get("result")!=null && obj.get("result")!=""){
			$("#result1").val(obj.get("result"));
		}
		if(obj.get("resultDetail")!=null && obj.get("resultDetail")!=""){
			$("#resultDetail1").val(obj.get("resultDetail"));
		}
		if(obj.get("lcjy")!=null && obj.get("lcjy")!=""){
			$("#advice1").val(obj.get("lcjy"));
		}
		if(obj.get("ycbg")!=null && obj.get("ycbg")!=""){
			$("#isException1").val(obj.get("ycbg"));
		}
	});
	var options = {};
	options.width = 700;
	options.height = 500;
	loadDialogPage($("#bat_area1_div"), "编辑数据", null, {
		"确定" : function() {
				var result = $("#result1").val();
				var resultDetail = $("#resultDetail1").val();
				var advice = $("#advice1").val();
				var isException = $("#isException1").val();
				fishFxTaskResultGrid.stopEditing();
				$.each(records, function(i, obj) {
					obj.set("result", result);
					obj.set("resultDetail", resultDetail);
					obj.set("lcjy", advice);
					obj.set("ycbg", isException);
				});
				fishFxTaskResultGrid.startEditing(0, 0);
			$(this).dialog("close");
		}
			}, true, options);
		}else if(records.length==0){
			message(biolims.common.selectEditSample);
		}else{
			message(biolims.common.onlyChooseOneSampleEdit);
		}
	}
});
//	opts.tbar.push({
//		text : "批量编辑数据",
//		handler : function() {
//			var records = fishFxTaskResultGrid.getSelectRecord();
//			if (records.length>0) {
//				var options = {};
//				options.width = 700;
//				options.height = 500;
//				loadDialogPage($("#bat_area2_div"), "批量编辑数据", null, {
//					"确定" : function() {
//							var result = $("#result2").val();
//							var resultDetail = $("#resultDetail2").val();
//							var advice = $("#advice2").val();
//							var isException = $("#isException2").val();
//							fishFxTaskResultGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								/*obj.set("result", result);
//								obj.set("resultDetail", resultDetail);
//								obj.set("lcjy", advice);
//								obj.set("ycbg", isException);*/
//								if(result!=null && result!=""){
//									obj.set("result", result);
//								}
//								if(resultDetail!=null && resultDetail!=""){
//									obj.set("resultDetail", resultDetail);
//								}
//								if(advice!=null && advice!=""){
//									obj.set("lcjy", advice);
//								}
//								if(lc!=null && lc!=""){
//									obj.set("ycbg", isException);
//								}
//							});
//							fishFxTaskResultGrid.startEditing(0, 0);
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}else{
//				message("请选择要编辑的样本！");
//			}
//		}
//	});
	opts.tbar.push({
		text :biolims.report.checkFile,
		handler : loadPic
	});
//	opts.tbar.push({
//		text : '选择报告模板',
//		handler : showsampleReportSelectList
//	});
	opts.tbar.push({
		text : biolims.report.checkReport,
		handler : lookReport
	});
//	opts.tbar.push({
//		text : "批量异常报告",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_isReport_div"), "批量报告", null, {
//				"确定" : function() {
//					var records = fishFxTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var isReport = $("#isReport").val();
//						fishFxTaskResultGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("ycbg", isReport);
//						});
//						fishFxTaskResultGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.isQualified,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_isGood_div"), biolims.common.BatchIsQualified, null, {
				"确定" : function() {
					var records = fishFxTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isGood = $("#isGood").val();
						fishFxTaskResultGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isGood", isGood);
						});
						fishFxTaskResultGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "下一步",
//		handler : function() {
//			var records = fishFxTaskResultGrid.getSelectRecord();
//			if(records.length>0){
//					loadTestNextFlowCob();
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});
//	opts.tbar.push({
//		text : "是否提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_isCommit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = fishFxTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var isCommit = $("#isCommit").val();
//						fishFxTaskResultGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("isCommit", isCommit);
//						});
//						fishFxTaskResultGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	fishFxTaskResultGrid=gridEditTable("fishFxTaskResultdiv",cols,loadParam,opts);
	$("#fishFxTaskResultdiv").data("fishFxTaskResultGrid", fishFxTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(fishFxTaskResultGrid);
	var id=$("#fishFxTask_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/fish/fx/fishFxTask/saveFishFxTaskResult.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					fishFxTaskResultGrid.getStore().commitChanges();
					fishFxTaskResultGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);
		}
	}else{
		message(biolims.common.noData2Save);
	}
	  }else{
		  message(biolims.storage.infoChange);
	}
}

function lookReport(){
	var ids=[];
	var idStr="";
	var idStr1="";
	var model="FishFxTaskResult";
	var task="fishFxTaskResult";
	var productId="";
	var selectGrid=fishFxTaskResultGrid.getSelectionModel().getSelections();
	var getGrid=fishFxTaskResultGrid.store;
	if(getGrid.getCount()>0){
		if (selectGrid.length==1) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				idStr+=obj.get("id");
				idStr1+=obj.get("id");
				productId+=obj.get("productId");
			});
			//idStr=ids.substring(0, ids.length-1);
			for ( var i = 0; i < selectGrid.length; i++) {
				if (!selectGrid[i].isNew) {
					ids.push(selectGrid[i].get("id"));
				}
			}
			window.open(window.ctx+"/reports/createReport/loadReport.action?id="
					+idStr+"&taskId="+idStr1+"&model="+model+"&task="+task+"&productId="+productId,'','height=650,width=1050,scrollbars=yes,resizable=yes');
		}else{
			message(biolims.common.pleaseSelectAPieceOfData);
		}
	}else{
		message(biolims.common.notDataList);
	}
}

function loadPic(){
	//var model="fishFxTask";		
	//window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="+$("#fishFxTask_id").val()+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
	var ids="";
	var idStr="";
	var model="fishFxTaskResult";
	var selectGrid=fishFxTaskResultGrid.getSelectionModel().getSelections();
	var getGrid=fishFxTaskResultGrid.store;
	if(getGrid.getCount()>0){
		if (selectGrid.length > 0) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				ids+="'"+obj.get("id")+"',";
			});
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}else{
			//没有勾选明细查看全部附件
			var selectGrid=fishFxTaskResultGrid.store;
			for(var j=0;j<selectGrid.getCount();j++){
				ids+="'"+selectGrid.getAt(j).get("id")+"',";
			}
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}
	}else{
		message(biolims.common.notDataList);
	}
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = fishFxTaskResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, biolims.common.selectNextFlow,  "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=FishFxTask&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = fishFxTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = fishFxTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}

//提交样本
function submitSample(){
	var id=$("#fishFxTask_id").val();
	if(fishFxTaskResultGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var record = fishFxTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(!record[i].get("isCommit")){
				flg=true;
			}
			if(record[i].get("isGood")==""
				|| record[i].get("isGood")==null){
				message(biolims.common.resultsIsEmpty);
				return;
			}
		}
	}else{
		var grid=fishFxTaskResultGrid.store;
		for(var i=0;i<grid.getCount();i++){
			if(grid.getAt(i).get("isCommit")==""){
				flg=true;
			}
			if(grid.getAt(i).get("isGood")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		var records = [];
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		ajax("post", "/experiment/fish/fx/fishFxTask/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				fishFxTaskResultGrid.getStore().commitChanges();
				fishFxTaskResultGrid.getStore().reload();
				message(biolims.common.submitSuccess);
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);
	}else{
		message(biolims.common.noData2Submit);
	}
}

//选择报告模板
function showsampleReportSelectList() {
	var selected=fishFxTaskResultGrid.getSelectionModel().getSelections();
	var productId="";
	var type="1";
	if(selected.length>0){
		if(selected.length>1){
			var productIds = new Array();
			$.each(selected, function(j, k) {
				productIds[j]=k.get("productId");
			});
			for(var i=0;i<selected.length;i++){
				if(i!=0 && productIds[i]!=productIds[i-1]){
					message(biolims.common.testDifferent);
					return;
				}else{
					productId=productIds[i];
				}
			}
		}else{
			$.each(selected, function(i, a) {
				productId=a.get("productId");
			});
		}
	}else{
		productId="";
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
	var showsampleReportSelectList = new Ext.Window(
			{
				id : 'showsampleReportSelectList',
				modal : true,
				title : biolims.common.selectTemplate,
				layout : 'fit',
				width : 480,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sysmanage/report/selReportTemplateList.action?productId="+productId+"&type="+type+"' frameborder='0' width='780' height='500' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						showsampleReportSelectList.close();
					}
				} ]
			});
	showsampleReportSelectList.show();
}

function setReportFun(rec) {
	var selected=fishFxTaskResultGrid.getSelectionModel().getSelections();
	if(selected.length>0){
		$.each(selected,function(i,b){
			b.set("reportInfo-id",rec.get('id'));
			b.set("reportInfo-name",rec.get('name'));
			b.set("template-id",rec.get('attach-id'));
			b.set("template-fileName",rec.get('attach-fileName'));
		});
	}else{
		message("请选择要出报告的样本！");
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
}

//生成并下载报告文件
function createReport(ids) {
	var selected=fishFxTaskResultGrid.getSelectionModel().getSelections();
	var ids = [];
	if(fishFxTaskResultGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	if(selected.length>0){
		var flag=true;
		var id="";//选中的Item的id
		var code="";//选中的样本号
		var fnames="";//样本号不对应的图片
		$.each(selected,function(i,b){
			code=b.get("code");
			id=b.get("id");
		});
		ajax("post", "/common/comsearch/com/compareCode.action", {
			id : id,model:"fishFxTaskResult"
		}, function(data) {
			if (data.success) {
				$.each(data.data, function(i, obj) {
					var str=obj.fileName;
					var scode1 = new Array();
					scode1=str.split("-");
					if(scode1[0].substring(0,scode1[0].length-1)==code){
						flag=true;
					}else{
						flag=false;
						fnames+="【"+obj.fileName+"】,";
					}
				});
			} else {
				message(biolims.sample.pleaseUploadImage2Save);
				return;
			}
		}, null);
		if(fnames==""){
			for ( var i = 0; i < selected.length; i++) {
				if (!selected[i].isNew) {
					ids.push(selected[i].get("id"));
				}
			}
			ajax("post", "/experiment/fish/fx/fishFxTask/createReportFile.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					fishFxTaskResultGrid.getStore().commitChanges();
					fishFxTaskResultGrid.getStore().reload();
					//message("生成报告成功！");
				} else {
					message(biolims.report.previewReportFailed);
				}
			}, null);
			downFiles();
		}else{
			message(biolims.common.sampleNumNotPhoto+fnames);
			return;
		}
	}else{
		message(biolims.report.selectPreview);
		return;
	}
}

//下载文件
function downFiles(){
	var selectGrid=fishFxTaskResultGrid.getSelectionModel().getSelections();
	if(selectGrid.length>0){
		$.each(selectGrid, function(i, obj) {
			var fileName="PDF"+obj.get("code");
			window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'.pdf','','');
		});
	}
//	var selectGrid=fishFxTaskResultGrid.getSelectionModel().getSelections();
//	if(selectGrid.length>0){
//		$.each(selectGrid, function(i, obj) {
//			ajax("post", "/common/comsearch/com/downloadReportFile.action", {
//				id : obj.get("id"),model:"fishFxTaskResult"
//			}, function(data) {
//				if (data.success) {
//					var num=data.num;
//					var fileName="PDF"+obj.get("code");
//					if(num>0){
//						if(num<=2){
//							window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'-1.pdf','','');
//							//message("下载成功，请打开预览！");
//						}else{
//							var num1=(num-2)%4;
//							for(var i=0;i<num1+1;i++){
//								window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'-'+(i+1)+'.pdf','','');
//							}
//						}
//					}
//				} else {
//					message("预览报告失败！");
//				}
//			}, null);
//		});
//	}
}