$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#fishFxTask_state").val();
	if(id=="3"){
		load("/experiment/fish/fx/fishFxTask/showFishFxTaskTempList.action", null, "#fishFxTaskTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#fishFxTaskTempPage").remove();
	}
});	
function add() {
	window.location = window.ctx + "/experiment/fish/fx/fishFxTask/editFishFxTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/fish/fx/fishFxTask/showFishFxTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});	
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("FishFxTask", {
					userId : userId,
					userName : userName,
					formId : $("#fishFxTask_id").val(),
					title : $("#fishFxTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	//var taskName=$("#taskName").val();
	//if(taskName=="Fish二审"){
		if (fishFxTaskResultGrid.getAllRecord().length > 0) {
			var selRecord = fishFxTaskResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("isCommit");
				if(oldv!=1){
					message(biolims.common.sampleUncommited);
					return;
				}
				if(selRecord.getAt(j).get("isGood")==""){
					message(biolims.common.isQualifiedNotNull);
					return;
				}
				/*if(selRecord.getAt(j).get("nextFlow")==""){
					message("请填写下一步！");
					return;
				}*/
			}
			if(fishFxTaskResultGrid.getModifyRecord().length > 0){
				message(biolims.common.pleaseSaveRecord);
				return;
			}
			completeTask($("#fishFxTask_id").val(), $(this).attr("taskId"), function() {
				document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/dashboard/toDashboard.action';
			});
		}else{
			completeTask($("#fishFxTask_id").val(), $(this).attr("taskId"), function() {
				document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/dashboard/toDashboard.action';
			});
		}
		
	//}else{
	//	completeTask($("#fishFxTask_id").val(), $(this).attr("taskId"), function() {
	//		document.getElementById('toolbarSaveButtonFlag').value = 'save';
	//		location.href = window.ctx + '/dashboard/toDashboard.action';
	//	});
	//	}
});






function save() {
if(checkSubmit()==true){
	    var fishFxTaskResultDivData = $("#fishFxTaskResultdiv").data("fishFxTaskResultGrid");
		document.getElementById('fishFxTaskResultJson').value = commonGetModifyRecords(fishFxTaskResultDivData);
//	    var fishFxTaskTempDivData = $("#fishFxTaskTempdiv").data("fishFxTaskTempGrid");
//		document.getElementById('fishFxTaskTempJson').value = commonGetModifyRecords(fishFxTaskTempDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/fish/fx/fishFxTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/fish/fx/fishFxTask/copyFishFxTask.action?id=' + $("#fishFxTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#fishFxTask_id").val() + "&tableId=fishFxTask");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#fishFxTask_id").val()){
		commonChangeState("formId=" + $("#fishFxTask_id").val() + "&tableId=FishFxTask");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#fishFxTask_id").val());
	nsc.push(biolims.user.NoNull);
	fs.push($("#fishFxTask_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.fishAnalysis,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/fish/fx/fishFxTask/showFishFxTaskResultList.action", {
				id : $("#fishFxTask_id").val()
			}, "#fishFxTaskResultpage");
load("/experiment/fish/fx/fishFxTask/showFishFxTaskTempList.action", {
				id : $("#fishFxTask_id").val()
			}, "#fishFxTaskTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
			    	text:biolims.common.copy
					});
item.on('click', editCopy);

//选择上机阅片
function FishSjTaskFun(){
	var win = Ext.getCmp('FishSjTaskFun');
	if (win) {win.close();}
	var FishSjTaskFun= new Ext.Window({
	id:'FishSjTaskFun',modal:true,title:'',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/experiment/fish/fishSjTask/fishSjTaskSelect.action?flag=FishSjTaskFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: 'close',
	 handler: function(){
	FishSjTaskFun.close(); }  }]  });
	FishSjTaskFun.show();
}
function setFishSjTaskFun(rec){
	document.getElementById('fishFxTask_acceptUser').value=rec.get('acceptUser-id');
	document.getElementById('fishFxTask_acceptUser_name').value=rec.get('acceptUser-name');
	var code=$("#fishFxTask_fishSjTask").val();
	if(code==""){
		document.getElementById('fishFxTask_fishSjTask').value=rec.get('id');
		document.getElementById('fishFxTask_fishSjTask_name').value=rec.get('name');
		var win = Ext.getCmp('FishSjTaskFun');
		if(win){win.close();}
		var id=rec.get('id');
		ajax("post", "/experiment/fish/fishSjTask/setResultToFishFx.action", {
			code : id,
			}, function(data) {
				if (data.success) {
					var ob = fishFxTaskResultGrid.getStore().recordType;
					fishFxTaskResultGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("code",obj.code);
						p.set("sampleCode",obj.sampleCode);
						p.set("slideCode",obj.slideCode);
						p.set("productId",obj.productId);
						p.set("productName",obj.productName);
						p.set("note",obj.note);
						fishFxTaskResultGrid.getStore().add(p);							
					});
					fishFxTaskResultGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null); 
	}else{
		if(rec.get('id')==code){
			var win = Ext.getCmp('FishSjTaskFun');
			if(win){win.close();}
		}else{
			var ob1 = fishFxTaskResultGrid.store;
			if (ob1.getCount() > 0) {
			for(var j=0;j<ob1.getCount();j++){
				var oldv = ob1.getAt(j).get("id");
				//根据ID删除
				if(oldv!=null){
					ajax("post", "/experiment/fish/fx/fishFxTask/delFishFxTaskResultOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message(biolims.common.deleteSuccess);
						} else {
							message(biolims.common.deleteFailed);
						}
					}, null);
				}else{								
					fishFxTaskResultGrid.store.removeAll();
				}
			}
				fishFxTaskResultGrid.store.removeAll();
			}
			document.getElementById('fishFxTask_fishSjTask').value=rec.get('id');
			document.getElementById('fishFxTask_fishSjTask_name').value=rec.get('name');
			var win = Ext.getCmp('FishSjTaskFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/experiment/fish/fishSjTask/setResultToFishFx.action", {
				code : id,
				}, function(data) {
					if (data.success) {
						var ob = fishFxTaskResultGrid.getStore().recordType;
						fishFxTaskResultGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.code);
							p.set("sampleCode",obj.sampleCode);
							p.set("slideCode",obj.slideCode);
							p.set("productId",obj.productId);
							p.set("productName",obj.productName);
							p.set("note",obj.note);
							fishFxTaskResultGrid.getStore().add(p);							
						});
						fishFxTaskResultGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
		}
	}
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = fishFxTaskResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}

			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.common.save
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "FishFxTask",id : $("#fishFxTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

