var fishFxTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'slideCode',
		type:"string"
	});
	//缴费状态
	fields.push({
		name:'chargeNote',
		type:"string"
	});
	fields.push({
		name:'patientName',
		type:"string"
	});
	fields.push({
		name:'reason',
		type:"string"
	});
	fields.push({
	   name:'gender',
	   type:"string"
	});
		fields.push({
	   name:'hospital',
	   type:"string"
	});
   		fields.push({
	   name:'doctor',
	   type:"string"
	});
   		fields.push({
	   name:'cellNum',
	   type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	
	cm.push({
		dataIndex:'slideCode',
		hidden : false,
		header:biolims.common.slideCode,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.user.patientName,
		width:20*6
	});
	var storegenderCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.male ], [ '0', biolims.common.female] ]
	});
	var genderCob = new Ext.form.ComboBox({
		store : storegenderCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'gender',
		hidden : false,
		header:biolims.common.gender,
		width:20*6,
		//editor : genderCob,
		renderer : Ext.util.Format.comboRenderer(genderCob)
	});
	cm.push({
		dataIndex:'hospital',
		hidden : false,
		header:biolims.master.hospitalName,
		width:30*6
	});
	cm.push({
		dataIndex:'doctor',
		hidden : false,
		header:biolims.sample.crmDoctorName,
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.causeInspection,
		width:30*6
	});
	
	var storechargeNoteCob = new Ext.data.ArrayStore({
		fields:['id','name'],
		data:[ ['1','待缴费'],['2','已缴费'],['3','待结算'],['4','科研'],['5','免费']]
	});
	var chargeNoteCob = new Ext.form.ComboBox({
		store:storechargeNoteCob,
		displayField:'name',
		valueField:'id',
		mode:'local'
	});
	cm.push({
		dataIndex:'chargeNote',
		hidden:false,
		header:biolims.common.payStatus,
		width:20*6,
		renderer:Ext.util.Format.comboRenderer(chargeNoteCob)
	});
	cm.push({
		dataIndex:'cellNum',
		hidden : false,
		header:biolims.common.bacteriaCount,
		width:30*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/fx/fishFxTask/showFishFxTaskTempListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.fishAnalysisTemporaryTable;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/fx/fishFxTask/delFishFxTaskTemp.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$(".jquery-ui-warning").html(biolims.common.longlongagolong1);
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = fishFxTaskTempGrid.getAllRecord();
							var store = fishFxTaskTempGrid.store;

							var isOper = true;
							var buf = [];
							fishFxTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							fishFxTaskTempGrid.getSelectionModel().selectRows(buf);
							if(isOper==false){
								message(biolims.common.samplecodeComparison);
								
							}else{
								addItem();
							}
							fishFxTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	fishFxTaskTempGrid=gridEditTable("fishFxTaskTempdiv",cols,loadParam,opts);
	$("#fishFxTaskTempdiv").data("fishFxTaskTempGrid", fishFxTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//添加任务到子表
function addItem(){
	var selectRecord=fishFxTaskTempGrid.getSelectionModel();
	var selRecord=fishFxTaskResultGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("fileNum");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					return;					
				}
			}
			if(!isRepeat){
			var ob = fishFxTaskResultGrid.getStore().recordType;
			fishFxTaskResultGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("fileNum",obj.get("id"));
			p.set("code",obj.get("code"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("state","1");
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("note",obj.get("note"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("slideCode",obj.get("slideCode"));
			p.set("cellNum",obj.get("cellNum"));
			p.set("isGood","1");
			ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
				model : "FishFxTask",productId:obj.get("productId")
			}, function(data) {
				p.set("nextFlowId",data.dnextId);
				p.set("nextFlow",data.dnextName);
			}, null);
			fishFxTaskResultGrid.getStore().add(p);
			fishFxTaskResultGrid.startEditing(0, 0);
			
		}
			
	});
	}else{
		message(biolims.common.pleaseChooseSamples);
	}
	
}
