﻿var samplePreTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'testUser-id',
		type:"string"
	});
	   fields.push({
		name:'testUser-name',
		type:"string"
	});
   fields.push({
		name:'tItem',
		type:"string"
	});
   fields.push({
		name:'startTime',
		type:"string"
	});
   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'state',
			type:"string"
	});
	   fields.push({
			name:'codes',
			type:"string"
	});
	    fields.push({
		name:'samplePreTask-id',
		type:"string"
	});
	    fields.push({
		name:'samplePreTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'步骤id',
		width:20*6
	});
	
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	cm.push({
		dataIndex:'testUser-id',
		hidden : true,
		header:'实验员id',
		width:20*6,
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		loadTestUser();
	});
	cm.push({
		dataIndex:'testUser-name',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*6,
		
		editor : testUser
	});
	cm.push({
		dataIndex:'startTime',
		header:biolims.common.startTime,
		width:20*6
	});
	cm.push({
		dataIndex:'endTime',
		header:biolims.common.endTime,
		width:20*6
	});

	cm.push({
		dataIndex:'codes',
		hidden : false,
		header:biolims.common.relateSample,
		width:40*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'samplePreTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10
	});
	cm.push({
		dataIndex:'samplePreTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/samplePreTask/showSamplePreTaskTemplateListJson.action?id="+$("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.templateDetail;
	opts.height =  document.body.clientHeight-220;
	opts.tbar = [];
	var state=$("#samplePreTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/samplePreTask/delSamplePreTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				samplePreTaskTemplateGrid.getStore().commitChanges();
				samplePreTaskTemplateGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_print',
		text :biolims.common.printList,
		handler : stampOrder
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : biolims.common.fillDetail,
		handler : templateSelect
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text :biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
}
	samplePreTaskTemplateGrid=gridEditTable("samplePreTaskTemplatediv",cols,loadParam,opts);
	$("#samplePreTaskTemplatediv").data("samplePreTaskTemplateGrid",samplePreTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");	
});

//打印执行单
function stampOrder(){
	var id=$("#samplePreTask_template").val();
	if(id==""){
		message(biolims.common.pleaseSelectTemplate);
		return;
	}else{
		var url = '__report=SamplePreTask.rptdesign&id=' + $("#samplePreTask_id").val();
		commonPrint(url);}
		//window.open(window.ctx+"/system/template/template/openTemplate.action?id="+id,'','height=768,width=1366,scrollbars=yes,resizable=yes');
}
	
//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=samplePreTaskTemplateGrid.getSelectionModel();
	var setNum = samplePreTaskReagentGrid.store;
	var selectRecords = samplePreTaskItemGrid.getSelectionModel();
	if(selectRecords.getSelections().length > 0){
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
				for(var i=0; i<setNum.getCount();i++){
					var num = setNum.getAt(i).get("itemId");
					if(num==obj.get("code")){
						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
					}
				}
			});
		}else{
			message(biolims.common.selectStepNum);
		}
		
		//var selectRecord=samplePreTaskItemGrid.getSelectionModel();
		var selRecord=samplePreTaskTemplateGrid.getSelectRecord();
		var codes = "";
		$.each(selectRecords.getSelections(), function(i, obj) {
			codes += obj.get("code")+",";
		});
		$.each(selRecord,function(i,obj){
			obj.set("codes",codes);
		});
	}else{
		message(biolims.common.pleaseSelectSamples);
		return;
	}
	
}

//获取停止时的时间
function getEndTime(){
	var setRecord=samplePreTaskItemGrid.store;
	var getIndex = samplePreTaskTemplateGrid.store;
	var getIndexs = samplePreTaskTemplateGrid.getSelectionModel().getSelections();
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=samplePreTaskTemplateGrid.getSelectionModel();
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			if(obj.get("startTime")!=undefined && obj.get("startTime")!=""){
				obj.set("endTime",str);
				var codes = obj.get("codes");
				var scode = new Array();
				scode = codes.split(",");
				
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(scode[j]==setRecord.getAt(i).get("code")){
							setRecord.getAt(i).set("stepNum",obj.get("code"));
						}
					}
				}
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("codes",codes);
			}else{
				message("请先开始实验！");
				return;
			}
		});
	}else{
		message(biolims.common.selectStepNum);
	}
}

function addSuccess(){
	//选中的item数据
	var getRecord = samplePreTaskItemGrid.getSelectionModel().getSelections();
	//选中的template数据
	var selectRecord = samplePreTaskTemplateGrid.getSelectionModel().getSelections();
	//template明细的所有数据
	var getTemplateAll=samplePreTaskTemplateGrid.store;
	//item明细的所有数据
	var getItemAll=samplePreTaskItemGrid.store;
	//result的所有数据
	var getResultAll=samplePreTaskResultGrid.store;
	var isNull=false;
	var isNull1=false;
	var isNull2=false;
	if(getItemAll.getCount()>0){
		for(var h=0;h<getItemAll.getCount();h++){
			var tid= getItemAll.getAt(h).get("dicSampleType-id");
			if(tid == null || tid == ""){
				isNull2 = true;
				message("请选择中间产物类型！");
				break;					
			}
		}
		for(var i=0;i<getItemAll.getCount();i++){
			var num=getItemAll.getAt(i).get("productNum");
			if(num==null || num=="" || num==0){
				isNull1=true;
				message("请填写中间产物数量！");
				break;
			}
		}
//		for(var h=0;h<getTemplateAll.getCount();h++){
//			var nulls = getTemplateAll.getAt(h).get("endTime");
//			if(nulls==null || nulls == ""){
//				isNull = true;
//				message("有未做实验的步骤！");
//				break;					
//			}
//		}
		if(isNull==false && isNull1==false && isNull2==false){
			if(selectRecord.length==0){
				//如果没有选中实验步骤，默认所有明细都生成结果
				var isCF=false;
				if(getResultAll.getCount()>0){
					for(var i=0; i<getItemAll.getCount(); i++){
						for(var j=0;j<getResultAll.getCount();j++){
							var itemCode = getItemAll.getAt(i).get("code");
							var infoCode = getResultAll.getAt(j).get("zjCode");
							if(itemCode == infoCode){
								isCF = true;
								message(biolims.common.haveDuplicate);
								break;					
							}
						}
					}
					if(isCF==false){
						toInfoData(getItemAll);
					}
				}else{
					toInfoData(getItemAll);
				}
			}else if(selectRecord.length==1){
				$.each(selectRecord, function(i, obj) {
					var isRepeat = true;
					var codes = obj.get("codes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i1=0; i1<scode.length; i1++){
						for(var j1=0;j1<getResultAll.getCount();j1++){
							var getv = scode[i1];
							var setv = getResultAll.getAt(j1).get("zjCode");
							if(getv == setv){
								isRepeat = false;
								message(biolims.common.haveDuplicate);
								break;					
							}
						}
					}
					if(isRepeat){
						for(var i=0; i<scode.length; i++){
							for(var j=0; j<getItemAll.getCount(); j++){
								if(scode[i]==getItemAll.getAt(j).get("code")){
									for(var k=1;k<=getItemAll.getAt(j).get("productNum");k++){
										var ob = samplePreTaskResultGrid.getStore().recordType;
										samplePreTaskResultGrid.stopEditing();
										var p = new ob({});
										p.isNew = true;
										p.set("tempId",getItemAll.getAt(j).get("tempId"));
										p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
										p.set("zjCode",getItemAll.getAt(j).get("code"));
										p.set("dicSampleType-id",getItemAll.getAt(j).get("dicSampleType-id"));
										p.set("dicSampleType-name",getItemAll.getAt(j).get("dicSampleType-name"));
										p.set("state",getItemAll.getAt(j).get("state"));
										p.set("productId",getItemAll.getAt(j).get("productId"));
										p.set("productName",getItemAll.getAt(j).get("productName"));
										p.set("patientName",getItemAll.getAt(j).get("patientName"));
										p.set("idCard",getItemAll.getAt(j).get("idCard"));
										p.set("sequencingFun",getItemAll.getAt(j).get("sequencingFun"));
										p.set("inspectDate",getItemAll.getAt(j).get("inspectDate"));
										p.set("acceptDate",getItemAll.getAt(j).get("acceptDate"));
										p.set("reportDate",getItemAll.getAt(j).get("reportDate"));
										p.set("reason",getItemAll.getAt(j).get("reason"));
										p.set("concentration",getItemAll.getAt(j).get("concentration"));
										p.set("volume",getItemAll.getAt(j).get("volume"));
										p.set("orderId",getItemAll.getAt(j).get("orderId"));
										p.set("phone",getItemAll.getAt(j).get("phone"));
										p.set("classify",getItemAll.getAt(j).get("classify"));
										p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
										p.set("result","1");
										ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
											model : "SamplePreTask",productId:getItemAll.getAt(j).get("productId")
										}, function(data) {
											p.set("nextFlowId",data.dnextId);
											p.set("nextFlow",data.dnextName);
										}, null);
										message(biolims.common.generateResultsSuccess);
										samplePreTaskResultGrid.getStore().add(p);
										samplePreTaskResultGrid.startEditing(0,0);
									}
								}
							}
						}
					}
				});
			}else if(selectRecord.length>1){
				message("请不要勾选多个步骤！");
				return;
			}
		}
	}else{
		message(biolims.common.addTaskSample);
		return;
	}
}
//向Info页面传值
function toInfoData(grid){
	for(var i=0;i<grid.getCount();i++){
		var productNum=grid.getAt(i).get("productNum");
		for(var k=1;k<=productNum;k++){
			var ob = samplePreTaskResultGrid.getStore().recordType;
			samplePreTaskResultGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("tempId",grid.getAt(i).get("tempId"));
			p.set("sampleCode",grid.getAt(i).get("sampleCode"));
			p.set("zjCode",grid.getAt(i).get("code"));
			p.set("dicSampleType-id",grid.getAt(i).get("dicSampleType-id"));
			p.set("dicSampleType-name",grid.getAt(i).get("dicSampleType-name"));
			p.set("state",grid.getAt(i).get("state"));
			p.set("productId",grid.getAt(i).get("productId"));
			p.set("productName",grid.getAt(i).get("productName"));
			p.set("patientName",grid.getAt(i).get("patientName"));
			p.set("idCard",grid.getAt(i).get("idCard"));
			p.set("sequencingFun",grid.getAt(i).get("sequencingFun"));
			p.set("inspectDate",grid.getAt(i).get("inspectDate"));
			p.set("acceptDate",grid.getAt(i).get("acceptDate"));
			p.set("reportDate",grid.getAt(i).get("reportDate"));
			p.set("reason",grid.getAt(i).get("reason"));
			p.set("concentration",grid.getAt(i).get("concentration"));
			p.set("volume",grid.getAt(i).get("volume"));
			p.set("orderId",grid.getAt(i).get("orderId"));
			p.set("phone",grid.getAt(i).get("phone"));
			p.set("classify",grid.getAt(i).get("classify"));
			p.set("sampleType",grid.getAt(i).get("sampleType"));
			p.set("result","1");
			ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
				model : "SamplePreTask",productId:grid.getAt(i).get("productId")
			}, function(data) {
				p.set("nextFlowId",data.dnextId);
				p.set("nextFlow",data.dnextName);
			}, null);
			message(biolims.common.generateResultsSuccess);
			samplePreTaskResultGrid.getStore().add(p);
			samplePreTaskResultGrid.startEditing(0,0);
		}
	}
}
//选择实验步骤
function templateSelect(){
	var option = {};
	option.width = 605;
	option.height = 558;
	loadDialogPage(null, biolims.common.chooseExperimentalSteps, "/experiment/fish/samplePreTask/showTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
		"确定" : function() {
			var operGrid = $("#template_wait_grid_div").data("grid");
			var ob = samplePreTaskTemplateGrid.getStore().recordType;
			samplePreTaskTemplateGrid.stopEditing();
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("code", obj.get("code"));
					p.set("name", obj.get("name"));
					samplePreTaskTemplateGrid.getStore().add(p);
					samplePreTaskTemplateGrid.startEditing(0, 0);
				});
				$(this).dialog("close");
				$(this).dialog("remove");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
		}
	}, true, option);

	
}
//查询实验员
function loadTestUser(){
	var win = Ext.getCmp('loadTestUser');
	if (win) {win.close();}
	var loadTestUser= new Ext.Window({
	id:'loadTestUser',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=loadTestUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 loadTestUser.close(); }  }]  });     loadTestUser.show(); }
	function setloadTestUser(id,name){
		var gridGrid = $("#samplePreTaskTemplatediv").data("samplePreTaskTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('testUser-id',id);
			obj.set('testUser-name',name);
		});
		var win = Ext.getCmp('loadTestUser');
		if(win){
			win.close();
		}
	}
