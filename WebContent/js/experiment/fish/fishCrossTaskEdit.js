﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
	var state = $("#fishCrossTask_state").val();
	if(state =="3"){
		load("/experiment/fish/fishCrossTask/showFishCrossTasktempList.action", null, "#fishCrossTaskTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showAcceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
	}
	setTimeout(function() {
		var getGrid=fishCrossTaskTemplateGrid.store;
		if(getGrid.getCount()==0){
			loadTemplate($("#fishCrossTask_template").val());
		}
	}, 2000);
});
function add() {
	window.location = window.ctx + "/experiment/fish/fishCrossTask/editFishCrossTask.action?maxNum="+$("#fishCrossTask_maxNum").val();
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/fish/fishCrossTask/showFishCrossTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("FishCrossTask", {
					userId : userId,
					userName : userName,
					formId : $("#fishCrossTask_id").val(),
					title : $("#fishCrossTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {

	if (fishCrossTaskResultGrid.getAllRecord().length > 0) {
		var selRecord = fishCrossTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			if(selRecord.getAt(j).get("result")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
			
		}
		if(fishCrossTaskResultGrid.getModifyRecord().length > 0){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
		completeTask($("#fishCrossTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});


function save() {
	var code=$("#fishCrossTask_template").val();
	if(code==""||code==null){
		message(biolims.common.selectTaskModel);
		return;
	}
if(checkSubmit()==true){    Ext.MessageBox.show({ msg:biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });   
	    var fishCrossTaskItemDivData = $("#fishCrossTaskItemdiv").data("fishCrossTaskItemGrid");
		document.getElementById('fishCrossTaskItemJson').value = commonGetModifyRecords(fishCrossTaskItemDivData);
	    var fishCrossTaskResultDivData = $("#fishCrossTaskResultdiv").data("fishCrossTaskResultGrid");
		document.getElementById('fishCrossTaskResultJson').value = commonGetModifyRecords(fishCrossTaskResultDivData);
		var fishCrossTaskTemplateDivData = $("#fishCrossTaskTemplatediv").data("fishCrossTaskTemplateGrid");
		document.getElementById('fishCrossTaskTemplateJson').value = commonGetModifyRecords(fishCrossTaskTemplateDivData);
		var fishCrossTaskReagentDivData = $("#fishCrossTaskReagentdiv").data("fishCrossTaskReagentGrid");
		document.getElementById('fishCrossTaskReagentJson').value = commonGetModifyRecords(fishCrossTaskReagentDivData);
		var fishCrossTaskCosDivData = $("#fishCrossTaskCosdiv").data("fishCrossTaskCosGrid");
		document.getElementById('fishCrossTaskCosJson').value = commonGetModifyRecords(fishCrossTaskCosDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/fish/fishCrossTask/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/fish/fishCrossTask/copyFishCrossTask.action?id=' + $("#fishCrossTask_id").val();
}

$("#toolbarbutton_status").click(function(){
//	var bloodInfoGrid = fishCrossTaskResultGrid.getStore();
//	var bloodItemGrid = fishCrossTaskItemGrid.getStore();
//	var num = 0;
//	for(var i= 0; i<bloodItemGrid.getCount();i++){
//		for(var j=0;j<bloodInfoGrid.getCount();j++){
//			if(bloodItemGrid.getAt(j).get("sampleCode")==bloodInfoGrid.getAt(i).get("sampleCode")){
//				j=bloodInfoGrid.getCount();
//			}else{
//				num=num+1;
//				if(num==bloodInfoGrid.getCount()){
//					num=0;
//					message("请完成实验！");
//					return;
//				}
//			}
////			if(bloodItemGrid.getAt(j).get("sampleCode")!=bloodInfoGrid.getAt(i).get("sampleCode")){
////				message("请完成实验！");
////				return;
////			}
//		}
//	}

	commonChangeState("formId=" + $("#fishCrossTask_id").val() + "&tableId=FishCrossTask");
});
//setTimeout(function() {
//	if($("#fishCrossTask_template").val()){
//		var maxNum = $("#fishCrossTask_maxNum").val();
//		if(maxNum>0){
//				load("/storage/container/sampleContainerTest.action", {
//					id : $("#fishCrossTask_template").val(),
//					type :$("#type").val(),
//					maxNum : 0
//				}, "#gridContainerdiv0", function(){
//					if(maxNum>1){
//					load("/storage/container/sampleContainerTest.action", {
//						id : $("#fishCrossTask_template").val(),
//						type :$("#type").val(),
//						maxNum : 1
//					}, "#gridContainerdiv1", null)
//					}
//				});
//			
//		}
//	}
//}, 100);
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#fishCrossTask_id").val());
	nsc.push(biolims.user.NoNull);
	fs.push($("#fishCrossTask_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.hybridsElution,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/fish/fishCrossTask/showFishCrossTaskItemList.action", {
	id : $("#fishCrossTask_id").val()
}, "#fishCrossTaskItempage");
load("/experiment/fish/fishCrossTask/showFishCrossTaskTemplateList.action", {
	id : $("#fishCrossTask_id").val()
}, "#fishCrossTaskTemplatepage");
load("/experiment/fish/fishCrossTask/showFishCrossTaskReagentList.action", {
	id : $("#fishCrossTask_id").val()
}, "#fishCrossTaskReagentpage");
load("/experiment/fish/fishCrossTask/showFishCrossTaskCosList.action", {
	id : $("#fishCrossTask_id").val()
}, "#fishCrossTaskCospage");
load("/experiment/fish/fishCrossTask/showFishCrossTaskResultList.action", {
	id : $("#fishCrossTask_id").val()
}, "#fishCrossTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);

//调用模板
function TemplateFun(){
		var type="doFishCross";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'Select Template',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: 'close',
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
function setTemplateFun(rec){
	var duringDays=rec.get('duringDays');
	var remindDays=rec.get('remindDays');
	document.getElementById('fishCrossTask_acceptUser').value=rec.get('acceptUser-id');
	document.getElementById('fishCrossTask_acceptUser_name').value=rec.get('acceptUser-name');
	var createDate=$('#fishCrossTask_createDate').val();
	createDate=new Date(createDate);
	var date = new Date(createDate.getTime() + 24*60*60*1000*duringDays);  
	var result = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
	var code=$("#fishCrossTask_template").val();
		if(code==""){
			document.getElementById('fishCrossTask_template').value=rec.get('id');
			document.getElementById('fishCrossTask_template_name').value=rec.get('name');
			var win = Ext.getCmp('TemplateFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/system/template/template/setTemplateItem.action", {
				code : id
				}, function(data) {
					if (data.success) {
						var ob = fishCrossTaskTemplateGrid.getStore().recordType;
						fishCrossTaskTemplateGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tItem",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							
							p.set("note",obj.note);
							fishCrossTaskTemplateGrid.getStore().add(p);							
						});
						
						fishCrossTaskTemplateGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateReagent.action", {
				code : id
				}, function(data) {
					if (data.success) {	

						var ob = fishCrossTaskReagentGrid.getStore().recordType;
						fishCrossTaskReagentGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tReagent",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							p.set("batch",obj.batch);
							p.set("isGood",obj.isGood);
							p.set("itemId",obj.itemId);
							
							p.set("oneNum",obj.num);
							p.set("note",obj.note);
							fishCrossTaskReagentGrid.getStore().add(p);							
						});
						
						fishCrossTaskReagentGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateCos.action", {
				code : id
				}, function(data) {
					if (data.success) {	

						var ob = fishCrossTaskCosGrid.getStore().recordType;
						fishCrossTaskCosGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tCos",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							p.set("isGood",obj.isGood);
							p.set("itemId",obj.itemId);
							
							p.set("temperature",obj.temperature);
							p.set("speed",obj.speed);
							p.set("time",obj.time);
							p.set("note",obj.note);
							fishCrossTaskCosGrid.getStore().add(p);							
						});			
						fishCrossTaskCosGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
 				var ob1 = fishCrossTaskTemplateGrid.store;
 				if (ob1.getCount() > 0) {
					for(var j=0;j<ob1.getCount();j++){
						var oldv = ob1.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
							ajax("post", "/experiment/fish/fishCrossTask/delFishCrossTaskTemplateOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null);
						}else{								
							fishCrossTaskTemplateGrid.store.removeAll();
						}
					}
					fishCrossTaskTemplateGrid.store.removeAll();
 				}
				var ob2 = fishCrossTaskReagentGrid.store;
				if (ob2.getCount() > 0) {
					for(var j=0;j<ob2.getCount();j++){
						var oldv = ob2.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
						ajax("post", "/experiment/fish/fishCrossTask/delFishCrossTaskReagentOne.action", {
							ids : oldv
						}, function(data) {
							if (data.success) {
								message(biolims.common.deleteSuccess);
							} else {
								message(biolims.common.deleteFailed);
							}
						}, null); 
						}else{
							fishCrossTaskReagentGrid.store.removeAll();
						}
					}
					fishCrossTaskReagentGrid.store.removeAll();
 				}
				var ob3 = fishCrossTaskCosGrid.store;
				if (ob3.getCount() > 0) {
					for(var j=0;j<ob3.getCount();j++){
						var oldv = ob3.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
							ajax("post", "/experiment/fish/fishCrossTask/delFishCrossTaskCosOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null); 
						}else{
							fishCrossTaskCosGrid.store.removeAll();
						}
					}
					fishCrossTaskCosGrid.store.removeAll();
 				}
 			 document.getElementById('fishCrossTask_template').value=rec.get('id');
 			 document.getElementById('fishCrossTask_template_name').value=rec.get('name');
			 var win = Ext.getCmp('TemplateFun');
			 if(win){win.close();}
				var id = rec.get('id');
				ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = fishCrossTaskTemplateGrid.getStore().recordType;
							fishCrossTaskTemplateGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tItem",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("note",obj.note);
								fishCrossTaskTemplateGrid.getStore().add(p);							
							});
							
							fishCrossTaskTemplateGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = fishCrossTaskReagentGrid.getStore().recordType;
							fishCrossTaskReagentGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tReagent",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("batch",obj.batch);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("oneNum",obj.num);
								p.set("note",obj.note);
								p.set("sn",obj.sn);
								
								fishCrossTaskReagentGrid.getStore().add(p);							
							});
							
							fishCrossTaskReagentGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = fishCrossTaskCosGrid.getStore().recordType;
							fishCrossTaskCosGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tCos",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("temperature",obj.temperature);
								p.set("speed",obj.speed);
								p.set("time",obj.time);
								p.set("note",obj.note);
								fishCrossTaskCosGrid.getStore().add(p);							
							});			
							fishCrossTaskCosGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null);
			 }
			
 		}	
}


//按条件加载原辅料
function showReagent(){
	
	//获取全部数据
	var allRcords=fishCrossTaskTemplateGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=fishCrossTaskTemplateGrid.getSelectionModel().getSelections();	
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#fishCrossTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/fish/fishCrossTask/showFishCrossTaskReagentList.action", {
			id : $("#fishCrossTask_id").val()
		}, "#fishCrossTaskReagentpage");
	}else if(length1==1){
		fishCrossTaskReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/fish/fishCrossTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = fishCrossTaskReagentGrid.getStore().recordType;
				fishCrossTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("fishCrossTask-id",obj.tId);
					p.set("fishCrossTask-name",obj.tName);
					
					fishCrossTaskReagentGrid.getStore().add(p);							
				});
				fishCrossTaskReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.pleaseSelectAPieceOfData);
		return;
	}
	
}

//按条件加载设备
function showCos(){
	//获取全部数据
	var allRcords=fishCrossTaskTemplateGrid.store;
	var flag="1";
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag="0";
		}else{
			flag="1";
		}
	}
	if(flag=="0"){
		message(biolims.common.pleaseHold);
		return;
	}else{
		//获取选择的数据
		var selectRcords=fishCrossTaskTemplateGrid.getSelectionModel().getSelections();
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid=$("#fishCrossTask_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/fish/fishCrossTask/showFishCrossTaskCosList.action", {
				id : $("#fishCrossTask_id").val()
			}, "#fishCrossTaskCospage");
		}else if(length1==1){
			fishCrossTaskCosGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/fish/fishCrossTask/setCos.action", {
				tid:tid,code : code
			}, function(data) {
				
				if (data.success) {	
					var ob = fishCrossTaskCosGrid.getStore().recordType;
					fishCrossTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						
						p.set("id",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("sampleNum",obj.sampleNum);
						p.set("time",obj.time);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("tCos",obj.tCos);
						p.set("fishCrossTask-id",obj.tId);
						p.set("fishCrossTask-name",obj.tName);
						p.set("note",obj.note);
						fishCrossTaskCosGrid.getStore().add(p);							
					});
					fishCrossTaskCosGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
			});		
		}else{
			message(biolims.common.pleaseSelectAPieceOfData);
			return;
		}
	}
}


Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : $("#fishCrossTask_id").val(), nextFlowId : "FishCrossTask"
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
						} else {
							message("回滚失败！");
						}
					}, null);
				
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "FishCrossTask",id : $("#fishCrossTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id
		}, function(data) {
			if (data.success) {
				var ob = fishCrossTaskTemplateGrid.getStore().recordType;
				fishCrossTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					
					p.set("note",obj.note);
					fishCrossTaskTemplateGrid.getStore().add(p);							
				});
				
				fishCrossTaskTemplateGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id
		}, function(data) {
			if (data.success) {	

				var ob = fishCrossTaskReagentGrid.getStore().recordType;
				fishCrossTaskReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("oneNum",obj.num);
					p.set("note",obj.note);
					fishCrossTaskReagentGrid.getStore().add(p);							
				});
				
				fishCrossTaskReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id
		}, function(data) {
			if (data.success) {	

				var ob = fishCrossTaskCosGrid.getStore().recordType;
				fishCrossTaskCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					fishCrossTaskCosGrid.getStore().add(p);							
				});			
				fishCrossTaskCosGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
}