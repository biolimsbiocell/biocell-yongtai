var fishCrossTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
			name:'confirmDate',
			type:"string"
	});
	    fields.push({
		name:'workOrder-id',
		type:"string"
	});
	    fields.push({
		name:'workOrder-name',
		type:"string"
	});
	    fields.push({
		name:'testUser-id',
		type:"string"
	});
	    fields.push({
		name:'testUser-name',
		type:"string"
	});
	    fields.push({
		name:'testDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
	    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
	    fields.push({
			name:'type',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.user.itemNo,
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'下达人ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.common.createUserName,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.common.createDate,
		width:25*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'confirmDate',
		header:biolims.common.confirmDate,
		width:25*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'testUser-id',
		hidden:true,
		header:'实验员ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'testUser-name',
		header:biolims.common.testUserName,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'testDate',
		header:biolims.common.testTime,
		width:15*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:'选择模板ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:biolims.common.selectTemplate,
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:'工作流状态',
		width:40*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:40*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-id',
		header:biolims.common.acceptUserId,
		width:15*10,
		sortable:true
	});
		cm.push({
		dataIndex:'acceptUser-name',
		header:biolims.common.acceptUserName,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden:true,
		header:'样本类型编号',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.sampleTypeName,
		width:15*10,
		sortable:true
	});
	var storeistype = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '分割' ], [ '2', '分离' ] ]
	});
	var type = new Ext.form.ComboBox({
		store : storeistype,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'type',
		hidden : false,
		header:biolims.common.type,
		width:15*6,
		editor : type,
		renderer : Ext.util.Format.comboRenderer(type)
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/fishCrossTask/showFishCrossTaskListJson.action";
	loadParam.limit = 200;
	var opts={};
	opts.title=biolims.common.hybridsElution;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	fishCrossTaskGrid=gridTable("show_fishCrossTask_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/experiment/fish/fishCrossTask/editFishCrossTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/fish/fishCrossTask/editFishCrossTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/fish/fishCrossTask/viewFishCrossTask.action?id=' + id;
}
function exportexcel() {
	fishCrossTaskGrid.title = '导出列表';
	var vExportContent = fishCrossTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#starttestDate").val() != undefined) && ($("#starttestDate").val() != '')) {
					var starttestDatestr = ">=##@@##" + $("#starttestDate").val();
					$("#testDate1").val(starttestDatestr);
				}
				if (($("#endtestDate").val() != undefined) && ($("#endtestDate").val() != '')) {
					var endtestDatestr = "<=##@@##" + $("#endtestDate").val();

					$("#testDate2").val(endtestDatestr);

				}
				
				
				commonSearchAction(fishCrossTaskGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
