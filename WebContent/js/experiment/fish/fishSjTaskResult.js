var fishSjTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'slideCode',
		type:"string"
	});
	   fields.push({
		name:'f18',
		type:"string"
	});
	   fields.push({
		name:'x',
		type:"string"
	});
	   fields.push({
		name:'y',
		type:"string"
	});
	   fields.push({
		name:'f13',
		type:"string"
	});
	   fields.push({
		name:'f21',
		type:"string"
	});
	   fields.push({
		name:'otherSit',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'isCommit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'fishSjTask-id',
		type:"string"
	});
	    fields.push({
		name:'fishSjTask-name',
		type:"string"
	});
	   	fields.push({
		name:'productId',
		type:"string"
	});
	   	fields.push({
		name:'productName',
		type:"string"
	});
	   	fields.push({
		name:'sampleType',
		type:"string"
	});
	   	fields.push({
			name:'cellsNum',
			type:"string"
		});
	   	fields.push({
			name:'nextFlowId',
			type:"string"
		});
		   fields.push({
			name:'nextFlow',
			type:"string"
		});  	
	
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6
	});
	cm.push({
		dataIndex:'slideCode',
		hidden : false,
		header:biolims.common.slideCode,
		width:25*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6
	});
	cm.push({
		dataIndex:'cellsNum',
		hidden : false,
		header:biolims.common.bacteriaCount,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:'样本类型',
		width:20*6
	});

	cm.push({
		dataIndex:'f18',
		hidden : false,
		header:'18/x/y',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'x',
		hidden : true,
		header:'x',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'y',
		hidden : true,
		header:'y',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'f13',
		hidden : false,
		header:'13/21',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'f21',
		hidden : true,
		header:'21',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'otherSit',
		hidden : false,
		header:biolims.common.probeBitOther,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow+'<font color="red">*</font>',
		width:15*10,
		sortable:true,
		hidden:false,
		editor : nextFlowCob
	});
	var storeisresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1',biolims.common.regular ], [ '0', biolims.common.abnormal ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeisresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.dna.dnaDetectionResults+'<font color="red">*</font>',
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isCommit',
		hidden : false,
		header:biolims.common.Submitted+'<font color="red">*</font>',
		width:20*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishSjTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishSjTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 80,
		hidden : false,
		header : biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.common.attachment,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')){
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:biolims.common.attachment,layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+ rec.get('id')+"&modelType=fishSjTaskResult' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: biolims.common.close,
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				}else{
					message(biolims.common.pleaseSaveRecord);
				}
			}
		}]
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/fishSjTask/showFishSjTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.onBoardDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/fishSjTask/delFishSjTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				fishSjTaskResultGrid.getStore().commitChanges();
				fishSjTaskResultGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), biolims.common.batchResult, null, {
				"确定" : function() {
					var records = fishSjTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						fishSjTaskResultGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isGood", result);
						});
						fishSjTaskResultGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = fishSjTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						fishSjTaskResultGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("isCommit", submit);
//						});
//						fishSjTaskResultGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.report.checkFile,
		handler : loadPic
	});
//	opts.tbar.push({
//		iconCls : 'save',
//		text : biolims.common.save,
//		handler : saveInfo
//	});
	fishSjTaskResultGrid=gridEditTable("fishSjTaskResultdiv",cols,loadParam,opts);
	$("#fishSjTaskResultdiv").data("fishSjTaskResultGrid", fishSjTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(fishSjTaskResultGrid);
	var id=$("#fishSjTask_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/fish/fishSjTask/saveFishSjTaskResult.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					fishSjTaskResultGrid.getStore().commitChanges();
					fishSjTaskResultGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);
		}
	}else{
		message(biolims.common.noData2Save);
	}
	  }else{
		  message(biolims.storage.infoChange);
	}
}

function loadPic(){
	var record = fishSjTaskResultGrid.getSelectionModel().getSelections();
	if(record.length==1){
		var model="fishSjTaskResult";
		var id="'"+record[0].get("id")+"'";
		window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="+id+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		
	}else{
		message(biolims.common.pleaseSelectAPieceOfData);
	}

}
//提交样本
function submitSample(){
	var id=$("#fishSjTask_id").val();  
	if(fishSjTaskResultGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var record = fishSjTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("isCommit")){
			flg=true;
		}
		if(record[i].get("isGood")==""){
			message(biolims.common.resultsIsEmpty);
			return;
		}
	}
	var grid=fishSjTaskResultGrid.store;
	for(var i=0;i<grid.getCount();i++){
		if(grid.getAt(i).get("isCommit")==""){
			flg=true;
		}
		if(grid.getAt(i).get("isGood")==""){
			message(biolims.common.resultsIsEmpty);
			return;
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		
		
		var records = [];
		
		
		
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		
		ajax("post", "/experiment/fish/fishSjTask/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				fishSjTaskResultGrid.getStore().commitChanges();
				fishSjTaskResultGrid.getStore().reload();
				message(biolims.common.submitSuccess);
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);
	}else{
		message(biolims.common.noData2Submit);
	}
}

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = fishSjTaskResultGrid.getSelectRecord();
	var productId1="";
	$.each(records1, function(j, k) {
		productId1=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null,biolims.common.selectNextFlow, "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=FishSjTask&productId="+productId1, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = fishSjTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = fishSjTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");


}
