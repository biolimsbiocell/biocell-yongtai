﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	
	var state = $("#samplePreTask_state").val();
	if(state =="3"){
		load("/experiment/fish/samplePreTask/showSamplePreTasktempList.action", null, "#samplePreTaskTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showAcceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#showworkOrder").css("display","none");
	}
	setTimeout(function() {
		var getGrid=samplePreTaskTemplateGrid.store;
		if(getGrid.getCount()==0){
			loadTemplate($("#samplePreTask_template").val());
		}
	}, 1000);
});
function add() {
	window.location = window.ctx + "/experiment/fish/samplePreTask/editSamplePreTask.action?maxNum="+$("#samplePreTask_maxNum").val();
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/fish/samplePreTask/showSamplePreTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("SamplePreTask", {
					userId : userId,
					userName : userName,
					formId : $("#samplePreTask_id").val(),
					title : $("#samplePreTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	var codeList = new Array();
	if (samplePreTaskResultGrid.getAllRecord().length > 0) {
		var selRecord = samplePreTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){

			codeList.push(selRecord.getAt(j).get("sampleCode"));
			if(selRecord.getAt(j).get("result")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
			if(selRecord.getAt(j).get("nextFlowId")==""){
				message(biolims.common.nextStepNotEmpty);
				return;
			}
		}
		if(samplePreTaskResultGrid.getModifyRecord().length > 0){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
	
		completeTask($("#samplePreTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});


function save() {
	var code=$("#samplePreTask_template").val();
	if(code==""||code==null){
		message(biolims.common.pleaseSelectTemplate);
		return;
	}
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });   
	    var samplePreTaskItemDivData = $("#samplePreTaskItemdiv").data("samplePreTaskItemGrid");
		document.getElementById('samplePreTaskItemJson').value = commonGetModifyRecords(samplePreTaskItemDivData);
	    var samplePreTaskResultDivData = $("#samplePreTaskResultdiv").data("samplePreTaskResultGrid");
		document.getElementById('samplePreTaskResultJson').value = commonGetModifyRecords(samplePreTaskResultDivData);
		var samplePreTaskTemplateDivData = $("#samplePreTaskTemplatediv").data("samplePreTaskTemplateGrid");
		document.getElementById('samplePreTaskTemplateJson').value = commonGetModifyRecords(samplePreTaskTemplateDivData);
		var samplePreTaskReagentDivData = $("#samplePreTaskReagentdiv").data("samplePreTaskReagentGrid");
		document.getElementById('samplePreTaskReagentJson').value = commonGetModifyRecords(samplePreTaskReagentDivData);
		var samplePreTaskCosDivData = $("#samplePreTaskCosdiv").data("samplePreTaskCosGrid");
		document.getElementById('samplePreTaskCosJson').value = commonGetModifyRecords(samplePreTaskCosDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/fish/samplePreTask/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/fish/samplePreTask/copySamplePreTask.action?id=' + $("#samplePreTask_id").val();
}

$("#toolbarbutton_status").click(function(){
//	var bloodInfoGrid = samplePreTaskResultGrid.getStore();
//	var bloodItemGrid = samplePreTaskItemGrid.getStore();
//	var num = 0;
//	for(var i= 0; i<bloodItemGrid.getCount();i++){
//		for(var j=0;j<bloodInfoGrid.getCount();j++){
//			if(bloodItemGrid.getAt(j).get("sampleCode")==bloodInfoGrid.getAt(i).get("sampleCode")){
//				j=bloodInfoGrid.getCount();
//			}else{
//				num=num+1;
//				if(num==bloodInfoGrid.getCount()){
//					num=0;
//					message("请完成实验！");
//					return;
//				}
//			}
////			if(bloodItemGrid.getAt(j).get("sampleCode")!=bloodInfoGrid.getAt(i).get("sampleCode")){
////				message("请完成实验！");
////				return;
////			}
//		}
//	}

	commonChangeState("formId=" + $("#samplePreTask_id").val() + "&tableId=SamplePreTask");
});
//setTimeout(function() {
//	if($("#samplePreTask_template").val()){
//		var maxNum = $("#samplePreTask_maxNum").val();
//		if(maxNum>0){
//				load("/storage/container/sampleContainerTest.action", {
//					id : $("#samplePreTask_template").val(),
//					type :$("#type").val(),
//					maxNum : 0
//				}, "#gridContainerdiv0", function(){
//					if(maxNum>1){
//					load("/storage/container/sampleContainerTest.action", {
//						id : $("#samplePreTask_template").val(),
//						type :$("#type").val(),
//						maxNum : 1
//					}, "#gridContainerdiv1", null)
//					}
//				});
//			
//		}
//	}
//}, 100);
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#samplePreTask_id").val());
	nsc.push(biolims.user.NoNull);
	fs.push($("#samplePreTask_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.samplePreprocess,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/fish/samplePreTask/showSamplePreTaskItemList.action", {
	id : $("#samplePreTask_id").val()
}, "#samplePreTaskItempage");
load("/experiment/fish/samplePreTask/showSamplePreTaskTemplateList.action", {
	id : $("#samplePreTask_id").val()
}, "#samplePreTaskTemplatepage");
load("/experiment/fish/samplePreTask/showSamplePreTaskReagentList.action", {
	id : $("#samplePreTask_id").val()
}, "#samplePreTaskReagentpage");
load("/experiment/fish/samplePreTask/showSamplePreTaskCosList.action", {
	id : $("#samplePreTask_id").val()
}, "#samplePreTaskCospage");
load("/experiment/fish/samplePreTask/showSamplePreTaskResultList.action", {
	id : $("#samplePreTask_id").val()
}, "#samplePreTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);

//调用模板
function TemplateFun(){
		var type="doSamplePre";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'Select Template',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: 'close',
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
function setTemplateFun(rec){
	//把实验模板中的中间产物类型和数量带入
	var itemGrid=samplePreTaskItemGrid.store;
	if(itemGrid.getCount()>0){
		for(var i=0;i<itemGrid.getCount();i++){
			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
		}
	}
	document.getElementById('samplePreTask_acceptUser').value=rec.get('acceptUser-id');
	document.getElementById('samplePreTask_acceptUser_name').value=rec.get('acceptUser-name');
	var duringDays=rec.get('duringDays');
	var remindDays=rec.get('remindDays');
	
	var createDate=$('#samplePreTask_createDate').val();
	createDate=new Date(createDate);
	var date = new Date(createDate.getTime() + 24*60*60*1000*duringDays);  
	var result = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
	var code=$("#samplePreTask_template").val();
		if(code==""){
			document.getElementById('samplePreTask_template').value=rec.get('id');
			document.getElementById('samplePreTask_template_name').value=rec.get('name');
			var win = Ext.getCmp('TemplateFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/system/template/template/setTemplateItem.action", {
				code : id
				}, function(data) {
					if (data.success) {
						var ob = samplePreTaskTemplateGrid.getStore().recordType;
						samplePreTaskTemplateGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tItem",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							
							p.set("note",obj.note);
							samplePreTaskTemplateGrid.getStore().add(p);							
						});
						
						samplePreTaskTemplateGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateReagent.action", {
				code : id
				}, function(data) {
					if (data.success) {	

						var ob = samplePreTaskReagentGrid.getStore().recordType;
						samplePreTaskReagentGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tReagent",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							p.set("batch",obj.batch);
							p.set("isGood",obj.isGood);
							p.set("itemId",obj.itemId);
							
							p.set("oneNum",obj.num);
							p.set("note",obj.note);
							samplePreTaskReagentGrid.getStore().add(p);							
						});
						
						samplePreTaskReagentGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateCos.action", {
				code : id
				}, function(data) {
					if (data.success) {	

						var ob = samplePreTaskCosGrid.getStore().recordType;
						samplePreTaskCosGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tCos",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							p.set("isGood",obj.isGood);
							p.set("itemId",obj.itemId);
							
							p.set("temperature",obj.temperature);
							p.set("speed",obj.speed);
							p.set("time",obj.time);
							p.set("note",obj.note);
							samplePreTaskCosGrid.getStore().add(p);							
						});			
						samplePreTaskCosGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
 				var ob1 = samplePreTaskTemplateGrid.store;
 				if (ob1.getCount() > 0) {
					for(var j=0;j<ob1.getCount();j++){
						var oldv = ob1.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
							ajax("post", "/experiment/fish/samplePreTask/delSamplePreTaskTemplateOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null);
						}else{								
							samplePreTaskTemplateGrid.store.removeAll();
						}
					}
					samplePreTaskTemplateGrid.store.removeAll();
 				}
				var ob2 = samplePreTaskReagentGrid.store;
				if (ob2.getCount() > 0) {
					for(var j=0;j<ob2.getCount();j++){
						var oldv = ob2.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
						ajax("post", "/experiment/fish/samplePreTask/delSamplePreTaskReagentOne.action", {
							ids : oldv
						}, function(data) {
							if (data.success) {
								message(biolims.common.deleteSuccess);
							} else {
								message(biolims.common.deleteFailed);
							}
						}, null); 
						}else{
							samplePreTaskReagentGrid.store.removeAll();
						}
					}
					samplePreTaskReagentGrid.store.removeAll();
 				}
				var ob3 = samplePreTaskCosGrid.store;
				if (ob3.getCount() > 0) {
					for(var j=0;j<ob3.getCount();j++){
						var oldv = ob3.getAt(j).get("id");
						//根据ID删除
						if(oldv!=null){
							ajax("post", "/experiment/fish/samplePreTask/delSamplePreTaskCosOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null); 
						}else{
							samplePreTaskCosGrid.store.removeAll();
						}
					}
					samplePreTaskCosGrid.store.removeAll();
 				}
 			 document.getElementById('samplePreTask_template').value=rec.get('id');
 			 document.getElementById('samplePreTask_template_name').value=rec.get('name');
			 var win = Ext.getCmp('TemplateFun');
			 if(win){win.close();}
				var id = rec.get('id');
				ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = samplePreTaskTemplateGrid.getStore().recordType;
							samplePreTaskTemplateGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tItem",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("note",obj.note);
								samplePreTaskTemplateGrid.getStore().add(p);							
							});
							
							samplePreTaskTemplateGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = samplePreTaskReagentGrid.getStore().recordType;
							samplePreTaskReagentGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tReagent",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("batch",obj.batch);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("oneNum",obj.num);
								p.set("note",obj.note);
								p.set("sn",obj.sn);
								
								samplePreTaskReagentGrid.getStore().add(p);							
							});
							
							samplePreTaskReagentGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id
					}, function(data) {
						if (data.success) {	

							var ob = samplePreTaskCosGrid.getStore().recordType;
							samplePreTaskCosGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tCos",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("temperature",obj.temperature);
								p.set("speed",obj.speed);
								p.set("time",obj.time);
								p.set("note",obj.note);
								samplePreTaskCosGrid.getStore().add(p);							
							});			
							samplePreTaskCosGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null);
			 }
			
 		}	
}


//按条件加载原辅料
function showReagent(){
	
	//获取全部数据
	var allRcords=samplePreTaskTemplateGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=samplePreTaskTemplateGrid.getSelectionModel().getSelections();	
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#samplePreTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/fish/samplePreTask/showSamplePreTaskReagentList.action", {
			id : $("#samplePreTask_id").val()
		}, "#samplePreTaskReagentpage");
	}else if(length1==1){
		samplePreTaskReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/fish/samplePreTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = samplePreTaskReagentGrid.getStore().recordType;
				samplePreTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("samplePreTask-id",obj.tId);
					p.set("samplePreTask-name",obj.tName);
					
					samplePreTaskReagentGrid.getStore().add(p);							
				});
				samplePreTaskReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}
	
}

//按条件加载设备
function showCos(){
	//获取全部数据
	var allRcords=samplePreTaskTemplateGrid.store;
	var flag="1";
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag="0";
		}else{
			flag="1";
		}
	}
	if(flag=="0"){
		message(biolims.common.pleaseHold);
		return;
	}else{
		//获取选择的数据
		var selectRcords=samplePreTaskTemplateGrid.getSelectionModel().getSelections();
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid=$("#samplePreTask_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/fish/samplePreTask/showSamplePreTaskCosList.action", {
				id : $("#samplePreTask_id").val()
			}, "#samplePreTaskCospage");
		}else if(length1==1){
			samplePreTaskCosGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/fish/samplePreTask/setCos.action", {
				tid:tid,code : code
			}, function(data) {
				
				if (data.success) {	
					var ob = samplePreTaskCosGrid.getStore().recordType;
					samplePreTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						
						p.set("id",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("sampleNum",obj.sampleNum);
						p.set("time",obj.time);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("tCos",obj.tCos);
						p.set("samplePreTask-id",obj.tId);
						p.set("samplePreTask-name",obj.tName);
						p.set("note",obj.note);
						samplePreTaskCosGrid.getStore().add(p);							
					});
					samplePreTaskCosGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
			});		
		}else{
			message(biolims.common.pleaseSelectAPieceOfData);
			return;
		}
	}
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.common.rollback
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = samplePreTaskResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}

			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.common.save
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "SamplePreTask",id : $("#samplePreTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id
		}, function(data) {
			if (data.success) {
				var ob = samplePreTaskTemplateGrid.getStore().recordType;
				samplePreTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					
					p.set("note",obj.note);
					samplePreTaskTemplateGrid.getStore().add(p);							
				});
				
				samplePreTaskTemplateGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id
		}, function(data) {
			if (data.success) {	

				var ob = samplePreTaskReagentGrid.getStore().recordType;
				samplePreTaskReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("oneNum",obj.num);
					p.set("note",obj.note);
					samplePreTaskReagentGrid.getStore().add(p);							
				});
				
				samplePreTaskReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id
		}, function(data) {
			if (data.success) {	

				var ob = samplePreTaskCosGrid.getStore().recordType;
				samplePreTaskCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					samplePreTaskCosGrid.getStore().add(p);							
				});			
				samplePreTaskCosGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
}
