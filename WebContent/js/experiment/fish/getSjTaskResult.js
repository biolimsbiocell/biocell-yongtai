var getSjTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'slideCode',
		type:"string"
	});
	   fields.push({
		name:'f18',
		type:"string"
	});
	   fields.push({
		name:'x',
		type:"string"
	});
	   fields.push({
		name:'y',
		type:"string"
	});
	   fields.push({
		name:'f13',
		type:"string"
	});
	   fields.push({
		name:'f21',
		type:"string"
	});
	   fields.push({
		name:'otherSit',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'isCommit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'fishSjTask-id',
		type:"string"
	});
	    fields.push({
		name:'fishSjTask-name',
		type:"string"
	});
	   	fields.push({
		name:'productId',
		type:"string"
	});
	   	fields.push({
		name:'productName',
		type:"string"
	});
	   	fields.push({
		name:'sampleType',
		type:"string"
	});
	   	fields.push({
			name:'cellsNum',
			type:"string"
		});
	   	
	
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:25*6
	});
	cm.push({
		dataIndex:'slideCode',
		hidden : false,
		header:'玻片编号',
		width:25*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:25*6
	});
	cm.push({
		dataIndex:'cellsNum',
		hidden : false,
		header:'细胞计数',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:'样本类型',
		width:20*6
	});


	cm.push({
		dataIndex:'f18',
		hidden : false,
		header:'18/x/y',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'x',
		hidden : true,
		header:'x',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'y',
		hidden : true,
		header:'y',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'f13',
		hidden : false,
		header:'13/21',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'f21',
		hidden : true,
		header:'21',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'otherSit',
		hidden : false,
		header:'其他探针位点',
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeisresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '正常' ], [ '0', '异常' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeisresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:'检测结果<font color="red">*</font>',
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isCommit',
		hidden : false,
		header:'是否已提交<font color="red">*</font>',
		width:20*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishSjTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fishSjTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 80,
		hidden : false,
		header : '上传附件',
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : '附件',
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')){
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+ rec.get('id')+"&modelType=fishSjTaskResult' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: '关闭',
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				}else{
					message("请先保存记录。");
				}
			}
		}]
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/fishSjTask/getFishSjTaskResultListJson.action?code="+ $("#code").val();
	var opts={};
	opts.title="上机阅片结果";
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/fishSjTask/delFishSjTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				fishSjTaskResultGrid.getStore().commitChanges();
				fishSjTaskResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//		text : '提交样本',
//		handler : submitSample
//	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
//	opts.tbar.push({
//		text : '查看附件',
//		handler : loadPic
//	});
	getSjTaskResultGrid=gridEditTable("getSjTaskResultdiv",cols,loadParam,opts);
	$("#getSjTaskResultdiv").data("getSjTaskResultGrid", getSjTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function loadPic(){
	var record = fishSjTaskResultGrid.getSelectionModel().getSelections();
	if(record.length==1){
		var model="fishSjTaskResult";
		window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="+record[0].get("id")+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		
	}else{
		message("请选择一条数据！");
	}

}
//提交样本
function submitSample(){
	var id=$("#fishSjTask_id").val();  
	if(fishSjTaskResultGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	var record = fishSjTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("isCommit")){
			flg=true;
		}
		if(record[i].get("isGood")==""){
			message("结果不能为空！");
			return;
		}
	}
	var grid=fishSjTaskResultGrid.store;
	for(var i=0;i<grid.getCount();i++){
		if(grid.getAt(i).get("isCommit")==""){
			flg=true;
		}
		if(grid.getAt(i).get("isGood")==""){
			message("结果不能为空！");
			return;
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		
		
		var records = [];
		
		
		
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		
		ajax("post", "/experiment/fish/fishSjTask/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				fishSjTaskResultGrid.getStore().commitChanges();
				fishSjTaskResultGrid.getStore().reload();
				message("提交成功！");
			} else {
				loadMarsk.hide();
				message("提交失败！");
			}
		}, null);
	}else{
		message("没有需要提交的样本！");
	}
}