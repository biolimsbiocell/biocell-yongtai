﻿var fishProTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'stateName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'sequencingFun',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'chargeNote',
		type:"string"
	});
	   fields.push({
		   name:'gender',
		   type:"string"
		});
	   fields.push({
		   name:'hospital',
		   type:"string"
		});
	   fields.push({
		   name:'doctor',
		   type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'样本名称',
		width:20*6
	});
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:'状态',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*5
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:30*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:30*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:biolims.user.patientName,
		width:20*6
	});
	var storegenderCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '男' ], [ '0', '女' ] ]
	});
	var genderCob = new Ext.form.ComboBox({
		store : storegenderCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'gender',
		hidden : false,
		header:biolims.common.gender,
		width:20*6,
		//editor : genderCob,
		renderer : Ext.util.Format.comboRenderer(genderCob)
	});
	cm.push({
		dataIndex:'hospital',
		hidden : false,
		header:biolims.master.hospitalName,
		width:30*6
	});
	cm.push({
		dataIndex:'doctor',
		hidden : false,
		header:biolims.sample.crmDoctorName,
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:biolims.common.causeInspection,
		width:30*6
	});
	var storechargeNoteCob = new Ext.data.ArrayStore({
		fields:['id','name'],
		data:[ ['1','待缴费'],['2','已缴费'],['3','待结算'],['4','科研'],['5','免费']]
	});
	var chargeNoteCob = new Ext.form.ComboBox({
		store:storechargeNoteCob,
		displayField:'name',
		valueField:'id',
		mode:'local'
	});
	cm.push({
		dataIndex:'chargeNote',
		hidden:false,
		header:biolims.common.payStatus,
		width:20*6,
		renderer:Ext.util.Format.comboRenderer(chargeNoteCob)
	});
	cm.push({
		dataIndex:'method',
		hidden : true,
		header:'处理意见',
		width:30*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:'浓度',
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积',
		width:20*6
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果',
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'步骤编号',
		width:30*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证号',
		width:30*6
	});
	cm.push({
		dataIndex:'sequencingFun',
		hidden : true,
		header:'检测方法',
		width:20*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:25*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:biolims.common.acceptDate,
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:25*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单id',
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:30*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务',
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/fishProTask/showFishProTasktempListJson.action";
	loadParam.limit=200;
	var opts={};
	opts.title=biolims.common.pendingSample;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];

	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$(".jquery-ui-warning").html(biolims.common.longlongagolong1);
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = fishProTaskTempGrid.getAllRecord();
							var store = fishProTaskTempGrid.store;
							var count = 0;
							var isOper = true;
							var buf = [];
							var buf1 = [];
							fishProTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
									}else{
//										if(buf1.indexOf(obj)==-1){
//											buf1.push(obj);
//										}
									}
									
								});
							});
							
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("code"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message(biolims.common.noMatchSample+nolist);
							}
							fishProTaskTempGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){

							}else{
								//message("样本"+buf1+"核对不符，请检查！");
								//message("样本号核对完毕！");
								addItem();
							}
							fishProTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	fishProTaskTempGrid=gridEditTable("fishProTaskTempdiv",cols,loadParam,opts);
	$("#fishProTaskTempdiv").data("fishProTaskTempGrid", fishProTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//从左边添加到右边的明细中
function addItem(){
	var selRecord = fishProTaskTempGrid.getSelectionModel().getSelections();//从左边获取数据
	var getRecord = fishProTaskItemGrid.store;//填充到当前的明细中
	var count=1;
	var max=0;
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var isRepeat = false;
			for(var j=0; j<getRecord.getCount();j++){
				var getData = getRecord.getAt(j).get("tempId");
				if(getData==obj.get("id")){
					isRepeat = true;
					message(biolims.common.haveDuplicate);
					break;
				}
			}
			//获取最大排序号
			for(var i=0; i<getRecord.getCount();i++){
				var a=getRecord.getAt(i).get("orderNumber");
				if(a>max){
					max=a;
				}
			}
			if(!isRepeat){
				var ob = fishProTaskItemGrid.getStore().recordType;
				fishProTaskItemGrid.stopEditing();
				var p= new ob({});
				p.set("orderNumber",Number(max)+count);
				p.set("code",obj.get("code"));
				p.set("sampleCode",obj.get("sampleCode"));
				p.set("tempId",obj.get("id"));
				p.set("name",obj.get("name"));
				p.set("patientName",obj.get("patientName"));
				var productId=obj.get("productId");
				if(productId.substring(productId.length-1,productId.length)==","){
					p.set("productId",productId.substring(0,productId.length-1));
				}else{
					p.set("productId",productId);
				}
				
				var productName=obj.get("productName");
				if(productName.substring(productName.length-1,productName.length)==","){
					p.set("productName",productName.substring(0,productName.length-1));
				}else{
					p.set("productName",productName);
				}
				
				
				p.set("state",obj.get("state"));
				p.set("stateName",obj.get("stateName"));
				p.set("note",obj.get("note"));
				p.set("concentration",obj.get("concentration"));
				p.set("result",obj.get("result"));
				
				p.set("reason",obj.get("reason"));
				p.set("reportDate",obj.get("reportDate"));
				p.set("volume",obj.get("volume"));
				p.set("idCard",obj.get("idCard"));
				
				p.set("sequencingFun",obj.get("sequencingFun"));
				p.set("inspectDate",obj.get("inspectDate"));
				p.set("acceptDate",obj.get("acceptDate"));
				p.set("orderId",obj.get("orderId"));
				p.set("phone",obj.get("phone"));
				
				p.set("classify",obj.get("classify"));
				p.set("sampleType",obj.get("sampleType"));
				p.set("sampleNum",obj.get("sampleNum"));
				fishProTaskItemGrid.getStore().add(p);
				fishProTaskItemGrid.startEditing(0,0);
			}
		});	
	}
}
