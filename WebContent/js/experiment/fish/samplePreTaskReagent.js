﻿var samplePreTaskReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
   fields.push({
		name:'tReagent',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
   fields.push({
		name:'itemId',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'samplePreTask-id',
		type:"string"
	});
	    fields.push({
		name:'samplePreTask-name',
		type:"string"
	});
	    fields.push({
			name:'oneNum',
			type:"string"
		});
	    fields.push({
			name:'sampleNum',
			type:"string"
		});
	    fields.push({
			name:'num',
			type:"string"
		});
	    
    fields.push({
		name:'count',
		type:"string"
	});
    fields.push({
		name:'sn',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'原辅料id',
		width:20*6
	});
	cm.push({
		dataIndex:'tReagent',
		hidden : true,
		header:'模板原辅料Id',
		width:20*6
	});
	
	var codes =new Ext.form.TextField({
        allowBlank: false
	});
	codes.on('focus', function() {
		var selectRecord = samplePreTaskReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var tid = $("#samplePreTask_template").val();
				loadReagentItemByCode(tid);
			});
		}
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.reagentNo,
		width:20*6,
		editor : codes
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'模板步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.reagentName,
		width:20*6
	});
	//鼠标单击触发事件 
	var batchs =new Ext.form.TextField({
            allowBlank: false
    });
	batchs.on('focus', function() {
		var selectRecord = samplePreTaskReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("code");
				loadStorageReagentBuy(code);
			});
		}
	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:biolims.common.batch,
		width:20*6,
		editor:batchs
	});
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '否' ], [ '1', '是' ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isGood,
		width:20*6,
		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	
	cm.push({
		dataIndex:'count',
		hidden : true,
		header:'数量',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'oneNum',
		hidden : false,
		header:biolims.common.singleDose,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.dose,
		width:20*6
	});
	cm.push({
		dataIndex:'sn',
		hidden : false,
		header:'sn',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cm.push({
		dataIndex:'samplePreTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10
	});
	cm.push({
		dataIndex:'samplePreTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/fish/samplePreTask/showSamplePreTaskReagentListJson.action?id="+$("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight-220;
	opts.tbar = [];
	var state=$("#samplePreTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/fish/samplePreTask/delSamplePreTaskReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				samplePreTaskReagentGrid.getStore().commitChanges();
				samplePreTaskReagentGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : function (){
			//获取选择的数据
			var selectRcords=samplePreTaskTemplateGrid.getSelectionModel().getSelections();
			//获取全部数据
			var allRcords=samplePreTaskTemplateGrid.store;
			//选中的数量
			var length1=selectRcords.length;
			//全部数据量
			var length2=allRcords.getCount();
			if(length2>0){
				if(length1==1){
					var code="";
					$.each(selectRcords, function(i, obj) {
						code=obj.get("code");
					});
					if(code!=""){
						showStorageList(code);
					}else{
						message(biolims.common.addTemplateDetail);
						return;
					}				
				}else if(length1>1){
					message(biolims.common.onlyChooseOne);
					return;
				}else{
					message(biolims.common.pleaseSelectData);
					return;
				}
			}else{
				message(biolims.common.theDataIsEmpty);
				return;
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	}
	samplePreTaskReagentGrid=gridEditTable("samplePreTaskReagentdiv",cols,loadParam,opts);
	$("#samplePreTaskReagentdiv").data("samplePreTaskReagentGrid", samplePreTaskReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//加载采购原辅料
function loadStorageReagentBuy(code){
	var win = Ext.getCmp('loadStorageReagentBuy');
	if (win) {win.close();}
	var loadStorageReagentBuy= new Ext.Window({
	id:'loadStorageReagentBuy',modal:true,title:'选择采购原辅料',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/template/showStorageReagentBuyList.action?flag=StorageReagentBuy&codes="+code+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 loadStorageReagentBuy.close(); }  }]  });     loadStorageReagentBuy.show();
}
function setStorageReagentBuy(rec){
	
	var gridGrid = $("#samplePreTaskReagentdiv").data("samplePreTaskReagentGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		
		obj.set('batch',rec.get("code"));
	});
	var win = Ext.getCmp('loadStorageReagentBuy')
	if(win){
		win.close();
	}
}
//加载库存主数据
function showStorageList(code){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, "库存主数据", url, {
		"确定" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					samplePreTaskReagentGrid.stopEditing();
					var ob = samplePreTaskReagentGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;

					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
					p.set("itemId",code);
				
					samplePreTaskReagentGrid.getStore().add(p);	
				});
				samplePreTaskReagentGrid.startEditing(0, 0);
				
				options.close();
			}else{
				message("请选择数据！");
			}
		}
	
	}, true, options);
}
//根据原辅料编号查询原辅料明细
function loadReagentItemByCode(tid){
		var options = {};
		options.width = 900;
		options.height = 460;
		var url="/system/template/template/showReagentItemByCodeList.action?tid="+tid;
		loadDialogPage(null, "选择明细", url, {
			 "确定": function() {
				 selRecord = reagentItem1Grid.getSelectionModel();
					if (selRecord.getSelections().length > 0) {
						$.each(selRecord.getSelections(), function(i, obj) {
							samplePreTaskReagentGrid.stopEditing();
							var ob = samplePreTaskReagentGrid.getStore().recordType;
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.get("code"));
							p.set("name",obj.get("name"));
							p.set("batch",obj.get("batch"));
							p.set("isTestSuccess",obj.get("isGood"));
							p.set("itemId",obj.get("itemId"));
							
							p.set("oneNum",obj.get("num"));
							p.set("note",obj.get("note"));
							samplePreTaskReagentGrid.getStore().add(p);	
						});
						samplePreTaskReagentGrid.startEditing(0, 0);
						options.close();
					}else{
						message("请选择数据！");
					}
				 options.close();
			}
		}, true, options);
}