var sampleOrderTable;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": biolims.common.orderCode,
		}];
		
		
	var options = table(true, "",
		"/system/sample/sampleOrder/showSampleOrderTableJson.action",colData , null)
	sampleOrderTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleOrderTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/system/sample/sampleOrder/editSampleOrder.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/sample/sampleOrder/editSampleOrder.action?id=' + id ;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/system/sample/sampleOrder/viewSampleOrder.action?id=' + id ;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},{
			"type": "table",
			"table": sampleOrderTable
		}
	];
}