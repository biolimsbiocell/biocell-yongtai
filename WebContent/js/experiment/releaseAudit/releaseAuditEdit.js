$(function() {
	$("#btn_list").text("返回");
	layui.use('form', function(){
		var form = layui.form;
		//监听提交
		form.on('submit(formDemo)', function(data){
			console.log(data);
			$.ajax({
				type : "post",
				url : ctx + "/experiment/ReleaseAudit/ReleaseAudit/saveReleaseAudit.action",
				data : {jsonItem:JSON.stringify(data.field)},
				success : function(data) {
					var data1 = JSON.parse(data);
					if (data1.success) {
						$("#maincontentframe",
								window.parent.document)[0].src = window.ctx
								+ "/experiment/ReleaseAudit/ReleaseAudit/toEditReleaseAudit.action?id="
								+ data1.id;
					}
				}
			});
//			layer.msg(JSON.stringify(data.field));
		return false;
		});
	});
	
	//日期格式化
	$("#releaseAudit_qualityAuthorizationDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//日期格式化
	$("#releaseAudit_qualityAssuranceTime").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	
//	//用户订单显示审核人,提交;客服订单显示状态
//	if($("#sampleOrder_orderType").val()=="0"){
//		$("#auditDiv").show();
//		if($("#sampleOrder_id").val()!="NEW"){
//			$("#btn_submit").show();
//		}
//	}else if($("#sampleOrder_orderType").val()=="1"){
//		if($("#sampleOrder_id").val()!="NEW"){
//			$("#btn_changeState").show();
//		}
//	}
//	
//	//日期格式化
//	$("#reinfusionPlan_reinfusionPlanDate").datepicker({
//		language: "zh-TW",
//		autoclose: true, //选中之后自动隐藏日期选择框
//		format: "yyyy-mm-dd" //日期格式，详见 
//	});
//	
//	//采血时间
//	$("#sampleOrder_drawBloodTime").datepicker({
//		language: "zh-TW",
//		autoclose: true, //选中之后自动隐藏日期选择框
//		format: "yyyy-mm-dd" //日期格式，详见 
//	});
//
//	//控制确认申请按钮显示
//	var flagValue = $("#sampleOrder_flagValue").val();
//	var state = $("#sampleOrder_stateName").val();
//	if((flagValue == "EDIT" || flagValue == "ADD") && (state == "Complete" || state == "完成")) {
//		$("#yesSpan").show();
//	} else {
//		$("#yesSpan").hide();
//	}
//	var handlemethod = $("#handlemethod").val();
//	if(handlemethod == "view"||$("#sampleOrder_state").val()=="2") {
//		settextreadonly();
//	}
//	var goal = $("#flag").val();
//	if(handlemethod == "modify") {
//		if(goal == "EDIT" || goal == "ADD") {
//			$("#sampleOrder_id").prop("readonly", false);
//		} else {
//			$("#sampleOrder_id").prop("readonly", "readonly");
//		}
//	}
//	if(handlemethod == "add"){
//		$("#oldOrder").show();
//		
//	}
//
//	var productId = $("#sampleOrder_productId").val();
//	// 上传附件(1.useType,2.modelType,3.id)
//	var mainFileInput = fileInput('1', 'sampleOrder', $("#sampleOrder_id").val());
//
//	//自定义字段
//	//fieldCustomFun();
//	//ai图片识别
//	$("#aiBtn").click(function() {
//		$("#AiPicture").val("");
//		$("#AiPicture").click();
//	});
//	$("#aiBtn2").click(function() {
//		$("#AiPicture2").val("");
//		$("#AiPicture2").click();
//	});
});

//跟据出生日期回填年龄
/*function fillAge(){
	var birth = $("#sampleOrder_birthDate").val();//2018-09-11
	if(birth!=null&&birth!=""){
		$.ajax({
			type: 'post',
			url: '/system/sample/sampleOrder/fillAgeByBirthDate.action',
			data: {
				birth: birth
			},
			success: function(data) {
				$("#sampleOrder_age").val(data);
			}
		})
	}
}*/

//选择老订单
function selectOrder(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.confirmSelected,
		content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action?flag=flag", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td")
			.eq(0).text();
			$.ajax({
				type: 'post',
				url: '/system/sample/sampleOrder/findSampleOrderById.action',
				data: {
					id: id
				},
				success: function(data) {
					var data = JSON.parse(data);
					console.log(data);
					$("#sampleOrder_name").val(data.sampleOrder.name);
					$("#sampleOrder_birthDate").val(data.birthDate);
					$("#sampleOrder_nation").val(data.sampleOrder.nation);
					$("#nativePlace").val(data.sampleOrder.nativePlace);
					$("#sampleOrder_medicalNumber").val(data.sampleOrder.medicalNumber);
					$("#sampleOrder_age").val(data.sampleOrder.age);
					$("#sampleOrder_IDcardNo").val(data.sampleOrder.idCard);
					$("#sampleOrder_oldId").val(data.sampleOrder.id);
					if(data.sampleOrder.familyId!=null){
						$("#sampleOrder_familyId_id").val(data.sampleOrder.familyId.id);
					}
					
					$("#ckGender input").each(function(i,v){
						if(v.value==data.sampleOrder.gender){
							v.setAttribute("checked",true);
						}
					});
					formm.render("checkbox");
				}
			})
			top.layer.close(index);
		},
	})
}

/**
 * onchange（参数）
 * 
 * @param id
 */
function change(id) {
	$("#" + id).css({
		"background-color": "white",
		"color": "black"
	});
}

function add() {
	window.location = window.ctx +
		"/system/sample/sampleOrder/editSampleOrder.action?type="+$("#order_type").val();
}

function list() {
	window.location = window.ctx +
		'/experiment/plasma/sampleReceiveEx/showSampleReceiveItemTable.action';
}

function validId() {
	$.ajax({
		type: "post",
		url: window.ctx + '/common/hasId.action',
		async: false,
		data: {
			id: $("#sampleOrder_id").val(),
			obj: 'SampleOrder'
		},
		success: function(data) {
			var obj = JSON.parse(data);
			if(obj.success) {
				if(obj.bool) {
					bool2 = true;
				} else {
					top.layer.msg(obj.msg);
				}
			} else {
				top.layer.msg(biolims.common.checkingFieldCodingFailure);
			}
		}
	});
}

function tjsp() {
	if($("#sampleOrder_confirmUser_name").val()==null||$("#sampleOrder_confirmUser_name").val()==""){
		top.layer.msg("请添加审核人,保存之后在执行此操作!");
		return false;
	}
		//订单为用户类型才可以提交
		top.top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {
			icon: 3,
			title: biolims.common.prompt,
			btn: biolims.common.selected
		}, function(index) {
			top.layer.open({
				title: biolims.common.approvalProcess,
				type: 2,
				anim: 2,
				area: ['800px', '500px'],
				btn: biolims.common.selected,
				content: window.ctx + "/workflow/processinstance/toStartView.action?formName=SampleOrder",
				yes: function(index, layer) {
					var datas = {
							userId: userId,
							userName: userName,
							formId: $("#sampleOrder_id").val(),
							title: $("#sampleOrder_name").val(),
							formName:'SampleOrder'
					}
					ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
						if(data.success) {
							top.layer.msg(biolims.common.submitSuccess);
							if(typeof callback == 'function') {
								callback(data);
							}
							dialogWin.dialog("close");
						} else {
							top.layer.msg(biolims.common.submitFail);
						}
					}, null);
					top.layer.close(index);
				},
				cancel: function(index, layer) {
					top.layer.close(index)
				}
				
			});
			top.layer.close(index);
		});

}

function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#sampleOrder_id").val();

	top.layer.open({
		title: biolims.common.approvalProcess,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx + "/workflow/processinstance/toCompleteTaskView.action?taskId=" + taskId + "&formId=" + formId,
		yes: function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#opinionVal").val();

			if(!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if(operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data: JSON.stringify(paramData),
					formId: formId,
					taskId: taskId,
					userId: window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action", reqData, function(data) {
					if(data.success) {
						top.layer.msg(biolims.common.submitSuccess);
						if(typeof callback == 'function') {}
					} else {
						top.layer.msg(biolims.common.submitFail);
					}
				}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}

	});
}

function save() {
	$("#radiosub").click();
//	//自定义字段
//	//拼自定义字段儿（实验记录）
//	var inputs = $("#fieldItemDiv input");
//	var options = $("#fieldItemDiv option");
//	var contentData = {};
//	var checkboxArr = [];
//	$("#fieldItemDiv .checkboxs").each(function(i, v) {
//		$(v).find("input").each(function(ii, inp) {
//			var k = inp.name;
//			if(inp.checked == true) {
//				checkboxArr.push(inp.value);
//				contentData[k] = checkboxArr;
//			}
//		});
//	});
//	var requiredField = requiredFilter();
//	if(!requiredField) {
//		return false;
//	}
//	inputs.each(function(i, inp) {
//		var k = inp.name;
//		if(inp.type != "checkbox") {
//			contentData[k] = inp.value;
//		}
//	});
//	options.each(function(i, opt) {
//		if(opt.selected == true) {
//			var k = opt.getAttribute("name");
//			contentData[k] = opt.value;
//		}
//	});
//	document.getElementById("fieldContent").value = JSON.stringify(contentData);
//	var changeLog = "客服订单录入-";
//	$('input[class="form-control"]').each(function(i, v) {
//		var valnew = $(v).val();
//		var val = $(v).attr("changelog");
//		if(val !== valnew) {
//			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
//		}
//	});
//
//	//子表日志
//	var changeLogItem = biolims.order.itemEdit + "：";
//	var data = saveItemjson($("#sampleInfoTable"));
//	changeLogItem = getChangeLog(data, $("#sampleInfoTable"), changeLogItem);
//	if(changeLogItem != biolims.order.itemEdit + "：") {
//		$("#changeLogItem").val(changeLogItem);
//	}
//	document.getElementById("changeLog").value = changeLog;

//	var goal = $("#flag").val();
//	var handlemethod = $("#handlemethod").val();
//	if(handlemethod == "modify" && checkSubmit() == true) {
//		document.getElementById('sampleOrderInfoJson').value = saveItemjson($("#sampleInfoTable"));
//		console.log($("#sampleOrderInfoJson").val());
//		top.layer.load(4, {
//			shade: 0.3
//		});
//		$("#form1").attr("action", "/system/sample/sampleOrder/save.action?flag=" + goal);
//		$("#form1").submit();
//		top.layer.closeAll();
//	} else {
//		$.ajax({
//			type: "post",
//			url: ctx + '/common/hasId.action',
//			data: {
//				id: $("#sampleOrder_id").val(),
//				obj: 'SampleOrder'
//			},
//			success: function(data) {
//				var data = JSON.parse(data);
//				if(data.message) {
//					top.layer.msg(data.message);
//				} else {
//					top.layer.load(4, {
//						shade: 0.3
//					});
//					$("#form1").attr("action", "/system/sample/sampleOrder/save.action?flag=" + goal);
//					$("#form1").submit();
//					top.layer.closeAll();
//				}
//			}
//		});
//	}
}

var changeId = $("#changeId").val();

function yesBtn() {
	window.location.href = ctx + "/system/sample/sampleOrderChange/editsampleOrderChange.action?id=" + changeId;
}

function editCopy() {
	window.location = window.ctx +
		'/system/sample/sampleOrder/copySampleOrder.action?id=' +
		$("#sampleOrder_id").val();
}

function changeState() {
	var paraStr = "formId=" + $("#sampleOrder_id").val() +
		"&tableId=sampleOrder";
	top.top.layer.confirm("状态完成之前请先保存", {
		icon: 3,
		title: biolims.common.prompt,
		btn: biolims.common.selected
	}, function(index) {
		top.layer.open({
			title: biolims.common.approvalProcess,
			type: 2,
			anim: 2,
			area: ['400px', '400px'],
			btn: biolims.common.selected,
			content: window.ctx +
				"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
				"&flag=changeState'",
			yes: function(index, layer) {
				top.layer.confirm(biolims.common.approve, {
					icon: 3,
					title: biolims.common.prompt,
					btn: biolims.common.selected
				}, function(index) {
					ajax("post", "/applicationTypeAction/exeFun.action", {
						applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
						formId: $("#sampleOrder_id").val()
					}, function(response) {
						var respText = response.message;
						if(respText == '') {
							window.location.reload();
						} else {
							top.layer.msg(respText);
						}
					}, null)
					top.layer.closeAll();
				})
	
			},
			cancel: function(index, layer) {
				top.layer.closeAll();
	
			}
	
		});
		top.layer.close(index);
	});
}

function makeSampleType() {
	var checks = "";
	$("[id='sck_checkedBoxTest']:checked").each(function() {
		checks += $(this).val() + ",";
	});
	document.getElementById("sampleOrder_sampleTypeId").value = checks;

}
/**
 * 复选框选中方法
 */
function viewSampleType() {

	var str1 = document.getElementById("sampleOrder_sampleTypeId").value;
	$("input[id='sck_checkedBoxTest']").each(function() {
		if(str1.indexOf($(this).val()) >= 0) {
			$(this).attr("checked", true);
		}
	});
}

function checkSubmit() {
	if($("#sampleOrder_id").val() == null || $("#sampleOrder_id").val() == "") {
		top.layer.msg(biolims.common.codeNotEmpty);
		return false;
	};
	return true;
}
// 送检医院与主治医生添加关联性
function showDoctors() {

	var sampleOrder_crmCustomer_id = $("#sampleOrder_crmCustomer_id").val();
	if(sampleOrder_crmCustomer_id == null || sampleOrder_crmCustomer_id == "") {
		top.layer.msg(biolims.common.selectedDoctor);
		return
	}

	top.layer.open({
		title: biolims.master.selectDoctor,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/crm/doctor/showDialogCrmPatientTable.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(
				0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(1).text();
			var mobile = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmPatientTable .chosed").children("td").eq(2).text();
			top.layer.close(index)
			$("#sampleOrder_attendingDoctor").val(name);
			$("#sampleOrder_attendingDoctorPhone").val(mobile);
		},
	})

}

function showCollectionManner() {
	top.layer.open({
		title: biolims.order.payType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=sflx", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder_collectionManner").val(id);
			$("#sampleOrder_collectionManner_name").val(name);
		},
	})
}
//医疗机构
function showHos() {
	top.layer.open({
		title: biolims.common.selectedDoctor,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/crm/customer/customer/crmCustomerSelectTable.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcrmCustomerTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder_crmCustomer_id").val(id);
			$("#sampleOrder_crmCustomer_name").val(name);
		},
	})

}

function setDicProbeFun(rec) {
	document.getElementById('sampleCancerTemp_attendingDoctor_id').value = rec.get('id');
	document.getElementById('sampleOrder_attendingDoctor').value = rec.get('name');
	document.getElementById('sampleOrder_attendingDoctorPhone').value = rec.get('mobile');
	top.layer.closeAll('iframe')
}

function showinspectionDepartment() {
	top.layer.open({
		title: biolims.sample.inspectionDepartmentName,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=ks", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder.inspectionDepartment.id").val(id);
			$("#sampleOrder_inspectionDepartment_name").val(name);
		},
	})
}

/*获取肿瘤类型*/
function showcancerTypeTable() {
	top.layer.open({
		title: biolims.sample.inspectionDepartmentName,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=cancerType", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder_dicCancarTyp_id").val(id);
			$("#sampleOrder_dicCancarTyp_name").val(name);
		},
	})
}

/*获取肿瘤分期*/
function showTimesTable() {
	top.layer.open({
		title: biolims.sample.inspectionDepartmentName,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=zlfqd", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder_cancerInstalment_id").val(id);
			$("#sampleOrder_cancerInstalment_name").val(name);
		},
	})
}

function setks(id, name) {
	$("#sampleOrder_inspectionDepartment_id").val(id);
	$("#sampleOrder_inspectionDepartment_name").val(name);
	top.layer.closeAll('iframe')
}
// 健康状况
function medicalHistoryFun() {
	top.layer.open({
		title: biolims.order.healthCondition,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=jkzk", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder_medicalHistoryId").val(id);
			$("#sampleOrder_medicalHistory").val(name);
		},
	})
};
// 正在用药情况
function sampleYongYaoFun() {
	top.layer.open({
		title: biolims.order.yongyao,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=yyqk", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder_yongyaoId").val(id);
			$("#sampleOrder_yongyao").val(name);
		},
	})
}
// 选择检查产品
function voucherProductFun() {

	top.layer.open({
		title: biolims.crm.selectProduct,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/com/biolims/system/product/showProductSelTree.action",
		yes: function(index, layer) {
			var name = [],
				id = [];
				price = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function(i, v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
				if($(v).children("td").eq(3).text()!=""){
					price.push($(v).children("td").eq(3).text());
				}
			});
			$("#sampleOrder_productId").val(id.join(","));
			$("#sampleOrder_productName").val(name.join(","));
			var sum = 0;
			for(var i=0;i<price.length;i++){
				var p = Number(price[i]);
				sum += p;
			}
			$("#sampleOrder_fee").val(sum);
			top.layer.close(index)
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

/**
 * 手机号验证
 * 
 * @param v
 */
function checktelephone(v) {
	var a = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(19[0-9]{1})|(14[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
	if(v.length != 11 || !v.match(a)) {
		message("不是完整的11位手机号或者正确的手机号!");
	}
}
/**
 * 邮箱验证
 * 
 * @param v
 */
function checkemail(v) {
	var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
	if(!v.match(myreg)) {
		message(("email格式不正确，请重新输入！"));
	}
}
/**
 * 获取姓名输入的input的value值
 * @returns
 */
/*function updateInputValue(){
//	$("[name='sampleOrder.name']").parent().children("span")
	alert($("#sampleOrder_name").val());
	
}
*/
/**
 * 选择电子病历编号
 */

function crmPatientTypeTwoFun() {
	var name=$("#sampleOrder_name").val();
	top.layer.open({
		title: biolims.sample.fillPatientId,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/crm/customer/patient/crmPatientSelectTable.action?name=" +
			encodeURIComponent(encodeURIComponent(name)), ''
		],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPatientTable .chosed").children("td").eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPatientTable .chosed").children("td").eq(1).text();
			var family = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addPatientTable .chosed").children("td").eq(5).text();
			$("#sampleOrder_medicalNumber").val(id);
			$("#sampleOrder_name").val(name);
			$("#sampleOrder_familyId_id").val(family);
			top.layer.close(index);
		},
	})
}
//运输方式
function yunshufs() {
	top.layer.open({
		title: biolims.order.ysfs,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=ysfs", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder_yushufs_id").val(id);
			$("#sampleOrder_yushufs_name").val(name);
		},
	})
}
//销售代表
function showcommissioner() {
	top.layer.open({
		title: biolims.common.selectTiveSelling,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=XSZ", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#sampleOrder_commissioner").val(id);
			$("#sampleOrder_commissioner_name").val(name);
		},
	})
}
//销售代表
function showconfirmUser() {
	top.layer.open({
		title: biolims.sample.pleaseSelectReviewer,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=QA003", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#releaseAudit_chargePerson").val(id);
			$("#releaseAudit_chargePerson").val(name)
		},
	})
}
function showconfirmUser1() {
	top.layer.open({
		title: biolims.sample.pleaseSelectReviewer,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=QA004", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#releaseAudit_qualityAuthorizer").val(id);
			$("#releaseAudit_qualityAuthorizer").val(name)
		},
	})
}
var loadAdvance;
// 查询代理商
function loadTestAdvance() {
	var options = {};
	options.width = document.body.clientWidth - 800;
	options.height = document.body.clientHeight - 40;
	loadAdvance = loadDialogPage(null, biolims.common.primary,
		"/crm/agent/primary/primaryTask/primaryTaskSelect.action", {
			"确定(Confirm)": function() {
				var operGrid = primaryTaskDialogGrid;
				var selectRecord = operGrid.getSelectionModel()
					.getSelections();
				if(selectRecord.length > 0) {
					$.each(selectRecord,
						function(i, obj) {
							$("#sampleOrder_primary")
								.val(obj.get("id"));
							$("#sampleOrder_primary_name").val(
								obj.get("name"));
						});
				} else {
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function setPrimaryTaskFun() {
	var operGrid = $("#show_dialog_primaryTask_div").data(
		"primaryTaskDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if(selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			$("#sampleOrder_primary").val(obj.get("id"));
			$("#sampleOrder_primary_name").val(obj.get("name"));
		});
	} else {
		message(biolims.common.selectYouWant);
		return;
	}
	loadAdvance.dialog("close");

}
//
//function makeSampleType1() {
//
//	var checks = "";
//	$("[id='sck_checkedBoxTest1']:checked").each(function() {
//		checks += $(this).val() + ",";
//	});
//	document.getElementById("sampleOrder_medicalHistory").value = checks;
//
//}
///**
// * 复选框选中方法
// */
//function viewSampleType23() {
//
//	var str1 = document.getElementById("sampleOrder_medicalHistory").value;
//	$("input[id='sck_checkedBoxTest1']").each(function() {
//		if (str1.indexOf($(this).val()) >= 0) {
//			$(this).attr("checked", true);
//		}
//	});
//}
//// yongyao
//function makeSampleType2() {
//
//	var checks = "";
//	$("[id='sck_checkedBoxTest2']:checked").each(function() {
//		checks += $(this).val() + ",";
//	});
//	document.getElementById("sampleOrder_yongyao").value = checks;
//
//}
///**
// * 复选框选中方法
// */
//function viewSampleType2() {
//
//	var str1 = document.getElementById("sampleOrder_yongyao").value;
//	$("input[id='sck_checkedBoxTest2']").each(function() {
//		if (str1.indexOf($(this).val()) >= 0) {
//			$(this).attr("checked", true);
//		}
//	});
//}

/*根据出生日期算出年龄*/
function jsGetAge() {
    var returnAge;
    var strBirthdayArr = $("#sampleOrder_birthDate").val().split("-");
    var birthYear = strBirthdayArr[0];
    var birthMonth = strBirthdayArr[1];
    var birthDay = strBirthdayArr[2];

    d = new Date();
    var nowYear = d.getFullYear();
    var nowMonth = d.getMonth() + 1;
    var nowDay = d.getDate();

    if (nowYear == birthYear) {
        returnAge = 1;//同年 则为0岁
    }
    else {
        var ageDiff = nowYear - birthYear; //年之差
        if (ageDiff > 0) {
            if (nowMonth == birthMonth) {
                var dayDiff = nowDay - birthDay;//日之差
                if (dayDiff < 0) {
                    returnAge = ageDiff - 1;
                }
                else {
                    returnAge = ageDiff;
                }
            }
            else {
                var monthDiff = nowMonth - birthMonth;//月之差
                if (monthDiff < 0) {
                    returnAge = ageDiff - 1;
                }
                else {
                    returnAge = ageDiff;
                }
            }
        }
        else {
            returnAge = -1;//返回-1 表示出生日期输入错误 晚于今天
        }
    }

    $("#sampleOrder_age").val(returnAge);
//    return returnAge;//返回周岁年龄

}
function getAge() {
	var age;
	// alert(cell);
	var aDate = new Date();
	var thisYear = aDate.getFullYear();
	var thisMonth = aDate.getMonth() + 1;
	var thisDay = aDate.getDate();
	var birth = parseDate(document.getElementById("sampleOrder_birthDate").value);
	// alert(birth);
	var birthy = birth.getFullYear(); // 出生年
	var birthm = birth.getMonth() + 1; // 出生月
	var birthd = birth.getDate(); // 出生日
	// 用当前年月日减去生日年月日
	var yearMinus = thisYear - birthy;
	var monthMinus = thisMonth - birthm;
	var dayMinus = thisDay - birthd;
	var age = ""; // 先大致赋值
	if(yearMinus < 0) { // 选了未来的年份
		age = 0;
	} else if(yearMinus == 0) { // 同年的，要么为1，要么为0
		if(monthMinus < 0) { // 选了未来的月份
			age = 0;
		} else if(monthMinus == 0) { // 同月份的
			if(dayMinus < 0) { // 选了未来的日期
				age = 0;
			} else if(dayMinus >= 0) {
				age = 0;
			}
		} else if(monthMinus > 0) {
			age = 0;
		}
	} else if(yearMinus > 1) {
		if(monthMinus < 0) { // 当前月>生日月
			age = yearMinus - 1;
		} else if(monthMinus == 0) { // 同月份的，再根据日期计算年龄
			if(dayMinus < 0) {
				age = yearMinus - 1 - 1;
			} else if(dayMinus >= 0) {
				age = yearMinus - 1;
			}
		} else if(monthMinus > 0) {
			age = yearMinus - 1;
		}
	} else if(yearMinus == 1) { // 去年出生日期

		if(monthMinus < 0) { // 当前月>生日月
			age = 1;
		} else if(monthMinus == 0) { // 同月份的，再根据日期计算年龄
			if(dayMinus <= 0) {
				age = 1;
			} else if(dayMinus > 0) {
				age = 0;
			}
		} else if(monthMinus > 0) {
			age = 0;
		}

	}
	$("#sampleOrder_age").val(age);
};

function parseDate(str) {
	if(str.match(/^\d{4}[\-\/\s+]\d{1,2}[\-\/\s+]\d{1,2}$/)) {
		return new Date(str.replace(/[\-\/\s+]/i, '/'));
	} else if(str.match(/^\d{8}$/)) {
		return new Date(str.substring(0, 4) + '/' + str.substring(4, 6) + '/' +
			str.substring(6));
	}
};

function fileUp() {
	if($("#sampleOrder_id").val() == "NEW") {
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}

function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=sampleOrder&id=" + $("#sampleOrder_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}

//自定义模块
function fieldCustomFun() {
	//获取检测项目
	var productId = $("#sampleOrder_productId").val();
	//获取自定义字段值的相关内容
	var fieldContent = $("#fieldContent").val();
	//查找自定义列表的订单模块的此检测项目的相关内容
	$.ajax({
		type: "post",
		url: window.ctx + "/system/customfields/findFieldByProductId.action",
		data: {
			productId: productId,
		},
		async: false,
		success: function(data) {
			var objValue = JSON.parse(data);
			if(objValue.success) {
				$.each(objValue.data, function(i, n) {
					var inputs = '';
					var disabled = n.readOnly ? ' ' : "disabled";
					var defaultValue = n.defaultValue ? n.defaultValue : ' ';
					if(n.fieldType == "checkbox") {
						var checkboxs = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							checkboxs += '<input type="checkbox" lay-ignore name=' + n.fieldName + ' value=' + jj + ' title=' + singleOptionNameArry[vv] + '>' + singleOptionNameArry[vv] + '';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group checkboxs " changelog=""><span class="input-group-addon">' + n.label + '</span>' + checkboxs + '</div></div>';
					} else if(n.fieldType == "radio") {
						var options = '';
						var singleOptionIdArry = n.singleOptionId.split("/");
						var singleOptionNameArry = n.singleOptionName.split("/");
						singleOptionIdArry.forEach(function(vv, jj) {
							options += '<option name=' + n.fieldName + ' value=' + jj + '>' + singleOptionNameArry[vv] + '</option>';
						});
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><select class="form-control">' + options + '</select></div></div>';
					} else if(n.fieldType == "date") {
						inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type="text" name=' + n.fieldName + ' required=' + n.isRequired + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
					} else {
						if(n.isRequired != "false") {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '<img class="requiredimage" src="/images/required.gif" /></span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						} else {
							inputs += '<div class="col-md-4 col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + n.label + '</span><input type=' + n.fieldType + ' name=' + n.fieldName + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
						}
					}
					$("#fieldItemDiv").append(inputs);
				});
			} else {
				top.layer.msg(biolims.customList.updateFailed);
			}
		}
	});

	//显示自定义字段的数据
	if(fieldContent != null && fieldContent != undefined && fieldContent != "") {
		if(fieldContent && fieldContent != "null") {
			var contentData = JSON.parse(fieldContent);
			for(var k in contentData) {
				$("#fieldItemDiv input").each(function(i, inp) {
					if(inp.name == k) {
						if(inp.type == "checkbox") {
							if(contentData[k].indexOf(inp.value) != -1) {
								inp.setAttribute("checked", true);
							}
						} else {
							inp.value = contentData[k];
						}
						inp.setAttribute("changelog", inp.value);
					}
				});
				$("#fieldItemDiv option").each(function(i, val) {
					if(k == val.getAttribute("name")) {
						if(val.value == contentData[k]) {
							val.setAttribute("selected", true);
							inp.setAttribute("changelog", inp.value);
						}
					}
				});
			};
		}
	}

	//日期格式化
	$("#fieldItemDiv").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//多选框 格式化
	$("#fieldItemDiv").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});

}

//Ai图片识别
function subimtBtn() {
	var form = $("#fileForm");
	var options = {
		url: ctx + '/common/aiUtils/AiTemplateReader.action',
		type: 'post',
		success: function(data) {
			console.log(data);
			var res = JSON.parse(data);
			var wordArray = res.data.ret;
			var str = "";
			for(var i = 0; i < wordArray.length; i++) {
				$("#" + wordArray[i].word_name).val(wordArray[i].word);
			}
		}
	};
	form.ajaxSubmit(options);
}
//Ai图片识别
function subimtBtn2(that) {
	if(that.files[0]) {
		top.layer.open({
			title: "图片裁剪",
			type: 2,
			skin: 'layui-layer-lan',
			area: ["98%", "600px"],
			shadeClose: true,
			content: window.ctx + "/system/sample/sampleOrderChange/casualMethod.action",
			success: function() {
				var files = that.files;
				var reader = new FileReader;
				reader.readAsDataURL(files[0]);
				reader.onload = function() {
					$('.layui-layer-iframe', parent.document).find("iframe").contents().find("img").attr("src", this.result);
				};
			},
			cancel: function(index, layer) {
				top.layer.close(index);
			}
		})
	}

}

//显示图片
function upLoadImg1() {
	top.layer.open({
		type: 2,
		title: "查看图片",
		area: ["35%", "80%"],
		shade: 0,
		content: window.ctx + "/system/sample/sampleOrder/showImg.action?id=" + $("#sampleOrder_id").val(),
	})
}
