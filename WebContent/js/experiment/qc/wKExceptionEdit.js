﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#wKException_id").val();
	if(id==""){
		load("/sample/sampleReceive/showSampleReceiveItemListByIsFull.action", { }, "#WKExceptionPage");
		$("#markup").css("width","75%");
	}
})	
function add() {
	window.location = window.ctx + "/experiment/qc/wKException/editWKException.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/qc/wKException/showWKExceptionList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#wKException", {
					userId : userId,
					userName : userName,
					formId : $("#wKException_id").val(),
					title : $("#wKException_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#wKException_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var wKQualityExceptionDivData = $("#wKQualityExceptiondiv").data("wKQualityExceptionGrid");
		document.getElementById('wKQualityExceptionJson').value = commonGetModifyRecords(wKQualityExceptionDivData);
	    var poolingQualityExceptionDivData = $("#poolingQualityExceptiondiv").data("poolingQualityExceptionGrid");
		document.getElementById('poolingQualityExceptionJson').value = commonGetModifyRecords(poolingQualityExceptionDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/qc/wKException/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/qc/wKException/copyWKException.action?id=' + $("#wKException_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#wKException_id").val() + "&tableId=wKException");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'文库异常',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/qc/wKException/showWKQualityExceptionList.action", {
				id : $("#wKException_id").val()
			}, "#wKQualityExceptionpage");
load("/experiment/qc/wKException/showPoolingQualityExceptionList.action", {
				id : $("#wKException_id").val()
			}, "#poolingQualityExceptionpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);