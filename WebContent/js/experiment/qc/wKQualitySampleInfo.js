﻿
var wKQualitySampleInfoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
	   name:'orderId',
	   type:"string"
   });
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkId',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'fragmentSize',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
//	   fields.push({
//		name:'qpcr',
//		type:"string"
//	});
	   fields.push({
		name:'specific',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
	   name:'reason',
	   type:"string"
   });
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'wKQualitySampleTask-id',
		type:"string"
	});
	    fields.push({
		name:'wKQualitySampleTask-name',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkType',
		type:"string"
	});
	   
   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
		   fields.push({
			name:'i5',
			type:"string"
		});
		   fields.push({
			name:'i7',
			type:"string"
		});
		   fields.push({
			name:'sampleNum',
			type:"string"
		});
		   fields.push({
				name:'tempId',
				type:"string"
			}); 
		   fields.push({
				name:'wkConcentration',
				type:"string"
			});
		   fields.push({
				name:'wkVolume',
				type:"string"
			});
		   fields.push({
				name:'wkSumTotal',
				type:"string"
			});
		   fields.push({
				name:'loopNum',
				type:"string"
			});
		   fields.push({
				name:'pcrRatio',
				type:"string"
			});
		   fields.push({
				name:'rin',
				type:"string"
			});
		   fields.push({
				name:'labCode',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-id',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-note',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-receiveDate',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-reportDate',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单编号',
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'INDEX',
		width:20*6
	});
	cm.push({
		dataIndex:'wkId',
		hidden : true,
		header:'文库编号',
		width:20*6
	});
	var wkTypeCobStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '6', '2100 or Caliper' ], [ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCobStore,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : true,
		header:'文库类型',
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i5',
		width:20*6
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i7',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:30*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机',
		width:30*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:30*6
	});
	cm.push({
		dataIndex:'fragmentSize',
		hidden : false,
		header:'片段长度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积',
		width:20*6
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:'单位',
		width:20*6
	});
//	cm.push({
//		dataIndex:'qpcr',
//		hidden : false,
//		header:'QPCR浓度',
//		width:20*6,
//		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
//	});
	cm.push({
		dataIndex:'specific',
		hidden : false,
		header:'比值',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkConcentration',
		header:'文库浓度ng/ul',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkVolume',
		header:'文库体积ul',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkSumTotal',
		header:'文库总量ng',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'loopNum',
		header:'循环数',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'pcrRatio',
		header:'扩增比例',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rin',
		header:'RIN',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeGresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeGresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果',
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向 ',
		width:15*10,
		editor : nextFlowCob
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'异常原因',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	

	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wKQualitySampleTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wKQualitySampleTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'techTaskId',
		hidden : true,
		header:'科技服务任务单',
		width:20*6,
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同ID',
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务',
		width:25*6
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:'开箱指令',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : false,
		header:'接收日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:'开箱指令',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQualitySampleTask/showWKQualitySampleInfoListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit=200;
	var opts={};
	opts.title="2100结果明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state = $("#wKQualitySampleTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKQualitySampleTask/delWKQualitySampleInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    

	opts.tbar.push({
		text : "批量数据",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_data2_div"), "批量数据", null, {
				"确定" : function() {
					var records = wKQualitySampleInfoGrid.getSelectRecord();
					if (records && records.length > 0) {
						var length = $("#length").val();
						var concentration2 = $("#concentration2").val();
//						var qpcr = $("#qpcr2").val();
						wKQualitySampleInfoGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("fragmentSize", length);
//							obj.set("qpcr", qpcr);
							obj.set("concentration", concentration2);
						});
						wKQualitySampleInfoGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "批量结果",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = wKQualitySampleInfoGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						wKQualitySampleInfoGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						wKQualitySampleInfoGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "批量下一步",
		handler : loadTestNextFlowCob
//		function() {
//			var records = wKQualitySampleInfoGrid.getSelectRecord();
//			if(records.length>0){
//				if(records.length>2){
//					var productId = new Array();
//					$.each(records, function(j, k) {
//						productId[j]=k.get("productId");
//					});
//					for(var i=0;i<records.length;i++){
//						if(i!=0&&productId[i]!=productId[i-1]){
//							message("检测项目不同！");
//							return;
//						}
//					}
//					loadTestNextFlowCob();
//				}else{
//					loadTestNextFlowCob();
//				}
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = wKQualitySampleInfoGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						wKQualitySampleInfoGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						wKQualitySampleInfoGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : "批量导入",
		handler : function() {
			
		
			//$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br />字段：接收样本号、体积、浓度、260/230、260/280");
			$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br />字段：样本号、片段长度、浓度");
			$("#many_bat_text").val("");
			$("#many_bat_text").attr("style", "width:465px;height: 339px");
			var options = {};
			options.width = 494;
			options.height = 508;
			loadDialogPage($("#many_bat_div"), "批量导入", null, {
				"确定" : function() {
					var positions = $("#many_bat_text").val();
					if (!positions) {
						message("请填写信息！");
						return;
					}
					var posiObj = {};
					var posiObj1 = {};
					var posiObj2 = {};
					var posiObj3 = {};
					var array = formatData(positions.split("\n"));
					$.each(array, function(i, obj) {
						var tem = obj.split("\t");
						posiObj[tem[0]] = tem[1];
						posiObj1[tem[0]] = tem[2];
						//posiObj2[tem[0]] = tem[3];
						//posiObj3[tem[0]] = tem[4];
					});
					var records = wKQualitySampleInfoGrid.getAllRecord();
					wKQualitySampleInfoGrid.stopEditing();
					$.each(records, function(i, obj) {
						if (posiObj[obj.get("code")]) {
							obj.set("fragmentSize", posiObj[obj.get("code")]);
						}
						if (posiObj1[obj.get("code")]) {
							obj.set("concentration", posiObj1[obj.get("code")]);
						}
//						if (posiObj2[obj.get("sampleInfo-code")]) {
//							obj.set("reason", posiObj2[obj.get("sampleInfo-code")]);
//						}
//						if (posiObj3[obj.get("sampleInfo-code")]) {
//							obj.set("od", posiObj3[obj.get("sampleInfo-code")]);
//						}
						//obj.set("unit-id","ng");
						//obj.set("unit-name","ng");
						//obj.set("result","1");
						//obj.set("method","1");
								
					});
					wKQualitySampleInfoGrid.startEditing(0, 0);
					$(this).dialog("close");
				}
			}, true, options);
		}
	});	
	opts.tbar.push({
		text : '导出列表',
		handler : exportexcel
	});
	opts.tbar.push({
		text : '提交样本',
		handler : submitSample
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
			 
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});	
	}
	wKQualitySampleInfoGrid=gridEditTable("wKQualitySampleInfodiv",cols,loadParam,opts);
	$("#wKQualitySampleInfodiv").data("wKQualitySampleInfoGrid", wKQualitySampleInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function exportexcel() {
	wKQualitySampleInfoGrid.title = '导出列表';
	var vExportContent = wKQualitySampleInfoGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = wKQualitySampleInfoGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=Qc2100Task&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = wKQualitySampleInfoGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = wKQualitySampleInfoGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}	
//提交样本
function submitSample(){
	var id=$("#wKQualitySampleTask_id").val();  
	if(wKQualitySampleInfoGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	var grid=wKQualitySampleInfoGrid.store;
	var record = wKQualitySampleInfoGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("submit")){
			flg=true;
		}
		if(record[i].get("nextFlowId")==""){
			message("有下一步未填写！");
			return;
		}
	}
	for(var i=0;i<grid.getCount();i++){
		if(grid.getAt(i).get("submit")==""){
			flg=true;
		}
		if(grid.getAt(i).get("nextFlowId")==""){
			message("有下一步未填写！");
			return;
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		var records = [];
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		ajax("post", "/experiment/qc/wKQualitySampleTask/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				wKQualitySampleInfoGrid.getStore().commitChanges();
				wKQualitySampleInfoGrid.getStore().reload();
				message("提交成功！");
			} else {
				loadMarsk.hide();
				message("提交失败！");
			}
		}, null);	
	}else{
		message("没有需要提交的样本！");
	}
}