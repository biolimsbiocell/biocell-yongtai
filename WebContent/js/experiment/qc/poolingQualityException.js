﻿
var poolingQualityExceptionGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
			name:'code',
			type:"string"
		});
	   fields.push({
		name:'wkType',
		type:"string"
	});
	   fields.push({
		name:'mixAmount',
		type:"string"
	});
	   fields.push({
		name:'mixBulk',
		type:"string"
	});
	   fields.push({
		name:'qpcr',
		type:"string"
	});
	   fields.push({
		name:'resultDecide',
		type:"string"
	});
	    fields.push({
		name:'nextFlow-id',
		type:"string"
	});
	    fields.push({
		name:'nextFlow-name',
		type:"string"
	});
	   fields.push({
		name:'handleIdea',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'isExecute',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'qualityException',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		header:'pooling编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkType',
		hidden : false,
		header:'文库类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'mixAmount',
		hidden : false,
		header:'混合量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'mixBulk',
		hidden : false,
		header:'混合体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'qpcr',
		hidden : false,
		header:'qpcr浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
//	cm.push({
//		dataIndex:'resultDecide',
//		hidden : false,
//		header:'结果判定',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var storeresultDecideCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '合格' ], [ '1', '不合格' ] ]
	});
	var resultDecideCob = new Ext.form.ComboBox({
		store : storeresultDecideCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'resultDecide',
		hidden : false,
		header:'结果判定',
		width:20*6,
		editor : resultDecideCob,
		renderer : Ext.util.Format.comboRenderer(resultDecideCob)
	});
//	cm.push({
//		dataIndex:'nextFlow-id',
//		hidden : true,
//		header:'下一步流向ID',
//		width:20*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:'下一步流向',
		width:20*10
	});
	cm.push({
		dataIndex:'handleIdea',
		hidden : false,
		header:'处理意见',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:'确认执行',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qualityException',
		hidden : true,
		header:'相关主表',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKException/showPoolingQualityExceptionListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="pooling质检异常";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKException/delPoolingQualityException.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	opts.tbar.push({
			text : '选择下一步流向',
			handler : selectnextFlowFun
		});
	
	
	
	
	
	
	
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = poolingQualityExceptionGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							poolingQualityExceptionGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	poolingQualityExceptionGrid=gridEditTable("poolingQualityExceptiondiv",cols,loadParam,opts);
	$("#poolingQualityExceptiondiv").data("poolingQualityExceptionGrid", poolingQualityExceptionGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectnextFlowFun(){
	var win = Ext.getCmp('selectnextFlow');
	if (win) {win.close();}
	var selectnextFlow= new Ext.Window({
	id:'selectnextFlow',modal:true,title:'选择下一步流向',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/StringSelect.action?flag=nextFlow' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectnextFlow.close(); }  }]  });     selectnextFlow.show(); }
	function setnextFlow(id,name){
		var gridGrid = $("#poolingQualityExceptiondiv").data("poolingQualityExceptionGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('nextFlow-id',id);
			obj.set('nextFlow-name',name);
		});
		var win = Ext.getCmp('selectnextFlow')
		if(win){
			win.close();
		}
	}
	
