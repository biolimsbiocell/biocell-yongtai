var qualityCheckDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'length',
		type:"string"
	});
	    fields.push({
		name:'qualityConcentrer',
		type:"string"
	});
	    fields.push({
		name:'molarity',
		type:"string"
	});
	    fields.push({
		name:'failReason',
		type:"string"
	});
	    fields.push({
		name:'testState',
		type:"string"
	});
	    fields.push({
		name:'handleState',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'length',
		header:'片段长度',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'qualityConcentrer',
		header:'质量浓度',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'molarity',
		header:'摩尔浓度',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'failReason',
		header:'异常原因',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'testState',
		header:'质检情况',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'handleState',
		header:'处理情况',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qualityCheck/showQualityCheckListJson.action";
	var opts={};
	opts.title="质检审核";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setQualityCheckFun(rec);
	};
	qualityCheckDialogGrid=gridTable("show_dialog_qualityCheck_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(qualityCheckDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
