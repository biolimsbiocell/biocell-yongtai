var wKQpcrDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'releasUser-id',
		type:"string"
	});
	    fields.push({
		name:'releasUser-name',
		type:"string"
	});
	    fields.push({
		name:'releaseDate',
		type:"string"
	});
	    fields.push({
		name:'testUser-id',
		type:"string"
	});
	    fields.push({
		name:'testUser-name',
		type:"string"
	});
	    fields.push({
		name:'testDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'releasUser-id',
		header:'下达人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'releasUser-name',
		header:'下达人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'releaseDate',
		header:'下达日期',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'testUser-id',
		header:'实验员ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'testUser-name',
		header:'实验员',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'testDate',
		header:'实验日期',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		header:'选择执行单ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:'选择执行单',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQpcrSampleTask/showWKQpcrListJson.action";
	var opts={};
	opts.title="文库质检";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setWKQpcrFun(rec);
	};
	wKQpcrDialogGrid=gridTable("show_dialog_wKQpcr_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(wKQpcrDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
