var qcPoolingAbnormalGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    	fields.push({
		name:'id',
		type:"string"
	});
    	fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
	   name:'orderId',
	   type:"string"
   });
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkId',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'template-id',
		type:"string"
	});
	   fields.push({
		name:'template-name',
		type:"string"
	});
	   fields.push({
		name:'sequencingReadLong',
		type:"string"
	});
	   fields.push({
		name:'sequencingType',
		type:"string"
	});
	   fields.push({
		name:'sequencingPlatform',
		type:"string"
	});  
	   fields.push({
		name:'product-id',
		type:"string"
	});
	   fields.push({
		name:'product-name',
		type:"string"
	});
	   fields.push({
		name:'totalAmount',
		type:"string"
	});
	   fields.push({
		name:'totalVolume',
		type:"string"
	});
	   fields.push({
		name:'fragmentSize',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'qpcr',
		type:"string"
	});
	   fields.push({
		name:'specific',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
	   name:'reason',
	   type:"string"
   });
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-id',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-name',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkType',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:'POOLING编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:'任务单编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'样本编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'wkId',
		hidden : false,
		header:'文库编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'INDEX',
		width:20*6
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:'模板ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'template-name',
		hidden : false,
		header:'模板',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingReadLong',
		hidden : false,
		header:'测序读长',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingType',
		hidden : true,
		header:'测序类型',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingPlatform',
		hidden : false,
		header:'测序平台',
		width:20*6,
	});
	cm.push({
		dataIndex:'product-id',
		hidden : true,
		header:'业务类型ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'product-name',
		hidden : false,
		header:'业务类型',
		width:20*6,
	});
	cm.push({
		dataIndex:'totalAmount',
		hidden : false,
		header:'总数据量',
		width:20*6,
	});
	cm.push({
		dataIndex:'totalVolume',
		hidden : true,
		header:'总体积',
		width:20*6,
	});
	cm.push({
		dataIndex:'fragmentSize',
		hidden : false,
		header:'片段长度',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'qpcr',
		hidden : false,
		header:'QPCR浓度',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'specific',
		hidden : false,
		header:'比值',
		width:10*6,
		
	});
	var storeGresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeGresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'处理结果',
		width:20*6,
		//editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	var nextFlow = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '上机测序'
			}, {
				id : '1',
				name : '终止'
			}, {
				id : '2',
				name : '重抽血'
			}, {
				id : '3',
				name : '入库'
			}, {
				id : '4',
				name : '向建库组反馈'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : true,
		header:'下一步流向',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(nextFlow),editor: nextFlow
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'异常原因',
		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	var isExecuteCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var isExecuteCob = new Ext.form.ComboBox({
		store : isExecuteCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : false,
		header:'是否执行',
		width:20*6,
		editor : isExecuteCob,
		renderer : Ext.util.Format.comboRenderer(isExecuteCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6,
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6,
	});
	var wkTypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : true,
		header:'文库类型',
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qcQpcrTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qcQpcrTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKException/showQcPoolingAbnormalListJson.action";
	var opts={};
	opts.title="Pooling异常";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		text : "批量执行",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok_div"), "批量执行", null, {
				"确定" : function() {
					var records = qcPoolingAbnormalGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isExecute2").val();
						qcPoolingAbnormalGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isExecute", isExecute);
						});
						qcPoolingAbnormalGrid.startEditing(0, 0);
					}else{
						message("请先选择数据！");
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : searchs
//	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : saves
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	
	qcPoolingAbnormalGrid=gridEditTable("qcPoolingAbnormaldiv",cols,loadParam,opts);
	$("#qcPoolingAbnormaldiv").data("qcPoolingAbnormalGrid", qcPoolingAbnormalGrid);
})
//保存pooling
function saves(){	
	var selectRecord = qcPoolingAbnormalGrid.getSelectionModel();
	var inItemGrid = $("#qcPoolingAbnormaldiv").data("qcPoolingAbnormalGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if(selectRecord.getSelections().length>0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/qc/wKException/saveQcPoolingAbnormal.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#qcPoolingAbnormaldiv").data("qcPoolingAbnormalGrid").getStore().commitChanges();
					$("#qcPoolingAbnormaldiv").data("qcPoolingAbnormalGrid").getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);			
		});
	}else{
		message("没有需要保存的数据！");
	}
}
//检索
function searchs() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
		
			
			commonSearchAction(qcPoolingAbnormalGrid);
			$(this).dialog("close");

		},
		"清空" : function() {
			form_reset();

		}
	}, true, option);
}
function selectQcPoolingInfo(){
	commonSearchAction(qcPoolingAbnormalGrid);
	$("#qcPoolingAbnormal_wkId").val("");
	$("#qcPoolingAbnormal_poolingCode").val("");
}