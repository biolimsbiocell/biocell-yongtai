var qcQpcrTaskItemmGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
    	fields.push({
		name:'orderNumber',
		type:"string"
	});
    	fields.push({
		name:'orderId',
		type:"string"
	});
    	fields.push({
		name:'name',
		type:"string"
	});
    	fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
		name:'template-id',
		type:"string"
	});
	   fields.push({
		name:'template-name',
		type:"string"
	});
	   fields.push({
		name:'sequencingReadLong',
		type:"string"
	});
	   fields.push({
		name:'sequencingType',
		type:"string"
	});
	   fields.push({
		name:'sequencingPlatform',
		type:"string"
	});  
	   fields.push({
		name:'product-id',
		type:"string"
	});
	   fields.push({
		name:'product-name',
		type:"string"
	});
	   fields.push({
		name:'totalAmount',
		type:"string"
	});
	   fields.push({
		name:'totalVolume',
		type:"string"
	});
	   fields.push({
		name:'others',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-id',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:'任务单编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:'POOLING编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:'模板ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'template-name',
		hidden : false,
		header:'模板',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingReadLong',
		hidden : false,
		header:'测序读长',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingType',
		hidden : true,
		header:'测序类型',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingPlatform',
		hidden : false,
		header:'测序平台',
		width:20*6,
	});
	cm.push({
		dataIndex:'product-id',
		hidden : true,
		header:'业务类型ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'product-name',
		hidden : false,
		header:'业务类型',
		width:20*6,
	});
	cm.push({
		dataIndex:'totalAmount',
		hidden : false,
		header:'总数据量',
		width:20*6,
	});
	cm.push({
		dataIndex:'totalVolume',
		hidden : true,
		header:'总体积',
		width:20*6,
	});
	cm.push({
		dataIndex:'others',
		hidden : true,
		header:'其他',
		width:20*6,
	});
	var resultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '不合格' ], [ '1', '合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : resultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'处理结果',
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:'实验排序号',
		width:10*7,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'失败原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : false,
		header:'步骤编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:30*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6,
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6,
	});
	cm.push({
		dataIndex:'qcQpcrTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qcQpcrTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQpcrSampleTask/showQcQpcrTaskItemmListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="Pooling明细";
	opts.height =  document.body.clientHeight-280;
	opts.tbar = [];
	var state = $("#wKQpcrSampleTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKQpcrSampleTask/delQcQpcrTaskItemm.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qcQpcrTaskItemmGrid.getStore().commitChanges();
				qcQpcrTaskItemmGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectwKQpcrFun
//		});
	/*opts.tbar.push({
		text : '添加质控品',
		handler :addQuality
	});*/
	/*opts.tbar.push({
		text : '开始实验',
		handler :startTest
	});*/
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = qcQpcrTaskItemmGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							qcQpcrTaskItemmGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	}
	qcQpcrTaskItemmGrid=gridEditTable("qcQpcrTaskItemmdiv",cols,loadParam,opts);
	$("#qcQpcrTaskItemmdiv").data("qcQpcrTaskItemmGrid", qcQpcrTaskItemmGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectwKQpcrFun(){
	var win = Ext.getCmp('selectwKQpcr');
	if (win) {win.close();}
	var selectwKQpcr= new Ext.Window({
	id:'selectwKQpcr',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKQpcrSelect.action?flag=wKQpcr' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectwKQpcr.close(); }  }]  });     selectwKQpcr.show(); }
	function setwKQpcr(id,name){
		var gridGrid = $("#qcQpcrTaskItemmdiv").data("qcQpcrTaskItemmGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('qcQpcrTask-id',id);
			obj.set('qcQpcrTask-name',name);
		});
		var win = Ext.getCmp('selectwKQpcr')
		if(win){
			win.close();
		}
	}
	
