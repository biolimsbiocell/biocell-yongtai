﻿
var wKQualityTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string",
	});
	   fields.push({
		name:'endTime',
		type:"string",
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'codes',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'wKQualitySampleTask-id',
		type:"string"
	});
	    fields.push({
		name:'wKQualitySampleTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10
	});
	//鼠标聚焦时触发事件 
	var reciveUser =new Ext.form.TextField({
            allowBlank: false
    });
	reciveUser.on('focus', function() {
		
		loadTestUser();
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:'实验员',
		width:20*6,

		editor : reciveUser
	});
	cm.push({
		dataIndex:'code',
		header:'步骤编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepName',
		header:'步骤名称',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤ID',
		width:20*6
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:'开始时间',
		width:20*6
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:'结束时间',
		width:20*6
	});
	cm.push({
		dataIndex:'codes',
		hidden : false,
		header:'关联样本',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wKQualitySampleTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wKQualitySampleTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQualitySampleTask/showWKQualityTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="执行步骤明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#wKQualitySampleTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKQualitySampleTask/delExecuteStepItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectwkQualityFun
//		});
	
	
	
	
	
	
	
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = wKQualityTemplateGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							wKQualityTemplateGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : '填加明细',
		handler : templateSelectQc
	});
	/*opts.tbar.push({
	 * iconCls : 'application_print',
		text : '打印执行单',
		handler : stampOrder
	});*/
	opts.tbar.push({
		iconCls : 'application_start',
		text : '开始执行',
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : '执行结束',
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : '生成结果明细',
		handler : addSuccess
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : '打印执行单',
		handler : stampOrder
	});
	}
	wKQualityTemplateGrid=gridEditTable("wKQualityTemplatediv",cols,loadParam,opts);
	$("#wKQualityTemplatediv").data("wKQualityTemplateGrid", wKQualityTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//打印执行单
function stampOrder(){
	var id=$("#wKQualitySampleTask_template").val();
	if(id==""){
		message("请先选择模板!");
		return;
	}else{
		var url = '__report=QC2100Task.rptdesign&id=' + $("#wKQualitySampleTask_id").val();
		commonPrint(url);
	}
}
function selectwkQualityFun(){
	var win = Ext.getCmp('selectwkQuality');
	if (win) {win.close();}
	var selectwkQuality= new Ext.Window({
	id:'selectwkQuality',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKQualitySelect.action?flag=wkQuality' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectwkQuality.close(); }  }]  });     selectwkQuality.show(); }
	function setwkQuality(id,name){
		var gridGrid = $("#wKQualityTemplatediv").data("wKQualityTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('wKQualitySampleTask-id',id);
			obj.set('wKQualitySampleTask-name',name);
		});
		var win = Ext.getCmp('selectwkQuality');
		if(win){
			win.close();
		}
	}
	
	
	//获取开始时的时间
	function getStartTime(){
		var getDate = new Date();
		var getTime=getDate.toLocaleString( );
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=wKQualityTemplateGrid.getSelectionModel();
		var setNum = wKQualityReagentGrid.store;
		var selectRecords=wKQualityItemGrid.getSelectionModel();
		if(selectRecords.getSelections().length>0){
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
				for(var i=0; i<setNum.getCount();i++){
					var num = setNum.getAt(i).get("itemId");
					if(num==obj.get("code")){
						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
					}
				}
			});
		}else{
			message("请先选择数据！");
		}
		//将所选的样本，放到关联样本
		var selRecord=wKQualityTemplateGrid.getSelectRecord();
		var codes = "";
			$.each(selectRecords.getSelections(), function(i, obj) {
				codes += obj.get("code")+",";
			});
			$.each(selRecord, function(i, obj) {
				obj.set("codes", codes);
			});
	}else{
		message("请选择实验样本");
	}
	}
	//获取执行结束的时间
	function getEndTime(){
		var getIndex = wKQualityTemplateGrid.store; 
		var getIndexs = wKQualityTemplateGrid.getSelectionModel().getSelections();
		var setRecord=wKQualityItemGrid.store;
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord = wKQualityTemplateGrid.getSelectionModel();
		if(selectRecord.getSelections().length>0){
			$.each(selectRecord.getSelections(),function(i,obj){
				obj.set("endTime",str);
				//将步骤编号，赋值到质检明细表
				var codes = obj.get("codes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(scode[j]==setRecord.getAt(i).get("code")){
							setRecord.getAt(i).set("stepNum",obj.get("code"));
						}
					}
				}
				//将当前行的关联样本传到下一行
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("codes",codes);
			});
		}
	}
	
	//生成结果明细
	function addSuccess(){
		

		var num =$("#wKQualitySampleTask_template").val();
			if(num!=""){
				var setNum = wKQualityReagentGrid.store;
				var selectRecords = wKQualityItemGrid.store;
					for(var i=0;i<setNum.getCount();i++){
						setNum.getAt(i).set("sampleNum",selectRecords.getCount());
				}


		var getRecord = wKQualityItemGrid.store;
		var selectRecord = wKQualityTemplateGrid.getSelectionModel();
		var selRecord = wKQualitySampleInfoGrid.store;
		if(selectRecord.getSelections().length > 0){
			$.each(selectRecord.getSelections(), function(i, obj) {
				var isRepeat = false;
				var codes = obj.get("codes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<scode.length; i++){
					for(var j=0;j<selRecord.getCount();j++){
						var getv = scode[i];
						var setv = selRecord.getAt(j).get("code");
						if(getv == setv){
							isRepeat = true;
							message("有重复的数据，请重新选择！");
							break;					
						}
					}
				}
				if(!isRepeat){
					var codes = obj.get("codes");
					var scode = new Array();
					scode = codes.split(",");
					
					for(var i=0; i<scode.length; i++){
						for(var j=0;j<getRecord.getCount();j++){
							if(scode[i] == getRecord.getAt(j).get("code")){
								var ob = wKQualitySampleInfoGrid.getStore().recordType;
								wKQualitySampleInfoGrid.stopEditing();
								var p = new ob({});
								p.isNew = true;
								p.set("tempId",getRecord.getAt(j).get("tempId"));
								p.set("orderId",getRecord.getAt(j).get("orderId"));
								p.set("code",getRecord.getAt(j).get("code"));
								p.set("sampleCode",getRecord.getAt(j).get("sampleCode"));
								p.set("wkType",getRecord.getAt(j).get("wkType"));
								p.set("indexa",getRecord.getAt(j).get("indexa"));
								p.set("result","1");
								p.set("reason",getRecord.getAt(j).get("reason"));
								p.set("patientName",getRecord.getAt(j).get("patientName"));
								p.set("productId",getRecord.getAt(j).get("productId"));
								p.set("productName",getRecord.getAt(j).get("productName"));
								p.set("inspectDate",getRecord.getAt(j).get("inspectDate"));
								p.set("acceptDate",getRecord.getAt(j).get("acceptDate"));
								p.set("idCard",getRecord.getAt(j).get("idCard"));
								p.set("phone",getRecord.getAt(j).get("phone"));
								p.set("sequenceFun",getRecord.getAt(j).get("sequenceFun"));
								p.set("reportDate",getRecord.getAt(j).get("reportDate"));
								p.set("volume",getRecord.getAt(j).get("volume"));
								p.set("unit",getRecord.getAt(j).get("unit"));
								p.set("techTaskId",getRecord.getAt(j).get("techTaskId"));
								p.set("contractId",getRecord.getAt(j).get("contractId"));
								p.set("projectId",getRecord.getAt(j).get("projectId"));
								p.set("orderType",getRecord.getAt(j).get("orderType"));
								p.set("classify",getRecord.getAt(j).get("classify"));
								p.set("sampleType",getRecord.getAt(j).get("sampleType"));
								p.set("i5",getRecord.getAt(j).get("i5"));
								p.set("i7",getRecord.getAt(j).get("i7"));
								p.set("wkConcentration",getRecord.getAt(j).get("wkConcentration"));
								p.set("wkVolume",getRecord.getAt(j).get("wkVolume"));
								p.set("wkSumTotal",getRecord.getAt(j).get("wkSumTotal"));
								p.set("loopNum",getRecord.getAt(j).get("loopNum"));
								p.set("pcrRatio",getRecord.getAt(j).get("pcrRatio"));
								p.set("rin",getRecord.getAt(j).get("rin"));
								p.set("labCode",getRecord.getAt(j).get("labCode"));
								p.set("sampleInfo-id",getRecord.getAt(j).get("sampleInfo-id"));
								p.set("sampleInfo-note",getRecord.getAt(j).get("sampleInfo-note"));
								p.set("sampleInfo-receiveDate",getRecord.getAt(j).get("sampleInfo-receiveDate"));
								p.set("sampleInfo-reportDate",getRecord.getAt(j).get("sampleInfo-reportDate"));
								ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
									model : "Qc2100Task",productId:getRecord.getAt(j).get("productId")
								}, function(data) {
									p.set("nextFlowId",data.dnextId);
									p.set("nextFlow",data.dnextName);
								}, null);
								message("生成结果成功！");
								wKQualitySampleInfoGrid.getStore().add(p);
								wKQualitySampleInfoGrid.startEditing(0,0);
							}
						}
					}
					
				}
			});
		}else{
			var selRecord = wKQualitySampleInfoGrid.store;
			var flag;
			
			var getRecord = wKQualityItemGrid.getAllRecord();
			$.each(getRecord,function(a,b){
				flag = true;
				for(var j1=0;j1<selRecord.getCount();j1++){
					var getv = b.get("code");
					var setv = selRecord.getAt(j1).get("code");
					if(getv == setv){
						flag = false;
						message("有重复的数据，请重新选择！");
						break;					
					}
				}
				if(flag==true){
					var ob = wKQualitySampleInfoGrid.getStore().recordType;
					wKQualitySampleInfoGrid.stopEditing();
					var p = new ob({});
					p.isNew = true;
					p.set("tempId",b.get("tempId"));
					p.set("orderId",b.get("orderId"));
					p.set("code",b.get("code"));
					p.set("sampleCode",b.get("sampleCode"));
					p.set("wkType",b.get("wkType"));
					p.set("indexa",b.get("indexa"));
					p.set("result","1");
					p.set("reason",b.get("reason"));
					p.set("patientName",b.get("patientName"));
					p.set("productId",b.get("productId"));
					p.set("productName",b.get("productName"));
					p.set("inspectDate",b.get("inspectDate"));
					p.set("acceptDate",b.get("acceptDate"));
					p.set("idCard",b.get("idCard"));
					p.set("phone",b.get("phone"));
					p.set("sequenceFun",b.get("sequenceFun"));
					p.set("reportDate",b.get("reportDate"));
					p.set("volume",b.get("volume"));
					p.set("unit",b.get("unit"));
					p.set("techTaskId",b.get("techTaskId"));
					p.set("contractId",b.get("contractId"));
					p.set("projectId",b.get("projectId"));
					p.set("orderType",b.get("orderType"));
					p.set("classify",b.get("classify"));
					p.set("sampleType",b.get("sampleType"));
					p.set("i5",b.get("i5"));
					p.set("i7",b.get("i7"));
					p.set("wkConcentration",b.get("wkConcentration"));
					p.set("wkVolume",b.get("wkVolume"));
					p.set("wkSumTotal",b.get("wkSumTotal"));
					p.set("loopNum",b.get("loopNum"));
					p.set("pcrRatio",b.get("pcrRatio"));
					p.set("rin",b.get("rin"));
					p.set("labCode",b.get("labCode"));
					p.set("sampleInfo-id",b.get("sampleInfo-id"));
					p.set("sampleInfo-note",b.get("sampleInfo-note"));
					p.set("sampleInfo-receiveDate",b.get("sampleInfo-receiveDate"));
					p.set("sampleInfo-reportDate",b.get("sampleInfo-reportDate"));
					ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
						model : "Qc2100Task",productId:getRecord.getAt(j).get("productId")
					}, function(data) {
						p.set("nextFlowId",data.dnextId);
						p.set("nextFlow",data.dnextName);
					}, null);
					message("生成结果成功！");
					wKQualitySampleInfoGrid.getStore().add(p);
					wKQualitySampleInfoGrid.startEditing(0,0);
				}
			});
		}

			}else{
				message("请选择实验模板！");
			}
	}
	//选择实验步骤
	function templateSelectQc(){
		var option = {};
		option.width = 605;
		option.height = 558;
		loadDialogPage(null, "选择实验步骤", "/experiment/qc/wKQualitySampleTask/showTemplateQcWaitList.action?id="+$("#id_parent_hidden").val(), {
			"确定" : function() {
				var operGrid = $("#template_wait_grid_div").data("grid");
				var ob = wKQualityTemplateGrid.getStore().recordType;
				wKQualityTemplateGrid.stopEditing();
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
//						for(var i=0;i<arr.length-1;i++){
							var p = new ob({});
							p.isNew = true;
							p.set("code", obj.get("code"));
							p.set("name", obj.get("name"));
//							p.set("blood-sequencingName", obj.get("sequencingName"));
//							p.set("blood-reportMan-id", obj.get("reportMan-id"));	
//							p.set("blood-patientId-name", obj.get("patientId-name"));	
							wKQualityTemplateGrid.getStore().add(p);
					});
					wKQualityTemplateGrid.startEditing(0, 0);
					$(this).dialog("close");
					$(this).dialog("remove");
				} else {
					message("请选择您要选择的数据");
					return;
				}
			}
		}, true, option);

		
	}
	//加载实验员
	function loadTestUser(){
		var win = Ext.getCmp('loadTestUser');
		if (win) {win.close();}
		var loadTestUser= new Ext.Window({
		id:'loadTestUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
			 loadTestUser.close(); }  }]  });     loadTestUser.show(); }
		function setreciveUserFun(id,name){
			var gridGrid = $("#wKQualityTemplatediv").data("wKQualityTemplateGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('reciveUser-id',id);
				obj.set('reciveUser-name',name);
			});
			var win = Ext.getCmp('loadTestUser')
			if(win){
				win.close();
			}
		}
