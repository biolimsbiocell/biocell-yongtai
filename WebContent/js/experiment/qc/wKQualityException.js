﻿
var wKQualityExceptionGrid;
$(function(){
	var cols={};
	cols.sm=true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
    	name:'sampleCode',
    	type:"string"
    });
	    fields.push({
		name:'wkCode',
		type:"string"
	});
	    fields.push({
		name:'indexa',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'wkType',
		type:"string"
	});
	    fields.push({
		name:'length',
		type:"string"
	});
	    fields.push({
		name:'qualityConcentrer',
		type:"string"
	});
	    fields.push({
		name:'reason',
		type:"string"
	});
	    fields.push({
		name:'nextFlowId',
		type:"string"
	});
	    fields.push({
		name:'nextFlow',
		type:"string"
	});
	    fields.push({
		name:'result',
		type:"string"
	});
	    fields.push({
		name:'method',
		type:"string"
	});

	    fields.push({
	    name:'note',
	    type:"string"
	});
	    fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
			name:'submit',
			type:"string"
		});
	    fields.push({
			name:'isRun',
			type:"string"
		});
	    fields.push({
			name:'orderId',
			type:"string"
		});
	    fields.push({
			name:'classify',
			type:"string"
		});
	    fields.push({
			name:'batch',
			type:"string"
		});
	    fields.push({
			name:'sampleType',
			type:"string"
		});
	    fields.push({
			name:'i5',
			type:"string"
		});
	    fields.push({
			name:'i7',
			type:"string"
		});
	    fields.push({
			name:'sampleNum',
			type:"string"
		});
	    
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:'样本编号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'原始样本编号',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'wkCode',
		header:'文库编号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'indexa',
		header:'INDEX',
		width:10*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		header:'样本类型',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleNum',
		header:'样本数量',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'i5',
		header:'i5',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'i7',
		header:'i7',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:20*6,
		hidden:true,
		sortable:true
	});
	var wkTypeCobStore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '2', '2100 or Caliper' ], [ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCobStore,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : false,
		header:'文库类型',
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6,
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6,
	});	
	cm.push({
		dataIndex:'length',
		header:'片段长度',
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'qualityConcentrer',
		header:'浓度',
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reason',
		header:'异常原因',
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'qualityConcentrer',
		header:'浓度',
		width:20*6,
		hidden : true,
		sortable:true
	});
//	var storenextFlowCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', 'Pooling' ], [ '1', '重抽血' ],[ '2', '重质检' ],['3','终止'] ,['4','异常反馈至项目管理'],['5','入库']]
//	});
//	var nextFlowCob = new Ext.form.ComboBox({
//		store : storenextFlowCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
//	cm.push({
//		dataIndex:'nextFlow',
//		hidden : false,
//		header:'下一步流向',
//		width:20*6,
////		editor : nextFlowCob,
//		renderer : Ext.util.Format.comboRenderer(nextFlowCob)
//	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		sortable:true,
		editor : nextFlowCob
	});
	var storeresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ],[ '0', '不合格' ]]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		header:'处理结果',
		hidden : false,
		width:20*6,
		sortable:true,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
//	var storemethodCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', 'Pooling' ], [ '1', '重抽血' ],[ '2', '重质检' ],['3','终止'] ,['4','入库']]
//	});
//	var methodCob = new Ext.form.ComboBox({
//		store : storemethodCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理意见',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
		//editor : methodCob,
		//renderer : Ext.util.Format.comboRenderer(methodCob)
	});
	var storeresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ],[ '0', '否' ]]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isRun',
		header:'是否执行',
		hidden : false,
		width:20*6,
		sortable:true,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:20*6,
		hidden:false,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单ID',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'batch',
		header : '批次号',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
//	var type="2";
//	loadParam.url=ctx+"/project/feedback/sampleFeedback/showQualityFeedbackListJson.action?type="+type;
	loadParam.url=ctx+"/experiment/qc/wKException/showQc2100AbnormalListJson.action";
	var opts={};
	opts.title="2100质控异常";
	opts.height=document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		text : "批量下一步",
		handler : function() {
			var records = wKQualityExceptionGrid.getSelectRecord();
			if(records.length>0){
				if(records.length>2){
					var productId = new Array();
					$.each(records, function(j, k) {
						productId[j]=k.get("productId");
					});
					for(var i=0;i<records.length;i++){
						if(i!=0&&productId[i]!=productId[i-1]){
							message("检测项目不同！");
							return;
						}
					}
					loadTestNextFlowCob();
				}else{
					loadTestNextFlowCob();
				}
				
			}else{
				message("请选择数据!");
			}
		}
	});
	opts.tbar.push({
		text : "批量执行",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok2100_div"), "批量执行", null, {
				"确定" : function() {
					var records = wKQualityExceptionGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isRun").val();
						wKQualityExceptionGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isRun", isExecute);
						});
						wKQualityExceptionGrid.startEditing(0, 0);
					}else{
						message("请先选择数据！");
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		iconCls : 'application_search',
//		text : '检索',
//		handler : search
//	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	wKQualityExceptionGrid=gridEditTable("wKQualityExceptiondiv",cols,loadParam,opts);
	$("#wKQualityExceptiondiv").data("wKQualityExceptionGrid", wKQualityExceptionGrid);
});

//
//

//保存
function save(){	
	//var selectRecord = wKQualityExceptionGrid.getSelectionModel();
	//var inItemGrid = $("#wKQualityExceptiondiv").data("wKQualityExceptionGrid");
	var itemJson = commonGetModifyRecords(wKQualityExceptionGrid);
	if(itemJson.length>0){
		//$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/qc/wKException/saveWKQualityException.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#wKQualityExceptiondiv").data("wKQualityExceptionGrid").getStore().commitChanges();
					$("#wKQualityExceptiondiv").data("wKQualityExceptionGrid").getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);			
		//});
	}else{
		message("没有需要保存的数据！");
	}
}
//检索
function selectQc2100Info(){
	commonSearchActionByMo(wKQualityExceptionGrid,"1");
	$("#wKQualityException_wkCode").val("");
	$("#wKQualityException_sampleCode").val("");
	$("#wKQualityException_method").val("");
	$("#wKQualityException_wkType").val("");
}
//下一步流向
function loadTestNextFlowCob(){
	var records1 = wKQualityExceptionGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=Qc2100Task&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = wKQualityExceptionGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}