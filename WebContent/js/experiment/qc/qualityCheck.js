var qualityCheckGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'wkType',
		type:"string"
	});
	    fields.push({
		name:'wkCode',
		type:"string"
	});
	    fields.push({
		name:'sampleCode',
		type:"string"
	});
	    fields.push({
		name:'length',
		type:"string"
	});
	    fields.push({
		name:'qualityConcentrer',
		type:"string"
	});
	    fields.push({
		name:'qpcrCon',
		type:"string"
	});
	    fields.push({
		name:'failReason',
		type:"string"
	});
	    fields.push({
		name:'result',
		type:"string"
	});
	    fields.push({
		name:'nextFlow',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'classify',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'样本编号',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'wkCode',
		header:'文库编号',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:30*6,
		hidden:true,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var wkTypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '6', '2100 or Caliper' ], [ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : false,
		header:'文库类型',
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
});
	cm.push({
		dataIndex:'length',
		header:'片段长度',
		width:10*10,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qualityConcentrer',
		header:'质量浓度',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qpcrCon',
		header:'QPCR浓度',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'failReason',
		header:'异常原因',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var testStateCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var testState = new Ext.form.ComboBox({
		store : testStateCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果',
		width:20*6,
		sortable:true,
		editor : testState,
		renderer : Ext.util.Format.comboRenderer(testState)
	});
	var handleState = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : 'Pooling'
			}, {
				id : '1',
				name : '让步上机'
			}, {
				id : '2',
				name : '重新纯化'
			}, {
				id : '3',
				name : '停止检测'
			}, {
				id : '4',
				name : '向建库组反馈'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : false,
		header:'下一步流向',
		width:25*6,
		sortable:true,
		renderer: Ext.util.Format.comboRenderer(handleState),editor: handleState
	});
	/*cm.push({
		dataIndex:'handleState',
		header:'处理情况',
		width:20*6,
		
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*6,
		
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单ID',
		width:50*6,
		sortable:true
	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qualityCheck/showQualityCheckListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title="质检审核";
	opts.height=document.body.clientHeight;
//	opts.rowselect=function(id){
//		$("#selectId").val(id);
//	};
//	opts.rowdblclick=function(id){
//		$('#selectId').val(id);
////		edit();
//	};
	opts.tbar.push({
		text : "批量结果",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = qualityCheckGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						qualityCheckGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						qualityCheckGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "批量下一步",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_next_div"), "批量下一步", null, {
				"确定" : function() {
					var records = qualityCheckGrid.getSelectRecord();
					if (records && records.length > 0) {
						var nextFlow = $("#nextFlow").val();
						qualityCheckGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("nextFlow", nextFlow);
						});
						qualityCheckGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
	opts.tbar.push({
		text : '保存',
		handler : save
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	qualityCheckGrid=gridEditTable("qualityCheckdiv",cols,loadParam,opts);
	$("#qualityCheckdiv").data("qualityCheckGrid", qualityCheckGrid);
});
//保存
function save(){	
	var selectRecord = qualityCheckGrid.getSelectionModel();
	var inItemGrid = $("#qualityCheckdiv").data("qualityCheckGrid");
	var itemJson = commonGetModifyRecords(inItemGrid);
	if(selectRecord.getSelections().length>0){
		$.each(selectRecord.getSelections(), function(i, obj) {
			ajax("post", "/experiment/qc/qualityCheck/saveQualityCheck.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					$("#qualityCheckdiv").data("qualityCheckGrid").getStore().commitChanges();
					$("#qualityCheckdiv").data("qualityCheckGrid").getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);			
		});
	}else{
		message("没有需要保存的数据！");
	}
}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/qc/qualityCheck/editQualityCheck.action?id=' + id;
}
//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
		
			
			commonSearchAction(qualityCheckGrid);
			$(this).dialog("close");

		},
		"清空" : function() {
			form_reset();

		}
	}, true, option);
}