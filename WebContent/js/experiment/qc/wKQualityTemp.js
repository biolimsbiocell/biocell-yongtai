
var wKQualityTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'wkType',
		type:"string"
	});
	   fields.push({
		name:'fragminSize',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'wkAccept-id',
		type:"string"
	});
	    fields.push({
		name:'wkAccept-name',
		type:"string"
	});
	    
	    fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
    fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
		   fields.push({
			name:'i5',
			type:"string"
		});
		   fields.push({
				name:'i7',
				type:"string"
			});
		   fields.push({
				name:'indexa',
				type:"string"
			});
		   fields.push({
				name:'sampleNum',
				type:"string"
			});
		   
		   fields.push({
				name:'wkConcentration',
				type:"string"
			});
		   fields.push({
				name:'wkVolume',
				type:"string"
			});
		   fields.push({
				name:'wkSumTotal',
				type:"string"
			});
		   fields.push({
				name:'loopNum',
				type:"string"
			});
		   fields.push({
				name:'pcrRatio',
				type:"string"
			});
		   fields.push({
				name:'rin',
				type:"string"
			});
		   fields.push({
				name:'labCode',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-id',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-note',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-receiveDate',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-reportDate',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单编号',
		width:20*6,
		hidden : true,
	});
	cm.push({
		dataIndex:'code',
		header:'样本编号',
		width:20*7,
		sortable:true,
		hidden : false
	});
	cm.push({
		dataIndex:'wkCode',
		header:'文库编号',
		width:20*6,
		hidden : true
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		sortable:true,
		width:20*6
		
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
	});
	var wkTypeCobs = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '2', '2100 质控' ],[ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCobs,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : false,
		header:'文库类型',
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'INDEX',
		width:20*6
	});
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i5',
		width:20*6
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i7',
		width:20*6
	});
	cm.push({
		dataIndex:'wkConcentration',
		hidden : true,
		header:'文库浓度ng/ul',
		width:20*6
	});
	cm.push({
		dataIndex:'wkVolume',
		hidden : true,
		header:'文库体积ul',
		width:20*6
	});
	cm.push({
		dataIndex:'wkSumTotal',
		hidden : true,
		header:'文库总量ng',
		width:20*6
	});
	cm.push({
		dataIndex:'loopNum',
		hidden : true,
		header:'循环数',
		width:20*6
	});
	cm.push({
		dataIndex:'pcrRatio',
		hidden : true,
		header:'扩增比例',
		width:20*6
	});
	cm.push({
		dataIndex:'rin',
		hidden : true,
		header:'RIN',
		width:20*6
	});
	cm.push({
		dataIndex:'fragminSize',
		hidden : true,
		header:'片段化大小',
		width:20*6,
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '不合格'
			}, {
				id : '1',
				name : '合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'处理结果',
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'异常原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6,
	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:30*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:'接收日期',
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6,
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:25*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:'储位',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
	});
	cm.push({
		dataIndex:'wkAccept-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
	});
	cm.push({
		dataIndex:'wkAccept-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:'单位',
		width:20*6,
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:20*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:20*6
	});
	cm.push({
		dataIndex:'techTaskId',
		hidden : false,
		header:'科技服务任务单',
		width:20*6,
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同ID',
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:'临床/科技服务',
		width:20*6,
		hidden : true,
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:'开箱指令id',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : false,
		header:'接收日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:'开箱指令',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKAccept/showWKQualityTempListJson.action";
	loadParam.limit=10000;
	var opts={};
	opts.title="待实验样本";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = wKQualityTempGrid.getAllRecord();
							var store = wKQualityTempGrid.store;

							var isOper = true;
							var buf = [];
							wKQualityTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("wkCode")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								})
							});
							
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("wkCode"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message("没有匹配到的样本有："+nolist);
							}
							wKQualityTempGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message("样本号核对不符，请检查！");
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							wKQualityTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		text : '科技服务',
		handler : techDNAService
	});
	wKQualityTempGrid=gridEditTable("wKQualityTempdiv",cols,loadParam,opts);
	$("#wKQualityTempdiv").data("wKQualityTempGrid", wKQualityTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//添加任务到子表
function addItem(){
	var selectRecord=wKQualityTempGrid.getSelectionModel();
	var selRecord=wKQualityItemGrid.store;
	var count=1;
	var max=0;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv==obj.get("id")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					break;					
				}
			}
			if(!isRepeat){
			var ob = wKQualityItemGrid.getStore().recordType;
			wKQualityItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
//			//截取拼接编号
//			var str=obj.get("sampleCode");
//			var s1=str.substring(0,1);
//			var s2=str.substring(2);
//			var s3=s1+"D"+s2;
//			
//			ajax("post", "/experiment/qc/wKQualitySampleTask/selectCodeCount.action", {
//				code : str
//			}, function(data) {
//				if (data.success) {
//						
//						if(data.data==0){
//							p.set("sampleId",s3+"A");
//							
//						}else if(data.data==1){
//							p.set("sampleId",s3+"B");
//							
//						}else if(data.data==2){
//							p.set("sampleId",s3+"C");
//							
//						}else if(data.data==3){
//							p.set("sampleId",s3+"D");
//							
//						}else{
//							message("请重新取样！");
//							return;
//						}
//						
//						wKQualityItemGrid.getStore().add(p);
//				}
//			});
			p.set("tempId",obj.get("id"));
			p.set("orderId",obj.get("orderId"));
			p.set("code",obj.get("code"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("wkId",obj.get("wkCode"));
			p.set("wkType",obj.get("wkType"));
			p.set("patientName",obj.get("patientName"));
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("inspectDate",obj.get("inspectDate"));
			p.set("acceptDate",obj.get("acceptDate"));
			p.set("idCard",obj.get("idCard"));
			p.set("phone",obj.get("phone"));
			p.set("sequenceFun",obj.get("sequenceFun"));
			p.set("reportDate",obj.get("reportDate"));
			p.set("orderNumber",Number(max)+count);
			
			p.set("volume",obj.get("volume"));
			p.set("unit",obj.get("unit"));
			p.set("rowCode",obj.get("rowCode"));
			p.set("colCode",obj.get("colCode"));
			p.set("counts",obj.get("counts"));
			p.set("techTaskId",obj.get("techTaskId"));
			p.set("contractId",obj.get("contractId"));
			p.set("projectId",obj.get("projectId"));
			p.set("orderType",obj.get("orderType"));
			p.set("classify",obj.get("classify"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("i5",obj.get("i5"));
			p.set("i7",obj.get("i7"));
			p.set("indexa",obj.get("indexa"));
			p.set("sampleNum",obj.get("sampleNum"));
			p.set("wkConcentration",obj.get("wkConcentration"));
			p.set("wkVolume",obj.get("wkVolume"));
			p.set("wkSumTotal",obj.get("wkSumTotal"));
			p.set("loopNum",obj.get("loopNum"));
			p.set("pcrRatio",obj.get("pcrRatio"));
			p.set("rin",obj.get("rin"));
			p.set("labCode",obj.get("labCode"));
			p.set("sampleInfo-id",obj.get("sampleInfo-id"));
			p.set("sampleInfo-note",obj.get("sampleInfo-note"));
			p.set("sampleInfo-receiveDate",obj.get("sampleInfo-receiveDate"));
			p.set("sampleInfo-reportDate",obj.get("sampleInfo-reportDate"));
//			var wkCode = obj.get("wkCode");
//			var index = wkCode.charAt(wkCode.length-1);
////			alert(index.charAt(index.length-1));
//			p.set("indexa",index);
			wKQualityItemGrid.getStore().add(p);
			count++;
		}
			
		});
		wKQualityItemGrid.startEditing(0, 0);
		}
	
}

//科技服务
function techDNAService(){
	 	var options = {};
	 	options.width = document.body.clientWidth - 470;
	 	options.height = document.body.clientHeight - 80;
		loadDialogPage(null, "选择实验任务单", "/technology/wk/techJkServiceTask/showTechJkServiceTaskDialogList.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						var id1=obj.get("id");
						
						wKQualityTempGrid.store.reload();
						var filter1 = function(record, id){
							var flag = true;
							if(id1){
								if (record.get("orderId").indexOf(id1)>=0){
									flag = true;
								}
								else{
									return false;
								}
							 }
							return flag;
						};
						var onStoreLoad1 = function(store, records, options){
						  store.filterBy(filter1);
						};
						wKQualityTempGrid.store.on("load", onStoreLoad1);
					});
					
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}