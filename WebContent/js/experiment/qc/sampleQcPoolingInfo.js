
var sampleQcPoolingInfoGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    	fields.push({
		name:'id',
		type:"string"
	});
    	fields.push({
		name:'poolingCode',
		type:"string"
	});
	   fields.push({
	   name:'orderId',
	   type:"string"
   });
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkId',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'template-id',
		type:"string"
	});
	   fields.push({
		name:'template-name',
		type:"string"
	});
	   fields.push({
		name:'sequencingReadLong',
		type:"string"
	});
	   fields.push({
		name:'sequencingType',
		type:"string"
	});
	   fields.push({
		name:'sequencingPlatform',
		type:"string"
	});  
	   fields.push({
		name:'product-id',
		type:"string"
	});
	   fields.push({
		name:'product-name',
		type:"string"
	});
	   fields.push({
		name:'totalAmount',
		type:"string"
	});
	   fields.push({
		name:'totalVolume',
		type:"string"
	});
	   fields.push({
		name:'fragmentSize',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'qpcr',
		type:"string"
	});
	   fields.push({
		name:'specific',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
	   name:'reason',
	   type:"string"
   });
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-id',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-name',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkType',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'poolingCode',
		hidden : false,
		header:'POOLING编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:'任务单编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'样本编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'wkId',
		hidden : true,
		header:'文库编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本编号',
		width:20*6,
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'INDEX',
		width:20*6
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:'模板ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'template-name',
		hidden : false,
		header:'模板',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingReadLong',
		hidden : false,
		header:'测序读长',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingType',
		hidden : true,
		header:'测序类型',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequencingPlatform',
		hidden : false,
		header:'测序平台',
		width:20*6,
	});
	cm.push({
		dataIndex:'product-id',
		hidden : true,
		header:'业务类型ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'product-name',
		hidden : false,
		header:'业务类型',
		width:20*6,
	});
	cm.push({
		dataIndex:'totalAmount',
		hidden : false,
		header:'总数据量',
		width:20*6,
	});
	cm.push({
		dataIndex:'totalVolume',
		hidden : true,
		header:'总体积',
		width:20*6,
	});
	cm.push({
		dataIndex:'fragmentSize',
		hidden : false,
		header:'片段长度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'qpcr',
		hidden : false,
		header:'QPCR浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'specific',
		hidden : false,
		header:'比值',
		width:10*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeGresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeGresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'处理结果',
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	var nextFlow = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '上机测序'
			}, {
				id : '1',
				name : '终止'
			}, {
				id : '2',
				name : '重抽血'
			}, {
				id : '3',
				name : '入库'
			}, {
				id : '4',
				name : '向建库组反馈'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden : true,
		header:'下一步流向',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(nextFlow),editor: nextFlow
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'异常原因',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6,
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6,
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:50*6,
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6,
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机',
		width:20*6,
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6,
	});
	var wkTypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : true,
		header:'文库类型',
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qcQpcrTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qcQpcrTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQpcrSampleTask/showSampleQcPoolingInfoListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="Pooling结果明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#wKQpcrSampleTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKQpcrSampleTask/delSampleQcPoolingInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectwKQpcrFun
//		});
	
	
	
	
	
	
	
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = sampleQcPoolingInfoGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							sampleQcPoolingInfoGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	}
	sampleQcPoolingInfoGrid=gridEditTable("sampleQcPoolingInfodiv",cols,loadParam,opts);
	$("#sampleQcPoolingInfodiv").data("sampleQcPoolingInfoGrid", sampleQcPoolingInfoGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectwKQpcrFun(){
	var win = Ext.getCmp('selectwKQpcr');
	if (win) {win.close();}
	var selectwKQpcr= new Ext.Window({
	id:'selectwKQpcr',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKQpcrSelect.action?flag=wKQpcr' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectwKQpcr.close(); }  }]  });     selectwKQpcr.show(); }
	function setwKQpcr(id,name){
		var gridGrid = $("#sampleQcPoolingInfodiv").data("sampleQcPoolingInfoGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('qcQpcrTask-id',id);
			obj.set('qcQpcrTask-name',name);
		});
		var win = Ext.getCmp('selectwKQpcr')
		if(win){
			win.close();
		}
	}
	
