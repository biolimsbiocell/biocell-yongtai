var qpcrjdTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'labCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'sumTotal',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'qpcrjdTask-id',
		type:"string"
	});
	   fields.push({
			name:'qpcrjdTask-name',
			type:"string"
		});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	   fields.push({
			name:'flux',
			type:"string"
		});
	   fields.push({
			name:'yhhzh',
			type:"string"
		});
		   fields.push({
			name:'hhzh',
			type:"string"
		});
		   fields.push({
			name:'xylr',
			type:"string"
		});
		   fields.push({
			name:'clr',
			type:"string"
		});
		   fields.push({
			name:'flr',
			type:"string"
		});
		   fields.push({
			name:'xzlr',
			type:"string"
		});
		   fields.push({
			name:'qtlr',
			type:"string"
		});
		   fields.push({
			name:'tl',
			type:"string"
		});
		   fields.push({
				name:'zlnd',
				type:"string"
			});
		   fields.push({
				name:'mend',
				type:"string"
			});
		   fields.push({
				name:'note',
				type:"string"
			});
		   fields.push({
				name:'wkpdcd',
				type:"string"
			});
		   
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'富集文库',
		width:20*7

		 
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'上机分组',
		width:20*6
	});cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:'预混合编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:'混合组号',
		width:20*6
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : false,
		header:'混合后文库长度（bp）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'zlnd',
		hidden : false,
		header:'质量浓度（ng/ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'mend',
		hidden : false,
		header:'摩尔浓度（nM）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:'文库体积（ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:'血液文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:'ctDNA文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:'FFPE文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:'新鲜组织文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:'其他文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	
	
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:'测序类型',
		width:20*6
		
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'测序平台',
		width:20*6
		
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'测序读长',
		width:20*6
	
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		editor : nextFlowCob
	});
	
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qpcrjdTask-id',
		hidden : true,
		header:'相关主表id',
		width:20*6
	
	});
	cm.push({
		dataIndex:'qpcrjdTask-name',
		hidden : true,
		header:'相关主表',
		width:20*6
	
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="文库定量QPCR结果";
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	var state = $("#qpcrjdTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/qpcrjd/qpcrjdTask/delQpcrjdTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};	
	opts.tbar.push({
		text : "批量下一步",
		handler : function() {
			var records = qpcrjdTaskResultGrid.getSelectRecord();
			if (records && records.length > 0) {
//				var options = {};
//				options.width = 400;
//				options.height = 300;
//				loadDialogPage($("#bat_result_div"), "批量下一步", null, {
//					"确定" : function() {
//							var result = $("#nextFlow").val();
//							var nextFlowId = $("#nextFlowId").val();
//							qpcrjdTaskResultGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								obj.set("nextFlow", result);
//								obj.set("nextFlowId",nextFlowId);
//							});
//							qpcrjdTaskResultGrid.startEditing(0, 0);
//						
//						$(this).dialog("close");
//					}
//				}, true, options);
				loadTestNextFlowCob();
			}else{
				message("请选择数据！");
			}
		}
	});
	
	
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});	
	}
	qpcrjdTaskResultGrid=gridEditTable("qpcrjdTaskResultdiv",cols,loadParam,opts);
	$("#qpcrjdTaskResultdiv").data("qpcrjdTaskResultGrid", qpcrjdTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = qpcrjdTaskResultGrid.getSelectRecord();
	var sequencingPlatform="";
	$.each(records1, function(j, k) {
		sequencingPlatform=k.get("productName");
	});
	
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/showPoolingTaskNextFlow.action?model=QpcrjdTask&sequencingPlatform="+sequencingPlatform, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qpcrjdTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = qpcrjdTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}	


