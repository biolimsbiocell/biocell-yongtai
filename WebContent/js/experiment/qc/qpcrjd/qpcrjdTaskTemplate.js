var qpcrjdTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'qpcrjdTask-id',
		type:"string"
	});
	    fields.push({
		name:'qpcrjdTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
	
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6,
		
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10,
		
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:'实验员',
		width:20*10
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤编id',
		width:20*6,
		
			});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'步骤编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:'步骤名称',
		width:20*6,
		
		
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:'开始时间',
		width:20*6,
		
		
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:'结束时间',
		width:20*6,
		
		
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'状态',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : true,
		header:'关联样本',
		width:20*6,
	
	});
	cm.push({
		dataIndex:'qpcrjdTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		
	});
	cm.push({
		dataIndex:'qpcrjdTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="执行步骤";
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/qpcrjd/qpcrjdTask/delQpcrjdTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : '填加明细',
		handler : templateSelect
	});
	opts.tbar.push({
		text : '实验员',
		handler : loadTestUser
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : '打印执行单',
		handler : stampOrder
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : '开始执行',
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : '执行结束',
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : '生成结果明细',
		handler : addSuccess
	});
	
	qpcrjdTaskTemplateGrid=gridEditTable("qpcrjdTaskTemplatediv",cols,loadParam,opts);
	$("#qpcrjdTaskTemplatediv").data("qpcrjdTaskTemplateGrid", qpcrjdTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

	//选择实验员
	function loadTestUser(){
		var win = Ext.getCmp('loadTestUser');
		if (win) {win.close();}
		var loadTestUser= new Ext.Window({
		id:'loadTestUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
			 loadTestUser.close(); }  }]  });     loadTestUser.show(); 
	}
	function setreciveUserFun(id,name){
		var gridGrid = $("#qpcrjdTaskTemplatediv").data("qpcrjdTaskTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reciveUser-id',id);
			obj.set('reciveUser-name',name);
		});
		var win = Ext.getCmp('loadTestUser');
		if(win){
			win.close();
		}
	}
	var selreciveUserVal = function(win) {
		var operGrid = userDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#qpcrjdTaskTemplatediv").data("qpcrjdTaskTemplateGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('reciveUser-id',rec.get('id'));
				obj.set('reciveUser-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};

	//打印执行单
	function stampOrder(){
		var id=$("#qpcrjdTask_template").val();
		if(id==""){
			message("请先选择模板!");
			return;
		}else{
			var url = '__report=qpcrjdTaskTask.rptdesign&id=' + $("#qpcrjdTask_id").val();
			commonPrint(url);}
	}
	//生成结果明细
	function addSuccess(){	

		var num =$("#qpcrjdTask_template").val();
			if(num!=""){
				var setNum = qpcrjdTaskReagentGrid.store;
				var selectRecords = qpcrjdTaskItemGrid.store;
					for(var i=0;i<setNum.getCount();i++){
						setNum.getAt(i).set("sampleNum",selectRecords.getCount());
				}


		var getRecord = qpcrjdTaskItemGrid.getSelectionModel().getSelections();
		var selectRecord = qpcrjdTaskTemplateGrid.getSelectionModel().getSelections();
		var selRecord = qpcrjdTaskResultGrid.store;
		if(selectRecord.length>0){
					$.each(selectRecord, function(i, obj) {
						var isRepeat = true;
						var codes = obj.get("sampleCodes");
						var scode = new Array();
						scode = codes.split(",");
						for(var i1=0; i1<scode.length; i1++){
							for(var j1=0;j1<selRecord.getCount();j1++){
								var getv = scode[i1];
								var setv = selRecord.getAt(j1).get("tempId");
								if(getv == setv){
									isRepeat = false;
									message("有重复的数据，请重新选择！");
									break;					
								}
							}
						}
						if(isRepeat){
								$.each(getRecord,function(a,b){
									var ob = qpcrjdTaskResultGrid.getStore().recordType;
									qpcrjdTaskResultGrid.stopEditing();
									var p = new ob({});
									p.isNew = true;
									p.set("tempId",b.get("sampleCode"));
									p.set("sampleCode",b.get("sampleCode"));
									p.set("hhzh",b.get("hhzh"));
									p.set("wkpdcd",b.get("wkpdcd"));
									p.set("zlnd",b.get("wknd"));
									p.set("productId",b.get("productId"));
									p.set("productName",b.get("productName"));
									p.set("name",b.get("name"));
									p.set("tl",parseFloat(b.get("tl"))-parseFloat(b.get("sample")));
									
									qpcrjdTaskResultGrid.getStore().add(p);
									qpcrjdTaskResultGrid.startEditing(0,0);
								});
								message("生成成功！");
						}
					});
				
		}else{
			var selRecord = qpcrjdTaskResultGrid.store;
			var flag;
			
			var getRecord = qpcrjdTaskItemGrid.getAllRecord();
			$.each(getRecord,function(a,b){
				flag = true;
				for(var j1=0;j1<selRecord.getCount();j1++){
					var getv = b.get("sampleCode");
					var setv = selRecord.getAt(j1).get("tempId");
					if(getv == setv){
						flag = false;
						message("有重复的数据，请重新选择！");
						break;					
					}
				}
				if(flag==true){
						var ob = qpcrjdTaskResultGrid.getStore().recordType;
						qpcrjdTaskResultGrid.stopEditing();
						var p = new ob({});
						p.isNew = true;
						p.set("tempId",b.get("sampleCode"));
						p.set("sampleCode",b.get("sampleCode"));
						p.set("hhzh",b.get("hhzh"));
						p.set("wkpdcd",b.get("wkpdcd"));
						p.set("zlnd",b.get("wknd"));
						p.set("productId",b.get("productId"));
						p.set("productName",b.get("productName"));
						p.set("name",b.get("name"));
						p.set("tl",parseFloat(b.get("tl"))-parseFloat(b.get("sample")));
						qpcrjdTaskResultGrid.getStore().add(p);
						qpcrjdTaskResultGrid.startEditing(0,0);
						message("生成成功！");
					}
				
			});
		}

			}else{
				message("请选择实验模板！");
			}
	}

	//获取开始时的时间
	function getStartTime(){
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=qpcrjdTaskTemplateGrid.getSelectionModel();
		var setNum = qpcrjdTaskReagentGrid.store;
		var selectRecords=qpcrjdTaskItemGrid.getSelectionModel();
		if(selectRecords.getSelections().length>0){
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
				for(var i=0; i<setNum.getCount();i++){
					var num = setNum.getAt(i).get("itemId");
					if(num==obj.get("code")){
						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
					}
				}
			});
		}else{
			message("请先选择数据！");
		}
		
		//将所选的样本，放到关联样本
		var selRecord=qpcrjdTaskTemplateGrid.getSelectRecord();
		var codes = "";
			$.each(selectRecords.getSelections(), function(i, obj) {
				codes += obj.get("sampleCode")+",";
			});
			$.each(selRecord, function(i, obj) {
				obj.set("sampleCodes", codes);
			});
		}else{
			message("请先选择实验样本！");
		}
	}
	//获取停止时的时间
	function getEndTime(){
			var setRecord=qpcrjdTaskItemGrid.store;
			var d = new Date();
			var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
			var selectRecord=qpcrjdTaskTemplateGrid.getSelectionModel();
			var getIndex = qpcrjdTaskTemplateGrid.store;
			var getIndexs = qpcrjdTaskTemplateGrid.getSelectionModel().getSelections();
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					obj.set("endTime",str);	
					//将步骤编号，赋值到明细表
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i=0; i<setRecord.getCount(); i++){
						for(var j=0; j<scode.length; j++){
							if(scode[j]==setRecord.getAt(i).get("sampleCode")){
								setRecord.getAt(i).set("stepNum",obj.get("code"));
							}
						}
					}
					//将当前行的关联样本传到下一行
					getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
				});
			}
			
			
	}
	
	
	//选择实验步骤
	function templateSelect(){
		var option = {};
		option.width = 605;
		option.height = 558;
		loadDialogPage(null, "选择实验步骤", "/experiment/qc/qpcrjd/qpcrjdTask/showTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
			"确定" : function() {
				var operGrid = $("#template_wait_grid_div").data("grid");
				var ob = qpcrjdTaskTemplateGrid.getStore().recordType;
				qpcrjdTaskTemplateGrid.stopEditing();
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code", obj.get("code"));
							p.set("name", obj.get("name"));
							qpcrjdTaskTemplateGrid.getStore().add(p);
					});
					qpcrjdTaskTemplateGrid.startEditing(0, 0);
					$(this).dialog("close");
					$(this).dialog("remove");
				} else {
					message("请选择您要选择的数据");
					return;
				}
			}
		}, true, option);
	}
