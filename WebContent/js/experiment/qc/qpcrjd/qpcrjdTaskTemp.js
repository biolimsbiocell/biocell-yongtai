var qpcrjdTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'labCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'yhhzh',
			type:"string"
		});
		   fields.push({
			name:'hhzh',
			type:"string"
		});
		   fields.push({
			name:'xylr',
			type:"string"
		});
		   fields.push({
			name:'clr',
			type:"string"
		});
		   fields.push({
			name:'flr',
			type:"string"
		});
		   fields.push({
			name:'xzlr',
			type:"string"
		});
		   fields.push({
			name:'qtlr',
			type:"string"
		});
		   fields.push({
			name:'tl',
			type:"string"
		});
		   fields.push({
			name:'wknd',
			type:"string"
		});
		   fields.push({
			name:'wkpdcd',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
	
	});
	
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'富集文库',
		width:20*7
		
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'上机分组',
		width:20*6
		
	});
	cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:'预混合编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:'混合组号',
		width:20*6
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:'血液文库比例%',
		width:20*6
	});
	cm.push({
		dataIndex:'clr',
		hidden : false,
		header:'ctDNA文库比例%',
		width:20*6
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:'FFPE文库比例%',
		width:20*6
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:'新鲜组织文库比例%',
		width:20*6
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:'其他文库比例%',
		width:20*6
	});
	
	cm.push({
		dataIndex:'wknd',
		hidden : false,
		header:'调整后文库浓度（ng/ul）',
		width:20*6
	});
	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:'调整后文库体积（ul）',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'调整后文库总量',
		width:20*6
		
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积',
		width:20*6
		
	});
	
	cm.push({
		dataIndex:'wkpdcd',
		hidden : true,
		header:'各组文库片段长度',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:'测序类型',
		width:20*6
		
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'测序平台',
		width:20*6
		
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'测序读长',
		width:20*6,
	
	});
	
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:20*6,
		
		renderer: formatDate
		
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
		
		renderer: formatDate
		
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:20*6,
		
		renderer: formatDate
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:20*6
		
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskTempListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="待文库定量QPCR样本";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = qpcrjdTaskTempGrid.getAllRecord();
							var store = qpcrjdTaskTempGrid.store;

							var isOper = true;
							var buf = [];
							qpcrjdTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							qpcrjdTaskTempGrid.getSelectionModel().selectRows(buf);
							if(isOper==false){
								message("样本号核对不符，请检查！");
								
							}else{
								addItem();
							}
							qpcrjdTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
//	opts.tbar.push({
//		text : '科技服务',
//		handler : techDNAService
//	});
	qpcrjdTaskTempGrid=gridEditTable("qpcrjdTaskTempdiv",cols,loadParam,opts);
	$("#qpcrjdTaskTempdiv").data("qpcrjdTaskTempGrid", qpcrjdTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//添加任务到子表
function addItem(){
	var selectRecord=qpcrjdTaskTempGrid.getSelectionModel();
	var selRecord=qpcrjdTaskItemGrid.store;
	var count=1;
	var max=0;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					return;					
				}
			}
			if(!isRepeat){
			var ob = qpcrjdTaskItemGrid.getStore().recordType;
			qpcrjdTaskItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("tempId",obj.get("id"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("hhzh",obj.get("hhzh"));
			p.set("xylr",obj.get("xylr"));
			p.set("clr",obj.get("clr"));
			p.set("flr",obj.get("flr"));
			p.set("xzlr",obj.get("xzlr"));
			p.set("qtlr",obj.get("qtlr"));
			p.set("wknd",obj.get("wknd"));
			p.set("tl",obj.get("tl"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("name",obj.get("name"));
			p.set("note",obj.get("note"));
			p.set("orderNumber",Number(max)+count);
			p.set("state","1");
			qpcrjdTaskItemGrid.getStore().add(p);
			count++;
			qpcrjdTaskItemGrid.startEditing(0, 0);
			
		}
			
	});
	}else{
		message("请选择样本！");
	}
	
}

//科技服务
function techDNAService(){
	 	var options = {};
	 	options.width = document.body.clientWidth - 470;
	 	options.height = document.body.clientHeight - 80;
		loadDialogPage(null, "选择提取任务单", "/technology/dna/techDnaServiceTask/showTechDnaServiceTaskDialog.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_techDnaServiceTask_div1").data("showTechDnaServiceTaskDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						var id1=obj.get("id");
						
						qpcrjdTaskTempGrid.store.reload();
						var filter1 = function(record, id){
							var flag = true;
							if(id1){
								if (record.get("orderId").indexOf(id1)>=0){
									flag = true;
								}
								else{
									return false;
								}
							 }
							return flag;
						};
						var onStoreLoad1 = function(store, records, options){
						  store.filterBy(filter1);
						};
						qpcrjdTaskTempGrid.store.on("load", onStoreLoad1);
						
					});
					
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}


