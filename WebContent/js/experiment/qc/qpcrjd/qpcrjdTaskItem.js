var qpcrjdTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'labCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'productNum',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'qpcrjdTask-id',
		type:"string"
	});
	    fields.push({
		name:'qpcrjdTask-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'tempId',
			type:"string"
		}); 
	   
	   fields.push({
			name:'yhhzh',
			type:"string"
		});
		   fields.push({
			name:'hhzh',
			type:"string"
		});
		   fields.push({
			name:'xylr',
			type:"string"
		});
		   fields.push({
			name:'clr',
			type:"string"
		});
		   fields.push({
			name:'flr',
			type:"string"
		});
		   fields.push({
			name:'xzlr',
			type:"string"
		});
		   fields.push({
			name:'qtlr',
			type:"string"
		});
		   fields.push({
			name:'tl',
			type:"string"
		});
		   fields.push({
			name:'wknd',
			type:"string"
		});
		   fields.push({
			name:'wkpdcd',
			type:"string"
		});
		   fields.push({
				name:'sample',
				type:"string"
			});
		   fields.push({
				name:'xycd',
				type:"string"
			});
		   fields.push({
				name:'ctcd',
				type:"string"
			});
		   fields.push({
				name:'fcd',
				type:"string"
			});
		   fields.push({
				name:'xzcd',
				type:"string"
			});
		   fields.push({
				name:'qtcd',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'富集文库',
		width:20*6
		
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'上机分组',
		width:20*6
	});
	
	cm.push({
		dataIndex:'yhhzh',
		hidden : true,
		header:'预混合编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'hhzh',
		hidden : false,
		header:'混合组号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:'混后血液文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:'混后ctDNA文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:'混后FFPE文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzlr',
		hidden : true,
		header:'混后新鲜组织文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:'混后其他文库比例%',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xycd',
		hidden : false,
		header:'血液文库长度（bp）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ctcd',
		hidden : false,
		header:'ctDNA文库长度（bp）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'fcd',
		hidden : false,
		header:'FFPE文库长度（bp）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzcd',
		hidden : false,
		header:'新鲜组织文库长度（bp）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qtcd',
		hidden : false,
		header:'其他文库长度（bp）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkpdcd',
		hidden : false,
		header:'混合后文库长度（bp）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wknd',
		hidden : false,
		header:'文库浓度（ng/ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'tl',
		hidden : false,
		header:'文库体积（ul）',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'调整后文库总量',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sample',
		hidden : false,
		header:'取样',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:'测序类型',
		width:20*6
		
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'测序平台',
		width:20*6
		
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'测序读长',
		width:20*6
	
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:'实验室样本号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'步骤编号',
		width:20*6
		 
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:20*6,
		
		renderer: formatDate
		 
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
		
		renderer: formatDate
		
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:20*6,
		
		renderer: formatDate
	
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:'样本用量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'productNum',
		hidden : true,
		header:'中间产物数量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6
		
	
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:'序号',
		width:20*6
		
	
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'qpcrjdTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10
		
	});
	cm.push({
		dataIndex:'qpcrjdTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:20*6
		
		
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="文库定量QPCR明细";
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/qpcrjd/qpcrjdTask/delQpcrjdTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qpcrjdTaskItemGrid.getStore().commitChanges();
				qpcrjdTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	qpcrjdTaskItemGrid=gridEditTable("qpcrjdTaskItemdiv",cols,loadParam,opts);
	$("#qpcrjdTaskItemdiv").data("qpcrjdTaskItemGrid", qpcrjdTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
