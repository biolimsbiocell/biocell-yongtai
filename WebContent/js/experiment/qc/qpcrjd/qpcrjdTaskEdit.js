$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#qpcrjdTask_state").val();
	var stateName = $("#qpcrjdTask_stateName").val();
	if(id =="3"||stateName==biolims.common.toModify){
		load("/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskTempList.action", null, "#qpcrjdTaskTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#qpcrjdTaskTemppage").remove();
	}
});	
function add() {
	window.location = window.ctx + "/experiment/qc/qpcrjd/qpcrjdTask/editQpcrjdTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("QpcrjdTask", {
					userId : userId,
					userName : userName,
					formId : $("#qpcrjdTask_id").val(),
					title : $("#qpcrjdTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});


//审批
$("#toolbarbutton_sp").click(function() {
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#qpcrjdTask_id").val();
	
	var options = {};
	options.width = 929;
	options.height = 534;
	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}

	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null, "审批任务", url, {
		"确定" : function() {
			
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};
				
				
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);
				
			}else if(operVal=="1"){
				if(taskName=="主管修改"){
					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();

					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};
					
					
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				}else{
					var codeList = new Array();
					var codeList1 = new Array();
					var selRecord1 = qpcrjdTaskItemGrid.store;
					var flag1=true;
					if (qpcrjdTaskResultGrid.getAllRecord().length > 0) {
						var selRecord = qpcrjdTaskResultGrid.store;
						for(var j=0;j<selRecord.getCount();j++){
							codeList.push(selRecord.getAt(j).get("tempId"));
							if(selRecord.getAt(j).get("nextFlow")==""){
								message("有下一步未填写！");
								return;
							}
						}
						for(var j=0;j<selRecord1.getCount();j++){
							if(codeList.indexOf(selRecord1.getAt(j).get("sampleCode"))==-1){
								codeList1.push(selRecord1.getAt(j).get("sampleCode"));
								flag1=false;
								message("有样本未完成实验！");
							};
						}
						if(qpcrjdTaskResultGrid.getModifyRecord().length > 0){
							message("请先保存记录！");
							return;
						}
						if(flag1){
								var myMask1 = new Ext.LoadMask(Ext.getBody(), {
									msg : '请等待...'
								});
								myMask1.show();
								Ext.MessageBox.confirm("确认", "请确认保存修改项后进行办理!", function(button, text) {
									if (button == "yes") {
										var paramData =  {};
										paramData.oper = $("#oper").val();
										paramData.info = $("#opinion").val();

										var reqData = {
											data : JSON.stringify(paramData),
											formId : formId,
											taskId : taskId,
											userId : window.userId
										};
										
										
										_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
											location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
										}, dialogWin);
										
										
									}
								});
								myMask1.hide();
						}else{
							message("有样本未完成实验！样本有："+codeList1);
						}
					}else{
						message("请填加任务明细并保存！");
						return;
					}
				}
			}
				
		},
		"查看流程图" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
	
});







function save() {
if(checkSubmit()==true){	Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var qpcrjdTaskItemDivData = $("#qpcrjdTaskItemdiv").data("qpcrjdTaskItemGrid");
		document.getElementById('qpcrjdTaskItemJson').value = commonGetModifyRecords(qpcrjdTaskItemDivData);
	    var qpcrjdTaskTemplateDivData = $("#qpcrjdTaskTemplatediv").data("qpcrjdTaskTemplateGrid");
		document.getElementById('qpcrjdTaskTemplateJson').value = commonGetModifyRecords(qpcrjdTaskTemplateDivData);
	    var qpcrjdTaskReagentDivData = $("#qpcrjdTaskReagentdiv").data("qpcrjdTaskReagentGrid");
		document.getElementById('qpcrjdTaskReagentJson').value = commonGetModifyRecords(qpcrjdTaskReagentDivData);
	    var qpcrjdTaskCosDivData = $("#qpcrjdTaskCosdiv").data("qpcrjdTaskCosGrid");
		document.getElementById('qpcrjdTaskCosJson').value = commonGetModifyRecords(qpcrjdTaskCosDivData);
	    var qpcrjdTaskResultDivData = $("#qpcrjdTaskResultdiv").data("qpcrjdTaskResultGrid");
		document.getElementById('qpcrjdTaskResultJson').value = commonGetModifyRecords(qpcrjdTaskResultDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/qc/qpcrjd/qpcrjdTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/qc/qpcrjd/qpcrjdTask/copyQpcrjdTask.action?id=' + $("#qpcrjdTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#qpcrjdTask_id").val() + "&tableId=qpcrjdTask");
//}
$("#toolbarbutton_status").click(function(){
	commonChangeState("formId=" + $("#qpcrjdTask_id").val() + "&tableId=QpcrjdTask");

});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#qpcrjdTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#qpcrjdTask_template").val());
	nsc.push("模板不能为空！");
	fs.push($("#qpcrjdTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'文库定量QPCR',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskItemList.action", {
				id : $("#qpcrjdTask_id").val()
			}, "#qpcrjdTaskItempage");
load("/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskTemplateList.action", {
				id : $("#qpcrjdTask_id").val()
			}, "#qpcrjdTaskTemplatepage");
load("/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskReagentList.action", {
				id : $("#qpcrjdTask_id").val()
			}, "#qpcrjdTaskReagentpage");
load("/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskCosList.action", {
				id : $("#qpcrjdTask_id").val()
			}, "#qpcrjdTaskCospage");
load("/experiment/qc/qpcrjd/qpcrjdTask/showQpcrjdTaskResultList.action", {
				id : $("#qpcrjdTask_id").val()
			}, "#qpcrjdTaskResultpage");

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	//调用模板
	function TemplateFun(){
		var type="doJddl";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
	
	function setTemplateFun(rec){
		var itemGrid=qpcrjdTaskItemGrid.store;
		if(itemGrid.getCount()>0){
			for(var i=0;i<itemGrid.getCount();i++){
				itemGrid.getAt(i).set("sampleNum",rec.get('sampleNum'));
			}
		}
		if($("#qpcrjdTask_acceptUser_name").val()==""){
			document.getElementById('qpcrjdTask_acceptUser').value=rec.get('acceptUser-id');
			document.getElementById('qpcrjdTask_acceptUser_name').value=rec.get('acceptUser-name');
		}
		var code=$("#qpcrjdTask_template").val();
		if(code==""){
					document.getElementById('qpcrjdTask_template').value=rec.get('id');
					document.getElementById('qpcrjdTask_template_name').value=rec.get('name');
					var win = Ext.getCmp('TemplateFun');
					if(win){win.close();}
					var id=rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {
								var ob = qpcrjdTaskTemplateGrid.getStore().recordType;
								qpcrjdTaskTemplateGrid.stopEditing();
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("code",obj.code);
									p.set("stepName",obj.name);
									
									p.set("note",obj.note);
									qpcrjdTaskTemplateGrid.getStore().add(p);							
								});
								
								qpcrjdTaskTemplateGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = qpcrjdTaskReagentGrid.getStore().recordType;
								qpcrjdTaskReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									qpcrjdTaskReagentGrid.getStore().add(p);							
								});
								
								qpcrjdTaskReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {

								var ob = qpcrjdTaskCosGrid.getStore().recordType;
								qpcrjdTaskCosGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									qpcrjdTaskCosGrid.getStore().add(p);							
								});			
								qpcrjdTaskCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null);

			
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
						var ob1 = qpcrjdTaskTemplateGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id"); 
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/qc/qpcrjd/qpcrjdTask/delQpcrjdTaskTemplateOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null);
								}else{								
									qpcrjdTaskTemplateGrid.store.removeAll();
								}
							}
							qpcrjdTaskTemplateGrid.store.removeAll();
		 				}

						var ob2 = qpcrjdTaskReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");

								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/qc/qpcrjd/qpcrjdTask/delQpcrjdaskReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
								}else{
									qpcrjdTaskReagentGrid.store.removeAll();
								}
							}
							qpcrjdTaskReagentGrid.store.removeAll();
		 				}
						//=========================================
						var ob3 = qpcrjdTaskCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");
								
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/qc/qpcrjd/qpcrjdTask/delQpcrjdTaskCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null); 
								}else{
									qpcrjdTaskCosGrid.store.removeAll();
								}
							}
							qpcrjdTaskCosGrid.store.removeAll();
		 				}
						document.getElementById('qpcrjdTask_template').value=rec.get('id');
						document.getElementById('qpcrjdTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = qpcrjdTaskTemplateGrid.getStore().recordType;
									qpcrjdTaskTemplateGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("stepName",obj.name);
										
										p.set("note",obj.note);
										qpcrjdTaskTemplateGrid.getStore().add(p);							
									});
									
									qpcrjdTaskTemplateGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = qpcrjdTaskReagentGrid.getStore().recordType;
									qpcrjdTaskReagentGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tReagent",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("oneNum",obj.num);
										p.set("note",obj.note);
										qpcrjdTaskReagentGrid.getStore().add(p);							
									});
									
									qpcrjdTaskReagentGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {	

									var ob = qpcrjdTaskCosGrid.getStore().recordType;
									qpcrjdTaskCosGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tCos",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("temperature",obj.temperature);
										p.set("speed",obj.speed);
										p.set("time",obj.time);
										p.set("note",obj.note);
										qpcrjdTaskCosGrid.getStore().add(p);							
									});			
									qpcrjdTaskCosGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						}
					}

}
	
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	function ckcrk(){
		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = qpcrjdTaskResultGrid.store;
				
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");
							} else {
								message("回滚失败！");
							}
						}, null);
					}
					
				}
			}
		});
	}
	
	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '保存'
			});
		item.on('click', ckcrk2);
		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);
		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "QpcrjdTask",id : $("#qpcrjdTask_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
			} else {
				message("办理回滚失败！");
			}
		}, null);
	}
	
	
	
	
	var loadtestUser;
	//选择实验组用户
	function testUser(){
		var gid=$("#qpcrjdTask_acceptUser").val();
		if(gid!=""){
			var options = {};
			options.width = 500;
			options.height = 500;
			var confirm=biolims.common.confirm;
			loadtestUser=loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
				confirm : function() {
					var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						$("#qpcrjdTask_testUser").val(selectRecord[0].get("user-id"));
						$("#qpcrjdTask_testUser_name").val(selectRecord[0].get("user-name"));
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
		}else{
			message("请选择实验组");
		}
		
	}
	function setUserGroupUser(){
		var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
				$("#qpcrjdTask_testUser").val(selectRecord[0].get("user-id"));
				$("#qpcrjdTask_testUser_name").val(selectRecord[0].get("user-name"));
		}else{
			message(biolims.common.selectYouWant);
			return;
		}
		loadtestUser.dialog("close");
	}



