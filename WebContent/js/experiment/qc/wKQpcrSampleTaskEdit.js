
$(function() {
	$("#tabs_1").tabs({
		select : function(event, ui) {
		}
	});
	$("#tabs_2").tabs({
		select : function(event, ui) {
		}
	});
	$("#tabs_3").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#wKQpcrSampleTask_state").val();
	var stateName = $("#wKQpcrSampleTask_stateName").val();
	if(id =="3"||stateName==biolims.common.toModify){
		load("/experiment/qc/wKAccept/showWKQpcrTempList.action", null, "#wKQpcrTempPage");
		$("#markup").css("width","75%");
//		$("#tabs1").tabs({
//			select : function(event, ui) {
//				if(ui.index==1){
//					load("/experiment/wk/showTechJkTaskList.action", { }, "#keji");
//				}
//			}
//		});
		
//		load("/experiment/qc/wKAccept/showWKQpcrTempList.action", { }, "#wKQpcrTempPage");
//		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showAcceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#wKQpcrTempPage").remove();
	}
	
});
function add() {
	window.location = window.ctx + "/experiment/qc/wKQpcrSampleTask/editWKQpcrSampleTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/qc/wKQpcrSampleTask/showWKQpcrSampleTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
setTimeout(function() {

	if($("#wKQpcrSampleTask_template").val()){
		var maxNum = $("#wKQpcrSampleTask_maxNum").val();
		if(maxNum>0){
				load("/storage/container/sampleContainerTest.action", {
					id : $("#wKQpcrSampleTask_template").val(),
					type :$("#type").val(),
					maxNum : 0
				}, "#3d_image0", function(){
					if(maxNum>1){
					load("/storage/container/sampleContainerTest.action", {
						id : $("#wKQpcrSampleTask_template").val(),
						type :$("#type").val(),
						maxNum : 1
					}, "#3d_image1", null)
					}
				});
			
		}
	}
}, 100);
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("WKQpcr", {
					userId : userId,
					userName : userName,
					formId : $("#wKQpcrSampleTask_id").val(),
					title : $("#wKQpcrSampleTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#wKQpcrSampleTask_id").val();
	
	var options = {};
	options.width = 929;
	options.height = 534;
	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}

	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null, "审批任务", url, {
		"确定" : function() {
			
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};
				
				
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);
				
			}else if(operVal=="1"){
				if(taskName=="主管修改"){
					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();

					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};
					
					
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				}else{
					var codeList = new Array();
					var codeList1 = new Array();
					var selRecord1 = wKQpcrItemGrid.store;
					var flag=true;
					var flag1=true;
					if (wKQpcrSampleInfoGrid.getAllRecord().length > 0) {
						var selRecord = wKQpcrSampleInfoGrid.store;
						for(var j=0;j<selRecord.getCount();j++){
							var oldv = selRecord.getAt(j).get("submit");
							codeList.push(selRecord.getAt(j).get("code"));
							if(oldv==""){
								flag=false;
								message("有样本未提交！");
								return;
							}
							if(selRecord.getAt(j).get("nextFlowId")==""){
								message("有下一步未填写！");
								return;
							}
						}
						for(var j=0;j<selRecord1.getCount();j++){
							if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
								codeList1.push(selRecord1.getAt(j).get("code"));
								flag1=false;
								message("有样本未完成实验！");
							};
						}
						if(wKQpcrSampleInfoGrid.getModifyRecord().length > 0){
							message("请先保存记录！");
							return;
						}
						if(flag1){
							if(flag){
								var myMask1 = new Ext.LoadMask(Ext.getBody(), {
									msg : '请等待...'
								});
								myMask1.show();
								Ext.MessageBox.confirm("确认", "请确认保存修改项后进行办理!", function(button, text) {
									if (button == "yes") {
										var paramData =  {};
										paramData.oper = $("#oper").val();
										paramData.info = $("#opinion").val();

										var reqData = {
											data : JSON.stringify(paramData),
											formId : formId,
											taskId : taskId,
											userId : window.userId
										};
										
										
										_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
											location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
										}, dialogWin);
										
										
									}
								});
								myMask1.hide();
							}else{
								message("有样本未提交！");
							}
						}else{
							message("有样本未完成实验！样本有："+codeList1);
						}
					}else{
						message("请填加任务明细并保存！");
						return;
					}
				}
			}
		},
		"查看流程图" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
	
	
//	var codeList = new Array();
//	var codeList1 = new Array();
//	var selRecord1 = wKQpcrItemGrid.store;
//	var flag=true;
//	var flag1=true;
//	if (wKQpcrSampleInfoGrid.getAllRecord().length > 0) {
//		var selRecord = wKQpcrSampleInfoGrid.store;
//		for(var j=0;j<selRecord.getCount();j++){
//			var oldv = selRecord.getAt(j).get("submit");
//			codeList.push(selRecord.getAt(j).get("code"));
//			if(oldv!=1){
//				flag=false;
//				message("有样本未提交！");
//				return;
//			}
//		}
//		for(var j=0;j<selRecord1.getCount();j++){
//			if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
//				codeList1.push(selRecord1.getAt(j).get("code"));
//				flag1=false;
//				message("有样本未完成实验！");
//			};
//		}
//		if(wKQpcrSampleInfoGrid.getModifyRecord().length > 0){
//			message("请先保存记录！");
//			return;
//		}
//		if(flag1){
//			if(flag){
//				var taskId = $(this).attr("taskId");
//				Ext.MessageBox.confirm("确认", "请确认保存修改项后进行办理!", function(button, text) {
//					if (button == "yes") {
//						completeTask($("#wKQpcrSampleTask_id").val(), taskId, function() {
//							document.getElementById('toolbarSaveButtonFlag').value = 'save';
//							location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
//						});
//					}
//				});
//			}else{
//				message("有样本未提交！");
//			}
//		}else{
//			message("有样本未完成实验！样本有："+codeList1);
//		}
//	}else{
//		message("请填加任务明细并保存！");
//		return;
//	}

	
	
	
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var wKQpcrItemDivData = $("#wKQpcrItemdiv").data("wKQpcrItemGrid");
		document.getElementById('wKQpcrItemJson').value = commonGetModifyRecords(wKQpcrItemDivData);
//		var qcQpcrTaskItemmDivData = $("#qcQpcrTaskItemmdiv").data("qcQpcrTaskItemmGrid");
//		document.getElementById('qcQpcrTaskItemmJson').value = commonGetModifyRecords(qcQpcrTaskItemmDivData);
	    var wKQpcrTemplateDivData = $("#wKQpcrTemplatediv").data("wKQpcrTemplateGrid");
		document.getElementById('wKQpcrTemplateJson').value = commonGetModifyRecords(wKQpcrTemplateDivData);
	    var wKQpcrDeviceDivData = $("#wKQpcrDevicediv").data("wKQpcrDeviceGrid");
		document.getElementById('wKQpcrDeviceJson').value = commonGetModifyRecords(wKQpcrDeviceDivData);
	    var wKQpcrSampleInfoDivData = $("#wKQpcrSampleInfodiv").data("wKQpcrSampleInfoGrid");
		document.getElementById('wKQpcrSampleInfoJson').value = commonGetModifyRecords(wKQpcrSampleInfoDivData);
//		var sampleQcPoolingInfoDivData = $("#sampleQcPoolingInfodiv").data("sampleQcPoolingInfoGrid");
//		document.getElementById('sampleQcPoolingInfoJson').value = commonGetModifyRecords(sampleQcPoolingInfoDivData);
	    var wKQpcrReagentDivData = $("#wKQpcrReagentdiv").data("wKQpcrReagentGrid");
		document.getElementById('wKQpcrReagentJson').value = commonGetModifyRecords(wKQpcrReagentDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/qc/wKQpcrSampleTask/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/qc/wKQpcrSampleTask/copyWKQpcrSampleTask.action?id=' + $("#wKQpcrSampleTask_id").val();
}
/*function changeState() {
	commonChangeState("formId=" + $("#wKQpcrSampleTask_id").val() + "&tableId=wKQpcr");
}*/
$("#toolbarbutton_status").click(function(){
	var grid1 = wKQpcrSampleInfoGrid.store;
	var grid2 = wKQpcrItemGrid.store;
//	var grid3 = qcQpcrTaskItemmGrid.store;
//	var grid4 = sampleQcPoolingInfoGrid.store;
	if(grid1.getCount()!=grid2.getCount()){
		message("有未使用的样本！");
	}else if ($("#wKQpcrSampleTask_id").val()){
		commonChangeState("formId=" + $("#wKQpcrSampleTask_id").val() + "&tableId=WKQpcr");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#wKQpcrSampleTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#wKQpcrSampleTask_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#wKQpcrSampleTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'QPCR质控',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/qc/wKQpcrSampleTask/showWKQpcrItemList.action", {
				id : $("#wKQpcrSampleTask_id").val()
			}, "#wKQpcrItempage");
load("/experiment/qc/wKQpcrSampleTask/showQcQpcrTaskItemmList.action", {
				id : $("#wKQpcrSampleTask_id").val()
			}, "#qcQpcrTaskItemmpage");
load("/experiment/qc/wKQpcrSampleTask/showWKQpcrTemplateList.action", {
				id : $("#wKQpcrSampleTask_id").val()
			}, "#wKQpcrTemplatepage");
load("/experiment/qc/wKQpcrSampleTask/showWKQpcrDeviceList.action", {
				id : $("#wKQpcrSampleTask_id").val()
			}, "#wKQpcrDevicepage");
load("/experiment/qc/wKQpcrSampleTask/showWKQpcrSampleInfoList.action", {
				id : $("#wKQpcrSampleTask_id").val()
			}, "#wKQpcrSampleInfopage");
load("/experiment/qc/wKQpcrSampleTask/showSampleQcPoolingInfoList.action", {
				id : $("#wKQpcrSampleTask_id").val()
			}, "#sampleQcPoolingInfopage");
load("/experiment/qc/wKQpcrSampleTask/showWKQpcrReagentList.action", {
				id : $("#wKQpcrSampleTask_id").val()
			}, "#wKQpcrReagentpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
//调用模板
function TemplateFun(){
	var type="doQua";
	var win = Ext.getCmp('TemplateFun');
	if (win) {win.close();}
	var TemplateFun= new Ext.Window({
	id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 TemplateFun.close(); }  }]  }); 
	 TemplateFun.show(); 
}
function setTemplateFun(rec){
	var itemGrid=wKQpcrItemGrid.store;
	if(itemGrid.getCount()>0){
		for(var i=0;i<itemGrid.getCount();i++){
			itemGrid.getAt(i).set("sampleConsume",rec.get('sampleNum'));
		}
	}
	if($("#wKQpcrSampleTask_acceptUser_name").val()==""){
		document.getElementById('wKQpcrSampleTask_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('wKQpcrSampleTask_acceptUser_name').value=rec.get('acceptUser-name');
	}
	
	
	var code=$("#wKQpcrSampleTask_template").val();
	if(code==""){
//		var cid=rec.get('id');
//		ajax("post", "/system/template/template/findInstrument.action", {
//			code : cid
//		}, function(data) {
//			if (data.success) {
//				if (data.data) {
//					message("设备被占用！");
//					var win = Ext.getCmp('TemplateFun');
//	 				if(win){win.close();}
//					return;
//				}else{
					document.getElementById('wKQpcrSampleTask_template').value=rec.get('id');
					document.getElementById('wKQpcrSampleTask_template_name').value=rec.get('name');
					var win = Ext.getCmp('TemplateFun');
					if(win){win.close();}
					var id=rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id,
					}, function(data) {
						if (data.success) {
							var ob = wKQpcrTemplateGrid.getStore().recordType;
							wKQpcrTemplateGrid.stopEditing();
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tItem",obj.id);
								p.set("code",obj.code);
								p.set("stepName",obj.name);
								wKQpcrTemplateGrid.getStore().add(p);							
							});
							wKQpcrTemplateGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = wKQpcrReagentGrid.getStore().recordType;
							wKQpcrReagentGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tReagent",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("batch",obj.batch);
								p.set("isTestSuccess",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("oneNum",obj.num);
								p.set("sn",obj.sn);
								wKQpcrReagentGrid.getStore().add(p);							
							});
							wKQpcrReagentGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = wKQpcrDeviceGrid.getStore().recordType;
							wKQpcrDeviceGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tCos",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("isTestSuccess",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("temperature",obj.temperature);
								p.set("speed",obj.speed);
								p.set("time",obj.time);
								p.set("note",obj.note);
								wKQpcrDeviceGrid.getStore().add(p);							
							});			
							wKQpcrDeviceGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null);
//				}
//			}
//		}, null);
		
	}else{
		var flag = true;
		if(rec.get('id')==code){
			flag = confirm("是否重新加载实验步骤，应有实验步骤将被删除？");
			 }
		if(flag==true){
//			var cid=rec.get('id');
//			ajax("post", "/system/template/template/findInstrument.action", {
//				code : cid
//			}, function(data) {
//				if (data.success) {
//					if (data.data) {
//						message("设备被占用！");
//						var win = Ext.getCmp('TemplateFun');
//		 				if(win){win.close();}
//						return;
//					}else{
						var ob1 = wKQpcrTemplateGrid.store;
						if (ob1.getCount() > 0) {
						for(var j=0;j<ob1.getCount();j++){
							var oldv = ob1.getAt(j).get("id");
							//=====================2015-11-30 ly======================//
							//根据ID删除
							if(oldv!=null){
								ajax("post", "/experiment/qc/wKQpcrSampleTask/delWKQpcrTemplateOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null);
							}else{								
								wKQpcrTemplateGrid.store.removeAll();
							}
						}
						wKQpcrTemplateGrid.store.removeAll();
	 				}
	 				//===========================================//
					var ob2 = wKQpcrReagentGrid.store;
					if (ob2.getCount() > 0) {
						for(var j=0;j<ob2.getCount();j++){
							var oldv = ob2.getAt(j).get("id");
							//=====================2015-11-30 ly======================//
							//根据ID删除
							if(oldv!=null){
							ajax("post", "/experiment/qc/wKQpcrSampleTask/delReagentListOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message("删除成功！");
								} else {
									message("删除失败！");
								}
							}, null); 
							}else{
								wKQpcrReagentGrid.store.removeAll();
							}
						}
						wKQpcrReagentGrid.store.removeAll();
	 				}
					//=========================================
					var ob3 = wKQpcrDeviceGrid.store;
					if (ob3.getCount() > 0) {
						for(var j=0;j<ob3.getCount();j++){
							var oldv = ob3.getAt(j).get("id");
							//=====================2015-11-30 ly======================//
							//根据ID删除
							if(oldv!=null){
								ajax("post", "/experiment/qc/wKQpcrSampleTask/delWKQpcrDeviceOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
							}else{
								wKQpcrDeviceGrid.store.removeAll();
							}
						}
						wKQpcrDeviceGrid.store.removeAll();
	 				}
					document.getElementById('wKQpcrSampleTask_template').value=rec.get('id');
	 				document.getElementById('wKQpcrSampleTask_template_name').value=rec.get('name');
	 				var win = Ext.getCmp('TemplateFun');
	 				if(win){win.close();}
					var id = rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {	

								var ob = wKQpcrTemplateGrid.getStore().recordType;
								wKQpcrTemplateGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("code",obj.code);
									p.set("stepName",obj.name);
									wKQpcrTemplateGrid.getStore().add(p);							
								});
								
								wKQpcrTemplateGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	

								var ob = wKQpcrReagentGrid.getStore().recordType;
								wKQpcrReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isTestSuccess",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									wKQpcrReagentGrid.getStore().add(p);							
								});
								wKQpcrReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	

								var ob = wKQpcrDeviceGrid.getStore().recordType;
								wKQpcrDeviceGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("isTestSuccess",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									wKQpcrDeviceGrid.getStore().add(p);							
								});			
								wKQpcrDeviceGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null);
					}
				}
//			}, null);
//		}	 				
//	}
}
//按条件加载原辅料
function showReagent(){
	//获取选择的数据
	var selectRcords=wKQpcrTemplateGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=wKQpcrTemplateGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid=$("#wKQpcrSampleTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/qc/wKQpcrSampleTask/showWKQpcrReagentList.action", {
			id : $("#wKQpcrSampleTask_id").val()
		}, "#wKQpcrReagentpage");
	}else if(length1==1){
//		$.each(selectRcords, function(i, obj) {
//			var code=obj.get("code");
//			load("/experiment/qc/wKQpcrSampleTask/showWKQpcrReagentList.action", {
//				id : $("#wKQpcrSampleTask_id").val(),
//				itemId : code
//			}, "#wKQpcrReagentpage");
//		});	
		wKQpcrReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/qc/wKQpcrSampleTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = wKQpcrReagentGrid.getStore().recordType;
				wKQpcrReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("isTestSuccess",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("wKQpcrSampleTask-id",obj.tId);
					p.set("wKQpcrSampleTask-name",obj.tName);
					
					wKQpcrReagentGrid.getStore().add(p);							
				});
				wKQpcrReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});	
	}else{
		message("请选择一条数据!");
		return;
	}
	
}

//按条件加载设备
function showCos(){
	//获取选择的数据
	var selectRcords=wKQpcrTemplateGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=wKQpcrTemplateGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid=$("#wKQpcrSampleTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/qc/wKQpcrSampleTask/showWKQpcrDeviceList.action", {
			id : $("#wKQpcrSampleTask_id").val()
		}, "#wKQpcrDevicepage");
	}else if(length1==1){
//		var code="";
//		$.each(selectRcords, function(i, obj) {
//			code=obj.get("code");
//			load("/experiment/qc/wKQpcrSampleTask/showWKQpcrDeviceList.action", {
//				id : $("#wKQpcrSampleTask_id").val(),
//				itemId : code
//			}, "#wKQpcrDevicepage");
//		});	
		wKQpcrDeviceGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");

		ajax("post", "/experiment/qc/wKQpcrSampleTask/setCos.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = wKQpcrDeviceGrid.getStore().recordType;
				wKQpcrDeviceGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("note",obj.note);
					p.set("time",obj.time);
					p.set("isTestSuccess",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tCos",obj.tCos);
					p.set("wKQpcrSampleTask-id",obj.tId);
					p.set("wKQpcrSampleTask-name",obj.tName);
					
					wKQpcrDeviceGrid.getStore().add(p);							
				});
				wKQpcrDeviceGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}
	
}


Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = wKQpcrSampleInfoGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}
			
			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}


Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "WKQpcr",id : $("#wKQpcrSampleTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}



var loadtestUser;
//选择实验组用户
function testUser(){
	var gid=$("#wKQpcrSampleTask_acceptUser").val();
	if(gid!=""){
		var options = {};
		options.width = 500;
		options.height = 500;
		var confirm=biolims.common.confirm;
		loadtestUser=loadDialogPage(null, biolims.common.chooseTester, "/system/user/userGroupUser/userGroupUserSelect.action?gid="+gid, {
			confirm : function() {
				var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$("#wKQpcrSampleTask_testUser").val(selectRecord[0].get("user-id"));
					$("#wKQpcrSampleTask_testUser_name").val(selectRecord[0].get("user-name"));
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}else{
		message("请选择实验组");
	}
	
}
function setUserGroupUser(){
	var operGrid = $("#showUserGroupUserDiv").data("showUserGroupUserGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
			$("#wKQpcrSampleTask_testUser").val(selectRecord[0].get("user-id"));
			$("#wKQpcrSampleTask_testUser_name").val(selectRecord[0].get("user-name"));
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadtestUser.dialog("close");
}

