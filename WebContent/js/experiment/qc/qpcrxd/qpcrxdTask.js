var qpcrxdTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'reciveDate',
		header:'实验时间',
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:'模板ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:'模板',
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-id',
		hidden:true,
		header:'实验组ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:'实验组',
		
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'下达人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'下达人',
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'下达时间',
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		hidden:true,
		header:'审核人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:'审核人',
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:'完成时间',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态名称',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qpcrxd/qpcrxdTask/showQpcrxdTaskListJson.action";
	var opts={};
	opts.title="文库混合QPCR";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	qpcrxdTaskGrid=gridTable("show_qpcrxdTask_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/experiment/qc/qpcrxd/qpcrxdTask/editQpcrxdTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/qc/qpcrxd/qpcrxdTask/editQpcrxdTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/qc/qpcrxd/qpcrxdTask/viewQpcrxdTask.action?id=' + id;
}
function exportexcel() {
	qpcrxdTaskGrid.title = '导出列表';
	var vExportContent = qpcrxdTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startreciveDate").val() != undefined) && ($("#startreciveDate").val() != '')) {
					var startreciveDatestr = ">=##@@##" + $("#startreciveDate").val();
					$("#reciveDate1").val(startreciveDatestr);
				}
				if (($("#endreciveDate").val() != undefined) && ($("#endreciveDate").val() != '')) {
					var endreciveDatestr = "<=##@@##" + $("#endreciveDate").val();

					$("#reciveDate2").val(endreciveDatestr);

				}
				
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
					var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
					$("#confirmDate1").val(startconfirmDatestr);
				}
				if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
					var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();

					$("#confirmDate2").val(endconfirmDatestr);

				}
				
				
				commonSearchAction(qpcrxdTaskGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
