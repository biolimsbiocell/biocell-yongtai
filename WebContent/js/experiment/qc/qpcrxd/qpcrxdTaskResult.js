var qpcrxdTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'labCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'sumTotal',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'qpcrxdTask-id',
		type:"string"
	});
	    fields.push({
		name:'qpcrxdTask-name',
		type:"string"
	});
	    fields.push({
			name:'tempId',
			type:"string"
		});
	    fields.push({
			name:'xylr',
			type:"string"
		});
	   fields.push({
			name:'clr',
			type:"string"
		});
	   fields.push({
			name:'flr',
			type:"string"
		});
	   fields.push({
			name:'xzLr',
			type:"string"
		});
	   fields.push({
			name:'qtlr',
			type:"string"
		});
	   fields.push({
			name:'tllr',
			type:"string"
		});
	   fields.push({
			name:'kzxl',
			type:"string"
		});
	   fields.push({
			name:'ect',
			type:"string"
		});
	   fields.push({
			name:'qpcrlr',
			type:"string"
		});
	   fields.push({
			name:'cdlr',
			type:"string"
		});
	
	   fields.push({
			name:'flux',
			type:"string"
		});
	   fields.push({
			name:'yhhNum',
			type:"string"
		});
	   fields.push({
			name:'probeCode-id',
			type:"string"
		});
	   fields.push({
			name:'probeCode-name',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
		 
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'富集文库',
		width:20*7
		
		 
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'上机分组',
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:'文库类型',
		width:20*6
	});
	cm.push({
		dataIndex:'yhhNum',
		hidden : true,
		header:'预混合组号',
		width:20*6
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:'血液文库比例%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:'ctDNA文库比例%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:'FFPE文库比例%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzLr',
		hidden : true,
		header:'新鲜组织文库比例%',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
		 
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:'其他文库比例%',
		width:20*8,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
		 
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:'科研编号',
		width:20*6
	});
	cm.push({
		dataIndex:'probeCode-id',
		hidden : true,
		header:'探针id',
		width:20*6
	});
	cm.push({
		dataIndex:'probeCode-name',
		hidden : true,
		header:'探针名称',
		width:20*6
	});
	cm.push({
		dataIndex:'flux',
		hidden : false,
		header:'通量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'tllr',
		hidden : false,
		header:'Cq mean',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'cdlr',
		hidden : false,
		header:'ΔCT',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'kzxl',
		hidden : false,
		header:'扩增效率',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ect',
		hidden : false,
		header:'E^-ΔCT',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度ng/ul',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:'总量ng',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'qpcrlr',
		hidden : true,
		header:'QCPR比例',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:'测序类型',
		width:20*6
		
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'测序平台',
		width:20*6
		
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'测序读长',
		width:20*6
		 
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:'实验室样本号',
		width:20*6
	});
	
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单',
		width:20*6
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'index',
		width:20*6
		
		 
	});
	
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'处理结果',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	

	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : true,
		header:'是否提交',
		width:20*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样时间',
		width:20*6,
		
		renderer: formatDate
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
		
		renderer: formatDate 
	});

	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告时间',
		width:20*6,
		
		renderer: formatDate
		 
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'method',
		hidden : true,
		header:'处理意见',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'qpcrxdTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qpcrxdTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qpcrxd/qpcrxdTask/showQpcrxdTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="文库混合QPCR结果";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state = $("#qpcrxdTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/qpcrxd/qpcrxdTask/delQpcrxdTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};	
	opts.tbar.push({
		text : "批量下一步",
		handler : function() {
			var records = qpcrxdTaskResultGrid.getSelectRecord();
			if (records && records.length > 0) {
//				var options = {};
//				options.width = 400;
//				options.height = 300;
//				loadDialogPage($("#bat_result_div"), "批量下一步", null, {
//					"确定" : function() {
//							var nextFlow = $("#nextFlow").val();
//							var nextFlowId = $("#nextFlowId").val();
//							qpcrxdTaskResultGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								obj.set("nextFlow", nextFlow);
//								obj.set("nextFlowId",nextFlowId);
//							});
//							qpcrxdTaskResultGrid.startEditing(0, 0);
//						
//						$(this).dialog("close");
//					}
//				}, true, options);
				loadTestNextFlowCob();
			}else{
				message("请选择数据！");
			}
		}
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});	
	}
	qpcrxdTaskResultGrid=gridEditTable("qpcrxdTaskResultdiv",cols,loadParam,opts);
	$("#qpcrxdTaskResultdiv").data("qpcrxdTaskResultGrid", qpcrxdTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = qpcrxdTaskResultGrid.getSelectRecord();
	var sequencingPlatform="";
	$.each(records1, function(j, k) {
		sequencingPlatform=k.get("productName");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/showPoolingTaskNextFlow.action?model=QpcrxdTask&sequencingPlatform="+sequencingPlatform, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qpcrxdTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = qpcrxdTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}	

