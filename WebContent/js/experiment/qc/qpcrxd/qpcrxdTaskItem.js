var qpcrxdTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'labCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'productNum',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'qpcrxdTask-id',
		type:"string"
	});
	    fields.push({
		name:'qpcrxdTask-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'tempId',
			type:"string"
		}); 
	   fields.push({
			name:'probeCode-id',
			type:"string"
		});
	   fields.push({
			name:'probeCode-name',
			type:"string"
		});
	   
	   fields.push({
			name:'xylr',
			type:"string"
		});
	   fields.push({
			name:'clr',
			type:"string"
		});
	   fields.push({
			name:'flr',
			type:"string"
		});
	   fields.push({
			name:'xzLr',
			type:"string"
		});
	   fields.push({
			name:'qtlr',
			type:"string"
		});
	   fields.push({
			name:'loopNum',
			type:"string"
		});
	   fields.push({
			name:'flux',
			type:"string"
		});
	   fields.push({
			name:'yhhNum',
			type:"string"
		});
	   fields.push({
			name:'qy',
			type:"string"
		});
	   fields.push({
			name:'wkType',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'富集文库',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'上机分组',
		width:20*6
	});
	cm.push({
		dataIndex:'wkType',
		hidden : true,
		header:'文库类型',
		width:20*6
	});
	cm.push({
		dataIndex:'yhhNum',
		hidden : true,
		header:'预混合组号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xylr',
		hidden : true,
		header:'血液文库比例%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'clr',
		hidden : true,
		header:'ctDNA文库比例%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flr',
		hidden : true,
		header:'FFPE文库比例%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'xzLr',
		hidden : true,
		header:'新鲜组织文库比例%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
		 
	});
	cm.push({
		dataIndex:'qtlr',
		hidden : true,
		header:'其他文库比例%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
		 
	});
	cm.push({
		dataIndex:'probeCode-id',
		hidden : true,
		header:'探针id',
		width:20*6
		
	});
	cm.push({
		dataIndex:'probeCode-name',
		hidden : false,
		header:'探针名称',
		width:20*6
		
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'科研编号',
		width:20*6
	});
	cm.push({
		dataIndex:'loopNum',
		hidden : false,
		header:'PCR循环数',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productNum',
		hidden : false,
		header:'浓度ng/ul',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积ul',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'总量ng',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'得率%',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'flux',
		hidden : false,
		header:'通量(%)',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qy',
		hidden : false,
		header:'取样（ul）',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:'测序类型',
		width:20*6
		
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'测序平台',
		width:20*6
		
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'测序读长',
		width:20*6
	});
	
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单',
		width:20*6
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:'序号',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'qpcrxdTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qpcrxdTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:20*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:20*6,
		renderer: formatDate
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/qpcrxd/qpcrxdTask/showQpcrxdTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="文库混合QPCR明细";
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/qpcrxd/qpcrxdTask/delQpcrxdTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qpcrxdTaskItemGrid.getStore().commitChanges();
				qpcrxdTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	qpcrxdTaskItemGrid=gridEditTable("qpcrxdTaskItemdiv",cols,loadParam,opts);
	$("#qpcrxdTaskItemdiv").data("qpcrxdTaskItemGrid", qpcrxdTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
