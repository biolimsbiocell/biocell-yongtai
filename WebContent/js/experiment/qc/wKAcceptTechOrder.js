﻿var techQcOrderGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'projectName',
		type:"string"
	});
	    fields.push({
		name:'projectId',
		type:"string"
	});
	    fields.push({
		name:'projectName',
		type:"string"
	});
	    fields.push({
		name:'contractId',
		type:"string"
	});
	    fields.push({
		name:'orderType',
		type:"string"
	});
	    fields.push({
		name:'cooperation',
		type:"string"
	});
	    fields.push({
		name:'projectLeader',
		type:"string"
	});
	    fields.push({
		name:'testLeader',
		type:"string"
	});
	    fields.push({
		name:'analysisLeader',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'projectId',
		header:'项目编号',
		width:30*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'projectName',
		header:'项目名称',
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'projectName',
		header:'项目名称',
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'contractId',
		header:'合同编号',
		width:20*6,
		
		sortable:true
	});
	
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', 'DNA' ], [ '1', '文库' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'orderType',
		header:'任务单类型',
		width:20*6,
		sortable:true,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});

	cm.push({
		dataIndex:'cooperation',
		header:'合作单位',
		width:30*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'projectLeader',
		header:'项目负责人',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'testLeader',
		header:'实验负责人',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'analysisLeader',
		header:'信息负责人',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态',
		width:20*6,
		
		sortable:true
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKAccept/showWKAcceptTechOrderListJson.action";
	var opts={};
	opts.title="科技服务任务单";
	opts.height=document.body.clientHeight-367;
	opts.rowselect=function(id){
		$("#selectId").val(id);
		//获取选择的数据
		var selectRcords=techQcOrderGrid.getSelectionModel().getSelections();
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		if(length1==1){
			wKAcceptTechTempGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("id");
			var type=obj.get("orderType");
			ajax("post", "/experiment/qc/wKAccept/setTempByOrder.action", {
				code : code
			}, function(data) {
				
				if (data.success) {	
					var ob = wKAcceptTechTempGrid.getStore().recordType;
					wKAcceptTechTempGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;

						p.set("id",obj.id);
						p.set("wkCode",obj.wkCode);
						p.set("sampleCode",obj.sampleCode);
						p.set("pdSzie",obj.pdSzie);
						p.set("massCon",obj.massCon);
						p.set("molarCon",obj.molarCon);
						p.set("volume",obj.volume);
						p.set("pdResult",obj.pdResult);
						p.set("molarResult",obj.molarResult);
						p.set("orderId",obj.orderId);
						p.set("orderType",type);
						
						wKAcceptTechTempGrid.getStore().add(p);							
					});
					wKAcceptTechTempGrid.startEditing(0, 0);
				}else{
					message("获取明细数据时发生错误！");
				}
			}, null);
			});		
		}else{
			message("请选择一条数据!");
			return;
		}
	};
	techQcOrderGrid=gridTable("awKAcceptTechOrderdiv",cols,loadParam,opts);
});



var wKAcceptTechTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'pdSzie',
		type:"string"
	});
	   fields.push({
		name:'massCon',
		type:"string"
	});
	   fields.push({
		name:'molarCon',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	    fields.push({
		name:'pdResult',
		type:"string"
	});
	   fields.push({
		name:'molarResult',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});

	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});

	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'wkCode',
		hidden : false,
		header:'文库编号',
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		width:30*6,
		header:'任务单号'
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', 'DNA' ], [ '1', '文库' ] ]
	});
	var orderType = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'orderType',
		header:'任务单类型',
		width:20*6,
		sortable:true,
		renderer : Ext.util.Format.comboRenderer(orderType)
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'左侧表ID'
	});
	cm.push({
		dataIndex:'pdSzie',
		hidden : false,
		header:'片段大小',
		width:20*6
	});
	cm.push({
		dataIndex:'massCon',
		hidden : false,
		header:'质量浓度',
		width:20*6
	});
	cm.push({
		dataIndex:'molarCon',
		hidden : false,
		header:'摩尔浓度',
		width:20*6
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'检测后体积',
		width:20*6
	});
	cm.push({
		dataIndex:'pdResult',
		hidden : false,
		header:'片段大小判定',
		width:20*6
	});
	cm.push({
		dataIndex:'molarResult',
		hidden : false,
		header:'摩尔浓度判定',
		width:20*6
	});
//	cm.push({
//		dataIndex:'note',
//		hidden : false,
//		header:'备注',
//		width:40*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKAccept/showWKAcceptTechTempListJson.action";
	var opts={};
	opts.title="待检测样本";
	opts.height =  document.body.clientHeight-345;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = wKAcceptTechTempGrid.getAllRecord();
							var store = wKAcceptTechTempGrid.store;

							var isOper = true;
							var buf = [];
							wKAcceptTechTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								})
							});
							wKAcceptTechTempGrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message("样本号核对不符，请检查！");
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							wKAcceptTechTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	wKAcceptTechTempGrid=gridEditTable("wKAcceptTechTempdiv",cols,loadParam,opts);
});

function addItem(){
	var selectRecord=wKAcceptTechTempGrid.getSelectionModel();
	var selRecord=wKAcceptItemGrid.store;
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("wkCode");
				if(oldv == obj.get("wkCode")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					break;					
				}
			}
			if(!isRepeat){
			var ob = wKAcceptItemGrid.getStore().recordType;
			wKAcceptItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
			p.set("wkCode",obj.get("wkCode"));
			p.set("itemId",obj.get("id"));
			p.set("techTaskId",obj.get("orderId"));
			p.set("wkType","0");
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("state",'1');
			wKAcceptItemGrid.getStore().add(p);
		}
			
		});
		wKAcceptItemGrid.startEditing(0, 0);
		}
	
}
