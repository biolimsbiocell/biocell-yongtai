var agrosTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'labCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'sampleInfo-note',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-note',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-receiveDate',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-reportDate',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6
		

	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*7
	
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
		
	});
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:'检测项目编号',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目名称',
		width:20*8
		
		
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : false,
		header:'取样日期',
		width:20*6,
		
		renderer: formatDate
		
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:'接收日期',
		width:20*6,
		
		renderer: formatDate
	
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6,
		
		renderer: formatDate

	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:'任务单',
		width:20*6
		

	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'状态',
		width:20*6
		
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6
		
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:'开箱指令id',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : false,
		header:'接收日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:'开箱指令',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/agros/agrosTask/showAgrosTaskTempListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="琼脂糖电泳AGROS临时表";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = agrosTaskTempGrid.getAllRecord();
							var store = agrosTaskTempGrid.store;

							var isOper = true;
							var buf = [];
							agrosTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							agrosTaskTempGrid.getSelectionModel().selectRows(buf);
							if(isOper==false){
								message("样本号核对不符，请检查！");
								
							}else{
								addItem();
							}
							agrosTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	opts.tbar.push({
		text : '科技服务',
		handler : techDNAService
	});
	agrosTaskTempGrid=gridEditTable("agrosTaskTempdiv",cols,loadParam,opts);
	$("#agrosTaskTempdiv").data("agrosTaskTempGrid", agrosTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//添加任务到子表
function addItem(){
	var selectRecord=agrosTaskTempGrid.getSelectionModel();
	var selRecord=agrosTaskItemGrid.store;
	var count=1;
	var max=0;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					return;					
				}
			}
			if(!isRepeat){
			var ob = agrosTaskItemGrid.getStore().recordType;
			agrosTaskItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("tempId",obj.get("id"));
			p.set("name",obj.get("name"));
			p.set("code",obj.get("code"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("orderNumber",Number(max)+count);
			p.set("state","1");
			p.set("labCode",obj.get("labCode"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("volume",obj.get("volume"));
			p.set("inspectDate",obj.get("inspectDate"));
			p.set("acceptDate",obj.get("acceptDate"));
			p.set("reportDate",obj.get("reportDate"));
			p.set("orderId",obj.get("orderId"));
			p.set("note",obj.get("note"));
			p.set("sampleInfo-id",obj.get("sampleInfo-id"));
			p.set("sampleInfo-name",obj.get("sampleInfo-name"));
			
			agrosTaskItemGrid.getStore().add(p);
			count++;
			agrosTaskItemGrid.startEditing(0, 0);
			
		}
			
	});
	}else{
		message("请选择样本！");
	}
	
}

//科技服务
function techDNAService(){
	 	var options = {};
	 	options.width = document.body.clientWidth - 470;
	 	options.height = document.body.clientHeight - 80;
		loadDialogPage(null, "选择提取任务单", "/technology/dna/techDnaServiceTask/showTechDnaServiceTaskDialog.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_techDnaServiceTask_div1").data("showTechDnaServiceTaskDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						var id1=obj.get("id");
						
						agrosTaskTempGrid.store.reload();
						var filter1 = function(record, id){
							var flag = true;
							if(id1){
								if (record.get("orderId").indexOf(id1)>=0){
									flag = true;
								}
								else{
									return false;
								}
							 }
							return flag;
						};
						var onStoreLoad1 = function(store, records, options){
						  store.filterBy(filter1);
						};
						agrosTaskTempGrid.store.on("load", onStoreLoad1);
						
					});
					
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}


