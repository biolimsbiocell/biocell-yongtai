var agrosTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'labCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'sumTotal',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'agrosTask-id',
		type:"string"
	});
	    fields.push({
		name:'agrosTask-name',
		type:"string"
	});
	    fields.push({
			name:'tempId',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-note',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-id',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-note',
			type:"string"
		});
	    fields.push({
			name:'sampleInfo-receiveDate',
			type:"string"
		});
	   fields.push({
			name:'sampleInfo-reportDate',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
		
	 
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6
		
		 
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*7
		
		 
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
		
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目名称',
		width:20*8
		 
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'index',
		width:20*6
		
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sumTotal',
		hidden : false,
		header:'总量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'处理结果',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'失败原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:'是否提交',
		width:20*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden : true,
		header:'下一步流向id',
		width:20*6
		
		 
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		editor : nextFlowCob
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样时间',
		width:20*6,
		
		renderer: formatDate
		 
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
		
		renderer: formatDate
		 
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6
		
	 
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告时间',
		width:20*6,
		
		renderer: formatDate
		 
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
		
		 
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理意见',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'agrosTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'agrosTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:'开箱指令id',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : false,
		header:'接收日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:'开箱指令',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/agros/agrosTask/showAgrosTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="琼脂糖电泳AGROS结果";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	var state = $("#agrosTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/agros/agrosTask/delAgrosTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : "批量结果",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = agrosTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						agrosTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						agrosTaskResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "批量下一步",
		handler : loadTestNextFlowCob
	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = agrosTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						agrosTaskResultGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						agrosTaskResultGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : '导出列表',
		handler : exportexcel
	});
	opts.tbar.push({
		text : '提交样本',
		handler : submitSample
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});	
	}
	agrosTaskResultGrid=gridEditTable("agrosTaskResultdiv",cols,loadParam,opts);
	$("#agrosTaskResultdiv").data("agrosTaskResultGrid", agrosTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function exportexcel() {
	agrosTaskResultGrid.title = '导出列表';
	var vExportContent = agrosTaskResultGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
var loadNextFlow;
	//下一步流向
	function loadTestNextFlowCob(){
		var records1 = agrosTaskResultGrid.getSelectRecord();
		var productId="";
		$.each(records1, function(j, k) {
			productId=k.get("productId");
		});
		 var options = {};
			options.width = 500;
			options.height = 500;
			loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=AgrosTask&productId="+productId, {
				"确定" : function() {
					var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					var records = agrosTaskResultGrid.getSelectRecord();
					if (selectRecord.length > 0) {
						$.each(records, function(i, obj) {
							$.each(selectRecord, function(a, b) {
								obj.set("nextFlowId", b.get("id"));
								obj.set("nextFlow", b.get("name"));
							});
						});
					}else{
						message("请选择您要选择的数据");
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
	}
	function setNextFlow(){
		var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
		var selectRecord = operGrid.getSelectionModel().getSelections();
		var records = agrosTaskResultGrid.getSelectRecord();
		if (selectRecord.length > 0) {
			$.each(records, function(i, obj) {
				$.each(selectRecord, function(a, b) {
					obj.set("nextFlowId", b.get("id"));
					obj.set("nextFlow", b.get("name"));
				});
			});
		}else{
			message("请选择您要选择的数据");
			return;
		}
		loadNextFlow.dialog("close");
	}	
	
	
//提交样本
function submitSample(){
	var id=$("#agrosTask_id").val();  
	if(agrosTaskResultGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	var record = agrosTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("submit")){
			flg=true;
		}
		if(record[i].get("nextFlowId")==""){
			message("有下一步未填写！");
			return;
		}
	}
	var grid=agrosTaskResultGrid.store;
	for(var i=0;i<grid.getCount();i++){
		if(grid.getAt(i).get("submit")==""){
			flg=true;
		}
		if(grid.getAt(i).get("nextFlowId")==""){
			message("有下一步未填写！");
			return;
		}
		
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		var records = [];
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		ajax("post", "/experiment/qc/agros/agrosTask/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				agrosTaskResultGrid.getStore().commitChanges();
				agrosTaskResultGrid.getStore().reload();
				message("提交成功！");
			} else {
				loadMarsk.hide();
				message("提交失败！");
			}
		}, null);	
	}else{
		message("没有需要提交的样本！");
	}
}
