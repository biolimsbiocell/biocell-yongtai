var agrosTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'agrosTask-id',
		type:"string"
	});
	    fields.push({
		name:'agrosTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6
		
		
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10
		
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:'实验员',
		width:20*10
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤编id',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'步骤编号',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:'步骤名称',
		width:20*6
		
		
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:'开始时间',
		width:20*6
		
	
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:'结束时间',
		width:20*6
		
	
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'状态',
		width:20*6
		
	
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6
		
	
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : true,
		header:'关联样本',
		width:20*6
	
	});
	cm.push({
		dataIndex:'agrosTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'agrosTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/agros/agrosTask/showAgrosTaskTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="执行步骤";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/agros/agrosTask/delAgrosTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//			text : '选择实验员',
//				handler : selectreciveUserFun
//		});
//	opts.tbar.push({
//			text : '选择相关主表',
//				handler : selectagrosTaskDialogFun
//		});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});	
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : '填加明细',
		handler : templateSelect
	});
	opts.tbar.push({
		text : '实验员',
		handler : loadTestUser
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : '打印执行单',
		handler : stampOrder
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : '开始执行',
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : '执行结束',
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : '生成结果明细',
		handler : addSuccess
	});
	
	agrosTaskTemplateGrid=gridEditTable("agrosTaskTemplatediv",cols,loadParam,opts);
	$("#agrosTaskTemplatediv").data("agrosTaskTemplateGrid", agrosTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

	//选择实验员
	function loadTestUser(){
		var win = Ext.getCmp('loadTestUser');
		if (win) {win.close();}
		var loadTestUser= new Ext.Window({
		id:'loadTestUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center',
		html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: '关闭',
		 handler: function(){
			 loadTestUser.close(); }  }]  });     loadTestUser.show(); 
	}
	function setreciveUserFun(id,name){
		var gridGrid = $("#agrosTaskTemplatediv").data("agrosTaskTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reciveUser-id',id);
			obj.set('reciveUser-name',name);
		});
		var win = Ext.getCmp('loadTestUser');
		if(win){
			win.close();
		}
	}
	var selreciveUserVal = function(win) {
		var operGrid = userDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#agrosTaskTemplatediv").data("agrosTaskTemplateGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('reciveUser-id',rec.get('id'));
				obj.set('reciveUser-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};

	//打印执行单
	function stampOrder(){
		var id=$("#agrosTask_template").val();
		if(id==""){
			message("请先选择模板!");
			return;
		}else{
			var url = '__report=agrosTaskTask.rptdesign&id=' + $("#agrosTask_id").val();
			commonPrint(url);}
	}
	//生成结果明细
	function addSuccess(){	
		
		var num =$("#agrosTask_template").val();
		if(num!=""){
			var setNum = agrosTaskReagentGrid.store;
			var selectRecords = agrosTaskItemGrid.store;
				for(var i=0;i<setNum.getCount();i++){
					setNum.getAt(i).set("sampleNum",selectRecords.getCount());
			}



		var getRecord = agrosTaskItemGrid.getSelectionModel().getSelections();
		var selectRecord = agrosTaskTemplateGrid.getSelectionModel().getSelections();
		var selRecord = agrosTaskResultGrid.store;
		if(selectRecord.length>0){
					$.each(selectRecord, function(i, obj) {
						var isRepeat = true;
						var codes = obj.get("sampleCodes");
						var scode = new Array();
						scode = codes.split(",");
						for(var i1=0; i1<scode.length; i1++){
							for(var j1=0;j1<selRecord.getCount();j1++){
								var getv = scode[i1];
								var setv = selRecord.getAt(j1).get("tempId");
								if(getv == setv){
									isRepeat = false;
									message("有重复的数据，请重新选择！");
									break;					
								}
							}
						}
						if(isRepeat){
								$.each(getRecord,function(a,b){
//									var productNum=b.get("productNum");
//										if(productNum==null||productNum==""||productNum==0){
//											message("请填写中间产物数量");
//										}else{
//											for(var k=1;k<=productNum;k++){
												var ob = agrosTaskResultGrid.getStore().recordType;
												agrosTaskResultGrid.stopEditing();
												var p = new ob({});
												p.isNew = true;
												
												p.set("tempId",b.get("code"));
												p.set("code",b.get("code"));
												p.set("name",b.get("name"));
												p.set("sampleCode",b.get("sampleCode"));
												p.set("labCode",b.get("labCode"));
												p.set("sampleType",b.get("sampleType"));
												p.set("productId",b.get("productId"));
												p.set("productName",b.get("productName"));
												p.set("",b.get("stepNum"));
												p.set("inspectDate",b.get("inspectDate"));
												p.set("acceptDate",b.get("acceptDate"));
												p.set("reportDate",b.get("reportDate"));
												p.set("volume",b.get("volume"));
//												p.set("",b.get("sampleNum"));
												//p.set("submit","0");
												p.set("result","1");
												//p.set("nextFlow","0");
//												p.set("volume",100);
//												p.set("unit",b.get("unit"));
												p.set("orderId",b.get("orderId"));
												p.set("",b.get("orderNumber"));
												p.set("state",b.get("state"));
												p.set("agrosTask-id",b.get("agrosTask-id"));
												p.set("agrosTask-name",b.get("agrosTask-name"));
												p.set("note",b.get("note"));
												p.set("sampleInfo-id",b.get("sampleInfo-id"));
												p.set("sampleInfo-note",b.get("sampleInfo-note"));
												p.set("sampleInfo-receiveDate",b.get("sampleInfo-receiveDate"));
												p.set("sampleInfo-reportDate",b.get("sampleInfo-reportDate"));
												
//												p.set("jkTaskId",b.get("jkTaskId"));
//												p.set("classify",b.get("classify"));
//												p.set("sampleType",b.get("sampleType"));
//												p.set("labCode",b.get("labCode"));
//												p.set("dataNum",b.get("dataNum"));
												$.ajax({
													type: "post",
													async: false,//设置为同步
													url: "/system/nextFlow/nextFlow/selectdnextId.action",
													data:{model : "AgrosTask",productId:b.get("productId")},
													dataType:"json",
													success: function(data) {
														p.set("nextFlowId",data.dnextId);
														p.set("nextFlow",data.dnextName);
													}
												});
												message("生成结果成功！");
												agrosTaskResultGrid.getStore().add(p);
												agrosTaskResultGrid.startEditing(0,0);
//											
//											}
//										}
								});
							
						}
					});
				
		}else{
			var selRecord = agrosTaskResultGrid.store;
			var flag;
			
			var getRecord = agrosTaskItemGrid.getAllRecord();
			$.each(getRecord,function(a,b){
				flag = true;
				for(var j1=0;j1<selRecord.getCount();j1++){
					var getv = b.get("code");
					var setv = selRecord.getAt(j1).get("tempId");
					if(getv == setv){
						flag = false;
						message("有重复的数据，请重新选择！");
						break;					
					}
				}
				if(flag==true){
//					var productNum=b.get("productNum");
//					if(productNum==null||productNum==""||productNum==0){
//						message("请填写中间产物数量");
//					}else{
//						for(var k=1;k<=productNum;k++){
							var ob = agrosTaskResultGrid.getStore().recordType;
							agrosTaskResultGrid.stopEditing();
							var p = new ob({});
							p.isNew = true;
							
							p.set("tempId",b.get("code"));
							p.set("code",b.get("code"));
							p.set("name",b.get("name"));
							p.set("sampleCode",b.get("sampleCode"));
							p.set("labCode",b.get("labCode"));
							p.set("sampleType",b.get("sampleType"));
							p.set("productId",b.get("productId"));
							p.set("productName",b.get("productName"));
							p.set("",b.get("stepNum"));
							p.set("inspectDate",b.get("inspectDate"));
							p.set("acceptDate",b.get("acceptDate"));
							p.set("reportDate",b.get("reportDate"));
							p.set("volume",b.get("volume"));
//							p.set("",b.get("sampleNum"));
							//p.set("submit","0");
							p.set("result","1");
							//p.set("nextFlow","0");
//							p.set("volume",100);
//							p.set("unit",b.get("unit"));
							p.set("orderId",b.get("orderId"));
							p.set("",b.get("orderNumber"));
							p.set("state",b.get("state"));
							p.set("agrosTask-id",b.get("agrosTask-id"));
							p.set("agrosTask-name",b.get("agrosTask-name"));
							p.set("note",b.get("note"));
							p.set("sampleInfo-id",b.get("sampleInfo-id"));
							p.set("sampleInfo-note",b.get("sampleInfo-note"));
							p.set("sampleInfo-receiveDate",b.get("sampleInfo-receiveDate"));
							p.set("sampleInfo-reportDate",b.get("sampleInfo-reportDate"));
							
							$.ajax({
								type: "post",
								async: false,//设置为同步
								url: "/system/nextFlow/nextFlow/selectdnextId.action",
								data:{model : "AgrosTask",productId:b.get("productId")},
								dataType:"json",
								success: function(data) {
									p.set("nextFlowId",data.dnextId);
									p.set("nextFlow",data.dnextName);
								}
							});
							message("生成结果成功！");
							agrosTaskResultGrid.getStore().add(p);
							agrosTaskResultGrid.startEditing(0,0);
//				}
//					}
					}
			});
			//message("请选择超声破碎明细！");
		}
		}else{
			message("请选择实验模板！");
		}
	}

	//获取开始时的时间
	function getStartTime(){
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=agrosTaskTemplateGrid.getSelectionModel();
		var setNum = agrosTaskReagentGrid.store;
		var selectRecords=agrosTaskItemGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
				for(var i=0; i<setNum.getCount();i++){
					var num = setNum.getAt(i).get("itemId");
					if(num==obj.get("code")){
						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
					}
				}
			});
		}else{
			message("请先选择数据！");
		}
		
		//将所选的样本，放到关联样本
		var selRecord=agrosTaskTemplateGrid.getSelectRecord();
		var codes = "";
			$.each(selectRecords.getSelections(), function(i, obj) {
				codes += obj.get("code")+",";
			});
			$.each(selRecord, function(i, obj) {
				obj.set("sampleCodes", codes);
			});
		
	}
	//获取停止时的时间
	function getEndTime(){
			var setRecord=agrosTaskItemGrid.store;
			var d = new Date();
			var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
			var selectRecord=agrosTaskTemplateGrid.getSelectionModel();
			var getIndex = agrosTaskTemplateGrid.store;
			var getIndexs = agrosTaskTemplateGrid.getSelectionModel().getSelections();
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					obj.set("endTime",str);	
					//将步骤编号，赋值到明细表
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i=0; i<setRecord.getCount(); i++){
						for(var j=0; j<scode.length; j++){
							if(scode[j]==setRecord.getAt(i).get("sampleCode")){
								setRecord.getAt(i).set("stepNum",obj.get("code"));
							}
						}
					}
					//将当前行的关联样本传到下一行
					getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
				});
			}
			
			
	}
	
	
	//选择实验步骤
	function templateSelect(){
		var option = {};
		option.width = 605;
		option.height = 558;
		loadDialogPage(null, "选择实验步骤", "/experiment/qc/agros/agrosTask/showTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
			"确定" : function() {
				var operGrid = $("#template_wait_grid_div").data("grid");
				var ob = agrosTaskTemplateGrid.getStore().recordType;
				agrosTaskTemplateGrid.stopEditing();
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code", obj.get("code"));
							p.set("name", obj.get("name"));
							agrosTaskTemplateGrid.getStore().add(p);
					});
					agrosTaskTemplateGrid.startEditing(0, 0);
					$(this).dialog("close");
					$(this).dialog("remove");
				} else {
					message("请选择您要选择的数据");
					return;
				}
			}
		}, true, option);
	}