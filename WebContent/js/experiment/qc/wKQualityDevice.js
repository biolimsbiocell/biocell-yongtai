﻿
var wKQualityDeviceGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'temperature',
		type:"string"
	});
	   fields.push({
		name:'speed',
		type:"string"
	});
	   fields.push({
		name:'time',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'isTestSuccess',
		type:"string"
	});
	    fields.push({
		name:'qc2100Task-id',
		type:"string"
	});
	    fields.push({
		name:'qc2100Task-name',
		type:"string"
	});
    fields.push({
		name:'itemId',
		type:"string"
	});
    fields.push({
		name:'tCos',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		header:'设备编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'temperature',
		header:'温度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:'速度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:'时间',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'isTestSuccess',
//		hidden : false,
//		header:'是否通过检验',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var storeisisTestSuccessCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '否' ], [ '1', '是' ] ]
	});
	var isTestSuccessCob = new Ext.form.ComboBox({
		store : storeisisTestSuccessCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isTestSuccess',
		hidden : false,
		header:'是否正常',
		width:20*6,
		editor : isTestSuccessCob,
		renderer : Ext.util.Format.comboRenderer(isTestSuccessCob)
	});
	cm.push({
		dataIndex:'qc2100Task-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qc2100Task-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'关联步骤编号',
		width:20*10
	});
	cm.push({
		dataIndex:'tCos',
		hidden : true,
		header:'模板设备ID',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQualitySampleTask/showWKQualityDeviceListJson.action?id="+ $("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title="设备明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#wKQualitySampleTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKQualitySampleTask/delWKQualityDevice.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectwkQualityFun
//		});
	
	
	
	
	
	
	
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = wKQualityDeviceGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							wKQualityDeviceGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : function (){
			//获取选择的数据
			var selectRcords=wKQualityTemplateGrid.getSelectionModel().getSelections();
			//获取全部数据
			var allRcords=wKQualityTemplateGrid.store;
			//选中的数量
			var length1=selectRcords.length;
			//全部数据量
			var length2=allRcords.getCount();
			if(length2>0){
				if(length1==1){
					var code="";
					$.each(selectRcords, function(i, obj) {
						code=obj.get("code");
					});
					if(code!=""){
						var ob = wKQualityDeviceGrid.getStore().recordType;
						var p = new ob({});
						p.isNew = true;
						p.set("itemId", code);
						wKQualityDeviceGrid.stopEditing();
						wKQualityDeviceGrid.getStore().insert(0, p);
						wKQualityDeviceGrid.startEditing(0, 0);
					}else{
						message("请先勾选或添加步骤编号！");
						return;
					}								
				}else if(length1>1){
					message("步骤明细中只能选择一条数据！");
					return;
				}else{
					message("请先选择步骤明细中数据！");
					return;
				}
			}else{
				message("步骤明细中数据为空！");
				return;
			}
		}
	});
	}
	wKQualityDeviceGrid=gridEditTable("wKQualityDevicediv",cols,loadParam,opts);
	$("#wKQualityDevicediv").data("wKQualityDeviceGrid", wKQualityDeviceGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectwkQualityFun(){
	var win = Ext.getCmp('selectwkQuality');
	if (win) {win.close();}
	var selectwkQuality= new Ext.Window({
	id:'selectwkQuality',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKQualitySelect.action?flag=wkQuality' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectwkQuality.close(); }  }]  });     selectwkQuality.show(); }
	function setwkQuality(id,name){
		var gridGrid = $("#wKQualityDevicediv").data("wKQualityDeviceGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('wKQualitySampleTask-id',id);
			obj.set('wKQualitySampleTask-name',name);
		});
		var win = Ext.getCmp('selectwkQuality')
		if(win){
			win.close();
		}
	}
	
