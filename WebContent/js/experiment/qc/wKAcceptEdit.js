﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#wKAccept_stateName").val();
	if(id!="完成"){
		load("/experiment/qc/wKAccept/showWKAcceptTemList.action", null, "#wKAcceptTempage");
//		$("#tabs1").tabs({
//			select : function(event, ui) {
//				if(ui.index==1){
//					load("/experiment/qc/wKAccept/showWKAcceptTechOrderList.action", { }, "#wKAcceptTechOrderdiv");
//				}
//			}
//		});
		$("#markup").css("width","75%");
	}else{
		$("#markup").css("width","100%");
		$("#wKAcceptTempage").remove();
	}
});
function add() {
	window.location = window.ctx + "/experiment/qc/wKAccept/editWKAccept.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/qc/wKAccept/showWKAcceptList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#wKAccept", {
					userId : userId,
					userName : userName,
					formId : $("#wKAccept_id").val(),
					title : $("#wKAccept_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#wKAccept_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var wKAcceptItemDivData = $("#wKAcceptItemdiv").data("wKAcceptItemGrid");
		document.getElementById('wKAcceptItemJson').value = commonGetModifyRecords(wKAcceptItemDivData);
	   /* var pOOLINGAcceptItemDivData = $("#pOOLINGAcceptItemdiv").data("pOOLINGAcceptItemGrid");
		document.getElementById('pOOLINGAcceptItemJson').value = commonGetModifyRecords(pOOLINGAcceptItemDivData);*/
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/qc/wKAccept/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/qc/wKAccept/copyWKAccept.action?id=' + $("#wKAccept_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#wKAccept_id").val() + "&tableId=wKAccept");
//}
$("#toolbarbutton_status").click(function(){
	var selstore= wKAcceptItemGrid.store;
	for(var i =0 ;i<selstore.getCount();i++){
		var result = selstore.getAt(i).get("result");
		if(result==""||result==null){
			message("部分接受明细处理结果为空，请处理后改变状态！");
			return;
		}
	}
	if ($("#wKAccept_id").val()){
		commonChangeState("formId=" + $("#wKAccept_id").val() + "&tableId=WKAccept");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#wKAccept_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'文库接收',
	    	   contentEl:'markup'
	       } ]
	   });
});

load("/experiment/qc/wKAccept/showWKAcceptItemList.action", {
				id : $("#wKAccept_id").val()
			}, "#wKAcceptItempage");
/*load("/experiment/qc/wKAccept/showPoolingAcceptItemList.action", {
				id : $("#wKAccept_id").val()
			}, "#pOOLINGAcceptItempage");*/
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
	text: '复制'
	});
item.on('click', editCopy);
	