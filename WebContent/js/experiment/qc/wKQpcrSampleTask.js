var wKQpcrSampleTaskGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'releasUser-id',
		type:"string"
	});
	    fields.push({
		name:'releasUser-name',
		type:"string"
	});
	    fields.push({
		name:'releaseDate',
		type:"string"
	});
	    fields.push({
		name:'testUser-id',
		type:"string"
	});
	    fields.push({
		name:'testUser-name',
		type:"string"
	});
	    fields.push({
		name:'testDate',
		type:"string"
	});
	    fields.push({
		name:'indexa',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
			name:'confirmDate',
			type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:30*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'releasUser-id',
		hidden:true,
		header:'下达人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'releasUser-name',
		header:'下达人',
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'releaseDate',
		header:'下达日期',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'confirmDate',
		header:'完成时间',
		width:25*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'testUser-id',
		hidden:true,
		header:'实验员ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'testUser-name',
		header:'实验员',
		hidden:true,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'testDate',
		header:'实验日期',
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		hidden:true,
		header:'选择执行单ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:'实验模板',
		
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'indexa',
		header:'INDEX',
		hidden:false,
		width:20*6,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:'工作流状态ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-id',
		header:'实验组',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptUser-name',
		header:'实验组',
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQpcrSampleTask/showWKQpcrSampleTaskListJson.action";
	var opts={};
	opts.title="QPCR质控";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	wKQpcrSampleTaskGrid=gridTable("wKQpcrSampleTaskdiv",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/qc/wKQpcrSampleTask/editWKQpcrSampleTask.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/qc/wKQpcrSampleTask/editWKQpcrSampleTask.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/qc/wKQpcrSampleTask/viewWKQpcrSampleTask.action?id=' + id;
}
function exportexcel() {
	wKQpcrSampleTaskGrid.title = '导出列表';
	var vExportContent = wKQpcrSampleTaskGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startreleaseDate").val() != undefined) && ($("#startreleaseDate").val() != '')) {
					var startreleaseDatestr = ">=##@@##" + $("#startreleaseDate").val();
					$("#releaseDate1").val(startreleaseDatestr);
				}
				if (($("#endreleaseDate").val() != undefined) && ($("#endreleaseDate").val() != '')) {
					var endreleaseDatestr = "<=##@@##" + $("#endreleaseDate").val();

					$("#releaseDate2").val(endreleaseDatestr);

				}
				
				if (($("#starttestDate").val() != undefined) && ($("#starttestDate").val() != '')) {
					var starttestDatestr = ">=##@@##" + $("#starttestDate").val();
					$("#testDate1").val(starttestDatestr);
				}
				if (($("#endtestDate").val() != undefined) && ($("#endtestDate").val() != '')) {
					var endtestDatestr = "<=##@@##" + $("#endtestDate").val();

					$("#testDate2").val(endtestDatestr);

				}
				
				
				commonSearchAction(wKQpcrSampleTaskGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
