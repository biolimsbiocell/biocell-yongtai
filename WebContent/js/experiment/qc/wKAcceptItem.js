﻿var wKAcceptItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'wkCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'wkType',
		type:"string"
	});
	   fields.push({
		name:'wkVolume',
		type:"string"
	});
	   fields.push({
		name:'fragminSize',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'location',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
//	    type:"date",
//		dateFormat:"Y-m-d"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
//		type:"date",
//		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'wkAccept-id',
		type:"string"
	});
	    fields.push({
		name:'wkAccept-name',
		type:"string"
	});
    fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   fields.push({
		name:'xyName',
		type:"string"
	});
   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
		   fields.push({
			name:'i5',
			type:"string"
		});
		   fields.push({
			name:'i7',
			type:"string"
		});
		   fields.push({
				name:'indexa',
				type:"string"
			});
		   fields.push({
				name:'sampleNum',
				type:"string"
			});
		   fields.push({
				name:'tempId',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'wkCode',
		header:'文库编号',
		width:20*6,
		hidden : false
	});
	cm.push({
		dataIndex:'code',
		header:'样本编号',
		hidden : false,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
	});
	var wkTypeCobs = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [['0','文库质控'], [ '2', '2100质控' ], [ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCobs,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : false,
		header:'文库类型 <font color="red">*<font>',
		width:20*6,
		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单编号',
		width:20*6,
		hidden : true
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6
	});

	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6,
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6,
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'INDEX',
		width:20*6,
	});
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i5',
		width:20*6,
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i7',
		width:20*6,
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:'接收日期',
		width:25*6
		//renderer: formatDate
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:25*6
		//renderer: formatDate
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机',
		width:20*6
	});
	cm.push({
		dataIndex:'xyName',
		hidden : false,
		header:'储位',
		width:30*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkVolume',
		hidden : false,
		header:'文库体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'fragminSize',
		hidden : false,
		header:'片段化大小',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '0',
				name : '不合格'
			}, {
				id : '1',
				name : '合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'处理结果',
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'异常原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:20*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:'储位',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
	});

	cm.push({
		dataIndex:'wkAccept-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'wkAccept-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'techTaskId',
		hidden : false,
		header:'科技服务任务单',
		width:20*6,
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同ID',
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKAccept/showWKAcceptItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title="文库接收明细";
	opts.height =  document.body.clientHeight-180;
	opts.tbar = [];
	var state = $("#wKAccept_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKAccept/delWKAcceptItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				wKAcceptItemGrid.getStore().commitChanges();
				wKAcceptItemGrids.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    	
	opts.tbar.push({
		text : "文库类型",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_wkType_div"), "文库类型", null, {
				"确定" : function() {
					var records = wKAcceptItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var iswkType = $("#iswkType").val();
						wKAcceptItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("wkType", iswkType);
						});
						wKAcceptItemGrid.startEditing(0, 0);
					}else{
						message("请先选择数据！");
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "批量处理",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ok_div"), "批量处理", null, {
				"确定" : function() {
					var records = wKAcceptItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isExecute = $("#isExecute").val();
						wKAcceptItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", isExecute);
						});
						wKAcceptItemGrid.startEditing(0, 0);
					}else{
						message("请先选择数据！");
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	
	opts.tbar.push({
		text : "批量导入",
		handler : function() {
			
		
			//$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br />字段：接收样本号、体积、浓度、260/230、260/280");
			$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br />字段：接收样本号、文库体积、片段大小、处理结果、异常原因");
			$("#many_bat_text").val("");
			$("#many_bat_text").attr("style", "width:465px;height: 339px");
			var options = {};
			options.width = 494;
			options.height = 508;
			loadDialogPage($("#many_bat_div"), "批量导入", null, {
				"确定" : function() {
					var positions = $("#many_bat_text").val();
					if (!positions) {
						message("请填写信息！");
						return;
					}
					var posiObj = {};
					var posiObj1 = {};
					var posiObj2 = {};
					var posiObj3 = {};
//					var posiObj4 = {};
					var array = formatData(positions.split("\n"));
					$.each(array, function(i, obj) {
						var tem = obj.split("\t");
						posiObj[tem[0]] = tem[1];
						posiObj1[tem[0]] = tem[2];
						posiObj2[tem[0]] = tem[3];
						posiObj3[tem[0]] = tem[4];
//						posiObj4[tem[0]] = tem[5];
					});
					var records = wKAcceptItemGrid.getAllRecord();
					wKAcceptItemGrid.stopEditing();
					$.each(records, function(i, obj) {
						if (posiObj[obj.get("sampleCode")]) {
							obj.set("wkVolume", posiObj[obj.get("sampleCode")]);
						}
						if (posiObj1[obj.get("sampleCode")]) {
							obj.set("fragminSize", posiObj1[obj.get("sampleCode")]);
						}
						if (posiObj2[obj.get("sampleCode")]) {
							obj.set("result", posiObj2[obj.get("sampleCode")]);
						}
						if (posiObj3[obj.get("sampleCode")]) {
							obj.set("reason", posiObj3[obj.get("sampleCode")]);
						}
						//obj.set("unit-id","ng");
						//obj.set("unit-name","ng");
						//obj.set("result","1");
						//obj.set("method","1");
								
					});
					wKAcceptItemGrid.startEditing(0, 0);
					$(this).dialog("close");
				}
			}, true, options);
		}
	});	
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	}
	wKAcceptItemGrid=gridEditTable("wKAcceptItemdiv",cols,loadParam,opts);
	$("#wKAcceptItemdiv").data("wKAcceptItemGrid", wKAcceptItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
