$(function() {
	load("/experiment/qc/wKException/showQc2100AbnormalList.action", null, "#wKQualityExceptiondiv");
	$("#tabs").tabs({
		select : function(event, ui) {
			if(ui.index==1){
				load("/experiment/qc/wKException/showQcQpcrAbnormalList.action", null, "#wKQpcrExceptiondiv");
			}
			if(ui.index==2){
				load("/experiment/qc/wKException/showQcPoolingAbnormalList.action", null, "#qcPoolingAbnormaldiv");
				
			}
		}
	});
});

