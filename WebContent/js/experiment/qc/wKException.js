var wKExceptionGrid;
$(function(){
	var cols={};
    var fields=[];
    fields.push({
    	name:'id',
		type:"string"
    });
    fields.push({
    	name:'name',
		type:"string"
    });
    fields.push({
    	name:'note',
		type:"string"
    });
    fields.push({
    	name:'stateName',
		type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKException/showWKExceptionListJson.action";
	var opts={};
	opts.title="文库异常";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	wKExceptionGrid=gridTable("show_wKException_div",cols,loadParam,opts);
});
function add(){
		window.location=window.ctx+'/experiment/qc/wKException/editWKException.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/qc/wKException/editWKException.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/qc/wKException/viewWKException.action?id=' + id;
}
function exportexcel() {
	wKExceptionGrid.title = '导出列表';
	var vExportContent = wKExceptionGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				
				commonSearchAction(wKExceptionGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
