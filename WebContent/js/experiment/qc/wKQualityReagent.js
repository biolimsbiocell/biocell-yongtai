﻿
var wKQualityReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'count',
		type:"string"
	});
	   fields.push({
		name:'oneNum',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
   fields.push({
		name:'itemId',
		type:"string"
	});
   fields.push({
		name:'tReagent',
		type:"string"
	});   
	   fields.push({
		name:'isTestSuccess',
		type:"string"
	});
	    fields.push({
		name:'qc2100Task-id',
		type:"string"
	});
	    fields.push({
		name:'qc2100Task-name',
		type:"string"
	});
    fields.push({
		name:'sn',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var codes =new Ext.form.TextField({
        allowBlank: false
	});
	codes.on('focus', function() {
		var selectRecord = wKQualityReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("code");
				var tid = $("#wKQualitySampleTask_template").val();
				loadReagentItemByCode(tid);
			});
		}
	});
	cm.push({
		dataIndex:'code',
		header:'原辅料编号',
		width:20*6,
		editor : codes
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:45*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	//鼠标单击触发事件 
	var batchs =new Ext.form.TextField({
            allowBlank: false
    });
	batchs.on('focus', function() {
		var selectRecord = wKQualityReagentGrid.getSelectionModel();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var code=obj.get("code");
				loadStorageReagentBuy(code);
			});
		}
	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:'批次',
		width:20*6,
		editor:batchs
	});
	cm.push({
		dataIndex:'count',
		hidden : false,
		header:'数量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'oneNum',
		hidden : false,
		header:'单个用量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本用量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'用量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sn',editor : new Ext.form.TextField({ allowBlank : true }),
		hidden : false,
		header:'sn',
		width:20*6
	});
//	cm.push({
//		dataIndex:'isTestSuccess',
//		hidden : false,
//		header:'是否通过检验',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var storeisisTestSuccessCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '否' ], [ '1', '是' ] ]
	});
	var isTestSuccessCob = new Ext.form.ComboBox({
		store : storeisisTestSuccessCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isTestSuccess',
		hidden : false,
		header:'是否正常',
		width:20*6,
		editor : isTestSuccessCob,
		renderer : Ext.util.Format.comboRenderer(isTestSuccessCob)
	});
	cm.push({
		dataIndex:'qc2100Task-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qc2100Task-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'关联步骤编号',
		width:20*10
	});
	cm.push({
		dataIndex:'tReagent',
		hidden : true,
		header:'模板原辅料ID',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQualitySampleTask/showWKQualityReagentListJson.action?id="+ $("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title="原辅料明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#wKQualitySampleTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKQualitySampleTask/delWKQualityReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectwkQualityFun
//		});
	
	
	
	
	
	
	
	
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = wKQualityReagentGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							wKQualityReagentGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : function (){
			//获取选择的数据
			var selectRcords=wKQualityTemplateGrid.getSelectionModel().getSelections();
			//获取全部数据
			var allRcords=wKQualityTemplateGrid.store;
			//选中的数量
			var length1=selectRcords.length;
			//全部数据量
			var length2=allRcords.getCount();
			if(length2>0){
				if(length1==1){
					var code="";
					$.each(selectRcords, function(i, obj) {
						code=obj.get("code");
					});
					if(code!=""){
						showStorageList(code);
					}else{
						message("请先勾选或添加步骤编号！");
						return;
					}								
				}else if(length1>1){
					message("步骤明细中只能选择一条数据！");
					return;
				}else{
					message("请先选择步骤明细中数据！");
					return;
				}
			}else{
				message("步骤明细中数据为空！");
				return;
			}
		}
	});
	}
	wKQualityReagentGrid=gridEditTable("wKQualityReagentdiv",cols,loadParam,opts);
	$("#wKQualityReagentdiv").data("wKQualityReagentGrid", wKQualityReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
//加载采购原辅料
function loadStorageReagentBuy(code){
	var win = Ext.getCmp('loadStorageReagentBuy');
	if (win) {win.close();}
	var loadStorageReagentBuy= new Ext.Window({
	id:'loadStorageReagentBuy',modal:true,title:'选择采购原辅料',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/system/template/template/showStorageReagentBuyList.action?flag=StorageReagentBuy&codes="+code+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 loadStorageReagentBuy.close(); }  }]  });     loadStorageReagentBuy.show();
}
function setStorageReagentBuy(rec){
	
	var gridGrid = $("#wKQualityReagentdiv").data("wKQualityReagentGrid");
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		
		obj.set('batch',rec.get("code"));
	});
	var win = Ext.getCmp('loadStorageReagentBuy')
	if(win){
		win.close();
	}
}
//加载库存主数据
function showStorageList(code){
	var options = {};
	options.width = 900;
	options.height = 460;
	var url = "/system/template/template/showStorageList.action";
	loadDialogPage(null, "库存主数据", url, {
		"确定" : function() {
			selRecord = showStorageListGrid.getSelectionModel();
			if (selRecord.getSelections().length > 0) {
				$.each(selRecord.getSelections(), function(i, obj) {
					wKQualityReagentGrid.stopEditing();
					var ob = wKQualityReagentGrid.getStore().recordType;
					var p = new ob({});
					p.isNew = true;

					p.set("code",obj.get("id"));
					p.set("name",obj.get("name"));
					p.set("itemId",code);
				
					wKQualityReagentGrid.getStore().add(p);	
				});
				wKQualityReagentGrid.startEditing(0, 0);
				options.close();
			}else{
				message("请选择数据！");
			}
		}
	}, true, options);
}
function selectwkQualityFun(){
	var win = Ext.getCmp('selectwkQuality');
	if (win) {win.close();}
	var selectwkQuality= new Ext.Window({
	id:'selectwkQuality',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKQualitySelect.action?flag=wkQuality' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectwkQuality.close(); }  }]  });     selectwkQuality.show(); }
	function setwkQuality(id,name){
		var gridGrid = $("#wKQualityReagentdiv").data("wKQualityReagentGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('wKQualitySampleTask-id',id);
			obj.set('wKQualitySampleTask-name',name);
		});
		var win = Ext.getCmp('selectwkQuality');
		if(win){
			win.close();
		}
	}
	
	//根据原辅料编号查询原辅料明细
//	function loadReagentItemByCode(code,tid){
	function loadReagentItemByCode(tid){
		var options = {};
		options.width = 900;
		options.height = 460;
//		var url="/system/template/template/showReagentItemByCodeList.action?code="+code+"&tid="+tid;
		var url="/system/template/template/showReagentItemByCodeList.action?tid="+tid;
		loadDialogPage(null, "选择明细", url, {
			 "确定": function() {
				 selRecord = reagentItem1Grid.getSelectionModel();
					if (selRecord.getSelections().length > 0) {
						$.each(selRecord.getSelections(), function(i, obj) {
							wKQualityReagentGrid.stopEditing();
							var ob = wKQualityReagentGrid.getStore().recordType;
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.get("code"));
							p.set("name",obj.get("name"));
							p.set("batch",obj.get("batch"));
							p.set("isTestSuccess",obj.get("isGood"));
							p.set("itemId",obj.get("itemId"));
							
							p.set("oneNum",obj.get("num"));
							p.set("note",obj.get("note"));
							wKQualityReagentGrid.getStore().add(p);	
						});
						wKQualityReagentGrid.startEditing(0, 0);
						options.close();
					}else{
						message("请选择数据！");
					}
			}
		}, true, options);
}
