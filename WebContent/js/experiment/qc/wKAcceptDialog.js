var wKAcceptDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'receiveUser-id',
		type:"string"
	});
	    fields.push({
		name:'receiveUser-name',
		type:"string"
	});
	    fields.push({
		name:'receiverDate',
		type:"string"
	});
	    fields.push({
		name:'location',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'receiveUser-id',
		header:'接收人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'receiveUser-name',
		header:'接收人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'receiverDate',
		header:'接收日期',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'location',
		header:'储位提示',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKAccept/showWKAcceptListJson.action";
	var opts={};
	opts.title="文库接收";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setWKAcceptFun(rec);
	};
	wKAcceptDialogGrid=gridTable("show_dialog_wKAccept_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(wKAcceptDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
