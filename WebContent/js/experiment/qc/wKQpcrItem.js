var wKQpcrItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
    	fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
   fields.push({
		name:'counts',
		type:"string"
	});
   fields.push({
		name:'colCode',
		type:"string"
	});
   fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'wkId',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
	   name:'femaleSampleNo',
	   type:"string"
   });
	   fields.push({
		name:'molality',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'wkType',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"string"
	});
	   fields.push({
	    name:'acceptDate',
	    type:"string"
    });
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"string"
	});
	   fields.push({
		name:'qualityProduct-id',
		type:"string"
	});
	    fields.push({
		name:'qualityProduct-name',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-id',
		type:"string"
	});
	    fields.push({
		name:'qcQpcrTask-name',
		type:"string"
	});
	    
    fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
   fields.push({
		name:'techTaskId',
		type:"string"
	});
	   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
		   fields.push({
			name:'i5',
			type:"string"
		});
		   fields.push({
			name:'i7',
			type:"string"
		});
	   fields.push({
			name:'sampleNum',
			type:"string"
		});
		   fields.push({
			name:'sampleConsume',
			type:"string"
		});
		   fields.push({
				name:'tempId',
				type:"string"
			}); 
		   fields.push({
				name:'wkConcentration',
				type:"string"
			});
		   fields.push({
				name:'wkVolume',
				type:"string"
			});
		   fields.push({
				name:'wkSumTotal',
				type:"string"
			});
		   fields.push({
				name:'loopNum',
				type:"string"
			});
		   fields.push({
				name:'pcrRatio',
				type:"string"
			});
		   fields.push({
				name:'rin',
				type:"string"
			});
		   fields.push({
				name:'labCode',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-id',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-note',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-receiveDate',
				type:"string"
			});
		   fields.push({
				name:'sampleInfo-reportDate',
				type:"string"
			});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单编号',
		width:20*6,
		hidden : true
	});
	cm.push({
		dataIndex:'code',
		header:'样本编号',
		hidden : false,
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'wkId',
		hidden : true,
		header:'文库编号',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexa',
		hidden : false,
		header:'INDEX',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleConsume',
		hidden : false,
		header:'样本用量',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'i5',
		hidden : false,
		header:'i5',
		width:20*6
	});
	cm.push({
		dataIndex:'i7',
		hidden : false,
		header:'i7',
		width:20*6
	});
	cm.push({
		dataIndex:'wkConcentration',
		header:'浓度ng/ul',
		width:20*6
	});
	cm.push({
		dataIndex:'wkVolume',
		header:'体积ul',
		width:20*6
	});
	cm.push({
		dataIndex:'wkSumTotal',
		header:'总量ng',
		width:20*6
	});
	cm.push({
		dataIndex:'loopNum',
		header:'循环数',
		width:20*6
	});
	cm.push({
		dataIndex:'pcrRatio',
		header:'扩增比例',
		width:20*6
	});
	cm.push({
		dataIndex:'rin',
		hidden : true,
		header:'RIN',
		width:20*6
	});
	var resultCobs = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '不合格' ], [ '1', '合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : resultCobs,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'处理结果',
		width:20*6,
		//editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:'实验排序号',
		width:10*7,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'失败原因',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : false,
		header:'步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'描述',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var wkTypeCobs = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '2100质控' ], [ '2', '2100 or Caliper' ], [ '1', 'QPCR质控' ] ]
	});
	var wkTypeCob = new Ext.form.ComboBox({
		store : wkTypeCobs,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'wkType',
		hidden : true,
		header:'文库类型',
		width:20*6,
//		editor : wkTypeCob,
		renderer : Ext.util.Format.comboRenderer(wkTypeCob)
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:50*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机',
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6
	});

	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:20*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:20*6
	});
	cm.push({
		dataIndex:'qcQpcrTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qcQpcrTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'volume',
		header:'体积',
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:'单位',
		width:20*6
	});
	cm.push({
		dataIndex:'techTaskId',
		hidden : true,
		header:'科技服务任务单',
		width:20*6,
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同ID',
		width:20*6
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目ID',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:'临床/科技服务',
		hidden : true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-id',
		hidden : true,
		header:'开箱指令id',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-receiveDate',
		hidden : false,
		header:'接收日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-reportDate',
		hidden : false,
		header:'应出报告日期',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:'开箱指令',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qc/wKQpcrSampleTask/showWKQpcrItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title="QPCR质控明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state = $("#wKQpcrSampleTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qc/wKQpcrSampleTask/delWKQpcrItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				wKQpcrItemGrid.getStore().commitChanges();
				wKQpcrItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectwKQpcrFun
//		});
	/*opts.tbar.push({
		text : '添加质控品',
		handler :addQuality
	});*/
	/*opts.tbar.push({
		text : '开始实验',
		handler :startTest
	});*/
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = wKQpcrItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							wKQpcrItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
//	opts.tbar.push({
//		text : "批量数据",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_data_div"), "批量数据", null, {
//				"确定" : function() {
//					var records = wKQpcrItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var molality = $("#molality").val();
//						wKQpcrItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("molality", molality);
//						});
//						wKQpcrItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : "批量结果",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_result_div"), "批量结果", null, {
//				"确定" : function() {
//					var records = wKQpcrItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var result = $("#result").val();
//						wKQpcrItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("result", result);
//						});
//						wKQpcrItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	}
	wKQpcrItemGrid=gridEditTable("wKQpcrItemdiv",cols,loadParam,opts);
	$("#wKQpcrItemdiv").data("wKQpcrItemGrid", wKQpcrItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//添加质控品
function addQuality(){
	var options = {};
	options.width = 900;
	options.height = 460;
	var setRecord = wKQpcrItemGrid.getSelectionModel().getSelections();
	if(setRecord.length >0){
		var url="/system/quality/qualityProduct/showSelectQualityList.action";
		loadDialogPage(null, "选择明细", url, {
			"确定": function() {
				var getQuality = selectQualityGrid.getSelectionModel().getSelections();
				var qualityId="";
				var qualityName="";
				if(getQuality.length > 0){
					$.each(getQuality, function(i, obj) {
						qualityId += obj.get("id")+",";
						qualityName += obj.get("name")+",";
					});
					$.each(setRecord, function(i, obj) {
						obj.set("qualityProduct-id", qualityId);
						obj.set("qualityProduct-name", qualityName);
					});
					options.close();
				}else{
					message("请先选择质控品！");
				}
		
			}
		}, true, options);
	}else{
		message("请先选择实验样本！");
	}
}

/*//开始实验
function startTest(){
	var selectRecord=wKQpcrItemGrid.getSelectionModel();
	var selRecord=wKQpcrTemplateGrid.getSelectRecord();
	if (selectRecord.getSelections().length > 0) {
		var codes = "";
		$.each(selectRecord.getSelections(), function(i, obj) {
			codes += obj.get("sampleId")+",";
		});
		if (!selRecord.length) {
			selRecord = wKQpcrTemplateGrid.getAllRecord();
		}
		if (selRecord&&selRecord.length>0) {
			wKQpcrTemplateGrid.stopEditing();
			$.each(selRecord, function(i, obj) {
				obj.set("codes", codes);
			});
			wKQpcrTemplateGrid.startEditing(0, 0);
		}
			
	}

}*/
function selectwKQpcrFun(){
	var win = Ext.getCmp('selectwKQpcr');
	if (win) {win.close();}
	var selectwKQpcr= new Ext.Window({
	id:'selectwKQpcr',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/WKQpcrSelect.action?flag=wKQpcr' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectwKQpcr.close(); }  }]  });     selectwKQpcr.show(); }
	function setwKQpcr(id,name){
		var gridGrid = $("#wKQpcrItemdiv").data("wKQpcrItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('qcQpcrTask-id',id);
			obj.set('qcQpcrTask-name',name);
		});
		var win = Ext.getCmp('selectwKQpcr')
		if(win){
			win.close();
		}
	}
	
