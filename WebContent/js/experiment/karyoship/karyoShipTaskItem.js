var karyoShipTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'experimentCode',
		type:"string"
	});
	   fields.push({
		name:'checkCode',
		type:"string"
	});

		fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'karyoShipTask-id',
		type:"string"
	});
	    fields.push({
		name:'karyoShipTask-name',
		type:"string"
	});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	   fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
	   fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
	   fields.push({
		name:'productNum',
		type:"string"
	});
	   fields.push({
		name:'harvestDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'shipDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'inoculateDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'preReapDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'experimentCode',
		hidden : false,
		header:biolims.common.expCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'checkCode',
		hidden : false,
		header:biolims.common.checkCode,
		sortable:true,
		width:25*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6
	});
	cm.push({
		dataIndex:'shipDate',
		hidden : false,
		header:biolims.common.flakingDate,
		width:25*6,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'harvestDate',
		hidden : false,
		header:biolims.common.harvesTime,
		width:25*6,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'inoculateDate',
		hidden : true,
		header:biolims.user.inoculationTime,
		width:25*6,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:biolims.common.acceptDate,
		width:20*6,
		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:biolims.common.dicSampleTypeId,
		width:20*6,
		sortable:true
	});
	var testDicSampleType2 =new Ext.form.TextField({
        allowBlank: false
	});
	testDicSampleType2.on('focus', function() {
		loadTestDicSampleType2();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		header:biolims.common.dicSampleTypeName+'<font color="red" size="4px">*</font>',
		width:15*10,
		sortable:true,
		hidden:true,
		editor : testDicSampleType2
	});
	cm.push({
		dataIndex:'productNum',
		hidden : true,
		header:biolims.common.productNum+'<font color="red" size="4px">*</font>',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.equipment.taskId,
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.sample.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:biolims.common.reportDate,
		width:20*6,
		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'karyoShipTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'karyoShipTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyoship/karyoShipTask/showKaryoShipTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit=500;
	var opts={};
	opts.title=biolims.common.samplePreparationDetail;
	opts.height =  document.body.clientHeight-240;
	opts.tbar = [];
	if($("#karyoShipTask_state").val()!="1"){
    opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyoship/karyoShipTask/delKaryoShipTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				karyoShipTaskItemGrid.getStore().commitChanges();
				karyoShipTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//		text : "批量中间产物类型",
//		handler : function() {
//			var options = {};
//			options.width = document.body.clientWidth-800;
//			options.height = document.body.clientHeight-40;
//			loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
//				"确定" : function() {
//					var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
//					var selectRecord = operGrid.getSelectionModel().getSelections();
//					var records = karyoShipTaskItemGrid.getSelectRecord();
//					if (selectRecord.length > 0) {
//						$.each(selectRecord, function(i, obj) {
//							$.each(records, function(a, b) {
//								b.set("dicSampleType-id", obj.get("id"));
//								b.set("dicSampleType-name", obj.get("name"));
//							});
//						});
//					}else{
//						message("请选择您要选择的数据");
//						return;
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	
//	opts.tbar.push({
//		text : "批量产物数量",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_productNum_div"), "批量产物数量", null, {
//				"确定" : function() {
//					var records = karyoShipTaskItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var productNum = $("#productNum").val();
//						karyoShipTaskItemGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("productNum", productNum);
//						});
//						karyoShipTaskItemGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text :biolims.common.batchPreparationTime,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_ready_div"), "批量接种时间", null, {
				"确定" : function() {
					var records = karyoShipTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var date = $("#date").val()+" 00:00:00";
						date = date.replace(/-/g,"/");
						var rdate = new Date(date);
						karyoShipTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("shipDate", rdate);
						});
						karyoShipTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	}
	karyoShipTaskItemGrid=gridEditTable("karyoShipTaskItemdiv",cols,loadParam,opts);
	$("#karyoShipTaskItemdiv").data("karyoShipTaskItemGrid", karyoShipTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(karyoShipTaskItemGrid);
	var id=$("#karyoShipTask_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/karyoship/karyoShipTask/saveKaryoShipTaskItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					karyoShipTaskItemGrid.getStore().commitChanges();
					karyoShipTaskItemGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

var loadDicSampleType2;
//查询样本类型
function loadTestDicSampleType2(){
	var options = {};
	options.width = document.body.clientWidth-800;
	options.height = document.body.clientHeight-40;
	loadDicSampleType2=loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action?a=2", {
		"确定" : function() {
			var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = karyoShipTaskItemGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					$.each(records, function(a, b) {
						b.set("dicSampleType-id", obj.get("id"));
						b.set("dicSampleType-name", obj.get("name"));
					});
				});
			}else{
				message("请选择您要选择的数据");
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}
function setDicSampleType2(){
	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = karyoShipTaskItemGrid.getSelectRecord();
	
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("dicSampleType-id", b.get("id"));
				obj.set("dicSampleType-name", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadDicSampleType2.dialog("close");

}