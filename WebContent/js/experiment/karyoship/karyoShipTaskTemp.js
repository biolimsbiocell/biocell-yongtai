var karyoShipTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'reapDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   	//缴费状态
		fields.push({
		name : 'chargeNote',
		type : "string"
	});
		fields.push({
		name:'inoculateDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
		fields.push({
		name:'harvestDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.user.name,
		width:50*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:biolims.user.patientName,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		sortable:true,
		width:20*6
	});
	var storechargeNoteCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.payPayment ], [ '2', biolims.common.alreadyPaid ], 
		         [ '3', biolims.common.settlementSettled ], [ '4',biolims.sample.kyPro ], [ '5', biolims.common.free ] ]
	});
	var chargeNoteCob = new Ext.form.ComboBox({
		store : storechargeNoteCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'chargeNote',
		hidden : false,
		header:biolims.common.payStatus,
		width:20*6,
		//editor : chargeNoteCob,
		renderer : Ext.util.Format.comboRenderer(chargeNoteCob)
	});
	cm.push({
		dataIndex:'harvestDate',
		hidden : false,
		header:biolims.common.harvesTime,
		width:20*6,
		sortable:true,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'inoculateDate',
		hidden : false,
		header:biolims.user.inoculationTime,
		width:20*6,
		
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
//	cm.push({
//		dataIndex:'reapDate',
//		hidden : false,
//		header:'收获时间',
//		width:20*6,
//		renderer: formatDate,
//	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:biolims.common.acceptDate,
		width:20*6,
		renderer: formatDate,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:biolims.common.reportDate,
		width:20*6,
		renderer: formatDate,
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.wk.sequenceBillName,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:biolims.common.note,
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyoship/karyoShipTask/showKaryoShipTaskTempListJson.action";
	loadParam.limit=500;
	var opts={};
	opts.title=biolims.common.waitPreTastSample;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text :biolims.common.addToTask,
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text :biolims.common.checkCode,
		handler : function() {
			$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：样本号 ");
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
			$("#many_bat_div"),
			biolims.common.checkCode,
			null,
			{
				"确定" : function() {
					var positions = $("#many_bat_text").val();
					if (!positions) {
						message("请填写条码号！");
						return;
					}
					var array = positions.split("\n");
					var records = karyoShipTaskTempGrid.getAllRecord();
					var store = karyoShipTaskTempGrid.store;
					var count = 0;
					var isOper = true;
					var buf = [];
					var buf1 = [];
					karyoShipTaskTempGrid.stopEditing();
					$.each(array,function(i, obj) {
						$.each(records, function(i, obj1) {
							if(obj==obj1.get("code")){
								buf.push(store.indexOfId(obj1.get("id")));
							}else{
								
							}
							
						});
					});
					
					//判断那些样本没有匹配到
					var nolist = new Array();
					var templist = new Array();
					$.each(records, function(i, obj1) {
						templist.push(obj1.get("code"));
					});
					$.each(array,function(i, obj) {
						if(templist.indexOf(obj) == -1){
							nolist.push(obj);
						}
					});
					if(nolist!="" && nolist.length>0){
						message("没有匹配到的样本有："+nolist);
					}
					karyoShipTaskTempGrid.getSelectionModel().selectRows(buf);
					if(isOper==false){

					}else{
						addItem();
					}
					karyoShipTaskTempGrid.startEditing(0, 0);
					$(this).dialog("close");
				}
			}, true, options);

		}
	});
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	karyoShipTaskTempGrid=gridEditTable("karyoShipTaskTempdiv",cols,loadParam,opts);
	$("#karyoShipTaskTempdiv").data("karyoShipTaskTempGrid", karyoShipTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


//从左边添加到右边的明细中
function addItem(){
	var selRecord = karyoShipTaskTempGrid.getSelectionModel().getSelections();//从左边获取数据
	var getRecord = karyoShipTaskItemGrid.store;//填充到当前的明细中
	var b=$("#karyoShipTask_createDate").val();
	var count=1;
	var max=0;
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var isRepeat = false;
			for(var j=0; j<getRecord.getCount();j++){
				var getData = getRecord.getAt(j).get("tempId");
				if(getData==obj.get("id")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					break;
				}
			}
			//获取最大排序号
			for(var i=0; i<getRecord.getCount();i++){
				var a=getRecord.getAt(i).get("experimentCode");
				if(a>max){
					max=a;
				}
			}
			
			if(!isRepeat){
				var ob = karyoShipTaskItemGrid.getStore().recordType;
				karyoShipTaskItemGrid.stopEditing();
				var p= new ob({});
				p.set("experimentCode",Number(max)+count);
				p.set("code",obj.get("code"));
				p.set("sampleCode",obj.get("sampleCode"));
				p.set("tempId",obj.get("id"));
				var productId=obj.get("productId");
				if(productId.substring(productId.length-1,productId.length)==","){
					p.set("productId",productId.substring(0,productId.length-1));
				}else{
					p.set("productId",productId);
				}
				var productName=obj.get("productName");
				if(productName.substring(productName.length-1,productName.length)==","){
					p.set("productName",productName.substring(0,productName.length-1));
				}else{
					p.set("productName",productName);
				}
				///获取当前日期,并格式化
				/*var curDate = new Date();
				var time = curDate.getFullYear()+"-"+(curDate.getMonth())+"-"+curDate.getDate();
				alert(time);*/
				var date = b+" 00:00:00";
				date = date.replace(/-/g,"/");
				var b1 = new Date(date);
				p.set("shipDate",b1);
				p.set("reportDate",obj.get("reportDate"));
				p.set("acceptDate",obj.get("acceptDate"));
				p.set("inoculateDate",obj.get("inoculateDate"));
				p.set("harvestDate",obj.get("harvestDate"));
				p.set("orderId",obj.get("orderId"));
				p.set("sampleType",obj.get("sampleType"));
				karyoShipTaskItemGrid.getStore().add(p);
			}
		});	
		karyoShipTaskItemGrid.startEditing(0,0);
	}
}