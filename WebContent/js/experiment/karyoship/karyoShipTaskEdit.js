$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#karyoShipTask_state").val()==3){
		load("/experiment/karyoship/karyoShipTask/showKaryoShipTaskTempList.action", {
			id : $("#karyoShipTask_id").val()
		}, "#karyoShipTaskTemppage");
		$("#markup").css("width","70%");
	}else{
		$("#showtemplate").css("display","none");
		$("#karyoShipTaskTemppage").css("display","none");
		$("#markup").css("width","100%");
	}
	setTimeout(function() {
		var getGrid=karyoShipTaskTemplateGrid.store;
		if(getGrid.getCount()==0){
			//alert(getGrid.getCount());
			loadTemplate($("#karyoShipTask_template").val());
		}
	}, 2000);
});	
function add() {
	window.location = window.ctx + "/experiment/karyoship/karyoShipTask/editKaryoShipTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/karyoship/karyoShipTask/showKaryoShipTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
//	var type=$("#karyoShipTask_sampleType").val();
	var template=$("#karyoShipTask_template").val();
//	if(type==""||type==null||type==undefined){
//		message("请选择样本类型！");
//		return;
//	}
	if(template==""||template==null||template==undefined){
		message("请选择实验模板！");
		return;
	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("KaryoShipTask", {
					userId : userId,
					userName : userName,
					formId : $("#karyoShipTask_id").val(),
					title : $("#karyoShipTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	var codeList = new Array();
	var flag=true;
	var flag1=true;
	if (karyoShipTaskResultGrid.getAllRecord().length > 0) {
		var selRecord = karyoShipTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			codeList.push(selRecord.getAt(j).get("sampleCode"));
			if(selRecord.getAt(j).get("isrStandard")==""){
				message("结果不能为空！");
				return;
			}
			if(selRecord.getAt(j).get("nextFlowId")==""){
				message("下一步不能为空！");
				return;
			}		
		}
		if(karyoShipTaskResultGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
		
		
		completeTask($("#karyoShipTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});






function save() {
if(checkSubmit()==true){
	    var karyoShipTaskItemDivData = $("#karyoShipTaskItemdiv").data("karyoShipTaskItemGrid");
		document.getElementById('karyoShipTaskItemJson').value = commonGetModifyRecords(karyoShipTaskItemDivData);
	    var karyoShipTaskTemplateDivData = $("#karyoShipTaskTemplatediv").data("karyoShipTaskTemplateGrid");
		document.getElementById('karyoShipTaskTemplateJson').value = commonGetModifyRecords(karyoShipTaskTemplateDivData);
	    var karyoShipTaskReagentDivData = $("#karyoShipTaskReagentdiv").data("karyoShipTaskReagentGrid");
		document.getElementById('karyoShipTaskReagentJson').value = commonGetModifyRecords(karyoShipTaskReagentDivData);
	    var karyoShipTaskCosDivData = $("#karyoShipTaskCosdiv").data("karyoShipTaskCosGrid");
		document.getElementById('karyoShipTaskCosJson').value = commonGetModifyRecords(karyoShipTaskCosDivData);
	    var karyoShipTaskResultDivData = $("#karyoShipTaskResultdiv").data("karyoShipTaskResultGrid");
		document.getElementById('karyoShipTaskResultJson').value = commonGetModifyRecords(karyoShipTaskResultDivData);
//	    var karyoShipTaskTempDivData = $("#karyoShipTaskTempdiv").data("karyoShipTaskTempGrid");
//		document.getElementById('karyoShipTaskTempJson').value = commonGetModifyRecords(karyoShipTaskTempDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/karyoship/karyoShipTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/karyoship/karyoShipTask/copyKaryoShipTask.action?id=' + $("#karyoShipTask_id").val();
}

$("#toolbarbutton_status").click(function(){
	if ($("#karyoShipTask_id").val()){
		commonChangeState("formId=" + $("#karyoShipTask_id").val() + "&tableId=KaryoShipTask");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#karyoShipTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#karyoShipTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.samplePreparation,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/karyoship/karyoShipTask/showKaryoShipTaskItemList.action", {
				id : $("#karyoShipTask_id").val()
			}, "#karyoShipTaskItempage");
load("/experiment/karyoship/karyoShipTask/showKaryoShipTaskTemplateList.action", {
				id : $("#karyoShipTask_id").val()
			}, "#karyoShipTaskTemplatepage");
load("/experiment/karyoship/karyoShipTask/showKaryoShipTaskReagentList.action", {
				id : $("#karyoShipTask_id").val()
			}, "#karyoShipTaskReagentpage");
load("/experiment/karyoship/karyoShipTask/showKaryoShipTaskCosList.action", {
				id : $("#karyoShipTask_id").val()
			}, "#karyoShipTaskCospage");
load("/experiment/karyoship/karyoShipTask/showKaryoShipTaskResultList.action", {
				id : $("#karyoShipTask_id").val()
			}, "#karyoShipTaskResultpage");
load("/experiment/karyoship/karyoShipTask/showKaryoShipTaskTempList.action", {
				id : $("#karyoShipTask_id").val()
			}, "#karyoShipTaskTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
			    	text: '复制'
					});
item.on('click', editCopy);


/**
 * 调用模板
 */
function TemplateFun(){
	var type="doCkaShip";
	var win = Ext.getCmp('TemplateFun');
	if (win) {win.close();}
	var TemplateFun= new Ext.Window({
	id:'TemplateFun',modal:true,title:'Select Template',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 TemplateFun.close(); }  }]  }); 
	 TemplateFun.show(); 
}
function setTemplateFun(rec){
	//把实验模板中的中间产物类型和数量带入
//	var itemGrid=karyoShipTaskItemGrid.store;
//	if(itemGrid.getCount()>0){
//		for(var i=0;i<itemGrid.getCount();i++){
//			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
//			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
//			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
//		}
//	}
	document.getElementById('karyoShipTask_acceptUser').value=rec.get('acceptUser-id');
	document.getElementById('karyoShipTask_acceptUser_name').value=rec.get('acceptUser-name');
	var code=$("#karyoShipTask_template").val();
	if(code==""){
	document.getElementById('karyoShipTask_template').value=rec.get('id');
	document.getElementById('karyoShipTask_template_name').value=rec.get('name');
	var win = Ext.getCmp('TemplateFun');
	if(win){win.close();}
	var id=rec.get('id');
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoShipTaskTemplateGrid.getStore().recordType;
				karyoShipTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("stepNum",obj.code);
					p.set("stepDescribe",obj.name);
					p.set("note",obj.note);
					karyoShipTaskTemplateGrid.getStore().add(p);							
					karyoShipTaskTemplateGrid.startEditing(0, 0);		
				});
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoShipTaskReagentGrid.getStore().recordType;
				karyoShipTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					karyoShipTaskReagentGrid.getStore().add(p);							
					karyoShipTaskReagentGrid.startEditing(0, 0);		
				});
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoShipTaskCosGrid.getStore().recordType;
				karyoShipTaskCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("instrumentCode",obj.code);
					p.set("instrumentName",obj.name);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					karyoShipTaskCosGrid.getStore().add(p);							
					karyoShipTaskCosGrid.startEditing(0, 0);		
				});			
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
	}else{
		if(rec.get('id')==code){
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
	}else{
		var ob1 = karyoShipTaskTemplateGrid.store;
		if (ob1.getCount() > 0) {
			for(var j=0;j<ob1.getCount();j++){
				var oldv = ob1.getAt(j).get("id");
				//根据ID删除
				if(oldv!=null){
					ajax("post", "/experiment/karyoship/karyoShipTask/delKaryoShipTaskTemplateOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null);
				}else{								
					karyoShipTaskTemplateGrid.store.removeAll();
				}
			}
			karyoShipTaskTemplateGrid.store.removeAll();
		}
		var ob2 = karyoShipTaskReagentGrid.store;
		if (ob2.getCount() > 0) {
			for(var j=0;j<ob2.getCount();j++){
				var oldv = ob2.getAt(j).get("id");
				if(oldv!=null){
					ajax("post", "/experiment/karyoship/karyoShipTask/delKaryoShipTaskReagentOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null); 
				}else{
					karyoShipTaskReagentGrid.store.removeAll();
				}
			}
			karyoShipTaskReagentGrid.store.removeAll();
		}
		var ob3 = karyoShipTaskCosGrid.store;
		if (ob3.getCount() > 0) {
			for(var j=0;j<ob3.getCount();j++){
				var oldv = ob3.getAt(j).get("id");
				if(oldv!=null){
					ajax("post", "/experiment/karyoship/karyoShipTask/delKaryoShipTaskCosOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null); 
				}else{
					karyoShipTaskCosGrid.store.removeAll();
				}
			}
			karyoShipTaskCosGrid.store.removeAll();
		}
		//experiment/karyo/karyoShipTask
		document.getElementById('karyoShipTask_template').value=rec.get('id');
		document.getElementById('karyoShipTask_template_name').value=rec.get('name');
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
		var id = rec.get('id');
		ajax("post", "/system/template/template/setTemplateItem.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoShipTaskTemplateGrid.getStore().recordType;
					karyoShipTaskTemplateGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tItem",obj.id);
						p.set("stepNum",obj.code);
						p.set("stepDescribe",obj.name);
						p.set("note",obj.note);
						karyoShipTaskTemplateGrid.getStore().add(p);							
						karyoShipTaskTemplateGrid.startEditing(0, 0);		
					});
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateReagent.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoShipTaskReagentGrid.getStore().recordType;
					karyoShipTaskReagentGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tReagent",obj.id);
						p.set("agentiaCode",obj.code);
						p.set("agentiaName",obj.name);
						p.set("batch",obj.batch);
						p.set("isCheck",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("singleDosage",obj.num);
						p.set("note",obj.note);
						p.set("sn",obj.sn);
						karyoShipTaskReagentGrid.getStore().add(p);							
						karyoShipTaskReagentGrid.startEditing(0, 0);		
					});
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateCos.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoShipTaskCosGrid.getStore().recordType;
					karyoShipTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tCos",obj.id);
						p.set("instrumentCode",obj.code);
						p.set("instrumentName",obj.name);
						p.set("isCheck",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("time",obj.time);
						p.set("note",obj.note);
						karyoShipTaskCosGrid.getStore().add(p);							
						karyoShipTaskCosGrid.startEditing(0, 0);		
					});			
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
		}
	}
}

/**
 * 按条件加载原辅料
 */
function showReagent(){
	//获取全部数据
	var allRcords=karyoShipTaskTemplateGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message("请先保存执行单数据!");
		return;
	}
	//获取选择的数据
	var selectRcords=karyoShipTaskTemplateGrid.getSelectionModel().getSelections();	
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#karyoShipTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/karyoship/karyoShipTask/showkaryoShipTaskReagentList.action", {
			id : $("#karyoShipTask_id").val()
		}, "#karyoShipTaskReagentpage");
	}else if(length1==1){
		karyoShipTaskReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/karyoship/karyoShipTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = karyoShipTaskReagentGrid.getStore().recordType;
				karyoShipTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("sn",obj.sn);
					p.set("reactionDosage",obj.reactionDosage);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("dosage",obj.dosage);
					p.set("tReagent",obj.tReagent);
					p.set("karyoShipTask-id",obj.tId);
					p.set("karyoShipTask-name",obj.tName);
					
					karyoShipTaskReagentGrid.getStore().add(p);							
				});
				karyoShipTaskReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}
	
}

/**
 * 按条件加载设备
 */
function showCos(){
	//获取全部数据
	var allRcords=karyoShipTaskTemplateGrid.store;
	var flag="1";
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag="0";
		}else{
			flag="1";
		}
	}
	if(flag=="0"){
		message("请先保存执行单数据!");
		return;
	}else{
		//获取选择的数据
		var selectRcords=karyoShipTaskTemplateGrid.getSelectionModel().getSelections();
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid=$("#karyoShipTask_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/karyoship/karyoShipTask/showkaryoShipTaskCosList.action", {
				id : $("#karyoShipTask_id").val()
			}, "#karyoShipTaskCospage");
		}else if(length1==1){
			karyoShipTaskCosGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/karyoship/karyoShipTask/setCos.action", {
				tid:tid,code : code
			}, function(data) {
				
				if (data.success) {	
					var ob = karyoShipTaskCosGrid.getStore().recordType;
					karyoShipTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						
						p.set("id",obj.id);
						p.set("instrumentCode",obj.instrumentCode);
						p.set("instrumentName",obj.instrumentName);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("sampleNum",obj.sampleNum);
						p.set("time",obj.time);
						p.set("isCheck",obj.isCheck);
						p.set("itemId",obj.itemId);
						p.set("tCos",obj.tCos);
						p.set("karyoShipTask-id",obj.tId);
						p.set("karyoShipTask-name",obj.tName);
						p.set("note",obj.note);
						karyoShipTaskCosGrid.getStore().add(p);							
					});
					karyoShipTaskCosGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null);
			});		
		}else{
			message("请选择一条数据!");
			return;
		}
	}
}

//查询样本类型
function loadTestDicSampleType(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(a, b) {
						$("#karyoShipTask_sampleType").val(b.get("id"));
						$("#karyoShipTask_sampleType_name").val(b.get("name"));
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = karyoShipTaskResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("isCommit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}
			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("isCommit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "KaryoShipTask",id : $("#karyoShipTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoShipTaskTemplateGrid.getStore().recordType;
				karyoShipTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("stepNum",obj.code);
					p.set("stepDescribe",obj.name);
					p.set("note",obj.note);
					karyoShipTaskTemplateGrid.getStore().add(p);							
					karyoShipTaskTemplateGrid.startEditing(0, 0);		
				});
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoShipTaskReagentGrid.getStore().recordType;
				karyoShipTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					karyoShipTaskReagentGrid.getStore().add(p);							
					karyoShipTaskReagentGrid.startEditing(0, 0);		
				});
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoShipTaskCosGrid.getStore().recordType;
				karyoShipTaskCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("instrumentCode",obj.code);
					p.set("instrumentName",obj.name);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					karyoShipTaskCosGrid.getStore().add(p);							
					karyoShipTaskCosGrid.startEditing(0, 0);		
				});			
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
}