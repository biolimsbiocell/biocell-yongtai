var karyoShipTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'stepDescribe',
		type:"string"
	});
	   fields.push({
		name:'experimentUser-id',
		type:"string"
	});
	   fields.push({
		name:'experimentUser-name',
		type:"string"
	});
	   fields.push({
		name:'startDate',
		type:"string"
	});
	   fields.push({
		name:'endDate',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'karyoShipTask-id',
		type:"string"
	});
	    fields.push({
		name:'karyoShipTask-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	cm.push({
		dataIndex:'testUser-id',
		hidden : true,
		header:'实验员id',
		width:20*6,
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		loadTestUser();
	});
	cm.push({
		dataIndex:'testUser-name',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*6,
		
		editor : testUser
	});
	cm.push({
		dataIndex:'startTime',
		header:biolims.common.startTime,
		width:20*6
	});
	cm.push({
		dataIndex:'endTime',
		header:biolims.common.endTime,
		width:20*6
	});

	cm.push({
		dataIndex:'codes',
		hidden : false,
		header:biolims.common.relateSample,
		width:40*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板主数据步骤ID',
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyoship/karyoShipTask/showKaryoShipTaskTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.templateDetail;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyoship/karyoShipTask/delKaryoShipTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				karyoShipTaskTemplateGrid.getStore().commitChanges();
				karyoShipTaskTemplateGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : biolims.common.printList,
		handler : stampOrder
	});
	karyoShipTaskTemplateGrid=gridEditTable("karyoShipTaskTemplatediv",cols,loadParam,opts);
	$("#karyoShipTaskTemplatediv").data("karyoShipTaskTemplateGrid", karyoShipTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//查询实验员
function loadTestUser(){
	var win = Ext.getCmp('loadTestUser');
	if (win) {win.close();}
	var loadTestUser= new Ext.Window({
	id:'loadTestUser',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/core/user/userSelect.action?flag=loadTestUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 loadTestUser.close(); }  }]  });     loadTestUser.show(); 
}
function setloadTestUser(id,name){
	var selRecords = karyoShipTaskTemplateGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		obj.set('experimentUser-id',id);
		obj.set('experimentUser-name',name);
	});
	var win = Ext.getCmp('loadTestUser');
	if(win){
		win.close();
	}
}




//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=karyoShipTaskTemplateGrid.getSelectionModel();
	//var setNum = bloodSplitReagentGrid.store;
	var selectRecords = karyoShipTaskItemGrid.getSelectionModel();
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			obj.set("startDate",str);
			//将所选样本的数量，放到原辅料样本数量处
//			for(var i=0; i<setNum.getCount();i++){
//				var num = setNum.getAt(i).get("itemId");
//				if(num==obj.get("code")){
//					setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
//				}
//			}
		});
	}else{
		message("请选择步骤编号");
	}
	var selRecord=karyoShipTaskTemplateGrid.getSelectRecord();
	var codes = "";
	$.each(selectRecords.getSelections(), function(i, obj) {
		codes += obj.get("code")+",";
	});
	$.each(selRecord,function(i,obj){
		obj.set("sampleCodes",codes);
	});
}

//获取停止时的时间
function getEndTime(){
	var setRecord=karyoShipTaskItemGrid.store;
	var getIndex = karyoShipTaskTemplateGrid.store;
	var getIndexs = karyoShipTaskTemplateGrid.getSelectionModel().getSelections();
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=karyoShipTaskTemplateGrid.getSelectionModel();
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			if(obj.get("startDate")!=""){
				obj.set("endDate",str);
				var codes = obj.get("sampleCodes");
				var scode = new Array();
				scode = codes.split(",");
				
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(scode[j]==setRecord.getAt(i).get("code")){
							setRecord.getAt(i).set("stepNum",obj.get("code"));
						}
					}
				}
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
			}else{
				message("请先开始实验！");
			}
		});
	}else{
		message("请选择步骤编号");
	}
}

function addSuccess(){
	//选中的item数据
	var getRecord = karyoShipTaskItemGrid.getSelectionModel().getSelections();
	//选中的template数据
	var selectRecord = karyoShipTaskTemplateGrid.getSelectionModel().getSelections();
	//template明细的所有数据
	var getTemplateAll=karyoShipTaskTemplateGrid.store;
	//item明细的所有数据
	var getItemAll=karyoShipTaskItemGrid.store;
	//result的所有数据
	var getResultAll=karyoShipTaskResultGrid.store;
	var isNull=false;
	var isNull1=false;
	var isNull2=false;
	if(getItemAll.getCount()>0){
//		for(var h=0;h<getTemplateAll.getCount();h++){
//			var tid= getItemAll.getAt(h).get("dicSampleType-id");
//			if(tid == null || tid == ""){
//				isNull2 = true;
//				message("请选择中间产物类型！");
//				break;					
//			}
//		}
//		for(var i=0;i<getItemAll.getCount();i++){
//			var num=getItemAll.getAt(i).get("productNum");
//			if(num==null || num=="" || num==0){
//				isNull1=true;
//				message("请填写中间产物数量！");
//				break;
//			}
//		}
//		for(var h=0;h<getTemplateAll.getCount();h++){
//			var nulls = getTemplateAll.getAt(h).get("endDate");
//			if(nulls==null || nulls == ""){
//				isNull = true;
//				message("有未做实验的步骤！");
//				break;					
//			}
//		}
		if(isNull==false){
			if(selectRecord.length==0){
				//如果没有选中实验步骤，默认所有明细都生成结果
				var isCF=false;
				if(getResultAll.getCount()>0){
					for(var i=0; i<getItemAll.getCount(); i++){
						for(var j=0;j<getResultAll.getCount();j++){
							var itemCode = getItemAll.getAt(i).get("code");
							var infoCode = getResultAll.getAt(j).get("code");
							if(itemCode == infoCode){
								isCF = true;
								message("数据重复，请删除结果，重新生成！");
								break;					
							}
						}
					}
					if(isCF==false){
						toInfoData(getItemAll);
					}
				}else{
					toInfoData(getItemAll);
				}
			}else if(selectRecord.length==1){
				$.each(selectRecord, function(i, obj) {
					var isRepeat = true;
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i1=0; i1<scode.length; i1++){
						for(var j1=0;j1<getResultAll.getCount();j1++){
							var getv = scode[i1];
							var setv = getResultAll.getAt(j1).get("code");
							if(getv == setv){
								isRepeat = false;
								message("有重复的数据，请重新选择！");
								break;					
							}
						}
					}
					if(isRepeat){
						for(var i=0; i<scode.length; i++){
							for(var j=0; j<getItemAll.getCount(); j++){
								if(scode[i]==getItemAll.getAt(j).get("code")){
									//for(var k=1;k<=getItemAll.getAt(j).get("productNum");k++){
										var ob = karyoShipTaskResultGrid.getStore().recordType;
										karyoShipTaskResultGrid.stopEditing();
										var p = new ob({});
										var date = new Date();
										p.isNew = true;
										p.set("tempId",getItemAll.getAt(j).get("tempId"));
										p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
										p.set("code",getItemAll.getAt(j).get("code"));
										p.set("productId",getItemAll.getAt(j).get("productId"));
										p.set("productName",getItemAll.getAt(j).get("productName"));
										p.set("dicSampleType-id",getItemAll.getAt(j).get("dicSampleType-id"));
										p.set("dicSampleType-name",getItemAll.getAt(j).get("dicSampleType-name"));
										p.set("orderId",getItemAll.getAt(j).get("orderId"));
										p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
										p.set("experimentCode",getItemAll.getAt(j).get("experimentCode"));
										p.set("isrStandard","1");
										//p.set("harvestDate",date);
										p.set("shipDate",getItemAll.getAt(j).get("shipDate"));
										p.set("harvestDate",getItemAll.getAt(j).get("harvestDate"));
										p.set("inoculateDate",getItemAll.getAt(j).get("inoculateDate"));
										p.set("acceptDate",getItemAll.getAt(j).get("acceptDate"));
										p.set("harversUser",$("#user").val());
										ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
											model : "KaryoShipTask",productId:getItemAll.getAt(j).get("productId")
										}, function(data) {
											p.set("nextFlowId",data.dnextId);
											p.set("nextFlow",data.dnextName);
										}, null);
										message("生成结果成功！");
										karyoShipTaskResultGrid.getStore().add(p);
										karyoShipTaskResultGrid.startEditing(0,0);
									//}
								}
							}
						}
					}
				});
			}else if(selectRecord.length>1){
				message("请不要勾选多个步骤！");
				return;
			}
		}
	}else{
		message("请先添加实验样本！");
		return;
	}
}
//向Info页面传值
function toInfoData(grid){
	for(var i=0;i<grid.getCount();i++){
		//var productNum=grid.getAt(i).get("productNum");
		//for(var k=1;k<=productNum;k++){
			var ob = karyoShipTaskResultGrid.getStore().recordType;
			karyoShipTaskResultGrid.stopEditing();
			var p = new ob({});
			var date = new Date();
			p.isNew = true;
			p.set("tempId",grid.getAt(i).get("tempId"));
			p.set("sampleCode",grid.getAt(i).get("sampleCode"));
			p.set("code",grid.getAt(i).get("code"));
			p.set("productId",grid.getAt(i).get("productId"));
			p.set("productName",grid.getAt(i).get("productName"));
			p.set("dicSampleType-id",grid.getAt(i).get("dicSampleType-id"));
			p.set("dicSampleType-name",grid.getAt(i).get("dicSampleType-name"));
			p.set("orderId",grid.getAt(i).get("orderId"));
			p.set("sampleType",grid.getAt(i).get("sampleType"));
			p.set("experimentCode",grid.getAt(i).get("experimentCode"));
			p.set("isrStandard","1");
			//p.set("harvestDate",date);
			p.set("harversUser",$("#user").val());
			p.set("shipDate",grid.getAt(i).get("shipDate"));
			p.set("harvestDate",grid.getAt(i).get("harvestDate"));
			p.set("inoculateDate",grid.getAt(i).get("inoculateDate"));
			p.set("acceptDate",grid.getAt(i).get("acceptDate"));
			ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
				model : "KaryoShipTask",productId:grid.getAt(i).get("productId")
			}, function(data) {
				p.set("nextFlowId",data.dnextId);
				p.set("nextFlow",data.dnextName);
			}, null);
			message("生成结果成功！");
			karyoShipTaskResultGrid.getStore().add(p);
			karyoShipTaskResultGrid.startEditing(0,0);
		//}
	}
}


//打印执行单
function stampOrder(){
	var id=$("#karyoShipTask_template").val();
	if(id==""){
		message("请先选择模板!");
		return;
	}else{
		var url = '__report=KaryoShipTask.rptdesign&id=' + $("#karyoShipTask_id").val();
		commonPrint(url);
	}
}