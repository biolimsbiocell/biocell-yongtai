var karyoShipTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'experimentCode',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'zjCode',
		type:"string"
	});
	   fields.push({
		name:'harvestDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'shipDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'inoculateDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'harversUser',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'isrStandard',
		type:"string"
	});
	   fields.push({
		name:'isCommit',
		type:"string"
	});
	    fields.push({
		name:'karyoShipTask-id',
		type:"string"
	});
	    fields.push({
		name:'karyoShipTask-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
	    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'tempId',
		type:"string"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
		name:'sampleType',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'zjCode',
		hidden : true,
		header:biolims.common.zJSample,
		sortable:true,
		width:25*6
	});

	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:25*6
	});
//	cm.push({
//		dataIndex:'harvestDate',
//		hidden : false,
//		header:'制片时间',
//		width:20*6,
//		sortable:true,
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
//	});
	cm.push({
		dataIndex:'harversUser',
		hidden : false,
		header:biolims.common.preparater,
		width:20*6,
		sortable:true,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'shipDate',
		hidden : false,
		header:biolims.common.flakingDate,
		width:25*6,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'harvestDate',
		hidden : false,
		header:biolims.common.harvesTime,
		width:25*6,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'inoculateDate',
		hidden : true,
		header:biolims.user.inoculationTime,
		width:25*6,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:biolims.common.acceptDate,
		width:20*6,
		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'experimentCode',
		hidden : true,
		header:biolims.common.expCode,
		width:20*6,
		sortable:true,
		sortable:true
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'左侧表ID',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'原任务单编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:'样本类型',
		width:20*10
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		hidden : true,
		header:'样本类型',
		width:20*10
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : '合格'
			}, {
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isrStandard',
		hidden : false,
		header:biolims.user.result+'<font color="red" size="4">*</font>',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden : true,
		header:biolims.common.nextFlowId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow+'<font color="red" size="4">*</font>',
		width:15*10,
		sortable:true,
		editor : nextFlowCob
	});
	var submit = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [{
				id : '1',
				name : biolims.common.yes
			}, {
				id : '0',
				name :biolims.common.no
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'isCommit',
		hidden : false,
		header:biolims.common.Submitted+'<font color="red" size="4">*</font>',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(submit),
//		editor: submit
	});
	cm.push({
		dataIndex:'karyoShipTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'karyoShipTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyoship/karyoShipTask/showKaryoShipTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit=500;
	var opts={};
	opts.title=biolims.common.state;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	if($("#karyoShipTask_state").val()!="1"){
    opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyoship/karyoShipTask/delKaryoShipTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				karyoShipTaskResultGrid.getStore().commitChanges();
				karyoShipTaskResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.batchPreparationTime,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_get_div"), "批量收获时间", null, {
				"确定" : function() {
					var records = karyoShipTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var date = $("#hdate").val()+" 00:00:00";
						date = date.replace(/-/g,"/");
						var rdate = new Date(date);
						karyoShipTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("harvestDate", rdate);
						});
						karyoShipTaskResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchPreparationer,
		handler:shUserFun
	});
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = karyoShipTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						karyoShipTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isrStandard", result);
						});
						karyoShipTaskResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : function() {
			var records = karyoShipTaskResultGrid.getSelectRecord();
			if(records.length>0){
					loadTestNextFlowCob();
				
			}else{
				message("请选择数据!");
			}
		}
	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = karyoShipTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						karyoShipTaskResultGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("isCommit", submit);
//						});
//						karyoShipTaskResultGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
//	opts.tbar.push({
//		text : '填加明细',
//		handler : null
//	});
	opts.tbar.push({
		text :biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		iconCls : 'save',
		text :biolims.common.save,
		handler : saveInfo
	});
	}
	karyoShipTaskResultGrid=gridEditTable("karyoShipTaskResultdiv",cols,loadParam,opts);
	$("#karyoShipTaskResultdiv").data("karyoShipTaskResultGrid", karyoShipTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});


//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(karyoShipTaskResultGrid);
	var id=$("#karyoShipTask_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/karyoship/karyoShipTask/saveKaryoShipTaskResult.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					karyoShipTaskResultGrid.getStore().commitChanges();
					karyoShipTaskResultGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = karyoShipTaskResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	//alert(productId);
	var options = {};
	options.width = 500;
	options.height = 500;
	loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=KaryoShipTask&productId="+productId, {
		"确定" : function() {
			var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = karyoShipTaskResultGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(records, function(i, obj) {
					$.each(selectRecord, function(a, b) {
						obj.set("nextFlowId", b.get("id"));
						obj.set("nextFlow", b.get("name"));
					});
				});
			}else{
				message("请选择您要选择的数据");
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}

function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = karyoShipTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}
	

//选择制片人
function shUserFun(){
	var win = Ext.getCmp('shUserFun');
	if (win) {win.close();}
	var shUserFun= new Ext.Window({
	id:'shUserFun',modal:true,title:'选择收获人',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/core/user/userSelect.action?flag=UserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 shUserFun.close(); }  }]  });     shUserFun.show(); }
function setUserFun(id,name){
	var xzGrid=karyoShipTaskResultGrid.getSelectRecord();
	$.each(xzGrid, function(i, obj) {
		obj.set("harversUser", name);
	});
	var win = Ext.getCmp('shUserFun');
	if(win){win.close();}
	}



//提交样本
function submitSample(){
	var id=$("#karyoShipTask_id").val();  
	if(karyoShipTaskResultGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	var record = karyoShipTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(!record[i].get("isCommit")){
				flg=true;
			}
			if(record[i].get("isrStandard")==""){
				message("结果不能为空！");
				return;
			}
			if(record[i].get("nextFlowId")==""){
				message("下一步不能为空！");
				return;
			}
		}
	}else{
		var grid=karyoShipTaskResultGrid.store;
		for(var i=0;i<grid.getCount();i++){
			if(grid.getAt(i).get("isCommit")==""){
				flg=true;
			}
			if(grid.getAt(i).get("isrStandard")==""){
				message("结果不能为空！");
				return;
			}
			if(grid.getAt(i).get("nextFlowId")==""){
				message("下一步不能为空！");
				return;
			}
		}
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		
		
		var records = [];
		
		
		
		for ( var i = 0; i < record.length; i++) {
			records.push(record[i].get("id"));
		}
		
		ajax("post", "/experiment/karyoship/karyoShipTask/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				karyoShipTaskResultGrid.getStore().commitChanges();
				karyoShipTaskResultGrid.getStore().reload();
				message("提交成功！");
			} else {
				loadMarsk.hide();
				message("提交失败！");
			}
		}, null);
	}else{
		message("没有需要提交的样本！");
	}
}
	

	


