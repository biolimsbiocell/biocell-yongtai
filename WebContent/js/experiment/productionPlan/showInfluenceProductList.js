var DicTypeTable;
$(function() {
	var colOpts = [];
//	colOpts.push({
//		"data" : "id",
//		"title" : biolims.common.id,
//		"visible":false,
//	});
	colOpts.push({
		"data" : "name",
		"title" : "描述",
	});
	colOpts.push({
		"data" : "orderCode",
		"title" : "产品",
	});
	colOpts.push({
		"data" : "note",
		"title" : "备注",
	});
	colOpts.push({
		"data" : "deviationHandlingReport-id",
		"title" : "偏差主表ID",
	});
	colOpts.push({
		"data" : "deviationHandlingReport-name",
		"title" : "偏差主表描述",
	});
	var tbarOpts = [];
	tbarOpts.push({
		text: "查看详情",
		action: function() {
			showMainInfo();
		}
	});
	var addDicTypeOptions = table(false, null,
			'/experiment/productionPlan/productionPlan/showInfluenceProductTable.action?flag=' + $("#id").val(),
			colOpts, tbarOpts)//传值
		DicTypeTable = renderData($("#addDicTypeTable"), addDicTypeOptions);
	$("#addDicTypeTable").on(
			'init.dt',
			function(e, settings) {
				// 清除操作按钮,弹窗搜索就隐藏下一行
				//$("#addDicTypeTable_wrapper .dt-buttons").empty();
				$('#addDicTypeTable_wrapper').css({
					"padding": "0 16px"
				});
			var trs = $("#addDicTypeTable tbody tr");
			DicTypeTable.ajax.reload();
			DicTypeTable.on('draw', function() {
				trs = $("#addDicTypeTable tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
			});
})
function showMainInfo(){
	var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(3).text();
	window.open(window.ctx +"/deviation/deviationHandlingReport/editDeviationHandlingReport.action?id="+id)
}