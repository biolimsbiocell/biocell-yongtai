var producerTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	    fields.push({
		name:'reciveDate',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'indexa',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-id',
		type:"string"
	});
	    fields.push({
		name:'acceptUser-name',
		type:"string"
	});
	    fields.push({
		name:'maxNum',
		type:"string"
	});
	    fields.push({
		name:'qcNum',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:20*10,
		sortable:true
	});

		cm.push({
		dataIndex:'createUser-id',
		header:'下达人ID',
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'下达人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'下达时间',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'template-id',
		header:'模板ID',
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:'模板',
		width:20*10,
		sortable:true
		});

	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*10,
		hidden:true,
		sortable:true
	});
		cm.push({
		dataIndex:'acceptUser-id',
		header:'实验组ID',
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'acceptUser-name',
		header:'实验组',
		width:20*10,
		sortable:true
		});

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/producer/producerTask/showDialogProducerTaskListJson.action";
	var opts={};
	opts.title="制片上机";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setProducerTaskFun(rec);
		//setproducerTaskFun(rec);
	};
	producerTaskDialogGrid=gridTable("show_dialog_producerTask_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(producerTaskDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
