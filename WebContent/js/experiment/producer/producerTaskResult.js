var producerTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'jzshCode',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'jkTaskId',
		type:"string"
	});
	   fields.push({
		name:'indexa',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'unit',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'reason',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'inspectDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'idCard',
		type:"string"
	});
	   fields.push({
		name:'phone',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'sequenceFun',
		type:"string"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'method',
		type:"string"
	});
	   fields.push({
		name:'isExecute',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'producerTask-id',
		type:"string"
	});
	    fields.push({
		name:'producerTask-name',
		type:"string"
	});
	   fields.push({
		name:'rowCode',
		type:"string"
	});
	   fields.push({
		name:'colCode',
		type:"string"
	});
	   fields.push({
		name:'counts',
		type:"string"
	});
	   fields.push({
		name:'contractId',
		type:"string"
	});
	   fields.push({
		name:'projectId',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'classify',
		type:"string"
	});
	   fields.push({
		name:'dataType',
		type:"string"
	});
	   fields.push({
		name:'dataNum',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'tempId',
		type:"string"
	});
	   fields.push({
		name:'slideCode',
		type:"string"
	});
	   fields.push({
		name:'resultDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		sortable:true,
		header:biolims.common.sampleCode,
		width:25*6
	});
	cm.push({
		dataIndex:'jzshCode',
		hidden : true,
		sortable:true,
		header:'接种收获样本编号',
		width:25*6
	});
	cm.push({
		dataIndex:'slideCode',
		hidden : false,
		header:biolims.common.slideCode,
		sortable:true,
		width:25*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'jkTaskId',
		hidden : true,
		header:'建库任务单编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'indexa',
		hidden : true,
		header:'index',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:'单位',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'resultDate',
		hidden : false,
		header:biolims.common.flakingDate,
		width:20*6,
		
		renderer: formatDate
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:biolims.common.acceptDate,
		width:20*6,
		
		renderer: formatDate
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.user.result+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow+'<font color="red" size="4">*</font>',
		width:15*10,
		sortable:true,
		hidden:false,
		editor : nextFlowCob
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.Submitted+'<font color="red" size="4">*</font>',
		width:20*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样时间',
		width:20*6,
		
		renderer: formatDate
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证',
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:20*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告时间',
		width:20*6,
		
		renderer: formatDate,
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
	});
	cm.push({
		dataIndex:'method',
		hidden : true,
		header:'处理意见',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'isExecute',
		hidden : true,
		header:'确定执行',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'producerTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10
	});
	cm.push({
		dataIndex:'producerTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:20*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:20*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:20*6
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同id',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'区分临床还是科技服务 0 临床 1 科技服务',
		width:40*6
	});
	cm.push({
		dataIndex:'dataType',
		hidden : true,
		header:'数据类型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'dataNum',
		hidden : true,
		header:'数据量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/producer/producerTask/showProducerTaskResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.scansResults;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	if($("#producerTask_state").val()!="1" 
		|| $("#producerTask_state").val()!="2"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/producer/producerTask/delProducerTaskResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				producerTaskResultGrid.getStore().commitChanges();
				producerTaskResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.batchSlideNo,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_slideCode_div"),biolims.common.batchSlideNo, null, {
				"确定" : function() {
					var records = producerTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var slideCode = $("#slideCode").val();
						producerTaskResultGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("slideCode", slideCode);
						});
						producerTaskResultGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"),biolims.common.batchResult, null, {
				"确定" : function() {
					var records = producerTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						producerTaskResultGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						producerTaskResultGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : biolims.common.exportList,
		handler : exportexcel
	});
//	opts.tbar.push({
//		text : "批量下一步",
//		handler : function() {
//			var records = producerTaskResultGrid.getSelectRecord();
//			if(records.length>0){
//				if(records.length>2){
//					var productId = new Array();
//					$.each(records, function(j, k) {
//						productId[j]=k.get("productId");
//					});
//					for(var i=0;i<records.length;i++){
//						if(i!=0&&productId[i]!=productId[i-1]){
//							message("检测项目不同！");
//							return;
//						}
//					}
//					loadTestNextFlowCob();
//				}else{
//					loadTestNextFlowCob();
//				}
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = producerTaskResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						producerTaskResultGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						producerTaskResultGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	}
	producerTaskResultGrid=gridEditTable("producerTaskResultdiv",cols,loadParam,opts);
	$("#producerTaskResultdiv").data("producerTaskResultGrid", producerTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(producerTaskResultGrid);
	var id=$("#producerTask_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/producer/producerTask/saveProducerTaskResult.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					producerTaskResultGrid.getStore().commitChanges();
					producerTaskResultGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);
		}
	}else{
		message(biolims.common.noData2Save);
	}
	  }else{
		  message(biolims.storage.infoChange);
	}
}

function exportexcel() {
	producerTaskResultGrid.title = biolims.common.exportList;
	var vExportContent = producerTaskResultGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = producerTaskResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, biolims.common.selectNextFlow,  "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=ProducerTask&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = producerTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = producerTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}
	


//提交样本
function submitSample(){
	var id=$("#producerTask_id").val();  
	if(producerTaskResultGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var record = producerTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("submit")){
			flg=true;
		}
		if(record[i].get("result")==""){
			message(biolims.common.resultsIsEmpty);
			return;
		}
	

	}
	var grid=producerTaskResultGrid.store;
	for(var i=0;i<grid.getCount();i++){
		if(grid.getAt(i).get("submit")==""){
			flg=true;
		}
		if(grid.getAt(i).get("result")==""){
			message(biolims.common.resultsIsEmpty);
			return;
		}
	
	}
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : biolims.common.beingProcessed,
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();
		
		
		var records = [];
		
		
		
		for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
		}
		
		ajax("post", "/experiment/producer/producerTask/submitSample.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				producerTaskResultGrid.getStore().commitChanges();
				producerTaskResultGrid.getStore().reload();
				message(biolims.common.submitSuccess);
			} else {
				loadMarsk.hide();
				message(biolims.common.submitFail);
			}
		}, null);
	}else{
		message(biolims.common.noData2Submit);
	}
}

