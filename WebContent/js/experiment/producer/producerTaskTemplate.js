var producerTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'producerTask-id',
		type:"string"
	});
	    fields.push({
		name:'producerTask-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤编id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		selectreciveUserFun();
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*10,
		editor : testUser
	});
	
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:biolims.common.startTime,
		width:20*6
		
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:biolims.common.endTime,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:biolims.common.relateSample,
		width:40*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'producerTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'producerTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/producer/producerTask/showProducerTaskTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.commonTemplate;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/producer/producerTask/delProducerTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				producerTaskTemplateGrid.getStore().commitChanges();
				producerTaskTemplateGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
			text : biolims.common.chooseTester,
				handler : selectreciveUserFun
		});
	
	
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : biolims.common.printList,
		handler : stampOrder
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
	producerTaskTemplateGrid=gridEditTable("producerTaskTemplatediv",cols,loadParam,opts);
	$("#producerTaskTemplatediv").data("producerTaskTemplateGrid", producerTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectreciveUserFun(){
	var win = Ext.getCmp('selectreciveUser');
	if (win) {win.close();}
	var selectreciveUser= new Ext.Window({
	id:'selectreciveUser',modal:true,title:biolims.common.chooseTester,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectreciveUser.close(); }  }]  }) ;  
    selectreciveUser.show(); }
	function setreciveUser(id,name){
		var gridGrid = $("#producerTaskTemplatediv").data("producerTaskTemplateGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('reciveUser-id',id);
			obj.set('reciveUser-name',name);
		});
		var win = Ext.getCmp('selectreciveUser');
		if(win){
			win.close();
		}
	}

	//打印执行单
	function stampOrder(){
		var id=$("#producerTask_template").val();
		if(id==""){
			message(biolims.common.pleaseSelectTemplate);
			return;
		}else{
			var url = '__report=ProducerTask.rptdesign&id=' + $("#producerTask_id").val();
			commonPrint(url);}
	}
	//生成结果明细
	function addSuccess(){	
		/*var getRecord = producerTaskItemGrid.getSelectionModel().getSelections();
		var selectRecord = producerTaskTemplateGrid.getSelectionModel().getSelections();
		var selRecord = producerTaskResultGrid.store;*/
		var getRecord = producerTaskItemGrid.getSelectionModel().getSelections();
		//选中的template数据
		var selectRecord = producerTaskTemplateGrid.getSelectionModel().getSelections();
		//template明细的所有数据
		var getTemplateAll=producerTaskTemplateGrid.store;
		//item明细的所有数据
		var getItemAll=producerTaskItemGrid.store;
		//result的所有数据
		var getResultAll=producerTaskResultGrid.store;
		var isNull=false;
		if(getItemAll.getCount()>0){
//			for(var h=0;h<getTemplateAll.getCount();h++){
//				var nulls = getTemplateAll.getAt(h).get("endTime");
//				if(nulls==null || nulls == ""){
//					isNull = true;
//					message("有未做实验的步骤！");
//					break;					
//				}
//			}
			if(isNull==false){
				if(selectRecord.length==0){
					//如果没有选中实验步骤，默认所有明细都生成结果
					var isCF=false;
					if(getResultAll.getCount()>0){
						for(var i=0; i<getItemAll.getCount(); i++){
							for(var j=0;j<getResultAll.getCount();j++){
								var itemCode = getItemAll.getAt(i).get("code");
								var infoCode = getResultAll.getAt(j).get("code");
								if(itemCode == infoCode){
									isCF = true;
									message(biolims.common.dataRepeat);
									break;					
								}
							}
						}
						if(isCF==false){
							toInfoData(getItemAll);
						}
					}else{
						toInfoData(getItemAll);
					}
				}else if(selectRecord.length==1){
					$.each(selectRecord, function(i, obj) {
						var isRepeat = true;
						var codes = obj.get("sampleCodes");
						var scode = new Array();
						scode = codes.split(",");
						for(var i1=0; i1<scode.length; i1++){
							for(var j1=0;j1<getResultAll.getCount();j1++){
								var getv = scode[i1];
								var setv = getResultAll.getAt(j1).get("code");
								if(getv == setv){
									isRepeat = false;
									message(biolims.common.dataRepeat);
									break;					
								}
							}
						}
						if(isRepeat){
							for(var i=0; i<scode.length; i++){
								for(var j=0; j<getItemAll.getCount(); j++){
									if(scode[i]==getItemAll.getAt(j).get("code")){
										var ob = producerTaskResultGrid.getStore().recordType;
										producerTaskResultGrid.stopEditing();
										var p = new ob({});
										p.isNew = true;
										p.set("tempId",getItemAll.getAt(j).get("tempId"));
										p.set("name",getItemAll.getAt(j).get("name"));
										p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
										p.set("note",getItemAll.getAt(j).get("note"));
										p.set("code",getItemAll.getAt(j).get("code"));
										p.set("patientName",getItemAll.getAt(j).get("patientName"));
										p.set("productId",getItemAll.getAt(j).get("productId"));
										p.set("productName",getItemAll.getAt(j).get("productName"));
										p.set("inspectDate",getItemAll.getAt(j).get("inspectDate"));
										p.set("acceptDate",getItemAll.getAt(j).get("acceptDate"));
										p.set("resultDate",getItemAll.getAt(j).get("resultDate"));
										p.set("idCard",getItemAll.getAt(j).get("idCard"));
										p.set("phone",getItemAll.getAt(j).get("phone"));
										p.set("orderId",getItemAll.getAt(j).get("orderId"));
										p.set("sequenceFun",getItemAll.getAt(j).get("sequenceFun"));
										p.set("reportDate",getItemAll.getAt(j).get("reportDate"));
										p.set("unit",getItemAll.getAt(j).get("unit"));
										p.set("rowCode",getItemAll.getAt(j).get("rowCode"));
										p.set("colCode",getItemAll.getAt(j).get("colCode"));
										p.set("counts",getItemAll.getAt(j).get("counts"));
										p.set("contractId",getItemAll.getAt(j).get("contractId"));
										p.set("projectId",getItemAll.getAt(j).get("projectId"));
										p.set("orderType",getItemAll.getAt(j).get("orderType"));
										p.set("jkTaskId",getItemAll.getAt(j).get("jkTaskId"));
										p.set("classify",getItemAll.getAt(j).get("classify"));
										p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
										p.set("result","1");
										if((getItemAll.getAt(j).get("productId"))=="A0012"){
											p.set("slideCode","S1/S2");
										}else if((getItemAll.getAt(j).get("productId"))=="A0013"){
											p.set("slideCode","P1/P2/S1/S2");
										}
										ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
											model : "ProducerTask",productId:getItemAll.getAt(j).get("productId")
										}, function(data) {
											p.set("nextFlowId",data.dnextId);
											p.set("nextFlow",data.dnextName);
										}, null);
										message(biolims.common.generateResultsSuccess);
										producerTaskResultGrid.getStore().add(p);
										producerTaskResultGrid.startEditing(0,0);
									}
								}
							}
						}
					});
				}else if(selectRecord.length>1){
					message(biolims.common.pleaseNotBuz);
					return;
				}
			}
		}else{
			message(biolims.common.addTaskSample);
			return;
		}
	}

	//获取开始时的时间
	function getStartTime(){
		var d = new Date();
		var str1 = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
		$("#producerTask_reciveDate").val(str1);
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=producerTaskTemplateGrid.getSelectionModel();
		var setNum = producerTaskReagentGrid.store;
		var selectRecords=producerTaskItemGrid.getSelectionModel();
		if(selectRecords.getSelections().length > 0){
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					obj.set("startTime",str);
					//将所选样本的数量，放到原辅料样本数量处
					for(var i=0; i<setNum.getCount();i++){
						var num = setNum.getAt(i).get("itemId");
						if(num==obj.get("code")){
							setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
						}
					}
				});
			}else{
				message(biolims.common.pleaseSelect);
			}
			
			//将所选的样本，放到关联样本
			var selRecord=producerTaskTemplateGrid.getSelectRecord();
			var codes = "";
			$.each(selectRecords.getSelections(), function(i, obj) {
				codes += obj.get("code")+",";
			});
			$.each(selRecord, function(i, obj) {
				obj.set("sampleCodes", codes);
			});
		}else{
			message(biolims.common.pleaseSelectSamples);
			return;
		}
	}
	//获取停止时的时间
	function getEndTime(){
			var setRecord=producerTaskItemGrid.store;
			var d = new Date();
			var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
			var selectRecord=producerTaskTemplateGrid.getSelectionModel();
			var getIndex = producerTaskTemplateGrid.store;
			var getIndexs = producerTaskTemplateGrid.getSelectionModel().getSelections();
			if (selectRecord.getSelections().length > 0) {
				$.each(selectRecord.getSelections(), function(i, obj) {
					if(obj.get("startTime")!=undefined && obj.get("startTime")!=""){
						obj.set("endTime",str);	
						//将步骤编号，赋值到明细表
						var codes = obj.get("sampleCodes");
						var scode = new Array();
						scode = codes.split(",");
						for(var i=0; i<setRecord.getCount(); i++){
							for(var j=0; j<scode.length; j++){
								if(scode[j]==setRecord.getAt(i).get("code")){
									setRecord.getAt(i).set("stepNum",obj.get("code"));
								}
							}
						}
						//将当前行的关联样本传到下一行
						getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
					}else{
						message(biolims.common.pleaseStartTask);
						return;
					}
				});
			}
	}
	//向Info页面传值
	function toInfoData(grid){
		for(var i=0;i<grid.getCount();i++){
			var ob = producerTaskResultGrid.getStore().recordType;
			producerTaskResultGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("tempId",grid.getAt(i).get("tempId"));
			p.set("name",grid.getAt(i).get("name"));
			p.set("sampleCode",grid.getAt(i).get("sampleCode"));
			p.set("note",grid.getAt(i).get("note"));
			p.set("code",grid.getAt(i).get("code"));
			p.set("patientName",grid.getAt(i).get("patientName"));
			p.set("productId",grid.getAt(i).get("productId"));
			p.set("productName",grid.getAt(i).get("productName"));
			p.set("inspectDate",grid.getAt(i).get("inspectDate"));
			p.set("acceptDate",grid.getAt(i).get("acceptDate"));
			p.set("resultDate",grid.getAt(i).get("resultDate"));
			p.set("idCard",grid.getAt(i).get("idCard"));
			p.set("phone",grid.getAt(i).get("phone"));
			p.set("orderId",grid.getAt(i).get("orderId"));
			p.set("sequenceFun",grid.getAt(i).get("sequenceFun"));
			p.set("reportDate",grid.getAt(i).get("reportDate"));
			p.set("unit",grid.getAt(i).get("unit"));
			p.set("rowCode",grid.getAt(i).get("rowCode"));
			p.set("colCode",grid.getAt(i).get("colCode"));
			p.set("counts",grid.getAt(i).get("counts"));
			p.set("contractId",grid.getAt(i).get("contractId"));
			p.set("projectId",grid.getAt(i).get("projectId"));
			p.set("orderType",grid.getAt(i).get("orderType"));
			p.set("jkTaskId",grid.getAt(i).get("jkTaskId"));
			p.set("classify",grid.getAt(i).get("classify"));
			p.set("sampleType",grid.getAt(i).get("sampleType"));
			p.set("result","1");
			if((grid.getAt(i).get("productId"))=="A0012"){
				p.set("slideCode","S1/S2");
			}else if((grid.getAt(i).get("productId"))=="A0013"){
				p.set("slideCode","P1/P2/S1/S2");
			}
			ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
				model : "ProducerTask",productId:grid.getAt(i).get("productId")
			}, function(data) {
				p.set("nextFlowId",data.dnextId);
				p.set("nextFlow",data.dnextName);
			}, null);
			message(biolims.common.generateResultsSuccess);
			producerTaskResultGrid.getStore().add(p);
			producerTaskResultGrid.startEditing(0,0);
		}
	}	
