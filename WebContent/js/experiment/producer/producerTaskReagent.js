var producerTaskReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'count',
		type:"string"
	});
	   fields.push({
		name:'oneNum',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'producerTask-id',
		type:"string"
	});
	    fields.push({
		name:'producerTask-name',
		type:"string"
	});
	   fields.push({
		name:'itemId',
		type:"string"
	});
	   fields.push({
		name:'tReagent',
		type:"string"
	});
	   fields.push({
			name:'sn',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.reagentNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.reagentName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:biolims.common.batch,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'count',
		hidden : true,
		header:'数量',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'oneNum',
		hidden : false,
		header:biolims.common.singleDose,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.dose,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'sn',
		hidden : false,
		header:'sn',
		width:20*6
	});
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '否' ], [ '1', '是' ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isDetecting,
		width:20*6,
		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'producerTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'producerTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:'关联步骤id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tReagent',
		hidden : true,
		header:'模板原辅料id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/producer/producerTask/showProducerTaskReagentListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/producer/producerTask/delProducerTaskReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				producerTaskReagentGrid.getStore().commitChanges();
				producerTaskReagentGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	producerTaskReagentGrid=gridEditTable("producerTaskReagentdiv",cols,loadParam,opts);
	$("#producerTaskReagentdiv").data("producerTaskReagentGrid", producerTaskReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
