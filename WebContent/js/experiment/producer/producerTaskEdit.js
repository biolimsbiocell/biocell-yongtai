$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#producerTask_state").val();
	if(id=="3"){
		load("/experiment/producer/producerTask/showProducerTaskTempList.action", null, "#producerTaskTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#producerTaskTempPage").remove();
	}
	setTimeout(function() {
		var getGrid=producerTaskTemplateGrid.store;
		if(getGrid.getCount()==0){
			//alert(getGrid.getCount());
			loadTemplate($("#producerTask_template").val());
		}
	}, 2000);
});	
function add() {
	window.location = window.ctx + "/experiment/producer/producerTask/editProducerTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/producer/producerTask/showProducerTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var t=$("#producerTask_template").val();
	var a=$("#producerTask_acceptUser").val();
	if(t==""||t==null||t==undefined){
		message(biolims.common.selectTaskModel);
		return;
	}
	if(a==""||a==null||a==undefined){
		message(biolims.common.pleaseSelectGroup);
		return;
	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("ProducerTask", {
					userId : userId,
					userName : userName,
					formId : $("#producerTask_id").val(),
					title : $("#producerTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	
	var codeList = new Array();
	if (producerTaskResultGrid.getAllRecord().length > 0) {
		var selRecord = producerTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			codeList.push(selRecord.getAt(j).get("sampleCode"));
		}
		if(producerTaskResultGrid.getModifyRecord().length > 0){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
		
		
		completeTask($("#producerTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});






function save() {
if(checkSubmit()==true){
	    var producerTaskItemDivData = $("#producerTaskItemdiv").data("producerTaskItemGrid");
		document.getElementById('producerTaskItemJson').value = commonGetModifyRecords(producerTaskItemDivData);
	    var producerTaskTemplateDivData = $("#producerTaskTemplatediv").data("producerTaskTemplateGrid");
		document.getElementById('producerTaskTemplateJson').value = commonGetModifyRecords(producerTaskTemplateDivData);
	    var producerTaskReagentDivData = $("#producerTaskReagentdiv").data("producerTaskReagentGrid");
		document.getElementById('producerTaskReagentJson').value = commonGetModifyRecords(producerTaskReagentDivData);
	    var producerTaskCosDivData = $("#producerTaskCosdiv").data("producerTaskCosGrid");
		document.getElementById('producerTaskCosJson').value = commonGetModifyRecords(producerTaskCosDivData);
	    var producerTaskResultDivData = $("#producerTaskResultdiv").data("producerTaskResultGrid");
		document.getElementById('producerTaskResultJson').value = commonGetModifyRecords(producerTaskResultDivData);

	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/producer/producerTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/producer/producerTask/copyProducerTask.action?id=' + $("#producerTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#producerTask_id").val() + "&tableId=producerTask");
//}
$("#toolbarbutton_status").click(function(){
//	var snpResultGrid = producerTaskResultGrid.getStore();
//	var snpItemGrid = producerTaskItemGrid.getStore();
//	var num = 0;
//	for(var i= 0; i<snpItemGrid.getCount();i++){
//		if(snpResultGrid.getCount()==0){
//			message("请完成实验！");
//			return;
//		}
//		for(var j=0;j<snpResultGrid.getCount();j++){
//			if(snpItemGrid.getAt(i).get("ySampleCode")==snpResultGrid.getAt(j).get("ySampleCode")){
//				j=snpResultGrid.getCount();
//			}else{
//				num=num+1;
//				if(num==snpResultGrid.getCount()){
//					num=0;
//					message("请完成实验！");
//					return;
//				}
//			}
//		}
//	}
	commonChangeState("formId=" + $("#producerTask_id").val() + "&tableId=ProducerTask");
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#producerTask_id").val());
	nsc.push(biolims.user.NoNull);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.scans,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/producer/producerTask/showProducerTaskItemList.action", {
				id : $("#producerTask_id").val()
			}, "#producerTaskItempage");
load("/experiment/producer/producerTask/showProducerTaskTemplateList.action", {
				id : $("#producerTask_id").val()
			}, "#producerTaskTemplatepage");
load("/experiment/producer/producerTask/showProducerTaskReagentList.action", {
				id : $("#producerTask_id").val()
			}, "#producerTaskReagentpage");
load("/experiment/producer/producerTask/showProducerTaskCosList.action", {
				id : $("#producerTask_id").val()
			}, "#producerTaskCospage");
load("/experiment/producer/producerTask/showProducerTaskResultList.action", {
				id : $("#producerTask_id").val()
			}, "#producerTaskResultpage");

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	//调用模板
	function TemplateFun(){
		var type="doProducer";
		var win = Ext.getCmp('TemplateFun');
		if (win) {win.close();}
		var TemplateFun= new Ext.Window({
		id:'TemplateFun',modal:true,title:'Select Template',layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
		 TemplateFun.close(); }  }]  }); 
		 TemplateFun.show(); 
	}
	
	function setTemplateFun(rec){
		document.getElementById('producerTask_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('producerTask_acceptUser_name').value=rec.get('acceptUser-name');
		var code=$("#producerTask_template").val();
		if(code==""){
			document.getElementById('producerTask_template').value=rec.get('id');
			document.getElementById('producerTask_template_name').value=rec.get('name');
			var win = Ext.getCmp('TemplateFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/system/template/template/setTemplateItem.action", {
				code : id,
				}, function(data) {
					if (data.success) {
						
						var ob = producerTaskTemplateGrid.getStore().recordType;
						producerTaskTemplateGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tItem",obj.id);
							p.set("code",obj.code);
							p.set("stepName",obj.name);
							
							p.set("note",obj.note);
							producerTaskTemplateGrid.getStore().add(p);							
							producerTaskTemplateGrid.startEditing(0, 0);		
						});
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateReagent.action", {
				code : id,
				}, function(data) {
					if (data.success) {

						var ob = producerTaskReagentGrid.getStore().recordType;
						producerTaskReagentGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tReagent",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							p.set("batch",obj.batch);
							p.set("isGood",obj.isGood);
							p.set("itemId",obj.itemId);
							
							p.set("oneNum",obj.num);
							p.set("note",obj.note);
							p.set("sn",obj.sn);
							producerTaskReagentGrid.getStore().add(p);							
						});
						
						producerTaskReagentGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null); 
				ajax("post", "/system/template/template/setTemplateCos.action", {
				code : id,
				}, function(data) {
					if (data.success) {

						var ob = producerTaskCosGrid.getStore().recordType;
						producerTaskCosGrid.stopEditing();
						
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("tCos",obj.id);
							p.set("code",obj.code);
							p.set("name",obj.name);
							p.set("isGood",obj.isGood);
							p.set("itemId",obj.itemId);
							
							p.set("temperature",obj.temperature);
							p.set("speed",obj.speed);
							p.set("time",obj.time);
							p.set("note",obj.note);
							producerTaskCosGrid.getStore().add(p);							
						});			
						producerTaskCosGrid.startEditing(0, 0);		
					} else {
						message(biolims.common.anErrorOccurred);
					}
				}, null);
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
				var ob1 = producerTaskTemplateGrid.store;
 				if (ob1.getCount() > 0) {
					for(var j=0;j<ob1.getCount();j++){
						var oldv = ob1.getAt(j).get("id"); 
						//根据ID删除
						if(oldv!=null){
							ajax("post", "/experiment/producer/producerTask/delProducerTaskTemplateOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null);
						}else{								
							producerTaskTemplateGrid.store.removeAll();
						}
					}
					producerTaskTemplateGrid.store.removeAll();
 				}

				var ob2 = producerTaskReagentGrid.store;
				if (ob2.getCount() > 0) {
					for(var j=0;j<ob2.getCount();j++){
						var oldv = ob2.getAt(j).get("id");

						//根据ID删除
						if(oldv!=null){
						ajax("post", "/experiment/producer/producerTask/delProducerTaskReagentOne.action", {
							ids : oldv
						}, function(data) {
							if (data.success) {
								message(biolims.common.deleteSuccess);
							} else {
								message(biolims.common.deleteFailed);
							}
						}, null); 
						}else{
							producerTaskReagentGrid.store.removeAll();
						}
					}
					producerTaskReagentGrid.store.removeAll();
 				}
				//=========================================
				var ob3 = producerTaskCosGrid.store;
				if (ob3.getCount() > 0) {
					for(var j=0;j<ob3.getCount();j++){
						var oldv = ob3.getAt(j).get("id");
						
						//根据ID删除
						if(oldv!=null){
							ajax("post", "/experiment/producer/producerTask/delProducerTaskCosOne.action", {
								ids : oldv
							}, function(data) {
								if (data.success) {
									message(biolims.common.deleteSuccess);
								} else {
									message(biolims.common.deleteFailed);
								}
							}, null); 
						}else{
							producerTaskCosGrid.store.removeAll();
						}
					}
					producerTaskCosGrid.store.removeAll();
 				}
 				document.getElementById('producerTask_template').value=rec.get('id');
				document.getElementById('producerTask_template_name').value=rec.get('name');
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
				var id = rec.get('id');
				ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = producerTaskTemplateGrid.getStore().recordType;
							producerTaskTemplateGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tItem",obj.id);
								p.set("code",obj.code);
								p.set("stepName",obj.name);
								
								p.set("note",obj.note);
								producerTaskTemplateGrid.getStore().add(p);							
							});
							
							producerTaskTemplateGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = producerTaskReagentGrid.getStore().recordType;
							producerTaskReagentGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tReagent",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("batch",obj.batch);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("oneNum",obj.num);
								p.set("note",obj.note);
								producerTaskReagentGrid.getStore().add(p);							
							});
							
							producerTaskReagentGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id,
					}, function(data) {
						if (data.success) {	

							var ob = producerTaskCosGrid.getStore().recordType;
							producerTaskCosGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tCos",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("temperature",obj.temperature);
								p.set("speed",obj.speed);
								p.set("time",obj.time);
								p.set("note",obj.note);
								producerTaskCosGrid.getStore().add(p);							
							});			
							producerTaskCosGrid.startEditing(0, 0);		
						} else {
							message(biolims.common.anErrorOccurred);
						}
					}, null); 
				}
			}

}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	function ckcrk(){
		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = producerTaskResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message("有样本未提交，不能初始化！");
						return;
					}
				}

				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : "ProducerTask"
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");
								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message("回滚失败！");
							}
						}, null);
					
				}
			}
		});
	}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: biolims.common.save
			});
		item.on('click', ckcrk2);
		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);
		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "ProducerTask",id : $("#producerTask_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
			} else {
				message("办理回滚失败！");
			}
		}, null);
	}

	
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				
				var ob = producerTaskTemplateGrid.getStore().recordType;
				producerTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("stepName",obj.name);
					
					p.set("note",obj.note);
					producerTaskTemplateGrid.getStore().add(p);							
					producerTaskTemplateGrid.startEditing(0, 0);		
				});
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = producerTaskReagentGrid.getStore().recordType;
				producerTaskReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("oneNum",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					producerTaskReagentGrid.getStore().add(p);							
				});
				
				producerTaskReagentGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = producerTaskCosGrid.getStore().recordType;
				producerTaskCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					producerTaskCosGrid.getStore().add(p);							
				});			
				producerTaskCosGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
}