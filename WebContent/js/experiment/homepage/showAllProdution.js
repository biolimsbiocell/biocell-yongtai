var bloodSplitTab;
$(function() {
//	var options = table(true, "",
//		"/experiment/allproduction/showAllProductionList.action", [{
//			"data": "id",
//			"title": "操作",
//			"width": "60px",
//			"render": function(data) {
//				return "<button id=" + data + " class='btn btn-info btn-xs plus' onclick='showSubTable(this)'>展开</button>&nbsp;&nbsp;<span class='glyphicon glyphicon-star text-green' style='display:none'></span>";
//			},
//			"createdCell": function(td, data, rowdata) {
//				if(rowdata.state == 3) {
//					if(rowdata.workUser) {
//						rowdata.workUser.split(",").forEach(function(v, i) {
//							if(v == window.userName) {
//								$(td).addClass("showTd");
//							}
//						});
//					}
//				}
//			}
//		}, /*{
//			"data": "id",
//			"title": biolims.common.id,
//		},  */{
//			"data": "batch",
//			"title": "产品批号",
//			"width": "88px",
//		},
////		, {
////			"data": "name",
////			"title": "姓名",
////		}
////		, {
////			"data": "patientName",
////			"title": "姓名",
////		},
//		{
//			"data": "filtrateCode",
//			"title": "筛选号",
//			"width": "88px",
//		},{
//			"data": "createUser-name",
//			"title": biolims.sample.createUserName,
//			"width": "88px",
//		}, {
//			"data": "createDate",
//			"title": biolims.sample.createDate,
//			"width": "88px",
//		}, {
//			"data": "workUser",
//			"title": biolims.common.testUserName,
//			"width": "88px",
//		}, {
//			"data": "template-name",
//			"title": biolims.common.experimentModule,
//			"width": "88px",
//		}, {
//			"data": "scopeName",
//			"title": biolims.purchase.costCenter,
//			"width": "88px",
//		}, {
//			"data": "stateName",
//			"title": biolims.common.stateName,
//			"width": "88px",
//		}], null);
//	bloodSplitTab = renderRememberData($("#allProduction"), options);
//	$('#allProduction').on('init.dt', function() {
//		recoverSearchContent(bloodSplitTab);
//	})
//新修改的主页生产代办
var colOpts = [];
colOpts.push({
	"data": "endTime",
	"title": "操作",
	"width": "30px",
	"render": function(data, type, row, meta) {
		if(data) {
			return "<button id=" + row['cellPassage-id'] + " class='btn btn-warning btn-xs' onclick='viewStrps(this)'>查看详情</button>";
		} else {
			return "<button id=" + row['cellPassage-id'] + " class='btn btn-info btn-xs' onclick='viewStrps(this)' name='"+row['id']+"'>操作</button>&nbsp;&nbsp;<span class='glyphicon glyphicon-star text-green' style='display:none'></span>";
		}
	},
	"createdCell": function(td, data, rowdata) {
		if(rowdata.testUserList) {
			rowdata.testUserList.split(",").forEach(function(v, i) {
				if(v == window.userName) {
					$(td).addClass("showTd");
				}
			});
		}
	}

});
colOpts.push({
"data": "cellPassage-batch",
"title": "产品批号",
"width": "120px",
"createdCell" : function(td, data, rowData) {
	$(td).attr("saveName", "cellPassage-batch");
	$(td).attr("cellPassage-id", rowData['cellPassage-id']);
	$(td).attr("orderNum", rowData['orderNum']);
	
}

});
colOpts.push({
"data": "cellPassage-patientName",
"title": "姓名",
"width": "40px",
});

colOpts.push({
	"data": 'planWorkDate',
	"title": "操作日期",
	"width": "80px",
});
colOpts.push({
	"data": 'templeItem-name',
	"title": "步骤名称",
	"width": "120px",
});
colOpts.push({
	"data": 'templeOperator-name',
	"title": "操作人",
	"width": "40px",
});
colOpts.push({
	"data": 'operatingRoomName',
	"title": "房间",
	"width": "40px",
	"createdCell": function(td, data, rowData) {
		$(td).attr("saveName", "operatingRoomName");
		$(td).attr("operatingRoomId", rowData['operatingRoomId']);
	}

});
colOpts.push({
	"data": 'counts',
	"title": "碳培养箱",
	"width": "80px",
});
colOpts.push({
	"data": 'posId',
	"title": "储位",
	"width": "50px",
});
colOpts.push({
	"data": 'cellPassage-infectionType',
	"title": "感染类型",
	"width": "50px",
});
colOpts.push({
"data": "cellPassage-filtrateCode",
"title": "筛选号",
"width": "40px",
});
colOpts.push({
	"data": "cellPassage-template-name",
	"title": "生产模块",
	"width": "120px",
    });
//colOpts.push({
//	"data": 'orderNum',
//	"title": "工序",
//	"width": "120px",
//});


colOpts.push({
	"data": 'estimatedDate',
	"title": "用时(天)",
	"width": "45px",
});



 
colOpts.push({
	"data": 'endTime',
	"title": "结束时间",
	"width": "60px",
});
colOpts.push({
	"data": 'templeItem-bottleOrBag',
	"title": "瓶/袋",
	"width": "30px",
	 render:function(data, type, row, meta) {
		 if (data == "0") {
				return "瓶";
			} else if (data == "1") {
				return "袋";
			} else {
				return "";
			}
	}
});
colOpts.push({
	"data": 'cellPassage-place',
	"title": "位置",
	"width": "40px",
});

colOpts.push({
	"data": 'note',
	"title": "备注",
	"width": "50px",
 });

		var options = table(true, "",
				"/experiment/productionPlan/productionPlan/NewFindCellPassageTemplate1List.action", colOpts, null);
		bloodSplitTab = renderRememberData($("#allProduction"), options);
//		$('#allProduction').on('init.dt', function() {
//			recoverSearchContent(bloodSplitTab);
//		})

});


//展示样本步骤
function showSubTable(that) {
	if(!$(that).parents("tr").next(".subTable").length) {
		$(that).parents("tr").after('<tr class="subTable" style="display:none"><td colspan="11"><table class="table table-bordered table-condensed" style="font-size: 12px;"></table></td></tr>');
	}
	var corspanTr = $(that).parents("tr").next(".subTable");
	if($(that).hasClass("plus")) { //展开
		$(that).removeClass("plus").addClass("minus");
		that.innerText = "关闭";
		corspanTr.slideDown();
		var elemen = corspanTr.find("table");
		if(!elemen.find("tr").length) {
			var id = that.id;
			renderSubTable(elemen, id);
		}
	} else if($(that).hasClass("minus")) { //关闭
		corspanTr.slideUp();
		that.innerText = "展开";
		$(that).removeClass("minus").addClass("plus");
	}
}
//
//function renderSubTable(elemen, id) {
//	colData = [{
//		"data": "endTime",
//		"title": "操作",
//		"render": function(data) {
//			if(data) {
//				return "<button id=" + id + " class='btn btn-warning btn-xs' onclick='viewStrps(this)'>查看详情</button>";
//			} else {
//				return "<button id=" + id + " class='btn btn-info btn-xs' onclick='viewStrps(this)'>操作</button>&nbsp;&nbsp;<span class='glyphicon glyphicon-star text-green' style='display:none'></span>";
//			}
//		},
//		"createdCell": function(td, data, rowdata) {
//			if(rowdata.testUserList) {
//				rowdata.testUserList.split(",").forEach(function(v, i) {
//					if(v == window.userName) {
//						$(td).addClass("showTd");
//					}
//				});
//			}
//		}
//
//	}, {
//		"data": 'orderNum',
//		"title": "工序",
//	}, {
//		"data": 'templeItem-name',
//		"title": "步骤名称",
//	}, {
//		"data": 'estimatedDate',
//		"title": "预计用时(天)",
//	}, {
//		"data": 'templeOperator-name',
//		"title": "操作人",
//	}, {
//		"data": 'operatingRoomName',
//		"title": "房间",
//	}, {
//		"data": 'planWorkDate',
//		"title": "预计操作日期",
//	}, {
//		"data": 'endTime',
//		"title": "结束时间",
//	}, {
//		"data": 'counts',
//		"title": "培养箱编号",
//	}, {
//		"data": 'posId',
//		"title": "位置",
//	}];
//	if(id.indexOf("CP")!=-1){//传代
////		if($("#type").val()=="1"){
//			var options = table(false, id,
//					"/experiment/productionPlan/productionPlan/findCellPassageTemplate1List.action", colData, null);
//			renderData(elemen, options);
////			bloodSplitTab = renderRememberData($("#allProduction"), options);
////			$('#allProduction').on('init.dt', function() {
////				recoverSearchContent(bloodSplitTab);
////			})
////		}else{
////			var options = table(false, id,
////					"/experiment/productionPlan/productionPlan/findCellPassageTemplateerList.action?inputDate="+$("#hide_input_date").val(), colData, null);
////			renderData(elemen, options);
////		}
//	}
//	
////	if(id.indexOf("PC")!=-1){//原代
////		var options = table(false, id,
////				"/experiment/cell/culture/cellPrimaryCulture/findCellPrimaryCultureTemplateList.action", colData, null);
////			renderData(elemen, options);
////	}else if(id.indexOf("CP")!=-1){//传代
////		var options = table(false, id,
////				"/experiment/cell/passage/cellPassage/findCellPassageTemplateList.action", colData, null);
////			renderData(elemen, options);
////	}else if(id.indexOf("PL")!=-1){//产品干
////		var options = table(false, id,
////				"/experiment/plasma/bloodSplit/findPlasmaTaskTemplateList.action", colData, null);
////			renderData(elemen, options);
////	}else if(id.indexOf("IP")!=-1){//免疫细胞
////		var options = table(false, id,
////				"/experiment/cell/immunecell/immuneCellProduction/findimmuneCellProductTemplateList.action", colData, null);
////			renderData(elemen, options);
////	}else if(id.indexOf("CT")!=-1){//细胞因子
////		var options = table(false, id,
////				"/experiment/cell/cytokine/cytokineProduction/findcytokineProductionTemplateList.action", colData, null);
////			renderData(elemen, options);
////	}
//}

function viewStrps(that) {
	var id = that.id;
//	var orderNum = $(that).parent("td").parent("tr").index() + 1;
//	var orderNum = $(that).parent("td").next().text();
	var orderNum=$(that).parent("td").next().attr("ordernum")
	sessionStorage.removeItem("dijibu");
	
	var cprId = $(that).attr("name");
	if(id.indexOf("CP")!=-1){//传代
//		$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//			"/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id=" + id + "&orderNum=" + orderNum;
//		if(cprId!=""&&cprId!=null){
//			$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//			"/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id=" + id + "&orderNum=" + orderNum+"&cprId="+cprId;
//		}else{
			$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
			"/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id=" + id + "&orderNum=" + orderNum;	
//		}	
	}
//	if(id.indexOf("PC")!=-1){//原代
//		$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//			"/experiment/cell/culture/cellPrimaryCulture/showCellPrimaryCultureSteps.action?id=" + id + "&orderNum=" + orderNum;
//	}else if(id.indexOf("CP")!=-1){//传代
//		$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//			"/experiment/cell/passage/cellPassage/showCellPassageSteps.action?id=" + id + "&orderNum=" + orderNum;
//	}else if(id.indexOf("PL")!=-1){
//		$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//			"/experiment/plasma/bloodSplit/showBloodSplitSteps.action?id=" + id + "&orderNum=" + orderNum;
//	}else if(id.indexOf("IP")!=-1){
//		$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//			"/experiment/cell/immunecell/immuneCellProduction/showImmuneCellProductionSteps.action?id=" + id + "&orderNum=" + orderNum;
//	}else if(id.indexOf("CT")!=-1){
//		$("#maincontentframe", window.parent.document)[0].src = window.ctx + 
//			"/experiment/cell/cytokine/cytokineProduction/showCytokineProductionSteps.action?id=" + id + "&orderNum=" + orderNum;
//	}
}

function serchpici(){
	var pici = $.trim($("#pici").val());

	if(pici==null||pici==""){
		bloodSplitTab.ajax.url("/experiment/productionPlan/productionPlan/NewFindCellPassageTemplate1List.action").load();
	}else{
		bloodSplitTab.ajax.url("/experiment/productionPlan/productionPlan/NewFindCellPassageTemplate1List.action?pici="+pici).load();
	}
}