/* 
 * 文件描述: 单位组的弹框
 * 
 */
var qualityTestForzj;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": "编号",
		"visible":false
	});
	colOpts.push({
		"data": "sampleOrder-id",
		"title": "关联订单号",
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
	});
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
	});
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.sampleType,
	});
	colOpts.push({
		"data": "dicSampleType-id",
		"title": biolims.common.dicSampleTypeId,
	});
	colOpts.push({
		"data": "dicSampleType-name",
		"title": biolims.common.dicSampleTypeName,
	});
	colOpts.push({
		"data": "qab",
		"title": '（Q01A01）表型检查',
		"visible":false,
	})
	colOpts.push({
		"data": "qaw1",
		"title": '（Q01A02）无菌1',
		"visible":false,
	})
	colOpts.push({
		"data": "qbw2",
		"title": '（Q01B01）无菌2',
		"visible":false,
	})
	colOpts.push({
		"data": "qbzy1",
		"title": '（Q01B02）支原体1',
		"visible":false,
	})
	colOpts.push({
		"data": "qbzy2",
		"title": '（Q01B03）支原体2',
		"visible":false,
	})
	colOpts.push({
		"data": "qcw1",
		"title": '（Q01C01）无菌1',
		"visible":false,
	})
	colOpts.push({
		"data": "qcNum",
		"title": 'PBMC分离:C（Q01C02）数量',
		"visible":false,
	})
	colOpts.push({
		"data": "qchl",
		"title": 'PBMC分离:C（Q01C03）活率',
		"visible":false,
	})
	colOpts.push({
		"data": "qmd",
		"title": '（Q0201）细胞密度',
		"visible":false,
	})
	colOpts.push({
		"data": "qw1",
		"title": '（Q0202）无菌1',
		"visible":false,
	})
	colOpts.push({
		"data": "qw3",
		"title": '（Q0301）无菌1',
		"visible":false,
	})
	colOpts.push({
		"data": "qw4",
		"title": '（Q0401）无菌1',
		"visible":false,
	})
	colOpts.push({
		"data": "qw42",
		"title": '（Q0401）无菌1',
		"visible":false,
	})
	colOpts.push({
		"data": "q5",
		"title":'（Q0502）表型检测',
		"visible":false,
	})
	colOpts.push({
		"data": "q6Num",
		"title":'（Q0601）细胞数量',
		"visible":false,
	})
	colOpts.push({
		"data": "q6hl",
		"title": '（Q0602）活率',
		"visible":false,
	})
	colOpts.push({
		"data": "q6w",
		"title": '（Q0603）无菌2',
		"visible":false,
	})
	colOpts.push({
		"data": "qzy1",
		"title":'（Q0604）支原体1',
		"visible":false,
	})
	colOpts.push({
		"data": "qzy2",
		"title": '（Q0605）支原体2',
		"visible":false,
	})
	colOpts.push({
		"data": "qds",
		"title": '（Q0606）细菌内毒素',
		"visible":false,
	})
	colOpts.push({
		"data": "result",
		"title": biolims.common.result,
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.disqualified;
			}
			if(data == "1") {
				return biolims.common.qualified;
			}
		}
	});
//	$.ajax({
//		  type: 'POST',
//		  url:ctx + "/system/detecyion/sampleDeteyion/fingItemListBySampleDeteyion.action",
//		  data:  {
//				id:$("#testId").val()
//			},
//		  async:false,
//		  success: function(data) {
//				var data = JSON.parse(data);
//				var list = data.list;
//				if(list.length){
//					for(var j=0;j<list.length;j++){
//						var field = list[j].note;
//						for(var i=0;i<colOpts.length;i++){
//							var data = colOpts[i].data;
//							if(data==field){
//								colOpts[i].visible = true;
//								break;
//							}
//						}
//					}
//				}
//				
//			}
//	
//		});
	var tbarOpts = [];

		var options = table(false, null,
				"/experiment/quality/qualityTest/showQualityTestResultListAllJsonForzj.action?testId="+$("#testId").val(), colOpts, null);
		qualityTestForzj = renderData($("#qualityTestResult"), options);
	
	/*$("#qualityTestResult").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			//$("#addSampleOrder_wrapper .dt-buttons").empty();
			$('#qualityTestResult_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#qualityTestResult tbody tr");
			
			qualityTestForzj.ajax.reload();
			qualityTestForzj.on('draw', function() {
				trs = $("#qualityTestResult tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});*/

})