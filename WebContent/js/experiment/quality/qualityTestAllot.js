﻿var qualityTestAllotTab;
var cellType = $("#cell_type").val();
var arr=[];
//左侧表
$(function() {
	var colOpts=[];
	if(cellType=="3"||cellType=="7"){
		colOpts.push({
			"data": "id",
			"visible": false,
			"title": biolims.common.id,
		});
		colOpts.push({
			"data": "storage-id",
			"title":  biolims.common.reagentNo,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-id");
			}
		});
		colOpts.push({
			"data": "storage-name",
			"title": biolims.common.designation,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-name");
			}
		});
		colOpts.push({
			"data": "storage-barCode",
			"title": biolims.tStorage.barCode,
			"visible":false,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "storage-barCode");
			},
		});
		colOpts.push({
			"data": "serial",
			"title": "产品批号ID",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "serial");
			},
		});
		
		colOpts.push({
			"data": "code",
			"title": "产品批号",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "code");
			},
		});
		colOpts.push({
			"data": "sampleDeteyion-name",
			"title":'检测项',
		});
		colOpts.push({
			"data": "scopeName",
			"visible":false,
			"title": biolims.purchase.costCenter,
		});
	}else{
		colOpts.push({
			"data": "id",
			"visible": false,
			"title": biolims.common.id,
		})
		colOpts.push({
			"data": "code",
			"title": biolims.common.code,
			"visible":false,
		})
		colOpts.push({
			"data": "SampleNumber",
			"title": "样本编号",
		})
		colOpts.push({
			"data": "sampleOrder-ccoi",
			"title": "CCOI",
			"createdCell": function(td, data,rowData) {
				$(td).attr("saveName", "sampleOrder-ccoi");
				$(td).attr("code", rowData['code']);
				
			},
		})
		colOpts.push({
			"data": "sampleCode",
			"visible":false,
			"title": biolims.common.sampleCode,
		})
		colOpts.push({
			"data": "sampleDeteyion-id",
			"visible":false,
			"title":'检测项',
		})
		colOpts.push({
			"data": "sampleDeteyion-name",
			"title":'检测项',
		})
		colOpts.push({
			"data": "productName",
			"title": biolims.common.productName,
		})
//		colOpts.push({
//			"data": "testItem",
//			"title": "检测项",
//		})
		/*colOpts.push({
			"data": "concentration",
			"title":biolims.common.concentration,
		})
		colOpts.push({
			"data": "volume",
			"title": biolims.common.volume,
		})*/
		colOpts.push({
			"data": "orderCode",
			"visible":false,
			"title": biolims.common.orderCode,
		})
		colOpts.push({
			"data": "batch",
			"title": "产品批号",
		})
		colOpts.push({
			"data": "sampleOrder-name",
			"title": "姓名",
		})
		colOpts.push({
			"data": "sampleOrder-filtrateCode",
			"title": "筛选号",
		})
		colOpts.push({
			"data": "sampleType",
			"title": biolims.common.sampleType,
		})
		colOpts.push({
			"data": "experimentalSteps",
			"title":"步骤",
		})
		colOpts.push({
			"data": "experimentalStepsName",
			"title":"步骤名称",
		})
		colOpts.push({
			"data": "sampleNum",
			"title": "检测量",
		})
		colOpts.push({
			"data": "sampleNumUnit",
			"title": "检验单位",
		})
		colOpts.push({
			"data": "qualitySubmitTime",
			"title": "质检提交时间",
		})
		colOpts.push({
			"data": "sampleInfo-id",
			"visible":false,
			"title": biolims.sample.sampleMasterData,
		})
		colOpts.push({
			"data": "sampleInfo-project-id",
			"visible":false,
			"title":biolims.common.projectId,
		})
		/*colOpts.push({
			"data": "scopeName",
			"title": biolims.purchase.costCenter,
		})*/
		colOpts.push({
			"data": "note",
			"title": "备注",
		})
		colOpts.push({
			"data": "sampleDeteyion-id",
			"title":'检测项ID',
			"createdCell": function(td) {
				$(td).attr("class", "sampleDeteyion-id");
			}
		});
	}
	var tbarOpts=[]
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	if(cellType=="3"||cellType=="7"){
		
	}else{
		tbarOpts.push({
			text: "入库",
			action:function(){
				ruku()
			}
		});
	}
	var options = table(true,null,"/experiment/quality/qualityTest/showQualityTestTempTableJson.action?cellType="+$("#cell_type").val(),colOpts, tbarOpts)
	//渲染样本的table
	qualityTestAllotTab=renderData($("#main"),options);
	
	
	//提示样本选中数量
	var selectednum=$(".selectednum").text();
		if(!selectednum){
			selectednum=0;
	}
	qualityTestAllotTab.on('draw', function() {
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				var	proId=$(this).parent().parent().siblings(".sampleDeteyion-id").text();
				$.ajax({
					type: 'post',
					url: ctx + "/experiment/quality/qualityTest/getTemplateId.action",
					data:{
						id: $("#qualityTest_id").text(),
						proId:proId,	
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
						var tpId=data.tpId;
						console.log($("[sopid='"+tpId+"']"))
						var length = $(".selected").length;
						$("[sopid='"+tpId+"']").addClass("sopChosed").siblings("li").removeClass("sopChosed");
						$(".label-warning").removeClass("selectednum").html("");
						$("[sopid='"+tpId+"']").children(".label-warning").addClass("selectednum").text(length + parseInt(selectednum));
						if($("[sopid='"+tpId+"']").children(".label-primary").text()) {
							if($("#maxNum").val() == "0") {
								$("#maxNum").val("1");
							}
						} else {
							$("#maxNum").val("0");
						}
						//显示隐藏模板里配置的实验员
						var userGroup=$("[sopid='"+tpId+"']").attr("suerGroup");
						$(".userLi").each(function (i,v) {
							var userGroupid=v.getAttribute("suerGroup");
							if (userGroup==userGroupid) {
								$(v).slideDown();
							}else{
								$(v).slideUp().children("img").removeClass("userChosed");
							}
						});
					
						}
					}
				});
			} else {
				index--;
			}
			$(".selectednum").text(index+parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	//选中模板的实验组的实验员显示隐藏
	if($(".sopChosed").length){
		var userGroup=$(".sopChosed").attr("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).show();
			}else{
				$(v).hide();
			}
		});
	}
	//sop和实验员的点击操作
	sopAndUserHandler(selectednum);
	//保存操作
	allotSave();
});
//左侧表入库
function ruku(){
	var arr = [];
	var rows = $("#main .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	rows.each(function(i, val) {
		var id = $(val).find("input[type=checkbox]").val();
		arr.push(id);
	});
	if(arr.length) {
		top.layer.confirm("确认入库？", function(index) {
			top.layer.close(index);
			$.ajax({
				type: "post",
				data: {
					ids: arr
				},
				url: ctx + "/experiment/quality/qualityTest/qualityTestRuku.action",
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						qualityTestAllotTab.ajax.reload();
					}
				}
			});
		});
	}
}
//sop和实验员的点击操作
function sopAndUserHandler(selectednum) {
	//执行sop点击选择操作
	$(".sopLi").click(function() {
		var length = $(".selected").length;
		$(this).addClass("sopChosed").siblings("li").removeClass("sopChosed");
		$(".label-warning").removeClass("selectednum").html("");
		$(this).children(".label-warning").addClass("selectednum").text(length+parseInt(selectednum));
		if($(this).children(".label-primary").text()) {
			if($("#maxNum").val() == "0") {
				$("#maxNum").val("1");
			}
		} else {
			$("#maxNum").val("0");
		}
		//显示隐藏模板里配置的实验员
		var userGroup=this.getAttribute("suerGroup");
		$(".userLi").each(function (i,v) {
			var userGroupid=v.getAttribute("suerGroup");
			if (userGroup==userGroupid) {
				$(v).slideDown();
			}else{
				$(v).slideUp().children("img").removeClass("userChosed");
			}
		});
	});
	//执行实验员点击选择操作
	$(".userLi").click(function() {
		var length = $(".selected").length;
			var img = $(this).children("img");
			if(img.hasClass("userChosed")) {
				img.removeClass("userChosed");
			} else {
				img.addClass("userChosed");
			}		
	});
}
//保存操作
function allotSave() {
	$("#save").click(function() {
		var changeLog = "";
		if(!$(".sopChosed").length) {
			top.layer.msg(biolims.common.selectTaskModel);
			return false;
		}
		if(!$(".userChosed").length) {
			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
			return false;
		}
		//获得选中样本的ID
		var sampleId = [];
		$(".selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
		});
		//获得选中实验员的ID
		var userId = [];
		$(".userChosed").each(function(i, val) {
			if($(".sopChosed").attr("suergroup")==$(val).parent("li").attr("suergroup")){
				userId.push($(val).parent("li").attr("userid"));
			}
		
		});
		
		changeLog = getChangeLog(userId, changeLog);
		if(changeLog !=""){
			changeLogs=changeLog;
		}
		
		//拼接主表的信息
		var main = {
			id: $("#qualityTest_id").text(),
			name: $("#qualityTest_name").val(),
			"createUser-id": $("#qualityTest_createUser").attr("userId"),
			createDate: $("#qualityTest_createDate").text(),
			state: $("#qualityTest_state").attr("state"),
			stateName: $("#qualityTest_state").text(),
			sampleNum:$(".selectednum").text(),
			maxNum:$("#maxNum").val(),
			cellType:$("#qualityTest_cellType").text()
		};
		var data = JSON.stringify(main);
		//拼接保存的data
		var info = {
			temp: sampleId,
			template: $(".sopChosed").attr("sopid"),
			user: userId.join(","),
			main: data,
			logInfo: changeLogs
		}
		top.layer.load(4, {shade:0.3}); 
		$.ajax({
		type : 'post',
		url:ctx+"/experiment/quality/qualityTest/saveAllot.action",
		data:info,
		success:function(data){
			var data = JSON.parse(data);
			if(data.success){	
				top.layer.closeAll();			
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx
			+ '/experiment/quality/qualityTest/editQualityTest.action?id=' + data.id+
			'&cellType='+$("#qualityTest_cellType").text();
			}
		}
		})
	});
}
//下一步操作
$("#next").click(function () {
	var qualityTest_id=$("#qualityTest_id").text();
	if(qualityTest_id=="NEW"){
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/quality/qualityTest/showQualityTestItemTable.action?id="+qualityTest_id+"&bpmTaskId="+$("#bpmTaskId").val()+"&cellType="+$("#cell_type").val();
});
//弹框模糊查询参数
function searchOptions() {
	if(cellType=="3"||cellType=="7"){
		return [{
			"txt" : biolims.common.designation,
			"type" : "input",
			"searchName" : "storage-name",
		}, {
			"txt":"产品批号",
			"type": "input",
			"searchName": "code",
		}, {
			"txt":"原辅料编号",
			"type": "input",
			"searchName": "storage-id",
		}, {
			"txt":"检测项",
			"type": "input",
			"searchName": "sampleDeteyion-name",
		},  {
			"type" : "table",
			"table" : qualityTestAllotTab
		} ];
	}else{
		return [{
			"txt" : biolims.common.code,
			"type" : "input",
			"searchName" : "sampleNumber",
		}/*,{
			"txt" : biolims.common.sampleCode,
			"type" : "input",
			"searchName" : "sampleCode",
		}*/, {
			"txt" : "检测项ID",
			"type": "input",
			"searchName": "sampleDeteyion-id",
		}, {
			"txt" : "检测项",
			"type": "input",
			"searchName": "sampleDeteyion-name",
		}, {
			"searchName": "productName",
			"txt": biolims.common.productName,
			"type": "input",
		}/*, {
			"txt":biolims.common.orderCode,
			"type": "input",
			"searchName": "orderCode",
		}*/, {
			"txt":"产品批号",
			"type": "input",
			"searchName": "batch",
		}, {
			"txt":"姓名",
			"type": "input",
			"searchName": "sampleOrder-name",
		}, {
			"txt":"筛选号",
			"type": "input",
			"searchName": "sampleOrder-filtrateCode",
		}, {
			"txt": biolims.common.sampleType,
			"type": "input",
			"searchName": "sampleType",
		},  {
			"type" : "table",
			"table" : qualityTestAllotTab
		} ];
	}

}
//核对编码
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layero) {
			var arr = $("#checkCode",parent.document).val().split("\n");
			qualityTestAllotTab.settings()[0].ajax.data = {"codes":arr};
			qualityTestAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});

}
//获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { //获取待处理样本的修改日志
	var sampleNew = [];
	$(".selected").each(function(i, val) {
		sampleNew.push($(val).children("td").eq(1).text());
	});
	changeLog += '"待处理样本"新增有：' + sampleNew + ";";
	//获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	//获取模板sop的修改日志
	if($(".sopChosed").attr("sopid") != $("#sopChangelogId").val()) {
		changeLog += '实验模板:由"' + $("#sopChangelogName").val() + '"变为"' + $(".sopChosed").children('.text').text() + '";';
		changeLog += '样本数量:由"' + $("#sopChangelogSampleNum").val() + '"变为"' + $(".sopChosed").children('.badge').text() + '";';
	}
	//获取实验员的修改日志
	var oldUserId = [],
		userName = [],
		oldUserName = [];
	$('input[class="changelogUserid"]').each(function(i, v) {
		oldUserId.push(v.value);
	});
	if(userId != oldUserId) {
		$(".userChosed").each(function(i, val) {
			userName.push($(val).next("a").text());
		});
		$('input[class="changelogUsername"]').each(function(i, v) {
			oldUserName.push(v.value);
		});
		changeLog += '实验员:由"' + oldUserName + '"变为"' + userName + '";';
	}

	return changeLog;
}


//质检条码确认
$("#codeConfirm").keyup(function(event){
	debugger
	console.log(event.keyCode)
	if(event.keyCode ==13){
		setTimeout(function() {
			codeConfirm();
		},500);
	}
 });
function codeConfirm(){
		var codeConfirm = $("#codeConfirm").val();
		if(codeConfirm!=""&&codeConfirm!=null){

			ajax(
					"post",
					"/experiment/cell/passage/cellPassage/findQualityTestSample.action",
					{
						codeConfirm: codeConfirm,

					},
					function(data) {
						if (data.success==true&&data.find!=true&&data.pan!=true) {
							top.layer.msg(biolims.common.submitSuccess);
							$("#codeConfirm").val("")
						} 
						if (data.success!=true&&data.find!=true&&data.pan!=true) {
							top.layer.msg("没有此条码或者此条码已经提交！");
							$("#codeConfirm").val("")
						} 
						if (data.success==true&&data.find==true&&data.pan!=true) {
							top.layer
							.msg("传递窗没有质检信息");
							$("#codeConfirm").val("")
						}
						if (data.success==true&&data.pan==true&&data.find!=true) {
							top.layer
							.msg("质检确认时间超过30分钟,样本已提交道异常列表");
							$("#codeConfirm").val("")
						}
					}, null);
		}else{
			
		}
	
}
//开始质检
$("#inspectionStart").keyup(function(event){
	if(event.keyCode ==13){
		setTimeout(function() {
			inspectionStart();
		},500);
	}
 });
function inspectionStart(){
		var inspectionStart = $("#inspectionStart").val();
		if(inspectionStart!=""&&inspectionStart!=null){
			ajax(
					"post",
					"/experiment/cell/passage/cellPassage/findQualityTestSampleStatr.action",
					{
						inspectionStart: inspectionStart,
						id:$("#qualityTest_id").text().trim()

					},
					function(data) {
						if (data.success==true&&data.pan!=true) {
//							top.layer
//							.msg("确认成功");
//							$("#inspectionStart").val("")
							if($("#qualityTest_id").text().trim()=="NEW"){
								var param = {
							            query:'{"sampleNumber":"%'+data.qualityTestSample.sampleNumber+'%","sampleDeteyion-id":"","sampleDeteyion-name":"","productName":"","batch":"","sampleOrder-name":"","sampleOrder-filtrateCode":"","sampleType":""}'
							            };
										qualityTestAllotTab.settings()[0].ajax.data = param
										qualityTestAllotTab.ajax.reload();
										qualityTestAllotTab.on('draw', function() {
											$('#main').find('.odd').addClass('selected');
											$('#main').find('.icheckbox_square-blue').addClass('checked');
										setTimeout(function() {
											
											
											$($("#main tbody tr")[0]).find('.iCheck-helper').click();
										},200);
										setTimeout(function() {
											$(".userLi").each(function(i,v){
												if($(v).attr('userid')==sessionStorage.getItem("userId")){
													$(v).click();
													return;
												}
											});
										},400);
										setTimeout(function() {
											$("#save").click();
										},600);
										});
										
									
							}else{
								
								$.ajax({
									type: "post",
									data: {
										id: $("#qualityTest_id").text().trim(),
										coed:$("#inspectionStart").val()
										
									},
									url: ctx + "/experiment/quality/qualityTest/findQualityTest.action",
									success: function(data) {
										var data = JSON.parse(data);
										if(data.success) {
											
											top.layer.msg(biolims.common.saveSuccess);
											window.location = window.ctx
											+ '/experiment/quality/qualityTest/editQualityTest.action?id=' + $("#qualityTest_id").text().trim()+
											'&cellType='+$("#qualityTest_cellType").text();
										}else{
											
											top.layer.msg("请选择相同检测项！")
											$("#inspectionStart").val('');
									}
									}
								});
								
							}
			                
//						

							
							
						
							
							
							
							
							
//								var SampleNumber = $(v).find("td[savename='SampleNumber']").text();
//								if(SampleNumber==data.qualityTestSample.sampleNumber){
//									
//								}
//									
							
						}
						if (data.success!=true&&data.pan!=true) {
							top.layer
							.msg("请先进行条码确认!");
							$("#inspectionStart").val("")
							
						}
						
						if(data.pan==true){
							top.layer
							.msg("QC已经扫码！");
							$("#inspectionStart").val("")
						}
					}, null);
		}else{
			
		}
}
