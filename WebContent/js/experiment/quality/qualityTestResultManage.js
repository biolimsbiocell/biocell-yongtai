var qualityTestTab;
$(function() {
	if($("#handlemethod").val() == "view") {
		settextreadonly();
	$("#item_input").hide();
}
	var options = table(true, "",
			"/experiment/quality/qualityTestResultManage/showQualityTestResultManageListJson.action", [ 
			 {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			},{
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "sampleOrder-id",
				"title" : "订单编号",
			}, {
				"data" : "sampleOrder-barcode",
				"title" : "产品批号",
			}, {
				"data" : "sampleOrder-batchStateName",
				"title" : "批次状态",
			},
//			{
//				"data" : "sampleOrderState",
//				"title" : "产品状态",
//			},
			{
				"data" : "createUser-name",
				"title" : "第一提交人",
			}, {
				"data" : "createDate",
				"title" : "第一提交时间",
			}, {
				"data" : "confirmUser-name",
				"title" : "第一审核人",
			}, {
				"data" : "confirmDate",
				"title" : "第一审核时间",
			},{
				"data" : "createUserTwo-name",
				"title" : "第二提交人",
			}, {
				"data" : "createDateTwo",
				"title" : "第二提交时间",
			}, {
				"data" : "confirmUserTwo-name",
				"title" : "第二审核人",
			}, {
				"data" : "confirmDateTwo",
				"title" : "第二审核时间",
			} ], null)
	qualityTestTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(qualityTestTab);
	});
	$("#btn_add").hide();
});

function add() {
	window.location = window.ctx
			+ '/experiment/quality/qualityTestResultManage/editQualityTestResultManage.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/quality/qualityTestResultManage/editQualityTestResultManage.action?id=' + id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/quality/qualityTestResultManage/viewQualityTestResultManage.action?id="
			+ id ;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : "订单编号",
		"type" : "input",
		"searchName" : "sampleOrder-id",
	}, {
		"txt" : "产品批号",
		"type" : "input",
		"searchName" : "sampleOrder-barcode",
	}, {
		"type" : "table",
		"table" : qualityTestTab
	} ];
}
