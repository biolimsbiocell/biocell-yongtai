var qualityTestTab;
$(function() {
	var options = table(true, "",
			"/experiment/quality/qualityTest/showQualityTestTableJson.action?cellType="
			+$("#cell_type").val(), [ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "name",
				"title" : biolims.common.name,
			}, {
				"data" : "sampleNum",
				"title" : "检测数量",
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "confirmDate",
				"title" : biolims.common.confirmDate,
			}, {
				"data" : "testUserOneName",
				"title" : biolims.common.testUserName,
			}, {
				"data" : "template-name",
				"title" : biolims.common.experimentModule,
			}, {
				"data" : "scopeName",
				"title" : biolims.purchase.costCenter,
			}, {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			} ], null)
	qualityTestTab = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(qualityTestTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/quality/qualityTest/editQualityTest.action?cellType='
			+ $("#cell_type").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/quality/qualityTest/editQualityTest.action?id=' + id +'&cellType='+$("#cell_type").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/quality/qualityTest/showQualityTestSteps.action?id="
			+ id +"&cellType="+$("#cell_type").val();
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"searchName" : "confirmDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "confirmDate##@@##2"
	}, {
		"type" : "table",
		"table" : qualityTestTab
	} ];
}
