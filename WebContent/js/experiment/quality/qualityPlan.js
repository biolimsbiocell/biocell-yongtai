var qualityPlanGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'planDate',
		type:"string"
	});
	    fields.push({
		name:'workState',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'名称',
		width:50*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'planDate',
		header:'计划日期',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'workState',
		header:'工作流状态',
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/quality/qualityPlan/showQualityPlanListJson.action";
	var opts={};
	opts.title="质控计划";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	qualityPlanGrid=gridTable("show_qualityPlan_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/quality/qualityPlan/editQualityPlan.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/quality/qualityPlan/editQualityPlan.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/quality/qualityPlan/viewQualityPlan.action?id=' + id;
}
function exportexcel() {
	qualityPlanGrid.title = '导出列表';
	var vExportContent = qualityPlanGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startplanDate").val() != undefined) && ($("#startplanDate").val() != '')) {
					var startplanDatestr = ">=##@@##" + $("#startplanDate").val();
					$("#planDate1").val(startplanDatestr);
				}
				if (($("#endplanDate").val() != undefined) && ($("#endplanDate").val() != '')) {
					var endplanDatestr = "<=##@@##" + $("#endplanDate").val();

					$("#planDate2").val(endplanDatestr);

				}
				
				
				commonSearchAction(qualityPlanGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
