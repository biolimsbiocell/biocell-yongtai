﻿var oldChangeLog;

$(function() {
	var colOpts = [];
	//查询实验步骤来显示隐藏某些字段
	var flg1A=false;
	var flg1B=false;
	var flg1C=false;
	var flg2=false;
	var flg3=false;
	var flg4=false;
	var flg5=false;
	var flg6=false;
	var flg7=false;
	var flg8=false;
	var jc=	$("#sampleDeteyionName").val().replace(/\s+/g,"");
	var ss =$("#experimentalSteps").val().replace(/\s+/g,"");;

		       if(ss=='PBMC分离'){
		    	if(jc=="检测项A"){
		    	
		    		flg1A=true;
		    	}else if(jc=="检测项B"){
		    		flg1B=true;
		    	}else if(jc=="检测项C"){
		    		flg1C=true;
		    	}
		       }else if(ss=="补液1"){
		    	   flg2=true;
		       }else if(ss=="补液2"){
		    	   flg3=true;
		       }else if(ss=="装袋培养"){
		    	   flg4=true;
		       }else if(ss=="分袋培养"){
		    	   flg5=true;
		       }else if(ss=="分袋培养1"){
		    	   flg6=true;
		       }else if(ss=="分袋培养2"){
		    	   flg7=true;
		       }else if(ss=="收获"){
		    	   flg8=true;
		       }
	
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	/*colOpts.push({
		"data": "sampleOrder-id",
		"title":"关联订单",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.common.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.sampleType,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleType");
		}
	})
	colOpts.push({
		"data": "dicSampleType-id",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	})
	colOpts.push({
		"data": "dicSampleType-name",
		"title":biolims.common.dicSampleTypeName,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-name");
		}
	})*/
	colOpts.push({
		"data": "experimentalSteps",
		"title": biolims.common.commonTemplate,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "experimentalSteps");
		},
	})
		colOpts.push({
		"data": "qab",
		"title": '（Q01A01）表型检查',
		"className": "edit",
		"visible": flg1A,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qab");
		},
	})
	colOpts.push({
		"data": "qaw1",
		"title": '（Q01A02）无菌1',
		"className": "edit",
		"visible": flg1A,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qaw1");
		},
	})
	colOpts.push({
		"data": "qbw2",
		"title": '（Q01B01）无菌2',
		"className": "edit",
		"visible": flg1B,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qbw2");
		},
	})
	colOpts.push({
		"data": "qbzy1",
		"title": '（Q01B02）支原体1',
		"className": "edit",
		"visible": flg1B,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qbzy1");
		},
	})
	colOpts.push({
		"data": "qbzy2",
		"title": '（Q01B03）支原体2',
		"className": "edit",
		"visible": flg1B,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qbzy2");
		},
	})
	colOpts.push({
		"data": "qcw1",
		"title": '（Q01C01）无菌1',
		"className": "edit",
		"visible": flg1C,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qcw1");
		},
	})
	colOpts.push({
		"data": "qcNum",
		"title": 'PBMC分离:C（Q01C02）数量',
		"className": "edit",
		"visible": flg1C,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qcNum");
		},
	})
	colOpts.push({
		"data": "qchl",
		"title": 'PBMC分离:C（Q01C03）活率',
		"className": "edit",
		"visible": flg1C,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qchl");
		},
	})
	colOpts.push({
		"data": "qmd",
		"title": '（Q0201）细胞密度',
		"className": "edit",
		"visible": flg4,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qmd");
		},
	})
	colOpts.push({
		"data": "qw1",
		"title": '（Q0202）无菌1',
		"className": "edit",
		"visible": flg4,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})
	colOpts.push({
		"data": "qw3",
		"title": '（Q0301）无菌1',
		"className": "edit",
		"visible": flg5,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qw3");
		},
	})
	colOpts.push({
		"data": "qw4",
		"title": '（Q0401）无菌1',
		"className": "edit",
		"visible": flg6,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qw4");
		},
	})
	colOpts.push({
		"data": "qw42",
		"title": '（Q0401）无菌1',
		"className": "edit",
		"visible": flg7,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qw42");
		},
	})
	colOpts.push({
		"data": "q5",
		"title":'（Q0502）表型检测',
		"className": "edit",
		"visible": flg7,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "q5");
		},
	})
	colOpts.push({
		"data": "q6Num",
		"title":'（Q0601）细胞数量',
		"className": "edit",
		"visible": flg8,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "q6Num");
		},
	})
	colOpts.push({
		"data": "q6hl",
		"title": '（Q0602）活率',
		"className": "edit",
		"visible": flg8,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "q6hl");
		},
	})
	colOpts.push({
		"data": "q6w",
		"title": '（Q0603）无菌2',
		"className": "edit",
		"visible": flg8,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})
	colOpts.push({
		"data": "qzy1",
		"title":'（Q0604）支原体1',
		"className": "edit",
		"visible": flg8,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qzy1");
		},
	})
	colOpts.push({
		"data": "qzy2",
		"title": '（Q0605）支原体2',
		"className": "edit",
		"visible": flg8,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qzy2");
		},
	})
	colOpts.push({
		"data": "qds",
		"title": '（Q0606）细菌内毒素',
		"className": "edit",
		"visible": flg8,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "qds");
		},
	})
	
	/*colOpts.push({
		"data": "orderId",
		"title": biolims.common.qcTask,
		"visible": false,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "experimentalSteps");
		},
	})
	colOpts.push({
		"data": "concentration",
		"title": biolims.common.concentration,
		"className":"edit",
		
		"createdCell": function(td) {
			$(td).attr("saveName", "concentration");
		}
	})
	colOpts.push({
		"data": "volume",
		"title": biolims.common.volume,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "volume");
		}
	})
	colOpts.push({
		"data": "sampleInfo-id",
		"title": biolims.common.volume,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleInfo-id");
		}
	})
	colOpts.push({
		"data": "result",
		"title": biolims.common.result+'<img src="/images/required.gif"/>',
		"className":"select",
		"name":biolims.common.qualified+"|"+biolims.common.disqualified,
		"createdCell": function(td) {
			$(td).attr("saveName", "result");
			$(td).attr("selectOpt", biolims.common.qualified+"|"+biolims.common.disqualified);
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.disqualified;
			}
			if(data == "1") {
				return biolims.common.qualified;
			}
		}
	})
	colOpts.push({
		"data": "nextFlowId",
		"title": biolims.common.nextFlowId,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlowId");
		}
	})
	colOpts.push({
		"data": "nextFlow",
		"title": biolims.common.nextFlow,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlow");
		}
	})
	colOpts.push({
		"data": "reportDate",
		"title": biolims.common.reportDate,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "reportDate");
		}
	})
	colOpts.push({
		"data": "method",
		"title": biolims.common.method,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "method");
		}
	})
	colOpts.push({
		"data": "submit",
		"title": biolims.common.toSubmit,
		"name":biolims.common.yes+"|"+biolims.common.no,
		"createdCell": function(td) {
			$(td).attr("saveName", "submit");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.yes;
			}
			else if(data == "0") {
				return biolims.common.no;
			}else {
				return "";
			}
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	})*/
	window.localStorage.clear();
	var visibleItem = $("#templateFieldsItemCode").val();
	if(visibleItem) {
		var visibleItemArr = visibleItem.split(",");
		visibleItemArr.forEach(function(v, i) {
			colOpts.forEach(function(vv, ii) {
				if(v == vv.data) {
					vv.visible=true;
				}
			});
		});
	}
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#qualityTest_state").text()!=biolims.common.finish){
/*	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#qualityTestResultdiv"),
				"/experiment/quality/qualityTest/delQualityTestResult.action","质量检测结果删除样本：",$("#qualityTest_id").text());
		}
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-th"></i>'+biolims.common.applicationOper,
		action: function() {
			$.ajax({
				type: "post",
				data: {
					id:$("#qualityTest_id").text()
				},
				url: ctx + "/experiment/quality/qualityTest/bringResult.action",
				success: function(data) {
					var data = JSON.parse(data)
					if(data.success) {
						qualityTestResultTab.ajax.reload();
					} else {
						top.layer.msg(biolims.purchase.failed)
					}
	
				}
			});
		}
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-share-alt"></i>'+biolims.common.uploadResult,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#qualityTest_id").text(),
						fileId: data.response.fileId
					},
					url: ctx + "/experiment/quality/qualityTest/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							qualityTestResultTab.ajax.reload();
						} else {
							top.layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.batchResult,
		className: 'btn btn-sm btn-success resultsBatchBtn',
	});
	tbarOpts.push({
		text: '<i class="glyphicon glyphicon-floppy-open"></i>'+biolims.common.uploadAttachment,
		action: function() {
			$("#uploadFile").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput =fileInput('1', 'qualityTest', $("#qualityTest_id").text());
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-paypal"></i> '+biolims.common.selectNextFlow,
		action: function() {
			nextFlow();
		}
	});
	tbarOpts.push({
		text: biolims.common.productType,
		action: function() {
			addSampleType();
		}
	})
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveItem();
		}
	});
	tbarOpts.push({
		text: biolims.common.submitSample,
		action: function() {
			submitSample();
		}
	});
	//添加浓度按钮
	tbarOpts.push({
		text: biolims.common.concentration,
		className: 'btn btn-sm btn-success btnConcentration',
		action: function() {
			submitConcentration();
		}
	});
	//添加体积按钮
	tbarOpts.push({
		text: biolims.common.volume,
		className: 'btn btn-sm btn-success btnVolume',
		action: function() {
			submitVolume();
		}
	});
	//添加下载Exel按钮
	tbarOpts.push({
		text : biolims.common.downloadCsvTemplet,
		action : function() {
			downLoadTemp();
		}
	});*/
	}
	
	var sid=	$("#sampleDeteyion").val()
	var es =$("#experimentalSteps").val();
      
      	sampleDeteyion=  encodeURI(encodeURI(sid));
      	experimentalSteps=  encodeURI(encodeURI(es));
	var qualityTestResultOps = table(true, $("#qualityTest_id").text(), "/experiment/quality/qualityTest/showSampledetecyionDialogJson.action?orderId="+$("#orderId").val()+"&experimentalSteps="+experimentalSteps+"&sampleDeteyion="+sampleDeteyion, colOpts, tbarOpts);
	qualityTestResultTab = renderData($("#qualityTestResultdiv"), qualityTestResultOps);
	qualityTestResultTab.on('draw', function() {
		oldChangeLog = qualityTestResultTab.ajax.json();
	});
	
	//批量结果
	btnChangeDropdown ($('#qualityTestResultdiv'),$(".resultsBatchBtn"),[biolims.common.qualified,biolims.common.disqualified],"result");
	bpmTask($("#bpmTaskId").val());
	//上一步下一步操作
	preAndNext();
	if(handlemethod == "view"||$("#qualityTest_state").text()==biolims.common.finish){
		settextreadonly();
		$("#save").hide()
		$("#sp").hide()
		$("#finish").hide()
	}
});
//下一步流向
function nextFlow() {
	var rows = $("#qualityTestResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var productId = "";
	var sampleType="";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
		sampleType = $(k).find("td[savename='dicSampleType-id']").text();
	});
	top.layer.open({
		title:biolims.common.selectNextFlow,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=QualityTest&productId="
					+ productIds+"&sampleType="+sampleType,''],
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
			.eq(1).text();
		var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
			0).text();
		rows.addClass("editagain");
		rows.find("td[savename='nextFlow']").text(name);
		rows.find("td[savename='nextFlowId']").text(id);

		top.layer.close(index)
		},
	})
}
// 保存
function saveItem() {
	var ele=$("#qualityTestResultdiv");
	var changeLog = "实验结果：";
	var data = saveItemjson(ele);
	if(!data){
		return false;
	}
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !=""){
		changeLogs=changeLog;
	}
	top.layer.load(4, {shade:0.3});
	$.ajax({
		type: 'post',
		url: '/experiment/quality/qualityTest/saveResult.action',
		data: {
			id: $("#qualityTest_id").text(),
			dataJson: data,
			logInfo: changeLogs,
			confirmUser:$("#confirmUser_id").val()
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
			    top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
			    top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
        var flag =true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			// 判断男女并转换为数字
			if(k == "result") {
				var result = $(tds[j]).text();
				if(result == biolims.common.disqualified) {
					json[k] = "0";
				} else if(result == biolims.common.qualified) {
					json[k] = "1";
				}
				continue;
			}
			//判断必填
			if(k=="nextFlow"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg(biolims.common.pleaseSelectNextFlow);
					return false;
				}
			}
			if(k=="result"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg(biolims.common.chooseTaskResult);
					return false;
				}
			}
			if(k == "submit") {
				var submit = $(tds[j]).text();
				if(submit == biolims.common.no) {
					json[k] = "0";
				} else if(submit == biolims.common.yes) {
					json[k] = "1";
				}
				continue;
			}
			if(k == "note"){
				json["sampleInfo-id"] = $(tds[j]).attr("sampleInfo-id");
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	if(flag){
		return JSON.stringify(data);
	}else{
		return false;
	}
}

//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/quality/qualityTest/showQualityTestSteps.action?id=" + $("#qualityTest_id").text()+"&bpmTaskId="+$("#bpmTaskId").val();
	});
	//上一步操作
	$("#finish").click(function() {
	var paraStr = "formId=" + $("#qualityTest_id").text() +
		"&tableId=QualityTest";
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layero) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: $("#qualityTest_id").text()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		},
		cancel: function(index, layero) {
			top.layer.close(index)
		}

	});
});
	
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
//添加样本类型
function addSampleType() {
	var rows = $("#$({entityName?uncap_first}Resultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.selectSampleType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/sample/dicSampleType/selectDicSampleTypeOne.action", ''],
		yes: function(index, layer) {
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td").eq(
				0).text();
			rows.addClass("editagain");
			rows.find("td[savename='dicSampleType-name']").attr(
				"dicSampleType-id", id).text(type);
			rows.find("td[savename='dicSampleType-id']").text(id);

			top.layer.close(index)
		},
	})
}
function submitSample(){
	var rows = $("#qualityTestResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids=[];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val())
	});
	ajax("post", "/experiment/quality/qualityTest/submitSample.action", {
		ids: ids,
		id: $("#qualityTest_id").text()
	}, function(data) {
		if(data.success) {
			tableRefresh();
		} else {
			top.layer.msg("提交失败");
		}
	}, null)
}
function sp() {
	if(!$("#confirmUser_id").val()){
		top.layer.open({
			title: biolims.sample.pleaseSelectReviewer,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
			yes: function(index, layer) {
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
				$("#confirmUser_name").text(name);
				$("#confirmUser_id").val(id);
				;
				top.layer.close(index);
			},
			end:function(){
				splc();
			}
		})
	}else{
		splc();
	}
}
function splc(){

	var taskId = $("#bpmTaskId").val();
	var formId = $("##qualityTest_id").text();

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								window.open(window.ctx+"/main/toPortal.action",'_parent');
								top.layer.close(index);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
		}
	});
}
//赋值勾选的浓度
function submitConcentration(){
	var rows = $("#qualityTestResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//勾选得chekbox得id数组集合
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val());
	});
	var concentrationList = [];
	$.each(rows, function(i, v) {
		concentrationList.push($(v).find("td[savename=concentration]").text());
	});
	//获取勾选的浓度得第一个值,最后要用的值
	var concentrationValue=concentrationList[0];
	$.each(rows, function(i, v) {
		$(v).find("td[savename=concentration]").text(concentrationValue);
		$(v).addClass("editagain");
	});
}

//赋值勾选的体积
function submitVolume(){
	var rows = $("#qualityTestResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//勾选得chekbox得id数组集合
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val());
	});
	var concentrationList = [];
	$.each(rows, function(i, v) {
		concentrationList.push($(v).find("td[savename=volume]").text());
		//获取勾选的浓度得第一个值,最后要用的值
		var volumeValue=concentrationList[0];
		
		$(v).find("td[savename=volume]").text(volumeValue);
		$(v).addClass("editagain");
	});
}
//downLoadTemp
function downLoadTemp(){
	var rows = $("#qualityTestResultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids = [];
	var codes = [];
	$.each(rows, function(i, k) {
		ids.push($(k).find("input[type=checkbox]").val());
		codes.push($(k).find("td[savename=code]").text());
	});
	if ("" == codes) {
		message(biolims.common.saveDownload);
		return;
	}
	window.open(window.ctx + '/experiment/quality/qualityTest/downLoadTemp.action?codes='+codes+'&ids='+ids);
}