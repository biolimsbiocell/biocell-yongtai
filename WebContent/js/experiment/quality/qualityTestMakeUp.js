/* 
 * 文件名称 :qualityTestMakeUp.js
 * 创建者 : 
 * 创建日期: 
 * 文件描述: 1. 生成待处理样本的table表
 * 			2. 根据上一步的选择的数量，页面动态生成不同布局的孔板div
 * 			3. 给div孔板绘制孔板
 * 			4. 选择数据，赋值给孔板，并生成坐标
 * 			5. 孔板上的样本移位置
 * 			6. 已排版样本重置
 */
//待排板样本
var qualityTestMakeUpTab;
var qualityTest_id = $("#qualityTest_id").text();
var isSeparate=$("#qualityTest_isSeparate").val();
var cell_type=$("#cell_type").val();
var flg=false;
if(isSeparate==1){
	flg=true;
}

$(function() {
	var colOpts = [];
	if(cell_type=="3"||cell_type=="7"){
		colOpts.push({
			"data": "id",
			"visible": false,
			"title": biolims.common.id,
		});
		colOpts.push({
			"data": "storagea-id",
			"title":  biolims.common.reagentNo,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-id");
			}
		});
		colOpts.push({
			"data": "storagea-name",
			"title": biolims.common.designation,
			"createdCell": function(td) {
				$(td).attr("saveName", "storage-name");
			}
		});
		colOpts.push({
			"data": "storagea-barCode",
			"title": biolims.tStorage.barCode,
			"visible": false,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "storage-barCode");
			},
		});
		colOpts.push({
			"data": "serial",
			"title": biolims.common.batchId,
			"createdCell": function(td, data,rowData) {
				$(td).attr("saveName", "serial");
				$(td).attr("code", rowData['code']);				
			},
		});
		
		colOpts.push({
			"data": "code",
			"title": biolims.storage.batchNo,
			"visible":false,
			"createdCell": function(td, data) {
				$(td).attr("saveName", "code");
			},
		});
		colOpts.push({
			"data": "sampleDeteyion-id",
			"visible":false,
			"title":'检测项',
		});
		colOpts.push({
			"data": "sampleDeteyion-name",
			"title":'检测项',
		});
	}else{
		colOpts.push({
			"data": "id",
			"title": biolims.common.id,
			"visible": false,
			"createdCell": function(td) {
				$(td).attr("saveName", "id");
			}
		});
		colOpts.push({
			"data": "sampleCode",
			"title": biolims.common.sampleCode,
			"createdCell": function(td) {
				$(td).attr("saveName", "sampleCode");
			}
		});
		colOpts.push({
			"data": "SampleNumber",
			"title": "样本编号",
		})
		colOpts.push({
			"data": "code",
			"visible":false,
			"title": biolims.common.code,
			"createdCell": function(td) {
				$(td).attr("saveName", "code");
			}
		})
		colOpts.push({
			"data": "productName",
			"title": biolims.common.productName,
		})
		colOpts.push({
			"data": "sampleType",
			"title": "样本类型",
		})
		colOpts.push({
			"data": "batch",
			"title": "批次号",
			"createdCell": function(td,data,rowData) {
				$(td).attr("saveName", "batch");
				$(td).attr("code", rowData['code']);
			}
		})
		colOpts.push({
			"data": "orderCode",
			"title": biolims.common.orderCode,
		})
		colOpts.push({
			"data": "sampleOrder-name",
			"title": "姓名",
		});
		colOpts.push({
			"data": "sampleOrder-filtrateCode",
			"title": "筛选号",
		});
		colOpts.push({
			"data": "chromosomalLocation",
			"title": biolims.sanger.chromosomalLocation,
			"visible":flg,
			"createdCell": function(td) {
				$(td).attr("saveName", "chromosomalLocation");
			}
		});
		colOpts.push({
			"data": "experimentalSteps",
			"title": "步骤",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "experimentalSteps");
			},
		});
		colOpts.push({
			"data": "testItem",
			"title": "检测项",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "testItem");
			},
		});
		colOpts.push({
			"data": "sampleNum",
			"title": "检测量",
		})
		colOpts.push({
			"data": "mark",
			"title": "来源模块",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "mark");
			},
		});
		colOpts.push({
			"data": "zjName",
			"title": "质检名称",
			"createdCell": function(td, data) {
				$(td).attr("saveName", "ziName");
			},
		});
		colOpts.push({
			"data": "note",
			"title": "备注",
		})
		/*colOpts.push({
			"data": "sampleDeteyion-id",
			"title": '检测项Id',
			"createdCell": function(td, data) {
				$(td).attr("saveName", "sampleDeteyion");
			},
		});
		colOpts.push({
			"data": "sampleDeteyion-name",
			"title": '检测项',
			"createdCell": function(td, data) {
				$(td).attr("saveName", "sampleDeteyion");
			},
		});*/
	}
	
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view"||$("#qualityTest_state").text()!="Complete"){
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#qualityTestMakeUpdiv"),
				"/experiment/quality/qualityTest/delQualityTestItem.action","质量检测删除待排版样本：",$("#qualityTest_id").text());
		}
	});
	//按钮添加质控品
	tbarOpts.push({
		text: biolims.wk.addQuality,
		action: function() {
			addLeftQuality();
		}
	});
	}
	var qualityTestMakeUpOps = table(true, qualityTest_id, "/experiment/quality/qualityTest/showQualityTestItemTableJson.action", colOpts, tbarOpts);
	qualityTestMakeUpTab = renderData($("#qualityTestMakeUpdiv"), qualityTestMakeUpOps);
	//根据上一步选择页面变化生成孔板
	
	if($("#maxNum").val()>0){
		$("#plateModal").show()
		//根据上一步选择页面变化生成孔板
		rendrModalDiv($("#maxNum").val(), $("#temRow").val(), $("#temCol").val());
		//填充孔板
		createShelf($("#temRow").val(), $("#temCol").val(), $(".plate"), renderChosedSample);
		showPlate();
	}
	
	
	//选择数据并提示
	qualityTestMakeUpTab.on('draw', function() {
		var index = 0;
		$("#qualityTestMakeUpdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
	//上一步下一步操作
	
	preAndNext();
	if(handlemethod == "view"||$("#qualityTest_state").text()=="Complete"||$("#qualityTest_state").text()=="完成"){
		settextreadonly();
		$("#save").hide()
		$("#tjsp").hide()
	}
	if($("#qualityTest_state").text()=="新建"||$("#qualityTest_state").text()=="NEW"){
		$("#tjsp").show()
	}else{
		$("#tjsp").hide()
	}
});
//根据上一步选择页面变化生成孔板
function rendrModalDiv(num, row, col) {
	if(row > 8) {
		for(var i = 1; i < num; i++) {
			var clonePlateDiv = $(".plateDiv").eq(0).clone();
			clonePlateDiv.find("#plateNum").attr("plateNum", "p" + (i + 1));
			$("#plateModal").append(clonePlateDiv);
		}
	} else {
		if(num > 1) {
			$(".plateDiv").removeClass("col-xs-12").addClass("col-xs-6");
			for(var i = 1; i < num; i++) {
				var clonePlateDiv = $(".plateDiv").eq(0).clone();
				clonePlateDiv.find("#plateNum").attr("plateNum", "p" + (i + 1));
				$("#plateModal").append(clonePlateDiv);
			}
		}
	}
	$(".plateDiv").find(".btn-sm").click(function() {
		$(this).addClass("active").siblings(".btn-sm").removeClass("active");
	});
}
//把选择的样本放在孔板上
function renderChosedSample() {
	var data = [];
	//在孔板上悬浮提示
	$(".plate div").mouseover(function() {
		var plateDiv = $(this).parents(".plateDiv");
		var plate = plateDiv.find(".plate");
		var order = plateDiv.find(".active").attr("order");
		var that = this;
		if(order == "h") {
			//样本横向排列悬浮提示
			orientationSampleHover(that, plate);
		} else if(order == "z") {
			//样本纵向排列悬浮提示
			portraitSampleHover(that, plate);
		}

	});
	$(".plate div").mouseout(function() {
		$(".plate div").css('box-shadow', "none");
	});
	//在孔板上点击赋值坐标
	var clickIndex=0;
	$(".plate div").click(function() {
		var sampleId = [];
		$("#qualityTestMakeUpdiv .selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
		});
		if(sampleId.length > 0) {
			var plateDiv = $(this).parents(".plateDiv");
			var plate = plateDiv.find(".plate");
			var order = plateDiv.find(".active").attr("order");
			var that = this;
			if(order == "h") {
				//样本横向排列
				data = orientationSampleClick(that, plate);
			} else if(order == "z") {
				//样本纵向排列
				clickIndex++;
				data = portraitSampleClick(that, plate,clickIndex);
			}
			$.ajax({
				type: "post",
				url: ctx + "/experiment/quality/qualityTest/plateLayout.action",
				data: {
					data: data
				},
				success: function(data) {
					qualityTestMakeUpTab.ajax.reload();
					qualityTestMakeUpAfTab.ajax.reload();
				}

			})
		} else {
			if($(this).attr("sId")) {
				$("#plateModal").find(".sel").removeClass("sel");
				$(this).addClass("sel");
				var sid=$(this).attr("sId");
				$("#qualityTestMakeUpAfdiv tbody .icheck").each(function (i,val) {
					$(val).iCheck('uncheck'); 
					if(val.value==sid){
						$(val).iCheck('check'); 
					}
				});
				$("#plateModal .mysample").css("border","1px solid gainsboro");
			} else {
				var plateDiv = $(this).parents(".plateDiv");
				var plate = plateDiv.find(".plate");
				if($("#plateModal").find(".sel").length > 0) {
					var sId = $("#plateModal").find(".sel").attr("sId");
					var sampleCode = $("#plateModal").find(".sel").attr("title");
					$("#plateModal").find(".sel").removeAttr("sid");
					$("#plateModal").find(".sel").removeAttr("title");
					$("#plateModal").find(".sel").css({"background-color":""});
					$("#plateModal").find(".sel").removeClass("sel");
					this.style.backgroundColor = "#007BB6";
					this.setAttribute("title", sampleCode);
					this.setAttribute("sId", sId);
					this.className = "mysample";
					var posId = this.getAttribute("coord");
					var plateNum = plate.parent("#plateNum")[0].getAttribute("platenum");
					data.push(sId + "," + posId + "," + plateNum);
					$.ajax({
						type: "post",
						url: ctx + "/experiment/quality/qualityTest/plateLayout.action",
						data: {
							data: data
						},
						success: function(data) {
							qualityTestMakeUpAfTab.ajax.reload();
						}

					})
				}
			}
		}
	});
}
//样本横向排列悬浮提示
function orientationSampleHover(that, plate) {
	var hh = parseInt(that.getAttribute("h"));
	var cc = $("#qualityTestMakeUpdiv .selected").length;
	var holes = plate.find("div");
	for(var j = 0; j < holes.length; j++) {
		if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
			holes[j].style.boxShadow = "0 0 3px #007BB6";
		}
	}
}
//样本横向排列点击渲染
function orientationSampleClick(that, plate) {
	var hh = parseInt(that.getAttribute("h"));
	var samples = $("#qualityTestMakeUpdiv .selected");
	var cc = samples.length;
	var holes = plate.find("div");
	var positionArr = [];
	var data = [];
	var flag = 0;
	var first;
	for(var j = 0; j < holes.length; j++) {
		if(hh <= holes[j].getAttribute("h") && holes[j].getAttribute("h") < hh + cc) {
			if(flag === 0) {
				first = j;
				flag++;
			}
			var sampleCode = samples.eq(j - first).children("td[savename='code']").text();
			var id = samples.eq(j - first).children("td").eq(0).find(".icheck").val();
			var posId = holes[j].getAttribute("coord");
			var plateNum = plate.parent("#plateNum")[0].getAttribute("platenum");
			//			samples.eq(j-first).children("td[savename='posId']").text(holes[j].getAttribute("coord"));
			data.push(id + "," + posId + "," + plateNum)
			holes[j].setAttribute("title", sampleCode);
			holes[j].setAttribute("sId", id);
			holes[j].className = "mysample";
			holes[j].style.backgroundColor = "#007BB6";
		}
	}
	return data;
}
//样本纵向排列悬浮提示
function portraitSampleHover(that, plate) {
	var zz = parseInt(that.getAttribute("z"));
	var cc = $("#qualityTestMakeUpdiv .selected").length;
	var holes = plate.find("div");
	for(var j = 0; j < holes.length; j++) {
		if(zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
			holes[j].style.boxShadow = "0 0 3px #007BB6";
		}
	}
}
//样本纵向排列点击渲染
function portraitSampleClick(that, plate,clickIndex) {
	var zz = parseInt(that.getAttribute("z"));
	var samples = $("#qualityTestMakeUpdiv .selected");
	var cc = samples.length;
	var holes = plate.find("div");
	var indexY = [];
	var data = [];
	for(var j = 0; j < holes.length; j++) {
		if(zz <= holes[j].getAttribute("z") && holes[j].getAttribute("z") < zz + cc) {
			var y = holes[j].getAttribute("y");
			if(indexY.indexOf(y) == -1) {
				indexY.push(y);
			}
			holes[j].className = "mysample "+clickIndex;
			holes[j].style.backgroundColor = "#007BB6";
		}
	}
	//获取选中孔板的坐标
	var mysample = plate.find("."+clickIndex);
	//按升序排列
	indexY.sort(function(x, y) {
		return x - y;
	});
	var positionArr = [];
	var first = 0;
	indexY.forEach(function(v, j) {
		mysample.each(function(i, val) {
			var yy = val.getAttribute("y");
			if(yy == v) {
				var sampleCode = samples.eq(first).children("td[savename='code']").text();
				var id = samples.eq(first).children("td").eq(0).find(".icheck").val();
				val.setAttribute("title", sampleCode);
				val.setAttribute("sId", id);
				positionArr.push(val.getAttribute("coord"));
				first++;
			}
		});
	});
	//为样本的位置列赋值
	var plateNum = plate.parent("#plateNum")[0].getAttribute("platenum");
	positionArr.forEach(function(val, i) {
		var id = samples.eq(i).children("td").eq(0).find(".icheck").val();
		data.push(id + "," + val + "," + plateNum);
	});
	return data;
}
/** 
 * 根据要求生成要求规格的架子或盒子
 * @param  m => 行
 * @param  n => 列
 */
function createShelf(m, n, element, callback) {
	var m = parseInt(m);
	var n = parseInt(n);
	//$(element).empty();
	var arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
	var tr0 = document.createElement("tr");
	var num = "";
	for(var i = 0; i <= n; i++) {
		num += '<td>' + i + '</td>';
	}
	tr0.innerHTML = num;
	tr0.children[0].innerHTML = "X";
	$(element).append(tr0);
	var h = 0;
	for(var i = 0; i < m; i++) {
		var tr = document.createElement("tr");
		var tds = "";
		var x = i + 1;
		for(var j = 0; j <= n; j++) {
			var jj = j < 10 ? "0" + j : j;
			tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m) + ' coord=' + arr[i] + jj + '></div></td>';
			x += m;
		}
		tr.innerHTML = tds;
		tr.children[0].innerHTML = arr[i];
		$(element).append(tr);
	}
	var divs = $(element).find("div");
	for(var i = 0; i < divs.length; i++) {
		var h = i + 1;
		divs[i].setAttribute("h", h);
	}

	callback();
}

//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/quality/qualityTest/editQualityTest.action?id=" + qualityTest_id+"&bpmTaskId="+$("#bpmTaskId").val()+"&cellType="+$("#cell_type").val();
	});
	//下一步操作
	$("#next").click(function() {
		var trs=$("#qualityTestMakeUpdiv tbody tr");
		if(trs.length>1){
			top.layer.msg(biolims.common.samplenotplaten);
			return false;
		}
		if(trs.length==1&&trs.children("td").length!=1){
			top.layer.msg(biolims.common.samplenotplaten);
			return false;
		}
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/quality/qualityTest/showQualityTestSteps.action?id=" + qualityTest_id+"&bpmTaskId="+$("#bpmTaskId").val();
	});
}
function tjsp() {
	top.layer.confirm(biolims.common.pleaseConfirmSaveBeforeSubmit, {icon: 3, title:biolims.common.prompt}, function(index){
		top.layer.open({
			  title: biolims.common.approvalTask,
			  type:2,
			  anim: 2,
			  area: ['800px','500px']
			  ,btn: biolims.common.selected,
			  content: window.ctx+"/workflow/processinstance/toStartView.action?formName=QualityTest",
			  yes: function(index, layero) {
				 var datas={
							userId : userId,
							userName : userName,
							formId : qualityTest_id,
							title : $("#qualityTest_name").val(),
							formName : "QualityTest"
						}
					ajax("post", "/workflow/processinstance/start.action", datas, function(data) {
						if (data.success) {
							top.layer.msg(biolims.common.submitSuccess);
							window.location=window.ctx+"/experiment/quality/qualityTest/editQualityTest.action?cellType="+cell_type1;
							if (typeof callback == 'function') {
								callback(data);
							}
//							dialogWin.dialog("close");
						} else {
							top.layer.msg(biolims.common.submitFail);
						}
					}, null);
					top.layer.close(index);
				},
				cancel: function(index, layero) {
					top.layer.close(index)
				}
		
		});     
		  top.layer.close(index);
		});
		  
					
}  
//渲染已保存到孔板上的样本
function showPlate() {
	$.ajax({
		type: "post",
		url: ctx + "/experiment/quality/qualityTest/showWellPlate.action",
		data: {
			id: qualityTest_id
		},
		success: function(data) {
			console.log(data);
			var data = JSON.parse(data);
			for(var i = 0; i < data.data.length; i++) {
				var platePoint = $("#plateModal").find("div[platenum='" + data.data[i].counts + "']").find("div[coord='" + data.data[i].posId + "']")[0];
				platePoint.style.backgroundColor = "#007BB6";
				platePoint.setAttribute("title", data.data[i].code);
				platePoint.setAttribute("sId", data.data[i].id);
				platePoint.className = "mysample";
			}
		}
	})
}

//选择添加质控品
function addLeftQuality() {
	top.layer.open({
		title: biolims.common.qualityProduct,
		type: 2,
		area: top.screeProportion,
		btn: biolims.common.selected,
		content: [window.ctx + "/system/quality/qualityProduct/qualityProductSelect.action", ''],
		yes: function(index, layer) {
//			var name = [],
			var QualityTest_id=$("#QualityTest_id").html();
			
			var	id = [];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .selected").each(function(i, v) {
//				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			top.layer.load(4, {shade:0.3}); 
			$.ajax({  
		        url:ctx+"/experiment/quality/qualityTest/saveLeftQuality.action",  
		        data:{
		        	id:qualityTest_id,
			        ids:id
		        },  
		        type:'post',  
		        dataType:"json",  
		        success:function(data){  
		        	window.location.href=ctx+"/experiment/quality/qualityTest/showQualityTestItemTable.action?id="+data.id;
		        	top.layer.closeAll();
		        }  
		});
		},
		
	});
}