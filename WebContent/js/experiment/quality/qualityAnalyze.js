var qualityAnalyzeGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'receiver-id',
		type:"string"
	});
	    fields.push({
		name:'receiver-name',
		type:"string"
	});
	    fields.push({
		name:'receiverDate',
		type:"string"
	});
	    fields.push({
		name:'resultDecide',
		type:"string"
	});
	    fields.push({
		name:'analyzeReason',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'receiver-id',
		hidden:true,
		header:'审核人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'receiver-name',
		header:'审核人',
		
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'receiverDate',
		header:'审核时间',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'resultDecide',
		header:'结果判定',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'analyzeReason',
		header:'分析原因',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/quality/qualityAnalyze/showQualityAnalyzeListJson.action";
	var opts={};
	opts.title="质控分析";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	qualityAnalyzeGrid=gridTable("show_qualityAnalyze_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/quality/qualityAnalyze/editQualityAnalyze.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/quality/qualityAnalyze/editQualityAnalyze.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/quality/qualityAnalyze/viewQualityAnalyze.action?id=' + id;
}
function exportexcel() {
	qualityAnalyzeGrid.title = '导出列表';
	var vExportContent = qualityAnalyzeGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startreceiverDate").val() != undefined) && ($("#startreceiverDate").val() != '')) {
					var startreceiverDatestr = ">=##@@##" + $("#startreceiverDate").val();
					$("#receiverDate1").val(startreceiverDatestr);
				}
				if (($("#endreceiverDate").val() != undefined) && ($("#endreceiverDate").val() != '')) {
					var endreceiverDatestr = "<=##@@##" + $("#endreceiverDate").val();

					$("#receiverDate2").val(endreceiverDatestr);

				}
				
				
				commonSearchAction(qualityAnalyzeGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
