﻿var oldChangeLog;

$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "resultName",
		"title": '检测结果名称',
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "resultName");
			$(td).attr("infoId", rowData['infoId']);
		},
	})
	colOpts.push({
		"data": "testingCriteria",
		"title": '检测标准',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "testingCriteria");
		},
	})
	colOpts.push({
		"data": "resultReferenceValue",
		"title": '质量标准',
		"createdCell": function(td, data) {
			$(td).attr("saveName", "resultReferenceValue");
		},
	})
	colOpts.push({
		"data": "result",
		"title": '检测结果',
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "result");
		},
	})
//	colOpts.push({
//		"data": "infoId",
//		"title": '关联子表',
//		"visible": false,
//		"className": "edit",
//		"createdCell": function(td, data) {
//			$(td).attr("saveName", "infoId");
//		},
//	})
	
//	$.ajax({
//		  type: 'POST',
//		  url:ctx + "/experiment/quality/qualityTest/findQualityTestResultbyId.action",
//		  data:  {
//				id:$("#qualityTest_id").text()
//			},
//		  async:false,
//		  success: function(data) {
//				var data = JSON.parse(data);
//				var list = data.list;
//				if(list.length){
//					for(var j=0;j<list.length;j++){
//						var field = list[j].note;
//						for(var i=0;i<colOpts.length;i++){
//							var data = colOpts[i].data;
//							if(data==field){
//								colOpts[i].visible = true;
//								break;
//							}
//						}
//					}
//				}
//				
//			}
//	
//		});
	
	window.localStorage.clear();
	var visibleItem = $("#templateFieldsItemCode").val();
	if(visibleItem) {
		var visibleItemArr = visibleItem.split(",");
		visibleItemArr.forEach(function(v, i) {
			colOpts.forEach(function(vv, ii) {
				if(v == vv.data) {
					vv.visible=true;
				}
			});
		});
	}
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();

	if(handlemethod == "view"||$("#qualityTest_state").text()!=biolims.common.finish){
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#qualityTestResultItemdiv"))
			}
		});
	}
	var qualityTestResultItemOps = table(true,null,"/experiment/quality/qualityTest/showQualityTestResultItemTableJson.action?itemid="+$("#itemid").val()+"&jiance="+$("#jiance").val(), colOpts, tbarOpts);
	qualityTestResultItemTab = renderData($("#qualityTestResultItemdiv"), qualityTestResultItemOps);
	qualityTestResultItemTab.on('draw', function() {
		oldChangeLog = qualityTestResultItemTab.ajax.json();
	});
	var ly=$("#ly").val();
	if(ly=="Z"){
		$("#save").hide()
	}
//	//批量结果
//	btnChangeDropdown ($('#qualityTestResultItemdiv'),$(".resultsBatchBtn"),[biolims.common.qualified,biolims.common.disqualified],"result");
//	bpmTask($("#bpmTaskId").val());
//	//上一步下一步操作
//	preAndNext();
//	if(handlemethod == "view"||$("#qualityTest_state").text()==biolims.common.finish){
//		settextreadonly();
//		$("#save").hide()
//		$("#sp").hide()
//		$("#finish").hide()
//	}
	
	if($("#sczj").val()=="sczj"){
		$("#save").hide();
	}
});
//下一步流向
function nextFlow() {
	var rows = $("#qualityTestResultItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var productId = "";
	var sampleType="";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
		sampleType = $(k).find("td[savename='dicSampleType-id']").text();
	});
	top.layer.open({
		title:biolims.common.selectNextFlow,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=QualityTest&productId="
					+ productIds+"&sampleType="+sampleType,''],
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
			.eq(1).text();
		var id = $(".layui-layer-iframe", parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
			0).text();
		rows.addClass("editagain");
		rows.find("td[savename='nextFlow']").text(name);
		rows.find("td[savename='nextFlowId']").text(id);

		top.layer.close(index)
		},
	})
}
// 保存
function saveInfoItem() {
	var ele=$("#qualityTestResultItemdiv");
	var changeLog = "实验结果：";
	var data = saveInfoItemjson(ele);
	if(!data){
		return false;
	}
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs="";
	if(changeLog !=""){
		changeLogs=changeLog;
	}
	console.log(data)
//	top.layer.load(4, {shade:0.3});
	$.ajax({
		type: 'post',
		url: '/experiment/quality/qualityTest/saveResultItem.action',
		data: {
			dataJson: data,
			logInfo: changeLogs
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveInfoItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
        var flag =true;
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			// 保存子表
			if(k == "resultName"){
				json["infoId"] = $(tds[j]).attr("infoId");
				continue;
			}
			//判断必填
			/*if(k=="nextFlow"){
				if(!json[k]){
					console.log(json[k]);
					flag=false;
					top.layer.msg(biolims.common.pleaseSelectNextFlow);
					return false;
				}
			}*/
//			if(k=="result"){
//				if(!json[k]){
//					console.log(json[k]);
//					flag=false;
//					top.layer.msg(biolims.common.chooseTaskResult);
//					return false;
//				}
//			}
			/*if(k == "submit") {
				var submit = $(tds[j]).text();
				if(submit == biolims.common.no) {
					json[k] = "0";
				} else if(submit == biolims.common.yes) {
					json[k] = "1";
				}
				continue;
			}*/
//			if(k == "note"){
//				json["sampleInfo-id"] = $(tds[j]).attr("sampleInfo-id");
//				continue;
//			}
//			if(k == "sampleDeteyion-name"){
//				json["sampleDeteyion-id"] = $(tds[j]).attr("sampleDeteyion-id");
//				continue;
//			}
//			if(k == "note"){
//				json["sampleInfo-id"] = $(tds[j]).attr("sampleInfo-id");
//				continue;
//			}
//			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	if(flag){
		return JSON.stringify(data);
	}else{
		return false;
	}
}

//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/quality/qualityTest/showQualityTestSteps.action?id=" + $("#qualityTest_id").text()+"&bpmTaskId="+$("#bpmTaskId").val();
	});
	//上一步操作
	$("#finish").click(function() {
	var paraStr = "formId=" + $("#qualityTest_id").text() +
		"&tableId=QualityTest";
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layero) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: $("#qualityTest_id").text()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.location.reload();
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.closeAll();
			})
		},
		cancel: function(index, layero) {
			top.layer.close(index)
		}

	});
});
	
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
//添加样本类型
function addSampleType() {
	var rows = $("#$({entityName?uncap_first}Resultdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.selectSampleType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/sample/dicSampleType/selectDicSampleTypeOne.action", ''],
		yes: function(index, layer) {
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td").eq(
				0).text();
			rows.addClass("editagain");
			rows.find("td[savename='dicSampleType-name']").attr(
				"dicSampleType-id", id).text(type);
			rows.find("td[savename='dicSampleType-id']").text(id);

			top.layer.close(index)
		},
	})
}
function submitSample(){
	var rows = $("#qualityTestResultItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids=[];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val())
	});
	ajax("post", "/experiment/quality/qualityTest/submitSample.action", {
		ids: ids,
		id: $("#qualityTest_id").text()
	}, function(data) {
		if(data.success) {
			tableRefresh();
		} else {
			top.layer.msg("提交失败");
		}
	}, null)
}
function sp() {
	if(!$("#confirmUser_id").val()){
		top.layer.open({
			title: biolims.sample.pleaseSelectReviewer,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.selected,
			content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
			yes: function(index, layer) {
				var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(0).text();
				$("#confirmUser_name").text(name);
				$("#confirmUser_id").val(id);
				saveInfoItem();
				top.layer.close(index);
			},
			end:function(){
				splc();
			}
		})
	}else{
		splc();
	}
}
function splc(){

	var taskId = $("#bpmTaskId").val();
	var formId = $("##qualityTest_id").text();

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								window.open(window.ctx+"/main/toPortal.action",'_parent');
								top.layer.close(index);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
		}
	});
}
//赋值勾选的浓度
function submitConcentration(){
	var rows = $("#qualityTestResultItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//勾选得chekbox得id数组集合
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val());
	});
	var concentrationList = [];
	$.each(rows, function(i, v) {
		concentrationList.push($(v).find("td[savename=concentration]").text());
	});
	//获取勾选的浓度得第一个值,最后要用的值
	var concentrationValue=concentrationList[0];
	$.each(rows, function(i, v) {
		$(v).find("td[savename=concentration]").text(concentrationValue);
		$(v).addClass("editagain");
	});
}

//赋值勾选的体积
function submitVolume(){
	var rows = $("#qualityTestResultItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	//勾选得chekbox得id数组集合
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find("input[type=checkbox]").val());
	});
	var concentrationList = [];
	$.each(rows, function(i, v) {
		concentrationList.push($(v).find("td[savename=volume]").text());
		//获取勾选的浓度得第一个值,最后要用的值
		var volumeValue=concentrationList[0];
		
		$(v).find("td[savename=volume]").text(volumeValue);
		$(v).addClass("editagain");
	});
}
//downLoadTemp
function downLoadTemp(){
	var rows = $("#qualityTestResultItemdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids = [];
	var codes = [];
	$.each(rows, function(i, k) {
		ids.push($(k).find("input[type=checkbox]").val());
		codes.push($(k).find("td[savename=code]").text());
	});
	if ("" == codes) {
		message(biolims.common.saveDownload);
		return;
	}
	window.open(window.ctx + '/experiment/quality/qualityTest/downLoadTemp.action?codes='+codes+'&ids='+ids);
}