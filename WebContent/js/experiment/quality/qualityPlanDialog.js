var qualityPlanDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'planDate',
		type:"string"
	});
	    fields.push({
		name:'workState',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'名称',
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'planDate',
		header:'计划日期',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'workState',
		header:'工作流状态',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/quality/qualityPlan/showQualityPlanListJson.action";
	var opts={};
	opts.title="质控计划";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setQualityPlanFun(rec);
	};
	qualityPlanDialogGrid=gridTable("show_dialog_qualityPlan_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(qualityPlanDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
