var qualityAnalyzeDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'receiver-id',
		type:"string"
	});
	    fields.push({
		name:'receiver-name',
		type:"string"
	});
	    fields.push({
		name:'receiverDate',
		type:"string"
	});
	    fields.push({
		name:'resultDecide',
		type:"string"
	});
	    fields.push({
		name:'analyzeReason',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'receiver-id',
		header:'审核人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'receiver-name',
		header:'审核人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'receiverDate',
		header:'审核时间',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'resultDecide',
		header:'结果判定',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'analyzeReason',
		header:'分析原因',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/quality/qualityAnalyze/showQualityAnalyzeListJson.action";
	var opts={};
	opts.title="质控分析";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setQualityAnalyzeFun(rec);
	};
	qualityAnalyzeDialogGrid=gridTable("show_dialog_qualityAnalyze_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(qualityAnalyzeDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
