var karyotypingTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'upTime',
		type:"string"
	});
	   fields.push({
		name:'slideCode',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'advice',
		type:"string"
	});
	   fields.push({
		name:'isException',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'resultDetail',
		type:"string"
	});
	   fields.push({
		name:'nextStepId',
		type:"string"
	});
	   fields.push({
		name:'nextStep',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'isCommit',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'karyotypingTask-id',
		type:"string"
	});
	    fields.push({
		name:'karyotypingTask-name',
		type:"string"
	});
	    fields.push({
		name:'productId',
		type:"string"
	});
	    fields.push({
		name:'productName',
		type:"string"
	});
	    fields.push({
		name:'reportInfo-id',
		type:"string"
	});
	    fields.push({
		name:'reportInfo-name',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-fileName',
		type:"string"
	});
	    fields.push({
		name : 'fileNum',
		type : "string"
	});
	    fields.push({
		name : 'flxCellNum',
		type : "string"
	});
	    fields.push({
		name : 'ktCellNum',
		type : "string"
	});
	    fields.push({
    	name : 'tempId',
    	type : "string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'slideCode',
		hidden : true,
		header:'玻片编号',
		sortable:true,
		width:25*6
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		sortable:true,
		width:15*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:15*6
	});

//	var storereCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
//	});
//	var reCob = new Ext.form.ComboBox({
//		store : storereCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	cm.push({
		dataIndex:'flxCellNum',
		hidden : false,
		header:biolims.common.CellSplitPhase+'<font color="red" size="4">*</font>',
		width:17*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'ktCellNum',
		hidden : false,
		header:biolims.common.karyotypeSplitNum+'<font color="red" size="4">*</font>',
		width:17*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.result+'<font color="red" size="4">*</font>',
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
		//editor : reCob,
		//renderer : Ext.util.Format.comboRenderer(reCob)
	});
	cm.push({
		dataIndex:'resultDetail',
		hidden : true,
		header:'结果解释',
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'advice',
		hidden : true,
		header:'临床建议',
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storeexCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no] ]
	});
	var exCob = new Ext.form.ComboBox({
		store : storeexCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isException',
		hidden : true,
		header:'是否异常报告',
		width:20*6,
		editor : exCob,
		renderer : Ext.util.Format.comboRenderer(exCob)
	});
	cm.push({
		dataIndex:'reportInfo-id',
		hidden : true,
		header:'模板ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfo-name',
		hidden : true,
		header:'报告模板',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:'附件ID',
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'template-fileName',
		hidden : true,
		header:'附件名称',
		sortable:true,
		width:15*10
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1',biolims.common.qualified ], [ '0', biolims.common.disqualified] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isQualified+'<font color="red" size="4">*</font>',
		width:15*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'nextStepId',
		hidden : true,
		header:'下一步流向ID',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextStep',
		hidden : false,
		header:biolims.common.nextFlow+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : nextFlowCob
	});

	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isCommit',
		hidden : false,
		header:biolims.common.toSubmit+'<font color="red" size="4">*</font>',
		width:15*6,
		//editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		xtype : 'actioncolumn',
		width : 120,
		hidden : false,
		header : biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : biolims.common.attachment,
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')){
					rec.set("upTime",(new Date()).toString());
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:biolims.common.attachment,layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+ rec.get('id')+"&modelType=karyotypingTaskItem' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: biolims.common.close,
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				}else{
					message(biolims.common.pleaseSaveRecord);
				}
			}
		}]
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'upTime',
		hidden : true,
		header:'附件修改时间',
		width:20*10
	});
	cm.push({
		dataIndex : 'fileNum',
		header : '临时表id',
		hidden : true,
		width : 60
		//	handler: requestScope.fileNum
	});

	cm.push({
		dataIndex:'karyotypingTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'karyotypingTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表',
		width:20*10
	});
	

	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyotyping/karyotypingTask/showKaryotypingTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.karyotypeAnalysisDetail;
	opts.height =  document.body.clientHeight-138;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyotyping/karyotypingTask/delKaryotypingTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				karyotypingTaskItemGrid.getStore().commitChanges();
				karyotypingTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//		text : '选择报告模板',
//		handler : showsampleReportSelectList
//	});
//	opts.tbar.push({
//		text : '预览报告',
//		handler : createReport
//	});
//	opts.tbar.push({
//		text : "批量报告",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_isReport_div"), "批量报告", null, {
//				"确定" : function() {
//					var records = karyotypingTaskItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var isReport = $("#isReport").val();
//						karyotypingTaskItemGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("isException", isReport);
//						});
//						karyotypingTaskItemGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : biolims.common.vim,
		handler : function() {
			var records = karyotypingTaskItemGrid.getSelectRecord();
			if (records.length==1) {
				$.each(records, function(i, obj) {
				if(obj.get("result")!=null && obj.get("result")!=""){
					$("#result1").val(obj.get("result"));
				}
//				if(obj.get("lcjy")!=null && obj.get("lcjy")!=""){
//					$("#lc").val(obj.get("lcjy"));
//				}
//				if(obj.get("jg")!=null && obj.get("jg")!=""){
//					$("#jg").val(obj.get("jg"));
//				}
//				if(obj.get("ycbg")!=null && obj.get("ycbg")!=""){
//					$("#yc").val(obj.get("ycbg"));
//				}
				});
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_area1_div"),biolims.common.vim, null, {
					"确定" : function() {
						//var records = karyotypingTaskItemGrid.getSelectRecord();
						//if (records.length==1) {
							var result = $("#result1").val();
							karyotypingTaskItemGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("result", result);
							});
							karyotypingTaskItemGrid.startEditing(0, 0);
						/*}else if(records.length==0){
							message("请选择要编辑的样本！");
						}else{
							message("只能选择一条样本编辑！");
						}*/
						$(this).dialog("close");
					}
				}, true, options);
			}else if(records.length==0){
				message(biolims.common.selectEditSample);
			}else{
				message(biolims.common.onlyChooseOneSampleEdit);
			}
		}
	});
//	opts.tbar.push({
//		text : "批量编辑数据",
//		handler : function() {
//			var records = karyotypingTaskItemGrid.getSelectRecord();
//			if (records && records.length>0) {
//				var options = {};
//				options.width = 400;
//				options.height = 300;
//				loadDialogPage($("#bat_area_div"), "批量编辑数据", null, {
//					"确定" : function() {
//						//var records = karyotypingTaskItemGrid.getSelectRecord();
//						//if (records && records.length>0) {
//							var result = $("#result").val();
//							karyotypingTaskItemGrid.stopEditing();
//							$.each(records, function(i, obj) {
//								if(result!=null && result!=""){
//									obj.set("result", result);
//								}
//							});
//							karyotypingTaskItemGrid.startEditing(0, 0);
//						//}else{
//						//	message("请选择要编辑的样本！");
//						//}
//						$(this).dialog("close");
//					}
//				}, true, options);
//			}else{
//				message("请选择要编辑的样本！");
//			}
//		}
//	});
	
	opts.tbar.push({
		text : biolims.common.BatchIsQualified,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_isGood_div"),biolims.common.BatchIsQualified, null, {
				"确定" : function() {
					var records = karyotypingTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isGood = $("#isGood").val();
						karyotypingTaskItemGrid .stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isGood", isGood);
						});
						karyotypingTaskItemGrid .startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "下一步流向",
//		handler : function() {
//			var records = karyotypingTaskItemGrid.getSelectRecord();
//			if(records.length>0){
//					loadTestNextFlowCob();
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_isCommit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = karyotypingTaskItemGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var isCommit = $("#isCommit").val();
//						karyotypingTaskItemGrid .stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("isCommit", isCommit);
//						});
//						karyotypingTaskItemGrid .startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text :biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text :biolims.report.checkFile,
		handler : loadPic
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	
	karyotypingTaskItemGrid=gridEditTable("karyotypingTaskItemdiv",cols,loadParam,opts);
	$("#karyotypingTaskItemdiv").data("karyotypingTaskItemGrid", karyotypingTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(karyotypingTaskItemGrid);
	var id=$("#karyotypingTask_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/karyotyping/karyotypingTask/saveKaryotypingTaskItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					karyotypingTaskItemGrid.getStore().commitChanges();
					karyotypingTaskItemGrid.getStore().reload();
					message(biolims.common.saveSuccess);
				} else {
					message(biolims.common.saveFailed);
				}
			}, null);
		}
	}else{
		message(biolims.common.noData2Save);
	}
	  }else{
		  message(biolims.storage.infoChange);
	}
}
//保存
/*function save1(){
	var id=$("#karyotypingTask_id").val();
	var itemJson = commonGetModifyRecords(snpAnalysisItemGrid);
	if(itemJson.length>0){
		ajax("post", "/experiment/karyotyping/karyotypingTask/saveKaryotypingTaskResult.action", {
			itemDataJson : itemJson,id : id
		}, function(data) {
			if (data.success) {
				karyotypingTaskItemGrid.getStore().commitChanges();
				karyotypingTaskItemGrid.getStore().reload();
				message("提交成功！");
			} else {
				message("提交失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}*/
function loadPic(){
	var ids="";
	var idStr="";
	var model="karyotypingTaskItem";
	var selectGrid=karyotypingTaskItemGrid.getSelectionModel().getSelections();
	var getGrid=karyotypingTaskItemGrid.store;
	if(getGrid.getCount()>0){
		if (selectGrid.length > 0) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				ids+="'"+obj.get("id")+"',";
			});
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}else{
			//没有勾选明细查看全部附件
			var selectGrid=karyotypingTaskItemGrid.store;
			for(var j=0;j<selectGrid.getCount();j++){
				ids+="'"+selectGrid.getAt(j).get("id")+"',";
			}
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}
	}else{
		message(biolims.common.listNotData);
	}
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = karyotypingTaskItemGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向",  "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=KaryotypingTask&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = karyotypingTaskItemGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextStepId", b.get("id"));
							obj.set("nextStep", b.get("name"));
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = karyotypingTaskItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextStepId", b.get("id"));
				obj.set("nextStep", b.get("name"));
			});
		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadNextFlow.dialog("close");
}
	

//选择报告模板
function showsampleReportSelectList() {
	var selected=karyotypingTaskItemGrid.getSelectionModel().getSelections();
	var productId="";
	var type="0";
	if(selected.length>0){
		if(selected.length>1){
			var productIds = new Array();
			$.each(selected, function(j, k) {
				productIds[j]=k.get("productId");
			});
			for(var i=0;i<selected.length;i++){
				if(i!=0 && productIds[i]!=productIds[i-1]){
					message("检测项目不同！");
					return;
				}else{
					productId=productIds[i];
				}
			}
		}else{
			$.each(selected, function(i, a) {
				productId=a.get("productId");
			});
		}
	}else{
		productId="";
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
	var showsampleReportSelectList = new Ext.Window(
			{
				id : 'showsampleReportSelectList',
				modal : true,
				title : biolims.common.selectTemplate,
				layout : 'fit',
				width : 480,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sysmanage/report/selReportTemplateList.action?productId="+productId+"&type="+type+"' frameborder='0' width='780' height='500' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						showsampleReportSelectList.close();
					}
				} ]
			});
	showsampleReportSelectList.show();
}

function setReportFun(rec) {
	var selected=karyotypingTaskItemGrid.getSelectionModel().getSelections();
	if(selected.length>0){
		$.each(selected,function(i,b){
			b.set("reportInfo-id",rec.get('id'));
			b.set("reportInfo-name",rec.get('name'));
			b.set("template-id",rec.get('attach-id'));
			b.set("template-fileName",rec.get('attach-fileName'));
		});
	}else{
		message("请选择要出报告的样本！");
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
}

//生成并下载报告文件
function createReport(ids) {
	var selected=karyotypingTaskItemGrid.getSelectionModel().getSelections();
	var ids = [];
	if(selected.length>0){
		var flag=true;
		$.each(selected,function(i,b){
			if(b.get("template-id")=="" || b.get("template-id")==null
					|| b.get("template-id")==undefined){
				flag=false;
				message("请选择报告模板！");
			}else{
				flag=true;
			}
		});
		if(flag){
			for ( var i = 0; i < selected.length; i++) {
				if (!selected[i].isNew) {
					ids.push(selected[i].get("id"));
				}
			}
			ajax("post", "/experiment/karyotyping/karyotypingTask/createReportFile.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					karyotypingTaskItemGrid.getStore().commitChanges();
					karyotypingTaskItemGrid.getStore().reload();
					//message("生成报告成功！");
				} else {
					message("预览报告失败！");
				}
			}, null);
			downFiles();
		}
		//setTimeout(function() {
		//	downFiles();
		//},2000);
	}else{
		message("请选择要预览的样本！");
		return;
	}
}

//下载文件
function downFiles(){
	var selectGrid=karyotypingTaskItemGrid.getSelectionModel().getSelections();
	if(selectGrid.length>0){
		$.each(selectGrid, function(i, obj) {
			var fileName="PDF"+obj.get("code");
			window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'.pdf','','');
		});
	}
}

//提交样本
function submitSample(){
	var id=$("#karyotypingTask_id").val();  
	if(karyotypingTaskItemGrid.getModifyRecord().length > 0){
		message(biolims.common.pleaseSaveRecord);
		return;
	}
	var record = karyotypingTaskItemGrid.getSelectionModel().getSelections();
	var flg=true;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(record[i].get("isCommit")==null
					|| record[i].get("isCommit")==""){
				flg=false;
			}
			if(record[i].get("result")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
			if(record[i].get("nextStepId")==""){
				message(biolims.common.nextStepNotEmpty);
				return;
			}
		}
		if(!flg){
			var loadMarsk = new Ext.LoadMask(Ext.getBody(),
					{
					        msg :biolims.common.beingProcessed,
					        removeMask : true// 完成后移除
					    });
			loadMarsk.show();
			var records = [];
			for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
			}
			ajax("post", "/experiment/karyotyping/karyotypingTask/submitSample.action", {
				id : id,
				ids : records
			}, function(data) {
				if (data.success) {
					loadMarsk.hide();
					karyotypingTaskItemGrid.getStore().commitChanges();
					karyotypingTaskItemGrid.getStore().reload();
					message(biolims.common.submitSuccess);
				} else {
					loadMarsk.hide();
					message(biolims.common.submitFail);
				}
			}, null);
		}else{
			message(biolims.common.noData2Submit);
		}
	}else{
		message(biolims.common.pleaseChooseSamples);
	}
}