var karyotypingTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});	
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	fields.push({
		name:'slideCode',
		type:"string"
	});
	//缴费状态
	fields.push({
		name : 'chargeNote',
		type : "string"
	});
	fields.push({
		name:'seqDate',
		type:"date",
		dateFormat:"Y-m-d"
	});

	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6,
		
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6,
		
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
		
	});
	cm.push({
		dataIndex:'slideCode',
		hidden : true,
		header:biolims.common.slideCode,
		width:20*6
		
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	var storechargeNoteCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.payPayment ], [ '2', biolims.common.alreadyPaid ], [ '3', biolims.common.settlementSettled ], 
		         [ '4', biolims.sample.kyPro ], [ '5', biolims.common.free] ]
	});
	var chargeNoteCob = new Ext.form.ComboBox({
		store : storechargeNoteCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'chargeNote',
		hidden : false,
		header:biolims.common.payStatus,
		width:20*6,
		//editor : chargeNoteCob,
		renderer : Ext.util.Format.comboRenderer(chargeNoteCob)
	});
	cm.push({
		dataIndex:'seqDate',
		hidden : false,
		header:biolims.sequencing.sequencingDate,
		width:20*6,
		sortable:true,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
	});
//	cm.push({
//		dataIndex:'note',
//		hidden : false,
//		header:'备注',
//		width:20*6,
//		
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyotyping/karyotypingTask/showKaryotypingTaskTempListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.karyotypeWaitAnalysisSample;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyotyping/karyotypingTask/delKaryotypingTaskTemp.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.addToTask,
		handler : addItem
	});
	
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$(".jquery-ui-warning").html(biolims.common.longlongagolong1);
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message(biolims.common.fillBarcode);
								return;
							}
							var array = positions.split("\n");
							var records = karyotypingTaskTempGrid.getAllRecord();
							var store = karyotypingTaskTempGrid.store;

							var isOper = true;
							var buf = [];
							karyotypingTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							karyotypingTaskTempGrid.getSelectionModel().selectRows(buf);
							if(isOper==false){
								message(biolims.common.samplecodeComparison);
								
							}else{
								addItem();
							}
							karyotypingTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	karyotypingTaskTempGrid=gridEditTable("karyotypingTaskTempdiv",cols,loadParam,opts);
	$("#karyotypingTaskTempdiv").data("karyotypingTaskTempGrid", karyotypingTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
////添加任务到子表
//function addItem(){
//	var selectRecord=karyotypingTaskTempGrid.getSelectionModel();
//	var selRecord=karyotypingTaskItemGrid.store;
//	if (selectRecord.getSelections().length > 0) {
//		$.each(selectRecord.getSelections(), function(i, obj) {
//			var isRepeat = false;
//			for(var j=0;j<selRecord.getCount();j++){
//				var oldv = selRecord.getAt(j).get("fileNum");
//				if(oldv == obj.get("id")){
//					isRepeat = true;
//					message("有重复的数据，请重新选择！");
//					return;					
//				}
//			}
//		
//			if(!isRepeat){
//			var ob = karyotypingTaskItemGrid.getStore().recordType;
//			karyotypingTaskItemGrid.stopEditing();
//			var p = new ob({});
//			p.isNew = true;
//			
//			p.set("fileNum",obj.get("id"));
//			p.set("code",obj.get("code"));
//			p.set("sampleCode",obj.get("sampleCode"));
//			p.set("state","1");
//			var productId=obj.get("productId");
//			if(productId.substring(productId.length-1,productId.length)==","){
//				p.set("productId",productId.substring(0,productId.length-1));
//			}else{
//				p.set("productId",productId);
//			}
//			var productName=obj.get("productName");
//			if(productName.substring(productName.length-1,productName.length)==","){
//				p.set("productName",productName.substring(0,productName.length-1));
//			}else{
//				p.set("productName",productName);
//			}
//			p.set("note",obj.get("note"));
//			p.set("slideCode",obj.get("slideCode"));
//			karyotypingTaskItemGrid.getStore().add(p);
//			count++;
//			karyotypingTaskItemGrid.startEditing(0, 0);
//			
//		}
//			
//	});
//	}else{
//		message("请选择样本！");
//	}
//	
//}


//添加任务到子表
function addItem(){
	var selRecord = karyotypingTaskTempGrid.getSelectionModel().getSelections();//从左边获取数据
	var getRecord = karyotypingTaskItemGrid.store;//填充到当前的明细中
	var count=1;
	var max=0;
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var isRepeat = false;
			for(var j=0; j<getRecord.getCount();j++){
				var getData = getRecord.getAt(j).get("fileNum");
				if(getData==obj.get("id")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					break;
				}
			}
			//获取最大排序号
			for(var i=0; i<getRecord.getCount();i++){
				var a=getRecord.getAt(i).get("experimentCode");
				if(a>max){
					max=a;
				}
			}
			if(!isRepeat){
				var ob = karyotypingTaskItemGrid.getStore().recordType;
				karyotypingTaskItemGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("experimentCode",Number(max)+count);
				
				p.set("fileNum",obj.get("id"));
				p.set("tempId",obj.get("id"));
				p.set("code",obj.get("code"));
				p.set("sampleCode",obj.get("sampleCode"));
				p.set("sampleType",obj.get("sampleType"));
				p.set("state","1");
				p.set("isGood","1");
				var productId=obj.get("productId");
				if(productId.substring(productId.length-1,productId.length)==","){
					p.set("productId",productId.substring(0,productId.length-1));
					ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
						model : "KaryotypingTask",productId:productId.substring(0,productId.length-1)
					}, function(data) {
						p.set("nextStepId",data.dnextId);
						p.set("nextStep",data.dnextName);
					}, null);
				}else{
					p.set("productId",productId);
					ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
						model : "KaryotypingTask",productId:productId
					}, function(data) {
						p.set("nextStepId",data.dnextId);
						p.set("nextStep",data.dnextName);
					}, null);
				}
				var productName=obj.get("productName");
				if(productName.substring(productName.length-1,productName.length)==","){
					p.set("productName",productName.substring(0,productName.length-1));
				}else{
					p.set("productName",productName);
				}
				p.set("note",obj.get("note"));
				p.set("slideCode",obj.get("slideCode"));
				//p.set("result","1");
				karyotypingTaskItemGrid.getStore().add(p);
			}
		});	
		karyotypingTaskItemGrid.startEditing(0,0);
	}
}
