$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#karyotypingTask_state").val();
	if(id=="3"){
		load("/experiment/karyotyping/karyotypingTask/showKaryotypingTaskTempList.action", null, "#karyotypingTaskTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#karyotypingTaskTemppage").remove();
	}
});	
function add() {
	window.location = window.ctx + "/experiment/karyotyping/karyotypingTask/editKaryotypingTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/karyotyping/karyotypingTask/showKaryotypingTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("KaryotypingTask", {
		userId : userId,
		userName : userName,
		formId : $("#karyotypingTask_id").val(),
		title : $("#karyotypingTask_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp").click(function() {
	//var taskName=$("#taskName").val();
	//if(taskName=="核型二审"){
	if (karyotypingTaskItemGrid.getAllRecord().length > 0) {
		var selRecord = karyotypingTaskItemGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			var oldv = selRecord.getAt(j).get("isCommit");
			if(selRecord.getAt(j).get("isGood")==""){
				message("请填写是否合格！");
				return;
			}
			if(oldv!=1){
				message("有样本未提交！");
				return;
			}
			/*if(selRecord.getAt(j).get("nextStep")==""){
				message("请填写下一步！");
				return;
			}*/
		}
		if(karyotypingTaskItemGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
		completeTask($("#karyotypingTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}else{
		completeTask($("#karyotypingTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
	//}else{
	//	completeTask($("#karyotypingTask_id").val(), $(this).attr("taskId"), function() {
	//		document.getElementById('toolbarSaveButtonFlag').value = 'save';
	//		location.href = window.ctx + '/dashboard/toDashboard.action';
	//	});
	//}
});






function save() {
	if(checkSubmit()==true){
		var karyotypingTaskItemDivData = $("#karyotypingTaskItemdiv").data("karyotypingTaskItemGrid");
		document.getElementById('karyotypingTaskItemJson').value = commonGetModifyRecords(karyotypingTaskItemDivData);
		//var karyotypingTaskTempDivData = $("#karyotypingTaskTempdiv").data("karyotypingTaskTempGrid");
		//document.getElementById('karyotypingTaskTempJson').value = commonGetModifyRecords(karyotypingTaskTempDivData);
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/karyotyping/karyotypingTask/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
			msg : '正在处理，请稍候。。。。。。',
			removeMask : true// 完成后移除
				});
		loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/karyotyping/karyotypingTask/copyKaryotypingTask.action?id=' + $("#karyotypingTask_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#karyotypingTask_id").val() + "&tableId=karyotypingTask");
//}

$("#toolbarbutton_status").click(function(){
	if ($("#karyotypingTask_id").val()){
		commonChangeState("formId=" + $("#karyotypingTask_id").val() + "&tableId=KaryotypingTask");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#karyotypingTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#karyotypingTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function(){
		var tabs=new Ext.TabPanel({
			id:'tabs11',
			renderTo:'maintab',
			height:document.body.clientHeight-30,
			autoWidth:true,
			activeTab:0,
			margins:'0 0 0 0',
			items:[{
				title:biolims.common.karyotypeAnalysis,
				contentEl:'markup'
			} ]
		});
	});
	load("/experiment/karyotyping/karyotypingTask/showKaryotypingTaskItemList.action", {
		id : $("#karyotypingTask_id").val()
	}, "#karyotypingTaskItempage");
	load("/experiment/karyotyping/karyotypingTask/showKaryotypingTaskTempList.action", {
		id : $("#karyotypingTask_id").val()
	}, "#karyotypingTaskTemppage");
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

var item = menu.add({
	text: '复制'
});
item.on('click', editCopy);


function ProducerTaskFun(){
	var win = Ext.getCmp('ProducerTaskFun');
	if (win) {win.close();}
	var ProducerTaskFun= new Ext.Window({
	id:'ProducerTaskFun',modal:true,title:'选择制片上机',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/experiment/producer/producerTask/producerTaskSelect.action?flag=ProducerTaskFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 ProducerTaskFun.close(); }  }]  });     
	 ProducerTaskFun.show(); 
 }

function setProducerTaskFun(rec){
	var code=$("#karyotypingTask_producerTask").val();
	if(code==""){
		document.getElementById('karyotypingTask_producerTask').value=rec.get('id');
		document.getElementById('karyotypingTask_producerTask_name').value=rec.get('name');
		var win = Ext.getCmp('ProducerTaskFun');
		if(win){win.close();}
		var id=rec.get('id');
		ajax("post", "/experiment/producer/producerTask/setItemToKaryotypingTask.action", {
			code : id,
			}, function(data) {
				if (data.success) {
					var ob = karyotypingTaskItemGrid.getStore().recordType;
					karyotypingTaskItemGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("code",obj.code);
						p.set("slideCode",obj.slideCode);
						p.set("sampleCode",obj.sampleCode);
						p.set("productId",obj.productId);
						p.set("productName",obj.productName);
						p.set("note",obj.note);
						karyotypingTaskItemGrid.getStore().add(p);							
					});
					karyotypingTaskItemGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
	}else{
		if(rec.get('id')==code){
			var win = Ext.getCmp('ProducerTaskFun');
			if(win){win.close();}
		}else{
			var ob1 = karyotypingTaskItemGrid.store;
			if (ob1.getCount() > 0) {
			for(var j=0;j<ob1.getCount();j++){
				var oldv = ob1.getAt(j).get("id");
				//根据ID删除
				if(oldv!=null){
					ajax("post", "/experiment/karyotyping/karyotypingTask/delKaryotypingTaskItemOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null);
				}else{								
					karyotypingTaskItemGrid.store.removeAll();
				}
			}
				karyotypingTaskItemGrid.store.removeAll();
			}
			document.getElementById('karyotypingTask_producerTask').value=rec.get('id');
			document.getElementById('karyotypingTask_producerTask_name').value=rec.get('name');
			var win = Ext.getCmp('ProducerTaskFun');
			if(win){win.close();}
			var id=rec.get('id');
			ajax("post", "/experiment/producer/producerTask/setItemToKaryotypingTask.action", {
				code : id,
				}, function(data) {
					if (data.success) {
						var ob = karyotypingTaskItemGrid.getStore().recordType;
						karyotypingTaskItemGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.code);
							p.set("slideCode",obj.slideCode);
							p.set("sampleCode",obj.sampleCode);
							p.set("productId",obj.productId);
							p.set("productName",obj.productName);
							p.set("note",obj.note);
							karyotypingTaskItemGrid.getStore().add(p);							
						});
						karyotypingTaskItemGrid.startEditing(0, 0);		
					} else {
						message("获取明细数据时发生错误！");
					}
				}, null); 
		}
	}
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = karyotypingTaskItemGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}

			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "KaryotypingTask",id : $("#karyotypingTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

