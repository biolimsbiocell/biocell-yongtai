﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#outTaskReceive_id").val();
	var state = $("#outTaskReceive_state").val();
	if(id=="" || state == "3"){
		var type="2";
		load("/experiment/out/outTaskReceive/showOutTaskReceiveItemListTo.action", {type:type}, "#OutTaskReceviceLeftPage");
		$("#markup").css("width","75%");
	}
});

function add() {
	window.location = window.ctx + "/experiment/out/outTaskReceive/editOutTaskReceive.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/out/outTaskReceive/showOutTaskReceiveList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#outTaskReceive", {
					userId : userId,
					userName : userName,
					formId : $("#outTaskReceive_id").val(),
					title : $("#outTaskReceive_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {
		completeTask($("#outTaskReceive_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var outTaskReceiveItemDivData = $("#outTaskReceiveItemdiv").data("outTaskReceiveItemGrid");
		document.getElementById('outTaskReceiveItemJson').value = commonGetModifyRecords(outTaskReceiveItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/out/outTaskReceive/save.action";
		form1.submit();	
		}
}	

function editCopy() {
	window.location = window.ctx + '/experiment/out/outTaskReceive/copyOutTaskReceive.action?id=' + $("#outTaskReceive_id").val();
}

$("#toolbarbutton_status").click(function(){
	var selRecord = outTaskReceiveItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var result = selRecord.getAt(j).get("method");
		if(result==""){
			message("处理结果不能为空！");
			return;
		}
	}
	if ($("#outTaskReceive_id").val()){
		commonChangeState("formId=" + $("#outTaskReceive_id").val() + "&tableId=OutTaskReceive");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#outTaskReceive_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#outTaskReceive_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}

$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'样本接收',
	    	   contentEl:'markup'
	       } ]
	   });
});

load("/experiment/out/outTaskReceive/showOutTaskReceiveItemList.action", {
				id : $("#outTaskReceive_id").val()
			}, "#outTaskReceiveItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);