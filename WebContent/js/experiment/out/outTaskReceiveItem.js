﻿var outTaskReceiveItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
    fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'reportDate',
		type:"string"
	});
    fields.push({
		name:'inspectDate',
		type:"string"
	});
    fields.push({
		name:'phone',
		type:"string"
	});
    fields.push({
		name:'idCard',
		type:"string"
	});
    fields.push({
		name:'orderId',
		type:"string"
	});
    fields.push({
		name:'sampleName',
		type:"string"
	});
    fields.push({
		name:'sampleId',
		type:"string"
	});
    fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
    fields.push({
		name:'stateName',
		type:"string"
	});
    fields.push({
		name:'method',
		type:"string"
	});
    fields.push({
		name:'reason',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'outTaskReceive-id',
		type:"string"
	});
    fields.push({
		name:'outTaskReceive-name',
		type:"string"
	});

	fields.push({
		name:'classify',
		type:"string"
	});
	fields.push({
		name:'sampleType',
		type:"string"
	});
	fields.push({
		name:'sampleNum',
		type:"string"
	});
	fields.push({
		name:'tempId',
		type:"string"
	});
	fields.push({
		name:'labCode',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:30*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目Id',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'病人姓名',
		width:20*6
	});
	var method = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '合格'
			},{
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'method',
		hidden : false,
		header:'处理结果',
		width:15*6,
		renderer: Ext.util.Format.comboRenderer(method),editor: method
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证号',
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:30*6
	});
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样日期',
		width:30*6
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:30*6
	});
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:'储位',
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sequenceFun',
		hidden : true,
		header:'检测方法',
		width:30*6
	});	
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单Id',
		width:30*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:'状态',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});	
	cm.push({
		dataIndex:'reason',
		hidden : false,
		header:'原因',
		width:30*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'outTaskReceive-name',
		hidden : true,
		header:'相关主表',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'outTaskReceive-id',
		hidden : true,
		header:'相关主表ID',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务',
		width:30*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/out/outTaskReceive/showOutTaskReceiveItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title="样本接收明细";
	opts.height =  document.body.clientHeight-140;
	opts.tbar = [];
	if($("#outTaskReceive_stateName").val()!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/out/outTaskReceive/delOutTaskReceiveItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				outTaskReceiveItemGrid.getStore().commitChanges();
				outTaskReceiveItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : "批量结果",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_method_div"), "批量结果", null, {
				"确定" : function() {
					var records = outTaskReceiveItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var method = $("#method").val();
						outTaskReceiveItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("method", method);
						});
						outTaskReceiveItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});	
	}
	outTaskReceiveItemGrid=gridEditTable("outTaskReceiveItemdiv",cols,loadParam,opts);
	$("#outTaskReceiveItemdiv").data("outTaskReceiveItemGrid", outTaskReceiveItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});