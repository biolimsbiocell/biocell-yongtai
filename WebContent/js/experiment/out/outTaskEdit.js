﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state = $("#outTask_state").val();
	var stateName = $("#outTask_stateName").val();
	if(state =="3"||stateName==biolims.common.toModify){
		load("/experiment/out/outTask/showOutTaskTempList.action", null, "#outTaskTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#outTaskTempPage").remove();
	}
	var mttf = $("#outTask_template_templateFieldsCode").val();
	var mttfItem = $("#outTask_template_templateFieldsItemCode").val();
	reloadOutTaskItem(mttf, mttfItem);

});	

function reloadOutTaskItem(mttf, mttfItem) {
	setTimeout(
			function() {
				if (mttf == null || mttf == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = outTaskItemGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttf.indexOf(colName) >= 0) {
							colArray2.push(colName);
							outTaskItemGrid.getColumnModel().setHidden(i,
									false);
						} else {
							outTaskItemGrid.getColumnModel().setHidden(i,
									true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
				if (mttfItem == null || mttfItem == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = outTaskResultGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttfItem.indexOf(colName) >= 0) {
							colArray2.push(colName);
							outTaskResultGrid.getColumnModel().setHidden(
									i, false);
						} else {
							outTaskResultGrid.getColumnModel().setHidden(
									i, true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
			}, 1000);
}


function add() {
	window.location = window.ctx + "/experiment/out/outTask/editOutTask.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/out/outTask/showOutTaskList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});	

$("#toolbarbutton_print").click(function(){
	var url = '__report=OutTask.rptdesign&id=' + $("#outTask_id").val();
	commonPrint(url);
});

setTimeout(function() {
	if($("#outTask_template").val()){
		var maxNum = $("#outTask_maxNum").val();
		if(maxNum>0){
			load("/storage/container/sampleContainerTest.action", {
				id : $("#outTask_template").val(),
				type :$("#type").val(),
				maxNum : 0
			}, "#3d_image0", function(){
				if(maxNum>1){
					load("/storage/container/sampleContainerTest.action", {
						id : $("#outTask_template").val(),
						type :$("#type").val(),
						maxNum : 1
					}, "#3d_image1", null);
				}
			});
		}
	}
}, 100);

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("OutTask", {
					userId : userId,
					userName : userName,
					formId : $("#outTask_id").val(),
					title : $("#outTask_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {	
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#outTask_id").val();	
	var options = {};
	options.width = 929;
	options.height = 534;	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}
	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null, "审批任务", url, {
		"确定" : function() {			
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();
				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};											
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);				
			}else if(operVal=="1"){
				if(taskName=="外包实验"){
				var codeList = new Array();
				var codeList1 = new Array();
				var selRecord1 = outTaskItemGrid.store;
				var flag=true;
				var flag1=true;
				if (outTaskResultGrid.getAllRecord().length > 0) {
					var selRecord = outTaskResultGrid.store;
					for(var j=0;j<selRecord.getCount();j++){
						var oldv = selRecord.getAt(j).get("submit");
						codeList.push(selRecord.getAt(j).get("outTaskCode"));
						if(oldv==""){
							flag=false;
							message("有样本未提交！");
							return;
						}
						if(selRecord.getAt(j).get("nextFlowId")==""){
							message("有下一步未填写！");
							return;
						}
					}
					for(var j=0;j<selRecord1.getCount();j++){						
						if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
							codeList1.push(selRecord1.getAt(j).get("code"));
							flag1=false;
							message("有样本未完成实验！");
						};
					}
					if(outTaskResultGrid.getModifyRecord().length > 0){
						message("请先保存记录！");
						return;
					}
					if(flag1){
							var myMask1 = new Ext.LoadMask(Ext.getBody(), {
								msg : '请等待...'
							});
							myMask1.show();
							Ext.MessageBox.confirm("确认", "请确认保存修改项后进行办理!", function(button, text) {
								if (button == "yes") {
									var paramData =  {};
									paramData.oper = $("#oper").val();
									paramData.info = $("#opinion").val();
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									};																		
									_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
										location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
									}, dialogWin);
																										}
							});
							myMask1.hide();
					}else{
						message("有样本未完成实验！样本有："+codeList1);
					}
				}else{
					message("请填加任务明细并保存！");
					return;
				}
				}else{
					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();
					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};										
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				}
			}				
		},
		"查看流程图" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var outTaskItemDivData = $("#outTaskItemdiv").data("outTaskItemGrid");
		document.getElementById('outTaskItemJson').value = commonGetModifyRecords(outTaskItemDivData);
	    var outTaskResultDivData = $("#outTaskResultdiv").data("outTaskResultGrid");
		document.getElementById('outTaskResultJson').value = commonGetModifyRecords(outTaskResultDivData);
		var outTaskTemplateItemDivData = $("#outTaskTemplateItemdiv").data("outTaskTemplateItemGrid");
		document.getElementById('outTaskTemplateItemJson').value = commonGetModifyRecords(outTaskTemplateItemDivData);
		var outTaskTemplateReagentDivData = $("#outTaskTemplateReagentdiv").data("outTaskTemplateReagentGrid");
		document.getElementById('outTaskTemplateReagentJson').value = commonGetModifyRecords(outTaskTemplateReagentDivData);
		var outTaskTemplateCosDivData = $("#outTaskTemplateCosdiv").data("outTaskTemplateCosGrid");
		document.getElementById('outTaskTemplateCosJson').value = commonGetModifyRecords(outTaskTemplateCosDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/out/outTask/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();	
		}
}	

function saveItem() {
	var id = $("#outTask_id").val();
	if (!id) {
		return;
	}
	
	// Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	 var outTaskItemDivData = $("#outTaskItemdiv").data("outTaskItemGrid");
		outTaskItemJson= commonGetModifyRecords(outTaskItemDivData);
	    var outTaskResultDivData = $("#outTaskResultdiv").data("outTaskResultGrid");
		outTaskResultJson= commonGetModifyRecords(outTaskResultDivData);
		var outTaskTemplateItemDivData = $("#outTaskTemplateItemdiv").data("outTaskTemplateItemGrid");
		outTaskTemplateItemJson= commonGetModifyRecords(outTaskTemplateItemDivData);
		var outTaskTemplateReagentDivData = $("#outTaskTemplateReagentdiv").data("outTaskTemplateReagentGrid");
		outTaskTemplateReagentJson= commonGetModifyRecords(outTaskTemplateReagentDivData);
		var outTaskTemplateCosDivData = $("#outTaskTemplateCosdiv").data("outTaskTemplateCosGrid");
		outTaskTemplateCosJson= commonGetModifyRecords(outTaskTemplateCosDivData);
	
	ajax("post", "/experiment/out/outTask/saveAjax.action", {
		outTaskItemJson:outTaskItemJson,
		outTaskResultJson:outTaskResultJson,
		outTaskTemplateReagentJson:outTaskTemplateReagentJson,
		outTaskTemplateItemJson:outTaskTemplateItemJson,
		outTaskTemplateCosJson:outTaskTemplateCosJson,
		id : id
	}, function(data) {
		if (data.success) {
			//message("保存成功！");
			if(data.equip!=0&&typeof(data.equip)!='undefined'){
				message(data.equip+"条设备数据保存成功！");
			}
			if(data.re!=0&&typeof(data.re)!='undefined'){
				message(data.re+"条原辅料数据保存成功！");
			}
			outTaskItemDivData.getStore().reload();
			outTaskResultDivData.getStore().reload();
			outTaskTemplateItemDivData.getStore().reload();
			outTaskTemplateReagentDivData.getStore().reload();
			outTaskTemplateCosDivData.getStore().reload();
		} else {
			message(biolims.common.saveFailed);
		}
	}, null);
}
function editCopy() {
	window.location = window.ctx + '/experiment/out/outTask/copyOutTask.action?id=' + $("#outTask_id").val();
}

$("#toolbarbutton_status").click(function(){
	var OutTaskInfoGrid = outTaskResultGrid.getStore();
	var OutTaskItemGrid = outTaskItemGrid.getStore();
	var num = 0;
	for(var i= 0; i<OutTaskItemGrid.getCount();i++){		
		if(OutTaskInfoGrid.getCount()==0){
			message("请完成实验！");
			return;
		}
		for(var j=0;j<OutTaskInfoGrid.getCount();j++){
			if(OutTaskItemGrid.getAt(i).get("yCode")==OutTaskInfoGrid.getAt(j).get("yCode")){
				j=OutTaskInfoGrid.getCount();
			}else{
				num=num+1;
				if(num==OutTaskInfoGrid.getCount()){
					num=0;
					message("请完成实验！");
					return;
				}
			}
		}		
	}
	commonChangeState("formId=" + $("#outTask_id").val() + "&tableId=OutTask");
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#outTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#outTask_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#outTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'外包实验',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/out/outTask/showOutTaskItemList.action", {
				id : $("#outTask_id").val()
			}, "#outTaskItempage");
load("/experiment/out/outTask/showTemplateItemList.action", {
	id:$("#outTask_id").val()
}, "#outTaskTemplateItempage");
load("/experiment/out/outTask/showOutTaskTemplateReagentList.action",{
				id:$("#outTask_id").val(),
			}, "#outTaskTemplateReagentpage");
load("/experiment/out/outTask/showOutTaskTemplateCosList.action", {
	id:$("#outTask_id").val()
}, "#outTaskTemplateCospage");
load("/experiment/out/outTask/showOutTaskInfoList.action", {
				id : $("#outTask_id").val()
			}, "#outTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
//调用模板
function TemplateFun(){
			var type="doOutTask";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: '关闭',
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}

//双击加载模板数据
function setTemplateFun(rec){	
	if($('#outTask_acceptUser_name').val()==""){
		document.getElementById('outTask_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('outTask_acceptUser_name').value=rec.get('acceptUser-name');
	}		
	var itemGrid=outTaskItemGrid.store;
	if(itemGrid.getCount()>0){
		for(var i=0;i<itemGrid.getCount();i++){
			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
			itemGrid.getAt(i).set("sampleConsume",rec.get('sampleNum'));
		}
	}
		var code=$("#outTask_template").val();
		if(code==""){
			var cid=rec.get('id');
						document.getElementById('outTask_template').value=rec.get('id');
						document.getElementById('outTask_template_name').value=rec.get('name');
						var win = Ext.getCmp('TemplateFun');
						if(win){win.close();}
						var id=rec.get('id');
						//加载模板明细
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {
									var ob = outTaskTemplateItemGrid.getStore().recordType;
									outTaskTemplateItemGrid.stopEditing();
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										outTaskTemplateItemGrid.getStore().add(p);							
									});									
									outTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						//加载原辅料明细
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = outTaskTemplateReagentGrid.getStore().recordType;
								outTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									outTaskTemplateReagentGrid.getStore().add(p);							
								});								
								outTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						//加载实验设备明细
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = outTaskTemplateCosGrid.getStore().recordType;
								outTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id",obj.typeId);
									p.set("type-name",obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									outTaskTemplateCosGrid.getStore().add(p);							
								});			
								outTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null);
		}else{
			var flag = true;
			if(rec.get('id')==code){
				flag = confirm("是否重新加载实验步骤，应有实验步骤将被删除？");
 			 }
			if(flag==true){
 				//判断设备是否占用
 				var cid=rec.get('id');
						var ob1 = outTaskTemplateItemGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id");				
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/out/outTask/delTemplateItemOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null);
								}else{								
									outTaskTemplateItemGrid.store.removeAll();
								}
							}
							outTaskTemplateItemGrid.store.removeAll();
		 				}		 				
						var ob2 = outTaskTemplateReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");								
								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/out/outTask/delTemplateReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
								}else{
									outTaskTemplateReagentGrid.store.removeAll();
								}
							}
							outTaskTemplateReagentGrid.store.removeAll();
		 				}
						var ob3 = outTaskTemplateCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");							
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/out/outTask/delTemplateCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null); 
								}else{
									outTaskTemplateCosGrid.store.removeAll();
								}
							}
							outTaskTemplateCosGrid.store.removeAll();
		 				}
						document.getElementById('outTask_template').value=rec.get('id');
		 				document.getElementById('outTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	
									var ob = outTaskTemplateItemGrid.getStore().recordType;
									outTaskTemplateItemGrid.stopEditing();									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										outTaskTemplateItemGrid.getStore().add(p);							
									});									
									outTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = outTaskTemplateReagentGrid.getStore().recordType;
								outTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									outTaskTemplateReagentGrid.getStore().add(p);							
								});								
								outTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = outTaskTemplateCosGrid.getStore().recordType;
								outTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id",obj.typeId);
									p.set("type-name",obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									outTaskTemplateCosGrid.getStore().add(p);							
								});			
								outTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
					}			
	}		
		reloadOutTaskItem(rec.get("templateFieldsCode"), rec
				.get("templateFieldsItemCode"));

}
	
//按条件加载原辅料
function showReagent(){
	//获取全部数据
	var allRcords=outTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message("请先保存执行单数据!");
		return;
	}
	//获取选择的数据
	var selectRcords=outTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=outTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#outTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/out/outTask/showOutTaskTemplateReagentList.action", {
			id : $("#outTask_id").val()
		}, "#outTaskTemplateReagentpage");
	}else if(length1==1){
		outTaskTemplateReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/out/outTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = outTaskTemplateReagentGrid.getStore().recordType;
				outTaskTemplateReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("expireDate",obj.expireDate);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("outTask-id",obj.tId);
					p.set("outTask-name",obj.tName);					
					outTaskTemplateReagentGrid.getStore().add(p);							
				});
				outTaskTemplateReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}	
}

//按条件加载设备
function showCos(){
	var allRcords=outTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message("请先保存执行单数据!");
		return;
	}
	//获取选择的数据
	var selectRcords=outTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=outTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#outTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/out/outTask/showOutTaskTemplateCosList.action", {
			id : $("#outTask_id").val()
		}, "#outTaskTemplateCospage");
	}else if(length1==1){
		outTaskTemplateCosGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/out/outTask/setCos.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = outTaskTemplateCosGrid.getStore().recordType;
				outTaskTemplateCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("note",obj.note);
					p.set("time",obj.time);
					p.set("type-id",obj.typeId);
					p.set("type-name",obj.typeName);
					p.set("state",obj.state);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tCos",obj.tCos);
					p.set("outTask-id",obj.tId);
					p.set("outTask-name",obj.tName);					
					outTaskTemplateCosGrid.getStore().add(p);							
				});
				outTaskTemplateCosGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}
}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	
	function ckcrk(){		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = outTaskResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message("有样本未提交，不能初始化！");
						return;
					}
				}
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");
								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message("回滚失败！");
							}
						}, null);
					}
					
				}
			}
		});
	}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '保存'
			});
		item.on('click', ckcrk2);		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "OutTask",id : $("#outTask_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
			} else {
				message("办理回滚失败！");
			}
		}, null);
	
}
		



