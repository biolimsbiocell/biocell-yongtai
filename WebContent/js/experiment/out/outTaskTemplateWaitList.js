$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : 'id',
		width : 120,
		hidden : true,
		sortable : true
	});
	cm.push({
		dataIndex : 'code',
		header : '步骤编码',
		width : 140,
		sortable : true
	});
	cm.push({
		dataIndex : 'name',
		header : '步骤名称',
		width : 120,
		hidden :true,
		sortable : true
	});
	cm.push({
		dataIndex : 'note',
		header : '备注',
		width : 80,
		sortable : true
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/experiment/out/outTask/showTemplateWaitListJson.action?id="+$("#id").val();
	loadParam.limit = 200;
	var opts = {};
	opts.title = "待处理样本";
	opts.width = document.body.clientWidth - 850;
	opts.height = document.body.clientHeight - 180;
	opts.rowselect = function(id) {
		$("#id").val(id);
	};
	opts.rowdblclick = function(id) {
		$("#id").val(id);
		view();
	};
	var grid = gridTable("template_wait_grid_div", cols, loadParam, opts);
	$("#template_wait_grid_div").data("grid", grid);
	opts.tbar = [];
});