var reinfusionPlanTable;
$(function() {
	var options = table(true, "",
			"/experiment/reinfusionPlan/reinfusionPlan/showReinfusionPlanListJson.action", [ {
				"data" : "id",
				"title" : "编码",
			}, {
				"data" : "name",
				"title" : "描述",
			},{
				"data" : "reinfusionPlanDate",
				"title" : "回输计划日期"
			},{
				"data" : "createUser-name",
				"title" : "创建人"
			},{
				"data" : "createDate",
				"title" : "创建时间"
			},{
				"data" : "stateName",
				"title" : "状态"
			} ], null)
	reinfusionPlanTable = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(reinfusionPlanTable);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/reinfusionPlan/reinfusionPlan/toEditReinfusionPlan.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/reinfusionPlan/reinfusionPlan/toEditReinfusionPlan.action?id='+ id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/reinfusionPlan/reinfusionPlan/toViewReinfusionPlan.action?id='+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" :  "回输计划编号",
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : "创建人姓名",
		"type" : "input",
		"searchName" : "createUser-name",
	}, {
		"txt" :"回输计划日期（开始）",
		"type" : "dataTime",
		"searchName" : "reinfusionPlanDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "回输计划日期（结束）",
		"type" : "dataTime",
		"searchName" : "reinfusionPlanDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" :"创建时间（开始）",
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : "创建时间（结束）",
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"type" : "table",
		"table" : reinfusionPlanTable
	} ];
}
