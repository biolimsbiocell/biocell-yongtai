var reinfusionPlanItemTab, oldChangeLog;
// 出库明细表
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.sample.applicationDetailId,
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "code");
			$(td).attr("state", rowData['state']);
			$(td).attr("orderCode", rowData['orderCode']);
			$(td).attr("tempId", rowData['tempId']);
			$(td).attr("reinfusionPlan-id", rowData['reinfusionPlan-id']);
		}
	});
	colOpts.push({
		"data": "result",
		"title": "确认结果",
		"className":"select",
		"name":"请选择|有效|无效",
		"createdCell": function(td) {
			$(td).attr("saveName", "result");
			$(td).attr("selectOpt", "请选择|有效|无效");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "无效";
			}
			if(data == "1") {
				return "有效";
			}
			else{
				return "请选择";
			}
		}
	});
	colOpts.push({
		"data": "feedBack",
		"title": "是否回输",
		"className":"select",
		"name":"请选择|是|否",
		"createdCell": function(td) {
			$(td).attr("saveName", "feedBack");
			$(td).attr("selectOpt", "请选择|是|否");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "是";
			}
			if(data == "0") {
				return "否";
			}
			else{
				return "请选择";
			}
		}
	});
	colOpts.push({
		"data": "delay",
		"title": "能否延迟",
		"className":"select",
		"name":"请选择|能|不能",
		"createdCell": function(td) {
			$(td).attr("saveName", "delay");
			$(td).attr("selectOpt", "请选择|能|不能");
		},
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return "能";
			}
			if(data == "0") {
				return "不能";
			}
			else{
				return "请选择";
			}
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : "姓名",
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data" : "age",
		"title" : "年龄",
		"createdCell" : function(td) {
			$(td).attr("saveName", "age");
		}
	});
	colOpts.push({
		"data" : "gender",
		"title" : "性别",
		"createdCell" : function(td) {
			$(td).attr("saveName", "gender");
		},
		"render": function(data, type, full, meta) {
			if (data == "1"){
				return "男";
			}else if (data == "0"){
				return "女";
			} else {
				return "未知";
			}
		}
	});
	colOpts.push({
		"data" : "nation",
		"title" : "民族",
		"createdCell" : function(td) {
			$(td).attr("saveName", "nation");
		}
	});
	colOpts.push({
		"data" : "hospital",
		"title" : "医院",
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospital");
		}
	});
	colOpts.push({
		"data" : "hospitalPhone",
		"title" : "医院电话",
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospitalPhone");
		}
	});
	colOpts.push({
		"data" : "note",
		"title" : "备注",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	});
	var tbarOpts = [];
	
	var state = $("#reinfusionPlan_state").text();
	if(state!="Complete"&&state!="完成"){
		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				//刷新左侧表
				var reinfusionPlanTempTabs = $("#reinfusionPlanTempdiv")
						.DataTable();
				removeChecked(
						$("#reinfusionPlanItemdiv"),
						"/experiment/reinfusionPlan/reinfusionPlan/delReinfusionPlanItem.action",
						"删除回输计划明细数据：", $("#reinfusionPlan_id").text(),reinfusionPlanTempTabs);
				reinfusionPlanTempTabs.ajax.reload();

			}
		});
		tbarOpts.push({
			text : biolims.common.Editplay,
			action : function() {
				editItemLayer($("#reinfusionPlanItemdiv"));
			}
		});
	}
	
	var reinfusionPlanItemOps = table(
			true,
			$("#reinfusionPlan_id").text(),
			"/experiment/reinfusionPlan/reinfusionPlan/showReinfusionPlanItemTableJson.action",
			colOpts, tbarOpts);
	reinfusionPlanItemTab = renderData($("#reinfusionPlanItemdiv"),
			reinfusionPlanItemOps);
	// 选择数据并提示
	reinfusionPlanItemTab.on('draw', function() {
		var index = 0;
		$("#reinfusionPlanItemdiv .icheck").on(
				'ifChanged',
				function(event) {
					if ($(this).is(':checked')) {
						index++;
						$("#plateModal").find(
								".mysample[sid='" + this.value + "']").css(
								"border", "2px solid #000");
					} else {
						var tt = $("#plateModal").find(
								".mysample[sid='" + this.value + "']").css(
								"border", "1px solid gainsboro");
						index--;
					}
					top.layer.msg(biolims.common.Youselect + index
							+ biolims.common.data);
				});
		oldChangeLog = reinfusionPlanItemTab.ajax.json();

	});
});

// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for ( var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			// 添加样本类型ID
			 if(k == "code") {
				 json["state"] = $(tds[j]).attr("state");
				 json["orderCode"] = $(tds[j]).attr("orderCode");
				 json["tempId"] = $(tds[j]).attr("tempId");
				 json["reinfusionPlan-id"] = $(tds[j]).attr("reinfusionPlan-id");
				 continue;
			 }
			 if(k == "gender") {
				 if($(tds[j]).text()=="男"){
					 json[k] = "1";
				 }else if($(tds[j]).text()=="女"){
					 json[k] = "0";
				 }else{
					 json[k] = "";
				 }
				 continue;
			 }
			 if(k == "result") {
				 if($(tds[j]).text()=="有效"){
					 json[k] = "1";
				 }else if($(tds[j]).text()=="无效"){
					 json[k] = "0";
				 }else{
					 json[k] = "";
				 }
				 continue;
			 }
			 if(k == "feedBack") {
				 if($(tds[j]).text()=="是"){
					 json[k] = "1";
				 }else if($(tds[j]).text()=="否"){
					 json[k] = "0";
				 }else{
					 json[k] = "";
				 }
				 continue;
			 }
			 if(k == "delay") {
				 if($(tds[j]).text()=="能"){
					 json[k] = "1";
				 }else if($(tds[j]).text()=="不能"){
					 json[k] = "0";
				 }else{
					 json[k] = "";
				 }
				 continue;
			 }
			
		}
		json.color = $(val).attr("background");
		data.push(json);
	});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开 创建日期: 2018/03/08 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '数量为"' + v.num + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k] && k != "color") {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
