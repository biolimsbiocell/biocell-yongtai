var qualityTestShowTab;
var qualityTestShowChangeLog;
$(function() {
	
	
	
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "sampleOrder-id",
		"title":"关联订单",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sampleOrder-id");
		}
	});
	colOpts.push({
		"data": "batch",
		"title": "批次号",
		"createdCell": function(td) {
			$(td).attr("saveName", "batch");
		}
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.common.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.sampleType,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleType");
		}
	})
	colOpts.push({
		"data": "dicSampleType-id",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	})
	colOpts.push({
		"data": "dicSampleType-name",
		"title":biolims.common.dicSampleTypeName,
		"createdCell": function(td) {
			$(td).attr("saveName", "dicSampleType-name");
		}
	})
	colOpts.push({
		"data": "experimentalSteps",
		"title": biolims.common.commonTemplate,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "experimentalSteps");
		},
	})
	colOpts.push({
		"data": "sampleDeteyion-name",
		"title": '检测项',
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sampleDeteyion-name");
			$(td).attr("sampleDeteyion-id",  rowData['sampleDeteyion-id']);
		},
	})
	 colOpts.push({
		"title": "查看结果",
		"width": "100px",
		"data": null,
		"createdCell": function(td, data) {
		},
		"render": function(data, type, row, meta) {
			return '<input type="button" value="查看结果信息" onClick="chakan(this);">'
		},
	});
	colOpts.push({
		"data": "result",
		"title": biolims.common.result+'<img src="/images/required.gif"/>',
		"name":biolims.common.qualified+"|"+biolims.common.disqualified,
		"createdCell": function(td) {
			$(td).attr("saveName", "result");
			$(td).attr("selectOpt", biolims.common.qualified+"|"+biolims.common.disqualified);
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.disqualified;
			}
			if(data == "1") {
				return biolims.common.qualified;
			}
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	})
	colOpts.push({
		"data": "mark",
		"title": "来源模块",
		"createdCell": function(td) {
			$(td).attr("saveName", "mark");
		}
	})
	colOpts.push({
		"data": "qualityTest-id",
		"title": "任务单号",
		"createdCell": function(td) {
			$(td).attr("saveName", "qualityTest-id");
		}
	})
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod != "view") {
	}
	var qualityTestShowTabOptions = table(true, $("#flag").val(), '/experiment/reinfusionPlan/reinfusionPlan/showQualityTestTableJson.action',
			colOpts, tbarOpts)
	qualityTestShowTab = renderData($("#addDicTypeTable"), qualityTestShowTabOptions);
	qualityTestShowTab.on('draw', function() {
		qualityTestShowChangeLog = qualityTestShowTab.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	stepViewChange();
});
function chakan(that){
	var itemid = $(that).parent("td").parent("tr").children("td").find(".icheck").eq(0).val();
	var jiance = $(that).parent("td").parent("tr").children("td").eq(10).attr("sampleDeteyion-id");
	top.layer.open({
		title:"查看检测结果",
		type:2,
		area:[document.body.clientWidth-50,document.body.clientHeight-50],
		btn: biolims.common.selected,
		content:[window.ctx+"/experiment/quality/qualityTest/showQualityTestResultItemTable.action?itemid="+itemid+"&jiance="
					+ jiance+"&ly=Z",''],
		yes: function(index, layero) {
			top.layer.close(index)
		},
	})
}