var oldChangeLog;
var reinfusionPlanItemTab;
$(function() {


	
	var colOpts = [];
	var datetime = "";
	colOpts.push({
		"data" : "id",
		"title" : "编号",
		"visible" : false,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "id");
		}
		});
	colOpts.push({
		"data" : "reinfusionPlanDate",
		"title" : "计划回输日期",
//		"className" : "date",
		"width" : "100px",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "reinfusionPlanDate");
//			if(rowData.diReinfusionPlanDate=="1"){
//				
//			}else{
//				$(td).removeClass(" date");
//			}
		}
	});
	
	colOpts.push({
		"data" : "modificationTime",
		"title" : "变更前回输日期"+ '<img src="/images/required.gif"/>',
//		"className" : "date",
		"width" : "100px",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "modificationTime");
//			if(rowData.diReinfusionPlanDate=="1"){
//				top.layer.msg("回输确认日期请填写!");
//			}else{
//				$(td).removeClass(" date");
//			}
		}
	});
	
	colOpts.push({
		"data" : 'diReinfusionPlanDate',
		"title" : "是否可修改回输日期",
		"visible":false,
//		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "diReinfusionPlanDate");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
//	colOpts.push({
//		"data" : "code",
//		"title" : "细胞编号",
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("saveName", "code");
//		}
//		,
//		});
	colOpts.push({
		"data" : "batch",
		"title" : "产品批号",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "batch");
			$(td).attr("sampleOrder-id", rowData['sampleOrder-id']);
			$(td).attr("state", rowData['state']);
			$(td).attr("diReinfusionPlanDate",rowData['diReinfusionPlanDate']);
			$(td).attr("code", rowData['code']);
		}
		,
		});
	colOpts.push({
		"data" : "name",
		"title" : "姓名",
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
		,
	});
	colOpts.push({
		"data" : 'gender',
		"title" : "性别",
//		"className" : "select",
		"name" : "|男|女",
		"createdCell" : function(td) {
			$(td).attr("saveName", "gender");
			$(td).attr("selectOpt", "|男|女");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "女";
			} else if (data == "1") {
				return "男";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : "filtrateCode",
		"title" : "筛选号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "filtrateCode");
		}
		,
	});
	colOpts.push({
		"data" : "hospital",
		"title" : "医院",
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospital");
		}
		,
	});
	colOpts.push({
		"data" : "hospitalPhone",
		"title" : "医院电话",
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospitalPhone");
		}
		,
	});
	colOpts.push({
		"data" : "inspectionDepartment",
		"title" : "科室",
		"createdCell" : function(td) {
			$(td).attr("saveName", "inspectionDepartment");
		}
		,
	});
	colOpts.push({
		"data" : "attendingDoctor",
		"title" : "医生",
		"createdCell" : function(td) {
			$(td).attr("saveName", "attendingDoctor");
		}
		,
	});
	colOpts.push({
		"data" : "attendingDoctorPhone",
		"title" : "医生电话",
		"createdCell" : function(td) {
			$(td).attr("saveName", "attendingDoctorPhone");
		}
		,
	});
	colOpts.push({
		"data" : "familyPhone",
		"title" : "家属联系方式",
		"createdCell" : function(td) {
			$(td).attr("saveName", "familyPhone");
		}
		,
	});
	colOpts.push({
		"data" : 'doctorNotice',
		"title" : '是否通知到医生'
			+ '<img src="/images/required.gif"/>',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "doctorNotice");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : 'doctorReply',
		"title" : '医生回复'
			+ '<img src="/images/required.gif"/>',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "doctorReply");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : 'patientNotice',
		"title" : '是否通知到患者'
			+ '<img src="/images/required.gif"/>',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "patientNotice");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : 'patientReply',
		"title" : '患者回复'
			+ '<img src="/images/required.gif"/>',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "patientReply");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : "createDateTime",
		"title" : "创建时间",
		"createdCell" : function(td) {
			$(td).attr("saveName", "createDateTime");
		}
	});
	colOpts.push({
		"data" : 'feedBack',
		"title" : "医事服务部确认回输",
//		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "feedBack");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : "note",
		"title" : "备注",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	});

	var tbarOpts = [];
//	tbarOpts.push({
//				text : "选择快递公司",
//				action : xzkdgs
//			});
//	tbarOpts.push({
//				text : "选择客服",
//				action : xzkf
//			});
	tbarOpts.push({
		text : "未确认",
		action : function() {
			zhuanhuan(2);
		}
	});
	tbarOpts.push({
		text : "已确认",
		action : function() {
			zhuanhuan(1);
		}
	});
	tbarOpts.push({
				text : biolims.common.search,
				action : function() {
					search();
				}
			});
	tbarOpts.push({
				text : biolims.common.save,
				action : function() {
					saveItem();
				}
			});
	tbarOpts.push({
		text: "查看质检结果",
		action:function(){
			showQualityTest();
		}
	});
	tbarOpts.push({
		text : "回输计划确认",
		action : function() {
			saveItemConfirm();
		}
	});
	tbarOpts.push({
		text : "回输计划变更",
		action : function() {
			reinfusionChangePlan();
		}
	});
//	tbarOpts.push({
//				text : "下载报告文件",
//				action : function() {
//					downFilesReport();
//				}
//			});
//	tbarOpts.push({
//				text : "下载说明文件",
//				action : function() {
//					downFilesReport1();
//				}
//			});
//	tbarOpts.push({
//				text : "下载其他文件文件",
//				action : function() {
//					downFilesReport2();
//				}
//			});
	var ops = table(true, null,
			"/experiment/reinfusionPlan/reinfusionPlan/showReinfusionPlanItemListJson.action?type="+$("#reinfusionPlanItem_type").val(), colOpts,
			tbarOpts);
	reinfusionPlanItemTab = renderData($("#reinfusionPlanItemDiv"), ops);
	reinfusionPlanItemTab.on('draw', function() {
				oldChangeLog = reinfusionPlanItemTab.ajax.json();
			});
	$('#reinfusionPlanItemDiv').on('init.dt', function() {
				recoverSearchContent(reinfusionPlanItemTab);
			})
});
//行内input编辑
function edittable() {
	document.addEventListener('click', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);
				//console.log("123456")
			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}
		}
	}, false)
	document.addEventListener('touchstart', function(event) {
		var evt = window.event || event;
		if(evt.target.nodeName.toLowerCase() == "td") {
			var ele = event.srcElement || evt.target;
			if($(ele).hasClass("date")) {
				changeDateInput(ele);
			} else if($(ele).hasClass("select")) {
				changeSelectInput(ele);
			} else if($(ele).hasClass("edit")) {
				changeTextInput(ele);

			} else if($(ele).hasClass("textarea")) {
				changeTextTextarea(ele);
			}
		}

	}, false)
	//变普通input输入框
	function changeTextInput(ele) {
		$(ele).css({
			"padding": "0px"
		});
		var width = $(ele).css("width");
		var value = ele.innerText;
		//console.log(value);
		var ipt = $('<input type="text" id="edit">');
		ipt.css({
			"width": width,
			"height": "32px",
		});
		$(ele).html(ipt.val(value));
		ipt.focus();
		ipt.select();
		$(ipt).click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			var tdIndex = $(ele).parent("tr").children("td").index($(ele));
			if(e.keyCode == 38) { //上：38 下：40 左：39 右：37 回车 ：13
				var shangtr = $(ele).parent("tr").prev("tr");
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				if(shangtr.length) {
					var ele1 = shangtr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 40||e.keyCode==13) {
				var xiatr = $(ele).parent("tr").next("tr");
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				if(xiatr.length) {
					var ele1 = xiatr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 39) {
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				var ele1 = $(ele).next("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.next("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}

			} else if(e.keyCode == 37) {
				var width = $("#edit").width();
				$("#edit").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#edit").parents("tr").addClass("editagain");
				$("#edit").parent("td").html($("#edit").val());
				var ele1 = $(ele).prev("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.prev("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}
			}
		}

		//		document.onkeydown = function(event) {
		//			var e = event || window.event || arguments.callee.caller.arguments[0];
		//			if(e.keyCode === 9) {
		//				var width = $("#edit").width();
		//				$("#edit").parent("td").css({
		//					"padding": "5px 0px 5px 5px",
		//					"overflow": "hidden",
		//					"text-overflow": "ellipsis",
		//					"max-width": width + "px",
		//					"box-shadow": "0px 0px 1px #17C697"
		//				});
		//				$("#edit").parents("tr").addClass("editagain");
		//				$("#edit").parent("td").html($("#edit").val());
		//				var next = $(ele).next("td");
		//				while(next.hasClass("undefined")) {
		//					next = next.next("td")
		//				}
		//				if(next.length) {
		//					next.click();
		//				} else {
		//					return false;
		//				}
		//
		//			}
		//
		//		};
	}
	//变普通Textarea输入框
	function changeTextTextarea(ele) {
		if($(ele).hasClass("edited")) {
			var width = $(ele).width() + "px";
		} else {
			var width = $(ele).width() + 10 + "px";
		}
		ele.style.padding = "0";
		$(ele).css({
			"min-width": width
		});
		var ipt = $('<textarea www=' + width + ' style="position: absolute;height:80px;width:200px" id="textarea">' + ele.innerText + '</textarea>');
		$(ele).html(ipt);
		$(ele).find("textarea").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			if(e.keyCode === 9) {
				var td = $("#textarea").parents("td");
				var width = $("#textarea").attr("www");
				td.css({
					"padding": "5px 0px",
					"max-width": width,
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"box-shadow": "0px 0px 1px #17C697",
				});
				td.addClass("edited");
				td.parent("tr").addClass("editagain");
				//td[0].title=$("#edit").val();
				td.html($("#textarea").val());

				var next = $(ele).next("td");
				while(next.hasClass("undefined")) {
					next = next.next("td")
				}
				if(next.length) {
					next.click();
				} else {
					return false;
				}
			}

		};
	}
	//变下拉框
	function changeSelectInput(ele) {
		$(ele).css("padding", "0px");
		var width = $(ele).width();
		var selectOpt = $(ele).attr("selectopt");
		var selectOptArr = selectOpt.split("|");
		var ipt = $('<select id="select"></select>');
		var value = ele.innerText;
		selectOptArr.forEach(function(val, i) {
			if(value == val) {
				ipt.append("<option selected>" + val + "</option>");
			} else {
				ipt.append("<option>" + val + "</option>");
			}
		});
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
		//$(ele).find("select").click();
		document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			var tdIndex = $(ele).parent("tr").children("td").index($(ele));
			if(e.keyCode == 38) { //上：38 下：40 左：39 右：37
				var shangtr = $(ele).parent("tr").prev("tr");
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());
				if(shangtr.length) {
					var ele1 = shangtr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 40||e.keyCode==13) {
				var xiatr = $(ele).parent("tr").next("tr");
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());
				if(xiatr.length) {
					var ele1 = xiatr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 39) {
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());
				var ele1 = $(ele).next("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.next("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}

			} else if(e.keyCode == 37) {
				$("#select").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#select").parents("tr").addClass("editagain");
				$("#select").parent("td").html($("#select option:selected").val());
				var ele1 = $(ele).prev("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.prev("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}
			}
		}
	}
	//变日期选择框
	function changeDateInput(ele) {
		$(ele).css("padding", "0px");
		var ipt = $('<input type="text" id="date" autofocus value=' + $(ele).text() + '>');
		var width = $(ele).width();
		ipt.css({
			"width": width + "px",
			"height": "32px",
		});
		$(ele).html(ipt);
		ipt.datepicker({
			//language: "zh-TW",
			language: 'cn',
			autoclose: true, //选中之后自动隐藏日期选择框
			format: "yyyy-mm-dd" //日期格式，详见 
		});
		$(ele).find("input").click();
			document.onkeydown = function(event) {
			var e = event || window.event || arguments.callee.caller.arguments[0];
			var tdIndex = $(ele).parent("tr").children("td").index($(ele));
			if(e.keyCode == 38) { //上：38 下：40 左：39 右：37
				var shangtr = $(ele).parent("tr").prev("tr");
				var width = $("#date").width();
				$("#date").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				if(shangtr.length) {
					var ele1 = shangtr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 40||e.keyCode==13) {
				var xiatr = $(ele).parent("tr").next("tr");
				var width = $("#date").width();
				$("#date").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				if(xiatr.length) {
					var ele1 = xiatr.children("td").eq(tdIndex)[0];
					ele1.click();
				}
			} else if(e.keyCode == 39) {
				var width = $("#date").width();
				$("#date").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				var ele1 = $(ele).next("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.next("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}

			} else if(e.keyCode == 37) {
				var width = $("#date").width();
				$("#date").parent("td").css({
					"padding": "5px 0px 5px 5px",
					"overflow": "hidden",
					"text-overflow": "ellipsis",
					"max-width": width + "px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
				var ele1 = $(ele).prev("td");
				var flag = ele;
				while(ele1.attr("class") == undefined) {
					ele1 = ele1.prev("td");
					if(!ele1.length) {
						ele1 = flag;
						break;
					}
				}
				if(ele1.length) {
					ele1.click();
				}
			}
		}
	}
	document.onmouseup = function(event) {
		var evt = window.event || event;
		if(document.getElementById("textarea") && evt.target.id != "textarea") {
			var td = $("#textarea").parents("td");
			var width = $("#textarea").attr("www");
			td.css({
				"padding": "5px 0px",
				"max-width": width,
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"box-shadow": "0px 0px 1px #17C697",
			});
			td.addClass("edited");
			td.parent("tr").addClass("editagain");
			//td[0].title=$("#edit").val();
			td.html($("#textarea").val());
		}
		if(document.getElementById("edit") && evt.target.id != "edit") {
			var width = $("#edit").width();
			$("#edit").parent("td").css({
				"padding": "5px 0px 5px 5px",
				"overflow": "hidden",
				"text-overflow": "ellipsis",
				"max-width": width + "px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#edit").parents("tr").addClass("editagain");
			$("#edit").parent("td").html($("#edit").val());
		}
		if(document.getElementById("select") && evt.target.id != "select") {
			$("#select").parent("td").css({
				"padding": "5px",
				"box-shadow": "0px 0px 1px #17C697"
			});
			$("#select").parents("tr").addClass("editagain");
			$("#select").parent("td").html($("#select option:selected").val());
		}
		if(document.getElementById("date") && evt.target.id != "date") {
			if(!$(".datepicker").length) {
				$("#date").parent("td").css({
					"padding": "5px",
					"box-shadow": "0px 0px 1px #17C697"
				});
				$("#date").parents("tr").addClass("editagain");
				$("#date").parent("td").html($("#date").val());
			}
		}
	}
}
function zhuanhuan(aa){
	reinfusionPlanItemTab.settings()[0].ajax.data = {
		type:aa
	};
	$("#reinfusionPlanItem_type").val(aa);
	reinfusionPlanItemTab.ajax.url("/experiment/reinfusionPlan/reinfusionPlan/showReinfusionPlanItemListJson.action").load();
	oldChangeLog = reinfusionPlanItemTab.ajax.json();
	if(aa=="2"){
		$("#titleNote").text("未确认计划");
	}
	if(aa=="1"){
		$("#titleNote").text("已确认计划");
	}
}

//回输计划变更
function reinfusionChangePlan() {
	if($("#reinfusionPlanItem_type").val()=="1"){
		var rows = $("#reinfusionPlanItemDiv .selected");
		if(rows.length!=1){
			top.layer.msg("请选择一条回输计划！");
			return false;
		}
		var data = "";//[]
		rows.each(function(i, val) {
			var tds = $(val).children("td");
			data = $(tds[0]).find("input").val();
//			data.push($(tds[0]).find("input").val());
		});
		top.layer.open({
			title : "填写修改内容",
			type : 1,
			area : ["30%", "60%"],
			btn : "确定提交",
			btnAlign : 'c',
			content : '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
			yes : function(indexx, layer) {
				var array = $("#checkCode", parent.document).val();
				$.ajax({
					type : 'post',
					url : '/experiment/reinfusionPlan/reinfusionPlan/saveReinfusionPlanItemChangPlan.action',
					data : {
						dataJson : data,
						changeLog : array
					},
					success : function(data1) {
						var data = JSON.parse(data1)
						if (data.success) {
							top.layer.closeAll();
//							top.layer.msg("回输计划变更成功！");
							if(data.id!=""){
								window.location = window.ctx +
								'/experiment/changePlan/changePlan/editChangePlan.action?id=' + data.id ;
							}
							tableRefresh();
						} else {
							top.layer.msg("回输计划变更失败！")
						};
					}
				});
			}
			,
		});
		
	}else{
		top.layer.msg("未确认数据不用提交申请！");
		return false;
	}
}
//function downFilesReport() {
//	var rows = $("#reinfusionPlanItemDiv .selected");
//	var length = rows.length;
//	if (!length) {
//		top.layer.msg(biolims.common.pleaseSelect);
//		return false;
//	}
//	$("#reinfusionPlanItemDiv .selected").each(function(i, v) {
//				var fileNum = $(v).find("td[savename='fileNum']").text();
//				if (fileNum != "" && fileNum != "0") {
//					var id = $(v).find("td[savename='id']").text();
//					if (id != "") {
//						ajax("post",
//								"/operfile/downloadReportItemsSingle.action", {
//									id : id,
//									model : "sampleReportItemNew",
//									usetype : "report"
//									,
//								}, function(data) {
//									if (data.success) {
//										$.each(data.data, function(i, obj) {
//													downFile(obj.id);
//												});
//									} else {
//										message("获取数据时发生错误！");
//									}
//								}, null);
//					}
//				}
//			});
//}

//function downFilesReport1() {
//	var rows = $("#reinfusionPlanItemDiv .selected");
//	var length = rows.length;
//	if (!length) {
//		top.layer.msg(biolims.common.pleaseSelect);
//		return false;
//	}
//	$("#reinfusionPlanItemDiv .selected").each(function(i, v) {
//				var fileNum1 = $(v).find("td[savename='fileNum1']").text();
//				if (fileNum1 != "" && fileNum1 != "0") {
//					var id = $(v).find("td[savename='id']").text();
//					if (id != "") {
//						ajax("post",
//								"/operfile/downloadReportItemsSingle.action", {
//									id : id,
//									model : "sampleReportItemNew",
//									usetype : "exp"
//									,
//								}, function(data) {
//									if (data.success) {
//										$.each(data.data, function(i, obj) {
//													downFile(obj.id);
//												});
//									} else {
//										message("获取数据时发生错误！");
//									}
//								}, null);
//					}
//				}
//			});
//}

//function downFilesReport2() {
//	var rows = $("#reinfusionPlanItemDiv .selected");
//	var length = rows.length;
//	if (!length) {
//		top.layer.msg(biolims.common.pleaseSelect);
//		return false;
//	}
//	$("#reinfusionPlanItemDiv .selected").each(function(i, v) {
//				var fileNum2 = $(v).find("td[savename='fileNum2']").text();
//				if (fileNum2 != "" && fileNum2 != "0") {
//					var id = $(v).find("td[savename='id']").text();
//					if (id != "") {
//						ajax("post",
//								"/operfile/downloadReportItemsSingle.action", {
//									id : id,
//									model : "sampleReportItemNew",
//									usetype : "misc"
//									,
//								}, function(data) {
//									if (data.success) {
//										$.each(data.data, function(i, obj) {
//													downFile(obj.id);
//												});
//									} else {
//										message("获取数据时发生错误！");
//									}
//								}, null);
//					}
//				}
//			});
//}

//function downFile(id) {
//	window.open(window.ctx + '/operfile/downloadById.action?id=' + id, '', '');
//}
//function xzkdgs() {
//	var rows = $("#reinfusionPlanItemDiv .selected");
//	var length = rows.length;
//	if (!length) {
//		top.layer.msg(biolims.common.pleaseSelect);
//		return false;
//	}
//	top.layer.open({
//		title : biolims.common.pleaseChoose,
//		type : 2,
//		area : [document.body.clientWidth - 300,
//				document.body.clientHeight - 100],
//		btn : biolims.common.selected,
//		content : [
//				window.ctx
//						+ "/system/express/expressCompany/expressCompanySelectTable.action?flag=ExpressCompanyFun",
//				''],
//		yes : function(index, layero) {
//			var name = $('.layui-layer-iframe', parent.document).find("iframe")
//					.contents().find("#addExpressCompanyTable .chosed")
//					.children("td").eq(1).text();
//			var id = $('.layui-layer-iframe', parent.document).find("iframe")
//					.contents().find("#addExpressCompanyTable .chosed")
//					.children("td").eq(0).text();
//			rows.addClass("editagain");
//			rows.find("td[savename='expressCompany']").text(name);
//			top.layer.close(index);
//		}
//		,
//	});
//}
//function xzkf() {
//	var rows = $("#reinfusionPlanItemDiv .selected");
//	var length = rows.length;
//	if (!length) {
//		top.layer.msg(biolims.common.pleaseSelect);
//		return false;
//	}
//	top.layer.open({
//				title : biolims.common.pleaseChoose,
//				type : 2,
//				area : [document.body.clientWidth - 300,
//						document.body.clientHeight - 100],
//				btn : biolims.common.selected,
//				content : [
//						window.ctx + "/core/user/selectSalesRepTable.action",
//						''],
//				yes : function(index, layero) {
//					var name = $('.layui-layer-iframe', parent.document)
//							.find("iframe").contents()
//							.find("#addContainer .chosed").children("td").eq(1)
//							.text();
//					var id = $('.layui-layer-iframe', parent.document)
//							.find("iframe").contents()
//							.find("#addContainer .chosed").children("td").eq(0)
//							.text();
//					rows.addClass("editagain");
//					rows.find("td[savename='reviewer-name']").text(name);
//					rows.find("td[savename='reviewer-id']").text(id);
//					top.layer.close(index)
//				}
//				,
//			})
//}

// 保存
function saveItem() {
	if($("#reinfusionPlanItem_type").val()=="1"){
		top.layer.msg("已确认数据，不允许直接保存！");
		return false;
	}else{
		var ele = $("#reinfusionPlanItemDiv");
		var changeLog = "回输计划：";
		var data = saveItemjson(ele);
		changeLog = getChangeLog(data, ele, changeLog);
		var changeLogs = "";
		if (changeLog != "回输计划：") {
			changeLogs = changeLog;
		}
		top.layer.load(4, {
					shade : 0.3
				});
		$.ajax({
			type : 'post',
			url : '/experiment/reinfusionPlan/reinfusionPlan/saveReinfusionPlanItemList.action',
			data : {
				dataJson : data,
				logInfo : changeLogs
			},
			success : function(data) {
				var data = JSON.parse(data)
				if (data.success) {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveSuccess);
					tableRefresh();
				} else {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveFailed)
				};
			}
		});
	}
}
//回输计划 确认
function saveItemConfirm() {
	if($("#reinfusionPlanItem_type").val()=="1"){
		top.layer.msg("已确认数据，不需再确认！");
		return false;
	}else{
		var ele = $("#reinfusionPlanItemDiv");
		var trs = ele.find("tbody").children(".editagain");
		if(trs.length>0){
			top.layer.msg("请先保存数据！");
			return false;
		}else{
			var rows = $("#reinfusionPlanItemDiv .selected");
			var length = rows.length;
			if(!length) {
				top.layer.msg(biolims.common.pleaseSelect);
				return false;
			}
			var data = [];
			rows.each(function(i, val) {
				var tds = $(val).children("td");
				data.push($(tds[0]).find("input").val());
			});
			$.ajax({
				type : 'post',
				url : '/experiment/reinfusionPlan/reinfusionPlan/saveReinfusionPlanItemConfirmList.action',
				data : {
					ids : data
				},
				success : function(datas) {
					var data = JSON.parse(datas);
					if (data.success) {
						top.layer.msg(biolims.common.saveSuccess);
						tableRefresh();
					} else {
						top.layer.msg(biolims.common.saveFailed)
					};
				}
			});
		}
	}
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
				var json = {};
				var tds = $(val).children("td");
				json["id"] = $(tds[0]).find("input").val();
				for (var j = 1; j < tds.length; j++) {
					var k = $(tds[j]).attr("savename");
					if (k == "batch") {
						json["sampleOrder-id"] = $(tds[j]).attr("sampleOrder-id");
						json["state"] = $(tds[j]).attr("state");
						
						
						json["code"] = $(tds[j]).attr("code");
//						json["diReinfusionPlanDate"] = $(tds[j]).attr("diReinfusionPlanDate");
					}
					if (k == "gender") {
						var gender = $(tds[j]).text();
						if (gender == "女") {
							json[k] = "0";
						} else if (gender == "男") {
							json[k] = "1";
						} else {
							json[k] = "";
						}
						continue;
					}
					if (k == "doctorNotice") {
						var doctorNotice = $(tds[j]).text();
						if (doctorNotice == "是") {
							json[k] = "1";
						} else if (doctorNotice == "否") {
							json[k] = "0";
						} else {
							json[k] = "";
						}
						continue;
					}
					if (k == "doctorReply") {
						var doctorReply = $(tds[j]).text();
						if (doctorReply == "是") {
							json[k] = "1";
						} else if (doctorReply == "否") {
							json[k] = "0";
						} else {
							json[k] = "";
						}
						continue;
					}

					if (k == "patientNotice") {
						var patientNotice = $(tds[j]).text();
						if (patientNotice == "是") {
							json[k] = "1";
						} else if (patientNotice == "否") {
							json[k] = "0";
						} else {
							json[k] = "";
						}
						continue;
					}
					
					if (k == "patientReply") {
						var patientReply = $(tds[j]).text();
						if (patientReply == "是") {
							json[k] = "1";
						} else if (patientReply == "否") {
							json[k] = "0";
						} else {
							json[k] = "";
						}
						continue;
					}

//					if (k == "id") {
//
//						json["report-id"] = $(tds[j]).attr("report-id");
//						continue;
//					}

					json[k] = $(tds[j]).text();
				}
				data.push(json);
			});
	return JSON.stringify(data);
}

/*
 * 
 * 创建者 : 郭恒开 创建日期: 2018/03/09 文件描述: 修改日志
 * 
 */
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
				var id = v.id;
				if (!id) {
					changeLog += '新增记录:';
					for (var k in v) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"为"' + v[k] + '";';
					}
					return true;
				}
				changeLog = "回输计划：";
				oldChangeLog.data.forEach(function(vv, ii) {
							if (vv.id == id) {
								for (var k in v) {
									if (v[k] != vv[k]) {
										var title = ele.find("th[savename=" + k
												+ "]").text();
										changeLog += '"' + title + '"'
												+ "由" + '"'
												+ vv[k] + '"'
												+ "改为" + '"'
												+ v[k] + '";';
									}
								}
								return false;
							}
						});
			});
	return changeLog;
}
// 弹框模糊查询参数
function searchOptions() {
	return [{
				"txt" : "产品批号",
				"type" : "input",
				"searchName" : "batch"
				,
			}, {
				"txt" : "姓名",
				"type" : "input",
				"searchName" : "name"
				,
			}, {
				"txt" : "筛选号",
				"type" : "input",
				"searchName" : "filtrateCode"
				,
			},{
				"txt" : "医院",
				"type" : "input",
				"searchName" : "hospital"
				,
			},{
				"txt" : "科室",
				"type" : "input",
				"searchName" : "inspectionDepartment"
				,
			},{
				"txt" : "医生",
				"type" : "input",
				"searchName" : "attendingDoctor"
				,
			},{
				"txt" : "计划回输日期",
				"type" : "dataTime",
				"searchName" : "reinfusionPlanDate##@@##1",
				"mark" : "z##@@##"
			},
//			{
//				"txt" : "医事服务部确认",
//				"type" : "select",
//				"options" :  "|" + biolims.common.yes
//						+ "|" + biolims.common.no,
//				"changeOpt" : "''|1|0|",
//				"searchName" : "feedBack"
//			}, 
//			{
//				"txt" : "客服审核状态",
//				"type" : "select",
//				"options" : "请选择|未审核|已审核|有更新",
//				"changeOpt" : "''|0|1|2|",
//				"searchName" : "reviewState"
//			}, {
//				"txt" : "报告完成日期(开始)",
//				"type" : "dataTime",
//				"searchName" : "completeDate##@@##1",
//				"mark" : "s##@@##"
//				,
//			}, {
//				"txt" : "报告完成日期(结束)",
//				"type" : "dataTime",
//				"searchName" : "completeDate##@@##2",
//				"mark" : "e##@@##"
//				,
//			}, {
//				"txt" : "报告发送日期(开始)",
//				"type" : "dataTime",
//				"searchName" : "sendDate##@@##1",
//				"mark" : "s##@@##"
//				,
//			}, {
//				"txt" : "报告发送日期(结束)",
//				"type" : "dataTime",
//				"searchName" : "sendDate##@@##2",
//				"mark" : "e##@@##"
//				,
//			}, 
			{
				"type" : "table",
				"table" : reinfusionPlanItemTab
			}];
}

// 报告上传
function fileUp1(that) {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	$("#uploadFileValx").fileinput({
		uploadUrl : ctx
				+ "/attachmentUpload?useType=report&modelType=sampleReportItemNew&contentId="
				+ id,
		uploadAsync : true,
		language : 'zh',
		showPreview : true,
		enctype : 'multipart/form-data',
		elErrorContainer : '#kartik-file-errors'
		,
	});
	// if($("#uploadFile .fileinput-remove").length){
	// $("#uploadFile .fileinput-remove").eq(0).click();
	// }
	$("#uploadFilex").modal("show");
	// var cfileInput = fileInput('1','samplingTaskItem', id);
}
// 报告查看
function fileView1(that) {
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title : biolims.common.attachment,
		type : 2,
		skin : 'layui-layer-lan',
		area : top.screeProportion,
		content : window.ctx
				+ "/operfile/initFileList.action?flag=report&modelType=sampleReportItemNew&id="
				+ id,
		cancel : function(index, layero) {
			top.layer.close(index)
		}
	})
}

// 上传说明
function fileUp2(that) {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	$("#uploadFileValx").fileinput({
		uploadUrl : ctx
				+ "/attachmentUpload?useType=exp&modelType=sampleReportItemNew&contentId="
				+ id,
		uploadAsync : true,
		language : 'zh',
		showPreview : true,
		enctype : 'multipart/form-data',
		elErrorContainer : '#kartik-file-errors'
		,
	});
	// if($("#uploadFile .fileinput-remove").length){
	// $("#uploadFile .fileinput-remove").eq(0).click();
	// }
	$("#uploadFilex").modal("show");
	// var cfileInput = fileInput('1','samplingTaskItem', id);
}
// 说明查看
function fileView2(that) {
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title : biolims.common.attachment,
		type : 2,
		skin : 'layui-layer-lan',
		area : top.screeProportion,
		content : window.ctx
				+ "/operfile/initFileList.action?flag=exp&modelType=sampleReportItemNew&id="
				+ id,
		cancel : function(index, layero) {
			top.layer.close(index)
		}
	})
}

// 上传其他附件
function fileUp3(that) {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	$("#uploadFileValx").fileinput({
		uploadUrl : ctx
				+ "/attachmentUpload?useType=misc&modelType=sampleReportItemNew&contentId="
				+ id,
		uploadAsync : true,
		language : 'zh',
		showPreview : true,
		enctype : 'multipart/form-data',
		elErrorContainer : '#kartik-file-errors'
		,
	});
	// if($("#uploadFile .fileinput-remove").length){
	// $("#uploadFile .fileinput-remove").eq(0).click();
	// }
	$("#uploadFilex").modal("show");
	// var cfileInput = fileInput('1','samplingTaskItem', id);
}
// 其他附件查看
function fileView3(that) {
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title : biolims.common.attachment,
		type : 2,
		skin : 'layui-layer-lan',
		area : top.screeProportion,
		content : window.ctx
				+ "/operfile/initFileList.action?flag=misc&modelType=sampleReportItemNew&id="
				+ id,
		cancel : function(index, layero) {
			top.layer.close(index)
		}
	})
}

function showQualityTest(){
	var rows=$("#reinfusionPlanItemDiv .selected");
	if(rows.length!=1){
		top.layer.msg("请选择一条数据！");
		return false;
	}
	var code=rows.find("td[savename='batch']").text();
	top.layer.open({
		title: "质检结果表",
		type: 2,
		area: [document.body.clientWidth-50,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content: [window.ctx +"/experiment/reinfusionPlan/reinfusionPlan/showQualityTestTable.action?code="+code,''],
		yes: function(index, layer) {
			top.layer.close(index)	
		},
	});
}