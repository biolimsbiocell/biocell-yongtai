//样本出库申请左侧待出库样本
var reinfusionPlanTempTab;
hideLeftDiv1();
function hideLeftDiv1() {
	var stateName = $("#reinfusionPlan_state").attr("state");
	var handlemethod=$("#handlemethod").val();
	if(stateName == "1"||handlemethod=="view") {
		settextreadonly();
		$("#leftDiv").hide();
		$("#rightDiv").attr("class", "col-md-12 col-xs-12");
	}
}
//日期格式化
$("#reinfusionPlan_reinfusionPlanDate").datepicker({
	language: "zh-TW",
	autoclose: true, //选中之后自动隐藏日期选择框
	format: "yyyy-mm-dd" //日期格式，详见 
});
var reinfusionPlan_id = $("#reinfusionPlan_id").text();
$("#save").hide();
$("#tjsp").hide();
$("#sp").hide();
$("#changeState").hide();
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"visible": false,
		"title": "编号",
	});
	colOpts.push({
		"data": "orderCode",
		"visible": false,
		"title": "订单编号",
		"createdCell" : function(td) {
			$(td).attr("saveName", "orderCode");
		}
	});
	colOpts.push({
		"data": "state",
		"visible": false,
		"title": "状态",
		"createdCell" : function(td) {
			$(td).attr("saveName", "state");
		}
	});
	colOpts.push({
		"data" : "code",
		"title" : biolims.common.code,
		"createdCell" : function(td) {
			$(td).attr("saveName", "code");
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : "姓名",
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		}
	});
	colOpts.push({
		"data" : "age",
		"title" : "年龄",
		"createdCell" : function(td) {
			$(td).attr("saveName", "age");
		}
	});
	colOpts.push({
		"data" : "gender",
		"title" : "性别",
		"createdCell" : function(td) {
			$(td).attr("saveName", "gender");
		},
		"render": function(data, type, full, meta) {
			if (data == "1"){
				return "男";
			}else if (data == "0"){
				return "女";
			} else {
				return "未知";
			}
		}
	});
	colOpts.push({
		"data" : "nation",
		"title" : "民族",
		"createdCell" : function(td) {
			$(td).attr("saveName", "nation");
		}
	});
	colOpts.push({
		"data" : "hospital",
		"title" : "医院",
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospital");
		}
	});
	colOpts.push({
		"data" : "hospitalPhone",
		"title" : "医院电话",
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospitalPhone");
		}
	});
	colOpts.push({
		"data" : "note",
		"title" : "备注",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	});
	var tbarOpts = [];
	var state = $("#reinfusionPlan_state").text();
	if(state!="Complete"&&state!="完成"){
		tbarOpts.push({
			text : biolims.common.addToDetail,
			action : function() {
				var sampleId = [];
				$("#reinfusionPlanTempdiv .selected").each(
					function(i, val) {
						sampleId.push($(val).children("td").eq(0).find(
								"input").val()
					);
				});

				// 先保存主表信息
				var note = $("#reinfusionPlan_name").val();
				var createUser = $("#reinfusionPlan_createUser").attr("userId");
				var createDate = $("#reinfusionPlan_createDate").text();
				var id = $("#reinfusionPlan_id").text();
				var reinfusionPlanDate = $("#reinfusionPlan_reinfusionPlanDate").val();
				$.ajax({
					type : 'post',
					url : '/experiment/reinfusionPlan/reinfusionPlan/addReinfusionPlanItem.action',
					data : {
						ids : sampleId,
						note : note,
						id : id,
						createDate : createDate,
						createUser : createUser,
						reinfusionPlanDate:reinfusionPlanDate
					},
					success : function(data) {
						var data = JSON.parse(data)
						if (data.success) {
							$("#reinfusionPlan_id").text(data.data);
							var param = {
								id : data.data
							};
							var reinfusionPlanItemTabs = $("#reinfusionPlanItemdiv").DataTable();
							reinfusionPlanItemTabs.settings()[0].ajax.data = param;
							reinfusionPlanItemTabs.ajax.reload();
							var reinfusionPlanTempTabs = $("#reinfusionPlanTempdiv").DataTable();
							reinfusionPlanTempTabs.ajax.reload();
						} else {
							top.layer.msg('添加失败');
						}
					}
				});
			}
		});
		
		$("#save").show();
		$("#tjsp").show();
		$("#sp").show();
		$("#changeState").show();
	}
	


	tbarOpts.push({
		text : '<i class="fa fa-ioxhost"></i>' + biolims.common.search,
		action : function() {
			search()
		}
	});

	var reinfusionPlanAllOps = table(true, reinfusionPlan_id,
			"/experiment/reinfusionPlan/reinfusionPlan/showReinfusionPlanTempTableJson.action",
			colOpts, tbarOpts);
	reinfusionPlanTempTab = renderData($("#reinfusionPlanTempdiv"),
			reinfusionPlanAllOps);

	// 选择数据并提示
	reinfusionPlanTempTab.on('draw', function() {
		var index = 0;
		$("#reinfusionPlanTempdiv .icheck").on('ifChanged', function(event) {
			if ($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	bpmTask($("#bpmTaskId").val());
});

// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"type" : "table",
		"table" : reinfusionPlanTempTab
	} ];
}

// 大保存
function save() {
	var changeLog = "回输计划:";
	$('input[class="form-control"]').each(
		function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if (val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val
						+ '"变为"' + valnew + '";';
		}
	});
	var jsonStr = JSON.stringify($("#form1").serializeObject());

	var dataItemJson = saveItemjson($("#reinfusionPlanItemdiv"));
	var ele = $("#reinfusionPlanItemdiv");
	var changeLogItems = "回输计划明细：";
	var changeLogItem = "";
	if(changeLogItems != "回输计划明细："){
		changeLogItem = getChangeLog(dataItemJson, ele, changeLogItems);
	}
	var id = $("#reinfusionPlan_id").text();
	var note = $("#reinfusionPlan_name").val();
	var reinfusionPlanDate = $("#reinfusionPlan_reinfusionPlanDate").val();
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	top.layer.load(4, {shade:0.3}); 
	$.ajax({
		url : ctx + '/experiment/reinfusionPlan/reinfusionPlan/savereinfusionPlanAndItem.action',
		dataType : 'json',
		type : 'post',
		data : {
			dataValue : jsonStr,
			changeLog : changeLog,
			ImteJson : dataItemJson,
			bpmTaskId : $("#bpmTaskId").val(),
			changeLogItem : changeLogItem,
			id : id,
			note : note,
			reinfusionPlanDate:reinfusionPlanDate
		},
		success : function(data) {
			if (data.success) {
				var url = "/experiment/reinfusionPlan/reinfusionPlan/toEditReinfusionPlan.action?id=" + data.id;

				url = url + "&bpmTaskId=" + $("#bpmTaskId").val();

				window.location.href = url;
				top.layer.closeAll();
			} else {
				top.layer.msg(data.msg);
				top.layer.closeAll();
			}
		}

	});

}

$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
function tjsp() {
	top.layer
			.confirm(
					biolims.common.pleaseConfirmSaveBeforeSubmit,
					{
						icon : 3,
						title : biolims.common.prompt
					},
					function(index) {
						top.layer
								.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '800px', '500px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/workflow/processinstance/toStartView.action?formName=reinfusionPlan",
									yes : function(index, layer) {
										var datas = {
											userId : userId,
											userName : userName,
											formId : reinfusionPlan_id,
											title : $("#reinfusionPlan_name")
													.val(),
											formName : "reinfusionPlan"
										}
										ajax(
												"post",
												"/workflow/processinstance/start.action",
												datas,
												function(data) {
													if (data.success) {
														top.layer
																.msg(biolims.common.submitSuccess);
														if (typeof callback == 'function') {
															callback(data);
														}
														// dialogWin.dialog("close");
													} else {
														top.layer
																.msg(biolims.common.submitFail);
													}
												}, null);
										top.layer.close(index);
									},
									cancel : function(index, layer) {
										top.layer.close(index)
									}

								});
						top.layer.close(index);
					});

}
function sp() {
	var taskId = $("#bpmTaskId").val();
	var formId = reinfusionPlan_id;

	top.layer.open({
		title : biolims.common.detailedExaminationAndApproval,
		type : 2,
		anim : 2,
		area : [ '800px', '500px' ],
		btn : biolims.common.selected,
		content : window.ctx
				+ "/workflow/processinstance/toCompleteTaskView.action?taskId="
				+ taskId + "&formId=" + formId,
		yes : function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find("iframe").contents()
					.find("#opinionVal").val();

			if (!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if (operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
						reqData, function(data) {
							if (data.success) {
								top.layer.msg(biolims.common.submitSuccess);
								if (typeof callback == 'function') {
								}
							} else {
								top.layer.msg(biolims.common.submitFail);
							}
						}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}

	});
}



//改变状态
function changeState() {
  var	id=$("#reinfusionPlan_id").text()
	var paraStr = "formId=" + id +
		"&tableId=reinfusionPlan";
	console.log(paraStr)
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			var flag=1;
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				if(flag==1){
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: $('.layui-layer-iframe', parent.document).find("iframe").contents().find("input:checked").val(),
					formId: id
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self');;
					} else {
						top.layer.msg(respText);
					}
				}, null)
				flag++;
				top.layer.closeAll();
				}
				
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});

	
}

function showType() {
	top.layer.open({
		title : biolims.user.selectedApplicationType,
		type : 2,
		area : [ document.body.clientWidth - 300,
				document.body.clientHeight - 100 ],
		btn : biolims.common.selected,
		content : [
				window.ctx
						+ "/dic/type/dicTypeSelectTable.action?flag=outApply",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find(
					"#addDicTypeTable .chosed").children("td").eq(0).text();
			top.layer.close(index)
			$("#reinfusionPlan_experimentType_id").val(id);
			$("#reinfusionPlan_experimentType_name").val(name);
		},
	})

}
function list(){
	window.location = window.ctx + '/experiment/reinfusionPlan/reinfusionPlan/showReinfusionPlanList.action';
}