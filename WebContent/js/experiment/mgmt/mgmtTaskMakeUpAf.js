var mgmtTaskMakeUpAfTab;
//已排板样本
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "dicSampleTypeId",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "dicSampleTypeId");
		},
	})
	colOpts.push({
		"data": "dicSampleTypeName",
		"title": biolims.common.dicSampleTypeName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleTypeName");
			$(td).attr("dicSampleTypeId", rowData['dicSampleTypeId']);
		}
	})
	colOpts.push({
		"data": "tempId",
		"title": biolims.common.tempId,
		"visible": false,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "tempId");
		},
	})
	colOpts.push({
		"data": "productNum",
		"title": biolims.common.productNum,
		"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "productNum");
		},
	})
	colOpts.push({
		"data": "posId",
		"title": biolims.common.location,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})
	colOpts.push({
		"data": "counts",
		"title": biolims.common.counts,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "counts");
		},
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.Editplay,
		action: function() {
			editItemLayer($("#mgmtTaskMakeUpAfdiv"))
		}

	})
	tbarOpts.push({
		text: biolims.common.plateAgain,
		action: function() {
			plateLayoutAgain($("#mgmtTaskMakeUpAfdiv"),
				"/experiment/mgmt/mgmtTask/delMgmtTaskItemAf.action")
		}
	})
	tbarOpts.push({
		text: biolims.common.productType,
		action: function() {
			addSampleType();
		}
	})
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveMgmtTaskMakeUpAfTab($("#mgmtTaskMakeUpAfdiv"));
		}
	})
	var mgmtTaskMakeUpAfOps = table(true, mgmtTask_id, "/experiment/mgmt/mgmtTask/showMgmtTaskItemAfTableJson.action", colOpts, tbarOpts);
	mgmtTaskMakeUpAfTab = renderData($("#mgmtTaskMakeUpAfdiv"), mgmtTaskMakeUpAfOps);
	//选择数据并提示
	mgmtTaskMakeUpAfTab.on('draw', function() {
		var index = 0;
		$("#mgmtTaskMakeUpAfdiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
				$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","2px solid #000");
			} else {
				var tt=$("#plateModal").find(".mysample[sid='" + this.value + "']").css("border","1px solid gainsboro");
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
});
//添加样本类型
function addSampleType() {
	var rows = $("#mgmtTaskMakeUpAfdiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.selectSampleType,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/sample/dicSampleType/showDialogDicSampleTypeTable.action", ''],
		yes: function(index, layer) {
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicSampleType .chosed").children("td").eq(
				0).text();
			rows.addClass("editagain");
			rows.find("td[savename='dicSampleTypeName']").attr(
				"dicSampleType-id", id).text(type);
			rows.find("td[savename='dicSampleTypeId']").text(id);

			top.layer.close(index)
		},
	})
}

function plateLayoutAgain(ele, urll) {
	var arr = [];
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.confirm(biolims.common.rearrange + length + biolims.common.record, function(index) {
		top.layer.close(index);
		rows.each(function(i, val) {
			var id = $(val).find("input[type=checkbox]").val();
			arr.push(id);
		});
		if(arr.length) {
			$.ajax({
				type: "post",
				data: {
					ids: arr
				},
				url: ctx + urll,
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						mgmtTaskMakeUpAfTab.ajax.reload();
						mgmtTaskMakeUpTab.ajax.reload();
						$(".mysample").each(function(i, div) {
							var id = $(div).attr("sid");
							arr.forEach(function(val, j) {
								if(id == val) {
									$(div).removeAttr("sid").removeAttr("title").removeClass().css("background-color", "#fff");
								}
							});
						});
					}
				}
			});
		}

	});

}
// 保存
function saveMgmtTaskMakeUpAfTab(ele) {
	var data = saveItemjson(ele);
	$.ajax({
		type: 'post',
		url: '/experiment/mgmt/mgmtTask/saveMakeUp.action',
		data: {
			id: mgmtTask_id,
			dataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 添加样本类型ID
			if(k == "dicSampleTypeName") {
				json["dicSampleTypeId"] = $(tds[j]).attr("dicSampleTypeId");
				continue;
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}