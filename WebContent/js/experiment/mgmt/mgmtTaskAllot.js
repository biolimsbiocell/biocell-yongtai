var mgmtTaskAllotTab;
$(function() {
	var colOpts=[]
	colOpts.push({
		"data": "id",
		"visible": false,
		"title": biolims.common.id,
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
	})
	var tbarOpts=[]
	tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	var options = table(true,null,"/experiment/mgmt/mgmtTask/showMgmtTaskTempTableJson.action",colOpts, tbarOpts)
	//渲染样本的table
	mgmtTaskAllotTab=renderData($("#main"),options);
	
	
	//提示样本选中数量
	var selectednum=$(".selectednum").text();
		if(!selectednum){
			selectednum=0;
	}
	mgmtTaskAllotTab.on('draw', function() {
		$(".selectednum").text(selectednum);
		var index = 0;
		$("tbody .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			$(".selectednum").text(index+parseInt(selectednum));
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
	//办理按钮控制
	bpmTask($("#bpmTaskId").val());
	//sop和实验员的点击操作
	sopAndUserHandler(selectednum);
	//保存操作
	allotSave();
});
//sop和实验员的点击操作
function sopAndUserHandler(selectednum) {
	//执行sop点击选择操作
	$(".sopLi").click(function() {
		var length = $(".selected").length;
		$(this).addClass("sopChosed").siblings("li").removeClass("sopChosed");
		$(".label-warning").removeClass("selectednum").html("");
		$(this).children(".label-warning").addClass("selectednum").text(length+parseInt(selectednum));
	});
	//执行实验员点击选择操作
	$(".userLi").click(function() {
		var length = $(".selected").length;
			var img = $(this).children("img");
			if(img.hasClass("userChosed")) {
				img.removeClass("userChosed");
			} else {
				img.addClass("userChosed");
			}		
	});
}
//保存操作
function allotSave() {
	$("#save").click(function() {
		if(!$(".sopChosed").length) {
			top.layer.msg(biolims.common.selectTaskModel);
			return false;
		}
		if(!$(".userChosed").length) {
			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
			return false;
		}
		//获得选中样本的ID
		var sampleId = [];
		$(".selected").each(function(i, val) {
			sampleId.push($(val).children("td").eq(0).find("input").val());
		});
		//获得选中实验员的ID
		var userId = [];
		$(".userChosed").each(function(i, val) {
			userId.push($(val).parent("li").attr("userid"));
		});
		//拼接主表的信息
		var main = {
			id: $("#mgmtTask_id").text(),
			name: $("#mgmtTask_name").val(),
			"createUser-id": $("#mgmtTask_createUser").attr("userId"),
			createDate: $("#mgmtTask_createDate").val(),
			state: $("#mgmtTask_state").attr("state"),
			stateName: $("#mgmtTask_state").text(),
			sampleNum:$(".selectednum").text(),
			maxNum:$("#maxNum").val()
		};
		var data = JSON.stringify(main);
		//拼接保存的data
		var info = {
			temp: sampleId,
			template: $(".sopChosed").attr("sopid"),
			user: userId.join(","),
			main: data
		}
		$.ajax({
		type : 'post',
		url:ctx+"/experiment/mgmt/mgmtTask/saveAllot.action",
		data:info,
		success:function(data){
			var data = JSON.parse(data);
			if(data.success){				
				top.layer.msg(biolims.common.saveSuccess);
				window.location = window.ctx
			+ '/experiment/mgmt/mgmtTask/editMgmtTask.action?id=' + data.id;
			}
		}
		})
	});
}
//下一步操作
$("#next").click(function () {
	var mgmtTask_id=$("#mgmtTask_id").text();
	if(mgmtTask_id=="NEW"){
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/mgmt/mgmtTask/showMgmtTaskItemTable.action?id="+mgmtTask_id+"&bpmTaskId="+$("#bpmTaskId").val();
});
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"type" : "table",
		"table" : mgmtTaskAllotTab
	} ];
}
//biolims.common.selected
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layer) {
			var arr = $("#checkCode",parent.document).val().split("\n");
			mgmtTaskAllotTab.settings()[0].ajax.data = {"codes":arr};
			mgmtTaskAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});

}
