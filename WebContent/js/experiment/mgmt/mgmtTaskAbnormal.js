var mgmtTaskAbnormalTab;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "orderId",
		"title": biolims.common.orderId,
		"createdCell": function(td) {
			$(td).attr("saveName", "orderId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "concentration",
		"title": biolims.common.concentration,
		"createdCell": function(td) {
			$(td).attr("saveName", "concentration");
		}
	})
	colOpts.push({
		"data": "volume",
		"title": biolims.common.volume,
		"createdCell": function(td) {
			$(td).attr("saveName", "volume");
		}
	})
	colOpts.push({
		"data": "result",
		"title": biolims.common.result,
		"name":biolims.common.qualified+"|"+biolims.common.disqualified,
		"createdCell": function(td) {
			$(td).attr("saveName", "result");
			$(td).attr("selectOpt", biolims.common.qualified+"|"+biolims.common.disqualified);
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return biolims.common.disqualified;
			}
			if(data == "1") {
				return biolims.common.qualified;
			}
		}
	})
	colOpts.push({
		"data": "nextFlowId",
		"title": biolims.common.nextFlowId,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlowId");
		}
	})
	colOpts.push({
		"data": "nextFlow",
		"title": biolims.common.nextFlow,
		"createdCell": function(td) {
			$(td).attr("saveName", "nextFlow");
		}
	})
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
		"className":"edit",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
		}
	})
	var tbarOpts = [];
	
	tbarOpts.push({
		text: biolims.common.search,
		action: function() {
			search();
		}
	});
	tbarOpts.push({
		text: biolims.common.order,
		action: function() {
			viewMgmtTask();
		}
	});
	tbarOpts.push({
		text: biolims.common.selectNextFlow,
		action: function() {
			nextFlow();
		}
	});
	var mgmtTaskAbnormalOps = table(true, null, "/experiment/mgmt/mgmtTaskAbnormal/showMgmtTaskAbnormalTableJson.action", colOpts, tbarOpts);
	mgmtTaskAbnormalTab = renderData($("#mgmtTaskAbnormaldiv"), mgmtTaskAbnormalOps);
	//选择数据并提示
	mgmtTaskAbnormalTab.on('draw', function() {
		var index = 0;
		$("#mgmtTaskAbnormaldiv .icheck").on('ifChanged', function(event) {
			if($(this).is(':checked')) {
				index++;
			} else {
				index--;
			}
			top.layer.msg(biolims.common.Youselect + index + biolims.common.data);
		});
	});
});
//下一步流向
function nextFlow() {
	var rows = $("#mgmtTaskAbnormaldiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	var productId = "";
	$.each(rows, function(j, k) {
		productIds = $(k).find("td[savename='productId']").text();
	});
	top.layer.open({
		title:biolims.common.selectNextFlow,
		type:2,
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content:[window.ctx+"/system/nextFlow/nextFlow/shownextFlowDialogTable.action?model=MgmtTask&productId="
					+ productIds,''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td")
			.eq(1).text();
		var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(
			0).text();
		rows.addClass("editagain");
		rows.find("td[savename='nextFlow']").text(name);
		rows.find("td[savename='nextFlowId']").text(id);

		top.layer.close(index)
		},
	})
}

// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//查看任务单
function viewMgmtTask(){
	var id = $(".selected").find("td[savename='orderId']").text();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/mgmt/mgmtTask/showMgmtTaskSteps.action?id=" + id;
}
function executeAbnormal(){
	var rows = $("#mgmtTaskAbnormaldiv .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var ids = [];
	$.each(rows, function(j, k) {
		ids.push($(k).find(".icheck").val());
	});
	var data = saveItemjson($("#mgmtTaskAbnormaldiv"));
	$.ajax({
		type: 'post',
		url: '/experiment/mgmt/executeAbnormal.action',
		data: {
			ids: ids,
			dataJson: data
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.Executesuccessfully);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.Onfailure)
			};
		}
	})
}
//弹框模糊查询参数
function searchOptions() {
	return [
		{
			"txt": biolims.common.sampleCode,
			"type": "input",
			"searchName": "sampleCode"
		},{
			"txt": biolims.common.code,
			"type": "input",
			"searchName": "code"
		},
		{
			"txt": biolims.common.orderId,
			"type": "input",
			"searchName": "orderId",
		},
	{
		"type":"table",
		"table":mgmtTaskAbnormalTab
	}];
}
