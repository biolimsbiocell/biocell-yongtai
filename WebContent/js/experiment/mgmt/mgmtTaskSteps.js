/* 
 * 文件名称 :mgmtTaskSteps.js
 * 创建者 : 
 * 创建日期: 
 * 文件描述: 
 * 
 */
var mgmtTask_id = $("#mgmtTask_id").text();
$(function() {
	//请求步骤的数据
	$.ajax({
		type: "post",
		data: {
			id: mgmtTask_id
		},
		url: ctx + "/experiment/mgmt/mgmtTask/showMgmtTaskStepsJson.action",
		success: function(data) {
			//console.log(data);
			var data = JSON.parse(data);
			var list = data.template;
			var reagent = data.reagent;
			var cos = data.cos;
			var plate = data.plate;
			//生成步骤
			var stepsLi = "<li><input class='content hide' value=" + list[0].content + "><input class='contentdata hide' value=" + list[0].contentData + "><em class='hide'>" + list[0].note + "</em><a class='selected step' stepid=" + list[0].id + "><span class='step_no'>" + list[0].orderNum + "</span><p class='step_descr'> " + list[0].name + "</p></a></li>";
			list.forEach(function(v, i) {
				if(i > 0) {
					var index = i + 1;
					stepsLi += "<li><input class='content hide' value=" + v.content + "><input class='contentdata hide' value=" + v.contentData + "><em class='hide'>" + v.note + "</em><a class='disabled step' stepid=" + list[0].id + "><span class='step_no'>" + v.orderNum + "</span><p class='step_descr'> " + v.name + "</p></a></li>";
				}
			});
			$(".wizard_steps").append(stepsLi);
			//默认渲染第一个步骤名称
			$("#steptiele").text(list[0].name);
			//默认渲染第一个步骤步骤明细（HTML代码）
			$("#stepcontent").html(list[0].note);
			$("#startTime").html(list[0].startTime);
			$("#endTime").html(list[0].endTime);
			//默认渲染第一个步骤实验记录（自定义字段）
			//console.log(list[0].contentData);
			renderContentData(list[0].content, list[0].contentData);
			//默认渲染第一个步骤原辅料
			renderReagentAndCos(reagent, cos);
			//生成孔板的box
			plate.forEach(function(v, i) {
				if(i > 0) {
					var clonePlateDiv = $("#plateDiv").eq(0).clone();
					clonePlateDiv.find(".box-titlem").text(v[0]);
					clonePlateDiv.find(".box-titlesub").text(v[1]);
					$("#plateModal").append(clonePlateDiv);
				} else {
					$("#plateDiv").find(".box-titlem").text(v[0]);
					$("#plateDiv").find(".box-titlesub").text(v[1]);
				}
			});
			renderPlate();
			//为每个步骤注册点击事件
			$(".wizard_steps .step").click(function() {
				$(".wizard_steps .step").removeClass("selected").addClass("disabled");
				$(this).removeClass("disabled").addClass("selected");
				//变步骤名称
				$("#steptiele").text($(this).children(".step_descr").text());
				//变步骤详情（HTML代码）
				$("#stepcontent").html($(this).siblings("em").html());
				//变实验记录
				renderContentData($(this).siblings(".content").val(), $(this).siblings(".contentdata").val());
				//变原辅料
				$.ajax({
					type: "post",
					data: {
						code: $(this).children(".step_no").text(),
						id: mgmtTask_id
					},
					url: ctx + "/experiment/mgmt/mgmtTask/showMgmtTaskStepsJson.action",
					success: function(data) {

						var data = JSON.parse(data);
						var reagent = data.reagent;
						var cos = data.cos;
						var list = data.template;
						$("#startTime").html(list[0].startTime);
						$("#endTime").html(list[0].endTime);
						renderReagentAndCos(reagent, cos);
					}
				});
				//变设备
			});
			//为步骤明细的HTML设置动画
			$("#stepContentBtn").click(function() {
				if($(this).hasClass("xxx")) {
					$(this).removeClass("xxx");
					$("#stepContentModer").animate({
						"height": "10%"
					}, 800, "swing", function() {
						$(this).animate({
							"width": "0%",
							"height": "0%",
						}, 600, "linear");
					});
				} else {
					$(this).addClass("xxx");
					$("#stepContentModer").animate({
						"width": "100%",
						"height": "10%"
					}, 500, "swing", function() {
						$(this).animate({
							"height": "90%"
						}, 600, "linear");
					});
				}
			});
		}
	});
	preAndNext();
	//原辅料的扫码
	ReagentScanCode();
	//设备的扫码
	cosScanCode();
	bpmTask($("#bpmTaskId").val());
});
//渲染实验记录（自定义字段）
function renderContentData(content, contentData) {
	//console.log(content);
	var inputs = '';
	var content = JSON.parse(content);
	content.forEach(function(v, i) {
		var disabled = v.readOnly ? "disabled" : ' ';
		var defaultValue=v.defaultValue?v.defaultValue:' ';
		if(v.type == "checkbox") {
			var checkboxs = '';
			v.singleOption.forEach(function(vv, jj) {
				checkboxs += '<input type=' + v.type + ' name=' + v.fieldName + ' value=' + vv.itemValue + '>' + vv.itemName + '';
			});
			inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span>' + checkboxs + '</div></div>';
		} else if(v.type == "radio") {
			var options = '';
			v.singleOption.forEach(function(vv, jj) {
				options += '<option name=' + v.fieldName + ' value=' + vv.itemValue + '>' + vv.itemName + '</option>';
			});
			inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><select class="form-control">' + options + '</select></div></div>';
		} else if(v.type == "date") {
			inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><input type="text" name=' + v.fieldName + ' required=' + v.required + ' ' + disabled + ' class="form-control datepick" value=' + defaultValue + '></div></div>';
		} else {
			inputs += '<div class="col-sm-6 col-xs-12"><div class="input-group"><span class="input-group-addon">' + v.label + '</span><input type=' + v.type + ' name=' + v.fieldName + ' required=' + v.required + '   class="form-control" ' + disabled + ' value=' + defaultValue + '></div></div>';
		}
	});
	$("#contentData").html(inputs);
	//日期格式化
	$("#contentData").find('.datepick').datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	//赋值哟
	if(contentData && contentData != "null") {
		var contentData = JSON.parse(contentData);
		for(var k in contentData) {
			$("#contentData input").each(function(i, inp) {
				if(inp.name == k) {
					if(inp.type == "checkbox") {
						if(contentData[k].indexOf(inp.value)!=-1) {
							inp.setAttribute("checked", true);
						}
					} else {
						inp.value = contentData[k];
						return false;
					}
				}
			});
			$("#contentData option").each(function(i, val) {
				if(k == val.getAttribute("name")) {
					if(val.value == contentData[k]) {
						val.setAttribute("selected", true);
						return false;
					}
				}
			});
		};
	}

	//多选框 格式化
	$("#contentData").find('input[type="checkbox"]').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		increaseArea: '20%' // optional
	});
}
//渲染原辅料和设备
function renderReagentAndCos(reagent, cos) {
	var reagentLis = "";
	reagent.forEach(function(v, i) {
		var batch = v.batch ? v.batch : "";
		var sn = v.sn ? v.sn : "";
		var code = v.code ? v.code : "";
		reagentLis += '<li class="reagli" reagentid=' + v.id + ' code=' + code + ' itemid=' + v.itemId + ' ><span><i class="fa fa-search-plus"></i></span><i class="fa fa-trash pull-right"></i><i class="fa fa-copy pull-right" style="margin-right: 5px;"></i><p class="text">' + v.name + '</p><small class="label label-primary">批次:<span>' + batch + '</span></small><small class="label label-info">sn:<span>' + sn + '</span></small></li>'
	});
	$("#reagentBody").html(reagentLis);
	var cosLis = "";
	cos.forEach(function(v, i) {
		var name = v.name ? v.name : "";
		var code = v.code ? v.code : "";
		cosLis += '<li class="cosli" cosid=' + v.id + ' itemid=' + v.itemId + '  typeid=' + v.type.id + ' code=' + code + '  ><span><i class="fa fa-search-plus"></i></span><i class="fa fa-trash pull-right"></i><i class="fa fa-copy pull-right" style="margin-right: 5px;"></i><p class="text">' + v.type.name + '</p><small class="label label-default">' + name + '</small></li>'
	});
	$("#cosBody").html(cosLis);
	//原辅料和设备复制操作
	reagentAndCosCopy();
	//原辅料和设备删除操作
	reagentAndCosRemove();
	//选择原辅料和设备
	reagentAndCosChose();
}
//原辅料和设备复制操作
function reagentAndCosCopy() {
	$(".fa-copy").unbind("click").click(function() {
		var li = $(this).parent("li").clone();
		//复制的是原辅料
		if(li.hasClass("reagli")) {
			li.attr("reagentid", ""); //id为空
			li.find(".label-primary span").text(""); //批次为空
			li.find(".label-info span").text(""); //sn为空
		}
		//复制的是设备
		if(li.hasClass("cosli")) {
			li.attr({
				"cosid": "",
				"code": ""
			}); //id为空
			li.children(".label-default").text(""); //设备名称为空
		}
		$(this).parent("li").after(li);
		reagentAndCosCopy();
		reagentAndCosRemove();
	});
}

//原辅料和设备删除操作
function reagentAndCosRemove() {
	$(".fa-trash").unbind("click").click(function() {
		var li = $(this).parent("li");
		if(li.hasClass("reagli")) {
			var id = li.attr("reagentid");
			if(id) {
				$.ajax({
					type: "post",
					url: ctx + "/experiment/mgmt/mgmtTask/delMgmtTaskReagent.action",
					data: {
						id: id
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							top.layer.msg(biolims.common.deleteSuccess);
						}
					}
				});
			} else {
				li.remove();
			}
		}
		if(li.hasClass("cosli")) {
			var id = li.attr("cosid");
			if(id) {
				$.ajax({
					type: "post",
					url: ctx + "/experiment/mgmt/mgmtTask/delMgmtTaskReagent.action",
					data: {
						id: id
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							top.layer.msg(biolims.common.deleteSuccess);
						}
					}
				});
			} else {
				li.remove();
			}
		}
	});
}

//原辅料和设备选择操作
function reagentAndCosChose() {
	$(".fa-search-plus").unbind("click").click(function() {
		var item = $(this).parents("li");
		if(item.hasClass("reagli")) {
			addReagent(item.attr("code"), item);
		}
		if(item.hasClass("cosli")) {
			addCos(item.attr("typeid"), item);
		}
	});
}

//选择原辅料批次
function addReagent(id, item) {
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		offset: ['10%', '10%'],
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/storage/getStrogeReagent.action?id=" + id + "", ""],
		yes: function(index, layer) {
			var batch = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addReagent .chosed").children("td").eq(2).text();
			var sn = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addReagent .chosed").children("td").eq(4).text();
			item.find(".label-primary span").text(batch);
			item.find(".label-info span").text(sn);
			top.layer.close(index)
		},
	});
}
//选择设备批次
function addCos(id, item) {
	//console.log()
	top.layer.open({
		title: biolims.common.selectInstrument,
		type: 2,
		offset: ['10%', '10%'],
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/equipment/main/selectCos.action?typeId=" + id + "", ""],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCos .chosed").children("td")
				.eq(1).text();
			var code = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addCos .chosed").children("td").eq(
				0).text();
			item.find(".label-default").text(name);
			item.attr("code", code);
			top.layer.close(index)
		},
	})
}

//原辅料的扫码
function ReagentScanCode() {
	$("#reagentScanInput").keypress(function(e) {
		var e = e || window.event;
		if(e.keyCode == "13") {
			$.ajax({
				type: "post",
				data: {
					sn: this.value
				},
				url: ctx + "/storage/getStorageBySn.action",
				success: function(data) {
					var data = JSON.parse(data).soi;
					var batch = data.code;
					var sn = data.note;
					var id = data.storage.id;
					$(".reagli").each(function(i, v) {
						if(v.getAttribute('code') == id) {
							if(!$(v).find(".label-primary span").text()) {
								$(v).find(".label-primary span").text(batch);
								$(v).find(".label-info span").text(sn);
								return false;
							}
						}
					});
				}
			});
		}
	})

}
//设备的扫码
function cosScanCode() {
	var e = e || window.event;
	$("#cosScanInput").keypress(function(e) {
		var e = e || window.event;
		if(e.keyCode == "13") {
			$.ajax({
				type: "post",
				data: {
					id: this.value
				},
				url: ctx + "/equipment/main/getCosById.action",
				success: function(data) {
					var data = JSON.parse(data).ins;
					var code = data.id;
					var name = data.name;
					var id = data.type.id;
					$(".cosli").each(function(i, v) {
						if(v.getAttribute('typeid') == id) {
							if(!$(v).find(".label-default").text()) {
								$(v).find(".label-default").text(name);
								v.setAttribute("code", code);
								return false;
							}
						}
					});
				}
			});
		}
	})
}

//获取样本并布置到孔板   
function renderPlate() {
	var overlay = '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>';
	$(".showPlate").click(function() {
		if($(this).children(".fa").hasClass("fa-plus")) {
			var plateDiv = $(this).parents("#plateDiv");
			var ele = plateDiv.find(".plate");
			if(!ele.children("tr").length) {
				var ele = plateDiv.find(".plate");
				var eletable = plateDiv.find("#mgmtTaskSample");
				plateDiv.append(overlay);
				var counts = plateDiv.find('.box-titlem').text();
				$.ajax({
					type: "post",
					data: {
						id: mgmtTask_id,
						counts: counts
					},
					url: ctx + "/experiment/mgmt/mgmtTask/plateSample.action",
					success: function(data) {
						//console.log(data);
						var data = JSON.parse(data);
						var list = data.list;
						showSamoleTable(counts, eletable);
						createShelf(data.rowNum, data.colNum, ele);
						showPlate(list, ele);
						ele.parents("#plateDiv").find(".overlay").remove();
					}
				});
			}
		}
	});
	$(".addSamp").click(function() {
		var plateDiv = $(this).parents("#plateDiv");
		var eletr = plateDiv.find("#mgmtTaskSample").find("tbody").find("tr");
		if(!eletr.length) {
			return false;
		}
		var titlePlate = plateDiv.find(".box-titlem").text();
		var arr = [];
		eletr.each(function(i, tr) {
			if($(tr).hasClass("selected")) {
				arr.push(titlePlate + "-" + $(tr).children("td[savename='code']").text());
			}
		});
		$("#contentData").find("input[name='sampleCodes']").val(arr.join(","));
	});
}

/** 
 * 根据要求生成要求规格的架子或盒子
 * @param  m => 行
 * @param  n => 列
 */
function createShelf(m, n, element) {
	element.empty();
	var m = parseInt(m);
	var n = parseInt(n);
	//$(element).empty();
	var arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
	var tr0 = document.createElement("tr");
	var num = "";
	for(var i = 0; i <= n; i++) {
		num += '<td>' + i + '</td>';
	}
	tr0.innerHTML = num;
	tr0.children[0].innerHTML = "X";
	$(element).append(tr0);
	var h = 0;
	for(var i = 0; i < m; i++) {
		var tr = document.createElement("tr");
		var tds = "";
		var x = i + 1;
		for(var j = 0; j <= n; j++) {
			var jj = j < 10 ? "0" + j : j;
			tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m) + ' coord=' + arr[i] + jj + '></div></td>';
			x += m;
		}
		tr.innerHTML = tds;
		tr.children[0].innerHTML = arr[i];
		$(element).append(tr);
	}
	var divs = $(element).find("div");
	for(var i = 0; i < divs.length; i++) {
		var h = i + 1;
		divs[i].setAttribute("h", h);
	}
}

//渲染已保存到孔板上的样本
function showPlate(list, ele) {
	for(var i = 0; i < list.length; i++) {
		var platePoint = ele.find("div[coord='" + list[i].posId + "']")[0];
		platePoint.style.backgroundColor = "#007BB6";
		platePoint.setAttribute("title", list[i].code);
	}
}
//渲染样本的datatables
function showSamoleTable(counts, eletable) {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "dicSampleTypeId",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "dicSampleTypeId");
		},
	})
	colOpts.push({
		"data": "dicSampleTypeName",
		"title": biolims.common.dicSampleTypeName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleTypeName");
			$(td).attr("dicSampleType-id", rowData['dicSampleTypeId']);
		}
	})
	colOpts.push({
		"data": "posId",
		"title": biolims.common.location,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})
	colOpts.push({
		"data": "counts",
		"title": biolims.common.counts,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "counts");
		},
	})
	var tbarOpts = [];
	var mgmtTaskMakeUpAfOps = table(true, mgmtTask_id, "/experiment/mgmt/mgmtTask/plateSampleTable.action?counts=" + counts + "", colOpts, tbarOpts);
	var mgmtTaskMakeUpAfTab = renderData(eletable, mgmtTaskMakeUpAfOps);
}

//保存数据
function saveStepItem() {
	//拼自定义字段（实验记录）
	var inputs = $("#contentData input");
	var options = $("#contentData option");
	var contentData = {};
	var checkboxArr = [];
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type == "checkbox") {
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		} else {
			contentData[k] = inp.value;
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
	//拼原辅料的数据
	var reagentli = $("#reagentBody li");
	var reagent = [];
	reagentli.each(function(i, val) {
		var reagentItem = {};
		reagentItem.id = val.getAttribute("reagentid");
		reagentItem.name = $(val).children(".text").text();
		reagentItem.code = val.getAttribute("code");
		reagentItem.itemId = val.getAttribute("itemid");
		reagentItem.batch = $(val).children(".label-primary").children("span").text();
		reagentItem.sn = $(val).children(".label-info").children("span").text();
		reagent.push(reagentItem);
	});
	//拼设备的数据
	var cosli=$("#cosBody li");
	var cos=[];
	cosli.each(function (i,val) {
		var cosItem={};
		cosItem.id=val.getAttribute("cosid"); 
		cosItem.name=$(val).children(".label-default").text();
		cosItem.code=val.getAttribute("code"); 
		cosItem.itemId=val.getAttribute("itemid"); 
		cosItem.typeId=val.getAttribute("typeid"); 
		cos.push(cosItem);
	});

	template = {
		id: $(".wizard_steps .selected").attr("stepid"),
		contentData:contentData,
		startTime: $("#startTime").text(),
		endTime: $("#endTime").text()
	};
	template = JSON.stringify(template);
	reagent = JSON.stringify(reagent);
	cos = JSON.stringify(cos);

	$.ajax({
		type: "post",
		url: ctx + "/experiment/mgmt/mgmtTask/saveSteps.action",
		data: {
			id: $("#mgmtTask_id").text(),
			templateJson: template,
			reagentJson: reagent,
			cosJson: cos
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
			}
		}
	});

}
//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/mgmt/mgmtTask/showMgmtTaskItemTable.action?id=" + mgmtTask_id+"&bpmTaskId="+$("#bpmTaskId").val();
	});
	//下一步操作
	$("#next").click(function() {
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/mgmt/mgmtTask/showMgmtTaskResultTable.action?id=" + mgmtTask_id+"&bpmTaskId="+$("#bpmTaskId").val();
	});
}