var qpcrTaskTab;
$(function() {
	var options = table(true, "",
			"/experiment/qpcr/qpcrTask/showQpcrTaskTableJson.action", [ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "name",
				"title" : biolims.common.name,
			}, {
				"data" : "createUser-name",
				"title" : biolims.sample.createUserName,
			}, {
				"data" : "createDate",
				"title" : biolims.sample.createDate,
			}, {
				"data" : "confirmDate",
				"title" : biolims.common.confirmDate,
			}, {
				"data" : "testUserOneName",
				"title" : biolims.common.testUserName,
			}, {
				"data" : "template-name",
				"title" : biolims.common.experimentModule,
			}, {
				"data" : "scopeName",
				"title" : biolims.purchase.costCenter,
			}, {
				"data" : "stateName",
				"title" : biolims.common.stateName,
			} ], null)
	qpcrTaskTab = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(qpcrTaskTab);
	})
});

function add() {
	window.location = window.ctx
			+ '/experiment/qpcr/qpcrTask/editQpcrTask.action?sampleStyle='
			+ $("#sampleStyle").val();
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/experiment/qpcr/qpcrTask/editQpcrTask.action?id=' + id
			+ "&sampleStyle=" + $("#sampleStyle").val();
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx
			+ "/experiment/qpcr/qpcrTask/showQpcrTaskSteps.action?id="
			+ id;
}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	}, {
		"txt" : biolims.sample.createDateStart,
		"type" : "dataTime",
		"searchName" : "createDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.sample.createDateEnd,
		"type" : "dataTime",
		"searchName" : "createDate##@@##2",
		"mark" : "e##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"searchName" : "confirmDate##@@##1",
		"mark" : "s##@@##",
	}, {
		"txt" : biolims.common.confirmDateStart,
		"type" : "dataTime",
		"mark" : "e##@@##",
		"searchName" : "confirmDate##@@##2"
	}, {
		"type" : "table",
		"table" : qpcrTaskTab
	} ];
}
