/* 
 * 文件名称 :reagentTaskSteps.js
 * 创建者 : 
 * 创建日期: 
 * 文件描述: 
 * 
 */
var cellPassage_id = $("#cellPassage_id").text();
var isSeparate = $("#cellPassage_isSeparate").val();
var changelogRea, changelogCos;
var oldChangeLog;
var flg = false;
var sUserAgent = navigator.userAgent.toLowerCase(); 
var isIpad = sUserAgent.match(/ipad/i) == "ipad";
if(isSeparate == 1) {
	flg = true;
}

//能否保存
var baocun = true;
//是不是操作员
var caozuo = true;


$(function() {
	if(isIpad){
		$('.ipddd').css({
			"height":"700px",
			"overflow-y":"auto"
		});
	   // 
	   
	}
	var dijibu1 = sessionStorage.getItem("dijibu");
	if (dijibu1) {
		var orderNums1 = dijibu1;
	} else {
		var orderNums1 = $("#orderNumBy").val();
	}
	$.ajax({
		type : 'post',
		url : ctx + '/experiment/reagent/reagentTask/serchInfos.action',
		async : false,
		data : {
			mainId : cellPassage_id,
			orderNums1 : orderNums1
		},
		success : function(data) {
			var data = JSON.parse(data);
			if (data.sueccess) {
				// 能否保存
				baocun = data.baocun;
			} else {
				// 查询报错 没想好要干嘛
			}
		}
	});
	
	
	
	
	// 箭头点击
	arrowClick();
	// 请求步骤的数据
	renderSteps();
	// 上一步下一步点击
	preAndNext();
	bpmTask($("#bpmTaskId").val());
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view" ||
		$("#cellPassage_state").text() == biolims.common.finish) {
		settextreadonly();
		$("#save").hide()
		$("#tjsp").hide()
		
	}
	// 判断如果是列表页进来的，直接跳到当前步骤
	renderOrderNums();
});

//

//


// 请求步骤数据
function renderSteps() {
	$.ajax({
		type: "post",
		data: {
			id: cellPassage_id
		},
		async: false,
		url: ctx +"/experiment/reagent/reagentNow/findTempleItemNumber.action",
		success: function(data) {
			var list = JSON.parse(data).data;
			// 生成步骤
			var stepsLi = "";
			list.forEach(function(v, i) {
				var index = i + 1;
				stepsLi += "<li><a class='disabled step' stepid=" +
					v.id + " code=" + v.orderNum +
					"><span class='step_no'>" + index +
					"</span><p class='step_descr'> " + v.name +
					"</p><p class='estimatedDate'>预计用时:" +
					v.today + "天</p></a></li>";
			});
			$(".wizard_steps").append(stepsLi);
			// 为每个步骤注册点击事件
			$(".wizard_steps .step").click(
					function() {
						$(".wizard_steps .step").removeClass("selected").addClass("disabled");
						$(this).removeClass("disabled").addClass("selected");
						// 变步骤名称
						$("#steptiele").text($(this).children(".step_descr").text());
						// 变步骤详情（HTML代码）
						$("#stepcontent").html("xxx");
						$(".bwizard-steps>li:eq(0)>a").click();
					});
			$(".wizard_steps .step:eq(0)").click();
			// 为步骤明细的HTML设置动画
			$("#stepContentBtn").click(function() {
				if($(this).hasClass("xxx")) {
					$(this).removeClass("xxx");
					$("#stepContentModer").animate({
						"height": "10%"
					}, 800, "swing", function() {
						$(this).animate({
							"width": "0%",
							"height": "0%",
						}, 600, "linear");
					});
				} else {
					$(this).addClass("xxx");
					$("#stepContentModer").animate({
						"width": "100%",
						"height": "10%"
					}, 500, "swing", function() {
						$(this).animate({
							"height": "90%"
						}, 600, "linear");
					});
				}
			});
		}
	});

}
// 箭头点击
function arrowClick() {
	
	$(".bwizard-steps>li").click(function() {
		
				var stepId = $(this).attr("note");
				var orderid = $(".wizard_steps .selected").attr("stepid");
				var orderNum = $(".wizard_steps .selected .step_no").text();
				
				$.ajax({
					type : 'post',
					url : ctx
							+ '/experiment/reagent/reagentTask/serchInfos.action',
					async : false,
					data : {
						mainId : cellPassage_id,
						orderNums1 : orderNum
					},
					success : function(data) {
						var data = JSON.parse(data);
						if (data.sueccess) {
							if (data.baocun) {
								baocun = true;
							} else {
								baocun = false;
							}

							if (baocun) {
								if (caozuo) {
									$("#save").show();
								} else {
									$("#save").hide();
								}
							} else {
								$("#save").hide();
							}
						} else {
							// 查询报错 没想好要干嘛
						}
					}
				});
				
				if(stepId == "1") {
					var stepNum = $(".wizard_steps .selected .step_no").text(); // 当前得步骤数
					// 判断按钮显示隐藏 ajax判断 当前步骤是否是该生产的最后一步
					// 并且继续生产按钮名称改为完成生产操作
					$.ajax({
						type : 'post',
						async : false,
						url : ctx
								+ '/experiment/reagent/reagentTask/changeButtonBlock.action',
						data : {
							mainId : cellPassage_id,
							stepNum : stepNum,
						},
						success : function(data) {
							var data1 = JSON
									.parse(data);
							if (data1.success) {
								if (data1.zhyb) {
									$("#jxsc").val("完成生产操作");
									$("#cc").show();
								}else{
									$("#cc").hide();
								}
								
							} else {

							}
						}
					});
					// 渲染工前准备
					if (baocun) {
						if (caozuo) {
							$("#qrsbfj").show();
							$("#ksgqzb").show();
						} else {
							$("#qrsbfj").hide();
							$("#ksgqzb").hide();
						}
					} else {
						$("#qrsbfj").hide();
						$("#ksgqzb").hide();
					}
					var action = "/experiment/reagent/reagentNow/findNumberNstructionsListinfo.action";
				} else if(stepId == "2") { // 渲染质检
					var action = "/experiment/reagent/reagentNow/findZhijianItemInfo.action";
				} else if(stepId == "3") { // 渲染BL操作
					// 判断是否选择设备
//					var arr = [];
//					var fuyong = true;
//
//					$("#pageOneChoseYQ .btn-info").each(function(i, v) {
//								var jsonn = {};
//								jsonn.id = v.getAttribute("cosid");
//								jsonn.code = v.getAttribute("code");
//								jsonn.name = v.getAttribute("name");
//
//								if (jsonn.name != null
//										&& jsonn.name != "null"
//										&& jsonn.name != "") {
//								} else {
//									fuyong = false;
//									return false;
//								}
//								arr.push(jsonn);
//							});
//					if (fuyong == false) {
//						top.layer.msg("请选择设备");
//						return false;
//					}
					// 判断是否选择
//					if ($("#operatingRoomName").val() != null
//							&& $("#operatingRoomName").val() != "") {
//					} else {
//						top.layer.msg("请选择房间");
//						return false;
//					}
					var action = "/experiment/reagent/reagentNow/findCosAndReagentInfo.action";
				} else if(stepId == "4") { // 渲染完工清场
					
					var action = "/experiment/reagent/reagentNow/findNstructionsEndListinfo.action";
				}
				$.ajax({
					type: "post",
					url: action,
					async:false,
					data: {
						id: cellPassage_id,
						orderNum: orderNum,
						stepId: stepId,
						orderid: orderid
					},
					success: function(res) {
						//console.log(res);
						var data = JSON.parse(res);
						if(stepId == "1") {
							// 渲染操作指令
							renderOperationalOrder(data.cellPass, stepId);
							// // 渲染生产记录
							// renderProductionRecord(data.cellReco.templeItem,
							// data.cellReco, stepId);
							// 渲染生产记录
							renderProductionRecord(
									data.cellReco.templeItem,
									data.cellReco, stepId,
									data.scjz);
							// 渲染选择设备
							$.ajax({
										type : "post",
										data : {
											id : cellPassage_id,
											stepNum : orderNum,
										},
										url : "/experiment/reagent/reagentTask/findProductCos.action",
										success : function(res) {
											// console.log(res);
											var data = JSON.parse(res).ReagentTaskCossNew;
											var trs = '';
											if (data.length) {
												data.forEach(function(v,i) {
															var name = v.instrumentName ? "选择设备:"
																	+ (v.instrumentName == null
																			|| v.instrumentName == undefined
																			|| v.instrumentName == "null" ? ""
																			: v.instrumentName)
																	: "选择设备";
															trs += '<tr><td>'
																	+ v.cellProduc.templeCell.producingTitle
																	+ '</td><td>'
																	+ v.templateCos.typeName
																	+ '</td><td>'
																	+ (v.instrumentCode==null?"空":v.instrumentCode)
																	+ '</td><td><button class="btn btn-xs btn-info" id="'
																	+ v.templateCos.id
																	+ '" typeid="'
																	+ v.templateCos.typeId
																	+ '" cosid="'
																	+ v.id
																	+ '" code="'
																	+ v.instrumentCode
																	+ '" name="'
																	+ v.instrumentName
																	+ '" onclick="addCos1(this)">'
																	+ name
																	+ '</button></td></tr>';
														})
												$("#pageOneChoseYQ").html(trs);
											} else {
												$("#pageOneChoseYQ").html('<tr><td colspan="3" style="    text-align: center;font-size: 14px; font-weight: 700;">生产模板未配置设备</td></tr>');
											}

										}
									});
						
						} else if(stepId == "2") {	
							if ($("#noBlendTable_wrapper").length) {
								$('#noBlendTable').DataTable().destroy();
								showSampleTable(
										$("#noBlendTable"),
										);
							} else {
								showSampleTable(
										$("#noBlendTable"),
										$(
												".wizard_steps .selected .step_no")
												.text());
							}
							// 已提交质检明细
							if ($("#zhijianDetails_wrapper").length) {
								$('#zhijianDetails')
										.DataTable().destroy();
								showZhijianDetails(
										$("#zhijianDetails"),
										$(
												".wizard_steps .selected .step_no")
												.text());
							} else {
								showZhijianDetails(
										$("#zhijianDetails"),
										$(
												".wizard_steps .selected .step_no")
												.text());
							}
							// 质检
							var zhiJian = data.temItemList;
							$("#zhijianBody").html("");
							if(zhiJian.length) {
								zhiJian.forEach(function(vvv, i) {
									$("#zjType").text(vvv.typeName);
									$("#zjType").attr("zjId", vvv.typeId);
									renderZhijian(vvv);
								});
							} else {
								$("#zjType").text("");
								$("#zjType").attr("");
								$("#zhijianBody").html('<li class="zhijianli zhijianOld" style="text-align:center">没有数据</li>');
							}
						} else if(stepId == '3') {
							renderCaozuo(data.reagenOrCos,data.jsonRes,data.customTest,data.cellResult,data.temProd);
							console.log(data.reagenOrCos);
//							console.log(data.jsonRes)
							if (data.cellResult != null) {
								$("#productStartTime").text(data.cellResult.productStartTime);
								$("#productEndTime").text(	data.cellResult.productEndTime);
								$("#notes").val(data.cellResult.notes);
							}
						} else if(stepId == '4') {
							// 渲染操作指令
							renderOperationalOrder(
									data.cellPass, stepId);
							// 渲染生产记录
							// renderProductionRecord(data.cellReco.templeItem,
							// data.cellReco, stepId);
							renderProductionRecord(
									data.cellReco.templeItem,
									data.cellReco, stepId);

							var stepNum = $(".wizard_steps .selected .step_no").text(); // 当前得步骤数
							// 判断按钮显示隐藏 ajax判断 当前步骤是否是该生产的最后一步
							// 并且继续生产按钮名称改为完成生产操作
							$.ajax({
								type : 'post',
								async : false,
								url : ctx
										+ '/experiment/reagent/reagentTask/changeButtonBlock.action',
								data : {
									mainId : cellPassage_id,
									stepNum : stepNum,
								},
								success : function(data) {
									var data1 = JSON
											.parse(data);
									if (data1.success) {
										if (data1.zhyb) {
											$("#jxsc").val("完成生产操作");
											$("#next").show();
										}else{
											$("#next").hide();
										}
										// 加判断，如果当前步骤完成其他按钮都不显示
										if (data1.dqbz) {
											if (baocun) {
												if (caozuo) {
													$("#jxsc").show();
												} else{
													$("#jxsc").hide();
												}
//												$("#next").hide();
											} else{
												$("#jxsc").hide();
											}
										} else {
											$("#jxsc").hide();
										}
									} else {

									}
								}
							});
						}
					}
				});
			});
}
// 渲染操作指令
function renderOperationalOrder(cellPass, stepId) {
	var trs = '';
	if(stepId == "1") {
		if(cellPass.length) {
			cellPass.forEach(function(v, i) {
				var zhiling=v.templeInsId;
				var checked = v.productionInspection == "0" ? '' : 'checked';
				var note = v.operationNotes?v.operationNotes:"";
				var wbtn=note?"btn-warning":"";
				trs += '<tr class="pageOneOperationalOrder" id="' +v.id +'" note="'+note+'" state="'+v.productionInspection+'"><td><span class="badge bg-green">' +zhiling.sort +'</span></td><td style="white-space: inherit;word-wrap: break-word;word-break: break-all;">' +zhiling.name +'</td><td><input type="checkbox" class="mySwitch" '+checked+'/></td><td><button class="btn btn-xs '+wbtn+'" onclick="layerNote(this)"> <i class="fa fa-pencil-square-o"></i>备注</button></td></tr>';
				})
		} else {
			trs = '<tr><td colspan="4" style="text-align: center;">没有数据</td></tr>';
		}
		$("#pageOneOperationalOrder").html(trs);
	} else if(stepId === "4") {
		if(cellPass.length) {
			//console.log(cellPass)
			cellPass.forEach(function(v, i) {
				var zhiling=v.templeInsId;
				var checked = v.productionInspection == "0" ? '' : 'checked';
				trs += '<tr class="pageFourOperationalOrder" id="' +v.id +'" state="'+v.productionInspection+'"><td><span class="badge bg-green">' +zhiling.sort +'</span></td><td>' +zhiling.name +'</td><td><input type="checkbox" class="mySwitch" '+checked+' /></td></tr>';
			})
		} else {
			trs = '<tr><td colspan="3" style="text-align: center;">没有数据</td></tr>';
		}
		$("#pageFourOperationalOrder").html(trs);
	}

	setTimeout(function(){
		$(".table .mySwitch").each(function() {
			var state = $(this).is(':checked');
			$(this).bootstrapSwitch({
				onText: "是",
				offText: "否",
				onSwitchChange: function(event, state) {
					if(state == true) {
						$(this).parents("tr").attr("state", "1");
					} else {
						$(this).parents("tr").attr("state", '0');
					}
				}
			}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
		})
	},0.1)
}
// 备注信息
function layerNote(that) {
	if($(that).hasClass("btn-warning")) {
		var txt = $(that).parents("tr").attr("note");
	} else {
		var txt = '';
	}
	top.layer
		.open({
			title: "备注",
			type: 1,
			area: ["30%", "60%"],
			btn: biolims.common.selected,
			btnAlign: 'c',
			content: '<textarea id="checkCounts" class="form-control" style="width: 99%;height: 99%;">' +
				txt + '</textarea>',
			yes: function(index, layero) {
				var content = $("#checkCounts", parent.document).val();
				if(content) {
					$(that).addClass("btn-warning").parents("tr").attr(
						"note", content);
				} else {
					$(that).removeClass("btn-warning").parents("tr").attr(
						"note", "");
				}
				top.layer.close(index);
			},
		});
}
// 渲染生产记录（自定义字段）
function renderProductionRecord(jilu, cellReco, stepId) {
	var ipts = '';
	var content = JSON.parse(jilu.content);
	content.forEach(function(v, i) {
			var txt="";
			if (v.fieldName == "operatingRoomName") {
				var faIcon = 'fa fa-home';
				ipts += '<div class="form-group"><label><i class="'
						+ faIcon
						+ '"></i> '
						+ v.label
						+ '<button class="btn btn-warning btn-xs" onclick="selectRoom();">+选择</button></label>'
						+ '<input type="hidden" id="operatingRoomId" class="form-control" changelog="" value=""/> <input type="'
						+ v.type + '" id="' + v.fieldName
						+ '" class="form-control" changelog="" value="' + txt
						+ '" readonly="readyOnly" /><span class="input-group-btn"></div>';
				return true;
			} else if(v.fieldName == "ambientTemperature") {
				var faIcon = 'fa fa-black-tie';
				var defaultValue = v.defaultValue ? v.defaultValue : '27';
				ipts += '<div class="form-group"><label><i class="' +faIcon +'"></i> ' +v.label +'</label> <input type="text" id="' +v.fieldName +'" class="slider form-control" data-slider-min="-20" data-slider-max="80" data-slider-step="1" data-slider-value="' +defaultValue +'" data-slider-tooltip="always" data-slider-id="green"></div>';
				return true;
			} else if(v.fieldName == "ambientHumidity") {
				var faIcon = 'fa fa-flask';
				var defaultValue = v.defaultValue ? v.defaultValue : '37';
				ipts += '<div class="form-group"><label><i class="' +faIcon +'"></i> ' +v.label +'</label> <input type="text" id="' +v.fieldName +'" class="slider form-control" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="' +defaultValue +'" data-slider-tooltip="always" data-slider-id="purple"></div>';
				return true;
			} 
			else if(v.fieldName == "templeOperator") {
				var faIcon = 'fa fa-male';
		        ipts += '<div class="form-group"><label><i class="' + faIcon + '"></i> ' + v.label + '<button class="btn btn-warning btn-xs" onclick="selectUserTo();">+选择</button></label> <input type="hidden" id="' + v.fieldName + '" class="form-control" changelog="" value="' + txt + '"/> <input type="' + v.type + '" id="' + v.fieldName + '-name" class="form-control" changelog="" value="' + txt + '" readOnly="readyOnly" /></div>';
                return true;
			} 
			else if(v.fieldName == "templeReviewer") {
				var faIcon = 'fa fa-user';
				ipts += '<div class="form-group"><label><i class="' + faIcon + '"></i> ' + v.label + '<button class="btn btn-warning btn-xs" onclick="selectUserTr();">+选择</button></label> <input type="hidden" id="' + v.fieldName + '" class="form-control" changelog="" value="' + txt + '"/> <input type="' + v.type + '" id="' + v.fieldName + '-name" class="form-control" changelog="" value="' + txt + '" readOnly="readyOnly" /></div>';
				return true;
				//消毒剂
			}else if (v.fieldName == "disinfectant") {
				var faIcon = 'fa fa-home';
				ipts += '<div class="form-group"><label><i class="'
					+ faIcon
					+ '"></i> '
					+ v.label
					+ '<button class="btn btn-warning btn-xs" onclick="selectDisinFectant();">+选择</button></label> <input type="'
					+ v.type + '" id="' + v.fieldName
					+ '" class="form-control" changelog="" value="' + txt
					+ '"/><span class="input-group-btn"></div>';
				return true;
				//紫外线
			} else if (v.fieldName == "ultravioletlamptime") {
				var faIcon = 'fa fa-home';
				ipts += '<div class="form-group"><label><i class="'
						+ faIcon
						+ '"></i> '
						+ v.label
						+ '</label> <input type="'
						+ v.type + '" id="' + v.fieldName
						+ '" class="form-control" changelog="" value="' + txt
						+ '"/><span class="input-group-btn"></div>';
				return true;
			}
			ipts += '<div class="form-group"><label><i class="' + faIcon +'"></i> ' + v.label + '</label> <input type="' +v.type + '" id="' + v.fieldName +'" class="form-control" changelog="" value="' + txt +'"/></div>';
		});
	if(stepId == "1") {
		$("#pageOneProductionRecord").html(ipts).attr("pid", cellReco.id);
	} else if(stepId == "4") {
		$("#pageFourProductionRecord").html(ipts).attr("pid", cellReco.id);
	}
		var target = $("div[pid=" + cellReco.id + "]");
		var content =JSON.parse(cellReco.content);
		for(var k in content) {
			if(k == "ambientHumidity" || k == "ambientTemperature") {
				target.find("#" + k).attr("data-slider-value", content[k]);
			}
			else if(k=="templeOperator"){
				txt=content[k]?content[k]:window.userName;
				target.find("#templeOperator").val(txt);
			}else if (k == "disinfectant") {
				txt = content[k] ? content[k] : "";
				$("#disinfectant").val(txt);
			} else if (k == "ultravioletlamptime") {
				txt = content[k] ? content[k] : "";
				$("#ultravioletlamptime").val(txt);
			}else{
				target.find("#" + k).val(content[k]);
			}
		}
	$('.slider').slider();
	// 日期格式化
	$("#contentData").find('input[type=data]').datepicker({
		language: "zh-TW",
		autoclose: true, // 选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" // 日期格式，详见
	});

}

// 渲染质检
function renderZhijian(zhijian) {
	var zhijianLis = "";
	var id = zhijian.id ? zhijian.id : "";
	var code = zhijian.code ? zhijian.code : "";
	var name = zhijian.name ? zhijian.name : "";
	var nextId = zhijian.nextId ? zhijian.nextId : "";
	var itemId = zhijian.itemId ? zhijian.itemId : "";
	zhijianLis += '<li class="zhijianli zhijianOld" id=' +
		id +
		'><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span nextid=\'' +
		nextId +
		'\' zhijianid=\'' +
		code +
		'\' class="zhijianName">' +
		name +
		'</span></small><small><i testId=\'' +
		code +
		'\' stepNum=\'' +
		itemId +
		'\' onclick="showResult(this)" class="fa fa-search-plus pull-right"></i></small></li>';
	$("#zhijianBody").append(zhijianLis);
}

// 展示质检提交到哪个质检的结果内容
function showResult(self) {
	var stepNum = $(self).attr("stepNum");
	var testId = $(self).attr("testId");
	var mark = "ReagentTask";
	top.layer.open({
			title: "质检结果",
			type: 2,
			area: [document.body.clientWidth - 300,
				document.body.clientHeight - 100
			],
			btn: biolims.common.selected,
			content: [
				window.ctx +
				"/experiment/quality/qualityTest/showQualityTestResultListForzj.action?" +
				"stepNum=" + stepNum + "&testId=" + testId +
				"&mark=" + mark, ''
			],
			yes: function(index, layer) {
				top.layer.closeAll();
			},
		})
}

//渲染BL操作
function renderCaozuo(caozuo,caozuoResult,customTest,
		customTestresult,temProduc) {
	var caozuo = JSON.parse(caozuo);
	var max = caozuo.length;
	if(max) {
		//渲染模板
		$(".timeline").html('<li class="time-label" id="startCaozuo"><span class="bg-red">严格执行有关文件的操作规范 </span><span class="time"><button id="btn_startTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 开始时间: <i id="productStartTime" class="stepsTime"></i></button></span></li>');
		caozuo.forEach(function(v, i) {
			var endTime=temProduc.length?temProduc[i].endTime:'';
			var startTime=temProduc.length?temProduc[i].startTime:'';
			
			if (startTime == undefined) {
				startTime = "";
			}
			if (endTime == undefined) {
				endTime = "";
			}
			
			var li = $('<li id="'
					+ v.id
					+ '" class="caozuoItem"><i class="fa fa-balance-scale bg-aqua"></i><div class="timeline-item"><span class="time"><input type="checkbox" class="mySwitch"><button id="btn_startTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 开始时间: <i id="startTime" class="stepsTime">'
					+ startTime
					+ '</i></button><button id="btn_endTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 结束时间:   <i id="endTime" class="stepsTime">'
					+ endTime
					+ '</i></button></span><h3 class="timeline-header"><a href="####">'
					+ v.producingTitle
					+ ' </a></h3><div class="timeline-body">'
					+ v.producingName
					+ '<button class="btn btn-success btn-xs pull-right" onclick="showLineFoot (this)">收起</button></div><div class="timeline-footer box-body"><div class="col-xs-6"><ul class="todo-list" id="reagentBody"></ul></div><div class="col-xs-6"><ul class="todo-list" id="cosBody"></ul></div><div class="col-xs-12" id="pageThree'
					+temProduc[i].id+'"></div></div></div></li>');
			var reagent = v.reagentList;
			var cos = v.cosList;
			var cpo = v.cpoList;
			reagent.forEach(function(vv, ii) {
				renderReagent(li, vv)
			})
			cos.forEach(function(vvv, iii) {
				renderCos(li, vvv)
			})
			$(".timeline").append(li);
			
			var result = temProduc.length ? temProduc[i].customTest
					: '';
			if (result == "") {

			} else {
				var resultz = JSON.parse(caozuo[i].customTest);

				var result1 = JSON.parse(temProduc[i].customTest);
				var ipts = '';

				resultz.forEach(function(z, x) {
							ipts += '<div class="col-xs-6"><div class="input-group"><span class="input-group-addon">'
									+ z.label
									+ '</span> <input id="'
									+ z.fieldName
									+ '" type="'
									+ z.type
									+ '" class="form-control"></div></div>';
						});
				$("#pageThree" + temProduc[i].id).html(ipts);

				for ( var k in result1) {
					$("#pageThree" + temProduc[i].id).find("#" + k)
							.val(result1[k]);
				}

			}

		});
		$(".timeline").append('<li><i class="fa fa-balance-scale bg-yellow"></i><div class="timeline-item">备注：<input type="text" id="notes" /><span class="time"><button id="btn_endTime" class="btn btn-sm btn-box-tool" onclick="watchTime(this)"><i class="fa fa-clock-o"></i> 结束时间: <i id="productEndTime" class="stepsTime"></i></button></span></div></li><li><i class="fa fa-clock-o bg-gray"></i></li>');
		//赋值
		var caozuoResult=JSON.parse(caozuoResult);
		console.log(caozuoResult)
		caozuoResult.forEach(function (v,i) {
			var targetli=$("#"+v.templeId);
			if(v.state&&v.state == "1"){
				var checked=true;
			}else{
				var checked=false;
			}
			targetli.attr({
				"czid":v.id,
				"state":v.state
			}).find(".mySwitch").prop("checked",checked);
			v.reagent.forEach(function (vv,ii) {
				var thisreagent=targetli.find("#"+vv.templateReagentId);
				thisreagent.attr("reagentid",vv.id).find(".label-primary span").text(vv.batch);
//				thisreagent.find(".label-info span").text(vv.cellPassSn);
				thisreagent.find("input").val(vv.reagentDosage);
				targetli.find("#"+vv.templateReagentId).find(".input-sm").val(vv.reagentDosage);
			});
			v.cosList.forEach(function(vv, ii) {
				targetli.find("#" + vv.templateCosId).attr({
					"cosid" : vv.id,
					"code" : vv.instrumentCode
				}).find(".label-warning").text(vv.instrumentName);
			});
		});
		
		
		setTimeout(function(){
			$(".timeline .mySwitch").each(function() {
				var state = $(this).is(':checked');
				$(this).bootstrapSwitch({
					onText: "已完成",
					offText: "未完成",
					onSwitchChange: function(event, state) {
						
						var num = $(this).parents('li').index();
						if(isIpad){
							// $('.ipddd').animate({
								
							// 	scrollTop: num*60
							// });
							 $('.ipddd').scrollTop(num*160)
						}else{
							$('html,body').animate({
								scrollTop: $(this).parents('.timeline-item').find('.timeline-body').offset().top+20
							});
						}
						if(state == true) {
							$(this).parents(".caozuoItem").attr("state", "1");
						} else {
							$(this).parents(".caozuoItem").attr("state", '0');
						}
					}
			}).bootstrapSwitch('size', "mini").bootstrapSwitch('state', state);
		})
		},0.1)
		
		// 选择原辅料和设备
		reagentAndCosChose();		
	} else {
		$(".timeline").html("没有数据");
	}
}
//渲染设备
function renderReagent(li, reagent) {
	var reagentLis = "";
	var code = reagent.code ? reagent.code : "";
	reagentLis += '<li class="reagli" id=' + reagent.id + ' code=' + code + ' ><span><i class="fa fa-search-plus"></i></span><p class="text">' + reagent.name + '</p><small class="label label-primary">批次:<span></span></small><small class="label label-info">sn:<span></span></small></br><div class="input-group"><span class="input-group-addon">原辅料用量</span> <input type="text" class="form-control input-sm"></div></li>';
	li.find("#reagentBody").append(reagentLis);
}
//渲染原辅料
function renderCos(li, cos) {
	var cosLis = "";
	var name = cos.name ? cos.name : "";
	var code = cos.code ? cos.code : "";
	cosLis += '<li class="cosli" id=' + cos.id + '  typeid=' + cos.typeId + '><span><i class="fa fa-search-plus"></i></span><p class="text">' + cos.typeName + '</p><small class="label label-warning"></small></li>';
	li.find("#cosBody").append(cosLis);
}
function renderStartAndEndTime(li, cpo) {
	var startTime = cpo.startTime ? cpo.startTime : "";
	var endTime = cpo.endTime ? cpo.endTime : "";
}
//获取当前时间
function watchTime(that) {
	$(that).children(".stepsTime").text(parent.moment(new Date()).format("YYYY-MM-DD  HH:mm"));
}
//设备原辅料展开关闭
function showLineFoot(that) {
	var target = $(that).parent(".timeline-body").next(".timeline-footer");
	if(target.hasClass("xxxxx")) {
		$(this).text("收起");
		target.slideDown().removeClass("xxxxx");
	} else {
		$(this).text("展开");
		target.slideUp().addClass("xxxxx");
	}
}
// 原辅料和设备选择操作
function reagentAndCosChose() {
	$(".fa-search-plus").unbind("click").click(function() {
		var item = $(this).parents("li");
		if(item.hasClass("reagli")) {
			addReagent(item.attr("code"), item);
		}
		if(item.hasClass("cosli")) {
			addCos(item.attr("typeid"), item);
		}
	});
}
// 选择原辅料批次
function addReagent(id, item) {
	top.layer.open({
		title: biolims.common.selReagent,
		type: 2,
		area: top.screeProportion,
		btn: biolims.common.selected,
		content: [
			window.ctx + "/storage/getStrogeReagent.action?id=" + id + "",
			""
		],
		yes: function(index, layero) {
			var batch = $(".layui-layer-iframe", parent.document)
				.find("iframe").contents().find("#addReagent .chosed")
				.children("td").eq(2).text();
			var sn = $(".layui-layer-iframe", parent.document).find("iframe")
				.contents().find("#addReagent .chosed").children("td")
				.eq(4).text();
			item.find(".label-primary span").text(batch);
			item.find(".label-info span").text(sn);
			top.layer.close(index)
		},
	});
}
// 第三页选择设备
function addCos(id, item) {
	// console.log()
	top.layer.open({
		title: biolims.common.selectInstrument,
		type: 2,
		area: top.screeProportion,
		btn: biolims.common.selected,
		content: [
			window.ctx + "/equipment/main/selectCos.action?typeId=" + id +
			"", ""
		],
		yes: function(index, layero) {
			var name = $(".layui-layer-iframe", parent.document).find("iframe")
				.contents().find("#addCos .chosed").children("td").eq(1)
				.text();
			var code = $(".layui-layer-iframe", parent.document).find("iframe")
				.contents().find("#addCos .chosed").children("td").eq(0)
				.text();
			item.find(".label-warning").text(name);
			item.attr("code", code);
			top.layer.close(index)
		},
	})
}

// 原辅料和设备复制操作
//function reagentAndCosCopy() {
//	$(".fa-copy").unbind("click").click(function() {
//		var li = $(this).parent("li").clone();
//		// 复制的是原辅料
//		if(li.hasClass("reagli")) {
//			li.attr("reagentid", ""); // id为空
//			li.find(".label-primary span").text(""); // 批次为空
//			li.find(".label-info span").text(""); // sn为空
//		}
//		// 复制的是设备
//		if(li.hasClass("cosli")) {
//			li.attr({
//				"cosid": "",
//				"code": ""
//			}); // id为空
//			li.children(".label-default").text(""); // 设备名称为空
//		}
//		$(this).parent("li").after(li);
//		reagentAndCosCopy();
//		reagentAndCosRemove();
//		reagentAndCosChose();
//	});
//}

// 原辅料和设备删除操作
//function reagentAndCosRemove() {
//	$(".fa-trash").unbind("click").click(function() {
//				var li = $(this).parent("li");
//				if(li.hasClass("reagli")) {
//					var id = li.attr("reagentid");
//					if(id) {
//						$.ajax({
//								type: "post",
//								url: ctx +"/experiment/cell/passage/cellPassage/delCellPassageReagent.action",
//								data: {
//									id: id,
//									mainId: $("#cellPassage_id").text(),
//									del: "细胞传代实验实验步骤：" +$(".wizard_steps .selected").find(".step_descr").text() +"删除原辅料："
//								},
//								success: function(data) {
//									var data = JSON.parse(data);
//									if(data.success) {
//										top.layer.msg(biolims.common.deleteSuccess);
//									}
//								}
//							});
//					} else {
//						li.remove();
//					}
//				}
//				if(li.hasClass("cosli")) {
//					var id = li.attr("cosid");
//					if(id) {
//						$.ajax({
//								type: "post",
//								url: ctx +"/experiment/cell/passage/cellPassage/delCellPassageCos.action",
//								data: {
//									id: id,
//									mainId: $("#cellPassage_id").text(),
//									del: "细胞传代实验实验步骤：" +
//										$(".wizard_steps .selected").find(".step_descr").text() +"删除设备："
//								},
//								success: function(data) {
//									var data = JSON.parse(data);
//									if(data.success) {
//										top.layer.msg(biolims.common.deleteSuccess);
//									}
//								}
//							});
//					} else {
//						li.remove();
//					}
//				}
//			});
//}


// 保存数据
var serial="";
function saveStepItem() {
	var stepId = $(".bwizard-steps>.active").attr("note");
	var action;
	var saveData;
	

	// 判断是否选择设备
	var arr = [];
	var fuyong = true;

	$("#pageOneChoseYQ .btn-info").each(function(i, v) {
		var jsonn = {};
		jsonn.id = v.getAttribute("cosid");
		jsonn.code = v.getAttribute("code");
		jsonn.name = v.getAttribute("name");

		if (jsonn.name != null && jsonn.name != "null" && jsonn.name != "") {
		} else {
			fuyong = false;
			return false;
		}
		arr.push(jsonn);
	});
	var changeLog = "试剂制备";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		console.log(val);
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	if (fuyong == false) {
		top.layer.msg("请选择设备");
		return false;
	}

	// 判断是否选择
	if ($("#operatingRoomName").val() != null
			&& $("#operatingRoomName").val() != "") {
	} else {
		top.layer.msg("请选择房间");
		return false;
	}
	
	if(stepId == "1") { // 工前准备
		// 操作指令
		var operationalOrder = [];
		$("#pageOneOperationalOrder .pageOneOperationalOrder").each(
			function(i, v) {
				var obj = {};
				obj.id = v.id;
				obj.productionInspection = v.getAttribute("state") ? v
					.getAttribute("state") : "0";
				obj.operationNotes = v.getAttribute("note") ? v
					.getAttribute("note") : "";
				operationalOrder.push(obj);
			})
		// 生产记录
		var productionRecord = {
			guohkId: $("#pageOneProductionRecord").attr("pid")
		};
		$("#pageOneProductionRecord input").each(function(i, v) {
			productionRecord[v.id] = v.value;
		});
		var template = {
			id: $("#cellPassage_id").text(),
			orderId: $(".wizard_steps .selected").attr("stepid"),
			orderNum: $(".wizard_steps .selected .step_no").text(),
			stepId: stepId,
			temNstr: operationalOrder,
			content: productionRecord
		};
		 saveData = JSON.stringify(template);
		// console.log(saveData);
		 action = "/experiment/reagent/reagentNow/savepreparationProductionRecord.action?changeLog="+changeLog;
	} else if(stepId == "2") {
		$("#noBlendTable tbody tr").each(function(i,j,k){
			serial=$(this).find("td[savename='serial']").text();
			console.log(serial)
		})
		$.ajax({
			type: "post",
			url: "/experiment/reagent/reagentNow/findC.action",
			data: {
				"serial" : serial,
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					// 拼质检的数据
					var noBlendTabledata = savenoBlendTablejson($("#noBlendTable"));
					var zhijian = {
						noBlendTabledata : noBlendTabledata,
						orderNum : $(".wizard_steps .selected .step_no").text(),
					}
					//质检日志
					var datas = savenoBlendTablejson($("#noBlendTable"));
					var changeLogItem = "质检：";
					changeLogItem = getChangeLog(datas, $("#noBlendTable"), changeLogItem);
					var changeLogItemLast = "";
					if (changeLogItem != "质检：") {
						changeLogItemLast = changeLogItem
					}
					saveData = JSON.stringify(zhijian);
					$.ajax({
						type: "post",
						url: ctx +  "/experiment/reagent/reagentNow/saveQualityInspectionResults.action",
						data: {
							"saveData": saveData,
							changeLogItem : changeLogItemLast
						},
						success: function(data) {
							var data = JSON.parse(data);
							if(data.success) {
								top.layer.msg(biolims.common.saveSuccess);
								$(".bwizard-steps .active>a").click();
							}
							top.layer.closeAll();
						}
					});	
				}else{
					top.layer.msg("批号有重复，请重新录入");	
				}
			
			}
		});
		
	} else if(stepId == "3") { // 生产操作
		var arr = [];
		$(".caozuoItem").each(
				function(i, v) {
					var obj = {};
					obj.id = v.getAttribute("czid");
					obj.state = v.getAttribute("state") ? v
							.getAttribute("state") : "0";
					obj.startTime = $(v).find("#btn_startTime .stepsTime")
							.text(); // $("#btn_startTime
					// .stepsTime").text();
					obj.endTime = $(v).find("#btn_endTime .stepsTime").text(); // $("#btn_endTime
					// .stepsTime").text();
					// 拼原辅料的数据
					var reagentli = $(v).find(".reagli");
					var reagent = [];
					reagentli.each(function(i, val) {
						var reagentItem = {};
						reagentItem.id = val.getAttribute("reagentid");
						reagentItem.name = $(val).children(".text").text();
						reagentItem.code = val.getAttribute("code");
						reagentItem.batch = $(val).children(".label-primary")
								.children("span").text();
						reagentItem.sn = "";
						// $(val).children(".label-info")
						// .children("span").text();
						reagentItem.inputVal = $(val).find(".input-sm").val();
						reagent.push(reagentItem);
					});
					obj.reagent = reagent;
					// 拼设备的数据
					var cosli = $(v).find(".cosli");
					var cos = [];
					cosli
							.each(function(i, val) {
								var cosItem = {};
								cosItem.id = val.getAttribute("cosid");
								cosItem.name = $(val)
										.children(".label-warning").text();
								cosItem.code = val.getAttribute("code");
								cosItem.typeId = val.getAttribute("typeid");
								cos.push(cosItem);
							});
					obj.cos = cos;

					var result1 = {}; // 自定义字段
					var resultValue = ""; // 自定义字段值
					$("#pageThree" + obj.id + " input").each(
							function(z, x) {
								result1[x.id] = x.value;
								resultValue = resultValue + $(x).prev().text()
										+ ":" + x.value + "\n";
							});
					obj.customTest = result1;
					obj.customTestValue = resultValue;

					arr.push(obj)
				});
		var result = {}; // 自定义字段
		$("#pageThree input").each(function(i, v) {
			result[v.id] = v.value;
		})
		var template = {
			id : $("#cellPassage_id").text(),
			orderId : $(".wizard_steps .selected").attr("stepid"),
			orderNum : $(".wizard_steps .selected .step_no").text(),
			stepId : stepId,
			result : result,
			content : arr,
			productStartTime : $("#productStartTime").text(),
			productEndTime : $("#productEndTime").text(),
			notes : $("#notes").val()
		// startime:$("#btn_startTime .stepsTime").text(),
		// endtime:$("#btn_endTime .stepsTime").text()
		};
		saveData = JSON.stringify(template);
		// console.log(saveData);
		action = "/experiment/reagent/reagentNow/saveCosandReagent.action";
	} else if(stepId == "4") {
		// 操作指令
		var operationalOrder = [];
		$("#pageFourOperationalOrder .pageFourOperationalOrder").each(
			function(i, v) {
				var obj = {};
				obj.id = v.id;
				obj.productionInspection = v.getAttribute("state") ? v
					.getAttribute("state") : "0";
				operationalOrder.push(obj);
			})
		// 生产记录
		var productionRecord = {
			guohkId: $("#pageFourProductionRecord").attr("pid")
		};
		$("#pageFourProductionRecord input").each(function(i, v) {
			productionRecord[v.id] = v.value;
		});
		var template = {
			id: $("#cellPassage_id").text(),
			orderId: $(".wizard_steps .selected").attr("stepid"),
			orderNum: $(".wizard_steps .selected .step_no").text(),
			stepId: stepId,
			temNstr: operationalOrder,
			content: productionRecord
		};
		 saveData = JSON.stringify(template);
		// console.log(saveData);
		var action = "/experiment/reagent/reagentNow/savefourthStepCellproductResult.action";
	}
	$.ajax({
		type: "post",
		url: ctx + action,
		async: false,
		data: {
			"saveData": saveData,
			changeLog:changeLog,
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				$(".bwizard-steps .active>a").click();
			}
			top.layer.closeAll();
		}
	});
//	//更改房间状态
//	var arr=[];
//	$("#pageOneChoseYQ .btn-info").each(function (i,v) {
//		var jsonn={};
//		jsonn.id=v.getAttribute("cosid");
//		jsonn.code=v.getAttribute("code");
//		jsonn.name=v.getAttribute("name");
//		
//		jsonn.rwdid=$("#cellPassage_id").text();
//		jsonn.bzid = $(".wizard_steps .selected").attr("stepid");
//		jsonn.bzname=$("#steptiele").text();
//		jsonn.bzorderNum = $(".wizard_steps .selected .step_no")
//			.text();
//		arr.push(jsonn);
//	});
//	var room=$("#operatingRoomName").val();
//	$.ajax({
//		type:"post",
//		url:"/experiment/cell/passage/cellPassage/startPrepare.action",
//		data:{
//			data:JSON.stringify(arr),
//			roomName:room,
//		},
//	});
	
	
}
// 上一步下一步操作
function preAndNext() {
	// 上一步操作
	$("#prev").click(function() {
				//$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/reagent/reagentNow/editCellPassage.action?id=" + cellPassage_id+"&bpmTaskId="+$("#bpmTaskId").val();
				$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/reagent/reagentTask/editReagentTask.action?id=" + cellPassage_id+"&bpmTaskId="+$("#bpmTaskId").val();
			});
	// 下一步操作
	$("#next").click(function() {
				$("#maincontentframe", window.parent.document)[0].src = window.ctx +
					"/experiment/reagent/reagentNow/showReagentTaskResultTable.action?id=" +
					cellPassage_id +
					"&bpmTaskId=" +
					$("#bpmTaskId").val();
			});
}

function sp() {

	var taskId = $("#bpmTaskId").val();
	var formId = $("#cellPassage_id").text();

	top.layer.open({
		title: biolims.common.detailedExaminationAndApproval,
		type: 2,
		anim: 2,
		area: ['800px', '500px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/workflow/processinstance/toCompleteTaskView.action?taskId=" +
			taskId + "&formId=" + formId,
		yes: function(index, layer) {
			var operVal = $('.layui-layer-iframe', parent.document).find(
				"iframe").contents().find("#oper").val();
			var opinionVal = $('.layui-layer-iframe', parent.document).find(
				"iframe").contents().find("#opinionVal").val();

			if(!operVal) {
				top.layer.msg(biolims.common.pleaseSelectOper);
				return false;
			}
			if(operVal == "1") {
				top.layer.msg(biolims.dna.pleaseFinish);
				return false;
			}
			if(operVal == "2") {
				_trunTodoTask(taskId, callback, dialogWin);
			} else {
				var paramData = {};
				paramData.oper = operVal;
				paramData.info = opinionVal;

				var reqData = {
					data: JSON.stringify(paramData),
					formId: formId,
					taskId: taskId,
					userId: window.userId
				}
				ajax("post", "/workflow/processinstance/completeTask.action",
					reqData,
					function(data) {
						if(data.success) {
							top.layer.msg(biolims.common.submitSuccess);
							if(typeof callback == 'function') {}
						} else {
							top.layer.msg(biolims.common.submitFail);
						}
					}, null);
			}
			top.layer.closeAll();
			location.href = window.ctx + "/lims/pages/dashboard/dashboard.jsp";
		}

	});
}

//判断如果是列表页进来的，直接跳到当前步骤
function renderOrderNums() {
	var dijibu = sessionStorage.getItem("dijibu");
	if(dijibu) {
		var orderNums = dijibu;
	} else {
		var orderNums = $("#orderNumBy").val();
	}
	if(orderNums && orderNums != 1) {
		$(".wizard_steps .step").each(function(i, v) {
			if($(v).children(".step_no").text() == orderNums) {
				$(v).click();
			}
		});
	}
}

//选择实验员
function showdutyManId() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: [document.body.clientWidth - 300,
			document.body.clientHeight - 100
		],
		btn: biolims.common.selected,
		content: [
			window.ctx + "/core/user/selectUserTable.action?groupId=admin",
			''
		],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
				.contents().find("#addUserTable .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
				.contents().find("#addUserTable .chosed").children("td")
				.eq(0).text();
			top.layer.close(index);
			$("#testUserList").val(name);
		},
	})
}
//选择检测项
function showSampledetecyion() {
	top.layer
		.open({
			title: biolims.common.pleaseChoose,
			type: 2,
			area: [document.body.clientWidth - 300,
				document.body.clientHeight - 100
			],
			btn: biolims.common.selected,
			content: [
				window.ctx +
				"/system/detecyion/sampleDeteyion/showSampledetecyion.action?",
				''
			],
			yes: function(index, layer) {
				var name = $('.layui-layer-iframe', parent.document).find(
						"iframe").contents().find(
						"#addSampleDeteyionTable .chosed").children("td")
					.eq(1).text();
				var id = $('.layui-layer-iframe', parent.document).find(
						"iframe").contents().find(
						"#addSampleDeteyionTable .chosed").children("td")
					.eq(0).text();
				top.layer.close(index);
				$("#sample_detecyion_id").val(id);
				$("#sample_detecyion_name").val(name);
			},
		})
}
//选择房间
function selectRoom() {
	top.layer
			.open({
				title : "选择房间",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/experiment/cell/passage/cellPassage/selectRoomTable.action",
						"" ],
				yes : function(index, layero) {
					// var name = $(".layui-layer-iframe", parent.document)
					// .find("iframe").contents().find("#addRoomTable
					// .chosed").children("td").eq(1).text();
					var name = [];
					var id=[];
					$('.layui-layer-iframe', parent.document).find("iframe")
							.contents().find("#addRoomTable .selected").each(
									function(i, v) {
										// id.push($(v).children("td").eq(2).text());
										name.push($(v).children("td").eq(2)
												.text());
										id.push($(v).children("td").eq(1)
												.text());
									})
					if (name.length > 1) {
						top.layer.msg("请选择一条数据！");
						return false;
					} else {
						top.layer.close(index);
						$("#operatingRoomName").val(name);
						$("#operatingRoomId").val(id);
					}
				},
			});
}

//打印
function printTempalte() {
	var id=$("#cellPassage_id").text();
	var orderNum=$(".wizard_steps .selected .step_no").text();
	$.ajax({
		type : "post",
		data : {
			id : $("#sampleReveice_id").text(),
			confirmDate : $("#cellPassage_createDate").text(),
			modelId:"scdlb"
		},
		url : ctx
				+ "/stamp/birtVersion/selectBirt.action",
		success : function(data) {
			var data = JSON.parse(data)
			if (data.reportN) {
				
			
				var url = '__report='+data.reportN+'&id=' + id+'&ordernum='+orderNum;
				commonPrint(url);
				
			} else {
				top.layer.msg("没有报表信息！");
			}
		}
	});
//	var url = '__report=templateitemSJ.rptdesign&id=' + id+'&ordernum='+orderNum;
//	commonPrint(url);
}
function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '', '');
}
//选择操作人
function selectUserTo() {
	top.layer.open({
		title: "选择操作人",
		type: 2,
		area: top.screeProportion,
		btn: biolims.common.selected,
		content: [ window.ctx + "/core/user/selectUserTable.action?groupId=SC002", ""],
		yes: function(index, layero) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index);
			$("#templeOperator").val(id);
			$("#templeOperator-name").val(name);
		},
	});
}
// 选择审核人
function selectUserTr() {
	top.layer.open({
		title: "选择审核人",
		type: 2,
		area: top.screeProportion,	
		btn: biolims.common.selected,
		content: [ window.ctx + "/core/user/selectUserTable.action?groupId=SC002", ""],
		yes: function(index, layero) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index);
			$("#templeReviewer").val(id);
			$("#templeReviewer-name").val(name);
		},
	});
}
//第一页选择设备
function addCos1(that) {
	// console.log()
	var roomName = $("#operatingRoomName").val();
	top.layer
			.open({
				title : biolims.common.selectInstrument,
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/experiment/reagent/reagentTask/selectCos.action?typeId="
								+ that.getAttribute("typeId")
								+ "&roomName="
								+ encodeURIComponent(encodeURIComponent(roomName))
								+ "", "" ],
				yes : function(index, layero) {
					var name = $(".layui-layer-iframe", parent.document).find(
							"iframe").contents().find("#addCos .chosed")
							.children("td").eq(1).text();
					var code = $(".layui-layer-iframe", parent.document).find(
							"iframe").contents().find("#addCos .chosed")
							.children("td").eq(0).text();
					that.innerText = "选择设备：" + name;
					that.setAttribute("code", code);
					that.setAttribute("name", name);
					$(that).parent().prev().text(code);
					top.layer.close(index)
				},
			})
}

//第一页设备保存
function savePage1Cos() {
	var arr = [];
	$("#pageOneChoseYQ .btn-info").each(function(i, v) {
		var jsonn = {};
		jsonn.id = v.getAttribute("cosid");
		jsonn.code = v.getAttribute("code");
		jsonn.name = v.getAttribute("name");
		arr.push(jsonn);
	});
	
	var changeLog = "试剂制备:";
	$('button[class="btn btn-xs btn-info"]').each(function(i, v) {
		var valnew = $(v).attr("name");
		var val = $(v).attr("code");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	$.ajax({
		type : "post",
		url : "/experiment/reagent/reagentTask/saveCosPrepare.action",
		data : {
			data : JSON.stringify(arr),
			roomName : $("#operatingRoomName").val(),
			roomId : $("#operatingRoomId").val(),
			zbid : $("#cellPassage_id").text(),
			bzs : $(".wizard_steps .selected .step_no").text(),
			changeLog:changeLog
		},
		success : function(res) {
			var data = JSON.parse(res);
			if (data.success) {
//				top.layer.msg("保存成功");
				startPrepare();
			} else {
				top.layer.msg("保存失败");
			}
		}
	});
}

function startPrepare() {
	var arr = [];
	$("#pageOneChoseYQ .btn-info").each(function(i, v) {
		var jsonn = {};
		jsonn.id = v.getAttribute("cosid");
		jsonn.code = v.getAttribute("code");
		jsonn.name = v.getAttribute("name");

		jsonn.rwdid = $("#cellPassage_id").text();
		jsonn.bzid = $(".wizard_steps .selected").attr("stepid");
		jsonn.bzname = $("#steptiele").text();
		jsonn.bzorderNum = $(".wizard_steps .selected .step_no").text();
		arr.push(jsonn);
	});
	var room = $("#operatingRoomName").val();
	var roomid = $("#operatingRoomId").val();
	$.ajax({
		type : "post",
		url : "/experiment/reagent/reagentTask/startPrepare.action",
		data : {
			data : JSON.stringify(arr),
			roomName : room,
			roomId : roomid,
		},
		success : function(res) {
			var data = JSON.parse(res);
			if (data.success) {
				if (data.zy) {
					top.layer.msg("仪器/房间已开启占用！");
				} else {
					top.layer.msg("仪器或房间已被占用请检查！");
				}
			} else {
				top.layer.msg("占用出现异常！");
			}
		}
	});
}

//上传附件
function fileInput1() {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $("#pageFourProductionRecord").attr("pid");
	$("#uploadFileValx").fileinput(
					{
						uploadUrl : ctx
								+ "/attachmentUpload?useType=ddfj&modelType=cellProductionCompleted&contentId="
								+ id,
						uploadAsync : true,
						language : 'zh',
						showPreview : true,
						enctype : 'multipart/form-data',
						elErrorContainer : '#kartik-file-errors',
					});
	$("#uploadFilex").modal("show");
}
// 查看附件
function fileView() {
	top.layer.open({
				title : biolims.common.attachment,
				type : 2,
				skin : 'layui-layer-lan',
				area : [ document.body.clientWidth - 300,
						document.body.clientHeight - 100 ],
				content : window.ctx
						+ "/operfile/initFileList.action?flag=ddfj&modelType=cellProductionCompleted&id="
						+ $("#pageFourProductionRecord").attr("pid"),
				cancel : function(index, layer) {
					top.layer.close(index)
				}
			})
}


var panduan = true;
//完成当前步骤

function finishStep() {
	if ($("#operatingRoomName").val() != null
			&& $("#operatingRoomName").val() != "") {
	} else {
		top.layer.msg("请选择房间");
		return false;
	}
	var arr = [];
	var fuyong = true;
	$("#pageOneChoseYQ .btn-info").each(
			function(i, v) {
				var jsonn = {};
				jsonn.id = v.getAttribute("cosid");
				jsonn.code = v.getAttribute("code");
				jsonn.name = v.getAttribute("name");

				if (jsonn.name != null
						&& jsonn.name != "null"
						&& jsonn.name != "") {
				} else {
					fuyong = false;
					return false;
				}
				arr.push(jsonn);
			});
	if (fuyong == false) {
		top.layer.msg("请选择设备");
		return false;
	}
    var orderNb =$(".wizard_steps .selected .step_no").text();
	if(orderNb=="2"){
		$.ajax({
			type: "post",
			url: "/experiment/reagent/reagentNow/findCk.action",
			data: {
				"serial" : serial,
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					top.layer.confirm("确认继续生产？",{
						  icon: 3,
						  title: biolims.common.prompt,
						 }, function(index) {
						  top.layer.close(index);
						  var bao = false;
							
							var id = "";
							var ids = [];
							var stepNum = $(".wizard_steps .selected .step_no").text(); // 当前得步骤数
							
							$.ajax({
								type : 'post',
								async : false,
								url : ctx
										+ '/experiment/reagent/reagentTask/finishPresentStep.action',
								data : {
									id : id,
									ids : ids,
									hhzt : "1",
									mainId : cellPassage_id,
									stepNum : stepNum,
								},
								success : function(data) {
									var data = JSON.parse(data);
									if (data.success) {
										top.layer.msg("确认完成");
										$("#jxsc").hide();
										$("#maincontentframe",window.parent.document)[0].src = window.ctx
												+ "/experiment/reagent/reagentTask/showReagentTaskSteps.action?id="
												+ cellPassage_id
												+ "&orderNum=" + stepNum;
										panduan = true;
										// noBlendTab.ajax.reload();
									} else {
										panduan = true;
									}
								}
							});
						 });
				}else{
					top.layer.msg("批号有重复，请重新录入");	
				}
			
			}
		});	
	}else{
		
		top.layer.confirm("确认继续生产？",{
			  icon: 3,
			  title: biolims.common.prompt,
			 }, function(index) {
			  top.layer.close(index);
			  var bao = false;
				
				var id = "";
				var ids = [];
				var stepNum = $(".wizard_steps .selected .step_no").text(); // 当前得步骤数
				
				$.ajax({
					type : 'post',
					async : false,
					url : ctx
							+ '/experiment/reagent/reagentTask/finishPresentStep.action',
					data : {
						id : id,
						ids : ids,
						hhzt : "1",
						mainId : cellPassage_id,
						stepNum : stepNum,
					},
					success : function(data) {
						var data = JSON.parse(data);
						if (data.success) {
							top.layer.msg("确认完成");
							$("#jxsc").hide();
							$("#maincontentframe",window.parent.document)[0].src = window.ctx
									+ "/experiment/reagent/reagentTask/showReagentTaskSteps.action?id="
									+ cellPassage_id
									+ "&orderNum=" + stepNum;
							panduan = true;
							// noBlendTab.ajax.reload();
						} else {
							panduan = true;
						}
					}
				});
			 });
		
	}
	
	
	
}

//选择消毒剂
function selectDisinFectant() {
	top.layer.open({
		title: "消毒剂",
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=disinfectant", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
//			$("#disinfectant").val(id);
			$("#disinfectant").val(name);
		},
	})
}
//渲染样本的datatables
function showSampleTable(eletable, stepNum) {
	var colOpts = [];
	  colOpts.push({
			"data" : "id",
			"title" : biolims.common.id,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "id");
			}
		});
		colOpts.push({
			"data" : "storage-id",
			"visible" : false,
			"title" : "原辅料编号",
			"createdCell" : function(td,data) {
				$(td).attr("saveName", "storage-id");
			}
		})
		colOpts.push({
			"data" : "storage-name",
			"title" : biolims.common.reagentName,
			"createdCell" : function(td, data, rowData) {
				$(td).attr("saveName", "storage-name");
				$(td).attr("storage-id", rowData['storage-id']);
				$(td).attr("orderCode", rowData['orderCode']);
//				$(td).attr("num", rowData['num']);
//				$(td).attr("unit", rowData['unit']);
			}
		});
		colOpts.push({
			"data" : "serial",
			"title" : "批号",
			"className":"edit",
			"createdCell" : function(td) {
				$(td).attr("saveName", "serial");
			}
		})
		colOpts.push({
			"data" : "sxDate",
			"title" : "失效期",
			"createdCell" : function(td) {
				$(td).attr("saveName", "sxDate");
			},
		"className":"day",
		})
		colOpts.push({
			"data" : "code",
			"title" : biolims.common.code,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "code");
			}
		})
		colOpts.push({
			"data" : "productId",
			"title" : biolims.common.productId,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "productId");
			}
		})
		colOpts.push({
			"data" : "productName",
			"title" : biolims.common.productName,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "productName");
			}
		})
		colOpts.push({
			"data" : "sampleType",
			"title" : biolims.common.sampleType,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "sampleType");
			}
		})
		colOpts.push({
			"data" : "dicSampleType-id",
			"title" : biolims.common.dicSampleTypeId,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "dicSampleType-id");
			}
		})
		colOpts.push({
			"data" : "dicSampleType-name",
			"title" : biolims.common.dicSampleTypeName,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "dicSampleType-name");
			}
		})
		colOpts.push({
			"data" : "concentration",
			"title" : biolims.common.concentration,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "concentration");
			}
		})
		colOpts.push({
			"data" : "volume",
			"title" : biolims.common.volume,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "volume");
			}
		})
		colOpts.push({
			"data" : "sampleInfo-id",
			"title" : biolims.common.volume,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "sampleInfo-id");
			}
		})
		colOpts.push({
			"data" : "result",
			"title" : biolims.common.result,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "result");
			},
		})
		colOpts.push({
			"data" : "reagents-id",
			"title" : biolims.common.reagentName,
			"visible" : false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "reagents-id");
			},
		})
		colOpts.push({
			"data" : "orderCode",
			"title" : "订单编号",
			"visible" : false,
			"createdCell" : function(td,data) {
				$(td).attr("saveName", "orderCode");
			},
		})
		colOpts.push({
			"data" : "num",
			"title" : biolims.common.count,
			"className" : "edit",
			"visible":false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "num");
			},
		})
		colOpts.push({
			"data" : "unit",
			"title" : biolims.common.unit,
			"className" : "edit",
			"visible":false,
			"createdCell" : function(td) {
				$(td).attr("saveName", "unit");
			},
		})
		colOpts.push({
			"data" : "note",
			"title" : biolims.common.note,
			"className" : "edit",
			"createdCell" : function(td) {
				$(td).attr("saveName", "note");
			}
		})
		colOpts.push({
		"data" : "submit",
		"title" : "是否已提交",
		"name":"是"+"|"+"否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "submit");
			$(td).attr("selectOpt", "是"+"|"+"否");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "否";
			}else if(data == "1") {
				return "是";
			}else{
				return "";
			}
		}
	})
//	colOpts.push({
//		"data" : "counts",
//		"title" : "二氧化碳培养箱编号",
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("saveName", "counts");
//			$(td).attr("instrument-id", rowData['instrument-id']);
//			$(td).attr("instrument-storageContainer-rowNum",
//					rowData['instrument-storageContainer-rowNum']);
//			$(td).attr("instrument-storageContainer-colNum",
//					rowData['instrument-storageContainer-colNum']);
//		}
//	})
//	colOpts.push({
//		"data" : "posId",
//		"title" : "储位",
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "posId");
//		}
//	})
	colOpts.push({
		"data" : "qualified",
		"title" : biolims.common.isQualified,
		"className" : "select",
		"name" : "2" + "|" + "1" + "|" + "3",
		"createdCell" : function(td) {
			$(td).attr("saveName", "qualified");
			$(td).attr(
					"selectOpt",
					biolims.common.qualified + "|"
							+ biolims.common.disqualified + "|" + "警戒");
		},
		"render" : function(data, type, full, meta) {
			if (data == "2") {
				return biolims.common.qualified;
			}
			if (data == "1") {
				return biolims.common.disqualified;
			}
			if (data == "3") {
				return "警戒";
			} else {
				return '';
			}
		}
	})
	var tbarOpts = [];
	if (baocun) {
		if (caozuo) {
		
			tbarOpts.push({
				text : '<i class="glyphicon glyphicon-th"></i>'+ biolims.common.applicationOper,
				action : function() {
					top.layer.open({
  					  type: 1,
  					  title:"生成结果",
  					  closeBtn: 1,
  					  offset: '100px',
  					  skin: 'yourclass',
  					  btn: ['确定'],
  					  anim: 3,
  					  btnAlign: 'c',
  					  content: `
  					           <div class="col-xs-12" style=" margin-top: 20px;">	
									<div class="input-group">
										<span class="input-group-addon">产物数量</span> 
  						  				<input type="text" class="form-control result" id="reagentResult"></input>
									</div>
								</div>` ,
  					 yes: function(index,layero){
  						 var result = layero.find("#reagentResult").val()
  						 var flage = true;
  						 if(result==null||result==""){
					        	top.layer.msg("请填写产物数量！");
				        		flage=false;
				        		return false;
					        }
  						 console.log(result)
  						 if(flage){
  							$.ajax({
  	  							 type : "post",
  	  							 data : {
  	  								 id : $("#cellPassage_id").text(),
  	  								 resultNum:result
  	  							 },
  	  							 url : ctx + "/experiment/reagent/reagentNow/saveRoductionResults.action",
  	  							 success : function(dataBack) {
  	  								 var data = JSON.parse(dataBack)
  	  								 if (data.success) {
  	  									 top.layer.msg("生成结果成功!")
  	  									 blendTab.ajax.reload();
  	  									 top.layer.closeAll();
  	  								 } else {
  	  									 top.layer.msg(biolims.purchase.failed)
  	  								 }
  	  							 }
  							}); 
  						 }
  					 }
				}); 
//					$.ajax({
//								type : "post",
//								data : {
//									id : $("#cellPassage_id").text()
//								},
//								url : ctx + "/experiment/reagent/reagentNow/saveRoductionResults.action",
//								success : function(dataBack) {
//									var data = JSON.parse(dataBack)
//									if (data.success) {
//										blendTab.ajax.reload();
//									} else {
//										top.layer.msg(biolims.purchase.failed)
//									}
//
//								}
//							});
				}
			});
	
			tbarOpts.push({
				
				text : '<i class="glyphicon glyphicon-upload"></i>'
						+ "选择质检项"
				/* biolims.common.submissionQuality */,
				action : function() {
					var rows = $("#noBlendTable .selected");
					if (!rows.length) {
						top.layer.msg(biolims.common.pleaseSelectData);
						return false;
					}
					var idList = [];
					rows.each(function(i, v) {
						idList.push($(v).find(".icheck").val());
					
					});

					var bodyZhijian = '';
					if($("#zhijianBody .zhijianli").length!=0){
						var zhijianlikong = true;
						$("#zhijianBody .zhijianli").each(
							function(i, v) {
								if($(v).attr("id")==undefined){
									top.layer.msg("该步骤没有配置质检项！");
									zhijianlikong = false;
								}
							}
							
						);
						if(zhijianlikong){
							
						}else{
							return false;
						}
					}
					$("#zhijianBody .zhijianli").each(
									function(i, v) {
										bodyZhijian += '<tr>'
												+ '<td>'
												+ $(v).find(".zhijianName").text()
												+ '</td>'
												+ '<td><input type="checkbox" class="icheckzj" value="'
												+ $(v).find(".zhijianName").attr("zhijianid")
												+ '" /></td>'
												+ '<td><input type="text" style="" /></td>'
												+ '<td><input type="text" style="" /></td>'
												+ '<td><input type="button" sampleTypeid="" value="选择样本名称" class="form-control btn-success" onclick="xuanze(this)"/></td>'
												+ '<td><input type="text" style="" /></td>'
												+ '</tr>';
									});
					var cheboxZhijian ='<div class="ipadmini">'+'<table class="table">'
							+ '<tr>' + '<th>检测项</th>' + '<th>选择</th>'
							+ '<th>样本量</th>' + '<th>单位</th>'
							+ '<th>样本名称</th>' + '<th>备注</th>' + '</tr>'
							+ bodyZhijian + '</table>'+'</div>';
					var chosedID = [];
					layer.open({
								title : "选择质检项目",
								offset: '40px',
								btn : biolims.common.selected,
								type : 1,
								area : [ document.body.clientWidth - 300,'500px' ],  								
								btnAlign : 'c',
								content : cheboxZhijian,
								success : function() {
									$('.layui-layer-btn-c').css({
										'position': 'absolute',
										'top':'0',
										'left': '50%',
										'transform': 'translateX(-50%)'
									})
									
									$('.icheckzj').iCheck({
														checkboxClass : 'icheckbox_square-blue',
														increaseArea : '20%' // optional
													}).on('ifChanged',function(event) {
														// var arr=[];
														// $("#pageOneChoseYQ
														// .btn-info").each(function
														// (i,v) {
														// var jsonn={};
														// jsonn.id=v.getAttribute("cosid");
														// jsonn.code=v.getAttribute("code");
														// jsonn.name=v.getAttribute("name");
														// arr.push(jsonn);
														// });
														// $.ajax({
														// type:"post",
														// url:"/experiment/cell/passage/cellPassage/saveCosPrepare.action",
														// data:{data:JSON.stringify(arr)},

														if ($(this).is(':checked')) {
															chosedID.push(this.value);
														} else {
															var value = this.value;
															chosedID = chosedID.filter(function(v) {
																		return v != value;
																	})
														}
													});
								},
								yes : function(index, layero) {
									if (chosedID.length > 0) {
										var arr = [];
										var panduanlx = true;
										for (var j = 0, len = chosedID.length; j < len; j++) {
											var jsonn = {};
											jsonn.jcxmc = $("input[type='checkbox'][value='"
															+ chosedID[j]
															+ "']:checked").parent().parent().prev().text();
											jsonn.jcxid = chosedID[j];
											jsonn.jcsl = $("input[type='checkbox'][value='"
															+ chosedID[j]
															+ "']:checked").parent().parent().next().find("input[type='text']").val();
											jsonn.jcxlxmc = $("input[type='checkbox'][value='"
															+ chosedID[j]
															+ "']:checked").parent().parent().next().next().find("input[type='text']").val();
											jsonn.jcxlx = $("input[type='checkbox'][value='"
															+ chosedID[j]
															+ "']:checked").parent().parent().next().next().next().find("input[type='button']").attr("sampletypeid");
											jsonn.jcbz = $("input[type='checkbox'][value='"
															+ chosedID[j]
															+ "']:checked").parent().parent().next().next().next().next().find("input[type='text']").val();
											jsonn.jcxsldw = $("input[type='checkbox'][value='"
															+ chosedID[j]
															+ "']:checked").parent().parent().next().next().find("input[type='text']").val();
													

											if (jsonn.jcxlx == null
													|| jsonn.jcxlx == ""
													|| jsonn.jclxmc == "选择样本类型") {
												top.layer
														.msg("请选择质检样本类型");
												panduanlx = false;
											}
											arr.push(jsonn);
										}
										if (panduanlx) {
											
											$.ajax({
													type : "post",
													url : ctx+ "/experiment/reagent/reagentTask/submitToQualityTestType.action",
													data : {
														idList : idList,
														chosedID : chosedID,
														mark : "ReagentTask",
														stepNum : $(".wizard_steps .selected").children(".step_no").text(),
														stepName : $(".wizard_steps .selected").children(".step_descr").text(),
														id : cellPassage_id,
														zj : $("#zjType").attr("zjid"),
														data : JSON.stringify(arr)
														},
													success : function(res) {
					
														var data = JSON.parse(res);
															if (data.success) {
																// top.layer.msg(biolims.common.submissionofSuccess);
																top.layer.msg("选择成功，请在质检明细中提交质检！");
																zhijianDetails.ajax.reload();
															} else {
																top.layer.msg("选择失败，请重新选择！");
																// top.layer.msg(biolims.common.submissionFailure);
															}
														}
													});
											layer.close(index);
										} else {

										}

									} else {
										top.layer.msg("请选择质检项");
									}
								},
							});
				}
			});
//			tbarOpts.push({
//						text : "选择培养箱",
//						action : function() {
//							var rows = $("#noBlendTable .selected");
//							var length = rows.length;
//							if (!length) {
//								top.layer.msg(biolims.common.pleaseSelect);
//								return false;
//							}
//							top.layer.open({
//										title : "选择培养箱",
//										type : 2,
//										area: top.screeProportion,
//										btn : biolims.common.selected,
//										content : [
//												window.ctx
//														+ "/experiment/reagent/reagentNow/selectInstrumentOne.action",
//												'' ],
//										yes : function(index, layer) {
//											var id = $('.layui-layer-iframe',
//													parent.document)
//													.find("iframe")
//													.contents()
//													.find(
//															"#addDicSampleType .chosed")
//													.children("td").eq(0)
//													.text();
//											var num = $('.layui-layer-iframe',
//													parent.document)
//													.find("iframe")
//													.contents()
//													.find(
//															"#addDicSampleType .chosed")
//													.children("td").eq(6)
//													.text();
//											if (num != "" && num > 0) {
//												rows.addClass("editagain");
//												rows
//														.find(
//																"td[savename='counts']")
//														.attr("instrument-id",
//																id).text(id);
//												top.layer.close(index);
//											} else {
//												top.layer
//														.msg("该培养箱空位不足，请选择其他培养箱");
//											}
//
//										},
//									});
//						}
//					});
			
			//样本名称按钮
//			tbarOpts.push({
//				text : "选择样本名称",
//				action : function() {
//					var rows = $("#blendTable .selected");
//					var length = rows.length;
//					if (!length) {
//						top.layer.msg(biolims.common.pleaseSelect);
//						return false;
//					}
//					top.layer.open({
//									title : "选择样本名称",
//									type : 2,
//									area: top.screeProportion,
//									btn : biolims.common.selected,
//									content : [
//											window.ctx
//													+ "/sample/dicSampleType/selectDicSampleTypeOne.action",
//											'' ],
//									yes : function(index, layer) {
//										var type = $('.layui-layer-iframe', parent.document).find(
//												"iframe").contents().find(
//												"#addDicSampleType .chosed").children("td").eq(1)
//												.text();
//										var id = $('.layui-layer-iframe', parent.document).find(
//												"iframe").contents().find(
//												"#addDicSampleType .chosed").children("td").eq(0)
//												.text();
//										rows.addClass("editagain");
////										rows.find("td[savename='sampleType-name']").attr(
////												"sampletype-id", id).text(type);
//										rows.find("td[savename='sampleType']").attr(
//												"sampletype-id", id).text(type);
//										/*rows.find("td[savename='sampleType']").text(type);*/
//										top.layer.close(index)
//									},
//								})
//					}
//					//
//			});
		}
	}
	var cellPassageMakeUpAfOps = table(true,  cellPassage_id,
			"/experiment/reagent/reagentTask/showReagentTaskResultTableJson.action", colOpts, tbarOpts);
	blendTab = renderData(eletable, cellPassageMakeUpAfOps);
//	preAndNext();

//	var cellPassageMakeUpAfOps = table(true, cellPassage_id,
//			"/experiment/cell/passage/cellPassage/blendPlateSampleTable.action?stepNum="
//					+ stepNum, colOpts, tbarOpts);
//	blendTab = renderData(eletable, cellPassageMakeUpAfOps);
	blendTab.on('draw', function() {
//		$("#noBlendTable tbody tr").each(
//				function(i, val) {
//					if ($(val).find("td[savename='counts']").text() != "") {
//						counts = $(val).find("td[savename='counts']").text();
//						rowNum = $(val).find("td[savename='counts']").attr(
//								"instrument-storagecontainer-rownum");
//						colNum = $(val).find("td[savename='counts']").attr(
//								"instrument-storagecontainer-colNum");
//					}
//				});
//		$("#noBlendTable tbody tr").each(function(i, val) {
//			if ($(val).find("td[savename='counts']").length == 0) {
//				counts = "";
//			} else {
//
//			}
//		});
//		if (counts != "") {
//			$("#plateModal").show();
//			// 根据上一步选择页面变化生成孔板
//			// rendrModalDiv($("#maxNum").val(), $("#temRow").val(),
//			// $("#temCol").val());
//			$("#plateNum .plate").text("");
//			$("#plateNum").attr("plateNum", counts);
//			rendrModalDiv(1, rowNum, colNum);
//			// 填充孔板
//			createShelf(rowNum, colNum, $(".plate"), renderChosedSample);
//			showPlate();
//		} else {
//			$("#plateModal").hide();
//		}
		oldChangeLog = blendTab.ajax.json();
	});
}
var counts = "";
var rowNum = "";
var colNum = "";
//if (counts != "") {
//	$("#plateModal").show()
//	// 根据上一步选择页面变化生成孔板
//	// rendrModalDiv($("#maxNum").val(), $("#temRow").val(),
//	// $("#temCol").val());
//	$("#plateNum .plate").text("");
//	$("#plateNum").attr("plateNum", counts);
//	rendrModalDiv(1, rowNum, colNum);
//	// 填充孔板
//	createShelf(rowNum, colNum, $(".plate"), renderChosedSample);
//	showPlate();
//} else {
//	$("#plateModal").hide();
//}

//渲染已提交质检明细datatables
function showZhijianDetails(eletable, stepNum) {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data": "SampleNumber",
		"title": "产品批号",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "SampleNumber");
			$(td).attr("qualityInfoId", rowData['qualityInfoId']);
		}
	})
//	colOpts.push({
//		"data" : "code",
//		"title" : "样本编号",
//		"visible":false,
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("saveName", "code");
//			$(td).attr("qualityInfoId", rowData['qualityInfoId']);
//		}
//	})
	colOpts.push({
		"data" : "sampleType",
		"title" : "样本名称",
	})
	colOpts.push({
		"data" : "sampleNum",
		"title" : "检验量",
	})
	colOpts.push({
		"data" : "sampleNumUnit",
		"title" : "单位",
	})
	colOpts.push({
		"data" : "productName",
		"title" : "检测项目",
		"visible":false,
	})
	
	colOpts.push({
		"data" : "storage-id",
		"title" : "原辅料编号",
		"visible":false,
	})
	colOpts.push({
		"data" : "storage-name",
		"title" : "原辅料",
	})
	colOpts.push({
		"data" : "cellType",
		"title" : "检测方式",
		// "className":"select",
		"name" : "|自主检测|第三方检测",
		"createdCell" : function(td) {
			$(td).attr("saveName", "cellType");
			$(td).attr("selectOpt", "|自主检测|第三方检测");
		},
		"render" : function(data, type, full, meta) {
			if (data == "3") {
				return "自主检测";
			}
			if (data == "7") {
				return "第三方检测";
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data" : "sampleDeteyion-name",
		"title" : "检测项",
	});
	colOpts.push({
		"data" : "stepNum",
		"title" : "步骤",
	});
	colOpts.push({
		"data" : "experimentalStepsName",
		"title" : "步骤名称",
	});
	colOpts.push({
		"data" : "submit",
		"title" : "质检是否提交",
		// "className":"select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "submit");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			}
			if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data" : "qualitySubmitTime",
		"title" : "质检提交时间",
	});
	colOpts.push({
		"data" : "qualitySubmitUser",
		"title" : "质检提交人",
	})
	colOpts.push({
		"data" : "qualityReceiveTime",
		"title" : "质检接收时间",
	});
	colOpts.push({
		"data" : "qualityFinishTime",
		"title" : "质检完成时间",
	});
	colOpts.push({
		"data" : "qualified",
		"title" : biolims.common.isQualified,
		// "className": "select",
		"name" : biolims.common.qualified + "|" + biolims.common.disqualified
				+ "|" + "警戒",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "qualified");
			$(td).attr(
					"selectOpt",
					biolims.common.qualified + "|"
							+ biolims.common.disqualified + "|" + "警戒");
			if (rowData['qualified'] == "1") {
				$(td).parent().addClass("qualified");
			}
			if (rowData['qualified'] == "3") {
				$(td).parent().addClass("qualified");// .css('color','red');
			}
		},
		"render" : function(data, type, full, meta) {
			if (data == "2") {
				return biolims.common.qualified;
			}
			if (data == "1") {
				return biolims.common.disqualified;
			}
			if (data == "3") {
				return "警戒";
			} else {
				return '';
			}
		}
	})
	colOpts.push({
		"data" : "qualityInfoId",
		"title" : "质检结果id",
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "qualityInfoId");
		},
	})
	colOpts.push({
		"data" : "note",
		"title" : "备注",
	})
	var tbarOpts = [];
	if (baocun) {
		if (caozuo) {
			tbarOpts
					.push({
						text : '删除',
						action : function() {
							var panduan = true;
							var rows = $("#zhijianDetails .selected");
							var length = rows.length;
							if (!length) {
								top.layer.msg("请选择数据");
								return false;
							}
							var ids = [];
							$.each(rows,
									function(j, k) {
										ids.push($(k).find(
												"input[type=checkbox]").val());
										if ($(k).find("td[savename=submit]")
												.text() == "是") {
											panduan = false;
										}
									});
							if (panduan) {
								ajax(
										"post",
										"/experiment/reagent/reagentTask/delQualityTestSample.action",
										{
											ids : ids,
											id:$("#cellPassage_id").text(),
										},
										function(data) {
											if (data.success) {
												top.layer.msg("删除成功！");
												zhijianDetails.ajax.reload();
											} else {
												top.layer.msg(biolims.common.submitFail);
											}
										}, null);
							} else {
								top.layer.msg("所选数据存在已提交数据，不允许删除！");
							}
						}
					});
			tbarOpts.push({
						text : '提交质检',
						action : function() {
							var panduan = true;
							var rows = $("#zhijianDetails .selected");
							var length = rows.length;
							if (!length) {
								top.layer.msg("请选择数据");
								return false;
							}
							var ids = [];
							$.each(rows,
									function(j, k) {
										ids.push($(k).find(
												"input[type=checkbox]").val());
										if ($(k).find("td[savename=submit]")
												.text() == "是") {
											panduan = false;
										}
									});
							if (panduan) {
								ajax(
										"post",
										"/experiment/reagent/reagentTask/submitToQualityTestSample.action",
										{
											ids : ids,
											id:$("#cellPassage_id").text(),
										},
										function(data) {
											if (data.success) {
												top.layer
														.msg(biolims.common.submitSuccess);
												zhijianDetails.ajax.reload();
											} else {
												top.layer
														.msg(biolims.common.submitFail);
											}
										}, null);
							} else {
								top.layer.msg("所选数据存在已提交数据，请重新选择！");
							}
						}
					});
		}
	}
	tbarOpts.push({
				text : '查看质检结果',
				action : function() {
					var rows = $("#zhijianDetails .selected");
					var length = rows.length;
					if (length != 1) {
						top.layer.msg("请选择一条数据");
						return false;
					}
					var infoid = "";
					rows.each(function(i, v) {
						infoid = $(v).find("td[savename='SampleNumber']").attr('qualityinfoid');

					})
					// $.each(rows, function(j, k) {
					// alert($(k).find("td[savename='id']").text())
					// infoid =
					// $(k).find("td[savename='qualityInfoId']").text();
					// });
					if (infoid == "") {
						top.layer.msg("该条记录还没有质检结果请稍后再试！");
						return false;
					}
					top.layer
							.open({
								title : "质检结果",
								type : 2,
								area: top.screeProportion,
								btn : "关闭",
								content : [
										window.ctx
												+ "/experiment/reagent/reagentTask/findQualityInfoByid.action?infoid="
												+ infoid, '' ],
								yes : function(index, layer) {
									top.layer.close(index)
								},
							})
				}
			});
//	tbarOpts.push({
//		text : '打印请验单',
//		action : function() {
//			var rows = $("#zhijianDetails .selected");
//			var length = rows.length;
//			if (!length) {
//				top.layer.msg("请选择数据！");
//				return false;
//			} else {
//				var result = "";
//				$("#zhijianDetails .selected").each(
//						function(i, o) {
//							if (result == "") {
//								result += $(this).find("input[type=checkbox]")
//										.val();
//							} else {
//								result += ","
//										+ $(this).find("input[type=checkbox]")
//												.val();
//							}
//						})
//						
//						$.ajax({
//						type : "post",
//						data : {
//							id : $("#sampleReveice_id").text(),
//							confirmDate : $("#cellPassage_createDate").text(),
//							modelId:"dyqyd"
//						},
//						url : ctx
//								+ "/stamp/birtVersion/selectBirt.action",
//						success : function(data) {
//							var data = JSON.parse(data)
//							if (data.reportN) {
//								
//								var url = '__report='+data.reportN+'&id=' + result;
//								commonPrint(url);
//								
//							} else {
//								top.layer.msg("没有报表信息！");
//							}
//						}
//					});
//			
//			}
//		}
//	})
	var options = table(true, cellPassage_id,
			"/experiment/reagent/reagentTask/findQualityItem.action",
			colOpts, tbarOpts);
	zhijianDetails = renderData(eletable, options);
}

//生成结果的乐观锁
var blendSamplelgs = true;
// 混合样本
function blendSample() {
	if (blendSamplelgs) {
		blendSamplelgs = false;

		var ids = [];
		var rows = $("#noBlendTable").find("tbody").children("tr");
		var blendRows = $("#noBlendTable").find("tbody").children("tr");
		var panduan = true;
		$.each(blendRows, function(ii, vv) {
			var id = $(vv).find("input[type='checkbox']").val();
			if (id != null && id != "" && id != undefined) {
				top.layer.msg("样本已经操作,请勿再执行此操作");
				panduan = false;
				return false;
			}
		});

	}

}




//渲染质检
function renderZhijian(zhijian) {
	var zhijianLis = "";
	var id = zhijian.id ? zhijian.id : "";
	var code = zhijian.code ? zhijian.code : "";
	var name = zhijian.name ? zhijian.name : "";
	var nextId = zhijian.nextId ? zhijian.nextId : "";
	var itemId = zhijian.itemId ? zhijian.itemId : "";
	zhijianLis += '<li class="zhijianli zhijianOld" id='
			+ id
			+ '><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span nextid=\''
			+ nextId
			+ '\' zhijianid=\''
			+ code
			+ '\' class="zhijianName">'
			+ name
			+ '</span></small><small><i testId=\''
			+ code
			+ '\' stepNum=\''
			+ itemId
			+ '\' onclick="showResult(this)" class="fa fa-search-plus pull-right"></i></small></li>';
	$("#zhijianBody").append(zhijianLis);
}

//展示质检提交到哪个质检的结果内容(改成查看该样本所有质检的结果)
function showResult(self) {

	// var stepNum = $(self).attr("stepNum");
	// var testId = $(self).attr("testId");
	// 任务单号
	var testId = $("#cellPassage_id").text();
	var mark = "ReagentTask";
	top.layer
			.open({
				title : "质检结果",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/experiment/quality/qualityTest/showQualityTestResultListAllForzj.action?testId="
								+ testId // " +
						// "stepNum=" + stepNum + "&testId=" + testId +
						// "&mark=" + mark
						, '' ],
				yes : function(index, layer) {
					top.layer.closeAll();
				},
			})
}

//选择样本质检类型
function xuanze(that) {
	top.layer
			.open({
				title : "选择样本名称",
				type : 2,
				area: top.screeProportion,
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/sample/dicSampleType/selectDicSampleTypeOne.action",
						'' ],
				yes : function(index, layer) {
					var type = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(1)
							.text();
					var id = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addDicSampleType .chosed").children("td").eq(0)
							.text();
					$(that).val(type);
					$(that).attr("sampleTypeid", id);
					top.layer.close(index)
				},
			})
}

//获得保存时的json数据
function savenoBlendTablejson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");

			// 是否合格
			if (k == "qualified") {
				var qualified = $(tds[j]).text();
				if (qualified == "合格") {
					json[k] = "2";
				} else if (qualified == "不合格") {
					json[k] = "1";
				} else if (qualified == "警戒") {
					json[k] = "3";
				} else {
					json[k] = "";
				}
				continue;
			}

			// 板子
			if (k == "counts") {
				json["instrument-id"] = $(tds[j]).attr("instrument-id");
			}
			
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

//根据上一步选择页面变化生成孔板
function rendrModalDiv(num, row, col) {
	if (row > 8) {
		for (var i = 1; i < num; i++) {
			var clonePlateDiv = $(".plateDiv").eq(0).clone();
			clonePlateDiv.find("#plateNum").attr("plateNum", counts);// "p" +
			// (i +
			// 1)
			$("#plateModal").append(clonePlateDiv);
		}
	} else {
		if (num > 1) {
			$(".plateDiv").removeClass("col-xs-12").addClass("col-xs-6");
			for (var i = 1; i < num; i++) {
				var clonePlateDiv = $(".plateDiv").eq(0).clone();
				clonePlateDiv.find("#plateNum").attr("plateNum", counts);// "p"
				// + (i
				// + 1)
				$("#plateModal").append(clonePlateDiv);
			}
		}
	}
	$(".plateDiv").find(".btn-sm").click(function() {
		$(this).addClass("active").siblings(".btn-sm").removeClass("active");
	});
}
/**
 * 根据要求生成要求规格的架子或盒子
 * 
 * @param m =>
 *            行
 * @param n =>
 *            列
 */
function createShelf(m, n, element, callback) {
	var m = parseInt(m);
	var n = parseInt(n);
	// $(element).empty();
	var arr = [ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "k", "L",
			"M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y",
			"Z" ];
	var tr0 = document.createElement("tr");
	var num = "";
	for (var i = 0; i <= n; i++) {
		num += '<td>' + i + '</td>';
	}
	tr0.innerHTML = num;
	tr0.children[0].innerHTML = "X";
	$(element).append(tr0);
	var h = 0;
	for (var i = 0; i < m; i++) {
		var tr = document.createElement("tr");
		var tds = "";
		var x = i + 1;
		for (var j = 0; j <= n; j++) {
			var jj = j < 10 ? "0" + j : j;
			tds += '<td><div x=' + arr[i] + ' y=' + j + ' z=' + (x - m)
					+ ' coord=' + arr[i] + jj + '></div></td>';
			x += m;
		}
		tr.innerHTML = tds;
		tr.children[0].innerHTML = arr[i];
		$(element).append(tr);
	}
	var divs = $(element).find("div");
	for (var i = 0; i < divs.length; i++) {
		var h = i + 1;
		divs[i].setAttribute("h", h);
	}

	callback();
}
//把选择的样本放在孔板上
function renderChosedSample() {
	var data = [];
	// 在孔板上悬浮提示
	$(".plate div").mouseover(function() {
		var plateDiv = $(this).parents(".plateDiv");
		var plate = plateDiv.find(".plate");
		var order = plateDiv.find(".active").attr("order");
		var that = this;
		if (order == "h") {
			// 样本横向排列悬浮提示
			orientationSampleHover(that, plate);
		} else if (order == "z") {
			// 样本纵向排列悬浮提示
			portraitSampleHover(that, plate);
		}

	});
	$(".plate div").mouseout(function() {
		$(".plate div").css('box-shadow', "none");
	});
	// 在孔板上点击赋值坐标
	var clickIndex = 0;
	$(".plate div")
			.click(
					function() {
						var sampleId = [];
						$("#noBlendTable .selected").each(
								function(i, val) {
									sampleId.push($(val).children("td").eq(0)
											.find("input").val());
								});
						if (sampleId.length > 0) {
							if (baocun) {
								if (caozuo) {
									if (this.title) {
										top.layer.msg("当前位置有样本，请换位置存储");
										return false;
									}
									var plateDiv = $(this).parents(".plateDiv");
									var plate = plateDiv.find(".plate");
									var order = plateDiv.find(".active").attr(
											"order");
									var that = this;
									if (order == "h") {
										// 样本横向排列
										data = orientationSampleClick(that,
												plate);
									} else if (order == "z") {
										// 样本纵向排列
										clickIndex++;
										data = portraitSampleClick(that, plate,
												clickIndex);
									}
									$.ajax({
												type : "post",
												url : ctx
														+ "/experiment/reagent/reagentNow/plateLayout.action",
												data : {
													data : data
												},
												success : function(data) {
													$("#plateModal").find(
															".sel").removeAttr(
															"sid");
													$("#plateModal").find(
															".sel").removeAttr(
															"title");
													$("#plateModal").find(
															".sel").css({
														"background-color" : ""
													});
													$("#plateModal").find(
															".sel")
															.removeClass("sel");
													blendTab.ajax.reload();
													// qualityTestMakeUpAfTab.ajax.reload();
												}

											});
								} else {
									top.layer.msg("您无权限使用该样本");
									return false;
								}
							} else {
								top.layer.msg("您无权限使用该样本");
								return false;
							}

						} else {
							if ($(this).attr("sId")) {
								$("#plateModal").find(".sel")
										.removeClass("sel");
								$(this).addClass("sel");
								var sid = $(this).attr("sId");
								$("#noBlendTable tbody .icheck").each(
										function(i, val) {
											$(val).iCheck('uncheck');
											if (val.value == sid) {
												$(val).iCheck('check');
											}
										});
								$("#plateModal .mysample").css("border",
										"1px solid gainsboro");
							} else {
								top.layer.msg("当前样本不是该步骤样本，无法移动该样本！");
							}
						}
					});
}

//渲染已保存到孔板上的样本
function showPlate() {
	$
			.ajax({
				type : "post",
				url : ctx
						+ "/experiment/reagent/reagentNow/showWellPlate.action",
				data : {
					id : counts
				},
				success : function(data) {
					var data = JSON.parse(data);
					for (var i = 0; i < data.data.length; i++) {
						var platePoint = $("#plateModal").find(
								"div[plateNum='" + counts + "']").find(
								"div[coord='" + data.data[i].location + "']")[0];
						platePoint.style.backgroundColor = "#007BB6";
						platePoint.setAttribute("title", data.data[i].batch);
						platePoint.setAttribute("sId", data.data[i].sId);
						platePoint.className = "mysample";
					}
				}
			})
}

//样本横向排列悬浮提示
function orientationSampleHover(that, plate) {
	var hh = parseInt(that.getAttribute("h"));
	var cc = $("#noBlendTable .selected").length;
	var holes = plate.find("div");
	for (var j = 0; j < holes.length; j++) {
		if (hh <= holes[j].getAttribute("h")
				&& holes[j].getAttribute("h") < hh + cc) {
			holes[j].style.boxShadow = "0 0 3px #007BB6";
		}
	}
}
// 样本横向排列点击渲染
function orientationSampleClick(that, plate) {
	var hh = parseInt(that.getAttribute("h"));
	var samples = $("#noBlendTable .selected");
	var cc = samples.length;
	var holes = plate.find("div");
	var positionArr = [];
	var data = [];
	var flag = 0;
	var first;
	for (var j = 0; j < holes.length; j++) {
		if (hh <= holes[j].getAttribute("h")
				&& holes[j].getAttribute("h") < hh + cc) {
			if (flag == 0) {
				first = j;
				flag++;
			}
			var sampleCode = samples.eq(j - first).children(
					"td[savename='code']").text();
			var id = samples.eq(j - first).children("td").eq(0).find(".icheck")
					.val();
			var posId = holes[j].getAttribute("coord");
			var plateNum = plate.parent("#plateNum")[0]
					.getAttribute("platenum");
			// samples.eq(j-first).children("td[savename='posId']").text(holes[j].getAttribute("coord"));
			data.push(id + "," + posId + "," + plateNum)
			holes[j].setAttribute("title", sampleCode);
			holes[j].setAttribute("sId", id);
			holes[j].className = "mysample";
			holes[j].style.backgroundColor = "#007BB6";
		}
	}
	return data;
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		if (!id) {
			changeLog += '新增记录:';
			for ( var k in v) {
				var title = ele.find("th[savename=" + k + "]").text();
				changeLog += '"' + title + '"为"' + v[k] + '";';
			}
			return true;
		}
		changeLog += 'ID"' + v.id + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}

