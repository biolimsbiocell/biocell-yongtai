﻿$(function() {

	// 办理按钮控制
	bpmTask($("#bpmTaskId").val());
	// 选中模板的实验组的实验员显示隐藏
	if($(".sopChosed").length) {
		var userGroup = $(".sopChosed").attr("suerGroup");
		$(".userLi").each(function(i, v) {
			var userGroupid = v.getAttribute("suerGroup");
			if(userGroup == userGroupid) {
				$(v).show();
			} else {
				$(v).hide();
			}
		});
	}
	// sop和实验员的点击操作
	sopAndUserHandler();
	// 保存操作
	allotSave();
});
// sop和实验员的点击操作
function sopAndUserHandler() {
	// 执行sop点击选择操作
	$(".sopLi").click(function() {
		$(this).addClass("sopChosed").siblings("li").removeClass("sopChosed");
		// 显示隐藏模板里配置的实验员
		var userGroup = this.getAttribute("suerGroup");
		$(".userLi").each(function(i, v) {
			var userGroupid = v.getAttribute("suerGroup");
			if(userGroup == userGroupid) {
				$(v).slideDown();
			} else {
				$(v).slideUp().children("img").removeClass("userChosed");
			}
		});
	});
	// 执行实验员点击选择操作
	$(".userLi").click(function() {
		var img = $(this).children("img");
		if(img.hasClass("userChosed")) {
			img.removeClass("userChosed");
		} else {
			img.addClass("userChosed");
		}
	});
}
// 保存操作
function allotSave() {
	$("#save").click(function() {
		var changeLog = "试剂制备";
		changeLog = getChangeLog(userId,changeLog);
		var changeLogs = "";
		if (changeLog != "试剂制备：") {
			changeLogs = changeLog;
		}
		if(!$(".sopChosed").length) {
			top.layer.msg(biolims.common.selectTaskModel);
			return false;
		}
		if(!$(".userChosed").length) {
			top.layer.msg(biolims.sample.pleaseSelectExperimenter);
			return false;
		}
		// 获得选中实验员的ID
		var userId = [];
		$(".userChosed").each(function(i, val) {
			userId.push($(val).parent("li").attr("userid"));
		});
		// 拼接主表的信息
		var main = {
			id: $("#reagentTask_id").val(),
			name: $("#reagentTask_name").val(),
			createUserId: $("#reagentTask_createUser").attr("userId"),
			createDate: $("#reagentTask_createDate").text(),
			state: $("#reagentTask_state").attr("state"),
			stateName: $("#reagentTask_state").text(),
		};
		var data = JSON.stringify(main);
		// 拼接保存的data
		var info = {
			template: $(".sopChosed").attr("sopid"),
			user: userId.join(","),
			main: data,
			logInfo:changeLog
		}
		top.layer.load(4, {
			shade: 0.3
		});
		$.ajax({
			type: 'post',
			url: ctx +"/experiment/reagent/reagentNow/saveAllotNow.action",
			data: info,
			success: function(data) {
				var data = JSON.parse(data);
				if(data.success) {
					top.layer.closeAll();
					top.layer.msg(biolims.common.saveSuccess);
					window.location = window.ctx +
						'/experiment/reagent/reagentTask/editReagentTask.action?id=' +
						data.id;
				} else {
					top.layer.closeAll();
					top.layer.msg("保存失败");
				}
			}
		})
	});
}
//下一步操作
$("#next").click(function () {
	var reagentTask_id=$("#reagentTask_id").val();
	console.log(reagentTask_id);
	if(reagentTask_id=="NEW"){
		top.layer.msg(biolims.common.pleaseSave);
		return false;
	}
	//现在的生产
	$("#maincontentframe", window.parent.document)[0].src =window.ctx+"/experiment/reagent/reagentNow/showreagentTaskEdit.action?id="+reagentTask_id+"&bpmTaskId="+$("#bpmTaskId").val();

});
// 获取描述和容器数量he模板sop的修改日志
function getChangeLog(userId, changeLog) { // 获取待处理样本的修改日志
	// 获取描述和容器数量的修改日志
	$('input[class="form-control"]').each(
		function(i, v) {
			var valnew = $(v).val();
			var val = $(v).attr("changelog");
			if(val !== valnew) {
				changeLog += $(v).prev("span").text() + ':由"' + val +
					'"变为"' + valnew + '";';
			}
		});
	// 获取模板sop的修改日志
	if($(".sopChosed").attr("sopid") != $("#sopChangelogId").val()) {
		changeLog += '生产模板:由"' + $("#sopChangelogName").val() + '"变为"' +
			$(".sopChosed").children('.text').text() + '";';
	}
	// 获取实验员的修改日志
	var oldUserId = [],
		userName = [],
		oldUserName = [];
	$('input[class="changelogUserid"]').each(function(i, v) {
		oldUserId.push(v.value);
	});
	if(userId != oldUserId) {
		$(".userChosed").each(function(i, val) {
			userName.push($(val).next("a").text());
		});
		$('input[class="changelogUsername"]').each(function(i, v) {
			oldUserName.push(v.value);
		});
		changeLog += '实验员:由"' + oldUserName + '"变为"' + userName + '";';
	}

	return changeLog;
}