﻿var oldChangeLog;
var reagentTaskResultTab;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : biolims.common.id,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		}
	});
	colOpts.push({
		"data" : "storage-id",
		"visible" : false,
		"title" : "原辅料编号",
		"createdCell" : function(td,data) {
			$(td).attr("saveName", "storage-id");
		}
	})
	colOpts.push({
		"data" : "storage-name",
		"title" : biolims.common.reagentName,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "storage-name");
			$(td).attr("storage-id", rowData['storage-id']);
			$(td).attr("orderCode", rowData['orderCode']);
		}
	});
	colOpts.push({
		"data" : "serial",
		"title" : "批号",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "serial");
		}
	})
//	colOpts.push({
//		"data" : "testType",
//		"title" : "质检类型",
//		"className":"select",
//		"name":"自主检测"+"|"+"第三方检测",
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "testType");
//			$(td).attr("selectOpt", "自主检测"+"|"+"第三方检测");
//		},
//		"render": function(data, type, full, meta) {
//			if(data == "0") {
//				return "第三方检测";
//			}else if(data == "1") {
//				return "自主检测";
//			}else{
//				return "";
//			}
//		}
//	})
//	colOpts.push({
//		"data" : "sampleDeteyion-id",
//		"title" : "质检项",
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "sampleDeteyion-id");
//		}
//	});
//	colOpts.push({
//		"data" : "sampleDeteyion-name",
//		"title" : "质检项",
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "sampleDeteyion-name");
//		}
//	});
	/*
	 * colOpts.push({ "data": "sampleCode", "title": biolims.common.sampleCode,
	 * "visible": false, "createdCell": function(td) { $(td).attr("saveName",
	 * "sampleCode"); } });
	 */
	colOpts.push({
		"data" : "code",
		"visible" : false,
		"title" : biolims.common.code,
		"createdCell" : function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data" : "productId",
		"title" : biolims.common.productId,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data" : "productName",
		"title" : biolims.common.productName,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data" : "sampleType",
		"title" : biolims.common.sampleType,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleType");
		}
	})
	colOpts.push({
		"data" : "dicSampleType-id",
		"title" : biolims.common.dicSampleTypeId,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "dicSampleType-id");
		}
	})
	colOpts.push({
		"data" : "dicSampleType-name",
		"title" : biolims.common.dicSampleTypeName,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "dicSampleType-name");
		}
	})
	colOpts.push({
		"data" : "concentration",
		"title" : biolims.common.concentration,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "concentration");
		}
	})
	colOpts.push({
		"data" : "volume",
		"title" : biolims.common.volume,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "volume");
		}
	})
	colOpts.push({
		"data" : "sampleInfo-id",
		"title" : biolims.common.volume,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleInfo-id");
		}
	})
	colOpts.push({
		"data" : "result",
		"title" : biolims.common.result,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "result");
		},
	})
	colOpts.push({
		"data" : "reagents-id",
		"title" : biolims.common.reagentName,
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "reagents-id");
		},
	})
	colOpts.push({
		"data" : "orderCode",
		"title" : "订单编号",
		"visible" : false,
		"createdCell" : function(td,data) {
			$(td).attr("saveName", "orderCode");
		},
	})
	/*
	 * colOpts.push({ "data": "reagents-storage-name", "title":
	 * biolims.common.reagentName, "createdCell": function(td, data, rowData) {
	 * $(td).attr("saveName", "reagents-storage-name");
	 * $(td).attr("reagents-id", rowData['reagents-id']); }, })
	 */
	/*
	 * colOpts.push({ "data": "serial", "title":
	 * biolims.tStorageReagentBuySerial.serial, "createdCell": function(td) {
	 * $(td).attr("saveName", "serial"); }, })
	 */
	/*
	 * colOpts.push({ "data": "expireDate", "title":
	 * biolims.common.expirationDate, "className": "date", "createdCell":
	 * function(td) { $(td).attr("saveName", "expireDate"); }, })
	 */
	colOpts.push({
		"data" : "num",
		"title" : biolims.common.count,
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		},
	})
	colOpts.push({
		"data" : "unit",
		"title" : biolims.common.unit,
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "unit");
		},
	})
	colOpts.push({
		"data" : "note",
		"title" : biolims.common.note,
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		}
	})
	colOpts.push({
		"data" : "submit",
		"title" : "是否已提交",
		"name":"是"+"|"+"否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "submit");
			$(td).attr("selectOpt", "是"+"|"+"否");
		},
		"render": function(data, type, full, meta) {
			if(data == "0") {
				return "否";
			}else if(data == "1") {
				return "是";
			}else{
				return "";
			}
		}
	})
	var tbarOpts = [];
	var handlemethod = $("#handlemethod").val();
	if(handlemethod!="view"&&$("#reagentTask_state").text()!="完成"){
	tbarOpts.push({
				text : biolims.common.delSelected,
				action : function() {
					removeChecked($("#reagentTaskResultdiv"),
							"/experiment/reagent/reagentTask/delReagentTaskResult.action",
							"原辅料制备结果删除样本：", $("#reagentTask_id").text());
				}
			});
	tbarOpts.push({
				text : '<i class="glyphicon glyphicon-th"></i>'+ biolims.common.applicationOper,
				action : function() {
					$.ajax({
								type : "post",
								data : {
									id : $("#reagentTask_id").text()
								},
								url : ctx + "/experiment/reagent/reagentNow/saveRoductionResults.action",
								success : function(dataBack) {
									var data = JSON.parse(dataBack)
									if (data.success) {
										reagentTaskResultTab.ajax.reload();
									} else {
										top.layer.msg(biolims.purchase.failed)
									}

								}
							});
				}
			});
//	tbarOpts.push({
//		text : "选择质检项",
//		action : function() {
//			selectsampleDeteyion();
//		}
//	});
//	tbarOpts.push({
//		text : "提交质检",
//		action : function() {
//			submitSJQualityTest();
//		}
//	});
//	tbarOpts.push({
//		text : biolims.common.selReagent,
//		action : function() {
//			selectReagent();
//		}
//	});
	}
	var reagentTaskResultOps = table(true, $("#reagentTask_id").text(),
			"/experiment/reagent/reagentTask/showReagentTaskResultTableJson.action", colOpts, tbarOpts);
	reagentTaskResultTab = renderData($("#reagentTaskResultdiv"), reagentTaskResultOps);
	reagentTaskResultTab.on('draw', function() {
		oldChangeLog = reagentTaskResultTab.ajax.json();
	});
	preAndNext();
	if(handlemethod=="view"||$("#reagentTask_state").text()=="完成"){
		$("#save").hide();
		$("#prev").hide();
		$("#finish").hide();
	}
});
//选择质检项
function selectsampleDeteyion(){
	var rows = $("#reagentTaskResultdiv .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/detecyion/sampleDeteyion/showSampledetecyion.action", ''],
		yes: function(index, layer) {
//			var reali = '';
			var id = "";
			var name = "";
			rows.addClass("editagain");
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleDeteyionTable .selected").each(function(i, v) {
				id = $(v).children("td").eq(1).text();
				name = $(v).children("td").eq(2).text();
//				reali += '<li class="zhijianli"><span><i class="glyphicon glyphicon-th-list"></i></span><small class="label label-primary"><span zhijianid=\'' + $(v).children("td").eq(1).text() + '\' class="zhijianName">' + $(v).children("td").eq(2).text() + '</span></small><i class="fa fa-trash pull-right"></i></li>';
			});
			rows.find("td[savename='sampleDeteyion-id']").text(id);
			rows.find("td[savename='sampleDeteyion-name']").text(name);
//			$("#zhijianBody").append(reali);
//			$("#zhijianBody .nozhijian").slideUp();
			top.layer.close(index);
//			choseType();
//			reagentAndCosRemove();
		},
	})
}
// 选择原辅料
function selectReagent() {
	var rows = $("#reagentTaskResultdiv .selected");
	var length = rows.length;
	if (!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title : biolims.common.selReagent,
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [ window.ctx + "/storage/showStorageDialogList.action", '' ],
		yes : function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addStorage .chosed").children("td")
					.eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addStorage .chosed").children("td")
					.eq(1).text();
			var serial = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#addStorage .chosed").children(
					"td").eq(2).text();
			var expireDate = $('.layui-layer-iframe', parent.document).find(
					"iframe").contents().find("#addStorage .chosed").children(
					"td").eq(3).text();
			var num = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addStorage .chosed").children("td")
					.eq(4).text();
			var unit = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addStorage .chosed").children("td")
					.eq(5).text();
			rows.addClass("editagain");
			rows.find("td[savename='reagents-storage-name']").attr(
					"reagents-id", id);
			rows.find("td[savename='reagents-storage-name']").text(name);
			rows.find("td[savename='serial']").text(serial);
			rows.find("td[savename='expireDate']").text(expireDate);
			rows.find("td[savename='num']").text(num);
			rows.find("td[savename='reagents-unit-name']").text(unit);
			top.layer.close(index);
		},
	})
}
// 保存
function saveItem() {
	var ele = $("#reagentTaskResultdiv");
	var changeLog = "实验结果：";
	var data = saveItemjson(ele);
	if (!data) {
		return false;
	}
	changeLog = getChangeLog(data, ele, changeLog);
	var changeLogs = "";
	if (changeLog != "") {
		changeLogs = changeLog;
	}
	top.layer.load(4, {
		shade : 0.3
	});
	$.ajax({
		type : 'post',
		url : '/experiment/reagent/reagentTask/saveResult.action',
		data : {
			id : $("#reagentTask_id").text(),
			dataJson : data,
			logInfo : changeLogs,
			confirmUser : $("#confirmUser_id").val()
		},
		success : function(data) {
			var data = JSON.parse(data)
			if (data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			}
			;
		}
	})
}
// 获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			if (k == "storage-name") {
				json["storage-id"] = $(tds[j]).attr("storage-id");
				json["orderCode"]=$(tds[j]).attr("orderCode");
			}
			if(k == "testType"){
				var submit = $(tds[j]).text();
				if(submit == "第三方检测") {
					json[k] = "0";
				} else if(submit == "自主检测") {
					json[k] = "1";
				}
				continue;	
			}
			if(k == "submit"){
				var submit = $(tds[j]).text();
				if(submit == "否") {
					json[k] = "0";
				} else if(submit == "是") {
					json[k] = "1";
				}
				continue;	
			}
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
// 上一步操作
$("#prev")
		.click(
				function() {
					$("#maincontentframe", window.parent.document)[0].src = window.ctx
							+ "/experiment/reagent/reagentTask/showReagentTaskSteps.action?id="
							+ $("#reagentTask_id").text()
							+ "&bpmTaskId="
							+ $("#bpmTaskId").val();
				});

// 上一步下一步操作
function preAndNext() {
	
}
	$("#finish").click(function() {
						var paraStr = "formId=" + $("#reagentTask_id").text()
								+ "&tableId=ReagentTask";
						top.layer.open({
									title : biolims.common.approvalTask,
									type : 2,
									anim : 2,
									area : [ '400px', '400px' ],
									btn : biolims.common.selected,
									content : window.ctx
											+ "/applicationTypeAction/applicationTypeActionLook.action?"
											+ paraStr + "&flag=changeState'",
									yes : function(index, layero) {
										top.layer.confirm(
														biolims.common.approve,
														{
															icon : 3,
															title : biolims.common.prompt
														},
														function(index) {
															ajax("post", "/applicationTypeAction/exeFun.action",
																	{
																		applicationTypeActionId : $('.layui-layer-iframe',parent.document).find("iframe").contents().find("input:checked").val(),
																		formId : $("#reagentTask_id").text()
																	},
																	function(response) {
																		var respText = response.message;
																		if (respText == '') {
																			window.location.reload();
																		} else {
																			top.layer.msg(respText);
																		}
																	}, null)
															top.layer.closeAll();
												})
									},
									cancel : function(index, layero) {
										top.layer.closeAll();
									}

								});
					});


function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function dayin(){
	var url = '__report=templateitemSJ.rptdesign&id=' + $("#reagentTask_id").text()+"&ordernum=1";
	commonPrint(url);
}
function commonPrint(pa) {
	var url = '/../report/frameset?__format=html&__svg=false&__locale=zh_CN&__designer=true&__masterpage=true&__rtl=false&__showtitle=false&';
	url = url + pa;
	window.open(url, '_blank', '');
}
function submitSJQualityTest(){
	var rows=$("#reagentTaskResultdiv .editagain");
	if(!rows.length){
	
	}else{
		top.layer.msg("请先保存数据！");
		return false;
	}
	
	
	var rows=$("#reagentTaskResultdiv .selected");
	if(!rows.length){
		top.layer.msg("请选择要提交的数据！");
		return false;
	}
	var ids=[];
	$("#reagentTaskResultdiv .selected").each(function(i,v){
		ids.push($(this).find("input[type='checkbox']").val());
	})
	$.ajax({
		type:"post",
		url:"/experiment/reagent/reagentTask/submitSJQualityTest.action",
		data:{
			id:ids,
		},
		success:function(dataBack){
			var data=JSON.parse(dataBack);
			if(data.success){
				top.layer.msg("提交成功！");
				reagentTaskResultTab.ajax.reload();
			}
		}
	})
}