$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
		}
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "sequencingTask-id",
		"title": biolims.common.relatedMainTableId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "sequencingTask-id");
		},
	})
	colOpts.push({
		"data": "sequencingTask-name",
		"title": biolims.common.relatedMainTableName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "sequencingTask-name");
			$(td).attr("sequencingTask-id", rowData['sequencingTask-id']);
		}
	})
	colOpts.push({
		"data": "productId",
		"title": biolims.common.productId,
		"createdCell": function(td) {
			$(td).attr("saveName", "productId");
		}
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.common.productName,
		"createdCell": function(td) {
			$(td).attr("saveName", "productName");
		}
	})
	colOpts.push({
		"data": "dicSampleTypeId",
		"title": biolims.common.dicSampleTypeId,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "dicSampleTypeId");
		},
	})
	colOpts.push({
		"data": "dicSampleTypeName",
		"title": biolims.common.dicSampleTypeName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "dicSampleTypeName");
			$(td).attr("dicSampleTypeId", rowData['dicSampleTypeId']);
		}
	})
	colOpts.push({
		"data": "productNum",
		"title": biolims.common.productNum,
		"createdCell": function(td, data) {
			$(td).attr("saveName", "productNum");
		},
	})
	colOpts.push({
		"data": "posId",
		"title": biolims.common.location,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "posId");
		},
	})
	colOpts.push({
		"data": "counts",
		"title": biolims.common.counts,
		//"className": "edit",
		"createdCell": function(td, data) {
			$(td).attr("saveName", "counts");
		},
	})
	var tbarOpts = [];
	tbarOpts.push({
		text: biolims.common.find,
		action: function() {
			search();
		}
	});
	tbarOpts.push({
		text: biolims.common.order,
		action: function() {
			viewSequencingTask();
		}
	});
	tbarOpts.push({
		text: biolims.common.batchInLib,
		action: function() {
			ruku();
		}
	});
	var sequencingTaskManageOps = table(true, null, "/experiment/sequencing/sequencingTaskManage/showSequencingTaskManageJson.action", colOpts, tbarOpts);
	sequencingTaskManageTab = renderData($("#sequencingTaskManagediv"), sequencingTaskManageOps);
	//上一步下一步操作
	preAndNext();
});

//上一步下一步操作
function preAndNext() {
	//上一步操作
	$("#prev").click(function() {
		$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/sequencing/sequencingTask/showSequencingTaskSteps.action?id=" + $("#sequencingTask_id").text();
	});
	//上一步操作
	$("#finish").click(function() {
	var paraStr = "formId=" + $("#sequencingTask_id").text() +
		"&tableId=PlasmaTask";
	top.layer.open({
		title: biolims.common.approvalTask,
		type: 2,
		anim: 2,
		area: ['400px', '400px'],
		btn: biolims.common.selected,
		content: window.ctx +
			"/applicationTypeAction/applicationTypeActionLook.action?" + paraStr +
			"&flag=changeState'",
		yes: function(index, layer) {
			top.layer.confirm(biolims.common.approve, {
				icon: 3,
				title: biolims.common.prompt,
				btn:biolims.common.selected
			}, function(index) {
				ajax("post", "/applicationTypeAction/exeFun.action", {
					applicationTypeActionId: '13',
					formId: $("#sequencingTask_id").text()
				}, function(response) {
					var respText = response.message;
					if(respText == '') {
						window.open(window.location,'_self');;
					} else {
						top.layer.msg(respText);
					}
				}, null)
				top.layer.close(index);
			})
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}

	});
});
}
function ruku(){
	var rows = $("#sequencingTaskManagediv .selected");
	if(rows.length>0){
		var ids=[];
		$.each(rows,function(i,j){
			ids.push($(j).find(".icheck").val());
		});
		console.log(ids)
		ajax("post", "/experiment/sequencing/sequencingTaskManage/sequencingTaskManageItemRuku.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				tableRefresh();
				top.layer.msg(biolims.plasma.waitingInstorage)
			} else{
				top.layer.msg(biolims.plasma.warehousingFailed)
			}
		}, null);
	}else{
		top.layer.msg(biolims.common.pleaseSelectData);
	}
}
//查看任务单
function viewSequencingTask(){
	var id = $(".selected").find("td[savename='sequencingTask-id']").text();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/experiment/sequencing/sequencingTask/showSequencingTaskSteps.action?id=" + id;
}

//弹框模糊查询参数
function searchOptions() {
	return [
		{
			"txt": biolims.common.sampleCode,
			"type": "input",
			"searchName": "sampleCode"
		},
		{
			"txt": biolims.common.name,
			"type": "input",
			"searchName": "sequencingTask-name",
		},
		{
			"txt": biolims.common.relatedMainTableId,
			"type": "input",
			"searchName": "sequencingTask-id",
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.sample.createDateEnd,
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		},
		{
			"txt": biolims.common.confirmDateStart,
			"type": "dataTime",
			"searchName": "confirmDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": biolims.common.confirmDateStart,
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "confirmDate##@@##2"
		}
	,{
		"type":"table",
		"table":sequencingTaskManageTab
	}];
}