var bobsSampleTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'bobsSample-id',
		type:"string"
	});
	    fields.push({
		name:'bobsSample-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:biolims.common.name,
		width:50*6
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:biolims.common.testUserId,
		width:20*10
	});
	var reciveUser =new Ext.form.TextField({
        allowBlank: false
	});
	reciveUser.on('focus', function() {
		
		loadTestUser();
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*6,
		editor : reciveUser
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:biolims.common.templateProcessId,
		width:20*6
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:biolims.common.startTime,
		width:20*6
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:biolims.common.endTime,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:biolims.common.relateSample,
		width:40*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsSample-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsSample-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/sample/bobsSample/showBobsSampleTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.executionStep;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/bobs/sample/bobsSample/delBobsSampleTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		iconCls : 'application_print',
		text : biolims.common.printList,
		handler : stampOrder
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : biolims.common.applicationOper,
		handler : addSuccess
	});
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	bobsSampleTemplateGrid=gridEditTable("bobsSampleTemplatediv",cols,loadParam,opts);
	$("#bobsSampleTemplatediv").data("bobsSampleTemplateGrid", bobsSampleTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//选择实验员
function loadTestUser(){
	var win = Ext.getCmp('loadTestUser');
	if (win) {win.close();}
	var loadTestUser= new Ext.Window({
	id:'loadTestUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 loadTestUser.close(); }  }]  });     loadTestUser.show(); 
}
function setreciveUserFun(id,name){
	var gridGrid = bobsSampleTemplateGrid;
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		obj.set('reciveUser-id',id);
		obj.set('reciveUser-name',name);
	});
	var win = Ext.getCmp('loadTestUser');
	if(win){
		win.close();
	}
}
//打印执行单
function stampOrder(){
	var id=$("#bobsSample_template").val();
	if(id==""){
		message("请先选择模板!");
		return;
	}else{
		var url = '__report=bobsSample.rptdesign&id=' + $("#bobsSample_id").val();
		commonPrint(url);}
}

//生成结果明细
function addSuccess(){
	//选中的item数据
	var getRecord = bobsSampleItemGrid.getSelectionModel().getSelections();
	//选中的template数据
	var selectRecord = bobsSampleTemplateGrid.getSelectionModel().getSelections();
	//template明细的所有数据
	var getTemplateAll=bobsSampleTemplateGrid.store;
	//item明细的所有数据
	var getItemAll=bobsSampleItemGrid.store;
	//result的所有数据
	var getResultAll=bobsSampleResultGrid.store;
	var isNull=false;
	if(getItemAll.getCount()>0){
//		for(var h=0;h<getTemplateAll.getCount();h++){
//			var nulls = getTemplateAll.getAt(h).get("endTime");
//			if(nulls==null || nulls == ""){
//				isNull = true;
//				message("有未做实验的步骤！");
//				break;					
//			}
//		}
		if(isNull==false){
			if(selectRecord.length==0){
				//如果没有选中实验步骤，默认所有明细都生成结果
				var isCF=false;
				if(getResultAll.getCount()>0){
					for(var i=0; i<getItemAll.getCount(); i++){
						for(var j=0;j<getResultAll.getCount();j++){
							var itemCode = getItemAll.getAt(i).get("code");
							var infoCode = getResultAll.getAt(j).get("code");
							if(itemCode == infoCode){
								isCF = true;
								message("数据重复，请删除结果，重新生成！");
								break;					
							}
						}
					}
					if(isCF==false){
						toInfoData(getItemAll);
					}
				}else{
					toInfoData(getItemAll);
				}
			}else if(selectRecord.length==1){
				$.each(selectRecord, function(i, obj) {
					var isRepeat = true;
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i1=0; i1<scode.length; i1++){
						for(var j1=0;j1<getResultAll.getCount();j1++){
							var getv = scode[i1];
							var setv = getResultAll.getAt(j1).get("code");
							if(getv == setv){
								isRepeat = false;
								message("有重复的数据，请重新选择！");
								break;					
							}
						}
					}
					if(isRepeat){
						for(var i=0; i<scode.length; i++){
							for(var j=0; j<getItemAll.getCount(); j++){
								if(scode[i]==getItemAll.getAt(j).get("code")){
									var ob = bobsSampleResultGrid.getStore().recordType;
									bobsSampleResultGrid.stopEditing();
									var p = new ob({});
									p.isNew = true;
									p.set("code",getItemAll.getAt(j).get("code"));
									p.set("orderNumber",getItemAll.getAt(j).get("orderNumber"));
									p.set("checkCode",getItemAll.getAt(j).get("checkCode"));
									p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
									p.set("tempId",getItemAll.getAt(j).get("tempId"));
									p.set("productId",getItemAll.getAt(j).get("productId"));
									p.set("productName",getItemAll.getAt(j).get("productName"));
									p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
									p.set("result","1");
									ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
										model : "BobsSample",productId:getItemAll.getAt(j).get("productId")
									}, function(data) {
										p.set("nextFlowId",data.dnextId);
										p.set("nextFlow",data.dnextName);
									}, null);
									message("生成结果成功！");
									bobsSampleResultGrid.getStore().add(p);
									bobsSampleResultGrid.startEditing(0,0);
								}
							}
						}
					}
				});
			}else if(selectRecord.length>1){
				message("请不要勾选多个步骤！");
				return;
			}
		}
	}else{
		message("请先添加实验样本！");
		return;
	}
}
//向结果页面传值
function toInfoData(getItemAll){
	for(var j=0;j<getItemAll.getCount();j++){
		var ob = bobsSampleResultGrid.getStore().recordType;
		bobsSampleResultGrid.stopEditing();
		var p = new ob({});
		p.isNew = true;
		p.set("code",getItemAll.getAt(j).get("code"));
		p.set("orderNumber",getItemAll.getAt(j).get("orderNumber"));
		p.set("checkCode",getItemAll.getAt(j).get("checkCode"));
		p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
		p.set("tempId",getItemAll.getAt(j).get("tempId"));
		p.set("productId",getItemAll.getAt(j).get("productId"));
		p.set("productName",getItemAll.getAt(j).get("productName"));
		p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
		p.set("result","1");
		ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
			model : "BobsSample",productId:getItemAll.getAt(j).get("productId")
		}, function(data) {
			p.set("nextFlowId",data.dnextId);
			p.set("nextFlow",data.dnextName);
		}, null);
		message("生成结果成功！");
		bobsSampleResultGrid.getStore().add(p);
		bobsSampleResultGrid.startEditing(0,0);
	}
}
//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=bobsSampleTemplateGrid.getSelectionModel();
	var setNum = bobsSampleReagentGrid.store;
	var selectRecords=bobsSampleItemGrid.getSelectionModel();
	if(selectRecords.getSelections().length>0){
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
				for(var i=0; i<setNum.getCount();i++){
					var num = setNum.getAt(i).get("itemId");
					if(num==obj.get("code")){
						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
					}
				}
			});
		}else{
			message("请先选择实验步骤！");
		}
		
		//将所选的样本，放到关联样本
		var selRecord=bobsSampleTemplateGrid.getSelectRecord();
		var codes = "";
		$.each(selectRecords.getSelections(), function(i, obj) {
			codes += obj.get("code")+",";
		});
		$.each(selRecord, function(i, obj) {
			obj.set("sampleCodes", codes);
		});
	}else{
		message("请先选择实验样本！");
	}
}
//获取停止时的时间
function getEndTime(){
	var setRecord=bobsSampleItemGrid.store;
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=bobsSampleTemplateGrid.getSelectionModel();
	var getIndex = bobsSampleTemplateGrid.store;
	var getIndexs = bobsSampleTemplateGrid.getSelectionModel().getSelections();
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var st=obj.get("startTime");
			if(st!=null && st!="" && st!=undefined){
				obj.set("endTime",str);	
				//将步骤编号，赋值到明细表
				var codes = obj.get("sampleCodes");
				var scode = new Array();
				scode = codes.split(",");
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(scode[j]==setRecord.getAt(i).get("code")){
							setRecord.getAt(i).set("stepNum",obj.get("code"));
						}
					}
				}
				//将当前行的关联样本传到下一行
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1)
							.set("sampleCodes",codes);
			}else{
				message("请先开始实验！");
			}
		});
	}else{
		message("请选择实验步骤！");
	}
}
