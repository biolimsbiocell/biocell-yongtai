var bobsSampleReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'count',
		type:"string"
	});
	   fields.push({
		name:'oneNum',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'isGood',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'bobsSample-id',
		type:"string"
	});
	    fields.push({
		name:'bobsSample-name',
		type:"string"
	});
	   fields.push({
		name:'itemId',
		type:"string"
	});
	   fields.push({
		name:'tReagent',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:50*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.reagentNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:biolims.common.batch,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'count',
		hidden : true,
		header:biolims.common.count,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'oneNum',
		hidden : false,
		header:biolims.common.singleDose,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.dose,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.ifPassTest,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsSample-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsSample-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.itemId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tReagent',
		hidden : true,
		header:biolims.common.templateReagentId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/sample/bobsSample/showBobsSampleReagentListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/bobs/sample/bobsSample/delBobsSampleReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
			text : biolims.common.selectRelevantTable,
				handler : selectbobsSampleDialogFun
		});
	opts.tbar.push({
		text : biolims.common.batchUploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = bobsSampleReagentGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			o= 10-1;
                			p.set("po.fieldName",this[o]);
                			o= 11-1;
                			p.set("po.fieldName",this[o]);
                			o= 12-1;
                			p.set("po.fieldName",this[o]);
							bobsSampleReagentGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	bobsSampleReagentGrid=gridEditTable("bobsSampleReagentdiv",cols,loadParam,opts);
	$("#bobsSampleReagentdiv").data("bobsSampleReagentGrid", bobsSampleReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectbobsSampleFun(){
	var win = Ext.getCmp('selectbobsSample');
	if (win) {win.close();}
	var selectbobsSample= new Ext.Window({
	id:'selectbobsSample',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectbobsSample.close(); }  }]  }) });  
    selectbobsSample.show(); }
	function setbobsSample(rec){
		var gridGrid = $("#bobsSampleReagentdiv").data("bobsSampleReagentGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('bobsSample-id',rec.get('id'));
			obj.set('bobsSample-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectbobsSample')
		if(win){
			win.close();
		}
	}
	function selectbobsSampleDialogFun(){
			var title = '';
			var url = '';
			title =biolims.common.selectRelevantTable;
			url = ctx + "/BobsSampleSelect.action?flag=bobsSample";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selbobsSampleVal(this);
				}
			}, true, option);
		}
	var selbobsSampleVal = function(win) {
		var operGrid = bobsSampleDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#bobsSampleReagentdiv").data("bobsSampleReagentGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('bobsSample-id',rec.get('id'));
				obj.set('bobsSample-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
