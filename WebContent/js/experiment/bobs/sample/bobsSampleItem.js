var bobsSampleItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'checkCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'xsyVolume',
		type:"string"
	});
	   fields.push({
		name:'bjqConcentration',
		type:"string"
	});
	   fields.push({
		name:'bjqVolume',
		type:"string"
	});
	   fields.push({
		name:'bjqDiluent',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'bobsSample-id',
		type:"string"
	});
	    fields.push({
		name:'bobsSample-name',
		type:"string"
	});
	   fields.push({
		name:'productNum',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
			name:'tempId',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:biolims.common.tempId,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'checkCode',
		hidden : false,
		header:biolims.common.checkedSampleCode,
		width:25*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.expCode,
		width:20*6,
		sortable:true
	});

	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:biolims.common.nucleicConcentration,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:biolims.common.sampleVolume1,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'xsyVolume',
		hidden : false,
		header:biolims.common.xsyVolume,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'bjqConcentration',
		hidden : false,
		header:biolims.common.preLabelledConcentration,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'bjqVolume',
		hidden : false,
		header:biolims.common.preLabelledTJVolumel,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'bjqDiluent',
		hidden : false,
		header:biolims.common.preMediatingDiluent,
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.user.associatedorderName,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'bobsSample-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsSample-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'productNum',
		hidden : true,
		header:biolims.common.productNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/sample/bobsSample/showBobsSampleItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.bobsSampleHandleDetail;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
	
	if($("#bobsSample_state").val()!="1" ||
			$("#bobsSample_state").val()!="2"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/bobs/sample/bobsSample/delBobsSampleItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				bobsSampleItemGrid.getStore().commitChanges();
				bobsSampleItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
		};
		opts.tbar.push({
			text :biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text :biolims.common.uncheck,
			handler : null
		});
/*		opts.tbar.push({
			text : "批量上传CSV",
			handler : function() {
				var options = {};
				options.width = 350;
				options.height = 200;
				loadDialogPage($("#batItem_uploadcsv_div"),"批量上传",null,{
					"确定" : function() {
						goInExcel3();
						$(this).dialog("close");
					}
				}, true, options);
			}
		});*/
		function goInExcel3(){
			 
			var file = document.getElementById("fileItem-uploadcsv").files[0];  
			var n = 0;
			var ob = bobsSampleItemGrid.getStore().recordType;
			var reader = new FileReader();  
			reader.readAsText(file,'GB2312');  
			reader.onload=function(f){  
				var csv_data = $.simple_csv(this.result);
				$(csv_data).each(function() {
	                	if(n>0){
	                		if(this[0]){
	                			var p = new ob({});
	                			p.isNew = true;				
	                			p.set("volume",this[6]);
	                			p.set("xsyVolume",this[8]);
	                			p.set("concentration",this[9]);
	                			bobsSampleItemGrid.getStore().insert(0, p);
	                		}
	                	}
	                    n = n +1;
	                });
	    	};
		}
		opts.tbar.push({
			text : biolims.common.batchCopyImport,
			handler : function() {
				$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：样本号、核酸浓度、样本体积、稀释液体积、标记前浓度、标记前调节体积");
				$("#many_bat_text2").val("");
				$("#many_bat_text2").attr("style", "width:465px;height: 339px");
				var options = {};
				options.width = 494;
				options.height = 508;
				loadDialogPage($("#many_bat_div2"), "批量导入", null, {
					"确定" : function() {
						var positions = $("#many_bat_text2").val();
						if (!positions) {
							message("请填写信息！");
							return;
						}
						var posiObj = {};
						var posiObj1 = {};
						var posiObj2 = {};
						var posiObj3 = {};
						var posiObj4 = {};
						var array = formatData(positions.split("\n"));
						$.each(array, function(i, obj) {
							var tem = obj.split("\t");
							posiObj[tem[0]] = tem[1];
							posiObj1[tem[0]] = tem[2];
							posiObj2[tem[0]] = tem[3];
							posiObj3[tem[0]] = tem[4];
							posiObj4[tem[0]] = tem[5];
						});
						var records = bobsSampleItemGrid.getAllRecord();
						bobsSampleItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							if (posiObj[obj.get("code")]) {
								obj.set("concentration", posiObj[obj.get("code")]);
							}
							//alert(posiObj[obj.get("code")]);
							if (posiObj1[obj.get("code")]) {
								obj.set("volume", posiObj1[obj.get("code")]);
							}
							if (posiObj2[obj.get("code")]) {
								obj.set("xsyVolume", posiObj2[obj.get("code")]);
							}
							if (posiObj3[obj.get("code")]) {
								obj.set("bjqConcentration", posiObj3[obj.get("code")]);
							}
							if (posiObj4[obj.get("code")]) {
								obj.set("bjqVolume", posiObj4[obj.get("code")]);
							}
						});
						bobsSampleItemGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}
		});	
		
		opts.tbar.push({
			text :biolims.common.exportList,
			handler : createCSV
		});
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveInfo
		});
	}
	bobsSampleItemGrid=gridEditTable("bobsSampleItemdiv",cols,loadParam,opts);
	$("#bobsSampleItemdiv").data("bobsSampleItemGrid", bobsSampleItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(bobsSampleItemGrid);
	var id=$("#bobsSample_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/bobs/sample/bobsSample/saveBobsSampleItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					bobsSampleItemGrid.getStore().commitChanges();
					bobsSampleItemGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

/*function exportexcelditem() {
	bobsSampleItemGrid.title = '导出列表';
	var vExportContentitem = bobsSampleItemGrid.getExcelXml();
	var x = document.getElementById('gridhtmitem');
	x.value = vExportContentitem;
	document.excelfrmitem.submit();
}*/

function createCSV(){
	var ids=[];
	var selected = bobsSampleItemGrid.getSelectionModel().getSelections(); 
	for ( var i = 0; i < selected.length; i++) {
		if (!selected[i].isNew) {
			ids.push(selected[i].get("id"));
		}
	}
	ajax("post", "/experiment/bobs/sample/bobsSample/createExcel.action", {
		ids :ids
	},function(data){
		if(data.success){
			message("生成成功！");
		}else{
			message("另一个程序正在使用此文件，进程无法访问！");
		}
	},null);
}


