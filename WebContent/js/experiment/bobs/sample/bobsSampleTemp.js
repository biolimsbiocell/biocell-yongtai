var bobsSampleTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	 //缴费状态
	   fields.push({
		name:'chargeNote',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6
		
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:20*6
	});
	var storechargeNoteCob = new Ext.data.ArrayStore({
		fields:['id','name'],
		data:[ ['1',biolims.common.payPayment],['2',biolims.common.alreadyPaid],['3',biolims.common.settlementSettled],
		       ['4',biolims.sample.kyPro],['5',biolims.common.free]]
	});
	var chargeNoteCob = new Ext.form.ComboBox({
		store:storechargeNoteCob,
		displayField:'name',
		valueField:'id',
		mode:'local'
	});
	cm.push({
		dataIndex:'chargeNote',
		hidden:false,
		header:biolims.common.payStatus,
		width:20*6,
		renderer:Ext.util.Format.comboRenderer(chargeNoteCob)
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.common.order,
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/sample/bobsSample/showBobsSampleTempListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.bobsSampleHandleWait;
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/bobs/sample/bobsSample/delBobsSampleTemp.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : biolims.common.delSelected,
		handler : addItem
	});
	
	opts.tbar.push({
		iconCls : 'application_search',
		text :biolims.common.checkCode,
		handler : function() {
			$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：样本号");
			$("#many_bat_text1").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div1"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text1").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = bobsSampleTempGrid.getAllRecord();
							var store = bobsSampleTempGrid.store;

							var isOper = true;
							var buf = [];
							bobsSampleTempGrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							bobsSampleTempGrid.getSelectionModel().selectRows(buf);
							if(isOper==false){
								message("样本号核对不符，请检查！");
								
							}else{
								addItem();
							}
							bobsSampleTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	bobsSampleTempGrid=gridEditTable("bobsSampleTempdiv",cols,loadParam,opts);
	$("#bobsSampleTempdiv").data("bobsSampleTempGrid", bobsSampleTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//添加任务到子表
function addItem(){
	var selectRecord=bobsSampleTempGrid.getSelectionModel();
	var selRecord=bobsSampleItemGrid.store;
	var count=1;
	var max=0;
	//获取最大排序号
	for(var i=0; i<selRecord.getCount();i++){
		var a=selRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("tempId");
				if(oldv == obj.get("id")){
					isRepeat = true;
					message("有重复的数据，请重新选择！");
					return;					
				}
			}
			if(!isRepeat){
			var ob = bobsSampleItemGrid.getStore().recordType;
			bobsSampleItemGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			
			p.set("tempId",obj.get("id"));
			p.set("code",obj.get("code"));
			p.set("sampleCode",obj.get("sampleCode"));
			p.set("sampleType",obj.get("sampleType"));
			p.set("orderNumber",Number(max)+count);
			p.set("state","1");
			p.set("productId",obj.get("productId"));
			p.set("productName",obj.get("productName"));
			p.set("orderId",obj.get("orderId"));
			p.set("note",obj.get("note"));
		
			bobsSampleItemGrid.getStore().add(p);
			count++;
			bobsSampleItemGrid.startEditing(0, 0);
			
		}
			
	});
	}else{
		message("请选择样本！");
	}
	
}

//左侧页面的查询功能
function selectWaitSamples(){
	commonSearchAction(bobsSampleTempGrid);
}