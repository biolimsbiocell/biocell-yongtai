$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#bobsSample_state").val();
	if(id=="3"){
		load("/experiment/bobs/sample/bobsSample/showBobsSampleTempList.action", null, "#bobsSampleTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#bobsSampleTemppage").remove();
	}
	setTimeout(function() {
		var getGrid=bobsSampleTemplateGrid.store;
		if(getGrid.getCount()==0){
			loadTemplate($("#bobsSample_template").val());
		}
	}, 1000);
});	
function add() {
	window.location = window.ctx + "/experiment/bobs/sample/bobsSample/editBobsSample.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/bobs/sample/bobsSample/showBobsSampleList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {

				submitWorkflow("BobsSample", {
					userId : userId,
					userName : userName,
					formId : $("#bobsSample_id").val(),
					title : $("#bobsSample_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	if (bobsSampleResultGrid.getAllRecord().length > 0) {
		var selRecord = bobsSampleResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			var oldv = selRecord.getAt(j).get("result");
			if(oldv==""){
				message("是否合格未填写！");
				return;
			}
		}
		if(bobsSampleResultGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
		completeTask($("#bobsSample_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
	
		});
}
});

function save() {
if(checkSubmit()==true){
	    var bobsSampleItemDivData = bobsSampleItemGrid;//$("#bobsSampleItemdiv").data("bobsSampleItemGrid");
		document.getElementById('bobsSampleItemJson').value = commonGetModifyRecords(bobsSampleItemDivData);
	    var bobsSampleTemplateDivData = bobsSampleTemplateGrid;//$("#bobsSampleTemplatediv").data("bobsSampleTemplateGrid");
		document.getElementById('bobsSampleTemplateJson').value = commonGetModifyRecords(bobsSampleTemplateDivData);
	    var bobsSampleReagentDivData = bobsSampleReagentGrid;//$("#bobsSampleReagentdiv").data("bobsSampleReagentGrid");
		document.getElementById('bobsSampleReagentJson').value = commonGetModifyRecords(bobsSampleReagentDivData);
	    var bobsSampleCosDivData = bobsSampleCosGrid;//$("#bobsSampleCosdiv").data("bobsSampleCosGrid");
		document.getElementById('bobsSampleCosJson').value = commonGetModifyRecords(bobsSampleCosDivData);
	    var bobsSampleResultDivData = bobsSampleResultGrid;//$("#bobsSampleResultdiv").data("bobsSampleResultGrid");
		document.getElementById('bobsSampleResultJson').value = commonGetModifyRecords(bobsSampleResultDivData);
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/bobs/sample/bobsSample/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/bobs/sample/bobsSample/copyBobsSample.action?id=' + $("#bobsSample_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#bobsSample_id").val() + "&tableId=bobsSample");
//}
//完成
$("#toolbarbutton_status").click(function(){
	var id = $("#bobsSample_id").val();
	ajax("post", "/experiment/bobs/sample/bobsSample/remindSubmit.action", {
		code : id,
		}, function(data) {
//			if (data.state == "20") {
				if ($("#bobsSample_id").val()){
					commonChangeState("formId=" + $("#bobsSample_id").val() + "&tableId=BobsSample");
				}	
//			} else {
//				message("没有提交，无法办理！");
//			}
		}, null);
});
//保存
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#bobsSample_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#bobsSample_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#bobsSample_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.bobsSampleHandle,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/bobs/sample/bobsSample/showBobsSampleItemList.action", {
				id : $("#bobsSample_id").val()
			}, "#bobsSampleItempage");
load("/experiment/bobs/sample/bobsSample/showBobsSampleTemplateList.action", {
				id : $("#bobsSample_id").val()
			}, "#bobsSampleTemplatepage");
load("/experiment/bobs/sample/bobsSample/showBobsSampleReagentList.action", {
				id : $("#bobsSample_id").val()
			}, "#bobsSampleReagentpage");
load("/experiment/bobs/sample/bobsSample/showBobsSampleCosList.action", {
				id : $("#bobsSample_id").val()
			}, "#bobsSampleCospage");
load("/experiment/bobs/sample/bobsSample/showBobsSampleResultList.action", {
				id : $("#bobsSample_id").val()
			}, "#bobsSampleResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
	
	//调用模板
	function TemplateFun(){
			var type="doBobsSample";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: '关闭',
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}
		
		function setTemplateFun(rec){
			document.getElementById('bobsSample_acceptUser').value=rec.get('acceptUser-id');
			document.getElementById('bobsSample_acceptUser_name').value=rec.get('acceptUser-name');
			var code=$("#bobsSample_template").val();
			if(code==""){
				document.getElementById('bobsSample_template').value=rec.get('id');
				document.getElementById('bobsSample_template_name').value=rec.get('name');
				var win = Ext.getCmp('TemplateFun');
				if(win){win.close();}
				var id=rec.get('id');
				ajax("post", "/system/template/template/setTemplateItem.action", {
					code : id,
					}, function(data) {
						if (data.success) {
							var ob = bobsSampleTemplateGrid.getStore().recordType;
							bobsSampleTemplateGrid.stopEditing();
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tItem",obj.id);
								p.set("code",obj.code);
								p.set("stepName",obj.name);
								
								p.set("note",obj.note);
								bobsSampleTemplateGrid.getStore().add(p);							
							});
							
							bobsSampleTemplateGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateReagent.action", {
					code : id,
					}, function(data) {
						if (data.success) {

							var ob = bobsSampleReagentGrid.getStore().recordType;
							bobsSampleReagentGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tReagent",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("batch",obj.batch);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("oneNum",obj.num);
								p.set("note",obj.note);
								p.set("sn",obj.sn);
								bobsSampleReagentGrid.getStore().add(p);							
							});
							
							bobsSampleReagentGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null); 
					ajax("post", "/system/template/template/setTemplateCos.action", {
					code : id,
					}, function(data) {
						if (data.success) {

							var ob = bobsSampleCosGrid.getStore().recordType;
							bobsSampleCosGrid.stopEditing();
							
							$.each(data.data, function(i, obj) {
								var p = new ob({});
								p.isNew = true;
								p.set("tCos",obj.id);
								p.set("code",obj.code);
								p.set("name",obj.name);
								p.set("isGood",obj.isGood);
								p.set("itemId",obj.itemId);
								
								p.set("temperature",obj.temperature);
								p.set("speed",obj.speed);
								p.set("time",obj.time);
								p.set("note",obj.note);
								bobsSampleCosGrid.getStore().add(p);							
							});			
							bobsSampleCosGrid.startEditing(0, 0);		
						} else {
							message("获取明细数据时发生错误！");
						}
					}, null);
			}else{
				if(rec.get('id')==code){
	 				var win = Ext.getCmp('TemplateFun');
	 				if(win){win.close();}
	 			 }else{
					var ob1 = bobsSampleTemplateGrid.store;
	 				if (ob1.getCount() > 0) {
						for(var j=0;j<ob1.getCount();j++){
							var oldv = ob1.getAt(j).get("id"); 
							//根据ID删除
							if(oldv!=null){
								ajax("post", "/experiment/bobs/sample/bobsSample/delOne.action", {
									ids : oldv,model:"1"
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null);
							}else{								
								bobsSampleTemplateGrid.store.removeAll();
							}
						}
						bobsSampleTemplateGrid.store.removeAll();
	 				}

					var ob2 = bobsSampleReagentGrid.store;
					if (ob2.getCount() > 0) {
						for(var j=0;j<ob2.getCount();j++){
							var oldv = ob2.getAt(j).get("id");

							//根据ID删除
							if(oldv!=null){
							ajax("post", "/experiment/bobs/sample/bobsSample/delOne.action", {
								ids : oldv,model:"2"
							}, function(data) {
								if (data.success) {
									message("删除成功！");
								} else {
									message("删除失败！");
								}
							}, null); 
							}else{
								bobsSampleReagentGrid.store.removeAll();
							}
						}
						bobsSampleReagentGrid.store.removeAll();
	 				}
					//=========================================
					var ob3 = bobsSampleCosGrid.store;
					if (ob3.getCount() > 0) {
						for(var j=0;j<ob3.getCount();j++){
							var oldv = ob3.getAt(j).get("id");
							
							//根据ID删除
							if(oldv!=null){
								ajax("post", "/experiment/bobs/sample/bobsSample/delOne.action", {
									ids : oldv,model:"3"
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
							}else{
								bobsSampleCosGrid.store.removeAll();
							}
						}
						bobsSampleCosGrid.store.removeAll();
	 				}
					document.getElementById('bobsSample_template').value=rec.get('id');
					document.getElementById('bobsSample_template_name').value=rec.get('name');
	 				var win = Ext.getCmp('TemplateFun');
	 				if(win){win.close();}
					var id = rec.get('id');
					ajax("post", "/system/template/template/setTemplateItem.action", {
						code : id,
						}, function(data) {
							if (data.success) {	

								var ob = bobsSampleTemplateGrid.getStore().recordType;
								bobsSampleTemplateGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tItem",obj.id);
									p.set("code",obj.code);
									p.set("stepName",obj.name);
									
									p.set("note",obj.note);
									bobsSampleTemplateGrid.getStore().add(p);							
								});
								
								bobsSampleTemplateGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	

								var ob = bobsSampleReagentGrid.getStore().recordType;
								bobsSampleReagentGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									bobsSampleReagentGrid.getStore().add(p);							
								});
								
								bobsSampleReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	

								var ob = bobsSampleCosGrid.getStore().recordType;
								bobsSampleCosGrid.stopEditing();
								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									bobsSampleCosGrid.getStore().add(p);							
								});			
								bobsSampleCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
					}
				}
			}
		
		
//查看上传的图片		
function loadPic(){
	var model="bobsSample";
	window.open(window.ctx+"/experiment/dna/experimentDnaGet/loadPic.action?id="+$("#bobsSample_id").val()+"&model="+model,'','height=700,width=1200,scrollbars=yes,resizable=yes');
}	




Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : $("#bobsSample_id").val(), nextFlowId : "BobsSample"
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							window.location.reload();
						} else {
							message("回滚失败！");
						}
					}, null);
				
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "BobsSample",id : $("#bobsSample_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
			window.location.reload();
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = bobsSampleTemplateGrid.getStore().recordType;
				bobsSampleTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("stepName",obj.name);
					
					p.set("note",obj.note);
					bobsSampleTemplateGrid.getStore().add(p);							
				});
				
				bobsSampleTemplateGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = bobsSampleReagentGrid.getStore().recordType;
				bobsSampleReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("oneNum",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					bobsSampleReagentGrid.getStore().add(p);							
				});
				
				bobsSampleReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = bobsSampleCosGrid.getStore().recordType;
				bobsSampleCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					bobsSampleCosGrid.getStore().add(p);							
				});			
				bobsSampleCosGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
}
