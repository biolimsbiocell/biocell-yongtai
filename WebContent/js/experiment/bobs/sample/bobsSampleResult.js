var bobsSampleResultGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'code',
		type : "string"
	});
	fields.push({
		name : 'orderNumber',
		type : "string"
	});
	fields.push({
		name : 'checkCode',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'sampleType',
		type : "string"
	});
	fields.push({
		name : 'productId',
		type : "string"
	});
	fields.push({
		name : 'productName',
		type : "string"
	});
	fields.push({
		name : 'concentration',
		type : "string"
	});
	fields.push({
		name : 'od280',
		type : "string"
	});
	fields.push({
		name : 'od230',
		type : "string"
	});
	fields.push({
		name : 'volume',
		type : "string"
	});
	fields.push({
		name : 'diluent',
		type : "string"
	});
	fields.push({
		name : 'nextFlowId',
		type : "string"
	});
	fields.push({
		name : 'nextFlow',
		type : "string"
	});
	fields.push({
		name : 'result',
		type : "string"
	});
	fields.push({
		name : 'submit',
		type : "string"
	});
	fields.push({
		name : 'reason',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'bobsSample-id',
		type : "string"
	});
	fields.push({
		name : 'bobsSample-name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'tempId',
		type : "string"
	});

	fields.push({
		name : 'ycReasonId',
		type : "string"
	});
	fields.push({
		name : 'ycReasonName',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header :biolims.user.itemNo,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'tempId',
		hidden : true,
		header :biolims.common.tempId,
		width : 30 * 6
	});
	cm.push({
		dataIndex : 'code',
		hidden : false,
		header :biolims.common.code,
		width : 25 * 6
	});
	cm.push({
		dataIndex : 'orderNumber',
		hidden : false,
		header : biolims.common.expCode,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'checkCode',
		hidden : false,
		header :biolims.common.checkedSampleCode,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'sampleCode',
		hidden : false,
		header : biolims.common.sampleCode,
		width : 20 * 6

	});
	cm.push({
		dataIndex : 'sampleType',
		hidden : false,
		header :  biolims.common.sampleType,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'productId',
		hidden : true,
		header : biolims.common.productId,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'productName',
		hidden : false,
		header : biolims.common.productName,
		width : 20 * 6
	});
	cm.push({
		dataIndex : 'concentration',
		hidden : false,
		header : biolims.common.chAfterConcentration,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'od280',
		hidden : false,
		header : '260/280',
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'od230',
		hidden : false,
		header : '260/230',
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'volume',
		hidden : false,
		header :biolims.common.chAfterConcentrationVolume,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'diluent',
		hidden : false,
		header : biolims.common.chAfterConcentrationDiluent,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'ycReasonId',
		hidden : true,
		header :biolims.common.errorReasonId,
		width : 20 * 6,

	});
	cm.push({
		dataIndex : 'ycReasonName',
		hidden : false,
		header :biolims.common.errorReason,
		width : 30 * 6,

	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified ], [ '0', biolims.common.disqualified ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'result',
		hidden : false,
		header : biolims.common.isQualified+'<font color="red" size="4">*</font>',
		width : 20 * 6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex : 'nextFlowId',
		hidden : true,
		header : biolims.common.nextFlowId,
		width : 15 * 10,
		sortable : true
	});
	var nextFlowCob = new Ext.form.TextField({
		allowBlank : false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex : 'nextFlow',
		header : biolims.common.nextFlow,
		width : 15 * 10,
		sortable : true,
		hidden : false,
		editor : nextFlowCob
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'submit',
		hidden : false,
		header : biolims.common.Submitted+'<font color="red" size="4">*</font>',
		width : 20 * 6,
		// editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex : 'reason',
		hidden : false,
		header : biolims.common.reason,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		hidden : true,
		header : biolims.common.state,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'orderId',
		hidden : true,
		header : biolims.user.associatedorderName,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'bobsSample-id',
		hidden : true,
		header : biolims.common.relatedMainTableId,
		width : 20 * 10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'bobsSample-name',
		hidden : true,
		header :biolims.common.relatedMainTableName,
		width : 20 * 10
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : biolims.common.note,
		width : 40 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/experiment/bobs/sample/bobsSample/showBobsSampleResultListJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title =biolims.common.bobsSampleHandleResult;
	opts.height = document.body.clientHeight - 100;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax(
				"post",
				"/experiment/bobs/sample/bobsSample/delBobsSampleResult.action",
				{
					ids : ids
				}, function(data) {
					if (data.success) {
						bobsSampleResultGrid.getStore().commitChanges();
						bobsSampleResultGrid.getStore().reload();
						message("删除成功！");
					} else {
						message("删除失败！");
					}
				}, null);
	};
	opts.tbar.push({
		text : biolims.common.batchData,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_infos_div"), "批量数据", null, {
				"确定" : function() {
					var records = bobsSampleResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var od230 = $("#od230").val();
						var od280 = $("#od280").val();
						var contraction = $("#contraction").val();
						bobsSampleResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("od230", od230);
							obj.set("od280", od280);
							obj.set("concentration", contraction);
						});
						bobsSampleResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});

	opts.tbar.push({
		text : biolims.common.batchResult,
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = bobsSampleResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						bobsSampleResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						bobsSampleResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});

	opts.tbar.push({
		text : biolims.common.batchUploadCSV,
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#batResult_uploadcsv_div"), "批量上传", null, {
				"确定" : function() {
					goInExcel3();
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	function goInExcel3() {

		var file = document.getElementById("fileResult-uploadcsv").files[0];
		var n = 0;
		var ob = bobsSampleResultGrid.getStore().recordType;
		var ob1 = bobsSampleResultGrid.store;
		var reader = new FileReader();
		reader.readAsText(file, 'GB2312');
		reader.onload = function(f) {
			var csv_data = $.simple_csv(this.result);
			// $(csv_data).each(function() {
			// if(n>0){
			// if(this[0]){
			// var p = new ob({});
			// p.isNew = true;
			// p.set("od230",this[6]);
			// p.set("od280",this[8]);
			// p.set("volume",this[9]);
			// bobsSampleResultGrid.getStore().insert(0, p);
			// }
			// }
			// n = n +1;
			// });
			for ( var k = 0; ob1.getCount(); k++) {
				$(csv_data).each(function() {
					if (n > 0) {
						if (this[0] == ob1.getAt(k).get("sampleCode")) {
							// alert(this[0]);
							// var p = new ob({});
							// p.isNew = true;
							ob1.getAt(k).set("od280", this[7]);
							ob1.getAt(k).set("od230", this[8]);
							ob1.getAt(k).set("concentration", this[9]);
							// experimentDnaGetResultGrid.getStore().insert(0,
							// p);
						}
					}
					n = n + 1;
				});
			}
		};
	}
	// opts.tbar.push({
	// text : "批量粘贴导入",
	// handler : function() {
	//				
	//			
	// $(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：实验编号、纯化后浓度、260/280、260/230、浓度调解后总体积、浓度调解后稀释液");
	// $("#many_bat_text").val("");
	// $("#many_bat_text").attr("style", "width:465px;height: 339px");
	// var options = {};
	// options.width = 550;
	// options.height = 508;
	// loadDialogPage($("#many_bat_div"), "批量导入", null, {
	// "确定" : function() {
	// var positions = $("#many_bat_text").val();
	// if (!positions) {
	// message("请填写信息！");
	// return;
	// }
	// var posiObj = {};
	// var posiObj1 = {};
	// var posiObj2 = {};
	// var posiObj3 = {};
	// var posiObj4 = {};
	// var array = formatData(positions.split("\n"));
	// $.each(array, function(i, obj) {
	// var tem = obj.split("\t");
	// posiObj[tem[0]] = tem[1];
	// posiObj1[tem[0]] = tem[2];
	// posiObj2[tem[0]] = tem[3];
	// posiObj3[tem[0]] = tem[4];
	// posiObj4[tem[0]] = tem[5];
	// });
	// var records = bobsSampleResultGrid.getAllRecord();
	// bobsSampleResultGrid.stopEditing();
	// $.each(records, function(i, obj) {
	// if (posiObj[obj.get("orderNumber")]) {
	// obj.set("concentration", posiObj[obj.get("orderNumber")]);
	// }
	// if (posiObj1[obj.get("orderNumber")]) {
	// obj.set("od280", posiObj1[obj.get("orderNumber")]);
	// }
	// if (posiObj2[obj.get("orderNumber")]) {
	// obj.set("od230", posiObj2[obj.get("orderNumber")]);
	// }
	// if (posiObj3[obj.get("orderNumber")]) {
	// obj.set("volume", posiObj3[obj.get("orderNumber")]);
	// }
	// if (posiObj4[obj.get("orderNumber")]) {
	// obj.set("diluent", posiObj4[obj.get("orderNumber")]);
	// }
	//							
	//						
	//									
	// });
	// bobsSampleResultGrid.startEditing(0, 0);
	// $(this).dialog("close");
	// }
	// }, true, options);
	// }
	// });
	// 批量下一步
	opts.tbar.push({
		text : biolims.common.batchNextStep,
		handler : function() {
			var records = bobsSampleResultGrid.getSelectRecord();
			if (records.length > 0) {
				if (records.length > 2) {
					var productId = new Array();
					$.each(records, function(j, k) {
						productId[j] = k.get("productId");
					});
					for ( var i = 0; i < records.length; i++) {
						if (i != 0 && productId[i] != productId[i - 1]) {
							message("检测项目不同！");
							return;
						}
					}
					loadTestNextFlowCob();
				} else {
					loadTestNextFlowCob();
				}

			} else {
				message("请选择数据!");
			}
		}
	});
	// opts.tbar.push({
	// text : "批量提交",
	// handler : function() {
	// var options = {};
	// options.width = 400;
	// options.height = 300;
	// loadDialogPage($("#bat_submit_div"), "批量提交", null, {
	// "确定" : function() {
	// var records = bobsSampleResultGrid.getSelectRecord();
	// if (records && records.length > 0) {
	// var submit = $("#submit").val();
	// bobsSampleResultGrid.stopEditing();
	// $.each(records, function(i, obj) {
	// obj.set("submit", submit);
	// });
	// bobsSampleResultGrid.startEditing(0, 0);
	// }
	// $(this).dialog("close");
	// }
	// }, true, options);
	// }
	// });
	opts.tbar.push({
		text : biolims.sample.selectUnusual,
		handler : ShowDicType
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.exportList,
		handler : exportexceld
	});
	
	opts.tbar.push({
		text : biolims.common.submitSample,
		handler : submitSample
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	
	bobsSampleResultGrid = gridEditTable("bobsSampleResultdiv", cols,loadParam, opts);
	$("#bobsSampleResultdiv").data("bobsSampleResultGrid", bobsSampleResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(bobsSampleResultGrid);
	var id=$("#bobsSample_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/bobs/sample/bobsSample/saveBobsSampleResult.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					bobsSampleResultGrid.getStore().commitChanges();
					bobsSampleResultGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

//提交样本
function submitSample(){
	var id = $("#bobsSample_id").val();
	if(bobsSampleResultGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	
	var record = bobsSampleResultGrid.getSelectionModel().getSelections();
	var flg=true;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(record[i].get("submit")==null
					|| record[i].get("submit")==""){
				flg=false;
			}
			if(record[i].get("result")==""){
				message("结果不能为空！");
				return;
			}
			/*if(record[i].get("nextStepId")==""){
				message("下一步不能为空！");
				return;
			}*/
		}
		if(!flg){
			var loadMarsk = new Ext.LoadMask(Ext.getBody(),
					{
					        msg : '正在处理，请稍候。。。。。。',
					        removeMask : true// 完成后移除
					    });
			loadMarsk.show();
			var records = [];
			for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
			}
			ajax("post", "/experiment/bobs/sample/bobsSample/submitSample.action", {
				id:id,
				ids : records
			}, function(data) {
				if (data.success) {
					loadMarsk.hide();
					bobsSampleResultGrid.getStore().commitChanges();
					bobsSampleResultGrid.getStore().reload();
					message("提交成功！");
				} else {
					loadMarsk.hide();
					message("提交失败！");
				}
			}, null);
		}else{
			message("没有需要提交的样本！");
		}
	}else{
		message("请选择提交的样本！");
	}
}
////提交样本
//function submitSample() {
//	alter(1234);
//	var id = $("#bobsSample_id").val();
//	alter(id);
//	if (bobsSampleResultGrid.getModifyRecord().length > 0) {
//		message("请先保存记录！");
//		return;
//	}
//	var record = bobsSampleResultGrid.getSelectionModel().getSelections();
//	var flg = false;
//	for ( var i = 0; i < record.length; i++) {
//		if (!record[i].get("submit")) {
//			flg = true;
//		}
//	}
//	var grid = bobsSampleResultGrid.store;
//	for ( var i = 0; i < grid.getCount(); i++) {
//		if (grid.getAt(i).get("submit") == "") {
//			flg = true;
//		}
//	}
//	if (flg) {
//		var loadMarsk = new Ext.LoadMask(Ext.getBody(), {
//			msg : '正在处理，请稍候。。。。。。',
//			removeMask : true
//		// 完成后移除
//		});
//		loadMarsk.show();
//
//		var records = [];
//
//		for ( var i = 0; i < record.length; i++) {
//			records.push(record[i].get("id"));
//		}
//
//		ajax("post", "/experiment/bobs/sample/bobsSample/submitSample.action",
//				{
//					id : id,
//					ids : records
//				}, function(data) {
//					if (data.success) {
//						loadMarsk.hide();
//						bobsSampleResultGrid.getStore().commitChanges();
//						bobsSampleResultGrid.getStore().reload();
//						message("提交成功！");
//					} else {
//						loadMarsk.hide();
//						message("提交失败！");
//					}
//				}, null);
//	} else {
//		message("没有需要提交的样本！");
//	}
//}
function exportexceld() {
	bobsSampleResultGrid.title = '导出列表';
	var vExportContent = bobsSampleResultGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
var loadNextFlow;
// 下一步流向
function loadTestNextFlowCob() {
	var records1 = bobsSampleResultGrid.getSelectRecord();
	var productId = "";
	$.each(records1, function(j, k) {
		productId = k.get("productId");
	});
	var options = {};
	options.width = 500;
	options.height = 500;
	loadNextFlow = loadDialogPage(
			null,
			"选择下一步流向",
			"/system/nextFlow/nextFlow/shownextFlowDialog.action?model=BobsSample&productId="
					+ productId, {
				"确定" : function() {
					var operGrid = $("#show_dialog_nextFlow_div1").data(
							"shownextFlowDialogGrid");
					var selectRecord = operGrid.getSelectionModel()
							.getSelections();
					var records = bobsSampleResultGrid.getSelectRecord();
					if (selectRecord.length > 0) {
						$.each(records, function(i, obj) {
							$.each(selectRecord, function(a, b) {
								obj.set("nextFlowId", b.get("id"));
								obj.set("nextFlow", b.get("name"));
							});
						});
					} else {
						message("请选择您要选择的数据");
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
}
function setNextFlow() {
	var operGrid = $("#show_dialog_nextFlow_div1").data(
			"shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = bobsSampleResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	} else {
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}

// 选择异常类型
function ShowDicType() {
	var selected = "unusual";
	var options = {};
	options.width = 400;
	options.height = 500;
	var url = "/system/sample/sampleOrder/showDicType.action?type=" + selected;
	loadDialogPage(null, "查看送检原因", url, {
		"确定" : function() {
			var selected = showDicTypeGrid.getSelectionModel().getSelections();
			var bobsgrid = bobsSampleResultGrid.getSelectionModel()
					.getSelections();
			var srId = "";
			var srName = "";
			if (selected.length > 0) {
				$.each(selected, function(i, obj) {
					srId += obj.get("id") + ",";
					srName += obj.get("name") + ",";
				});
				if (bobsgrid.length > 0) {
					$.each(bobsgrid, function(a, b) {
						b.set("ycReasonId", srId);
						b.set("ycReasonName", srName);
					});
				}
				options.close();
			} else {
				options.close();
			}
		}
	}, true, options);
}


