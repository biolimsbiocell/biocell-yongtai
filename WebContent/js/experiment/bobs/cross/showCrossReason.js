var crossReasonGrid;
$(function(){
	var cols={};
	cols.sm = true;
	var fields=[];
    fields.push({
	name:'id',
	type:"string"
});
    fields.push({
	name:'name',
	type:"string"
});
    fields.push({
	name:'createUser-id',
	type:"string"
});
    fields.push({
	name:'createUser-name',
	type:"string"
});
    fields.push({
	name:'createDate',
	type:"string"
});
    fields.push({
	name:'washDate',
	type:"string"
});
    fields.push({
	name:'template-id',
	type:"string"
});
    fields.push({
	name:'template-name',
	type:"string"
});
    fields.push({
	name:'acceptUser-id',
	type:"string"
});
    fields.push({
	name:'acceptUser-name',
	type:"string"
});
    fields.push({
	name:'bobsSample-id',
	type:"string"
});
    fields.push({
	name:'bobsSample-name',
	type:"string"
});
    fields.push({
	name:'note',
	type:"string"
});
    fields.push({
	name:'state',
	type:"string"
});
    fields.push({
	name:'stateName',
	type:"string"
});
cols.fields=fields;
var cm=[];
cm.push({
	dataIndex:'id',
	header:'编号',
	width:30*6,
	sortable:true
});
cm.push({
	dataIndex:'name',
	header:'描述',
	width:20*10,
	sortable:true
});
	cm.push({
	dataIndex:'createUser-id',
	hidden:true,
	header:'创建人ID',
	width:20*10,
	sortable:true
	});
	cm.push({
	dataIndex:'createUser-name',
	header:'创建人',
	
	width:20*6,
	sortable:true
	});
cm.push({
	dataIndex:'createDate',
	header:'创建时间',
	width:20*6,
	
	sortable:true
});
cm.push({
	dataIndex:'washDate',
	header:'洗脱时间',
	width:20*6,
	
	sortable:true
});
	cm.push({
	dataIndex:'template-id',
	hidden:true,
	header:'实验模板ID',
	width:20*10,
	sortable:true
	});
	cm.push({
	dataIndex:'template-name',
	header:'实验模板',
	
	width:20*10,
	sortable:true
	});
	cm.push({
	dataIndex:'acceptUser-id',
	hidden:true,
	header:'实验组ID',
	width:20*10,
	sortable:true
	});
	cm.push({
	dataIndex:'acceptUser-name',
	header:'实验组',
	
	width:20*10,
	sortable:true
	});
	cm.push({
	dataIndex:'bobsSample-id',
	hidden:true,
	header:'样本处理编号ID',
	width:20*10,
	sortable:true
	});
	cm.push({
	dataIndex:'bobsSample-name',
	header:'样本处理编号',
	
	width:20*10,
	sortable:true
	});
cm.push({
	dataIndex:'note',
	header:'备注',
	width:30*6,
	
	sortable:true
});
cm.push({
	dataIndex:'state',
	header:'状态',
	width:20*6,
	hidden:true,
	sortable:true
});
cm.push({
	dataIndex:'stateName',
	header:'状态名称',
	width:20*6,
	
	sortable:true
});
	cols.cm=cm;
	var loadParam={};
	//loadParam.url=ctx+"/experiment/bobs/sample/bobsSample/showBobsSampleListJson.action";
	loadParam.url=ctx+"/experiment/bobs/cross/bobsCross/showBobsSampleList1Json.action";
	var opts={};
	opts.title="Bobs样本处理";
	opts.height=document.body.clientHeight-200;
	/*opts.rowselect=function(id){
		$("#selectId").val(id);
	};*/
	/*opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setGoodsMaterialsPackFun(rec);
	};*/
	crossReasonGrid=gridTable("show_CrossReason_div",cols,loadParam,opts);
});
//function sc(){
//		var option = {};
//		option.width = 542;
//		option.height = 417;
//		loadDialogPage($("#jstj"), "搜索", null, {
//			"开始检索" : function() {
//				commonSearchAction(goodsMaterialsPackDialogGridByState);
//				$(this).dialog("close");
//
//			},
//			"清空" : function() {
//				form_reset();
//
//			}
//		}, true, option);
//	}
function selectInfo(){
	commonSearchAction(submitReasonGrid);
	$("#dicType_name").val("");
}