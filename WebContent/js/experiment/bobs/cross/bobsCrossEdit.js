$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	/*var id=$("#bobsCross_state").val();
	if(id=="3"){
		load("/experiment/bobs/cross/bobsCross/showBobsCrossTempList.action", null, "#bobsCrossTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#showtemplate").css("display","none");
		$("#showacceptUser").css("display","none");
		$("#doclinks_img").css("display","none");
		$("#bobsCrossTemppage").remove();
	}*/
	setTimeout(function() {
		var getGrid=bobsCrossTemplateGrid.store;
		if(getGrid.getCount()==0){
			loadTemplate($("#bobsCross_template").val());
		}
	}, 1000);
});	
function add() {
	window.location = window.ctx + "/experiment/bobs/cross/bobsCross/editBobsCross.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/bobs/cross/bobsCross/showBobsCrossList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("BobsCross", {
					userId : userId,
					userName : userName,
					formId : $("#bobsCross_id").val(),
					title : $("#bobsCross_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	//var selRecord = bobsCrossResultGrid.store;
	var selRecord = bobsCrossItemGrid.store;
	if (selRecord.getCount() > 0) {
		for(var j=0;j<selRecord.getCount();j++){
			var oldv = selRecord.getAt(j).get("result");
			if(oldv==""){
				message("是否合格未填写！");
				return;
			}
		}
		//if(bobsCrossResultGrid.getModifyRecord().length > 0){
		if(bobsCrossItemGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
		completeTask($("#bobsCross_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});






function save() {
if(checkSubmit()==true){
	    var bobsCrossItemDivData = bobsCrossItemGrid;
		document.getElementById('bobsCrossItemJson').value = commonGetModifyRecords(bobsCrossItemDivData);
	    var bobsCrossTemplateDivData =bobsCrossTemplateGrid;
		document.getElementById('bobsCrossTemplateJson').value = commonGetModifyRecords(bobsCrossTemplateDivData);
	    var bobsCrossReagentDivData = bobsCrossReagentGrid;
		document.getElementById('bobsCrossReagentJson').value = commonGetModifyRecords(bobsCrossReagentDivData);
	    var bobsCrossCosDivData = bobsCrossCosGrid;
		document.getElementById('bobsCrossCosJson').value = commonGetModifyRecords(bobsCrossCosDivData);
	    var bobsCrossResultDivData = bobsCrossResultGrid;
		document.getElementById('bobsCrossResultJson').value = commonGetModifyRecords(bobsCrossResultDivData);
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/bobs/cross/bobsCross/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/bobs/cross/bobsCross/copyBobsCross.action?id=' + $("#bobsCross_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#bobsCross_id").val() + "&tableId=bobsCross");
//}
$("#toolbarbutton_status").click(function(){
	
	var id = $("#bobsCross_id").val();
	ajax("post", "/experiment/bobs/cross/bobsCross/remindSubmit.action", {
		code : id,
		}, function(data) {
//			if (data.state == "20") {
				if ($("#bobsCross_id").val()){
					commonChangeState("formId=" + $("#bobsCross_id").val() + "&tableId=BobsCross");
				}	
//			} else {
//				message("没有提交，无法办理！");
//			}
		}, null);
	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#bobsCross_id").val());
	nsc.push("编号不能为空！"); 
	fs.push($("#bobsCross_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#bobsCross_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.bobsHybridization,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/bobs/cross/bobsCross/showBobsCrossItemList.action", {
				id : $("#bobsCross_id").val()
			}, "#bobsCrossItempage");
load("/experiment/bobs/cross/bobsCross/showBobsCrossTemplateList.action", {
				id : $("#bobsCross_id").val()
			}, "#bobsCrossTemplatepage");
load("/experiment/bobs/cross/bobsCross/showBobsCrossReagentList.action", {
				id : $("#bobsCross_id").val()
			}, "#bobsCrossReagentpage");
load("/experiment/bobs/cross/bobsCross/showBobsCrossCosList.action", {
				id : $("#bobsCross_id").val()
			}, "#bobsCrossCospage");
load("/experiment/bobs/cross/bobsCross/showBobsCrossResultList.action", {
				id : $("#bobsCross_id").val()
			}, "#bobsCrossResultpage");
//load("/experiment/bobs/cross/bobsCross/showBobsCrossTempList.action", {
//				id : $("#bobsCross_id").val()
//			}, "#bobsCrossTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
	//调用模板
	function TemplateFun(){
			var type="doBobsCross";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: '关闭',
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}
		
function setTemplateFun(rec){
	document.getElementById('bobsCross_acceptUser').value=rec.get('acceptUser-id');
	document.getElementById('bobsCross_acceptUser_name').value=rec.get('acceptUser-name');
	var code=$("#bobsCross_template").val();
	if(code==""){
		document.getElementById('bobsCross_template').value=rec.get('id');
		document.getElementById('bobsCross_template_name').value=rec.get('name');
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
		var id=rec.get('id');
		ajax("post", "/system/template/template/setTemplateItem.action", {
			code : id,
			}, function(data) {
				if (data.success) {
					var ob = bobsCrossTemplateGrid.getStore().recordType;
					bobsCrossTemplateGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tItem",obj.id);
						p.set("code",obj.code);
						p.set("stepName",obj.name);
						
						p.set("note",obj.note);
						bobsCrossTemplateGrid.getStore().add(p);							
					});
					
					bobsCrossTemplateGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateReagent.action", {
			code : id,
			}, function(data) {
				if (data.success) {

					var ob = bobsCrossReagentGrid.getStore().recordType;
					bobsCrossReagentGrid.stopEditing();
					
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tReagent",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("batch",obj.batch);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						
						p.set("oneNum",obj.num);
						p.set("note",obj.note);
						p.set("sn",obj.sn);
						bobsCrossReagentGrid.getStore().add(p);							
					});
					
					bobsCrossReagentGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateCos.action", {
			code : id,
			}, function(data) {
				if (data.success) {

					var ob = bobsCrossCosGrid.getStore().recordType;
					bobsCrossCosGrid.stopEditing();
					
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tCos",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("time",obj.time);
						p.set("note",obj.note);
						bobsCrossCosGrid.getStore().add(p);							
					});			
					bobsCrossCosGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null);
		}else{
			if(rec.get('id')==code){
 				var win = Ext.getCmp('TemplateFun');
 				if(win){win.close();}
 			 }else{
		var ob1 = bobsCrossTemplateGrid.store;
		if (ob1.getCount() > 0) {
			for(var j=0;j<ob1.getCount();j++){
				var oldv = ob1.getAt(j).get("id"); 
				//根据ID删除
				if(oldv!=null){
					ajax("post", "/experiment/bobs/cross/bobsCross/delOne.action", {
						ids : oldv,model:"1"
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null);
				}else{								
					bobsCrossTemplateGrid.store.removeAll();
				}
			}
			bobsCrossTemplateGrid.store.removeAll();
		}

		var ob2 = bobsCrossReagentGrid.store;
		if (ob2.getCount() > 0) {
			for(var j=0;j<ob2.getCount();j++){
				var oldv = ob2.getAt(j).get("id");

				//根据ID删除
				if(oldv!=null){
				ajax("post", "/experiment/bobs/cross/bobsCross/delOne.action", {
					ids : oldv,model:"2"
				}, function(data) {
					if (data.success) {
						message("删除成功！");
					} else {
						message("删除失败！");
					}
				}, null); 
				}else{
					bobsCrossReagentGrid.store.removeAll();
				}
			}
			bobsCrossReagentGrid.store.removeAll();
		}
		//=========================================
		var ob3 = bobsCrossCosGrid.store;
		if (ob3.getCount() > 0) {
			for(var j=0;j<ob3.getCount();j++){
				var oldv = ob3.getAt(j).get("id");
				
				//根据ID删除
				if(oldv!=null){
					ajax("post", "/experiment/bobs/cross/bobsCross/delOne.action", {
						ids : oldv,model:"3"
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null); 
				}else{
					bobsCrossCosGrid.store.removeAll();
				}
			}
			bobsCrossCosGrid.store.removeAll();
		}
		document.getElementById('bobsCross_template').value=rec.get('id');
		document.getElementById('bobsCross_template_name').value=rec.get('name');
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
		var id = rec.get('id');
		ajax("post", "/system/template/template/setTemplateItem.action", {
			code : id,
			}, function(data) {
				if (data.success) {	

					var ob = bobsCrossTemplateGrid.getStore().recordType;
					bobsCrossTemplateGrid.stopEditing();
					
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tItem",obj.id);
						p.set("code",obj.code);
						p.set("stepName",obj.name);
						
						p.set("note",obj.note);
						bobsCrossTemplateGrid.getStore().add(p);							
					});
					
					bobsCrossTemplateGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateReagent.action", {
			code : id,
			}, function(data) {
				if (data.success) {	

					var ob = bobsCrossReagentGrid.getStore().recordType;
					bobsCrossReagentGrid.stopEditing();
					
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tReagent",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("batch",obj.batch);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						
						p.set("oneNum",obj.num);
						p.set("note",obj.note);
						bobsCrossReagentGrid.getStore().add(p);							
					});
					
					bobsCrossReagentGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateCos.action", {
			code : id,
			}, function(data) {
				if (data.success) {	

					var ob = bobsCrossCosGrid.getStore().recordType;
					bobsCrossCosGrid.stopEditing();
					
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tCos",obj.id);
						p.set("code",obj.code);
						p.set("name",obj.name);
						p.set("isGood",obj.isGood);
						p.set("itemId",obj.itemId);
						
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("time",obj.time);
						p.set("note",obj.note);
						bobsCrossCosGrid.getStore().add(p);							
					});			
					bobsCrossCosGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
		}
	}
}
//12345	
		
//选择样本处理
/*function BobsSampleFun(){
	var win = Ext.getCmp('BobsSampleFun');
	if (win) {win.close();}
	var BobsSampleFun= new Ext.Window({
	id:'BobsSampleFun',modal:true,title:'选择样本处理',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/experiment/bobs/sample/bobsSample/bobsSampleSelect.action?flag=BobsSampleFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 BobsSampleFun.close(); }  }]  });     BobsSampleFun.show(); }
function setBobsSampleFun(rec){
	var id=$("#bobsCross_bobsSample").val();
	if(id==""){
		document.getElementById('bobsCross_bobsSample').value=rec.get('id');
		document.getElementById('bobsCross_bobsSample_name').value=rec.get('name');
		var win = Ext.getCmp('BobsSampleFun');
		if(win){
			win.close();
		}
		var code=rec.get('id');
		ajax("post","/experiment/bobs/sample/bobsSample/setResultToCross.action",
			{code:code},function(data){
			if(data.success){
				var ob=bobsCrossItemGrid.getStore().recordType;
				bobsCrossItemGrid.stopEditing();
				$.each(data.data,function(i,obj){
					if(obj.result=="1"){
						var p = new ob({});
						p.isNew = true;
						p.set("code",obj.code);
						p.set("sampleCode",obj.sampleCode);
						p.set("checkCode",obj.checkCode);
						p.set("orderNumber",obj.orderNumber);
						p.set("sampleType",obj.sampleType);
						p.set("productId",obj.productId);
						p.set("productName",obj.productName);
						p.set("note",obj.note);
						bobsCrossItemGrid.getStore().add(p);
						bobsCrossItemGrid.startEditing(0, 0);
					}
				});
			}else {
				message("获取明细数据时发生错误！");
			}
		});
	}else{
		if(rec.get('id')==id){
			var win = Ext.getCmp('BobsSampleFun');
			if(win){win.close();}
		}else{
			var ob1 = bobsCrossItemGrid.store;
			if (ob1.getCount() > 0) {
				for(var j=0;j<ob1.getCount();j++){
					var oldv = ob1.getAt(j).get("id");
					//根据ID删除
					if(oldv!=null){
						ajax("post", "/experiment/bobs/cross/bobsCross/delBobsCrossItemOne.action", {
							ids : oldv
						}, function(data) {
							if (data.success) {
								message("删除成功！");
							} else {
								message("删除失败！");
							}
						}, null);
					}else{								
						bobsCrossItemGrid.store.removeAll();
					}
				}
				bobsCrossItemGrid.store.removeAll();
			}
			document.getElementById('bobsCross_bobsSample').value=rec.get('id');
			document.getElementById('bobsCross_bobsSample_name').value=rec.get('name');
			var win = Ext.getCmp('BobsSampleFun');
			if(win){
				win.close();
			}
			var code=rec.get('id');
			ajax("post","/experiment/bobs/sample/bobsSample/setResultToCross.action",
				{code:code},function(data){
				if(data.success){
					var ob=bobsCrossItemGrid.getStore().recordType;
					bobsCrossItemGrid.stopEditing();
					$.each(data.data,function(i,obj){
						if(obj.result=="1"){
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.code);
							p.set("sampleCode",obj.sampleCode);
							p.set("checkCode",obj.checkCode);
							p.set("orderNumber",obj.orderNumber);
							p.set("sampleType",obj.sampleType);
							p.set("productId",obj.productId);
							p.set("productName",obj.productName);
							p.set("note",obj.note);
							bobsCrossItemGrid.getStore().add(p);
							bobsCrossItemGrid.startEditing(0, 0);
						}
					});
				}else {
					message("获取明细数据时发生错误！");
				}
			});
		}
	}
}*/

//2134567
//选择样本处理1
function BobsSampleFun(){
	var code=$("#bobsCross_bobsSampleId").val();
	var options = {};
	options.width = 600;
	options.height = 600;
	//var url="/experiment/bobs/sample/bobsSample/bobsSampleSelect1.action";
	var url="/experiment/bobs/cross/bobsCross/bobsSampleList1.action";
	loadDialogPage(null, "选择样本处理", url, {
		 "确定": function() {
			 var selected=crossReasonGrid.getSelectionModel().getSelections();
			 var srId="";
			 var srName="";
			 var idStr="";
			 var idStr1="";
			 if(selected.length>0){
				 $.each(selected, function(i, obj) {
					 srId += obj.get("id")+",";
					 srName += obj.get("name")+",";
					 idStr += "'"+obj.get("id")+"',";
					
				 });
				 idStr1=idStr.substring(0,idStr.length-1); 
				 if(code==""||code==null){
					 $("#bobsCross_bobsSampleId").val(srId);
					 $("#bobsCross_bobsSampleName").val(srName);
					
					 ajax("post","/experiment/bobs/sample/bobsSample/setResultToCross.action",
								{code:idStr1},function(data){
								if(data.success){
									var ob=bobsCrossItemGrid.getStore().recordType;
									bobsCrossItemGrid.stopEditing();
									$.each(data.data,function(i,obj){
										if(obj.result=="1"){
											var p = new ob({});
											p.isNew = true;
											p.set("code",obj.code);
											p.set("sampleCode",obj.sampleCode);
											p.set("checkCode",obj.checkCode);
											p.set("orderNumber",obj.orderNumber);
											p.set("sampleType",obj.sampleType);
											p.set("productId",obj.productId);
											p.set("productName",obj.productName);
											p.set("note",obj.note);
											bobsCrossItemGrid.getStore().add(p);
											bobsCrossItemGrid.startEditing(0, 0);
										}
									});
								}else {
									message("获取明细数据时发生错误！");
								}
							},null);
					 options.close();
				 }else{
					var ob1 = bobsCrossItemGrid.store;
					if (ob1.getCount() > 0) {
						for(var j=0;j<ob1.getCount();j++){
							var oldv = ob1.getAt(j).get("id");
							//根据ID删除
							if(oldv!=null){
								ajax("post", "/experiment/bobs/cross/bobsCross/delBobsCrossItemOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null);
							}else{								
								bobsCrossItemGrid.store.removeAll();
							}
						}
						bobsCrossItemGrid.store.removeAll();
					}
					 $("#bobsCross_bobsSampleId").val(srId);
					 $("#bobsCross_bobsSampleName").val(srName);
					
					 ajax("post","/experiment/bobs/sample/bobsSample/setResultToCross.action",
								{code:idStr1},function(data){
								if(data.success){
									var ob=bobsCrossItemGrid.getStore().recordType;
									bobsCrossItemGrid.stopEditing();
									$.each(data.data,function(i,obj){
										if(obj.result=="1"){
											var p = new ob({});
											p.isNew = true;
											p.set("code",obj.code);
											p.set("sampleCode",obj.sampleCode);
											p.set("checkCode",obj.checkCode);
											p.set("orderNumber",obj.orderNumber);
											p.set("sampleType",obj.sampleType);
											p.set("productId",obj.productId);
											p.set("productName",obj.productName);
											p.set("note",obj.note);
											bobsCrossItemGrid.getStore().add(p);
											bobsCrossItemGrid.startEditing(0, 0);
										}
									});
								}else {
									message("获取明细数据时发生错误！");
								}
							},null);
					 options.close();
				 }
			 }else{
				 options.close();
			 }
		}
	}, true, options);
}

Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : $("#bobsCross_id").val(), nextFlowId : "BobsCross"
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							window.location.reload();
						} else {
							message("回滚失败！");
						}
					}, null);
				
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "BobsCross",id : $("#bobsCross_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
			window.location.reload();	
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = bobsCrossTemplateGrid.getStore().recordType;
				bobsCrossTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true; 
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("stepName",obj.name);
					p.set("note",obj.note);
					bobsCrossTemplateGrid.getStore().add(p);							
				});
				bobsCrossTemplateGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = bobsCrossReagentGrid.getStore().recordType;
				bobsCrossReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("oneNum",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					bobsCrossReagentGrid.getStore().add(p);							
				});
				
				bobsCrossReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = bobsCrossCosGrid.getStore().recordType;
				bobsCrossCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					bobsCrossCosGrid.getStore().add(p);							
				});			
				bobsCrossCosGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
}
