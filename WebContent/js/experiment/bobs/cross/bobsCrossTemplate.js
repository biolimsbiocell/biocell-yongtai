var bobsCrossTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'stepName',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-id',
		type:"string"
	});
	    fields.push({
		name:'reciveUser-name',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	   fields.push({
		name:'startTime',
		type:"string"
	});
	   fields.push({
		name:'endTime',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	    fields.push({
		name:'bobsCross-id',
		type:"string"
	});
	    fields.push({
		name:'bobsCross-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'stepName',
		hidden : false,
		header:'步骤名称',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6
		
	});
	cm.push({
		dataIndex:'reciveUser-id',
		hidden : true,
		header:'实验员ID',
		width:20*10
	});
	var reciveUser =new Ext.form.TextField({
        allowBlank: false
	});
	reciveUser.on('focus', function() {
		
		loadTestUser();
	});
	cm.push({
		dataIndex:'reciveUser-name',
		hidden : false,
		header:'实验员',
		width:20*6,
		editor : reciveUser
	});
	cm.push({
		dataIndex:'tItem',
		hidden : true,
		header:'模板步骤编id',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'startTime',
		hidden : false,
		header:'开始时间',
		width:20*6
	});
	cm.push({
		dataIndex:'endTime',
		hidden : false,
		header:'结束时间',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCodes',
		hidden : false,
		header:'关联样本',
		width:50*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'bobsCross-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsCross-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/cross/bobsCross/showBobsCrossTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="执行步骤";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/bobs/cross/bobsCross/delBobsCrossTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : '打印执行单',
		handler : stampOrder
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text : '开始执行',
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : '执行结束',
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text : '生成杂交结果',
		handler : addSuccess
	});
	bobsCrossTemplateGrid=gridEditTable("bobsCrossTemplatediv",cols,loadParam,opts);
	$("#bobsCrossTemplatediv").data("bobsCrossTemplateGrid", bobsCrossTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
//选择实验员
function loadTestUser(){
	var win = Ext.getCmp('loadTestUser');
	if (win) {win.close();}
	var loadTestUser= new Ext.Window({
	id:'loadTestUser',modal:true,title:'选择实验员',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/core/user/userSelect.action?flag=reciveUserFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 loadTestUser.close(); }  }]  });     loadTestUser.show(); 
}
function setreciveUserFun(id,name){
	var gridGrid = bobsCrossTemplateGrid;
	var selRecords = gridGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		obj.set('reciveUser-id',id);
		obj.set('reciveUser-name',name);
	});
	var win = Ext.getCmp('loadTestUser');
	if(win){
		win.close();
	}
}
//打印执行单
function stampOrder(){
	var id=$("#bobsCross_template").val();
	if(id==""){
		message("请先选择模板!");
		return;
	}else{
		var url = '__report=bobsCross.rptdesign&id=' + $("#bobsCross_id").val();
		commonPrint(url);}
}
//生成结果明细
function addSuccess(){
	//选中的item数据
	var getRecord = bobsCrossItemGrid.getSelectionModel().getSelections();
	//选中的template数据
	var selectRecord = bobsCrossTemplateGrid.getSelectionModel().getSelections();
	//template明细的所有数据
	var getTemplateAll=bobsCrossTemplateGrid.store;
	//item明细的所有数据
	var getItemAll=bobsCrossItemGrid.store;
	//result的所有数据
	var getResultAll=bobsCrossResultGrid.store;
	var isNull=false;
	if(getItemAll.getCount()>0){
//		for(var h=0;h<getTemplateAll.getCount();h++){
//			var nulls = getTemplateAll.getAt(h).get("endTime");
//			if(nulls==null || nulls == ""){
//				isNull = true;
//				message("有未做实验的步骤！");
//				break;					
//			}
//		}
		if(isNull==false){
			if(selectRecord.length==0){
				//如果没有选中实验步骤，默认所有明细都生成结果
				var isCF=false;
				if(getResultAll.getCount()>0){
					for(var i=0; i<getItemAll.getCount(); i++){
						for(var j=0;j<getResultAll.getCount();j++){
							var itemCode = getItemAll.getAt(i).get("code");
							var infoCode = getResultAll.getAt(j).get("code");
							if(itemCode == infoCode){
								isCF = true;
								message("数据重复，请删除结果，重新生成！");
								break;					
							}
						}
					}
					if(isCF==false){
						toInfoData(getItemAll);
					}
				}else{
					toInfoData(getItemAll);
				}
			}else if(selectRecord.length==1){
				$.each(selectRecord, function(i, obj) {
					var isRepeat = true;
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i1=0; i1<scode.length; i1++){
						for(var j1=0;j1<getResultAll.getCount();j1++){
							var getv = scode[i1];
							var setv = getResultAll.getAt(j1).get("code");
							if(getv == setv){
								isRepeat = false;
								message("有重复的数据，请重新选择！");
								break;					
							}
						}
					}
					if(isRepeat){
						for(var i=0; i<scode.length; i++){
							for(var j=0; j<getItemAll.getCount(); j++){
								if(scode[i]==getItemAll.getAt(j).get("code")){
									var ob = bobsCrossResultGrid.getStore().recordType;
									bobsCrossResultGrid.stopEditing();
									var p = new ob({});
									p.isNew = true;
									p.set("code",getItemAll.getAt(j).get("code"));
									p.set("orderNumber",getItemAll.getAt(j).get("orderNumber"));
									//p.set("checkCode",getItemAll.getAt(j).get("checkCode"));
									p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
									//p.set("tempId",getItemAll.getAt(j).get("tempId"));
									p.set("productId",getItemAll.getAt(j).get("productId"));
									p.set("productName",getItemAll.getAt(j).get("productName"));
									p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
									p.set("result","1");
									ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
										model : "BobsCross",productId:getItemAll.getAt(j).get("productId")
									}, function(data) {
										p.set("nextFlowId",data.dnextId);
										p.set("nextFlow",data.dnextName);
									}, null);
									message("生成结果成功！");
									bobsCrossResultGrid.getStore().add(p);
									bobsCrossResultGrid.startEditing(0,0);
								}
							}
						}
					}
				});
			}else if(selectRecord.length>1){
				message("请不要勾选多个步骤！");
				return;
			}
		}
	}else{
		message("请先添加实验样本！");
		return;
	}
}
//向结果页面传值
function toInfoData(getItemAll){
	for(var j=0;j<getItemAll.getCount();j++){
		var ob = bobsCrossResultGrid.getStore().recordType;
		bobsCrossResultGrid.stopEditing();
		var p = new ob({});
		p.isNew = true;
		p.set("code",getItemAll.getAt(j).get("code"));
		p.set("orderNumber",getItemAll.getAt(j).get("orderNumber"));
		//p.set("checkCode",getItemAll.getAt(j).get("checkCode"));
		p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
		//p.set("tempId",getItemAll.getAt(j).get("tempId"));
		p.set("productId",getItemAll.getAt(j).get("productId"));
		p.set("productName",getItemAll.getAt(j).get("productName"));
		p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
		p.set("result","1");
		ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
			model : "BobsCross",productId:getItemAll.getAt(j).get("productId")
		}, function(data) {
			p.set("nextFlowId",data.dnextId);
			p.set("nextFlow",data.dnextName);
		}, null);	
		message("生成结果成功！");
		bobsCrossResultGrid.getStore().add(p);
		bobsCrossResultGrid.startEditing(0,0);
	}
}
//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=bobsCrossTemplateGrid.getSelectionModel();
	var setNum = bobsCrossReagentGrid.store;
	var selectRecords=bobsCrossItemGrid.getSelectionModel();
	if (selectRecords.getSelections().length > 0) {
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				obj.set("startTime",str);
				//将所选样本的数量，放到原辅料样本数量处
				for(var i=0; i<setNum.getCount();i++){
					var num = setNum.getAt(i).get("itemId");
					if(num==obj.get("code")){
						setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
					}
				}
			});
		}else{
			message("请先选择数据！");
		}
		
		//将所选的样本，放到关联样本
		var selRecord=bobsCrossTemplateGrid.getSelectRecord();
		var codes = "";
		$.each(selectRecords.getSelections(), function(i, obj) {
			codes += obj.get("code")+",";
		});
		$.each(selRecord, function(i, obj) {
			obj.set("sampleCodes", codes);
		});
	}else{
		message("请先选择实验样本 ！");
	}
}
//获取停止时的时间
function getEndTime(){
		var setRecord=bobsCrossItemGrid.store;
		var d = new Date();
		var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
		var selectRecord=bobsCrossTemplateGrid.getSelectionModel();
		var getIndex = bobsCrossTemplateGrid.store;
		var getIndexs = bobsCrossTemplateGrid.getSelectionModel().getSelections();
		if (selectRecord.getSelections().length > 0) {
			$.each(selectRecord.getSelections(), function(i, obj) {
				var st=obj.get("startTime");
				if(st!=null && st!=undefined && st!=""){
					obj.set("endTime",str);	
					//将步骤编号，赋值到明细表
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i=0; i<setRecord.getCount(); i++){
						for(var j=0; j<scode.length; j++){
							if(scode[j]==setRecord.getAt(i).get("code")){
								setRecord.getAt(i).set("stepNum",obj.get("code"));
							}
						}
					}
					//将当前行的关联样本传到下一行
					getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1)
					.set("sampleCodes",codes);
				}else{
					message("请先开始实验！");
				}
			});
		}else{
			message("请选择实验步骤！");
		}
}