var bobsCrossResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'bobsCross-id',
		type:"string"
	});
	    fields.push({
		name:'bobsCross-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:'实验编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:25*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:25*6
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'是否合格<font color="red" size=4"">*</font>',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		sortable:true,
		hidden:true,
		editor : nextFlowCob
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : true,
		header:'是否已提交<font color="red" size=4"">*</font>',
		width:20*6,
//		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsCross-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsCross-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/cross/bobsCross/showBobsCrossResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="BoBs杂交结果";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/bobs/cross/bobsCross/delBobsCrossResult.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				bobsCrossResultGrid.getStore().commitChanges();
				bobsCrossResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : "批量结果",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
					var records = bobsCrossResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var result = $("#result").val();
						bobsCrossResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						bobsCrossResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
//	opts.tbar.push({
//		text : "批量下一步",
//		handler : function() {
//			var records = bobsCrossResultGrid.getSelectRecord();
//			if(records.length>0){
//				if(records.length>2){
//					var productId = new Array();
//					$.each(records, function(j, k) {
//						productId[j]=k.get("productId");
//					});
//					for(var i=0;i<records.length;i++){
//						if(i!=0&&productId[i]!=productId[i-1]){
//							message("检测项目不同！");
//							return;
//						}
//					}
//					loadTestNextFlowCob();
//				}else{
//					loadTestNextFlowCob();
//				}
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});
//	opts.tbar.push({
//		text : "批量提交",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_submit_div"), "批量提交", null, {
//				"确定" : function() {
//					var records = bobsCrossResultGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var submit = $("#submit").val();
//						bobsCrossResultGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("submit", submit);
//						});
//						bobsCrossResultGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
//	opts.tbar.push({
//		text : '提交样本',
//		handler : submitSample
//	});
	bobsCrossResultGrid=gridEditTable("bobsCrossResultdiv",cols,loadParam,opts);
	$("#bobsCrossResultdiv").data("bobsCrossResultGrid", bobsCrossResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = bobsCrossResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=BobsCross&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = bobsCrossResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = bobsCrossResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}
////提交样本
//function submitSample(){
//	var id=$("#bobsCross_id").val();  
//	if(bobsCrossResultGrid.getModifyRecord().length > 0){
//		message("请先保存记录！");
//		return;
//	}
//	var record = bobsCrossResultGrid.getSelectionModel().getSelections();
//	var flg=false;
//	if(record.length>0){
//		for(var i=0;i<record.length;i++){
//			if(!record[i].get("submit")){
//				flg=true;
//			}
//		}
//	}else{
//		var grid=bobsCrossResultGrid.store;
//		for(var i=0;i<grid.getCount();i++){
//			if(grid.getAt(i).get("submit")==""){
//				flg=true;
//			}
//		}
//	}
//	if(flg){
//		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
//				{
//				        msg : '正在处理，请稍候。。。。。。',
//				        removeMask : true// 完成后移除
//				    });
//		loadMarsk.show();
//		
//		
//		var records = [];
//		
//		
//		
//		for ( var i = 0; i < record.length; i++) {
//			records.push(record[i].get("id"));
//		}
//		
//		ajax("post", "/experiment/bobs/cross/bobsCross/submitSample.action", {
//			id : id,
//			ids : records
//		}, function(data) {
//			if (data.success) {
//				loadMarsk.hide();
//				bobsCrossResultGrid.getStore().commitChanges();
//				bobsCrossResultGrid.getStore().reload();
//				message("提交成功！");
//			} else {
//				loadMarsk.hide();
//				message("提交失败！");
//			}
//		}, null);
//	}else{
//		message("没有需要提交的样本！");
//	}
//}
