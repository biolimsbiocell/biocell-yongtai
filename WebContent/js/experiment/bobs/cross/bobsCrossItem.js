var bobsCrossItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'checkCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'bobsCross-id',
		type:"string"
	});
	    fields.push({
		name:'bobsCross-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});

	cm.push({
		dataIndex:'checkCode',
		hidden : false,
		header:biolims.common.checkedSampleCode,
		width:25*6
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.expCode,
		width:20*6,
		sortable:true
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.qualified ], [ '0', biolims.common.disqualified ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.isQualified+'<font color="red" size=4"">*</font>',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : true,
		header:biolims.common.Submitted+'<font color="red" size=4"">*</font>',
		width:20*6,
//		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:biolims.user.associatedorderName,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsCross-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsCross-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/cross/bobsCross/showBobsCrossItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.bobsHybridizationDetail;
	opts.height =  document.body.clientHeight-161;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/bobs/cross/bobsCross/delBobsCrossItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				bobsCrossItemGrid.getStore().commitChanges();
				bobsCrossItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
	text : biolims.common.batchResult,
	handler : function() {
		var records = bobsCrossItemGrid.getSelectRecord();
		if (records && records.length > 0) {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_result_div"), "批量结果", null, {
				"确定" : function() {
						var result = $("#result").val();
						bobsCrossItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("result", result);
						});
						bobsCrossItemGrid.startEditing(0, 0);
					$(this).dialog("close");
					}
				}, true, options);
			}else{
				message("列表没有数据!");
			}
		}
	});
	opts.tbar.push({
		text :biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.fillDetail,
		handler : null
	});
	/*opts.tbar.push({
		text : '导出列表',
		handler : exportexceldCrossItem
	});*/
	opts.tbar.push({
		text : biolims.common.exportList,
		handler : createCSV
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	bobsCrossItemGrid=gridEditTable("bobsCrossItemdiv",cols,loadParam,opts);
	$("#bobsCrossItemdiv").data("bobsCrossItemGrid", bobsCrossItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(bobsCrossItemGrid);
	var id=$("#bobsCross_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/bobs/cross/bobsCross/saveBobsCrossItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					bobsCrossItemGrid.getStore().commitChanges();
					bobsCrossItemGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

/*function exportexceldCrossItem() {
	bobsCrossItemGrid.title = '导出列表';
	var vExportContent = bobsCrossItemGrid.getExcelXml();
	var x = document.getElementById('crossItem1');
	x.value = vExportContent;
	document.excelfrm.submit();
}*/

function createCSV(){
	var ids=[];
	var selected = bobsCrossItemGrid.getSelectionModel().getSelections(); 
	for ( var i = 0; i < selected.length; i++) {
		if (!selected[i].isNew) {
			ids.push(selected[i].get("id"));
		}
	}
	ajax("post", "/experiment/bobs/cross/bobsCross/createExcel.action", {
		ids :ids
	},function(data){
		if(data.success){
			message("生成成功！");
		}else{
			message("另一个程序正在使用此文件，进程无法访问！");
		}
	},null);
}