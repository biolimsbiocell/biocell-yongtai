var bobsAbnormalGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'acceptDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'reportDate',
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'isSubmit',
		type:"string"
	});
	   fields.push({
		name:'taskId',
		type:"string"
	});
	   fields.push({
		name:'taskName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:50*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		sortable:true,
		width:25*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		sortable:true,
		width:30*6
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : true,
		header:'接收日期',
		width:20*6,
		sortable:true,
		renderer: formatDate,
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:20*6,
		sortable:true,
		renderer: formatDate,
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单',
		width:20*6
		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	var storeisresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeisresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果<font color="red" size=4"">*</font>',
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden : true,
		header:'下一步ID',
		width:20*6
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向<font color="red" size=4"">*</font>',
		width:20*6,
		sortable:true
		//editor : nextFlowCob
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isSubmit',
		hidden : false,
		header:'是否提交<font color="red" size=4"">*</font>',
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'taskId',
		hidden : false,
		header:'相关任务单号',
		width:25*6
	});
	cm.push({
		dataIndex:'taskName',
		hidden : false,
		header:'相关实验模块',
		width:30*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:30*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobsAbnormal/showBobsAbnormalListJson.action";
	var opts={};
	opts.title="bobs检测异常样本";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
//	opts.tbar.push({
//		text : "批量结果",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#bat_result_div"), "批量结果", null, {
//				"确定" : function() {
//					var records = bobsAbnormalGrid.getSelectRecord();
//					if (records && records.length > 0) {
//						var result = $("#result").val();
//						bobsAbnormalGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("result", result);
//						});
//						bobsAbnormalGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : "批量下一步",
//		handler : function() {
//			var records = bobsAbnormalGrid.getSelectRecord();
//			if(records.length>0){
//				if(records.length>2){
//					var productId = new Array();
//					$.each(records, function(j, k) {
//						productId[j]=k.get("productId");
//					});
//					for(var i=0;i<records.length;i++){
//						if(i!=0&&productId[i]!=productId[i-1]){
//							message("检测项目不同！");
//							return;
//						}
//					}
//					loadTestNextFlowCob();
//				}else{
//					loadTestNextFlowCob();
//				}
//				
//			}else{
//				message("请选择数据!");
//			}
//		}
//	});
	/*opts.tbar.push({
		text : "批量提交",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_isSubmit_div"), "批量提交", null, {
				"确定" : function() {
					var records = bobsAbnormalGrid.getSelectRecord();
					if (records && records.length > 0) {
						var isSubmit = $("#isSubmit").val();
						bobsAbnormalGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("isSubmit", isSubmit);
						});
						bobsAbnormalGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});*/
	
	opts.tbar.push({
		text : '提交样本',
		handler : submitSample
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : saveAbnormal
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	bobsAbnormalGrid=gridEditTable("bobsAbnormaldiv",cols,loadParam,opts);
	$("#bobsAbnormaldiv").data("bobsAbnormalGrid", bobsAbnormalGrid);
});

//提交样本
function submitSample(){
	if(bobsAbnormalGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	
	var record = bobsAbnormalGrid.getSelectionModel().getSelections();
	var flg=true;
	if(record.length>0){
		for(var i=0;i<record.length;i++){
			if(record[i].get("isSubmit")==null
					|| record[i].get("isSubmit")==""){
				flg=false;
			}
			/*if(record[i].get("isGood")==""){
				message("结果不能为空！");
				return;
			}
			if(record[i].get("nextStepId")==""){
				message("下一步不能为空！");
				return;
			}*/
		}
		if(!flg){
			var loadMarsk = new Ext.LoadMask(Ext.getBody(),
					{
					        msg : '正在处理，请稍候。。。。。。',
					        removeMask : true// 完成后移除
					    });
			loadMarsk.show();
			var records = [];
			for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
			}
			ajax("post", "/experiment/bobsAbnormal/submitSample.action", {
				ids : records
			}, function(data) {
				if (data.success) {
					loadMarsk.hide();
					bobsAbnormalGrid.getStore().commitChanges();
					bobsAbnormalGrid.getStore().reload();
					message("提交成功！");
				} else {
					loadMarsk.hide();
					message("提交失败！");
				}
			}, null);
		}else{
			message("没有需要提交的样本！");
		}
	}else{
		message("请选择提交的样本！");
	}
}

function saveAbnormal(){
	var itemJson = commonGetModifyRecords(bobsAbnormalGrid);
	if(itemJson.length>0){
		ajax("post", "/experiment/bobsAbnormal/saveAbnormal.action", {
			itemDataJson : itemJson
		}, function(data) {
			if (data.success) {
				bobsAbnormalGrid.getStore().commitChanges();
				bobsAbnormalGrid.getStore().reload();
				message("保存成功！");
			} else {
				message("保存失败！");
			}
		}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}
//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
			commonSearchAction(bobsAbnormalGrid);
			$(this).dialog("close");
		},
		"清空" : function() {
			form_reset();

		}
	}, true, option);
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = bobsAbnormalGrid.getSelectRecord();
	var productId1="";
	$.each(records1, function(j, k) {
		productId1=k.get("productId");
	});
	 var options = {};
	options.width = 500;
	options.height = 500;
	loadNextFlow= loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=BobsAbnormal&productId="+productId1, {
		"确定" : function() {
			var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = bobsAbnormalGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(records, function(i, obj) {
					$.each(selectRecord, function(a, b) {
						obj.set("nextFlowId", b.get("id"));
						obj.set("nextFlow", b.get("name"));
					});
				});
			}else{
				message("请选择您要选择的数据");
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = bobsAbnormalGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}
//加载检测项目
function voucherProductFun() {
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
	var voucherProductFun = new Ext.Window(
			{
				id : 'voucherProductFun',
				modal : true,
				title : '选择项目',
				layout : 'fit',
				width : 600,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/productSelect.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						voucherProductFun.close();
					}
				} ]
			});
	voucherProductFun.show();
}	

function setProductFun(rec) {
	document.getElementById("bobsAbnormal_productName").value = rec.get('name');
	var win = Ext.getCmp('voucherProductFun');
	if (win) {
		win.close();
	}
}


//查询样本类型
function loadTestDicSampleType(){
	var options = {};
	options.width = 440;
	options.height = 600;
	loadDicSampleType=loadDialogPage(null, "样本类型","/sample/dicSampleType/dicSampleTypeSelect.action", {
		"确定" : function() {
			var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(a, b) {
					$("#bobsAbnormal_sampleType").val(b.get("name"));
				});
			}else{
				message("请选择您要选择的数据");
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}
function setDicSampleType(){
	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(a, b) {
			$("#bobsAbnormal_sampleType").val(b.get("name"));
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadDicSampleType.dialog("close");
}	
