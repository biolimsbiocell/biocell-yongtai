var bobsSjItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'orderNumber',
		type:"string"
	});
	   fields.push({
		name:'checkCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'bobsSj-id',
		type:"string"
	});
	    fields.push({
		name:'bobsSj-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'checkCode',
		hidden : false,
		header:biolims.common.checkedSampleCode,
		width:25*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : false,
		header:biolims.common.expCode,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:biolims.common.stepNum,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsSj-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsSj-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/sj/bobsSj/showBobsSjItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.bobsSJDetail;
	opts.height =  document.body.clientHeight-200;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/bobs/sj/bobsSj/delBobsSjItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				bobsSjItemGrid.getStore().commitChanges();
				bobsSjItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text :biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		text :biolims.common.exportList,
		handler : createCSV
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : saveInfo
	});
	bobsSjItemGrid=gridEditTable("bobsSjItemdiv",cols,loadParam,opts);
	$("#bobsSjItemdiv").data("bobsSjItemGrid", bobsSjItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(bobsSjItemGrid);
	var id=$("#bobsSj_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/bobs/sj/bobsSj/saveBobsSjItem.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					bobsSjItemGrid.getStore().commitChanges();
					bobsSjItemGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

function createCSV(){
	var ids=[];
	var selected = bobsSjItemGrid.getSelectionModel().getSelections(); 
	for ( var i = 0; i < selected.length; i++) {
		if (!selected[i].isNew) {
			ids.push(selected[i].get("id"));
		}
	}
	ajax("post", "/experiment/bobs/sj/bobsSj/createExcel.action", {
		ids :ids
	},function(data){
		if(data.success){
			message("生成成功！");
		}else{
			message("另一个程序正在使用此文件，进程无法访问！");
		}
	},null);
}