$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	setTimeout(function() {
		var getGrid=bobsSjTemplateGrid.store;
		if(getGrid.getCount()==0){
			loadTemplate($("#bobsSj_template").val());
		}
	}, 1000);
});	
function add() {
	window.location = window.ctx + "/experiment/bobs/sj/bobsSj/editBobsSj.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/bobs/sj/bobsSj/showBobsSjList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("BobsSj", {
					userId : userId,
					userName : userName,
					formId : $("#bobsSj_id").val(),
					title : $("#bobsSj_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	if (bobsSjResultGrid.getAllRecord().length > 0) {
		var selRecord = bobsSjResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			var oldv = selRecord.getAt(j).get("result");
			if(oldv==""){
				message("结果不能为空！");
				return;
			}
		}
		if(bobsSjResultGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
	
		completeTask($("#bobsSj_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
}
});






function save() {
if(checkSubmit()==true){
	    var bobsSjItemDivData = $("#bobsSjItemdiv").data("bobsSjItemGrid");
		document.getElementById('bobsSjItemJson').value = commonGetModifyRecords(bobsSjItemDivData);
	    var bobsSjTemplateDivData = $("#bobsSjTemplatediv").data("bobsSjTemplateGrid");
		document.getElementById('bobsSjTemplateJson').value = commonGetModifyRecords(bobsSjTemplateDivData);
	    var bobsSjReagentDivData = $("#bobsSjReagentdiv").data("bobsSjReagentGrid");
		document.getElementById('bobsSjReagentJson').value = commonGetModifyRecords(bobsSjReagentDivData);
	    var bobsSjCosDivData = $("#bobsSjCosdiv").data("bobsSjCosGrid");
		document.getElementById('bobsSjCosJson').value = commonGetModifyRecords(bobsSjCosDivData);
	    var bobsSjResultDivData = $("#bobsSjResultdiv").data("bobsSjResultGrid");
		document.getElementById('bobsSjResultJson').value = commonGetModifyRecords(bobsSjResultDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/bobs/sj/bobsSj/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/bobs/sj/bobsSj/copyBobsSj.action?id=' + $("#bobsSj_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#bobsSj_id").val() + "&tableId=bobsSj");
//}
$("#toolbarbutton_status").click(function(){
	
	var id = $("#bobsSj_id").val();
	ajax("post", "/experiment/bobs/sj/bobsSj/remindSubmit.action", {
		code : id,
		}, function(data) {
//			if (data.state == "20") {
				if ($("#bobsSj_id").val()){
					commonChangeState("formId=" + $("#bobsSj_id").val() + "&tableId=BobsSj");
				}	
//			} else {
//				message("没有提交，无法办理！");
//			}
		}, null);
	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#bobsSj_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#bobsSj_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#bobsSj_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.bobsSJ,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/bobs/sj/bobsSj/showBobsSjItemList.action", {
				id : $("#bobsSj_id").val()
			}, "#bobsSjItempage");
load("/experiment/bobs/sj/bobsSj/showBobsSjTemplateList.action", {
				id : $("#bobsSj_id").val()
			}, "#bobsSjTemplatepage");
load("/experiment/bobs/sj/bobsSj/showBobsSjReagentList.action", {
				id : $("#bobsSj_id").val()
			}, "#bobsSjReagentpage");
load("/experiment/bobs/sj/bobsSj/showBobsSjCosList.action", {
				id : $("#bobsSj_id").val()
			}, "#bobsSjCospage");
load("/experiment/bobs/sj/bobsSj/showBobsSjResultList.action", {
				id : $("#bobsSj_id").val()
			}, "#bobsSjResultpage");
//load("/experiment/bobs/sj/bobsSj/showBobsSjTempList.action", {
//				id : $("#bobsSj_id").val()
//			}, "#bobsSjTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
	//调用模板
	function TemplateFun(){
			var type="doBobsSj";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: '关闭',
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}
		
		function setTemplateFun(rec){
			document.getElementById('bobsSj_acceptUser').value=rec.get('acceptUser-id');
			document.getElementById('bobsSj_acceptUser_name').value=rec.get('acceptUser-name');
			var code=$("#bobsSj_template").val();
			if(code==""){
						document.getElementById('bobsSj_template').value=rec.get('id');
						document.getElementById('bobsSj_template_name').value=rec.get('name');
						var win = Ext.getCmp('TemplateFun');
						if(win){win.close();}
						var id=rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {
									var ob = bobsSjTemplateGrid.getStore().recordType;
									bobsSjTemplateGrid.stopEditing();
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("stepName",obj.name);
										
										p.set("note",obj.note);
										bobsSjTemplateGrid.getStore().add(p);							
									});
									
									bobsSjTemplateGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateReagent.action", {
							code : id,
							}, function(data) {
								if (data.success) {

									var ob = bobsSjReagentGrid.getStore().recordType;
									bobsSjReagentGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tReagent",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("batch",obj.batch);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("oneNum",obj.num);
										p.set("note",obj.note);
										p.set("sn",obj.sn);
										bobsSjReagentGrid.getStore().add(p);							
									});
									
									bobsSjReagentGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
							ajax("post", "/system/template/template/setTemplateCos.action", {
							code : id,
							}, function(data) {
								if (data.success) {

									var ob = bobsSjCosGrid.getStore().recordType;
									bobsSjCosGrid.stopEditing();
									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tCos",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);
										p.set("isGood",obj.isGood);
										p.set("itemId",obj.itemId);
										
										p.set("temperature",obj.temperature);
										p.set("speed",obj.speed);
										p.set("time",obj.time);
										p.set("note",obj.note);
										bobsSjCosGrid.getStore().add(p);							
									});			
									bobsSjCosGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null);

				
			}else{
				if(rec.get('id')==code){
	 				var win = Ext.getCmp('TemplateFun');
	 				if(win){win.close();}
	 			 }else{
							var ob1 = bobsSjTemplateGrid.store;
			 				if (ob1.getCount() > 0) {
								for(var j=0;j<ob1.getCount();j++){
									var oldv = ob1.getAt(j).get("id"); 
									//根据ID删除
									if(oldv!=null){
										ajax("post", "/experiment/bobs/sj/bobsSj/delOne.action", {
											ids : oldv,model:"1"
										}, function(data) {
											if (data.success) {
												message("删除成功！");
											} else {
												message("删除失败！");
											}
										}, null);
									}else{								
										bobsSjTemplateGrid.store.removeAll();
									}
								}
								bobsSjTemplateGrid.store.removeAll();
			 				}

							var ob2 = bobsSjReagentGrid.store;
							if (ob2.getCount() > 0) {
								for(var j=0;j<ob2.getCount();j++){
									var oldv = ob2.getAt(j).get("id");

									//根据ID删除
									if(oldv!=null){
									ajax("post", "/experiment/bobs/sj/bobsSj/delOne.action", {
										ids : oldv,model:"2"
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null); 
									}else{
										bobsSjReagentGrid.store.removeAll();
									}
								}
								bobsSjReagentGrid.store.removeAll();
			 				}
							//=========================================
							var ob3 = bobsSjCosGrid.store;
							if (ob3.getCount() > 0) {
								for(var j=0;j<ob3.getCount();j++){
									var oldv = ob3.getAt(j).get("id");
									
									//根据ID删除
									if(oldv!=null){
										ajax("post", "/experiment/bobs/sj/bobsSj/delOne.action", {
											ids : oldv,model:"3"
										}, function(data) {
											if (data.success) {
												message("删除成功！");
											} else {
												message("删除失败！");
											}
										}, null); 
									}else{
										bobsSjCosGrid.store.removeAll();
									}
								}
								bobsSjCosGrid.store.removeAll();
			 				}
							document.getElementById('bobsSj_template').value=rec.get('id');
							document.getElementById('bobsSj_template_name').value=rec.get('name');
			 				var win = Ext.getCmp('TemplateFun');
			 				if(win){win.close();}
							var id = rec.get('id');
							ajax("post", "/system/template/template/setTemplateItem.action", {
								code : id,
								}, function(data) {
									if (data.success) {	

										var ob = bobsSjTemplateGrid.getStore().recordType;
										bobsSjTemplateGrid.stopEditing();
										
										$.each(data.data, function(i, obj) {
											var p = new ob({});
											p.isNew = true;
											p.set("tItem",obj.id);
											p.set("code",obj.code);
											p.set("stepName",obj.name);
											
											p.set("note",obj.note);
											bobsSjTemplateGrid.getStore().add(p);							
										});
										
										bobsSjTemplateGrid.startEditing(0, 0);		
									} else {
										message("获取明细数据时发生错误！");
									}
								}, null); 
								ajax("post", "/system/template/template/setTemplateReagent.action", {
								code : id,
								}, function(data) {
									if (data.success) {	

										var ob = bobsSjReagentGrid.getStore().recordType;
										bobsSjReagentGrid.stopEditing();
										
										$.each(data.data, function(i, obj) {
											var p = new ob({});
											p.isNew = true;
											p.set("tReagent",obj.id);
											p.set("code",obj.code);
											p.set("name",obj.name);
											p.set("batch",obj.batch);
											p.set("isGood",obj.isGood);
											p.set("itemId",obj.itemId);
											
											p.set("oneNum",obj.num);
											p.set("note",obj.note);
											bobsSjReagentGrid.getStore().add(p);							
										});
										
										bobsSjReagentGrid.startEditing(0, 0);		
									} else {
										message("获取明细数据时发生错误！");
									}
								}, null); 
								ajax("post", "/system/template/template/setTemplateCos.action", {
								code : id,
								}, function(data) {
									if (data.success) {	

										var ob = bobsSjCosGrid.getStore().recordType;
										bobsSjCosGrid.stopEditing();
										
										$.each(data.data, function(i, obj) {
											var p = new ob({});
											p.isNew = true;
											p.set("tCos",obj.id);
											p.set("code",obj.code);
											p.set("name",obj.name);
											p.set("isGood",obj.isGood);
											p.set("itemId",obj.itemId);
											
											p.set("temperature",obj.temperature);
											p.set("speed",obj.speed);
											p.set("time",obj.time);
											p.set("note",obj.note);
											bobsSjCosGrid.getStore().add(p);							
										});			
										bobsSjCosGrid.startEditing(0, 0);		
									} else {
										message("获取明细数据时发生错误！");
									}
								}, null); 
							}
						}
	}
		
function BobsCrossFun(){
	var win = Ext.getCmp('BobsCrossFun');
	if (win) {win.close();}
	var BobsCrossFun= new Ext.Window({
	id:'BobsCrossFun',modal:true,title:'选择杂交编号',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/experiment/bobs/cross/bobsCross/bobsCrossSelect.action?flag=BobsCrossFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 BobsCrossFun.close(); }  }]  });     BobsCrossFun.show();
}

function setBobsCrossFun(rec){
	var id=$("#bobsSj_bobsCross").val();
	if(id==""){
		document.getElementById('bobsSj_bobsCross').value=rec.get('id');
		document.getElementById('bobsSj_bobsCross_name').value=rec.get('name');
		var win = Ext.getCmp('BobsCrossFun');
		if(win){
			win.close();
		}
		var code=rec.get('id');
		ajax("post","/experiment/bobs/cross/bobsCross/setItemToSj.action",
			{code:code},function(data){
			if(data.success){
				var ob=bobsSjItemGrid.getStore().recordType;
				bobsSjItemGrid.stopEditing();
				$.each(data.data,function(i,obj){
					//if(obj.result=="1"){
						var p = new ob({});
						p.isNew = true;
						p.set("code",obj.code);
						p.set("sampleCode",obj.sampleCode);
						//p.set("checkCode",obj.checkCode);
						p.set("orderNumber",obj.orderNumber);
						p.set("sampleType",obj.sampleType);
						p.set("productId",obj.productId);
						p.set("productName",obj.productName);
						p.set("note",obj.note);
						bobsSjItemGrid.getStore().add(p);
						bobsSjItemGrid.startEditing(0, 0);
					//}
				});
			}else {
				message("获取明细数据时发生错误！");
			}
		});
	}else{
		if(rec.get('id')==id){
			var win = Ext.getCmp('BobsCrossFun');
			if(win){win.close();}
		}else{
			var ob1 = bobsSjItemGrid.store;
			if (ob1.getCount() > 0) {
				for(var j=0;j<ob1.getCount();j++){
					var oldv = ob1.getAt(j).get("id");
					//根据ID删除
					if(oldv!=null){
						ajax("post", "/experiment/bobs/sj/bobsSj/delBobsSjItemOne.action", {
							ids : oldv
						}, function(data) {
							if (data.success) {
								message("删除成功！");
							} else {
								message("删除失败！");
							}
						}, null);
					}else{								
						bobsSjItemGrid.store.removeAll();
					}
				}
				bobsSjItemGrid.store.removeAll();
			}
			document.getElementById('bobsSj_bobsCross').value=rec.get('id');
			document.getElementById('bobsSj_bobsCross_name').value=rec.get('name');
			var win = Ext.getCmp('BobsCrossFun');
			if(win){
				win.close();
			}
			var code=rec.get('id');
			ajax("post","/experiment/bobs/cross/bobsCross/setItemToSj.action",
				{code:code},function(data){
				if(data.success){
					var ob=bobsSjItemGrid.getStore().recordType;
					bobsSjItemGrid.stopEditing();
					$.each(data.data,function(i,obj){
						//if(obj.result=="1"){
							var p = new ob({});
							p.isNew = true;
							p.set("code",obj.code);
							p.set("sampleCode",obj.sampleCode);
							//p.set("checkCode",obj.checkCode);
							p.set("orderNumber",obj.orderNumber);
							p.set("sampleType",obj.sampleType);
							p.set("productId",obj.productId);
							p.set("productName",obj.productName);
							p.set("note",obj.note);
							bobsSjItemGrid.getStore().add(p);
							bobsSjItemGrid.startEditing(0, 0);
						//}
					});
				}else {
					message("获取明细数据时发生错误！");
				}
			});
		}
	}
}


Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = bobsSjResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}

			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : "BobsSj"
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "BobsSj",id : $("#bobsSj_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
			window.location.reload();
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = bobsSjTemplateGrid.getStore().recordType;
				bobsSjTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("code",obj.code);
					p.set("stepName",obj.name);
					
					p.set("note",obj.note);
					bobsSjTemplateGrid.getStore().add(p);							
				});
				
				bobsSjTemplateGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = bobsSjReagentGrid.getStore().recordType;
				bobsSjReagentGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("oneNum",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					bobsSjReagentGrid.getStore().add(p);							
				});
				
				bobsSjReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {

				var ob = bobsSjCosGrid.getStore().recordType;
				bobsSjCosGrid.stopEditing();
				
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					bobsSjCosGrid.getStore().add(p);							
				});			
				bobsSjCosGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
}