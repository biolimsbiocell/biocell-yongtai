$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
//	if($("#bobsAnalysis_state").val()!="1"){
//		$("#showbobsSj").css("display","");
//	}else{
//		$("#showbobsSj").css("display","none");
//	}
//	if($("#bobsAnalysis_state").val()=="3"){
//		load("/experiment/bobs/analysis/bobsAnalysis/showBobsAnalysisTempList.action", {
//			id : $("#bobsAnalysis_id").val()
//		}, "#bobsAnalysisTemppage");
//		$("#bobsAnalysisTemppage").css("width","25%");
//	}
	
	var id=$("#bobsAnalysis_state").val();
	if(id=="3"){
		load("/experiment/bobs/analysis/bobsAnalysis/showBobsAnalysisTempList.action", null, "#bobsAnalysisTemppage");
		$("#markup").css("width","75%");
	}else{
		$("#bobsAnalysisTemppage").remove();
	}

});	
function add() {
	window.location = window.ctx + "/experiment/bobs/analysis/bobsAnalysis/editBobsAnalysis.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/bobs/analysis/bobsAnalysis/showBobsAnalysisList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("BobsAnalysis", {
					userId : userId,
					userName : userName,
					formId : $("#bobsAnalysis_id").val(),
					title : $("#bobsAnalysis_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	//var taskName=$("#taskName").val();
	//if(taskName=="BoBs分析二审"){
	if (bobsAnalysisResultGrid.getAllRecord().length > 0) {
		var selRecord = bobsAnalysisResultGrid.store;
		if(bobsAnalysisResultGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
		for(var j=0;j<selRecord.getCount();j++){
			var oldv = selRecord.getAt(j).get("submit");
			if(oldv!=1){
				flag=false;
				message("有样本未提交！");
				return;
			}
			if(selRecord.getAt(j).get("result")==""){
				message("是否合格不能为空！");
				return;
			}
			/*if(selRecord.getAt(j).get("nextFlow")==""){
				message("请填写下一步！");
				return;
			}*/
		}
		
		
		completeTask($("#bobsAnalysis_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}else{
		completeTask($("#bobsAnalysis_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});






function save() {
if(checkSubmit()==true){
	    var bobsAnalysisResultDivData = $("#bobsAnalysisResultdiv").data("bobsAnalysisResultGrid");
		document.getElementById('bobsAnalysisResultJson').value = commonGetModifyRecords(bobsAnalysisResultDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/bobs/analysis/bobsAnalysis/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/bobs/analysis/bobsAnalysis/copyBobsAnalysis.action?id=' + $("#bobsAnalysis_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#bobsAnalysis_id").val() + "&tableId=bobsAnalysis");
//}
$("#toolbarbutton_status").click(function(){
	
	
	var id = $("#bobsAnalysis_id").val();
	ajax("post", "/experiment/bobs/analysis/bobsAnalysis/remindSubmit.action", {
		code : id,
		}, function(data) {
//			if (data.state == "20") {
				if ($("#bobsAnalysis_id").val()){
					commonChangeState("formId=" + $("#bobsAnalysis_id").val() + "&tableId=BobsAnalysis");
				}	
//			} else {
//				message("没有提交，无法办理！");
//			}
		}, null);
	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#bobsAnalysis_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#bobsAnalysis_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.bobsAnalysis,
	    	   contentEl:'markup'
	       }]
	   });
});
load("/experiment/bobs/analysis/bobsAnalysis/showBobsAnalysisResultList.action", {
				id : $("#bobsAnalysis_id").val()
			}, "#bobsAnalysisResultpage");
load("/experiment/bobs/analysis/bobsAnalysis/showBobsAnalysisTempList.action", {
	id : $("#bobsAnalysis_id").val()
}, "#bobsAnalysisTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
			    	text: '复制'
					});
item.on('click', editCopy);

function BobsSjFun(){
	var win = Ext.getCmp('BobsSjFun');
	if (win) {win.close();}
	var BobsSjFun= new Ext.Window({
	id:'BobsSjFun',modal:true,title:'选择上机编号',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/experiment/bobs/sj/bobsSj/bobsSjSelect.action?flag=BobsSjFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 BobsSjFun.close(); }  }]  });     BobsSjFun.show(); 
}
function setBobsSjFun(rec){
	var id=$("#bobsAnalysis_bobsSj").val();
	if(id==""){
		document.getElementById('bobsAnalysis_bobsSj').value=rec.get('id');
		document.getElementById('bobsAnalysis_bobsSj_name').value=rec.get('name');
		var win = Ext.getCmp('BobsSjFun');
		if(win){
			win.close();
		}
		var code=rec.get('id');
		ajax("post","/experiment/bobs/sj/bobsSj/setResultToAnalysis.action",
			{code:code},function(data){
			if(data.success){
				var ob=bobsAnalysisResultGrid.getStore().recordType;
				bobsAnalysisResultGrid.stopEditing();
				$.each(data.data,function(i,obj){
					var p = new ob({});
					p.isNew = true;
					p.set("code",obj.code);
					p.set("sampleCode",obj.sampleCode);
					//p.set("checkCode",obj.checkCode);
					//p.set("orderNumber",obj.orderNumber);
					p.set("sampleType",obj.sampleType);
					p.set("productId",obj.productId);
					p.set("productName",obj.productName);
					p.set("note",obj.note);
					bobsAnalysisResultGrid.getStore().add(p);
					bobsAnalysisResultGrid.startEditing(0, 0);
				});
			}else {
				message("获取明细数据时发生错误！");
			}
		});
	}else{
		if(rec.get('id')==id){
			var win = Ext.getCmp('BobsSjFun');
			if(win){win.close();}
		}else{
			var ob1 = bobsAnalysisResultGrid.store;
			if (ob1.getCount() > 0) {
				for(var j=0;j<ob1.getCount();j++){
					var oldv = ob1.getAt(j).get("id");
					//根据ID删除
					if(oldv!=null){
						ajax("post", "/experiment/bobs/Analysis/bobsAnalysis/delBobsAnalysisItemOne.action", {
							ids : oldv
						}, function(data) {
							if (data.success) {
								message("删除成功！");
							} else {
								message("删除失败！");
							}
						}, null);
					}else{								
						bobsAnalysisResultGrid.store.removeAll();
					}
				}
				bobsAnalysisResultGrid.store.removeAll();
			}
			document.getElementById('bobsAnalysis_bobsSj').value=rec.get('id');
			document.getElementById('bobsAnalysis_bobsSj_name').value=rec.get('name');
			var win = Ext.getCmp('BobsSjFun');
			if(win){
				win.close();
			}
			var code=rec.get('id');
			ajax("post","/experiment/bobs/sj/bobsSj/setResultToAnalysis.action",
				{code:code},function(data){
				if(data.success){
					var ob=bobsAnalysisResultGrid.getStore().recordType;
					bobsAnalysisResultGrid.stopEditing();
					$.each(data.data,function(i,obj){
						var p = new ob({});
						p.isNew = true;
						p.set("code",obj.code);
						p.set("sampleCode",obj.sampleCode);
						//p.set("checkCode",obj.checkCode);
						//p.set("orderNumber",obj.orderNumber);
						p.set("note",obj.note);
						p.set("sampleType",obj.sampleType);
						p.set("productId",obj.productId);
						p.set("productName",obj.productName);
						bobsAnalysisResultGrid.getStore().add(p);
						bobsAnalysisResultGrid.startEditing(0, 0);
					});
				}else {
					message("获取明细数据时发生错误！");
				}
			});
		}
	}
}


Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = bobsAnalysisResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("submit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}

			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("submit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "BobsAnalysis",id : $("#bobsAnalysis_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

