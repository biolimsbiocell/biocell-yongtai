var bobsAnalysisResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleType',
		type:"string"
	});
	   fields.push({
		name:'lcjy',
		type:"string"
	});
	   fields.push({
		name:'ycbg',
		type:"string"
	});
	   fields.push({
		name:'jg',
		type:"string"
	});
	   fields.push({
		name:'jgjs',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'nextFlowId',
		type:"string"
	});
	   fields.push({
		name:'nextFlow',
		type:"string"
	});
	   fields.push({
		name:'result',
		type:"string"
	});
	   fields.push({
		name:'submit',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'bobsAnalysis-id',
		type:"string"
	});
	    fields.push({
		name:'bobsAnalysis-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'reportInfo-id',
		type:"string"
	});
	    fields.push({
		name:'reportInfo-name',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-fileName',
		type:"string"
	});
	    fields.push({
		name:'fileNum',
		type:"string"
	});
	 fields.push({
			name:'fileState',
			type:"string"
	});
	    fields.push({
		name:'upTime',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.user.itemNo,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.code,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:biolims.common.sampleCode,
		width:25*6,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:biolims.common.sampleType,
		width:25*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:biolims.common.productId,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:biolims.common.productName,
		width:25*6
		
	});
	cm.push({
		xtype : 'actioncolumn',
		hidden : false,
		width : 120,
		header :biolims.common.uploadAttachment,
		items : [ {
			icon : window.ctx + '/images/img_attach.gif',
			tooltip : '附件',
			handler : function(grid, rowIndex, colIndex) {
				var rec = grid.getStore().getAt(rowIndex);
				if(rec.get('id')){
					rec.set("upTime",(new Date()).toString());
					var win = Ext.getCmp('doc');
					if (win) {win.close();}
					var doc= new Ext.Window({
					id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
					collapsible: true,maximizable: true,
					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/operfile/initFileList.action?flag=doc&id="+ rec.get('id')+"&modelType=bobsAnalysisResult' frameborder='0' width='100%' height='100%' ></iframe>"}),
					buttons: [
					{ text: '关闭',
					 handler: function(){
					 doc.close(); }  }]  }); 
					 doc.show();
				}else{
					message("请先保存记录。");
				}
			}
		}]
	});
	
	cm.push({
		dataIndex : 'fileNum',
		header : biolims.common.attachmentNum,
		hidden : false,
		width : 20*6
//		handler: requestScope.fileNum
	});
	cm.push({
		dataIndex : 'fileState',
		header : biolims.common.attachmentState,
		hidden : false,
		width : 20*6
	});
	cm.push({
		dataIndex:'upTime',
		hidden : true,
		header:biolims.common.uploadAttachmentTime,
		width:20*6
	});
	cm.push({
		dataIndex:'template-id',
		hidden : true,
		header:biolims.report.reportFileId,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'template-fileName',
		hidden : true,
		header:biolims.common.attachmentName,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfo-id',
		hidden : true,
		header:biolims.common.templateId,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'reportInfo-name',
		hidden : false,
		header:biolims.report.ReportTemplate,
		sortable:true,
		width:15*10
	});
	cm.push({
		dataIndex:'jg',
		hidden : false,
		header:biolims.common.result,
		width:30*6,
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'jgjs',
		hidden : false,
		header:biolims.common.resultsCommentate,
		width:30*6,
		
		editor : new Ext.form.TextArea({
			allowBlank : true
		})
	});
	var storelcjyCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.outpatientVisits ], [ '0', biolims.common.ycoutpatientVisits ] ]
	});
	var lcjyCob = new Ext.form.ComboBox({
		store : storelcjyCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'lcjy',
		hidden : false,
		header:biolims.common.clinicalNote,
		width:20*6,
		editor : lcjyCob,
		renderer : Ext.util.Format.comboRenderer(lcjyCob)
	});
//	cm.push({
//		dataIndex:'lcjy',
//		hidden : false,
//		header:'临床建议',
//		width:30*6,
//		
//		editor : new Ext.form.TextArea({
//			allowBlank : true
//		})
//	});
	
	var storeexCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.regular ], [ '0',biolims.common.abnormal ], [ '2', biolims.common.polymorphic ] ]
	});
	var exCob = new Ext.form.ComboBox({
		store : storeexCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	
	cm.push({
		dataIndex:'ycbg',
		hidden : false,
		header:biolims.common.exceptionReported,
		width:20*6,
		
		editor : exCob,
		renderer : Ext.util.Format.comboRenderer(exCob)
	});
	
//	var storereCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
//	});
//	var reCob = new Ext.form.ComboBox({
//		store : storereCob,
//		displayField : 'name',
//		valueField : 'id',
//		mode : 'local'
//	});
	

	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1',biolims.common.qualified ], [ '0',biolims.common.qualified ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:biolims.common.isQualified+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:biolims.common.nextFlowId,
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:biolims.common.nextFlow+'<font color="red" size="4">*</font>',
		width:15*10,
		sortable:true,
		hidden:true,
		editor : nextFlowCob
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.common.yes ], [ '0', biolims.common.no] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : false,
		header:biolims.common.toSubmit+'<font color="red" size="4">*</font>',
		width:20*6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:biolims.common.state,
		width:20*6
	});
	cm.push({
		dataIndex:'bobsAnalysis-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bobsAnalysis-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/bobs/analysis/bobsAnalysis/showBobsAnalysisResultListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.bobsAnalysisResult;
	opts.height =  document.body.clientHeight-148;
	opts.tbar = [];
	if($("#bobsAnalysis_state").val()!="1"){
       opts.delSelect = function(ids) {
			ajax("post", "/experiment/bobs/analysis/bobsAnalysis/delBobsAnalysisResult.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					bobsAnalysisResultGrid.getStore().commitChanges();
					bobsAnalysisResultGrid.getStore().reload();
					message("删除成功！");
				} else {
					message("删除失败！");
				}
			}, null);
       };
		opts.tbar.push({
			text :biolims.report.checkFile,
			handler : loadPic
		});
		opts.tbar.push({
			text : biolims.common.vim,
			handler : function() {
				var records = bobsAnalysisResultGrid.getSelectRecord();
				if (records && records.length==1) {
					$.each(records, function(i, obj) {
						if(obj.get("jgjs")!=null && obj.get("jgjs")!=""){
							$("#re").val(obj.get("jgjs"));
						}
						if(obj.get("lcjy")!=null && obj.get("lcjy")!=""){
							$("#lc").val(obj.get("lcjy"));
						}
						if(obj.get("jg")!=null && obj.get("jg")!=""){
							$("#jg").val(obj.get("jg"));
						}
						if(obj.get("ycbg")!=null && obj.get("ycbg")!=""){
							$("#yc").val(obj.get("ycbg"));
						}
					});
					var options = {};
					options.width = 700;
					options.height = 500;
					loadDialogPage($("#bat_area_div"), "编辑数据", null, {
						"确定" : function() {
								var re = $("#re").val();
								var lc = $("#lc").val();
								var jg = $("#jg").val();
								var yc = $("#yc").val();
								bobsAnalysisResultGrid.stopEditing();
								$.each(records, function(i, obj) {
									obj.set("jgjs", re);
									obj.set("lcjy", lc);
									obj.set("jg", jg);
									obj.set("ycbg", yc);
								});
								bobsAnalysisResultGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);
				}else if(records.length==0){
					message("请选择要编辑的样本！");
				}else{
					message("只能选择一条样本编辑！");
				}
			}
		});
		opts.tbar.push({
			text :biolims.common.batchVim,
			handler : function() {
				var records = bobsAnalysisResultGrid.getSelectRecord();
				if (records && records.length>0) {
					var options = {};
					options.width = 700;
					options.height = 500;
					loadDialogPage($("#bat_area1_div"), "批量编辑数据", null, {
						"确定" : function() {
								var re = $("#re1").val();
								var lc = $("#lc1").val();
								var jg = $("#jg1").val();
								var yc = $("#yc1").val();
								bobsAnalysisResultGrid.stopEditing();
								$.each(records, function(i, obj) {
									if(re!=null && re!=""){
										obj.set("jgjs", re);
									}
									if(lc!=null && lc!=""){
										obj.set("lcjy", lc);
									}
									if(lc!=null && lc!=""){
										obj.set("jg", jg);
									}
									if(lc!=null && lc!=""){
										obj.set("ycbg", yc);
									}
								});
								bobsAnalysisResultGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);
				}else{
					message("请选择要编辑的样本！");
				}
			}
		});
//		opts.tbar.push({
//			text : '选择报告模板',
//			handler : showsampleReportSelectList
//		});
		opts.tbar.push({
			text : biolims.report.checkReport,
			handler : lookReport
		});
		opts.tbar.push({
			text :biolims.common.BatchIsQualified,
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_result_div"), "批量是否合格", null, {
					"确定" : function() {
						var records = bobsAnalysisResultGrid.getSelectRecord();
						if (records && records.length > 0) {
							var result = $("#result").val();
							bobsAnalysisResultGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("result", result);
							});
							bobsAnalysisResultGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});
//		opts.tbar.push({
//			text : "批量下一步",
//			handler : function() {
//				var records = bobsAnalysisResultGrid.getSelectRecord();
//				if(records.length>0){
//						loadTestNextFlowCob();
//					
//				}else{
//					message("请选择数据!");
//				}
//			}
//		});
		/*opts.tbar.push({
			text : "批量提交",
			handler : function() {
				var options = {};
				options.width = 400;
				options.height = 300;
				loadDialogPage($("#bat_submit_div"), "批量提交", null, {
					"确定" : function() {
						var records = bobsAnalysisResultGrid.getSelectRecord();
						if (records && records.length > 0) {
							var submit = $("#submit").val();
							bobsAnalysisResultGrid.stopEditing();
							$.each(records, function(i, obj) {
								obj.set("submit", submit);
							});
							bobsAnalysisResultGrid.startEditing(0, 0);
						}
						$(this).dialog("close");
					}
				}, true, options);
			}
		});*/
		opts.tbar.push({
			text : biolims.common.submitSample,
			handler : submitSample
		});
		opts.tbar.push({
			text : biolims.common.editableColAppear,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.uncheck,
			handler : null
		});
		opts.tbar.push({
			text : biolims.common.fillDetail,
			handler : null
		});
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveInfo
		});
	}
	
	bobsAnalysisResultGrid=gridEditTable("bobsAnalysisResultdiv",cols,loadParam,opts);
	$("#bobsAnalysisResultdiv").data("bobsAnalysisResultGrid", bobsAnalysisResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//保存
function saveInfo(){
	var itemJson = commonGetModifyRecords(bobsAnalysisResultGrid);
	var id=$("#bobsAnalysis_id").val();
	if(id != "NEW" ){
	if(itemJson.length>0){
		if(id!="" && id !=null){
			ajax("post", "/experiment/bobs/analysis/bobsAnalysis/saveBobsAnalysisResult.action", {
				id : id,
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {					
					bobsAnalysisResultGrid.getStore().commitChanges();
					bobsAnalysisResultGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);
		}
	}else{
		message("没有要保存的数据！");
	}
	  }else{
		  message("请点击上面的保存！");
	}
}

		//提交样本
		function submitSample(){
			var id=$("#bobsAnalysis_id").val();
			if(bobsAnalysisResultGrid.getModifyRecord().length > 0){
				message("请先保存记录！");
				return;
			}
			
			var record = bobsAnalysisResultGrid.getSelectionModel().getSelections();
			var flg=true;
			if(record.length>0){
				for(var i=0;i<record.length;i++){
					if(record[i].get("submit")==null
							|| record[i].get("submit")==""){
						flg=false;
					}
					if(record[i].get("isGood")==""){
						message("结果不能为空！");
						return;
					}
					/*if(record[i].get("nextStepId")==""){
						message("下一步不能为空！");
						return;
					}*/
				}
				if(!flg){
					var loadMarsk = new Ext.LoadMask(Ext.getBody(),
							{
							        msg : '正在处理，请稍候。。。。。。',
							        removeMask : true// 完成后移除
							    });
					loadMarsk.show();
					var records = [];
					for ( var i = 0; i < record.length; i++) {
						records.push(record[i].get("id"));
					}
					ajax("post", "/experiment/bobs/analysis/bobsAnalysis/submitSample.action", {
						id:id,
						ids : records
					}, function(data) {
						if (data.success) {
							loadMarsk.hide();
							bobsAnalysisResultGrid.getStore().commitChanges();
							bobsAnalysisResultGrid.getStore().reload();
							message("提交成功！");
						} else {
							loadMarsk.hide();
							message("提交失败！");
						}
					}, null);
				}else{
					message("没有需要提交的样本！");
				}
			}else{
				message("请选择提交的样本！");
			}
		}
function lookReport(){
	var ids=[];
	var idStr="";
	var idStr1="";
	var model="BobsAnalysisResult";
	var task="bobsAnalysisResult";
	var productId="";
	var selectGrid=bobsAnalysisResultGrid.getSelectionModel().getSelections();
	var getGrid=bobsAnalysisResultGrid.store;
	if(getGrid.getCount()>0){
		if (selectGrid.length==1) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				idStr+=obj.get("id");
				idStr1+=obj.get("id");
				productId+=obj.get("productId");
			});
			//idStr=ids.substring(0, ids.length-1);
			for ( var i = 0; i < selectGrid.length; i++) {
				if (!selectGrid[i].isNew) {
					ids.push(selectGrid[i].get("id"));
				}
			}
			window.open(window.ctx+"/reports/createReport/loadBobsReport.action?id="
					+idStr+"&taskId="+idStr1+"&model="+model+"&task="+task+"&productId="+productId,'','height=650,width=1050,scrollbars=yes,resizable=yes');
		}else{
			message("请选择一条数据！");
		}
	}else{
		message("列表中无数据！");
	}
}

function loadPic(){
	var ids="";
	var idStr="";
	var model="bobsAnalysisResult";
	var selectGrid=bobsAnalysisResultGrid.getSelectionModel().getSelections();
	var getGrid=bobsAnalysisResultGrid.store;
	if(getGrid.getCount()>0){
		if (selectGrid.length > 0) {
			//查看勾选明细的附件
			$.each(selectGrid, function(i, obj) {
				ids+="'"+obj.get("id")+"',";
			});
			idStr=ids.substring(0, ids.length-1);
			//alert(idStr);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}else{
			//没有勾选明细查看全部附件
			for(var j=0;j<getGrid.getCount();j++){
				ids+="'"+getGrid.getAt(j).get("id")+"',";
			}
			idStr=ids.substring(0, ids.length-1);
			window.open(window.ctx+"/experiment/karyotyping/karyotypingTask/loadPic.action?id="
					+idStr+"&model="+model,'','height=600,width=1200,scrollbars=yes,resizable=yes');
		}
	}else{
		message("列表中没有数据！");
	}
}
var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = bobsAnalysisResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=BobsAnalysis&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = bobsAnalysisResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = bobsAnalysisResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}
	

//选择报告模板
function showsampleReportSelectList() {
	var selected=bobsAnalysisResultGrid.getSelectionModel().getSelections();
	var productId="";
	var type="3";
	if(selected.length>0){
		if(selected.length>1){
			var productIds = new Array();
			$.each(selected, function(j, k) {
				productIds[j]=k.get("productId");
			});
			for(var i=0;i<selected.length;i++){
				if(i!=0 && productIds[i]!=productIds[i-1]){
					message("检测项目不同！");
					return;
				}else{
					productId=productIds[i];
				}
			}
		}else{
			$.each(selected, function(i, a) {
				productId=a.get("productId");
			});
		}
	}else{
		productId="";
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
	var showsampleReportSelectList = new Ext.Window(
			{
				id : 'showsampleReportSelectList',
				modal : true,
				title : '选择模版',
				layout : 'fit',
				width : 480,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sysmanage/report/selReportTemplateList.action?productId="+productId+"&type="+type+"' frameborder='0' width='780' height='500' ></iframe>"
						}),
				buttons : [ {
					text: biolims.common.close,
					handler : function() {
						showsampleReportSelectList.close();
					}
				} ]
			});
	showsampleReportSelectList.show();
}

function setReportFun(rec) {
	var selected=bobsAnalysisResultGrid.getSelectionModel().getSelections();
	if(selected.length>0){
		$.each(selected,function(i,b){
			b.set("reportInfo-id",rec.get('id'));
			b.set("reportInfo-name",rec.get('name'));
			b.set("template-id",rec.get('attach-id'));
			b.set("template-fileName",rec.get('attach-fileName'));
		});
	}else{
		message("请选择要出报告的样本！");
	}
	var win = Ext.getCmp('showsampleReportSelectList');
	if (win) {
		win.close();
	}
}

//生成并下载报告文件
function createReport(ids) {
	var selected=bobsAnalysisResultGrid.getSelectionModel().getSelections();
	var ids = [];
	if(bobsAnalysisResultGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	if(selected.length>0){
		var flag=true;
		var id="";//选中的Item的id
		var code="";//选中的样本号
		var fnames="";//样本号不对应的图片
		$.each(selected,function(i,b){
			code=b.get("code");
			id=b.get("id");
		});
		ajax("post", "/common/comsearch/com/compareCode.action", {
			id : id,model:"bobsAnalysisResult"
		}, function(data) {
			if (data.success) {
				$.each(data.data, function(i, obj) {
					var str=obj.fileName;
					var scode1 = new Array();
					scode1=str.split("-");
					if(scode1[0]==code){
						flag=true;
					}else{
						flag=false;
						fnames+="【"+obj.fileName+"】,";
					}
				});
			} else {
				message("请上传图片！");
				return;
			}
		}, null);
		if(fnames==""){
			for ( var i = 0; i < selected.length; i++) {
				if (!selected[i].isNew) {
					ids.push(selected[i].get("id"));
				}
			}
			ajax("post", "/experiment/bobs/analysis/bobsAnalysis/createReportFile.action", {
				ids : ids
			}, function(data) {
				if (data.success) {
					bobsAnalysisResultGrid.getStore().commitChanges();
					bobsAnalysisResultGrid.getStore().reload();
					//message("生成报告成功！");
				} else {
					message("预览报告失败！");
				}
			}, null);
			downFiles();
		}else{
			message("样本号不对应的图片有："+fnames);
			return;
		}
	}else{
		message("请选择要预览的样本！");
		return;
	}
}

//下载文件
function downFiles(){
	var selectGrid=bobsAnalysisResultGrid.getSelectionModel().getSelections();
	if(selectGrid.length>0){
		$.each(selectGrid, function(i, obj) {
			var fileName="PDF"+obj.get("code");
			window.open(window.ctx + '/operfile/downloadFileByPath.action?fileName=\\PDF\\'+fileName+'.pdf','','');
		});
	}
}