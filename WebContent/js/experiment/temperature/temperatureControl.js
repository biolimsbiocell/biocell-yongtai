var sampleAbnormalGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'createDate',
		type : "string"
	});
	fields.push({
		name : 'temperature',
		type : "Double"
	});
	fields.push({
		name : 'humidity',
		type : "Double"
	});
	fields.push({
		name : 'controlMeasures',
		type : "string"
	});
	fields.push({
		name : 'afterTemperature',
		type : "Double"
	});
	fields.push({
		name : 'afterHumidity',
		type : "Double"
	});
	fields.push({
		name : 'createUser-id',
		type : "string"
	});
	fields.push({
		name : 'createUser-name',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 20 * 6,
		hidden : true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'createDate',
		header : biolims.common.date,
		width : 20 * 6,
//		renderer : formatDate,
		sortable : true,
//		editor : new Ext.form.TextField({
//			allowBlank : true//		})
	});
	cm.push({
		dataIndex : 'temperature',
		header : biolims.common.temperature,
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex : 'humidity',
		header :biolims.common.relativeHumidity,
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'controlMeasures',
		header : biolims.common.controlMeasures,
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'afterTemperature',
		header : biolims.common.postControl,
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'afterHumidity',
		header : biolims.common.relativeLeft,
		width : 20 * 6,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'createUser-id',
		header : biolims.common.recorder,
		width : 20 * 10,
		sortable : true,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex : 'createUser-name',
		header : biolims.user.userId,
		width : 20 * 10,
		sortable : true,
		hidden:true
	});
	
	cm.push({
		dataIndex : 'note',
		header : biolims.common.note, // 备注
		width : 20 * 10,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex : 'state',
//		header : biolims.common.state,
//		width : 20 * 10,
//		hidden : true,
//		sortable : true
//	});
//	cm.push({
//		dataIndex : 'sampleType',
//		header : biolims.common.sampleType,
//		width : 20 * 6,
//		hidden : true,
//		sortable : true
//	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/experiment/temperature/showTemperatureListJson.action";
	var opts = {};
	opts.title = biolims.pooling.abnormalSamples;
	opts.height = document.body.clientHeight;

	opts.tbar = [];

	opts.tbar.push({
		text : biolims.common.editableColAppear, // 显示可编辑列
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,// '取消选中'
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'save',
		text : biolims.common.save,
		handler : save
	});


	opts.rowselect = function(id) {
		$("#selectId").val(id);
	};

	opts.rowdblclick = function(id) {
		$('#selectId').val(id);
		edit();
	};
	sampleAbnormalGrid = gridEditTable("show_sampleAbnormal_div", cols,
			loadParam, opts);
	$("#show_sampleAbnormal_div")
			.data("sampleAbnormalGrid", sampleAbnormalGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
// 保存Grid
function save() {
	var itemJson = commonGetModifyRecords(sampleAbnormalGrid);
	if (itemJson.length > 0) {
		ajax("post", "/experiment/temperature/save.action", {
			itemDataJson : itemJson
		}, function(data) {
			if (data.success) {
				sampleAbnormalGrid.getStore().commitChanges();
				sampleAbnormalGrid.getStore().reload();
				message(biolims.common.saveSuccess);
			} else {
				message(biolims.common.saveFailed);
			}
		}, null);
	} else {
		message("There is no data to save!");
	}
}
// 检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(sampleAbnormalGrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();
		}
	}, true, option);
}

function loadunusualMethodCob() {
	var records1 = sampleAbnormalGrid.getSelectRecord();
	var sampleTypeId = "";
	$.each(records1, function(j, k) {
		sampleTypeId = k.get("dicSampleType-id");
	});
	var options = {};
	options.width = 500;
	options.height = 500;
	loadDialogPage(null, biolims.sample.unusualMethod,
			"/sample/sampleAbnormal/showDicUnusualMethod.action", {
				"Confirm" : function() {
					var operGrid = $("#show_DicUnusualMethod_div1").data(
							"showDicUnusualMethodGrid");
					var selectRecord = operGrid.getSelectionModel()
							.getSelections();
					var records = sampleAbnormalGrid.getSelectRecord();
					if (selectRecord.length > 0) {
						$.each(records, function(i, obj) {
							$.each(selectRecord, function(a, b) {
								obj.set("dicType-id", b.get("id"));
								obj.set("dicType-name", b.get("name"));
							});
						});
					} else {
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
}

// 异常类型
function loadTestUnusualCob() {
	var records1 = sampleAbnormalGrid.getSelectRecord();
	var sampleTypeId = "";
	$.each(records1, function(j, k) {
		sampleTypeId = k.get("dicSampleType-id");
	});
	var options = {};
	options.width = 500;
	options.height = 500;
	loadDialogPage(null, biolims.sample.unusualId,
			"/sample/sampleAbnormal/showAbnormalType.action", {
				"Confirm" : function() {
					var operGrid = $("#show_abnormalType_div1").data(
							"showAbnormalTypeGrid");
					var selectRecord = operGrid.getSelectionModel()
							.getSelections();
					var records = sampleAbnormalGrid.getSelectRecord();
					if (selectRecord.length > 0) {
						$.each(records, function(i, obj) {
							$.each(selectRecord, function(a, b) {
								obj.set("unusual-id", b.get("id"));
								obj.set("unusual-name", b.get("name"));
							});
						});
					} else {
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
}

// 下一步流向
function loadTestNextFlowCob() {
	var records1 = sampleAbnormalGrid.getSelectRecord();
	var sampleTypeId = "";
	$.each(records1, function(j, k) {
		sampleTypeId = k.get("dicSampleType-id");
	});
	var options = {};
	options.width = 500;
	options.height = 500;
	loadDialogPage(
			null,
			biolims.common.selectNextFlow,
			"/system/nextFlow/nextFlow/shownextFlowDialogBysampleTypeId.action?sampleTypeId="
					+ sampleTypeId, {
				"Confirm" : function() {
					var operGrid = $("#show_dialog_nextFlow_div1").data(
							"shownextFlowDialogGrid");
					var selectRecord = operGrid.getSelectionModel()
							.getSelections();
					var records = sampleAbnormalGrid.getSelectRecord();
					if (selectRecord.length > 0) {
						$.each(records, function(i, obj) {
							$.each(selectRecord, function(a, b) {
								obj.set("nextFlowId", b.get("id"));
								obj.set("nextFlow", b.get("name"));
							});
						});
					} else {
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
}