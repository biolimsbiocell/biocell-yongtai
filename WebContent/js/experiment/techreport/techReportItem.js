﻿var techReportItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleCode',
		type:"string"
	});
	   fields.push({
		name:'sampleName',
		type:"string"
	});
	   fields.push({
		name:'code',
		type:"string"
	});
	    fields.push({
		name:'fileInfo-id',
		type:"string"
	});
	    fields.push({
		name:'fileInfo-name',
		type:"string"
	});
	    fields.push({
		name:'reportTemplateInfo-id',
		type:"string"
	});
	    fields.push({
		name:'reportTemplateInfo-name',
		type:"string"
	});
	   fields.push({
		name:'orderType',
		type:"string"
	});
	   fields.push({
		name:'orderId',
		type:"string"
	});
	   fields.push({
		name:'productId',
		type:"string"
	});
	   fields.push({
		name:'productName',
		type:"string"
	});
	   fields.push({
		name:'contentId',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'sysTechReportMain-id',
		type:"string"
	});
	    fields.push({
		name:'sysTechReportMain-name',
		type:"string"
	});
	   fields.push({
		name:'retiId',
		type:"string"
	});
	   fields.push({
		name:'retiName',
		type:"string"
	});
	   fields.push({
		name:'retiNote',
		type:"string"
	});
	   fields.push({
		name:'fileNum',
		type:"string"
	});
	   fields.push({
			name : 'projectId',
			type : "string"
		});
		fields.push({
			name : 'projectName',
			type : "string"
		});
		fields.push({
			name : 'crmCustomerId',
			type : "string"
		});
		fields.push({
			name : 'crmCustomerName',
			type : "string"
		});
		fields.push({
			name : 'crmDoctorId',
			type : "string"
		});
		fields.push({
			name : 'crmDoctorName',
			type : "string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleName',
		hidden : false,
		header:'样本名称',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:30*6
	});

	cm.push({
		dataIndex:'fileInfo-id',
		hidden : true,
		header:'报告文件ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'reportTemplateInfo-id',
		hidden : true,
		header:'报告模板ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'reportTemplateInfo-name',
		hidden : true,
		header:'报告模板',
		width:15*10
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:'任务单编号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	
	cm.push({
		dataIndex:'projectId',
		header:"项目编号",
		width:20*6,
		hidden:false
	});
	cm.push({
		dataIndex:'projectName',
		header:"项目名称",
		width:20*6,
		hidden:false
	});
	cm.push({
		dataIndex:'crmCustomerId',
		header:"客户id",
		width:20*10,
		hidden:true,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'crmCustomerName',
		header:"客户",
		width:20*10,
		hidden:false
	});
	cm.push({
		dataIndex:'crmDoctorId',
		header:"医生id",
		width:20*10,
		hidden:true
	});
	cm.push({
		dataIndex:'crmDoctorName',
		header:"医生",
		width:20*6,
		hidden:false
	});
//	cm.push({
//		dataIndex:'productId',
//		hidden : false,
//		header:'项目编号',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'productName',
//		hidden : true,
//		header:'项目名称',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'contentId',
//		hidden : false,
//		header:'合同编号',
//		width:20*6
//	});
	cm.push({
		dataIndex:'sysTechReportMain-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sysTechReportMain-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
//	cm.push({
//		dataIndex:'retiId',
//		hidden : true,
//		header:'模板id',
//		width:40*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'retiName',
//		hidden : false,
//		header:'模板名称',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'retiNote',
//		hidden : false,
//		header:'模板路径',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'fileInfo-name',
//		hidden : false,
//		header:'报告文件',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
	xtype : 'actioncolumn',
	width : 80,
	header : '上传附件',
	items : [ {
		icon : window.ctx + '/images/img_attach.gif',
		tooltip : '附件',
		handler : function(grid, rowIndex, colIndex) {
			var rec = grid.getStore().getAt(rowIndex);
			if(rec.get('id')&&rec.get('id')!='NEW'){
				
				var win = Ext.getCmp('doc');
				if (win) {win.close();}
				var doc= new Ext.Window({
				id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
				plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
				collapsible: true,maximizable: true,
				items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
				html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=techReportItem&flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
				buttons: [
				{ text: '关闭',
				 handler: function(){
				 doc.close(); }  }]  }); 
				 doc.show();
			
			}else{
				message("请先保存记录。");
			}
		}
	} ]
});
	cm.push({
		dataIndex:'fileNum',
		hidden : false,
		header:'附件数量',
		width:15*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/techreport/techReportMain/showTechReportItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="报告明细";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
	if($("#techReportMain_stateName").val()!=biolims.common.finish){
		
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/techreport/techReportMain/delTechReportItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				techReportItemGrid.getStore().commitChanges();
				techReportItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};

//	opts.tbar.push({
//		text : '选择报告模板',
//		handler : sampleReportSelectFun
//	});
//	opts.tbar.push({
//		text : '生成  / 查看报告',
//		handler : function() {
//			//alert($("#techReportMain_orderId").val());
//			var user=window.userName;
//			window.userId="admin";
//			//window.userName = "管理员";
//			//var selRecord = reportTemplateGrid.getSelectionModel().getSelections();
//			var selRecord=techReportItemGrid.store;
//			
//			var url = '__report=DnaTask.rptdesign&id=' + $("#experimentDnaGet_id").val();
//			commonPrint(url);
//			
//			
////			if($("#techReportMain_reportTemplateInfo").val()!=""){
////				if (selRecord) {
////					var url = window.ctx+"/experiment/techreport/techReportMain/toReportGen.action?user="+window.userId+"&orderId="+$("#techReportMain_orderId").val()+"&tid="+$("#techReportMain_id").val();
////					window.open(url,"","");
////				}else {
////					message("请在左侧选择您要生成报告的数据！");
////					return;
////				}
////			}else{
////				message("请选择报告模板！");
////				return;
////			}
//		}
//	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	}
	techReportItemGrid=gridEditTable("techReportItemdiv",cols,loadParam,opts);
	$("#techReportItemdiv").data("techReportItemGrid", techReportItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

