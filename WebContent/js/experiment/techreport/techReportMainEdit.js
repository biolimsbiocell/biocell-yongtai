﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
//	if($("#techReportMain_id").val()!="000000"){
//		load("/experiment/techreport/techReportMain/showSampleReportList.action",null, "#reportpage");
//		$("#markup").css("width","75%");
//	}
});
function add() {
	window.location = window.ctx + "/experiment/techreport/techReportMain/editTechReportMain.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/techreport/techReportMain/showTechReportMainList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#techReportMain", {
					userId : userId,
					userName : userName,
					formId : $("#techReportMain_id").val(),
					title : $("#techReportMain_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#techReportMain_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});


$("#toolbarbutton_print").click(function(){
//	var url = '__report=qcReport.rptdesign&id=' + $("#techReportMain_id").val();
	var url = '__report=SQR_cn.rptdesign&id=' + $("#techReportMain_id").val()+'&&__title=&__showtitle=false&__toolbar=true&__navigationbar=true&&__format=docx&__locale=zh_CN&__clean=true&__filename=3333'
	commonPrint(url);
});




function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var techReportItemDivData = $("#techReportItemdiv").data("techReportItemGrid");
		document.getElementById('techReportItemJson').value = commonGetModifyRecords(techReportItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/techreport/techReportMain/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/techreport/techReportMain/copyTechReportMain.action?id=' + $("#techReportMain_id").val();
}
//function changeState() {
//	commonChangeState("formId=" + $("#techReportMain_id").val() + "&tableId=techReportMain");
//}
$("#toolbarbutton_status").click(function(){
	if ($("#techReportMain_id").val()){
		commonChangeState("formId=" + $("#techReportMain_id").val() + "&tableId=TechReportMain");
	}	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#techReportMain_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'检测报告',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/techreport/techReportMain/showTechReportItemList.action", {
				id : $("#techReportMain_id").val()
			}, "#techReportItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
	//科研任务单
	var loadTechService;
	function selectTechJkServiceTask(){
		 	var options = {};
		 	options.width = document.body.clientWidth - 470;
		 	options.height = document.body.clientHeight - 80;
		 	loadTechService=loadDialogPage(null, biolims.common.selectExtract, "/technology/wk/techJkServiceTask/showTechJkServiceTaskDialogList.action?type=0&state=2", {
				"Confirm" : function() {
					var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
					var selectRecord = operGrid.getSelectionModel().getSelections();
					if (selectRecord.length > 0) {
						$("#techReportMain_techJkServiceTask_name").val(selectRecord[0].get("name"));
						$("#techReportMain_techJkServiceTask").val(selectRecord[0].get("id"));
						ajax("post", "/technology/wk/techJkServiceTask/selTechJkserverTaskItem.action", {
							id : selectRecord[0].get("id"),
							}, function(data) {
								if (data.success) {	
									var ob = techReportItemGrid.getStore().recordType;
									techReportItemGrid.stopEditing();
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										
										p.set("code", obj.code);
										p.set("sampleCode", obj.sampleCode);
										if(obj.project!=null){
											p.set("projectId",obj.project.id);
											p.set("projectName",obj.project.name);
										}
										if(obj.crmDoctor!=null){
											p.set("crmDoctorName",obj.crmDoctor.name);
											p.set("crmDoctorId",obj.crmDoctor.id);
										}
										if(obj.crmCustomer){
											p.set("crmCustomerId",obj.crmCustomer.id);
											p.set("crmCustomerName",obj.crmCustomer.name);
										}
										if(obj.techJkServiceTask!=null){
											p.set("orderId",obj.techJkServiceTask.id);
										}
										if(obj.sampleInfoIn!=null){
											p.set("sampleName",obj.sampleInfoIn.patientName);
										}
										
										
//										p.set("patientName", obj.dataType);
//										p.set("productId", obj.techJkServiceTask.proId);
//										p.set("productName", obj.techJkServiceTask.proName);
//										p.set("sampleInItemId", obj.tempId);
//										p.set("sampleType", obj.sampleType);
//										p.set("location", obj.location);
//										p.set("concentration", obj.concentration);
//										p.set("volume", obj.volume);
//										p.set("sumTotal", obj.sampleNum);
//										p.set("applyUser-id", obj.techJkServiceTask.createUser.id);
//										p.set("applyUser-name", obj.techJkServiceTask.createUser.name);
//										p.set("nextFlowId", obj.nextFlowId);
//										p.set("nextFlow", obj.nextFlow);
//										p.set("note", obj.name);
										techReportItemGrid.getStore().add(p);							
									});
									
									techReportItemGrid.startEditing(0, 0);		
								} 
							}, null); 
						
					}else{
						message(biolims.common.selectYouWant);
						return;
					}
					$(this).dialog("close");
				}
			}, true, options);
	}
	
	function setTechService(){

		var operGrid = $("#show_dialog_techJkServiceTask_div1").data("showTechJkServiceTaskDialogGrid");
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			$("#techReportMain_techJkServiceTask_name").val(selectRecord[0].get("name"));
			$("#techReportMain_techJkServiceTask").val(selectRecord[0].get("id"));
			ajax("post", "/technology/wk/techJkServiceTask/selTechJkserverTaskItem.action", {
				id : selectRecord[0].get("id"),
				}, function(data) {
					if (data.success) {	
						var ob = techReportItemGrid.getStore().recordType;
						techReportItemGrid.stopEditing();
						$.each(data.data, function(i, obj) {
							var p = new ob({});
							p.isNew = true;
							p.set("code", obj.code);
							p.set("sampleCode", obj.sampleCode);
							if(obj.project!=null){
								p.set("projectId",obj.project.id);
								p.set("projectName",obj.project.name);
							}
							if(obj.crmDoctor!=null){
								p.set("crmDoctorName",obj.crmDoctor.name);
								p.set("crmDoctorId",obj.crmDoctor.id);
							}
							if(obj.crmCustomer){
								p.set("crmCustomerId",obj.crmCustomer.id);
								p.set("crmCustomerName",obj.crmCustomer.name);
							}
							if(obj.techJkServiceTask!=null){
								p.set("orderId",obj.techJkServiceTask.id);
							}
							if(obj.sampleInfoIn!=null){
								p.set("sampleName",obj.sampleInfoIn.patientName);
							}
//							p.set("patientName", obj.dataType);
//							p.set("productId", obj.techJkServiceTask.proId);
//							p.set("productName", obj.techJkServiceTask.proName);
//							p.set("sampleInItemId", obj.tempId);
//							p.set("sampleType", obj.sampleType);
//							p.set("location", obj.location);
//							p.set("concentration", obj.concentration);
//							p.set("volume", obj.volume);
//							p.set("sumTotal", obj.sampleNum);
//							p.set("applyUser-id", obj.techJkServiceTask.createUser.id);
//							p.set("applyUser-name", obj.techJkServiceTask.createUser.name);
//							p.set("nextFlowId", obj.nextFlowId);
//							p.set("nextFlow", obj.nextFlow);
//							p.set("note", obj.name);
							techReportItemGrid.getStore().add(p);							
						});
						
						techReportItemGrid.startEditing(0, 0);		
					} 
				}, null); 
			
			
		}else{
			message(biolims.common.selectYouWant);
			return;
		}
		loadTechService.dialog("close");

	}

	
	
//	function TechDnaServiceTaskFun(){
//		var win = Ext.getCmp('TechDnaServiceTaskFun');
//		if (win) {win.close();}
//		var TechDnaServiceTaskFun= new Ext.Window({
//		id:'TechDnaServiceTaskFun',modal:true,title:'选择任务单',layout:'fit',width:500,height:500,closeAction:'close',
//		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//		collapsible: true,maximizable: true,
//		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
//		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/experiment/dna/experimentDnaGet/showDnaTechTaskDialog.action?flag=TechDnaServiceTaskFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
//		buttons: [
//		{ text: '关闭',
//		 handler: function(){
//		 TechDnaServiceTaskFun.close(); }  }]  });  
//		TechDnaServiceTaskFun.show(); }
//	function setTechServiceDnaTaskFun(rec){
//		document.getElementById('techReportMain_orderId').value=rec.get('id');
//		document.getElementById('orderId').value=rec.get('id');
//		//$("#techReportMain_orderId").val(rec.get('id'));
//		var win = Ext.getCmp('TechDnaServiceTaskFun');
//		if(win){win.close();}
//		selectReport2();
//	}
	//左侧页面的查询功能
	function selectReport2(){
		var rid = $("#report_tem_grid_div").data("reportTemplateGrid");
		commonSearchActionByMo(reportTemplateGrid,"21");
	}
	
	function sampleReportSelectFun() {
		var record = techReportItemGrid.getSelectionModel().getSelected();
		showsampleReportSelectList();
		}
	function showsampleReportSelectList() {
		var win = Ext.getCmp('showsampleReportSelectList');
		if (win) {
			win.close();
		}
		var showsampleReportSelectList = new Ext.Window(
				{
					id : 'showsampleReportSelectList',
					modal : true,
					title : '选择模版',
					layout : 'fit',
					width : 780,
					height : 500,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sysmanage/report/selReportTemplateList.action' frameborder='0' width='780' height='500' ></iframe>"
							}),
					buttons : [ {
						text: biolims.common.close,
						handler : function() {
							showsampleReportSelectList.close();
						}
					} ]
				});
		showsampleReportSelectList.show();
	}
	function setReportFun(rec) {
		$("#techReportMain_reportTemplateInfo").val(rec.get('id'));
		$("#techReportMain_reportTemplateInfo_name").val(rec.get('name'));
		var win = Ext.getCmp('showsampleReportSelectList');
		if (win) {
			win.close();
		}
	}