var techReportMainGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	    fields.push({
			name:'state',
			type:"string"
		});
	    fields.push({
			name:'stateName',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:40*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:40*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'报告人ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'报告人',
		
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'报告日期',
		width:25*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单号',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'工作流状态',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/techreport/techReportMain/showTechReportMainListJson.action";
	var opts={};
	opts.title="检测报告";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	techReportMainGrid=gridTable("show_techReportMain_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/experiment/techreport/techReportMain/editTechReportMain.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/techreport/techReportMain/editTechReportMain.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/techreport/techReportMain/viewTechReportMain.action?id=' + id;
}
function exportexcel() {
	techReportMainGrid.title = '导出列表';
	var vExportContent = techReportMainGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(techReportMainGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
