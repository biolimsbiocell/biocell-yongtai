var techReportMainDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'orderId',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:40*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:'报告人ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'报告人',
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'报告日期日期',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'任务单号',
		width:40*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/techreport/techReportMain/showTechReportMainListJson.action";
	var opts={};
	opts.title="检测报告";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setTechReportMainFun(rec);
	};
	techReportMainDialogGrid=gridTable("show_dialog_techReportMain_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(techReportMainDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
