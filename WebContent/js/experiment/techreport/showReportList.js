var reportTemplateGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'projectId',
		type : "string"
	});
	fields.push({
		name : 'contractId',
		type : "string"
	});
	fields.push({
		name : 'orderType',
		type : "string"
	});
	fields.push({
		name : 'completeDate',
		type : "date",
		dateFormat:'Y-m-d'
	});
	fields.push({
		name : 'reportFile-id',
		type : "string"
	});
	fields.push({
		name : 'reportFile-fileName',
		type : "string"
	});
	fields.push({
		name : 'reportResult',
		type : "string"
	});
	fields.push({
		name : 'reportResultId',
		type : "string"
	});
	fields.push({
		name : 'reti-id',
		type : "string"
	});
	fields.push({
		name : 'reti-name',
		type : "string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'reti-note',
		type : "string"
	});
	fields.push({
		name : 'disease',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'dnaCode',
		type : "string"
	});
	fields.push({
		name : 'sampleCode',
		type : "string"
	});
	fields.push({
		name : 'fileNum',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : 'ID',
		hidden :true,
		width : 120
	});
	cm.push({
		dataIndex : 'dnaCode',
		header : '样本编号',
		//hidden:true,
		width : 150
	});
	cm.push({
		dataIndex : 'sampleCode',
		header : '原始样本编号',
		//hidden:true,
		width : 150
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex : 'contractId',
		header : '合同号',
		width : 120
	});
	cm.push({
		dataIndex : 'projectId',
		header : '项目号',
		width : 120
	});
//	cm.push({
//		dataIndex : 'orderType',
//		header : '任务单类型',
//		hidden:true,
//		width : 120
//	});
//	var storeStateCob = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '待生成报告' ], [ '1', '已生成报告' ],['','请选择'] ]
//	});
//	cm.push({
//		dataIndex : 'state',
//		header : '状态',
//		width : 120,
//		editor : gridIsFee,
//		renderer : function(value, metadata, record, rowIndex,
//				colIndex, store) {
//			if (value == "1") {
//				return "已生成报告";
//			} else  if (value == "0"){
//				return "待生成报告";
//			} else{
//				return "请选择";
//			}
//		}
//	});
	cm.push({
		dataIndex : 'completeDate',
		header : '报告完成日期',
		width : 100,
		renderer : formatDate
	});
	cm.push({
		dataIndex : 'orderId',
		header : '任务单编号',
		width : 100,
		hidden : false
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', 'DNA' ], [ '1', '文库' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'orderType',
		header:'任务单类型',
		width:20*6,
		sortable:true,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
//	cm.push({
//		dataIndex : 'reti-id',
//		header : '模版ID',
//		width : 100,
//		hidden : true
//	});
//
//	cm.push({
//		dataIndex : 'reti-name',
//		header : '模版名称',
//		width : 180
//	});
//	cm.push({
//		dataIndex : 'reti-note',
//		header : '模版路径',
//		width : 100,
//		hidden : true
//	});
//	cm.push({
//		dataIndex : 'reportResult',
//		header : '结果说明',
//		width : 300,
//		editor :  new Ext.form.HtmlEditor({
//			readOnly:false
//			})
//	});
//	cm.push({
//		dataIndex : 'reportResultId',
//		header : '编码',
//		width  : 300,
//		hidden : true
//	});
//	cm.push({
//		dataIndex : 'reportFile-id',
//		header : '附件ID',
//		width : 200,
//		hidden : true
//	});
//	cm.push({
//		dataIndex:'note',
//		hidden : false,
//		header:'备注',
//		width:15*10,
//		hidden:true
//	});
//	cm.push({
//		xtype : 'actioncolumn',
//		width : 80,
//		header : '上传附件',
//		items : [ {
//			icon : window.ctx + '/images/img_attach.gif',
//			tooltip : '附件',
//			handler : function(grid, rowIndex, colIndex) {
//				var rec = grid.getStore().getAt(rowIndex);
//				if(rec.get('id')&&rec.get('id')!='NEW'){
//					
//					var win = Ext.getCmp('doc');
//					if (win) {win.close();}
//					var doc= new Ext.Window({
//					id:'doc',modal:true,title:'附件',layout:'fit',width:900,height:500,closeAction:'close',
//					plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//					collapsible: true,maximizable: true,
//					items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
//					html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/operfile/initFileList.action?modelType=sampleReportItem&flag=doc&id="+ rec.get('id')+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
//					buttons: [
//					{ text: '关闭',
//					 handler: function(){
//					 doc.close(); }  }]  }); 
//					 doc.show();
//				
//				}else{
//					message("请先保存记录。");
//				}
//			}
//		} ]
//	});
//	cm.push({
//		dataIndex : 'fileNum',
//		header : '附件数量',
//		width : 150
//	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/experiment/techreport/techReportMain/showSampleReportListJson.action";
	loadParam.limit=100;
	var opts = {};
	opts.height = document.body.clientHeight-30;
	opts.title = "待报告明细";
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '添加样本',
		handler : addItem
	});
//	opts.tbar.push({
//		text : "选择模版",
//		handler : function() {
//			sampleReportSelectFun();
//		}
//	});
	
//	function sampleReportSelectFun() {
//		var record = reportTemplateGrid.getSelectionModel().getSelected();
//		showsampleReportSelectList();
//	}
//	function showsampleReportSelectList() {
//		var win = Ext.getCmp('showsampleReportSelectList');
//		if (win) {
//			win.close();
//		}
//		var showsampleReportSelectList = new Ext.Window(
//				{
//					id : 'showsampleReportSelectList',
//					modal : true,
//					title : '选择模版',
//					layout : 'fit',
//					width : 780,
//					height : 500,
//					closeAction : 'close',
//					plain : true,
//					bodyStyle : 'padding:5px;',
//					buttonAlign : 'center',
//					collapsible : true,
//					maximizable : true,
//					items : new Ext.BoxComponent(
//							{
//								id : 'maincontent',
//								region : 'center',
//								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/sysmanage/report/selReportTemplateList.action' frameborder='0' width='780' height='500' ></iframe>"
//							}),
//					buttons : [ {
//						text: biolims.common.close,
//						handler : function() {
//							showsampleReportSelectList.close();
//						}
//					} ]
//				});
//		showsampleReportSelectList.show();
//	}
	
//	opts.tbar.push({
//		text : "批量状态",
//		handler : function() {
//			var options = {};
//			options.width = 400;
//			options.height = 300;
//			loadDialogPage($("#state_div"), "批量状态", null, {
//				"确定" : function() {
//					var records = reportTemplateGrid.getSelectRecord();
//					if (!records.length) {
//						records = reportTemplateGrid.getAllRecord();
//					}
//					if (records && records.length > 0) {
//						var isGood = $("#zt").val();
//						reportTemplateGrid.stopEditing();
//						$.each(records, function(i, obj) {
//							obj.set("state", isGood);
//						});
//						reportTemplateGrid.startEditing(0, 0);
//					}
//					$(this).dialog("close");
//				}
//			}, true, options);
//		}
//	});
//	opts.tbar.push({
//		text : '保存',
//		handler : function() {
//			var itemJson = commonGetModifyRecords(reportTemplateGrid);
//			var reportGrid = $("#report_tem_grid_div").data("reportTemplateGrid");
//			var selectRecord = reportTemplateGrid.getSelectRecord(reportGrid);
//			var flag=false;
//			if (selectRecord && selectRecord.length > 0) {
//				$.each(selectRecord, function(i, obj) {
//					if(obj.get('state')!==''||!obj.get('state')==null ||!obj.get('state')==undefined){
//						flag=true;
//					}
//				});
//				if(flag){
//					if (itemJson.length > 0) {
//						ajax("post", "/experiment/techreport/saveSampleReportList.action", {
//							itemDataJson : itemJson
//						}, function(data) {
//							if (data.success) {
//								message("保存成功！");
//								reportTemplateGrid.getStore().commitChanges();
//								reportTemplateGrid.getStore().reload();
//							} else {
//								message("保存失败！");
//							}
//						}, null);
//					}else{
//						message("请选择数据！");
//					}
//				}else{
//					message("请选择状态！");
//				}
//		}
//	});

//	opts.tbar.push({
//		text : "生成报告",
//		handler : function() {
//			var user=window.userName;
//			//window.userId="admin";
//			//window.userName = "管理员";
//			var selRecord = reportTemplateGrid.getSelectionModel().getSelections();
//			var ids="";
//			if (selRecord) {
//				$.each(selectRcords, function(i, obj) {
//					var code=obj.get("id");
//					ids+=code+",";
//				});
//			}else {
//				message("请选择您要生成报告的数据！");
//			}
//			//判断任务单是否为空
//			if($("#orderId").val()!=""){
//				var selecttp = reportTemplateGrid.getSelectOneRecord();
//				if(selecttp) {
//					var url = window.ctx+"/experiment/techreport/toReportGen.action?ids="+ids+"&user="+user+"&orderId"+$("#orderId").val();
//					window.open(url,"","");
//				}
//			}else{
//				message("请先选择任务单！");
//			}
//		}
//	});
/*	opts.tbar.push({
		text : '下载打印',
		handler : function() {
			download();
		}
	});*/
//	opts.tbar.push({
//		text : "导出文件",
//		handler : function() {
//			exportexcel();
//		}
//	});

//	var getReportTemplateItemData = function() {
//		var modifyRecord = reportTemplateGrid.getModifyRecord();
//		var itemDataJson = [];
//		if (modifyRecord && modifyRecord.length > 0) {
//			$.each(modifyRecord, function(i, obj) {
//				var data = {};
//				data.id = obj.get("id");
//				data["blood-id"] = obj.get("blood-id");
//				data["blood-code"] = obj.get("blood-code");
//				data["judgMent-id"] = obj.get("judgMent-id");
//				data["judgMent-name"] = obj.get("judgMent-name");
//				data.state = obj.get("state");
//				data.poolId = obj.get("poolId");
//				data.completeDate = obj.get("completeDate");
//				data["reportFile-id"] = obj.get("reportFile-id");
//				data["reportFile-fileName"] = obj.get("reportFile-fileName");
//				data.expressCompany = obj.get("expressCompany");
//				data.emsId = obj.get("emsId");
//				data.arc = obj.get("arc");
//				data.note= obj.get("note");
//				data.remark = obj.get("remark");
//				data["blood-sequencingName"] = obj.get("blood-sequencingName");
//				data["sampleBloodReceive-id"] = obj.get("sampleBloodReceive-id");
//				data.reportResult = obj.get("reportResult");
//				data.reportResultId = obj.get("reportResultId");
//				data["crmProduct-id"] = obj.get("crmProduct-id");
//				data["crmProduct-name"] = obj.get("crmProduct-name");
//				data["reti-id"] = obj.get("reti-id");
//				data["reti-name"] = obj.get("reti-name");
//				data["reti-note"] = obj.get("reti-note");
//				data.disease = obj.get("disease");
//				itemDataJson.push(data);
//			});
//			return JSON.stringify(itemDataJson);
//		} else {
//			return "";
//		}
//	};

	reportTemplateGrid = gridEditTable("report_tem_grid_div", cols, loadParam, opts);
	$("#report_tem_grid_div").data("reportTemplateGrid", reportTemplateGrid);
});

//function exportexcel() {
//	reportTemplateGrid.title = '导出列表';
//	var vExportContent = reportTemplateGrid.getExcelXml();
//	var x = document.getElementById('gridhtm');
//	x.value = vExportContent;
//	document.excelfrm.submit();
//}
//
//function openW(id){
//	window.open(window.ctx + '/sample/blood/main/toEditSampleMain.action?noButton=true&reqMethodType=view&id='+id,'','height=768,width=1366,scrollbars=yes,resizable=yes');
//}
//function downFile(id,bloodId){
//	window.open(window.ctx+"/sample/report/toReportEdit.action?id="+id+"&bloodId="+bloodId,'','height=768,width=1366,scrollbars=yes,resizable=yes');
//}
//function downFiles(id,bloodId){
//	window.open(window.ctx+"/sample/report/toReportEdits.action?id="+id+"&bloodId="+bloodId,'','height=768,width=1366,scrollbars=yes,resizable=yes');
//}
//function getAllSel(){
//	var names = document.getElementsByName("sel");
//	var ids='';
//	if(names){
//		for(i=0;i<names.length;i++){
//			if(names[i].checked){
//				ids = ids+"'"+names[i].value+"',";
//			}
//		}
//		if(ids.length>0){
//			ids = ids.substring(0,ids.length-1);
//		}
//	}
//	return ids;
//}

//function checkAll(str){
//	if($("#all").val()=='0'){
//		var a= document.getElementsByName(str);
//		 var n=a.length;
//		 for(var i=0;i<n;i++)
//		 a[i].checked = "checked";
//		 $("#all").val("1");
//	}else{
//		var a= document.getElementsByName(str);
//		 var n=a.length;
//		 for(var i=0;i<n;i++)
//		 a[i].checked = "";
//		 $("#all").val("0");
//	}
//}
//
//function setReportFun(rec) {
//	var records = reportTemplateGrid.getSelectRecord();
//		$.each(records, function(i, obj) {
//			obj.set('reti-id', rec.get('id'));
//			obj.set('reti-name', rec.get('name'));
//			obj.set('reti-note', rec.get('note'));
//		});
//	var win = Ext.getCmp('showsampleReportSelectList');
//	if (win) {
//		win.close();
//	}
//}
//function sampleUnitSelectFun() {
//	var record = inItemGrid.getSelectionModel().getSelected();
//	showsampleUnitSelectList();
//}

//function TechDnaServiceTaskFun(){
//	var win = Ext.getCmp('TechDnaServiceTaskFun');
//	if (win) {win.close();}
//	var TechDnaServiceTaskFun= new Ext.Window({
//	id:'TechDnaServiceTaskFun',modal:true,title:'选择任务单',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/experiment/dna/experimentDnaGet/showDnaTechTaskDialog.action?flag=TechDnaServiceTaskFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//	 TechDnaServiceTaskFun.close(); }  }]  });     TechDnaServiceTaskFun.show(); }
//function setTechServiceDnaTaskFun(rec){
//	document.getElementById('orderId').value=rec.get('id');
//	document.getElementById('orderName').value=rec.get('name');
//	$("#techReportMain_orderId").val(rec.get('id'));
//	var win = Ext.getCmp('TechDnaServiceTaskFun');
//	if(win){win.close();}
//	reportTemplateGrid.store.removeAll();
//	var code=rec.get('id');
//	ajax("post", "/experiment/techreport/showReportData.action", {
//		code : code
//	}, function(data) {
//		if (data.success) {	
//			var ob = reportTemplateGrid.getStore().recordType;
//			reportTemplateGrid.stopEditing();
//			
//			$.each(data.data, function(i, obj) {
//				var p = new ob({});
//				p.isNew = true;
//				
//				p.set("id", obj.mid);
//				p.set("projectId", obj.projectId);
//				p.set("contractId", obj.contractId);
//				p.set("orderType", obj.orderType);
//				p.set("completeDate", obj.completeDate);
//				p.set("reportFile-id", obj.reportFileId);
//				p.set("reportFile-name", obj.reportFileName);
//				p.set("reportResult", obj.reportResult);
//				p.set("reportResultId", obj.reportResultId);
//				p.set("reti-id", obj.retiId);
//				p.set("reti-name", obj.retiName);
//				p.set("reti-note", obj.retiNote);
//				p.set("orederId", obj.orederId);
//				p.set("state", obj.state);
//				p.set("disease", obj.disease);
//				p.set("fileNum", obj.fileNum);
//				p.set("sampleCode", obj.sampleCode);
//				p.set("dnaCode", obj.dnaCode);
//				reportTemplateGrid.getStore().add(p);							
//			});
//			reportTemplateGrid.startEditing(0, 0);
//		} else {
//			message("获取明细数据时发生错误！");
//		}
//	}, null);
//}
////左侧页面的查询功能
//function selectReport2(){
//	var rid = $("#report_tem_grid_div").data("reportTemplateGrid");
//	alert(rid);
//	commonSearchActionByMo(reportTemplateGrid,"21");
//	alert(1);
//}
//从左边添加到右边的明细中
function addItem(){
	if($("#techReportMain_orderId").val()!=""){
		var selRecord = reportTemplateGrid.getSelectionModel().getSelections();//从左边获取数据
		var getRecord = techReportItemGrid.store;//填充到当前的明细中
		if(selRecord.length >0){
			$.each(selRecord,function(i, obj){
				var isRepeat = false;
				for(var j=0; j<getRecord.getCount();j++){
					var getData = getRecord.getAt(j).get("sampleCode");
					if(getData==obj.get("sampleCode")){
						isRepeat = true;
						message("有重复的数据，请重新选择！");
						break;
					}
				}
				if(!isRepeat){
					var ob = techReportItemGrid.getStore().recordType;
					techReportItemGrid.stopEditing();
					var p= new ob({});
					p.isNew = true;
					p.set("sampleCode",obj.get("sampleCode"));
					p.set("code",obj.get("dnaCode"));
					p.set("productId",obj.get("projectId"));
					//p.set("productName",obj.get("productName"));
					p.set("contentId",obj.get("contractId"));
					p.set("orderId",obj.get("orderId"));
					p.set("orderType",obj.get("orderType"));
					techReportItemGrid.getStore().add(p);
				}
				
			});
			techReportItemGrid.startEditing(0,0);
		}else{
			message("请选择样本添加！");
		}
	}else{
		message("请先选择任务单！");
	}
}