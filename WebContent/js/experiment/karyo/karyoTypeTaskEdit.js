$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#karyoTypeTask_state").val()==3){
		load("/experiment/karyo/karyoTypeTask/showKaryoTypeTaskTempList.action", {
			id : $("#karyoTypeTask_id").val()
		}, "#karyoTypeTaskTemppage");
		$("#markup").css("width","70%");
	}else{
		$("#showtemplate").css("display","none");
		$("#karyoTypeTaskTemppage").css("display","none");
		$("#markup").css("width","100%");
	}
	setTimeout(function() {
		var getGrid=karyoTypeTaskTemplateGrid.store;
		if(getGrid.getCount()==0){
			//alert(getGrid.getCount());
			loadTemplate($("#karyoTypeTask_template").val());
		}
	}, 2000);
});	
function add() {
	window.location = window.ctx + "/experiment/karyo/karyoTypeTask/editKaryoTypeTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/karyo/karyoTypeTask/showKaryoTypeTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
//	var type=$("#karyoTypeTask_sampleType").val();
	var template=$("#karyoTypeTask_template").val();
//	if(type==""||type==null||type==undefined){
//		message("请选择样本类型！");
//		return;
//	}
	if(template==""||template==null||template==undefined){
		message(biolims.common.selectTaskModel);
		return;
	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("KaryoTypeTask", {
					userId : userId,
					userName : userName,
					formId : $("#karyoTypeTask_id").val(),
					title : $("#karyoTypeTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	var codeList = new Array();
	var flag=true;
	var flag1=true;
	if (karyoTypeTaskHarvestGrid.getAllRecord().length > 0) {
		var selRecord = karyoTypeTaskHarvestGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			codeList.push(selRecord.getAt(j).get("sampleCode"));
			if(selRecord.getAt(j).get("isrStandard")==""){
				message(biolims.common.resultsIsEmpty);
				return;
			}
			if(selRecord.getAt(j).get("nextFlowId")==""){
				message(biolims.common.nextStepNotEmpty);
				return;
			}
		}
		if(karyoTypeTaskHarvestGrid.getModifyRecord().length > 0){
			message(biolims.common.pleaseSaveRecord);
			return;
		}
		
			
				var taskId =  $(this).attr("taskId");
				completeTask($("#karyoTypeTask_id").val(), taskId, function() {
					document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/dashboard/toDashboard.action';
				});
			
		
}
});






function save() {
if(checkSubmit()==true){
	    var karyoTypeTaskItemDivData = $("#karyoTypeTaskItemdiv").data("karyoTypeTaskItemGrid");
		document.getElementById('karyoTypeTaskItemJson').value = commonGetModifyRecords(karyoTypeTaskItemDivData);
	    var karyoTypeTaskTemplateDivData = $("#karyoTypeTaskTemplatediv").data("karyoTypeTaskTemplateGrid");
		document.getElementById('karyoTypeTaskTemplateJson').value = commonGetModifyRecords(karyoTypeTaskTemplateDivData);
	    var karyoTypeTaskAgentiaDivData = $("#karyoTypeTaskAgentiadiv").data("karyoTypeTaskAgentiaGrid");
		document.getElementById('karyoTypeTaskAgentiaJson').value = commonGetModifyRecords(karyoTypeTaskAgentiaDivData);
	    var karyoTypeTaskCosDivData = $("#karyoTypeTaskCosdiv").data("karyoTypeTaskCosGrid");
		document.getElementById('karyoTypeTaskCosJson').value = commonGetModifyRecords(karyoTypeTaskCosDivData);
	    var karyoTypeTaskHarvestDivData = $("#karyoTypeTaskHarvestdiv").data("karyoTypeTaskHarvestGrid");
		document.getElementById('karyoTypeTaskHarvestJson').value = commonGetModifyRecords(karyoTypeTaskHarvestDivData);
//	    var karyoTypeTaskTempDivData = $("#karyoTypeTaskTempdiv").data("karyoTypeTaskTempGrid");
//		document.getElementById('karyoTypeTaskTempJson').value = commonGetModifyRecords(karyoTypeTaskTempDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/karyo/karyoTypeTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : biolims.common.beingProcessed,
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/karyo/karyoTypeTask/copyKaryoTypeTask.action?id=' + $("#karyoTypeTask_id").val();
}

$("#toolbarbutton_status").click(function(){
	if ($("#karyoTypeTask_id").val()){
		commonChangeState("formId=" + $("#karyoTypeTask_id").val() + "&tableId=KaryoTypeTask");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#karyoTypeTask_id").val());
	nsc.push(biolims.user.NoNull);
	fs.push($("#karyoTypeTask_acceptUser").val());
	nsc.push(biolims.common.createUserEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.sampleInoculation,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/karyo/karyoTypeTask/showKaryoTypeTaskItemList.action", {
				id : $("#karyoTypeTask_id").val()
			}, "#karyoTypeTaskItempage");
load("/experiment/karyo/karyoTypeTask/showKaryoTypeTaskTemplateList.action", {
				id : $("#karyoTypeTask_id").val()
			}, "#karyoTypeTaskTemplatepage");
load("/experiment/karyo/karyoTypeTask/showKaryoTypeTaskAgentiaList.action", {
				id : $("#karyoTypeTask_id").val()
			}, "#karyoTypeTaskAgentiapage");
load("/experiment/karyo/karyoTypeTask/showKaryoTypeTaskCosList.action", {
				id : $("#karyoTypeTask_id").val()
			}, "#karyoTypeTaskCospage");
load("/experiment/karyo/karyoTypeTask/showKaryoTypeTaskHarvestList.action", {
				id : $("#karyoTypeTask_id").val()
			}, "#karyoTypeTaskHarvestpage");
load("/experiment/karyo/karyoTypeTask/showKaryoTypeTaskTempList.action", {
				id : $("#karyoTypeTask_id").val()
			}, "#karyoTypeTaskTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
			    	text: '复制'
					});
item.on('click', editCopy);


/**
 * 调用模板
 */
function TemplateFun(){
	var type="doCka";
	var win = Ext.getCmp('TemplateFun');
	if (win) {win.close();}
	var TemplateFun= new Ext.Window({
	id:'TemplateFun',modal:true,title:'Select Template',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
	 TemplateFun.close(); }  }]  }); 
	 TemplateFun.show(); 
}
function setTemplateFun(rec){
	document.getElementById('karyoTypeTask_acceptUser').value=rec.get('acceptUser-id');
	document.getElementById('karyoTypeTask_acceptUser_name').value=rec.get('acceptUser-name');
	//把实验模板中的中间产物类型和数量带入
	var itemGrid=karyoTypeTaskItemGrid.store;
	if(itemGrid.getCount()>0){
		for(var i=0;i<itemGrid.getCount();i++){
			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
		}
	}
	var code=$("#karyoTypeTask_template").val();
	if(code==""){
	document.getElementById('karyoTypeTask_template').value=rec.get('id');
	document.getElementById('karyoTypeTask_template_name').value=rec.get('name');
	var win = Ext.getCmp('TemplateFun');
	if(win){win.close();}
	var id=rec.get('id');
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoTypeTaskTemplateGrid.getStore().recordType;
				karyoTypeTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("stepNum",obj.code);
					p.set("stepDescribe",obj.name);
					p.set("note",obj.note);
					karyoTypeTaskTemplateGrid.getStore().add(p);							
					karyoTypeTaskTemplateGrid.startEditing(0, 0);		
				});
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoTypeTaskAgentiaGrid.getStore().recordType;
				karyoTypeTaskAgentiaGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					karyoTypeTaskAgentiaGrid.getStore().add(p);							
					karyoTypeTaskAgentiaGrid.startEditing(0, 0);		
				});
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoTypeTaskCosGrid.getStore().recordType;
				karyoTypeTaskCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("instrumentCode",obj.code);
					p.set("instrumentName",obj.name);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					karyoTypeTaskCosGrid.getStore().add(p);							
					karyoTypeTaskCosGrid.startEditing(0, 0);		
				});			
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
	}else{
		if(rec.get('id')==code){
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
	}else{
		var ob1 = karyoTypeTaskTemplateGrid.store;
		if (ob1.getCount() > 0) {
			for(var j=0;j<ob1.getCount();j++){
				var oldv = ob1.getAt(j).get("id");
				//根据ID删除
				if(oldv!=null){
					ajax("post", "/experiment/karyo/karyoTypeTask/delKaryoTypeTaskTemplateOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message(biolims.common.deleteSuccess);
						} else {
							message(biolims.common.deleteFailed);
						}
					}, null);
				}else{								
					karyoTypeTaskTemplateGrid.store.removeAll();
				}
			}
			karyoTypeTaskTemplateGrid.store.removeAll();
		}
		var ob2 = karyoTypeTaskAgentiaGrid.store;
		if (ob2.getCount() > 0) {
			for(var j=0;j<ob2.getCount();j++){
				var oldv = ob2.getAt(j).get("id");
				if(oldv!=null){
					ajax("post", "/experiment/karyo/karyoTypeTask/delKaryoTypeTaskReagentOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message(biolims.common.deleteSuccess);
						} else {
							message(biolims.common.deleteFailed);
						}
					}, null); 
				}else{
					karyoTypeTaskAgentiaGrid.store.removeAll();
				}
			}
			karyoTypeTaskAgentiaGrid.store.removeAll();
		}
		var ob3 = karyoTypeTaskCosGrid.store;
		if (ob3.getCount() > 0) {
			for(var j=0;j<ob3.getCount();j++){
				var oldv = ob3.getAt(j).get("id");
				if(oldv!=null){
					ajax("post", "/experiment/karyo/karyoTypeTask/delKaryoTypeTaskCosOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message(biolims.common.deleteSuccess);
						} else {
							message(biolims.common.deleteFailed);
						}
					}, null); 
				}else{
					karyoTypeTaskCosGrid.store.removeAll();
				}
			}
			karyoTypeTaskCosGrid.store.removeAll();
		}
		//experiment/karyo/karyoTypeTask
		document.getElementById('karyoTypeTask_template').value=rec.get('id');
		document.getElementById('karyoTypeTask_template_name').value=rec.get('name');
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
		var id = rec.get('id');
		ajax("post", "/system/template/template/setTemplateItem.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoTypeTaskTemplateGrid.getStore().recordType;
					karyoTypeTaskTemplateGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tItem",obj.id);
						p.set("stepNum",obj.code);
						p.set("stepDescribe",obj.name);
						p.set("note",obj.note);
						karyoTypeTaskTemplateGrid.getStore().add(p);							
						karyoTypeTaskTemplateGrid.startEditing(0, 0);		
					});
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateReagent.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoTypeTaskAgentiaGrid.getStore().recordType;
					karyoTypeTaskAgentiaGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tReagent",obj.id);
						p.set("agentiaCode",obj.code);
						p.set("agentiaName",obj.name);
						p.set("batch",obj.batch);
						p.set("isCheck",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("singleDosage",obj.num);
						p.set("note",obj.note);
						p.set("sn",obj.sn);
						karyoTypeTaskAgentiaGrid.getStore().add(p);							
						karyoTypeTaskAgentiaGrid.startEditing(0, 0);		
					});
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateCos.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoTypeTaskCosGrid.getStore().recordType;
					karyoTypeTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tCos",obj.id);
						p.set("instrumentCode",obj.code);
						p.set("instrumentName",obj.name);
						p.set("isCheck",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("time",obj.time);
						p.set("note",obj.note);
						karyoTypeTaskCosGrid.getStore().add(p);							
						karyoTypeTaskCosGrid.startEditing(0, 0);		
					});			
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null); 
		}
	}
}

/**
 * 按条件加载原辅料
 */
function showReagent(){
	//获取全部数据
	var allRcords=karyoTypeTaskTemplateGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message(biolims.common.pleaseHold);
		return;
	}
	//获取选择的数据
	var selectRcords=karyoTypeTaskTemplateGrid.getSelectionModel().getSelections();	
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#karyoTypeTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/karyo/karyoTypeTask/showkaryoTypeTaskReagentList.action", {
			id : $("#karyoTypeTask_id").val()
		}, "#karyoTypeTaskAgentiapage");
	}else if(length1==1){
		karyoTypeTaskAgentiaGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/karyo/karyoTypeTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = karyoTypeTaskAgentiaGrid.getStore().recordType;
				karyoTypeTaskAgentiaGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("sn",obj.sn);
					p.set("reactionDosage",obj.reactionDosage);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("dosage",obj.dosage);
					p.set("tReagent",obj.tReagent);
					p.set("karyoTypeTask-id",obj.tId);
					p.set("karyoTypeTask-name",obj.tName);
					
					karyoTypeTaskAgentiaGrid.getStore().add(p);							
				});
				karyoTypeTaskAgentiaGrid.startEditing(0, 0);		
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
		});		
	}else{
		message(biolims.common.pleaseSelectAPieceOfData);
		return;
	}
	
}

/**
 * 按条件加载设备
 */
function showCos(){
	//获取全部数据
	var allRcords=karyoTypeTaskTemplateGrid.store;
	var flag="1";
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag="0";
		}else{
			flag="1";
		}
	}
	if(flag=="0"){
		message(biolims.common.pleaseHold);
		return;
	}else{
		//获取选择的数据
		var selectRcords=karyoTypeTaskTemplateGrid.getSelectionModel().getSelections();
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid=$("#karyoTypeTask_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/karyo/karyoTypeTask/showkaryoTypeTaskCosList.action", {
				id : $("#karyoTypeTask_id").val()
			}, "#karyoTypeTaskCospage");
		}else if(length1==1){
			karyoTypeTaskCosGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/karyo/karyoTypeTask/setCos.action", {
				tid:tid,code : code
			}, function(data) {
				
				if (data.success) {	
					var ob = karyoTypeTaskCosGrid.getStore().recordType;
					karyoTypeTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						
						p.set("id",obj.id);
						p.set("instrumentCode",obj.instrumentCode);
						p.set("instrumentName",obj.instrumentName);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("sampleNum",obj.sampleNum);
						p.set("time",obj.time);
						p.set("isCheck",obj.isCheck);
						p.set("itemId",obj.itemId);
						p.set("tCos",obj.tCos);
						p.set("karyoTypeTask-id",obj.tId);
						p.set("karyoTypeTask-name",obj.tName);
						p.set("note",obj.note);
						karyoTypeTaskCosGrid.getStore().add(p);							
					});
					karyoTypeTaskCosGrid.startEditing(0, 0);		
				} else {
					message(biolims.common.anErrorOccurred);
				}
			}, null);
			});		
		}else{
			message(biolims.common.pleaseSelectAPieceOfData);
			return;
		}
	}
}

//var loadDicSampleType;
////查询样本类型
//function loadTestDicSampleType(){
//	 var options = {};
//		options.width = document.body.clientWidth-800;
//		options.height = document.body.clientHeight-40;
//		loadDicSampleType=loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
//			"确定" : function() {
//				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
//				var selectRecord = operGrid.getSelectionModel().getSelections();
//				if (selectRecord.length > 0) {
//					$.each(selectRecord, function(a, b) {
//						$("#karyoTypeTask_sampleType").val(b.get("id"));
//						$("#karyoTypeTask_sampleType_name").val(b.get("name"));
//					});
//				}else{
//					message("请选择您要选择的数据");
//					return;
//				}
//				$(this).dialog("close");
//			}
//		}, true, options);
//}
//
//function setDicSampleType(){
//
//	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
//	var selectRecord = operGrid.getSelectionModel().getSelections();
//	if (selectRecord.length > 0) {
//		$.each(selectRecord, function(a, b) {
//			$("#karyoTypeTask_sampleType").val(b.get("id"));
//			$("#karyoTypeTask_sampleType_name").val(b.get("name"));
//		});
//	}else{
//		message("请选择您要选择的数据");
//		return;
//	}
//
//	loadDicSampleType.dialog("close");
//
//}

Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = karyoTypeTaskHarvestGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("isCommit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}
			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("isCommit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.common.save
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	alert($("#karyoTypeTask_id").val());
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "KaryoTypeTask",id : $("#karyoTypeTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoTypeTaskTemplateGrid.getStore().recordType;
				karyoTypeTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("stepNum",obj.code);
					p.set("stepDescribe",obj.name);
					p.set("note",obj.note);
					karyoTypeTaskTemplateGrid.getStore().add(p);							
					karyoTypeTaskTemplateGrid.startEditing(0, 0);		
				});
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoTypeTaskAgentiaGrid.getStore().recordType;
				karyoTypeTaskAgentiaGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					karyoTypeTaskAgentiaGrid.getStore().add(p);							
					karyoTypeTaskAgentiaGrid.startEditing(0, 0);		
				});
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoTypeTaskCosGrid.getStore().recordType;
				karyoTypeTaskCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("instrumentCode",obj.code);
					p.set("instrumentName",obj.name);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					karyoTypeTaskCosGrid.getStore().add(p);							
					karyoTypeTaskCosGrid.startEditing(0, 0);		
				});			
			} else {
				message(biolims.common.anErrorOccurred);
			}
		}, null);
}