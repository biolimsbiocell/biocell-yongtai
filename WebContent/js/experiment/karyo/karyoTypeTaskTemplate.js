var karyoTypeTaskTemplateGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'stepNum',
		type:"string"
	});
	   fields.push({
		name:'stepDescribe',
		type:"string"
	});
	   fields.push({
		name:'experimentUser-id',
		type:"string"
	});
	   fields.push({
		name:'experimentUser-name',
		type:"string"
	});
	   fields.push({
		name:'startDate',
		type:"string"
	});
	   fields.push({
		name:'endDate',
		type:"string"
	});
	   fields.push({
		name:'patientName',
		type:"string"
	});
	   fields.push({
		name:'sampleCodes',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'karyotypeTask-id',
		type:"string"
	});
	    fields.push({
		name:'karyotypeTask-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'tItem',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.stepNum,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.stepName,
		width:20*6
	});
	cm.push({
		dataIndex:'testUser-id',
		hidden : true,
		header:'实验员id',
		width:20*6,
	});
	var testUser =new Ext.form.TextField({
        allowBlank: false
	});
	testUser.on('focus', function() {
		loadTestUser();
	});
	cm.push({
		dataIndex:'testUser-name',
		hidden : false,
		header:biolims.common.testUserName,
		width:20*6,
		
		editor : testUser
	});
	cm.push({
		dataIndex:'startTime',
		header:biolims.common.startTime,
		width:20*6
	});
	cm.push({
		dataIndex:'endTime',
		header:biolims.common.endTime,
		width:20*6
	});

	cm.push({
		dataIndex:'codes',
		hidden : false,
		header:biolims.common.relateSample,
		width:40*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'samplePreTask-id',
		hidden : true,
		header:'关联主表ID',
		width:50*10
	});
	cm.push({
		dataIndex:'samplePreTask-name',
		hidden : true,
		header:'关联主表',
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyo/karyoTypeTask/showKaryoTypeTaskTemplateListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.templateDetail;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyo/karyoTypeTask/delKaryoTypeTaskTemplate.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				karyoTypeTaskTemplateGrid.getStore().commitChanges();
				karyoTypeTaskTemplateGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'add',
		text : biolims.common.fillDetail,
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_start',
		text :biolims.common.applicationStart,
		handler : getStartTime
	});
	opts.tbar.push({
		iconCls : 'application_end',
		text : biolims.common.applicationEnd,
		handler : getEndTime
	});
	opts.tbar.push({
		iconCls : 'application_oper',
		text :biolims.common.applicationOper,
		handler : addSuccess
	});
	opts.tbar.push({
		iconCls : 'application_print',
		text : biolims.common.printList,
		handler : stampOrder
	});
	karyoTypeTaskTemplateGrid=gridEditTable("karyoTypeTaskTemplatediv",cols,loadParam,opts);
	$("#karyoTypeTaskTemplatediv").data("karyoTypeTaskTemplateGrid", karyoTypeTaskTemplateGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//查询实验员
function loadTestUser(){
	var win = Ext.getCmp('loadTestUser');
	if (win) {win.close();}
	var loadTestUser= new Ext.Window({
	id:'loadTestUser',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/core/user/userSelect.action?flag=loadTestUser' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 loadTestUser.close(); }  }]  });     loadTestUser.show(); 
}
function setloadTestUser(id,name){
	var selRecords = karyoTypeTaskTemplateGrid.getSelectionModel().getSelections(); 
	$.each(selRecords, function(i, obj) {
		obj.set('experimentUser-id',id);
		obj.set('experimentUser-name',name);
	});
	var win = Ext.getCmp('loadTestUser');
	if(win){
		win.close();
	}
}


//选择实验步骤
//function templateSelect(){
//	var option = {};
//	option.width = 605;
//	option.height = 558;
//	loadDialogPage(null, "选择实验步骤", "/experiment/karyo/karyoTypeTask/showTemplateWaitList.action?id="+$("#id_parent_hidden").val(), {
//		"确定" : function() {
//			var operGrid = $("#template_wait_grid_div").data("grid");
//			var ob = karyoTypeTaskTemplateGrid.getStore().recordType;
//			karyoTypeTaskTemplateGrid.stopEditing();
//			var selectRecord = operGrid.getSelectionModel().getSelections();
//			if (selectRecord.length > 0) {
//				$.each(selectRecord, function(i, obj) {
//					var p = new ob({});
//					p.isNew = true;
//					p.set("stepNum", obj.get("code"));
//					p.set("stepDescribe", obj.get("name"));
//					karyoTypeTaskTemplateGrid.getStore().add(p);
//					karyoTypeTaskTemplateGrid.startEditing(0, 0);
//				});
//				$(this).dialog("close");
//				$(this).dialog("remove");
//			} else {
//				message("请选择您要选择的数据");
//				return;
//			}
//		}
//	}, true, option);
//}


//获取开始时的时间
function getStartTime(){
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=karyoTypeTaskTemplateGrid.getSelectionModel();
	//var setNum = bloodSplitReagentGrid.store;
	var selectRecords = karyoTypeTaskItemGrid.getSelectionModel();
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			obj.set("startDate",str);
			//将所选样本的数量，放到原辅料样本数量处
//			for(var i=0; i<setNum.getCount();i++){
//				var num = setNum.getAt(i).get("itemId");
//				if(num==obj.get("code")){
//					setNum.getAt(i).set("sampleNum",selectRecords.getSelections().length);
//				}
//			}
		});
	}else{
		message(biolims.common.selectStepNum);
	}
	var selRecord=karyoTypeTaskTemplateGrid.getSelectRecord();
	var codes = "";
	$.each(selectRecords.getSelections(), function(i, obj) {
		if(obj.get("code")!=null && obj.get("code")!=""){
			codes += obj.get("code")+",";
		}else{
			codes += obj.get("sampleCode")+",";
		}
	});
	$.each(selRecord,function(i,obj){
		obj.set("sampleCodes",codes);
	});
}

//获取停止时的时间
function getEndTime(){
	var setRecord=karyoTypeTaskItemGrid.store;
	var getIndex = karyoTypeTaskTemplateGrid.store;
	var getIndexs = karyoTypeTaskTemplateGrid.getSelectionModel().getSelections();
	var d = new Date();
	var str = d.getFullYear()+"/"+(d.getMonth()+1)+"/"+d.getDate()+"/"+d.getHours()+":"+d.getMinutes();
	var selectRecord=karyoTypeTaskTemplateGrid.getSelectionModel();
	if (selectRecord.getSelections().length > 0) {
		$.each(selectRecord.getSelections(), function(i, obj) {
			if(obj.get("startDate")!=""){
				obj.set("endDate",str);
				var codes = obj.get("sampleCodes");
				var scode = new Array();
				scode = codes.split(",");
				
				for(var i=0; i<setRecord.getCount(); i++){
					for(var j=0; j<scode.length; j++){
						if(setRecord.getAt(i).get("code")!=null
								&& setRecord.getAt(i).get("code")!=""){
							if(scode[j]==setRecord.getAt(i).get("code")){
								setRecord.getAt(i).set("stepNum",obj.get("code"));
							}
						}else{
							if(scode[j]==setRecord.getAt(i).get("sampleCode")){
								setRecord.getAt(i).set("stepNum",obj.get("code"));
							}
						}
					}
				}
				getIndex.getAt(getIndex.indexOfId(getIndexs[0].get("id"))+1).set("sampleCodes",codes);
			}else{
				message("请先开始实验！");
			}
		});
	}else{
		message(biolims.common.selectStepNum);
	}
}

function addSuccess(){
	//选中的item数据
	var getRecord = karyoTypeTaskItemGrid.getSelectionModel().getSelections();
	//选中的template数据
	var selectRecord = karyoTypeTaskTemplateGrid.getSelectionModel().getSelections();
	//template明细的所有数据
	var getTemplateAll=karyoTypeTaskTemplateGrid.store;
	//item明细的所有数据
	var getItemAll=karyoTypeTaskItemGrid.store;
	//result的所有数据
	var getResultAll=karyoTypeTaskHarvestGrid.store;
	var isNull=false;
	var isNull1=false;
	var isNull2=false;
	if(getItemAll.getCount()>0){
		for(var h=0;h<getItemAll.getCount();h++){
			var tid= getItemAll.getAt(h).get("dicSampleType-id");
			if(tid == null || tid == ""){
				isNull2 = true;
				message(biolims.common.pleaseFillDic);
				break;					
			}
	
			var num=getItemAll.getAt(h).get("productNum");
			if(num==null || num=="" || num==0){
				isNull1=true;
				message(biolims.common.pleaseFillDicNum);
				break;
			}
		}
//		for(var h=0;h<getTemplateAll.getCount();h++){
//			var nulls = getTemplateAll.getAt(h).get("endDate");
//			if(nulls==null || nulls == ""){
//				isNull = true;
//				message("有未做实验的步骤！");
//				break;					
//			}
//		}
		if(isNull==false){
			if(selectRecord.length==0){
				//如果没有选中实验步骤，默认所有明细都生成结果
				var isCF=false;
				if(getResultAll.getCount()>0){
					for(var i=0; i<getItemAll.getCount(); i++){
						for(var j=0;j<getResultAll.getCount();j++){
							var itemCode = getItemAll.getAt(i).get("sampleCode");
							var infoCode = getResultAll.getAt(j).get("sampleCode");
							var itemCode1 = getItemAll.getAt(i).get("code");
							var infoCode1 = getResultAll.getAt(j).get("zjCode");
							if(itemCode1!=null && itemCode1!=""){
								if(itemCode1 == infoCode1){
									isCF = true;
									message(biolims.common.dataRepeat);
									break;					
								}
							}else{
								if(itemCode == infoCode){
									isCF = true;
									message(biolims.common.dataRepeat);
									break;					
								}
							}
						}
					}
					if(isCF==false){
						toInfoData(getItemAll);
					}
				}else{
					toInfoData(getItemAll);
				}
			}else if(selectRecord.length==1){
				$.each(selectRecord, function(i, obj) {
					var isRepeat = true;
					var codes = obj.get("sampleCodes");
					var scode = new Array();
					scode = codes.split(",");
					for(var i1=0; i1<scode.length; i1++){
						for(var j1=0;j1<getResultAll.getCount();j1++){
							var getv = scode[i1];
							var setv = getResultAll.getAt(j1).get("sampleCode");
							var setv1 = getResultAll.getAt(j1).get("zjCode");
							if(setv1!=null && setv1!=""){
								if(getv == setv1){
									isRepeat = false;
									message(biolims.common.dataRepeat);
									break;					
								}
							}else{
								if(getv == setv){
									isRepeat = false;
									message(biolims.common.dataRepeat);
									break;					
								}
							}
						}
					}
					if(isRepeat){
						for(var i=0; i<scode.length; i++){
							for(var j=0; j<getItemAll.getCount(); j++){
								if(getItemAll.getAt(j).get("code")!=null && getItemAll.getAt(j).get("code")!=""){
									if(scode[i]==getItemAll.getAt(j).get("code")){
										for(var k=1;k<=getItemAll.getAt(j).get("productNum");k++){
											var ob = karyoTypeTaskHarvestGrid.getStore().recordType;
											karyoTypeTaskHarvestGrid.stopEditing();
											var p = new ob({});
											
											
											
											p.isNew = true;
											p.set("tempId",getItemAll.getAt(j).get("tempId"));
											p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
											p.set("zjCode",getItemAll.getAt(j).get("code"));
											p.set("productId",getItemAll.getAt(j).get("productId"));
											p.set("productName",getItemAll.getAt(j).get("productName"));
											p.set("dicSampleType-id",getItemAll.getAt(j).get("dicSampleType-id"));
											p.set("dicSampleType-name",getItemAll.getAt(j).get("dicSampleType-name"));
											p.set("orderId",getItemAll.getAt(j).get("orderId"));
											p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
											p.set("experimentCode",getItemAll.getAt(j).get("experimentCode"));
											p.set("isrStandard","1");
											p.set("harversUser",$("#user").val());
											p.set("harvestDate",getItemAll.getAt(j).get("inoculateDate"));//接种时间
											p.set("preReapDate",getItemAll.getAt(j).get("preReapDate"));//预计收获时间
											p.set("acceptDate",getItemAll.getAt(j).get("acceptDate"));//接受日期
											
											ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
												model : "KaryoTypeTask",productId:getItemAll.getAt(j).get("productId")
											}, function(data) {
												p.set("nextFlowId",data.dnextId);
												p.set("nextFlow",data.dnextName);
											}, null);
											message(biolims.common.generateResultsSuccess);
											karyoTypeTaskHarvestGrid.getStore().add(p);
											karyoTypeTaskHarvestGrid.startEditing(0,0);
										}
									}
								}else{
									if(scode[i]==getItemAll.getAt(j).get("sampleCode")){
										for(var k=1;k<=getItemAll.getAt(j).get("productNum");k++){
											var ob = karyoTypeTaskHarvestGrid.getStore().recordType;
											karyoTypeTaskHarvestGrid.stopEditing();
											var p = new ob({});
											p.isNew = true;
											p.set("tempId",getItemAll.getAt(j).get("tempId"));
											p.set("sampleCode",getItemAll.getAt(j).get("sampleCode"));
											p.set("zjCode",getItemAll.getAt(j).get("code"));
											p.set("productId",getItemAll.getAt(j).get("productId"));
											p.set("productName",getItemAll.getAt(j).get("productName"));
											p.set("dicSampleType-id",getItemAll.getAt(j).get("dicSampleType-id"));
											p.set("dicSampleType-name",getItemAll.getAt(j).get("dicSampleType-name"));
											p.set("orderId",getItemAll.getAt(j).get("orderId"));
											p.set("sampleType",getItemAll.getAt(j).get("sampleType"));
											p.set("experimentCode",getItemAll.getAt(j).get("experimentCode"));
											p.set("isrStandard","1");
											p.set("harversUser",$("#user").val());
											p.set("inoculateDate",getItemAll.getAt(j).get("inoculateDate"));//接种时间
											p.set("preReapDate",getItemAll.getAt(j).get("preReapDate"));//预计收获时间
											p.set("acceptDate",getItemAll.getAt(j).get("acceptDate"));//接受日期
											ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
												model : "KaryoTypeTask",productId:getItemAll.getAt(j).get("productId")
											}, function(data) {
												p.set("nextFlowId",data.dnextId);
												p.set("nextFlow",data.dnextName);
											}, null);
											message(biolims.common.generateResultsSuccess);
											karyoTypeTaskHarvestGrid.getStore().add(p);
											karyoTypeTaskHarvestGrid.startEditing(0,0);
										}
									}
								}
							}
						}
					}
				});
				
			}else if(selectRecord.length>1){
				message(biolims.common.pleaseNotBuz);
				return;
			}
		}
		
	}else {
		message(biolims.common.addTaskSample);
		return;
	}
}
//向Info页面传值
function toInfoData(grid){
	for(var i=0;i<grid.getCount();i++){
		var productNum=grid.getAt(i).get("productNum");
			for(var k=1;k<=productNum;k++){
				var ob = karyoTypeTaskHarvestGrid.getStore().recordType;
				karyoTypeTaskHarvestGrid.stopEditing();
				var p = new ob({});
				p.isNew = true;
				p.set("tempId",grid.getAt(i).get("tempId"));
				p.set("sampleCode",grid.getAt(i).get("sampleCode"));
				p.set("zjCode",grid.getAt(i).get("code"));
				p.set("productId",grid.getAt(i).get("productId"));
				p.set("productName",grid.getAt(i).get("productName"));
				p.set("dicSampleType-id",grid.getAt(i).get("dicSampleType-id"));
				p.set("dicSampleType-name",grid.getAt(i).get("dicSampleType-name"));
				p.set("orderId",grid.getAt(i).get("orderId"));
				p.set("sampleType",grid.getAt(i).get("sampleType"));
				p.set("experimentCode",grid.getAt(i).get("experimentCode"));
				p.set("isrStandard","1");
				p.set("harversUser",$("#user").val());	
				p.set("inoculateDate",grid.getAt(i).get("inoculateDate"));//接种时间
				p.set("preReapDate",grid.getAt(i).get("preReapDate"));//预计收获时间
				p.set("acceptDate",grid.getAt(i).get("acceptDate"));//接受日期
//				p.set("nextFlowId","0035");
//				p.set("nextFlow","样本收获");
				ajax("post", "/system/nextFlow/nextFlow/selectdnextId.action", {
					model : "KaryoTypeTask",productId:grid.getAt(i).get("productId")
				}, function(data) {
					p.set("nextFlowId",data.dnextId);
					p.set("nextFlow",data.dnextName);
				}, null);

				message(biolims.common.generateResultsSuccess);
				karyoTypeTaskHarvestGrid.getStore().add(p);
				karyoTypeTaskHarvestGrid.startEditing(0,0);
//			}
		}
	}
}


//打印执行单
function stampOrder(){
	var id=$("#karyoTypeTask_template").val();
	if(id==""){
		message(biolims.common.pleaseSelectTemplate);
		return;
	}else{
		var url = '__report=KaryoTypeTask.rptdesign&id=' + $("#karyoTypeTask_id").val();
		commonPrint(url);
	}
}