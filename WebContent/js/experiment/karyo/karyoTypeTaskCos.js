var karyoTypeTaskCosGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'instrumentCode',
		type:"string"
	});
	   fields.push({
		name:'instrumentName',
		type:"string"
	});
	   fields.push({
		name:'isCheck',
		type:"string"
	});
	   fields.push({
		name:'temperature',
		type:"string"
	});
	   fields.push({
		name:'speed',
		type:"string"
	});
	   fields.push({
		name:'time',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'karyotypeTask-id',
		type:"string"
	});
	    fields.push({
		name:'karyotypeTask-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'instrumentCode',
		hidden : false,
		header:biolims.common.instrumentNo,
		width:20*6
	});
	cm.push({
		dataIndex:'instrumentName',
		hidden : false,
		header:biolims.common.instrumentName,
		width:20*6
	});
	cm.push({
		dataIndex:'isCheck',
		hidden : false,
		header:biolims.common.isTest,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.common.temperature,
		width:20*6
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:biolims.common.speed,
		width:20*6
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.common.time,
		width:20*6
	});
	cm.push({
		dataIndex:'karyotypeTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'karyotypeTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyo/karyoTypeTask/showKaryoTypeTaskCosListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.instrumentDetail;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyo/karyoTypeTask/delKaryoTypeTaskCos.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//			text : '选择相关主表',
//				handler : selectkaryotypeTaskDialogFun
//		});
//	opts.tbar.push({
//		text : "批量上传（csv文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
//	function goInExcelcsv(){
//		var file = document.getElementById("file-uploadcsv").files[0];  
//		var n = 0;
//		var ob = karyoTypeTaskCosGrid.getStore().recordType;
//		var reader = new FileReader();  
//		reader.readAsText(file,'GB2312');  
//		reader.onload=function(f){  
//			var csv_data = $.simple_csv(this.result);
//			$(csv_data).each(function() {
//                	if(n>0){
//                		if(this[0]){
//                			var p = new ob({});
//                			p.isNew = true;				
//                			var o;
//                			o= 0-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 1-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 2-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 3-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 4-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 5-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 6-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 7-1;
//                			p.set("po.fieldName",this[o]);
//                			o= 8-1;
//                			p.set("po.fieldName",this[o]);
//							karyoTypeTaskCosGrid.getStore().insert(0, p);
//                		}
//                	}
//                     n = n +1;
//                	
//                });
//    	}
//	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	karyoTypeTaskCosGrid=gridEditTable("karyoTypeTaskCosdiv",cols,loadParam,opts);
	$("#karyoTypeTaskCosdiv").data("karyoTypeTaskCosGrid", karyoTypeTaskCosGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function selectkaryotypeTaskFun(){
	var win = Ext.getCmp('selectkaryotypeTask');
	if (win) {win.close();}
	var selectkaryotypeTask= new Ext.Window({
	id:'selectkaryotypeTask',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectkaryotypeTask.close(); }  }]  }) });  
    selectkaryotypeTask.show(); }
	function setkaryotypeTask(rec){
		var gridGrid = $("#karyoTypeTaskCosdiv").data("karyoTypeTaskCosGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('karyotypeTask-id',rec.get('id'));
			obj.set('karyotypeTask-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectkaryotypeTask')
		if(win){
			win.close();
		}
	}
	function selectkaryotypeTaskDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/KaryoTypeTaskSelect.action?flag=karyotypeTask";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selkaryotypeTaskVal(this);
				}
			}, true, option);
		}
	var selkaryotypeTaskVal = function(win) {
		var operGrid = karyoTypeTaskDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#karyoTypeTaskCosdiv").data("karyoTypeTaskCosGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('karyotypeTask-id',rec.get('id'));
				obj.set('karyotypeTask-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
