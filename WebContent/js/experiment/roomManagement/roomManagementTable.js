var roomManagementTable;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": "房间编号",
		}, {
			"data": "roomName",
			"title": "房间名称",
		}, {
			"data": "name",
			"title": "备注",
		}, {
			"data": "roomState",
			"title": "房间状态",
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return "可以使用";
				}
				if(data == "0") {
					return "不可以使用";
				}
				else {
					return '';
				}
			}
		},  {
			"data": "isFull",
			"title": "占用状态",
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return "占用";
				}
				if(data == "0") {
					return "未占用";
				}
				else {
					return data;
				}
			}
		}, {
			"data": "describe",
			"title": "房间描述",
		}, {
			"data": "regionalManagement-name",
			"title": "区域",
		}, {
			"data": "createUser-name",
			"title": "创建人",
		}, {
			"data": "createDate",
			"title": "创建日期",
		}, {
			"data": "stateName",
			"title": "状态",
		},{
			"data": "testingTime",
			"title": "测试时间",
		},{
			"data": "validityPeriod",
			"title": "结果时间的有效期",
		},{
			"data": "cuntdown",
			"title": "倒计时",
		}];
		
	var options = table(true, "",
		"/experiment/roomManagement/roomManagement/showRoomManagementTableJson.action",colData , null)
	roomManagementTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(roomManagementTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/experiment/roomManagement/roomManagement/editRoomManagement.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/experiment/roomManagement/roomManagement/editRoomManagement.action?id=' + id ;
}
// 查看
function view() {
//	var id = $(".selected").find("input").val();
//	if(id == "" || id == undefined) {
//		top.layer.msg(biolims.common.selectRecord);
//		return false;
//	}
//	window.location = window.ctx +
//		'/experiment/roomManagement/roomManagement/viewRoomManagement.action?id=' + id + '&type=' + $("#order_type").val();
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "房间编号",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "房间名称",
			"type": "input",
			"searchName": "roomName"
		},
		{
			"type": "table",
			"table": roomManagementTable
		}
	];
}