var RoomTable;
$(function() {

		var colOpts = [];
		colOpts.push({
			"data" : "id",
			"title" : "房间编号",
		});
		colOpts.push({
			"data" : "roomName",
			"title" : "房间名称",
		});
		colOpts.push({
			"data" : "regionalManagement-name",
			"title" : "所在区域",
		});
		colOpts.push({
			"data" : "createUser-name",
			"title" : "创建人",
		});
		colOpts.push({
			"data": "roomState",
			"title": "房间状态",
			"render": function(data, type, full, meta) {
				if(data == "1") {
					return "可以使用";
				}
				if(data == "0") {
					return "不可以使用";
				}else {
					return '';
				}
			}
		});
		colOpts.push({
			"data": "isFull",
			"title": "占用状态",
			"render": function(data, type, full, meta) {
				if(data == "0") {
					return "未占用";
				}
				if(data == "1") {
					return "占用";
				}
				else {
					return data;
				}
			}
		});
		colOpts.push({
			"data":"centiare",
			"title":"房间面积m²"		
		});
		colOpts.push({
			"data":"stere",
			"title":"房间体积m³"
				
		});
		var tbarOpts = [];
		var addUserOptions = table(true, null,'/experiment/cell/passage/cellPassage/selectRoomTableAllJson.action?id='+$("#quId").val(),
				colOpts, tbarOpts)
		RoomTable = renderData($("#addRoomTable"), addUserOptions);
		$("#addRoomTable").on(
				'init.dt',
				function(e, settings) {
					// 清除操作按钮
					$("#addRoomTable_wrapper .dt-buttons").empty();
					$('#addRoomTable_wrapper').css({
						"padding": "0 16px"
					});
				var trs = $("#addRoomTable tbody tr");
				RoomTable.ajax.reload();
				RoomTable.on('draw', function() {
					trs = $("#addRoomTable tbody tr");
					trs.click(function() {
						$(this).addClass("chosed").siblings("tr")
							.removeClass("chosed");
					});
				});
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
				});
	
})
function query(){
	var roomName = $.trim($("#roomName").val());
	var searchItemLayerValue = {};
	var k1 = $("#roomName").attr("searchname");
	if(roomName != null && roomName != "") {
		searchItemLayerValue[k1] = "%" + roomName + "%";
	} else {
		searchItemLayerValue[k1] = roomName;
	}
	var regional = $.trim($("#regional").val());
	var k2 = $("#regional").attr("searchname");
	if(regional != null && regional != "") {
		searchItemLayerValue[k2] = "%" + regional + "%";
	} else {
		searchItemLayerValue[k2] = regional;
	}
	var id = $.trim($("#id").val());
	var k3 = $("#id").attr("searchname");
	if(id != null && id != "") {
		searchItemLayerValue[k3] = "%" + id + "%";
	} else {
		searchItemLayerValue[k3] = id;
	}
	var query = JSON.stringify(searchItemLayerValue);
	var param = {
		query: query
	};
	sessionStorage.setItem("searchContent", query);
	RoomTable.settings()[0].ajax.data = param;
	RoomTable.ajax.reload();
}
