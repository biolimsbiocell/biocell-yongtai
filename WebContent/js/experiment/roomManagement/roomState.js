var roomStateTable;
var oldroomStateChangeLog;
$(function() {
	// 加载子表
	var id = $("#roomManagement_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data":"roomCode",
		"title": "房间编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "roomCode");
	    },
		"visible": false,	
		
	});
	colOpts.push({
		"data":"stageName",
		"title": "占用实验名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "stageName");
	    },
	    "visible": false,	
	});
	colOpts.push({
		"data":"tableTypeId",
		"title": "占用实验单号",
		"createdCell": function(td) {
			$(td).attr("saveName", "tableTypeId");
	    },
	    "visible": false,	
	});
	colOpts.push({
		"data":"batch",
		"title": "批次号",
		"createdCell": function(td) {
			$(td).attr("saveName", "batch");
	    },
	});
	colOpts.push({
		"data":"name",
		"title": "患者姓名",
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
	});
	colOpts.push({
		"data":"note",
		"title": "占用实验阶段",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
	});
	colOpts.push({
		"data":"note2",
		"title": "占用实验阶段名称",
		"createdCell": function(td) {
			$(td).attr("saveName", "note2");
	    },
	});
	colOpts.push({
		"data":"acceptUser",
		"title": "操作人",
		"createdCell": function(td) {
			$(td).attr("saveName", "acceptUser");
	    },
	});
	colOpts.push({
		"data":"startDate",
		"title": "开始时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "startDate");
	    },
	});
	colOpts.push({
		"data":"endDate",
		"title": "结束时间",
		"createdCell": function(td) {
			$(td).attr("saveName", "endDate");
	    },
	});
	colOpts.push({
		"data":"roomState",
		"title": "设备状态",
		"createdCell": function(td) {
			$(td).attr("saveName", "roomState");
	    },
	});

	var handlemethod = $("#handlemethod").val();
	
	var roomStateOptions = table(true,
		id,
		'/experiment/roomManagement/roomManagement/showRoomStateTableJson.action', colOpts, tbarOpts)
	roomStateTable = renderData($("#roomStateTable"), roomStateOptions);
	roomStateTable.on('draw', function() {
		oldroomStateChangeLog = roomStateTable.ajax.json();
	});

});
