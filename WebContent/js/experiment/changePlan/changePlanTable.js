var changePlanTable;
$(function() {
	
	var colData = [{
			"data": "id",
			"title": "计划调整单号",
		}, {
			"data": "batch",
			"title": "产品批号",
		},
		{
			"data": "filtrateCode",
			"title": "筛选号",
		},
		{
			"data": "name",
			"title": "描述",
		}, {
			"data": "changeType-name",
			"title": "计划调整类型",
		}, {
			"data": "createUser-name",
			"title": "创建人",
		}, {
			"data": "createDate",
			"title": "创建日期",
		}, {
			"data": "confirmDate",
			"title": "完成日期",
		}, {
			"data": "sampleState",
			"title": "状态",
			"render" : function(data, type, full, meta) {
				if (data == "0") {
					return "在组";
				}
				if (data == "1") {
					return "剔除";
				}
				if (data == "2") {
					return "脱落";
				}
				if (data == "3") {
					return "复发";
				} 
				if (data == "4") {
					return "死亡";
				} 
				if (data == "5") {
					return "其它";
				}else {
					return "";
				}
			}
		}, {
			"data": "stateName",
			"title": "状态",
		}];
		
	var options = table(true, "",
		"/experiment/changePlan/changePlan/showChangePlanTableJson.action",colData , null)
	changePlanTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(changePlanTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		'/experiment/changePlan/changePlan/editChangePlan.action';
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/experiment/changePlan/changePlan/editChangePlan.action?id=' + id ;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/experiment/changePlan/changePlan/editChangePlan.action?id=' + id ;
//	var id = $(".selected").find("input").val();
//	if(id == "" || id == undefined) {
//		top.layer.msg(biolims.common.selectRecord);
//		return false;
//	}
//	window.location = window.ctx +
//		'/experiment/qaAudit/qaaudit/viewSampleOrder.action?id=' + id + '&type=' + $("#order_type").val();
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": "变更申请单号",
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": "产品批号",
			"type": "input",
			"searchName": "batch"
		},
		{
			"txt": "筛选号",
			"type": "input",
			"searchName": "filtrateCode"
		},
		{
			"type": "table",
			"table": changePlanTable
		}
	];
}