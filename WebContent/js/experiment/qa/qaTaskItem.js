﻿var qaTaskItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'expCode',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'rowCode',
		type:"string"
	});
	fields.push({
		name:'colCode',
		type:"string"
	});
    fields.push({
		name:'counts',
		type:"string"
	});
    fields.push({
		name:'sampleName',
		type:"string"
    });
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'sampleVolume',
		type:"string"
	});
    fields.push({
		name:'addVolume',
		type:"string"
	});
    fields.push({
		name:'sumVolume',
		type:"string"
	});
    fields.push({
		name:'indexs',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
		name:'tempId',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
    fields.push({
		name : 'orderId',
		type : "string"
	});
//	fields.push({
//		name : 'idCard',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
    fields.push({
		name:'concentration',
		type:"string"
	});
	fields.push({
		name:'result',
		type:"string"
	});
	fields.push({
		name:'reason',
		type:"string"
	});
	fields.push({
		name:'stepNum',
		type:"string"
	});
    fields.push({
	    name:'qaTask-id',
	    type:"string"
	});
	fields.push({
		name:'qaTask-name',
		type:"string"
	});
	fields.push({
		name:'orderNumber',
		type:"string"
	});
	fields.push({
		name:'projectId',
		type:"string"
	});
	fields.push({
		name:'contractId',
		type:"string"
	});
	fields.push({
		name:'orderType',
		type:"string"
	});
    fields.push({
		name:'taskId',
		type:"string"
	});  
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'productNum',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'sampleConsume',
		type:"string"
	});
    fields.push({
		name:'labCode',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
    
    
    fields.push({
		name:'eventClass',
		type:"string"
	});
    fields.push({
		name:'eventMs',
		type:"string"
	});
    fields.push({
		name:'eventXgry',
		type:"string"
	});
    fields.push({
		name:'inflSample',
		type:"string"
	});
    fields.push({
		name:'handleFf',
		type:"string"
	});
    fields.push({
		name:'handleCs',
		type:"string"
	});
	fields.push({
		name:'handleJtms',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	
	cm.push({
		dataIndex:'eventClass',
		hidden : false,
		header:'事件分类',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'eventMs',
		hidden : false,
		header:'事件描述',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'eventXgry',
		hidden : false,
		header:'事件相关人员',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'inflSample',
		hidden : false,
		header:'受影响样本',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'handleFf',
		hidden : false,
		header:'处理方法',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'handleCs',
		hidden : false,
		header:'处理措施',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'handleJtms',
		hidden : false,
		header:'处理具体描述',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	cm.push({
		dataIndex:'expCode',
		hidden : true,
		header:'实验编号',
		width:20*6
	});	
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'样本编号',
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本编号',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:'实验室样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'indexs',
		hidden : true,
		header:'Index',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:'身份证号',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:'手机号',
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		hidden : true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		hidden : true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目ID',
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样时间',
		hidden : true,
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		hidden : true,
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'关联任务单',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'stepNum',
		hidden : true,
		header:'步骤编号',
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:'中间产物类型编号',
		width:20*6
	});
	var testDicType =new Ext.form.TextField({
        allowBlank: false
	});
	testDicType.on('focus', function() {
		loadTestDicType();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		hidden : true,
		header:'中间产物类型',
		width:15*10,
		editor : testDicType
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'Name',
		hidden : true,
		header:'样本名称',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleConsume',
		hidden : true,
		header:'样本用量',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleVolume',
		hidden : true,
		header:'取样体积（μl）',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'addVolume',
		hidden : true,
		header:'补充体积（μl）',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sumVolume',
		hidden : true,
		header:'总体积（μl）',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});	
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同编号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	cm.push({
		dataIndex:'taskId',
		hidden : true,
		header:'科技服务任务单',
		width:20*6
	});	
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务 ',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:'浓度',
		width:20*6,		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var result = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '合格'
			},{
				id : '0',
				name : '不合格'
			}]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果',
		width:20*6,
		renderer: Ext.util.Format.comboRenderer(result),editor: result
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:30*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'rowCode',
		hidden : true,
		header:'行号',
		width:30*6
	});
	cm.push({
		dataIndex:'colCode',
		hidden : true,
		header:'列号',
		width:30*6
	});
	cm.push({
		dataIndex:'counts',
		hidden : true,
		header:'板号',
		width:30*6
	});
	cm.push({
		dataIndex:'qaTask-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qaTask-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cm.push({
		dataIndex:'qaTaskCode',
		hidden : true,
		header:'QA任务编号',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'orderNumber',
		hidden : true,
		header:'实验排序号',
		width:15*10
	});
	cm.push({
		dataIndex:'productNum',
		hidden : true,
		header:'中间产物数量',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : true,
		header:'接收备注',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/qaTask/showQaTaskItemListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title="QA任务明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state=$("#qaTask_stateName").val();
	if(state!="完成"){
		
		opts.tbar.push({
			text : biolims.common.fillDetail,
			iconCls : 'add',
			handler : function() {
				/*var grid=reagentTaskItemGrid.store;
				if(grid.getCount()>0){
					message("只能录入一条样本！");
					return;
				}else{
					
				}*/
				var ob = qaTaskItemGrid.getStore().recordType;
				var p = new ob({});
				p.isNew = true;
			
				qaTaskItemGrid.stopEditing();
				qaTaskItemGrid.getStore().add(p);
				qaTaskItemGrid.startEditing(0, 0);
			}
		});
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qa/qaTask/delQaTaskItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qaTaskItemGrid.getStore().commitChanges();
				qaTaskItemGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	/*opts.tbar.push({
		text : "批量数据",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_item_div"), "批量数据", null, {
				"确定" : function() {
					var records = qaTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var Consume = $("#Consume").val();
						var Volume = $("#Volume").val();
						var addVolume = $("#addVolume").val();
						qaTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("sampleConsume", Consume);
							obj.set("sampleVolume", Volume);
							obj.set("addVolume", addVolume);
						});
						qaTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : "产物类型",
		handler : loadTestDicType
	});
	opts.tbar.push({
		text : "批量产物数量",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_productNum_div"), "批量产物数量", null, {
				"确定" : function() {
					var records = qaTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var productNum = $("#productNum").val();
						qaTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("productNum", productNum);
						});
						qaTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});*/
	opts.tbar.push({
		text : '事件分类',
		handler :  function() {
			var records = qaTaskItemGrid.getSelectRecord();
			if(records.length>0){
				loadTesteventClass();
			}else{
				message(biolims.common.pleaseSelect);
			}
			
		}
	});
	opts.tbar.push({
		text : '事件描述',
		handler :function() {
			
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_qaResult1_div"), "事件描述", null, {
				"确定" : function() {
					var records = qaTaskItemGrid.getSelectRecord();
					if (records && records.length > 0) {
						var eventMs = $("#eventMs").val();
						qaTaskItemGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("eventMs", eventMs);
						});
						qaTaskItemGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	opts.tbar.push({
		text : '处理措施',
		handler : function() {
			var records = qaTaskItemGrid.getSelectRecord();
			if(records.length>0){
				loadTesthandleCs();
			}else{
				message(biolims.common.pleaseSelect);
			}
			
		}
	});
	opts.tbar.push({
		text : '处理方法',
		handler : function() {
			var records = qaTaskItemGrid.getSelectRecord();
			if(records.length>0){
				loadTesthandleff();
			}else{
				message(biolims.common.pleaseSelect);
			}
			
		}
	});
	opts.tbar.push({
		text : '处理具体描述',
		handler : function() {
			
					var options = {};
					options.width = 400;
					options.height = 300;
					loadDialogPage($("#bat_qaResult_div"), "处理具体描述", null, {
						"确定" : function() {
							var records = qaTaskItemGrid.getSelectRecord();
							if (records && records.length > 0) {
								var handleJtms = $("#handleJtms").val();
								qaTaskItemGrid.stopEditing();
								$.each(records, function(i, obj) {
									obj.set("handleJtms", handleJtms);
								});
								qaTaskItemGrid.startEditing(0, 0);
							}
							$(this).dialog("close");
						}
					}, true, options);
				}
			
			
		
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	/*opts.tbar.push({
		text : '填加明细',
		handler : null
	});*/
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	}
	qaTaskItemGrid=gridEditTable("qaTaskItemdiv",cols,loadParam,opts);
	$("#qaTaskItemdiv").data("qaTaskItemGrid", qaTaskItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectprojectFun(){
	var win = Ext.getCmp('selectproject');
	if (win) {win.close();}
	var selectproject= new Ext.Window({
	id:'selectproject',modal:true,title:'选择检测项目',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/com/biolims/system/product/productSelect.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectproject.close(); }  }]  });     selectproject.show(); }
	function setProductFun1(rec){
		var gridGrid = $("#qaTaskItemdiv").data("qaTaskItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('product-id',rec.get('id'));
			obj.set('product-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectproject');
		if(win){
			win.close();
		}
	}
	
function setRowSelected(rowIndex){
    //设置rowIndex行被选中   
	qaTaskTemplateItemGrid.getSelectionModel().selectRow(rowIndex);  
}  

var loadDicType;	
//查询样本类型
function loadTestDicType(){
	var options = {};
	options.width = document.body.clientWidth-800;
	options.height = document.body.clientHeight-40;
	loadDicType=loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
		"确定" : function() {
			var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
			var selectRecord = operGrid.getSelectionModel().getSelections();
			var records = qaTaskItemGrid.getSelectRecord();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					$.each(records, function(a, b) {
						b.set("dicSampleType-id", obj.get("id"));
						b.set("dicSampleType-name", obj.get("name"));
					});
				});
			}else{
				message("请选择您要选择的数据");
				return;
			}
			$(this).dialog("close");
		}
	}, true, options);
}

function setDicType(){
	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = qaTaskItemGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			$.each(records, function(a, b) {
				b.set("dicSampleType-id", obj.get("id"));
				b.set("dicType-name", obj.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadDicType.dialog("close");
}



var loadeventClass;
//查询样本类型
function loadTesteventClass(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadeventClass=loadDialogPage(null, biolims.common.sampleType, "/dic/type/dicTypeJquerySelect.action?flag=eventtype", {
			"Confirm" : function() {
				var operGrid = dicTypeDialogGridGrid;
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qaTaskItemGrid.getSelectRecord();
				
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("eventClass", b.get("name"));
							
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

var loadeventff;
function loadTesthandleff(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadeventff=loadDialogPage(null, biolims.common.sampleType, "/dic/type/dicTypeJquerySelect.action?flag=handleff", {
			"Confirm" : function() {
				var operGrid = dicTypeDialogGridGrid;
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qaTaskItemGrid.getSelectRecord();
				
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("handleFf", b.get("name"));
							
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function loadTesteventMs(){}

var loadeventcs;
function loadTesthandleCs(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadeventcs=loadDialogPage(null, biolims.common.sampleType, "/dic/type/dicTypeJquerySelect.action?flag=handlecs", {
			"Confirm" : function() {
				var operGrid = dicTypeDialogGridGrid;
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qaTaskItemGrid.getSelectRecord();
				
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("handleCs", b.get("name"));
							
						});
					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	
}
function loadTesthandleJtms(){}
