﻿var qaTaskTemplateCosGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
	    name:'id',
	    type:"string"
    });
    fields.push({
	    name:'code',
	    type:"string"
	});
    fields.push({
		name:'itemId',
	    type:"string"
	});
    fields.push({
		name:'type-id',
		type:"string"
	});
   fields.push({
		name:'type-name',
		type:"string"
	});
    fields.push({
		name:'tCos',
	    type:"string"
    });
    fields.push({
	    name:'name',
	    type:"string"
    });
    fields.push({
	    name:'state',
	    type:"string"
    });
    fields.push({
		name:'temperature',
		type:"string"
	});
    fields.push({
		name:'speed',
		type:"string"
	});
    fields.push({
		name:'time',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
	    name:'isGood',
	    type:"string"
    });
    fields.push({
	    name:'qaTask-id',
	    type:"string"
    });
    fields.push({
	    name:'qaTask-name',
	    type:"string"
    });
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.instrumentId,
		width:20*6
	});
	cm.push({
		dataIndex:'tCos',
		hidden : true,
		header:biolims.common.templateInstrumentId,
		width:20*6
	});
	cm.push({
		dataIndex:'itemId',
		hidden : true,
		header:biolims.common.templateStepNo,
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:biolims.common.instrumentNo,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var instrumentName =new Ext.form.TextField({
        allowBlank: false
	});
	instrumentName.on('focus', function() {
		setinstrumentName();
	 });
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.instrumentName,
		width:20*6,
		editor:instrumentName
		
	});
	cm.push({
		dataIndex:'type-id',
		hidden : true,
		header:"设备类型ID",
		width:20*6
	});
	cm.push({
		dataIndex:'type-name',
		hidden : false,
		header:"设备类型",
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:"设备状态",
		width:20*6
	});
//	cm.push({
//		dataIndex:'isGood',
//		hidden : false,
//		header:'是否通过检验',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	var storeisGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.no ], [ '1', biolims.common.yes ] ]
	});
	var isGoodCob = new Ext.form.ComboBox({
		store : storeisGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'isGood',
		hidden : false,
		header:biolims.common.isGood,
		width:20*6,
		editor : isGoodCob,
		renderer : Ext.util.Format.comboRenderer(isGoodCob)
	});
	//
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.common.temperature,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:biolims.common.speed,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.common.time,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'bloodSampleTask-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:50*10
	});
	cm.push({
		dataIndex:'bloodSampleTask-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:50*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/qaTask/showQaTaskTemplateCosListJson.action?id="+$("#id_parent_hidden").val()+"&itemId="+$("#itemId").val();
	var opts={};
	opts.title="设备明细";
	opts.height =  document.body.clientHeight*0.65;
	opts.tbar = [];
	var state=$("#qaTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qa/qaTask/delqaTaskTemplateCos.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//	opts.tbar.push({
//		text : '填加明细',
//		handler : function (){
//			//获取选择的数据
//			var selectRcords=qaTaskTemplateItemGrid.getSelectionModel().getSelections();
//			//获取全部数据
//			var allRcords=qaTaskTemplateItemGrid.store;
//			//选中的数量
//			var length1=selectRcords.length;
//			//全部数据量
//			var length2=allRcords.getCount();			
//			if(length2>0){
//				if(length1==1){
//					var code="";
//					$.each(selectRcords, function(i, obj) {
//						code=obj.get("code");
//					});
//					if(code!=null){
//						var ob = qaTaskTemplateCosGrid.getStore().recordType;
//						var p = new ob({});
//						p.isNew = true;
//						p.set("itemId", code);
//						qaTaskTemplateCosGrid.stopEditing();
//						qaTaskTemplateCosGrid.getStore().insert(0, p);
//						qaTaskTemplateCosGrid.startEditing(0, 0);
//					}else{
//						message("请先添加模板明细数据！");
//						return;
//					}				
//				}else if(length1>1){
//					message("模板明细中只能选择一条数据！");
//					return;
//				}else{
//					message("请先选择模板明细中数据！");
//					return;
//				}
//			}else{
//				message("模板明细中数据为空！");
//				return;
//			}
//		}
//    });
	opts.tbar.push({
		text : biolims.common.copy,
		handler : function() {
			var ob = qaTaskTemplateCosGrid.getStore().recordType;
			var p = new ob({});
			p.isNew = true;
			var records = qaTaskTemplateCosGrid.getSelectRecord();
			if (records.length==1) {
				$.each(records, function(i, obj) {
					p.set("code",obj.get("code"));
					p.set("name",obj.get("name"));
					p.set("temperature",obj.get("temperature"));
					p.set("speed",obj.get("speed"));
					p.set("time",obj.get("time"));
					p.set("isGood",obj.get("isGood"));
					p.set("itemId",obj.get("itemId"));
					p.set("tCos",obj.get("tCos"));
					p.set("type-id",obj.get("type-id"));
					p.set("type-name",obj.get("type-name"));
					p.set("qaTask-id",obj.get("qaTask-id"));
					p.set("qaTask-name",obj.get("qaTask-name"));
					p.set("note",obj.get("note"));
					qaTaskTemplateCosGrid.stopEditing();
					qaTaskTemplateCosGrid.getStore().add(p);
					qaTaskTemplateCosGrid.startEditing(0, 0);
				});
			}else{
				message("请选择一条您要复制的数据！");
			}
		}
	});
	opts.tbar.push({
		text : biolims.common.delSelected,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.fillDetail,
		handler : null
	})
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});	
	if($("#qaTask_id").val()&&$("#qaTask_id").val()!="NEW"){
		opts.tbar.push({
			iconCls : 'save',
			text : biolims.common.save,
			handler : saveItem
		});
	}
	}
	qaTaskTemplateCosGrid=gridEditTable("qaTaskTemplateCosdiv",cols,loadParam,opts);
	$("#qaTaskTemplateCosdiv").data("qaTaskTemplateCosGrid", qaTaskTemplateCosGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
function setinstrumentName(){
	var record = qaTaskTemplateCosGrid.getSelectionModel().getSelected();
	var options = {};
	 options.width = 800;
	 options.height = 500;
	 loadDialogPage(null, "选择设备", "/equipment/repair/showInstrument.action", {
	 "Confirm" : function() {
	 var operGrid = $("#show_instrument_div").data("showInstrumentGrid");
	 var selectRecord = operGrid.getSelectionModel().getSelections();
	 if (selectRecord.length > 0) {
		 $.each(selectRecord, function(i, obj) {
			 record.set("code", obj.get("id"));
			 record.set("name", obj.get("name"));
			 record.set("state", obj.get("state-name"));
		 });
			
	 }else{
	 message(biolims.common.selectYouWant);
	 return;}
	 $(this).dialog("close");}
	 }, true, options);
}	
