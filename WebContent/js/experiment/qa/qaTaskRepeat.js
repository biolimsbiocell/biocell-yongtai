var qaTaskRepeatGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
    fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
    fields.push({
		name:'productName',
		type:"string"
	});   
	fields.push({
		name:'sequenceFun',
		type:"string"
	});
    fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name:'idCard',
		type:"string"
	});
    fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'sampleType',
		type : "string"
	});
    fields.push({
		name:'sampleCondition',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'method',
		type:"string"
	});
    fields.push({
		name:'isExecute',
		type:"string"
	});
    fields.push({
		name:'feedbackTime',
		type:"string"
	});
    fields.push({
		name:'note',
		type:"string"
	});
    fields.push({
		name:'name',
		type:"string"
	});
    fields.push({
		name:'classify',
		type:"string"
	});
    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		header:'样本编号',
		width:20*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'原始样本编号',
		width:20*6,		
		sortable:true
	});	
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : false,
		header:'检测项目',
		width:30*6
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样时间',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'idCard',
		header:'身份证号',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		header:'应出报告日期',
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'phone',
		hidden : false,
		header:'手机号',
		width:30*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:'关联任务单',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		header:'样本类型',
		width:20*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'Condition',
		header:'样本情况',
		width:20*6,
		sortable:true
	});
	var storeresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : false,
		header:'结果',
		width:20*6,
		editor : resultCob,
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'method',
		header:'处理意见',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var storesubmitCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var submitCob = new Ext.form.ComboBox({
		store : storesubmitCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex : 'isExecute',
		header : '是否执行',
		width : 20 * 6,
		editor : submitCob,
		renderer : Ext.util.Format.comboRenderer(submitCob)
	});
	cm.push({
		dataIndex:'feedbackTime',
		hidden : false,
		header:'反馈时间',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:20*6,
		
		sortable:true
	});	
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:'临床/科技服务',
		width:20*6,		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/showRepeatQaTaskListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title="重新QA任务";
	opts.height=document.body.clientHeight;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		iconCls : 'application_search',
		text : '检索',
		handler : search
	});
	opts.tbar.push({
		iconCls : 'save',
		text : '保存',
		handler : save
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : "批量处理意见",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_next_div"), "批量处理意见", null, {
				"确定" : function() {
					var records = qaTaskRepeatGrid.getSelectRecord();
					if (records && records.length > 0) {
						var nextFlow = $("#method").val();
						qaTaskRepeatGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("method", nextFlow);
						});
						qaTaskRepeatGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
	qaTaskRepeatGrid=gridEditTable("show_qaTaskRepeat_div",cols,loadParam,opts);
	$("#show_qaTaskRepeat_div").data("qaTaskRepeatGrid", qaTaskRepeatGrid);
});

//保存
function save(){
	var itemJson = commonGetModifyRecords(qaTaskRepeatGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/qaTask/saveQaTaskRepeat.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {
					qaTaskRepeatGrid.getStore().commitChanges();
					qaTaskRepeatGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);					
	}else{
		message("没有需要保存的数据！");
	}
}

//检索
function search() {
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), "搜索", null, {
		"开始检索" : function() {
			commonSearchAction(qaTaskRepeatGrid);
			$(this).dialog("close");
		},
		"清空" : function() {
			form_reset();
		}
	}, true, option);
}
