﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var state = $("#qaTask_state").val();
	var stateName = $("#qaTask_stateName").val();
	if(state =="3"||stateName==biolims.common.toModify){
		load("/experiment/qa/qaTask/showQaTaskTempList.action", null, "#qaTaskTempPage");
		$("#markup").css("width","75%");
	}else{
		$("#qaTaskTempPage").remove();
	}
	
	var mttf = $("#qaTask_template_templateFieldsCode").val();
	var mttfItem = $("#qaTask_template_templateFieldsItemCode").val();
	reloadQaTaskItem(mttf, mttfItem);

});	

function reloadQaTaskItem(mttf, mttfItem) {
	setTimeout(
			function() {
				if (mttf == null || mttf == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = qaTaskItemGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttf.indexOf(colName) >= 0) {
							colArray2.push(colName);
							qaTaskItemGrid.getColumnModel().setHidden(i,
									false);
						} else {
							qaTaskItemGrid.getColumnModel().setHidden(i,
									true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
				if (mttfItem == null || mttfItem == "") {

				} else {
					var colArray = [];
					var colArray2 = [];
					var colModel = qaTaskResultGrid.colModel;
					var colObj = colModel.config;
					for ( var i = 2; i < colObj.length; i++) {
						var colName = colModel.getDataIndex(i); // grid表头名
						colArray.push(colName);
						if (mttfItem.indexOf(colName) >= 0) {
							colArray2.push(colName);
							qaTaskResultGrid.getColumnModel().setHidden(
									i, false);
						} else {
							qaTaskResultGrid.getColumnModel().setHidden(
									i, true);
						}
					}
//					 alert(colArray);
//					 alert(colArray2);
				}
			}, 1000);
}


function add() {
	window.location = window.ctx + "/experiment/qa/qaTask/editQaTask.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/qa/qaTask/showQaTaskList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});	

$("#toolbarbutton_print").click(function(){
	var url = '__report=QaTask.rptdesign&id=' + $("#qaTask_id").val();
	commonPrint(url);
});

setTimeout(function() {
	if($("#qaTask_template").val()){
		var maxNum = $("#qaTask_maxNum").val();
		if(maxNum>0){
			load("/storage/container/sampleContainerTest.action", {
				id : $("#qaTask_template").val(),
				type :$("#type").val(),
				maxNum : 0
			}, "#3d_image0", function(){
				if(maxNum>1){
					load("/storage/container/sampleContainerTest.action", {
						id : $("#qaTask_template").val(),
						type :$("#type").val(),
						maxNum : 1
					}, "#3d_image1", null);
				}
			});
		}
	}
}, 100);

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("QaTask", {
					userId : userId,
					userName : userName,
					formId : $("#qaTask_id").val(),
					title : $("#qaTask_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {	
	var taskName=$("#taskName").val();
	var taskId =  $(this).attr("taskId");
	var formId=$("#qaTask_id").val();	
	var options = {};
	options.width = 929;
	options.height = 534;	
	if (window.ActiveXObject) {
		// IE浏览器
		options.height = options.height + "px";
	}
	options.data = {};
	options.data.taskId = taskId;
	options.data.formId = formId;
	var url = "/workflow/processinstance/toCompleteTaskView.action";
	var dialogWin = loadDialogPage(null, "审批任务", url, {
		"确定" : function() {			
			var operVal = $("#oper").val();
			if(operVal=="0"){
				var paramData = {};
				paramData.oper = $("#oper").val();
				paramData.info = $("#opinion").val();
				var reqData = {
					data : JSON.stringify(paramData),
					formId : formId,
					taskId : taskId,
					userId : window.userId
				};											
				_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
				location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
			}, dialogWin);				
			}else if(operVal=="1"){
				if(taskName=="QA任务"){
				var codeList = new Array();
				var codeList1 = new Array();
				var selRecord1 = qaTaskItemGrid.store;
				var flag=true;
				var flag1=true;
				if (qaTaskResultGrid.getAllRecord().length > 0) {
					var selRecord = qaTaskResultGrid.store;
					for(var j=0;j<selRecord.getCount();j++){
						var oldv = selRecord.getAt(j).get("submit");
						codeList.push(selRecord.getAt(j).get("qaTaskCode"));
						if(oldv==""){
							flag=false;
							message("有样本未提交！");
							return;
						}
						if(selRecord.getAt(j).get("nextFlowId")==""){
							message("有下一步未填写！");
							return;
						}
					}
					for(var j=0;j<selRecord1.getCount();j++){						
						if(codeList.indexOf(selRecord1.getAt(j).get("code"))==-1){
							codeList1.push(selRecord1.getAt(j).get("code"));
							flag1=false;
							message("有样本未完成实验！");
						};
					}
					if(qaTaskResultGrid.getModifyRecord().length > 0){
						message("请先保存记录！");
						return;
					}
					if(flag1){
							var myMask1 = new Ext.LoadMask(Ext.getBody(), {
								msg : '请等待...'
							});
							myMask1.show();
							Ext.MessageBox.confirm("确认", "请确认保存修改项后进行办理!", function(button, text) {
								if (button == "yes") {
									var paramData =  {};
									paramData.oper = $("#oper").val();
									paramData.info = $("#opinion").val();
									var reqData = {
										data : JSON.stringify(paramData),
										formId : formId,
										taskId : taskId,
										userId : window.userId
									};																		
									_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
										location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
									}, dialogWin);
																										}
							});
							myMask1.hide();
					}else{
						message("有样本未完成实验！样本有："+codeList1);
					}
				}else{
					message("请填加任务明细并保存！");
					return;
				}
				}else{
					var paramData = {};
					paramData.oper = $("#oper").val();
					paramData.info = $("#opinion").val();
					var reqData = {
						data : JSON.stringify(paramData),
						formId : formId,
						taskId : taskId,
						userId : window.userId
					};										
					_complete(reqData, function() {document.getElementById('toolbarSaveButtonFlag').value = 'save';
					location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
				}, dialogWin);
				}
			}				
		},
		"查看流程图" : function() {
			var url = "/workflow/processinstance/toTraceProcessInstanceView.action";
			openDialog(window.ctx + url + "?instanceId=" + $("#instance_id").val());
		}
	}, true, options);
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var qaTaskItemDivData = $("#qaTaskItemdiv").data("qaTaskItemGrid");
		document.getElementById('qaTaskItemJson').value = commonGetModifyRecords(qaTaskItemDivData);
	    var qaTaskResultDivData = $("#qaTaskResultdiv").data("qaTaskResultGrid");
		document.getElementById('qaTaskResultJson').value = commonGetModifyRecords(qaTaskResultDivData);
		var qaTaskTemplateItemDivData = $("#qaTaskTemplateItemdiv").data("qaTaskTemplateItemGrid");
		document.getElementById('qaTaskTemplateItemJson').value = commonGetModifyRecords(qaTaskTemplateItemDivData);
		var qaTaskTemplateReagentDivData = $("#qaTaskTemplateReagentdiv").data("qaTaskTemplateReagentGrid");
		document.getElementById('qaTaskTemplateReagentJson').value = commonGetModifyRecords(qaTaskTemplateReagentDivData);
		var qaTaskTemplateCosDivData = $("#qaTaskTemplateCosdiv").data("qaTaskTemplateCosGrid");
		document.getElementById('qaTaskTemplateCosJson').value = commonGetModifyRecords(qaTaskTemplateCosDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/qa/qaTask/save.action";
		form1.submit();
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();	
		}
}	
function saveItem() {
	var id = $("#qaTask_id").val();
	if (!id) {
		return;
	}
	var qaTaskItemDivData = $("#qaTaskItemdiv").data("qaTaskItemGrid");
	qaTaskItemJson = commonGetModifyRecords(qaTaskItemDivData);
    var qaTaskResultDivData = $("#qaTaskResultdiv").data("qaTaskResultGrid");
	qaTaskResultJson= commonGetModifyRecords(qaTaskResultDivData);
	var qaTaskTemplateItemDivData = $("#qaTaskTemplateItemdiv").data("qaTaskTemplateItemGrid");
	qaTaskTemplateItemJson= commonGetModifyRecords(qaTaskTemplateItemDivData);
	var qaTaskTemplateReagentDivData = $("#qaTaskTemplateReagentdiv").data("qaTaskTemplateReagentGrid");
	qaTaskTemplateReagentJson= commonGetModifyRecords(qaTaskTemplateReagentDivData);
	var qaTaskTemplateCosDivData = $("#qaTaskTemplateCosdiv").data("qaTaskTemplateCosGrid");
	qaTaskTemplateCosJson = commonGetModifyRecords(qaTaskTemplateCosDivData);
	
	ajax("post", "/experiment/qa/qaTask/saveAjax.action", {
		qaTaskItemJson:qaTaskItemJson,
		qaTaskResultJson:qaTaskResultJson,
		qaTaskTemplateItemJson:qaTaskTemplateItemJson,
		qaTaskTemplateReagentJson:qaTaskTemplateReagentJson,
		qaTaskTemplateCosJson:qaTaskTemplateCosJson,
		id : id
	}, function(data) {
		if (data.success) {
			//message("保存成功！");
			// message("保存成功！");
			if (data.equip != 0 && typeof (data.equip) != 'undefined') {
				message(data.equip + "条设备数据保存成功！");
			}
			if (data.re != 0 && typeof (data.re) != 'undefined') {
				message(data.re + "条原辅料数据保存成功！");
			}
			qaTaskItemDivData.getStore().reload();
			qaTaskResultDivData.getStore().reload();
			qaTaskTemplateItemDivData.getStore().reload();
			qaTaskTemplateReagentDivData.getStore().reload();
			qaTaskTemplateCosJson.getStore().reload();
		} else {
			message(biolims.common.saveFailed);
		}
	}, null);
}
function editCopy() {
	window.location = window.ctx + '/experiment/qa/qaTask/copyQaTask.action?id=' + $("#qaTask_id").val();
}

$("#toolbarbutton_status").click(function(){
	var QaTaskInfoGrid = qaTaskResultGrid.getStore();
	var QaTaskItemGrid = qaTaskItemGrid.getStore();
	var num = 0;
	for(var i= 0; i<QaTaskItemGrid.getCount();i++){		
		if(QaTaskInfoGrid.getCount()==0){
			message("请完成实验！");
			return;
		}
		for(var j=0;j<QaTaskInfoGrid.getCount();j++){
			if(QaTaskItemGrid.getAt(i).get("yCode")==QaTaskInfoGrid.getAt(j).get("yCode")){
				j=QaTaskInfoGrid.getCount();
			}else{
				num=num+1;
				if(num==QaTaskInfoGrid.getCount()){
					num=0;
					message("请完成实验！");
					return;
				}
			}
		}		
	}
	commonChangeState("formId=" + $("#qaTask_id").val() + "&tableId=QaTask");
});

function checkSubmit() {
	/*var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#qaTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#qaTask_template").val());
	nsc.push("实验模板不能为空！");
	fs.push($("#qaTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}*/
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'QA任务',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/qa/qaTask/showQaTaskItemList.action", {
				id : $("#qaTask_id").val()
			}, "#qaTaskItempage");
load("/experiment/qa/qaTask/showTemplateItemList.action", {
	id:$("#qaTask_id").val()
}, "#qaTaskTemplateItempage");
load("/experiment/qa/qaTask/showQaTaskTemplateReagentList.action",{
				id:$("#qaTask_id").val(),
			}, "#qaTaskTemplateReagentpage");
load("/experiment/qa/qaTask/showQaTaskTemplateCosList.action", {
	id:$("#qaTask_id").val()
}, "#qaTaskTemplateCospage");
load("/experiment/qa/qaTask/showQaTaskInfoList.action", {
				id : $("#qaTask_id").val()
			}, "#qaTaskResultpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
//调用模板
function TemplateFun(){
			var type="doQaTask";
			var win = Ext.getCmp('TemplateFun');
			if (win) {win.close();}
			var TemplateFun= new Ext.Window({
			id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: '关闭',
			 handler: function(){
			 TemplateFun.close(); }  }]  }); 
			 TemplateFun.show(); 
		}

//双击加载模板数据
function setTemplateFun(rec){	
	if($('#qaTask_acceptUser_name').val()==""){
		document.getElementById('qaTask_acceptUser').value=rec.get('acceptUser-id');
		document.getElementById('qaTask_acceptUser_name').value=rec.get('acceptUser-name');
	}		
	var itemGrid=qaTaskItemGrid.store;
	if(itemGrid.getCount()>0){
		for(var i=0;i<itemGrid.getCount();i++){
			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
			itemGrid.getAt(i).set("sampleConsume",rec.get('sampleNum'));
		}
	}
		var code=$("#qaTask_template").val();
		if(code==""){
			var cid=rec.get('id');
						document.getElementById('qaTask_template').value=rec.get('id');
						document.getElementById('qaTask_template_name').value=rec.get('name');
						var win = Ext.getCmp('TemplateFun');
						if(win){win.close();}
						var id=rec.get('id');
						//加载模板明细
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {
									var ob = qaTaskTemplateItemGrid.getStore().recordType;
									qaTaskTemplateItemGrid.stopEditing();
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										qaTaskTemplateItemGrid.getStore().add(p);							
									});									
									qaTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						//加载原辅料明细
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = qaTaskTemplateReagentGrid.getStore().recordType;
								qaTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									p.set("sn",obj.sn);
									qaTaskTemplateReagentGrid.getStore().add(p);							
								});								
								qaTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						//加载实验设备明细
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = qaTaskTemplateCosGrid.getStore().recordType;
								qaTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id", obj.typeId);
									p.set("type-name", obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									qaTaskTemplateCosGrid.getStore().add(p);							
								});			
								qaTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null);
		}else{
			var flag = true;
			if(rec.get('id')==code){
				flag = confirm("是否重新加载实验步骤，应有实验步骤将被删除？");
 			 }
			if(flag==true){
 				//判断设备是否占用
 				var cid=rec.get('id');
						var ob1 = qaTaskTemplateItemGrid.store;
		 				if (ob1.getCount() > 0) {
							for(var j=0;j<ob1.getCount();j++){
								var oldv = ob1.getAt(j).get("id");				
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/qa/qaTask/delTemplateItemOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null);
								}else{								
									qaTaskTemplateItemGrid.store.removeAll();
								}
							}
							qaTaskTemplateItemGrid.store.removeAll();
		 				}		 				
						var ob2 = qaTaskTemplateReagentGrid.store;
						if (ob2.getCount() > 0) {
							for(var j=0;j<ob2.getCount();j++){
								var oldv = ob2.getAt(j).get("id");								
								//根据ID删除
								if(oldv!=null){
								ajax("post", "/experiment/qa/qaTask/delTemplateReagentOne.action", {
									ids : oldv
								}, function(data) {
									if (data.success) {
										message("删除成功！");
									} else {
										message("删除失败！");
									}
								}, null); 
								}else{
									qaTaskTemplateReagentGrid.store.removeAll();
								}
							}
							qaTaskTemplateReagentGrid.store.removeAll();
		 				}
						var ob3 = qaTaskTemplateCosGrid.store;
						if (ob3.getCount() > 0) {
							for(var j=0;j<ob3.getCount();j++){
								var oldv = ob3.getAt(j).get("id");							
								//根据ID删除
								if(oldv!=null){
									ajax("post", "/experiment/qa/qaTask/delTemplateCosOne.action", {
										ids : oldv
									}, function(data) {
										if (data.success) {
											message("删除成功！");
										} else {
											message("删除失败！");
										}
									}, null); 
								}else{
									qaTaskTemplateCosGrid.store.removeAll();
								}
							}
							qaTaskTemplateCosGrid.store.removeAll();
		 				}
						document.getElementById('qaTask_template').value=rec.get('id');
		 				document.getElementById('qaTask_template_name').value=rec.get('name');
		 				var win = Ext.getCmp('TemplateFun');
		 				if(win){win.close();}
						var id = rec.get('id');
						ajax("post", "/system/template/template/setTemplateItem.action", {
							code : id,
							}, function(data) {
								if (data.success) {	
									var ob = qaTaskTemplateItemGrid.getStore().recordType;
									qaTaskTemplateItemGrid.stopEditing();									
									$.each(data.data, function(i, obj) {
										var p = new ob({});
										p.isNew = true;
										p.set("tItem",obj.id);
										p.set("code",obj.code);
										p.set("name",obj.name);										
										p.set("note",obj.note);
										qaTaskTemplateItemGrid.getStore().add(p);							
									});									
									qaTaskTemplateItemGrid.startEditing(0, 0);		
								} else {
									message("获取明细数据时发生错误！");
								}
							}, null); 
						ajax("post", "/system/template/template/setTemplateReagent.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = qaTaskTemplateReagentGrid.getStore().recordType;
								qaTaskTemplateReagentGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tReagent",obj.id);
									p.set("code",obj.code);
									p.set("name",obj.name);
									p.set("batch",obj.batch);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);
									p.set("oneNum",obj.num);
									p.set("note",obj.note);
									qaTaskTemplateReagentGrid.getStore().add(p);							
								});								
								qaTaskTemplateReagentGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
						ajax("post", "/system/template/template/setTemplateCos.action", {
						code : id,
						}, function(data) {
							if (data.success) {	
								var ob = qaTaskTemplateCosGrid.getStore().recordType;
								qaTaskTemplateCosGrid.stopEditing();								
								$.each(data.data, function(i, obj) {
									var p = new ob({});
									p.isNew = true;
									p.set("tCos",obj.id);
									p.set("type-id", obj.typeId);
									p.set("type-name", obj.typeName);
									p.set("isGood",obj.isGood);
									p.set("itemId",obj.itemId);									
									p.set("temperature",obj.temperature);
									p.set("speed",obj.speed);
									p.set("time",obj.time);
									p.set("note",obj.note);
									qaTaskTemplateCosGrid.getStore().add(p);							
								});			
								qaTaskTemplateCosGrid.startEditing(0, 0);		
							} else {
								message("获取明细数据时发生错误！");
							}
						}, null); 
					}			
	}				
		reloadQaTaskItem(rec.get("templateFieldsCode"), rec
				.get("templateFieldsItemCode"));

}
	
//按条件加载原辅料
function showReagent(){
	//获取全部数据
	var allRcords=qaTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message("请先保存执行单数据!");
		return;
	}
	//获取选择的数据
	var selectRcords=qaTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=qaTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#qaTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/qa/qaTask/showQaTaskTemplateReagentList.action", {
			id : $("#qaTask_id").val()
		}, "#qaTaskTemplateReagentpage");
	}else if(length1==1){
		qaTaskTemplateReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/qa/qaTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = qaTaskTemplateReagentGrid.getStore().recordType;
				qaTaskTemplateReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("batch",obj.batch);
					p.set("num",obj.num);
					p.set("expireDate",obj.expireDate);
					p.set("oneNum",obj.oneNum);
					p.set("sampleNum",obj.sampleNum);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tReagent",obj.tReagent);
					p.set("qaTask-id",obj.tId);
					p.set("qaTask-name",obj.tName);					
					qaTaskTemplateReagentGrid.getStore().add(p);							
				});
				qaTaskTemplateReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}	
}

//按条件加载设备
function showCos(){
	var allRcords=qaTaskTemplateItemGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message("请先保存执行单数据!");
		return;
	}
	//获取选择的数据
	var selectRcords=qaTaskTemplateItemGrid.getSelectionModel().getSelections();
	//获取全部数据
	var allRcords=qaTaskTemplateItemGrid.store;
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	var tid =$("#qaTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/qa/qaTask/showQaTaskTemplateCosList.action", {
			id : $("#qaTask_id").val()
		}, "#qaTaskTemplateCospage");
	}else if(length1==1){
		qaTaskTemplateCosGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/qa/qaTask/setCos.action", {
			tid:tid,code : code
		}, function(data) {			
			if (data.success) {	
				var ob = qaTaskTemplateCosGrid.getStore().recordType;
				qaTaskTemplateCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;					
					p.set("id",obj.id);
					p.set("code",obj.code);
					p.set("name",obj.name);
					p.set("type-id", obj.typeId);
					p.set("type-name", obj.typeName);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("note",obj.note);
					p.set("state",obj.state);
					p.set("time",obj.time);
					p.set("isGood",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("tCos",obj.tCos);
					p.set("qaTask-id",obj.tId);
					p.set("qaTask-name",obj.tName);					
					qaTaskTemplateCosGrid.getStore().add(p);							
				});
				qaTaskTemplateCosGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}
}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '回滚'
			});
		item.on('click', ckcrk);
		
		});
	
	function ckcrk(){		
		Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
			if (button == "yes") {
				var selRecord = qaTaskResultGrid.store;
				for(var j=0;j<selRecord.getCount();j++){
					var submit = selRecord.getAt(j).get("submit");
					if(submit==""){
						message("有样本未提交，不能初始化！");
						return;
					}
				}
				for(var j=0;j<selRecord.getCount();j++){
					var code = selRecord.getAt(j).get("code");
					var nextFlowId = selRecord.getAt(j).get("nextFlowId");
					if(nextFlowId!=null){
						ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
							code : code, nextFlowId : nextFlowId
						}, function(data) {
							if (data.success) {	
								message("回滚成功！");
								selRecord.getAt(j).set("submit","");
								save();
							} else {
								message("回滚失败！");
							}
						}, null);
					}
					
				}
			}
		});
	}	
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '保存'
			});
		item.on('click', ckcrk2);		
		});
	function ckcrk2(){
		save();
	}
	Ext.onReady(function(){
		var item = menu.add({
		    	text: '办理回滚结果'
			});
		item.on('click', ckcrk3);		
		});
	function ckcrk3(){
		Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
		ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
			model : "QaTask",id : $("#qaTask_id").val()
		}, function(data) {
			if (data.success) {	
				message("办理回滚成功！");
			} else {
				message("办理回滚失败！");
			}
		}, null);
	
}
		



