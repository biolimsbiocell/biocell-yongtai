var qaTaskReceiveGrid;
$(function(){
	var cols={};
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
		name:'receiveUser-id',
		type:"string"
	});
	fields.push({
		name:'receiveUser-name',
		type:"string"
	});
	fields.push({
		name:'sampleType',
		type:"string"
	});
	fields.push({
		name:'receiverDate',
		type:"string"
	});
	fields.push({
		name:'method',
		type:"string"
	});
	fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'nextFlow',
		type:"string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
	fields.push({
		name:'advice',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'isExecute',
		type:"string"
	});
	fields.push({
		name:'feedbackTime',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'receiveUser-id',
		hidden:true,
		header:'接收人ID',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'receiveUser-name',
		header:'接收人',
		hidden:false,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'receiverDate',
		header:'接收日期',
		width:20*6,		
		sortable:true
	});
	cm.push({
		dataIndex:'location',
		header:'储位提示',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:20*6,
		hidden : false,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:20*6,		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/qaTaskReceive/showQaTaskReceiveListJson.action";
	var opts={};
	opts.title="样本接收";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	qaTaskReceiveGrid=gridTable("show_qaTaskReceive_div",cols,loadParam,opts);
})

function add(){
		window.location=window.ctx+'/experiment/qa/qaTaskReceive/editQaTaskReceive.action';
	}

function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/experiment/qa/qaTaskReceive/editQaTaskReceive.action?id=' + id;
}

function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/experiment/qa/qaTaskReceive/viewQaTaskReceive.action?id=' + id;
}

function exportexcel() {
	qaTaskReceiveGrid.title = '导出列表';
	var vExportContent = qaTaskReceiveGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {			
				if (($("#startreceiverDate").val() != undefined) && ($("#startreceiverDate").val() != '')) {
					var startreceiverDatestr = ">=##@@##" + $("#startreceiverDate").val();
					$("#receiverDate1").val(startreceiverDatestr);
				}
				if (($("#endreceiverDate").val() != undefined) && ($("#endreceiverDate").val() != '')) {
					var endreceiverDatestr = "<=##@@##" + $("#endreceiverDate").val();
					$("#receiverDate2").val(endreceiverDatestr);
				}
				commonSearchAction(qaTaskReceiveGrid);
				$(this).dialog("close");
			},
			"清空" : function() {
				form_reset();
			}
		}, true, option);
	});
});
