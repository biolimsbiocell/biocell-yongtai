﻿var qaTaskTempGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name:'sampleName',
		type:"string"
	});
    fields.push({
		name:'sampleId',
		type:"string"
	});
	fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
    fields.push({
		name:'method',
		type:"string"
	});
    fields.push({
		name:'reason',
		type:"string"
    });
    fields.push({
		name:'note',
		type:"string"
    });
	fields.push({
		name : 'orderId',
		type : "string"
	});
//	fields.push({
//		name : 'idCard',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
	    name : 'classify',
	    type : "string"
    });
	fields.push({
		name : 'sampleType',
		type : "string"
	});
	fields.push({
		name : 'sampleNum',
		type : "string"
	});
	fields.push({
		name:'labCode',
		type:"string"
	});
	fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : false,
		header:'患者姓名',
		sortable:true,
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:'身份证号',
//		width:20*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		hidden : true,
//		header:'手机号',
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样时间',
		width:30*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleId',
		hidden : true,
		header:'样本ID',
		width:30*6
	});
	cm.push({
		dataIndex:'Name',
		hidden : true,
		header:'样本名称',
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : true,
		header:'储位',
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : false,
		header:'关联任务单',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'原因',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : false,
		header:'接收备注',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/qaTask/showQaTaskTempJson.action";
	loadParam.limit=10000;
	var opts={};
	opts.title="待实验样本";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];       
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = qaTaskTempGrid.getAllRecord();
							var store = qaTaskTempGrid.store;
							var isOper = true;
							var buf = [];
							qaTaskTempGrid.stopEditing();
							$.each(array,function(i, obj) {																
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));										
									}									
								});
							});
							qaTaskTempGrid.getSelectionModel().selectRows(buf);																																			
							//判断那些样本没有匹配到
							var nolist = new Array();
							var templist = new Array();
							$.each(records, function(i, obj1) {
								templist.push(obj1.get("code"));
							});
							$.each(array,function(i, obj) {
								if(templist.indexOf(obj) == -1){
									nolist.push(obj);
								}
							});
							if(nolist!="" && nolist.length>0){
								message("没有匹配到的样本有："+nolist);
							}							
							if(isOper==false){								
								//message("样本号核对不符，请检查！");								
							}else{								
								//message("样本号核对完毕！");
								addItem();
							}
							qaTaskTempGrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);
		}
	});
	opts.tbar.push({
		text : '科技服务',
		handler : techQA任务Service
	});
	qaTaskTempGrid=gridEditTable("qaTaskTempdiv",cols,loadParam,opts);
	$("#qaTaskTempdiv").data("qaTaskTempGrid", qaTaskTempGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//从左边添加到右边的明细中
function addItem(){
	var selRecord = qaTaskTempGrid.getSelectionModel().getSelections();//从左边获取数据
	var getRecord = qaTaskItemGrid.store;//填充到当前的明细中
	var count = 1;
	var max=0;
	for(var i=0; i<getRecord.getCount();i++){
		var a=getRecord.getAt(i).get("orderNumber");
		if(a>max){
			max=a;
		}
	}
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var isRepeat = false;
			for(var j=0; j<getRecord.getCount();j++){
				var getData = getRecord.getAt(j).get("tempId");
				if(getData==obj.get("id")){
					message("有重复的数据，请重新选择！");
					isRepeat = true;
					break;
				}
			}	
			if(!isRepeat){
				var ob = qaTaskItemGrid.getStore().recordType;
				qaTaskItemGrid.stopEditing();
				var p= new ob({});				
				var str=obj.get("code");				
				p.set("orderNumber",Number(max)+count);
				p.set("tempId",obj.get("id"));
				p.set("code",obj.get("code"));
				p.set("sampleCode",obj.get("sampleCode"));
				p.set("sampleName",obj.get("sampleName"));
				p.set("patientName",obj.get("patientName"));
				p.set("productId",obj.get("productId"));
				p.set("productName",obj.get("productName"));
				p.set("sequenceFun",obj.get("sequenceFun"));
				p.set("phone",obj.get("phone"));
				p.set("idCard",obj.get("idCard"));
				p.set("inspectDate",obj.get("inspectDate"));
				p.set("orderId",obj.get("orderId"));
				p.set("reportDate",obj.get("reportDate"));
				p.set("location",obj.get("binLocation"));
				p.set("classify",obj.get("classify"));
				p.set("sampleType",obj.get("sampleType"));
				p.set("sampleNum",obj.get("sampleNum"));
				p.set("state","1");
				p.set("labCode",obj.get("labCode"));
				qaTaskItemGrid.getStore().add(p);
			
			}
		});		
		qaTaskItemGrid.startEditing(0,0);
	}else{
		message("请选择样本！");
	}
}

//科技服务
function techQA任务Service(){
	 	var options = {};
	 	options.width = document.body.clientWidth - 470;
	 	options.height = document.body.clientHeight - 80;
		loadDialogPage(null, "选择QA任务任务单", "/technology/qaTask/techQaTaskServiceTask/showTechQaTaskServiceTaskDialog.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_techQaTaskServiceTask_div1").data("showTechQaTaskServiceTaskDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(i, obj) {
						var id1=obj.get("id");						
						qaTaskTempGrid.store.reload();
						var filter1 = function(record, id){
							var flag = true;
							if(id1){
								if (record.get("orderId").indexOf(id1)>=0){
									flag = true;
								}
								else{
									return false;
								}
							 }
							return flag;
						};
						var onStoreLoad1 = function(store, records, options){
						  store.filterBy(filter1);
						};
						qaTaskTempGrid.store.on("load", onStoreLoad1);
					});					
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
