﻿//QA任务左侧
var qaTaskFromReceiveLeftgrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'sampleName',
		type:"string"
	});
	fields.push({
		name:'location',
		type:"string"
	});
	fields.push({
		name:'testProject',
		type:"string"
	});
    fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'qaTaskReceive-id',
		type:"string"
	});
	fields.push({
	    name:'qaTaskReceive-name',
		type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'患者姓名',
		sortable:true,
		width:50*6
	});
	cm.push({
		dataIndex:'Name',
		hidden : false,
		header:'样本名称',
		width:20*6
	});
	cm.push({
		dataIndex:'location',
		hidden : false,
		header:'储位',
		width:20*6
	});
	cm.push({
		dataIndex:'testProject',
		hidden : false,
		header:'检测项目',
		width:20*6
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'stateName',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'qaTaskReceive-id',
		hidden : true,
		header:'相关主表',
		width:20*6
	});
	cm.push({
		dataIndex:'qaTaskReceive-name',
		hidden : true,
		header:'相关主表',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/qaTask/showQaTaskFromReceiveListJson.action";
	var opts={};
	opts.title="样本接收明细";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = qaTaskFromReceiveLeftgrid.getAllRecord();
							var store = qaTaskFromReceiveLeftgrid.store;
							var isOper = true;
							var buf = [];
							qaTaskFromReceiveLeftgrid.stopEditing();
							$.each(array,function(i, obj) {						
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("code")){
										buf.push(store.indexOfId(obj1.get("id")));										
									}									
								})
							});
							qaTaskFromReceiveLeftgrid.getSelectionModel().selectRows(buf);							
							if(isOper==false){								
								message("样本号核对不符，请检查！");								
							}else{								
								addItem();
							}
							qaTaskFromReceiveLeftgrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);
		}
	});
	qaTaskFromReceiveLeftgrid=gridEditTable("qaTaskFromReceiveLeftdiv",cols,loadParam,opts);
	$("#qaTaskFromReceiveLeftdiv").data("qaTaskFromReceiveLeftgrid", qaTaskFromReceiveLeftgrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//从左边添加到右边的明细中
function addItem(){
	var selRecord = qaTaskFromReceiveLeftgrid.getSelectionModel().getSelections();//从左边获取数据
	var getRecord = qaTaskItemGrid.store;//填充到当前的明细中
	var count = 0;
	if(selRecord.length >0){
		$.each(selRecord,function(i, obj){
			var isRepeat = false;
			for(var j=0; j<getRecord.getCount();j++){
				var getData = getRecord.getAt(j).get("Code");
				if(getData==obj.get("code")){
					message("有重复的数据，请重新选择！");
					isRepeat = true;
					break;
				}
			}
			if(!isRepeat){
				var ob = qaTaskItemGrid.getStore().recordType;
				qaTaskItemGrid.stopEditing();
				var p= new ob({});				
				var str=obj.get("code");
				var s1=str.substring(0,1);
				var s2=str.substring(2);
				var s3=s1+"D"+s2;
				ajax("post", "/experiment/qa/qaTask/selectCodeCount.action", {
					code : str
				}, function(data) {
					if (data.success) {
							if(data.data==0){
								p.set("qaTaskCode",s3+"A");
							}else if(data.data==1){
								p.set("qaTaskCode",s3+"B");
							}else if(data.data==2){
								p.set("qaTaskCode",s3+"C");
							}else if(data.data==3){
								p.set("qaTaskCode",s3+"D");
							}else{
								message("请重新取样！");
								return;
							}
							p.set("Code",obj.get("code"));
							p.set("sampleName",obj.get("sampleName"));
							p.set("patient",obj.get("name"));
							p.set("state",obj.get("state"));
							p.set("testProject",obj.get("businessType"));
							p.set("location",obj.get("binLocation"));						
							var index = $("#qaTask_indexs").val();
							if(index!=""||index!=0){								
								p.set("indexs", Number(index)+count);								
							}else{
								p.set("indexs", count+1);								
							}
							count++;							
					}
					qaTaskItemGrid.getStore().add(p);
				});
			}
		});		
		qaTaskItemGrid.startEditing(0,0);
	}else{
		message("请选择样本！");
	}
}

