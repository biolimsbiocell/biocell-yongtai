﻿var qaTaskResultGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'code',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
	    name:'patientName',
	    type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
//	fields.push({
//		name : 'idCard',
//		type : "string"
//	});
//	fields.push({
//		name : 'phone',
//		type : "string"
//	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
    fields.push({
	    name:'location',
	    type:"string"
	});
    fields.push({
		name:'qbcontraction',
		type:"string"
	});	   
	fields.push({
		name:'volume',
		type:"string"
	});
	fields.push({
		name:'unit',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'projectId',
		type:"string"
	});
	fields.push({
		name:'contractId',
		type:"string"
	});
	fields.push({
		name:'orderType',
		type:"string"
	});
	fields.push({
		name:'taskId',
		type:"string"
	});
    fields.push({
		name:'submit',
		type:"string"
	});
	fields.push({
		name:'qaTask-id',
		type:"string"
	});
	fields.push({
		name:'qaTask-name',
		type:"string"
	});
    fields.push({
		name:'sampleNum',
		type:"string"
	});
    fields.push({
		name:'sumVolume',
		type:"string"
	});
    fields.push({
		name:'result',
		type:"string"
	});
    fields.push({
		name:'isToProject',
		type:"string"
	});
    fields.push({
		name:'nextFlowId',
		type:"string"
	});
    fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'advice',
		type:"string"
	});
    fields.push({
		name:'od260',
		type:"string"
	});
    fields.push({
		name:'od280',
		type:"string"
	});
    fields.push({
		name:'rin',
		type:"string"
	});
    fields.push({
		name:'contraction',
		type:"string"
	});
    fields.push({
		name:'tempId',
		type:"string"
	});
    fields.push({
	    name:'classify',
	    type:"string"
	});
    fields.push({
		name:'dicSampleType-id',
		type:"string"
	});
    fields.push({
		name:'dicSampleType-name',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'sampleType',
		type:"string"
	});
    fields.push({
		name:'dataBits',
		type:"string"
	});
    fields.push({
		name:'labCode',
		type:"string"
	});
    fields.push({
		name:'sampleInfo-note',
		type:"string"
	});
    
    fields.push({
		name:'qaTaskId',
		type:"string"
	});
    fields.push({
		name:'handleResult',
		type:"string"
	});
    fields.push({
		name:'hxRequire',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	
	cm.push({
		dataIndex:'qaTaskId',
		hidden : true,
		header:'QA任务号',
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	var testDicType2 =new Ext.form.TextField({
        allowBlank: true
	});
	testDicType2.on('focus', function() {
		setResult();
	});
	cm.push({
		dataIndex:'handleResult',
		hidden : false,
		header:'处理结果',
		width:40*6,		
		editor : testDicType2
	});cm.push({
		dataIndex:'hxRequire',
		hidden : false,
		header:'后续要求',
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:40*6
	});
	cm.push({
		dataIndex:'code',
		hidden : true,
		header:'样本编号',
		sortable:true,
		width:20*7
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : true,
		header:'原始样本编号',
		sortable:true,
		width:20*6
	});
	cm.push({
		dataIndex:'labCode',
		hidden : true,
		header:'实验室样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'qaTaskCode',
		hidden : true,
		header:'QA任务编号',
		width:30*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : true,
		header:'样本类型',
		width:20*6
	});
//	cm.push({
//		dataIndex:'idCard',
//		hidden : true,
//		header:'身份证号',
//		width:30*6
//	});
//	cm.push({
//		dataIndex:'phone',
//		header:'手机号',
//		hidden:true,
//		width:20*6
//	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		hidden:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden:true,
		header:'中间产物类型编号',
		width:15*10,
		sortable:true
	});
	var testDicType2 =new Ext.form.TextField({
        allowBlank: true
	});
	testDicType2.on('focus', function() {
		loadTestDicType2();
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		hidden:true,
		header:'中间产物类型',
		width:15*10,
		editor : testDicType2
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样时间',
		width:30*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'关联任务单',
		hidden:true,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积ul',
		width:20*6,		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'contraction',
		hidden : true,
		header:'Nanodrop浓度ng/ul',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : true,
		header:'QA任务总量ng',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'od280',
		hidden : true,
		header:'OD260/280',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'od260',
		hidden : true,
		header:'OD260/230',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'qbcontraction',
		hidden : true,
		header:'Qubit浓度ng/ul',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:3
		})
	});
	cm.push({
		dataIndex:'rin',
		hidden : true,
		header:'RIN',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'projectId',
		hidden : true,
		header:'项目编号',
		width:20*6
	});
	cm.push({
		dataIndex:'contractId',
		hidden : true,
		header:'合同编号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderType',
		hidden : true,
		header:'任务单类型',
		width:20*6
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ] ]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果',
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: true
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		hidden:true,
		header:'下一步流向',
		width:15*10,
		editor : nextFlowCob
	});
	var storePutCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '是' ], [ '0', '否' ] ]
	});
	var putCob = new Ext.form.ComboBox({
		store : storePutCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'submit',
		hidden : true,
		header:'是否提交',
		width:20*6,
		renderer : Ext.util.Format.comboRenderer(putCob)
	});
	var storeDataBits = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '种系' ], [ '0', '变异' ] ]
	});
	var DataBits = new Ext.form.ComboBox({
		store : storeDataBits,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'dataBits',
		hidden : true,
		header:'种系/变异',
		width:20*6,
		editor : DataBits,
		renderer : Ext.util.Format.comboRenderer(DataBits)
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:40*6,	
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : true,
		header:'备注',
		width:40*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6,		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'tempId',
		hidden : true,
		header:'临时表Id',
		width:20*6
	});
	cm.push({
		dataIndex:'qaTask-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'qaTask-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cm.push({
		dataIndex:'taskId',
		hidden : true,
		header:'科技服务任务单',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		header:'临床/科技服务 ',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sampleInfo-note',
		hidden : true,
		header:'接收备注',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/qaTask/showQaTaskInfoListJson.action?id="+ $("#id_parent_hidden").val();
	loadParam.limit = 200;
	var opts={};
	opts.title="QA任务结果";
	opts.height =  document.body.clientHeight*0.8;
	opts.tbar = [];
	var state=$("#qaTask_stateName").val();
	if(state!="完成"){
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/qa/qaTask/delQaTaskInfo.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				qaTaskResultGrid.getStore().commitChanges();
				qaTaskResultGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
/*	opts.tbar.push({
		text : "产物类型",
		handler : loadTestDicType2
	});
	opts.tbar.push({
		text : "批量数据",
		handler : function() {
			var options = {};
			options.width = 400;
			options.height = 300;
			loadDialogPage($("#bat_infos_div"), "批量数据", null, {
				"确定" : function() {
					var records = qaTaskResultGrid.getSelectRecord();
					if (records && records.length > 0) {
						var od230 = $("#od230").val();
						var od260 = $("#od260").val();
						var contraction = $("#contraction").val();
						var volume = $("#volume").val();
						var rin = $("#rin").val();
						var qbcontraction = $("#qbcontraction").val();
						qaTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							obj.set("od260", od230);
							obj.set("od280", od260);
							obj.set("contraction", contraction);
							obj.set("volume", volume);
							obj.set("rin", rin);
							obj.set("qbcontraction", qbcontraction);
						});
						qaTaskResultGrid.startEditing(0, 0);
					}
					$(this).dialog("close");
				}
			}, true, options);
		}
	});
		
	 opts.tbar.push({
			text : "批量粘贴导入",
			handler : function() {				
				$(".jquery-ui-warning").html("请从Excel表格中拷贝并直接粘贴到下面的文本框中<br/>字段：样本号、体积、Nanodrop浓度、260/230、260/280、Qubit浓度、RIN");
				$("#many_bat_text").val("");
				$("#many_bat_text").attr("style", "width:465px;height: 339px");
				var options = {};
				options.width = 494;
				options.height = 508;
				loadDialogPage($("#many_bat_div"), "批量导入", null, {
					"确定" : function() {
						var positions = $("#many_bat_text").val();
						alert(positions);
						if (!positions) {
							message("请填写信息");
							return;
						}
						var posiObj = {};
						var posiObj1 = {};
						var posiObj2 = {};
						var posiObj3 = {};
						var posiObj4 = {};
						var posiObj5 = {};
						var array = formatData(positions.split("\n"));
						$.each(array, function(i, obj) {
							var tem = obj.split("\t");
							posiObj[tem[0]] = tem[1];
							posiObj1[tem[0]] = tem[2];
							posiObj2[tem[0]] = tem[3];
							posiObj3[tem[0]] = tem[4];
							posiObj4[tem[0]] = tem[5];
							posiObj5[tem[0]] = tem[6];
						});
						var records = qaTaskResultGrid.getAllRecord();
						qaTaskResultGrid.stopEditing();
						$.each(records, function(i, obj) {
							if (posiObj[obj.get("code")]) {
								obj.set("volume", posiObj[obj.get("code")]);
							}
							if (posiObj1[obj.get("code")]) {
								obj.set("contraction", posiObj1[obj.get("code")]);
							}
							if (posiObj2[obj.get("code")]) {
								obj.set("od260", posiObj2[obj.get("code")]);
							}
							if (posiObj3[obj.get("code")]) {
								obj.set("od280", posiObj3[obj.get("code")]);
							}
							if (posiObj4[obj.get("code")]) {
								obj.set("qbcontraction", posiObj4[obj.get("code")]);
							}
							if (posiObj5[obj.get("code")]) {
								obj.set("rin", posiObj5[obj.get("code")]);
							}																
						});
						qaTaskResultGrid.startEditing(0, 0);
						$(this).dialog("close");
					}
				}, true, options);
			}
		});	*/		 	 
	 opts.tbar.push({
			text : "批量结果",
			handler : setResult
	 });
/*	opts.tbar.push({
		text : "批量下一步",
		handler : null
	});

	opts.tbar.push({
		text : '导出列表',
		handler : null
	});
	opts.tbar.push({
		text : '提交样本',
		handler : null
	});*/
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	}
	qaTaskResultGrid=gridEditTable("qaTaskResultdiv",cols,loadParam,opts);
	$("#qaTaskResultdiv").data("qaTaskResultGrid", qaTaskResultGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function exportexcel() {
	qaTaskResultGrid.title = '导出列表';
	var vExportContent = qaTaskResultGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}

var loadDicType2;	
//查询样本类型
function loadTestDicType2(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadDicType2=loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action?a=2", {
			"确定" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qaTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("dicSampleType-id", b.get("id"));
							obj.set("dicSampleType-name", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function setDicType2(){
	var operGrid = $("#show_dialog_dicType_div").data("dicTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = qaTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {
			$.each(records, function(a, b) {
				b.set("dicType-id", obj.get("id"));
				b.set("dicType-name", obj.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadDicType2.dialog("close");

}

var loadNextFlow;
//下一步流向
function loadTestNextFlowCob(){
	var records1 = qaTaskResultGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadNextFlow=loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=QaTask&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qaTaskResultGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}

function setNextFlow(){
	var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var records = qaTaskResultGrid.getSelectRecord();
	if (selectRecord.length > 0) {
		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				obj.set("nextFlowId", b.get("id"));
				obj.set("nextFlow", b.get("name"));
			});
		});
	}else{
		message("请选择您要选择的数据");
		return;
	}
	loadNextFlow.dialog("close");
}	
	
//提交样本
function submit(){
	var id=$("#qaTask_id").val();  
	if(qaTaskResultGrid.getModifyRecord().length > 0){
		message("请先保存记录！");
		return;
	}
	var record = qaTaskResultGrid.getSelectionModel().getSelections();
	var flg=false;
	for(var i=0;i<record.length;i++){
		if(!record[i].get("submit")){
			flg=true;
		}
		if(record[i].get("nextFlowId")==""){
			message("有下一步未填写！");
			return;
		}
	}
	var grid=qaTaskResultGrid.store;
	if(flg){
		var loadMarsk = new Ext.LoadMask(Ext.getBody(),
				{
				        msg : '正在处理，请稍候。。。。。。',
				        removeMask : true// 完成后移除
				    });
		loadMarsk.show();				
		var records = [];						
		for ( var i = 0; i < record.length; i++) {
				records.push(record[i].get("id"));
		}		
		ajax("post", "/experiment/qa/qaTask/submit.action", {
			id : id,
			ids : records
		}, function(data) {
			if (data.success) {
				loadMarsk.hide();
				qaTaskResultGrid.getStore().commitChanges();
				qaTaskResultGrid.getStore().reload();
				message("提交成功！");
			} else {
				loadMarsk.hide();
				message("提交失败！");
			}
		}, null);
	}else{
		message("没有需要提交的样本！");
	}
}
function setResult(){
	var options = {};
	options.width = 400;
	options.height = 300;
	var records = qaTaskResultGrid.getSelectRecord();
	if(records.length>0){
	loadDialogPage($("#bat_result_div"), "批量结果", null, {
		"确定" : function() {
			if (records && records.length > 0) {
				var result = $("#result").val();
				qaTaskResultGrid.stopEditing();
				$.each(records, function(i, obj) {
					obj.set("handleResult", result);
				});
				qaTaskResultGrid.startEditing(0, 0);
			}
			
			$(this).dialog("close");
		}
	}, true, options);}else{
		message("请选择您要选择的数据");
		return;
	}
}

