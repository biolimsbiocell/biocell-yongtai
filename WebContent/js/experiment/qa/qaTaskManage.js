var qaTaskManageGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'volume',
		type:"string"
	});
	fields.push({
		name:'note',
		type:"string"
	});
	fields.push({
		name:'result',
		type:"string"
	});
	fields.push({
		name:'nextFlow',
		type:"string"
	});
    fields.push({
		name:'nextFlowId',
		type:"string"
	});
    fields.push({
		name:'code',
		type:"string"
	});
    fields.push({
		name:'sampleCode',
		type:"string"
	});
    fields.push({
		name:'patientName',
		type:"string"
	});
    fields.push({
		name:'sequenceFun',
		type:"string"
	});   
	fields.push({
		name:'productName',
		type:"string"
	});
    fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'inspectDate',
		type:"string"
	});
	fields.push({
		name : 'orderId',
		type : "string"
	});
	fields.push({
		name : 'idCard',
		type : "string"
	});
	fields.push({
		name : 'phone',
		type : "string"
	});
	fields.push({
		name : 'reportDate',
		type : "string"
	});
	fields.push({
		name:'classify',
		type:"string"
	});
	fields.push({
		name:'dicType-id',
		type:"string"
	});
    fields.push({
		name:'dicType-name',
		type:"string"
	});
	
	fields.push({
		name:'sampleType',
		type:"string"
	});
	fields.push({
		name:'labCode',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		hidden : true,
		sortable:true
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6
	});
	
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:30*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证号',
		width:30*6
	});
	cm.push({
		dataIndex:'phone',
		header:'手机号',
		hidden:true,
		width:20*6
	});
	cm.push({
		dataIndex:'sequenceFun',
		header:'检测方法',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',
		width:20*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		header:'检测项目ID',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'dicType-id',
		hidden:true,
		header:'中间产物编号',
		width:15*10,
		sortable:true
	});
	var testDicType2 =new Ext.form.TextField({
        allowBlank: false
	});
	testDicType2.on('focus', function() {
		loadTestDicType2();
	});
	cm.push({
		dataIndex:'dicType-name',
		header:'中间产物类型',
		width:15*10,
		sortable:true,
	});
	cm.push({
		dataIndex:'inspectDate',
		header:'取样时间',
		width:30*6,
		hidden : false,
		sortable:true
	});
	cm.push({
		dataIndex:'reportDate',
		hidden : false,
		header:'应出报告日期',
		width:30*6,
		sortable:true
	});
	cm.push({
		dataIndex:'orderId',
		header:'关联任务单',
		hidden:false,
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'volume',
		header:'体积',
		hidden:true,
		width:20*6
	});

	cm.push({
		dataIndex:'nextFlowId',
		hidden:true,
		header:'下一步流向编号',
		width:15*10,
		sortable:true
	});
	var nextFlowCob =new Ext.form.TextField({
        allowBlank: false
	});
	nextFlowCob.on('focus', function() {
		loadTestNextFlowCob();
	});
	cm.push({
		dataIndex:'nextFlow',
		header:'下一步流向',
		width:15*10,
		sortable:true,		
	});
	var storeresultCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', '合格' ], [ '0', '不合格' ]]
	});
	var resultCob = new Ext.form.ComboBox({
		store : storeresultCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果',
		width:20*6,		
		renderer : Ext.util.Format.comboRenderer(resultCob)
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*6,		
		sortable:true
	});
	cm.push({
		dataIndex : 'classify',
		header : '临床/科技服务 ',
		width : 20 * 6,
		hidden : true,
		sortable : true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/qaTaskManage/showQaTaskManageListJson.action";
	var opts={};
	opts.tbar = [];
	opts.title="QA任务结果管理";
	opts.height=document.body.clientHeight-200;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	qaTaskManageGrid=gridEditTable("show_qaTaskManage_div",cols,loadParam,opts);
	$("#show_qaTaskManage_div").data("qaTaskManageGrid", qaTaskManageGrid);

});

//保存
function save(){	
	var itemJson = commonGetModifyRecords(qaTaskManageGrid);
	if(itemJson.length>0){
			ajax("post", "/experiment/qa/qaTaskManage/saveQA任务Manage.action", {
				itemDataJson : itemJson
			}, function(data) {
				if (data.success) {			
					qaTaskManageGrid.getStore().commitChanges();
					qaTaskManageGrid.getStore().reload();
					message("保存成功！");
				} else {
					message("保存失败！");
				}
			}, null);			
	}else{
		message("没有需要保存的数据！");
	}
}

function selectQaTaskInfo(){
	commonSearchAction(qaTaskManageGrid);
	$("#qaTaskManage_code").val("");
	$("#qaTaskManage_Code").val("");
}

//下一步流向
function loadTestNextFlowCob(){
	var records1 = qaTaskManageGrid.getSelectRecord();
	var productId="";
	$.each(records1, function(j, k) {
		productId=k.get("productId");
	});
	 var options = {};
		options.width = 500;
		options.height = 500;
		loadDialogPage(null, "选择下一步流向", "/system/nextFlow/nextFlow/shownextFlowDialog.action?model=QaTask&productId="+productId, {
			"确定" : function() {
				var operGrid = $("#show_dialog_nextFlow_div1").data("shownextFlowDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = qaTaskManageGrid.getSelectRecord();
				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("nextFlowId", b.get("id"));
							obj.set("nextFlow", b.get("name"));
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}