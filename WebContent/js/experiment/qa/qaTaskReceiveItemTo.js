﻿var qaTaskReceiveItemTogrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	   });
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	   fields.push({
			name:'name',
			type:"string"
		});
	   fields.push({
			name:'patientName',
			type:"string"
		});
	   fields.push({
			name:'sampleType',
			type:"string"
		});
	   fields.push({
			name:'productId',
			type:"string"
		});
	   fields.push({
			name:'productName',
			type:"string"
		});
		   
	   fields.push({
			name:'inspectDate',
			type:"string"
		});
	   fields.push({
			name:'acceptDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	   fields.push({
			name:'volume',
			type:"string"
		});
	   fields.push({
			name:'unit',
			type:"string"
		});
	   fields.push({
			name:'state',
			type:"string"
		});
	   fields.push({
			name:'note',
			type:"string"
		});
	   fields.push({
			name:'concentration',
			type:"string"
		});
				   
	   fields.push({
			name:'result',
			type:"string"
		});
	   fields.push({
			name:'reason',
			type:"string"
		});
	   fields.push({
			name:'idCard',
			type:"string"
		});
	   fields.push({
			name:'sequencingFun',
			type:"string"
		});
	   fields.push({
			name:'reportDate',
			type:"date",
			dateFormat:"Y-m-d"
		});
	   fields.push({
			name:'orderId',
			type:"string"
		});
	   fields.push({
			name:'phone',
			type:"string"
		});
	   
	   fields.push({
			name:'classify',
			type:"string"
		});
	   fields.push({
			name:'sampleNum',
			type:"string"
		});
	   fields.push({
			name : 'labCode',
			type : "string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'id',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'样本编号',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		hidden : false,
		header:'检测项目',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'labCode',
		hidden : false,
		header:'实验室样本号',
		width:20*6
	});
	cm.push({
		dataIndex:'productId',
		hidden : true,
		header:'检测项目',
		width:20*6
	});
	cm.push({
		dataIndex:'name',
		hidden : true,
		header:'描述',
		width:20*6
	});
	cm.push({
		dataIndex:'patientName',
		hidden : true,
		header:'患者姓名',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleType',
		hidden : false,
		header:'样本类型',
		width:20*6,
		sortable:true
	});
	
	cm.push({
		dataIndex:'inspectDate',
		hidden : true,
		header:'取样时间',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'acceptDate',
		hidden : false,
		header:'接收日期',
		width:20*10,
		sortable:true,
		renderer: formatDate
		//editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'state',
		hidden : true,
		header:'状态',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleNum',
		header:'样本数量',
		width:20*6
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6
	});
	cm.push({
		dataIndex:'sequencingFun',
		hidden : true,
		header:'检测方法',
		width:20*6
	});
	
	cm.push({
		dataIndex:'reportDate',
		hidden : true,
		header:'应出报告日期',
		width:20*10,
		sortable:true,
		renderer: formatDate
	});
	cm.push({
		dataIndex:'volume',
		hidden : true,
		header:'体积',
		width:20*6
	});
	cm.push({
		dataIndex:'unit',
		hidden : true,
		header:'单位',
		width:20*6
	});
	cm.push({
		dataIndex:'concentration',
		hidden : true,
		header:'浓度',
		width:20*10
	});
	cm.push({
		dataIndex:'result',
		hidden : true,
		header:'结果',
		width:20*10
	});
	cm.push({
		dataIndex:'reason',
		hidden : true,
		header:'失败原因',
		width:20*6
	});
	cm.push({
		dataIndex:'idCard',
		hidden : true,
		header:'身份证号',
		width:20*6
	});
	cm.push({
		dataIndex:'orderId',
		hidden : true,
		header:'任务单id',
		width:20*6
	});
	cm.push({
		dataIndex:'phone',
		hidden : true,
		header:'手机号',
		width:20*6
	});
	cm.push({
		dataIndex:'classify',
		hidden : true,
		header:'临床/科技服务',
		width:20*6
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/qa/qaTaskReceive/qaTaskReceiveItemTogrid.action";
	var opts={};
	opts.title="待接收样本";
	opts.height =  document.body.clientHeight-30;
	opts.tbar = [];
    
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '填加明细',
		handler : null
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
	opts.tbar.push({
		iconCls : 'application_taskadd',
		text : '添加到任务',
		handler : addItem
	});
	opts.tbar.push({
		iconCls : 'application_search',
		text : biolims.common.checkCode,
		handler : function() {
			$("#many_bat_text").val("");
			var options = {};
			options.width = 474;
			options.height = 480;
			loadDialogPage(
					$("#many_bat_div"),
					biolims.common.checkCode,
					null,
					{
						"确定" : function() {
							var positions = $("#many_bat_text").val();
							if (!positions) {
								message("请填写条码号！");
								return;
							}
							var array = positions.split("\n");
							var records = qaTaskReceiveItemTogrid.getAllRecord();
							var store = qaTaskReceiveItemTogrid.store;

							var isOper = true;
							var buf = [];
							qaTaskReceiveItemTogrid.stopEditing();
							$.each(array,function(i, obj) {
								
								
								$.each(records, function(i, obj1) {
									if(obj==obj1.get("sampleCode")){
										buf.push(store.indexOfId(obj1.get("id")));
										
									}
									
								});
							});
							qaTaskReceiveItemTogrid.getSelectionModel().selectRows(buf);
							//$("#show_plasma_storage_in_item_div").data("isOper", isOper);
							if(isOper==false){
								
								message("样本号核对不符，请检查！");
								
							}else{
								
								//message("样本号核对完毕！");
								addItem();
							}
							qaTaskReceiveItemTogrid.startEditing(0, 0);
							$(this).dialog("close");
						}
					}, true, options);

		}
	});
	qaTaskReceiveItemTogrid=gridEditTable("qaTaskReceiveItemTodiv",cols,loadParam,opts);
	$("#qaTaskReceiveItemTodiv").data("qaTaskReceiveItemTogrid", qaTaskReceiveItemTogrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});



	//从左边添加到右边的明细中
	function addItem(){
		var selRecord = qaTaskReceiveItemTogrid.getSelectionModel().getSelections();//从左边获取数据
		var getRecord = qaTaskReceiveItemGrid.store;//填充到当前的明细中
		if(selRecord.length >0){
			$.each(selRecord,function(i, obj){
				var isRepeat = false;
				for(var j=0; j<getRecord.getCount();j++){
					var getData = getRecord.getAt(j).get("tempId");
					if(getData==obj.get("id")){
						isRepeat = true;
						message("有重复的数据，请重新选择！");
						break;
					}
				}
				if(!isRepeat){
					var ob = qaTaskReceiveItemGrid.getStore().recordType;
					qaTaskReceiveItemGrid.stopEditing();
					var p= new ob({});
					p.set("tempId",obj.get("id"));
					p.set("sampleCode", obj.get("sampleCode"));
					p.set("name", obj.get("name"));
					p.set("patientName", obj.get("patientName"));
					p.set("productId", obj.get("productId"));
					p.set("productName", obj.get("productName"));
					p.set("inspectDate", obj.get("inspectDate"));
					p.set("idCard", obj.get("idCard"));
					p.set("sequencingFun", obj.get("sequencingFun"));
					p.set("reportDate", obj.get("reportDate"));
					p.set("orderId", obj.get("orderId"));
					p.set("phone", obj.get("phone"));
					
					p.set("note", obj.get("note"));
					p.set("state", obj.get("state"));
					p.set("stateName", obj.get("stateName"));
					p.set("storage-id", obj.get("storage-id"));
					p.set("storage-name", obj.get("storage-name"));
					p.set("result", obj.get("result"));
					p.set("reason", obj.get("reason"));
					p.set("acceptDate", obj.get("acceptDate"));
					p.set("volume", obj.get("volume"));
					p.set("unit", obj.get("unit"));
					p.set("concentration", obj.get("concentration"));
					p.set("sampleType", obj.get("sampleType"));
					p.set("sampleNum", obj.get("sampleNum"));
					p.set("labCode", obj.get("labCode"));
					qaTaskReceiveItemGrid.getStore().add(p);
					
				}
					
					
			});
			
			qaTaskReceiveItemGrid.startEditing(0,0);
		}else{
			message("请选择样本");
		}
	}