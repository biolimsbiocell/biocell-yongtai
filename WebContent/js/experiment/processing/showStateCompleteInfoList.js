$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.name,
	});
	colOpts.push({
		"data": "createUser-name",
		"title": biolims.sample.createUserName,
	});
	colOpts.push({
		"data": "createDate",
		"title": biolims.sample.createDate,
	});
	var tbarOpts = [];
	var options = table(false, null,
		'/experiment/processing/productProcessing/showStateCompleteInfoDialogListJson.action', colOpts, tbarOpts)
	var addStateCompleteInfo = renderData($("#addStateCompleteInfo"), options);
	$("#addStateCompleteInfo").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addStateCompleteInfo_wrapper .dt-buttons").empty();
			$('#addStateCompleteInfo_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addStateCompleteInfo tbody tr");
			addStateCompleteInfo.ajax.reload();
			addStateCompleteInfo.on('draw', function() {
				trs = $("#addStateCompleteInfo tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})