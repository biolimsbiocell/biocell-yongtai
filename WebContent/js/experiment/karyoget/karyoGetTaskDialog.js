var karyoGetTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'sampleType-id',
		type:"string"
	});
	    fields.push({
		name:'sampleType-name',
		type:"string"
	});
	    fields.push({
		name:'template-id',
		type:"string"
	});
	    fields.push({
		name:'template-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:'创建人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建时间',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'sampleType-id',
		header:'样本类型ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'sampleType-name',
		header:'样本类型',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-id',
		header:'实验模板ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'template-name',
		header:'实验模板',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:'状态',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'状态名称',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'note',
		header:'备注',
		width:50*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyoget/karyoGetTask/showKaryoGetTaskListJson.action";
	var opts={};
	opts.title="样本收获";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setKaryoGetTaskFun(rec);
	};
	karyoGetTaskDialogGrid=gridTable("show_dialog_karyoGetTask_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(karyoGetTaskDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
