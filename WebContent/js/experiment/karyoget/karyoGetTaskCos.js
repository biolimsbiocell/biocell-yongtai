var karyoGetTaskCosGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'instrumentCode',
		type:"string"
	});
	   fields.push({
		name:'instrumentName',
		type:"string"
	});
	   fields.push({
		name:'isCheck',
		type:"string"
	});
	   fields.push({
		name:'temperature',
		type:"string"
	});
	   fields.push({
		name:'speed',
		type:"string"
	});
	   fields.push({
		name:'time',
		type:"date",
		dateFormat:"Y-m-d"
	});
	    fields.push({
		name:'karyoGetTask-id',
		type:"string"
	});
	    fields.push({
		name:'karyoGetTask-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'instrumentCode',
		hidden : false,
		header:biolims.common.instrumentNo,
		width:20*6
	});
	cm.push({
		dataIndex:'instrumentName',
		hidden : false,
		header:biolims.common.instrumentName,
		width:20*6
	});
	cm.push({
		dataIndex:'isCheck',
		hidden : false,
		header:biolims.common.isTest,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'temperature',
		hidden : false,
		header:biolims.common.temperature,
		width:20*6
	});
	cm.push({
		dataIndex:'speed',
		hidden : false,
		header:biolims.common.speed,
		width:20*6
	});
	cm.push({
		dataIndex:'time',
		hidden : false,
		header:biolims.common.time,
		width:20*6
	});
	cm.push({
		dataIndex:'karyoGetTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'karyoGetTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyoget/karyoGetTask/showKaryoGetTaskCosListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.instrumentDetail;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyoget/karyoGetTask/delKaryoGetTaskCos.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				karyoGetTaskCosGrid.getStore().commitChanges();
				karyoGetTaskCosGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	karyoGetTaskCosGrid=gridEditTable("karyoGetTaskCosdiv",cols,loadParam,opts);
	$("#karyoGetTaskCosdiv").data("karyoGetTaskCosGrid", karyoGetTaskCosGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});
