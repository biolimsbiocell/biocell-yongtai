var karyoGetTaskReagentGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'ReagentCode',
		type:"string"
	});
	   fields.push({
		name:'ReagentName',
		type:"string"
	});
	   fields.push({
		name:'batch',
		type:"string"
	});
	   fields.push({
		name:'isCheck',
		type:"string"
	});
	   fields.push({
		name:'singleDosage',
		type:"string"
	});
	   fields.push({
		name:'reactionDosage',
		type:"string"
	});
	   fields.push({
		name:'dosage',
		type:"string"
	});
	    fields.push({
		name:'karyoGetTask-id',
		type:"string"
	});
	    fields.push({
		name:'karyoGetTask-name',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ReagentCode',
		hidden : false,
		header:biolims.common.reagentNo,
		width:25*6
	});
	cm.push({
		dataIndex:'ReagentName',
		hidden : false,
		header:biolims.common.reagentName,
		width:20*6
	});
	cm.push({
		dataIndex:'batch',
		hidden : false,
		header:biolims.common.batch,
		width:20*6
	});
	cm.push({
		dataIndex:'isCheck',
		hidden : false,
		header:biolims.common.isTest,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'singleDosage',
		hidden : false,
		header:biolims.common.singleDose,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'reactionDosage',
		hidden : true,
		header:'反应用量',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals:true,
			decimalPrecision:2
		})
	});
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:biolims.common.sampleNum,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'dosage',
		hidden : false,
		header:biolims.common.dose,
		width:20*6
		
//		editor : new Ext.form.NumberField({
//			allowDecimals:true,
//			decimalPrecision:2
//		})
	});
	cm.push({
		dataIndex:'karyoGetTask-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'karyoGetTask-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:biolims.common.note,
		width:30*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/karyoget/karyoGetTask/showKaryoGetTaskReagentListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.common.reagentDetail;
	opts.height =  document.body.clientHeight-300;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/experiment/karyoget/karyoGetTask/delKaryoGetTaskReagent.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				karyoGetTaskReagentGrid.getStore().commitChanges();
				karyoGetTaskReagentGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	karyoGetTaskReagentGrid=gridEditTable("karyoGetTaskReagentdiv",cols,loadParam,opts);
	$("#karyoGetTaskReagentdiv").data("karyoGetTaskReagentGrid", karyoGetTaskReagentGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});