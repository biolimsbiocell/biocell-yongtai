$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	if($("#karyoGetTask_state").val()=="3"){
		load("/experiment/karyoget/karyoGetTask/showKaryoGetTaskTempList.action", {
			id : $("#karyoGetTask_id").val()
		}, "#karyoGetTaskTemppage");
		$("#markup").css("width","70%");
	}else{
		$("#showtemplate").css("display","none");
		$("#karyoGetTaskTemppage").css("display","none");
		$("#markup").css("width","100%");
	}
	setTimeout(function() {
		var getGrid=karyoGetTaskTemplateGrid.store;
		if(getGrid.getCount()==0){
			//alert(getGrid.getCount());
			loadTemplate($("#karyoGetTask_template").val());
		}
	}, 2000);
});	
function add() {
	window.location = window.ctx + "/experiment/karyoget/karyoGetTask/editKaryoGetTask.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/experiment/karyoget/karyoGetTask/showKaryoGetTaskList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
//	var type=$("#karyoGetTask_sampleType").val();
	var template=$("#karyoGetTask_template").val();
//	if(type==""||type==null||type==undefined){
//		message("请选择样本类型！");
//		return;
//	}
	if(template==""||template==null||template==undefined){
		message("请选择实验模板！");
		return;
	}
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("KaryoGetTask", {
					userId : userId,
					userName : userName,
					formId : $("#karyoGetTask_id").val(),
					title : $("#karyoGetTask_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
	
	var codeList = new Array();
	var flag=true;
	var flag1=true;
	if (karyoGetTaskResultGrid.getAllRecord().length > 0) {
		var selRecord = karyoGetTaskResultGrid.store;
		for(var j=0;j<selRecord.getCount();j++){
			codeList.push(selRecord.getAt(j).get("sampleCode"));
			if(selRecord.getAt(j).get("isrStandard")==""){
				message("结果不能为空！");
				return;
			}
			if(selRecord.getAt(j).get("nextFlowId")==""){
				message("下一步不能为空！");
				return;
			}
		}
		if(karyoGetTaskResultGrid.getModifyRecord().length > 0){
			message("请先保存记录！");
			return;
		}
		
		
		completeTask($("#karyoGetTask_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
	}
});






function save() {
if(checkSubmit()==true){
	    var karyoGetTaskItemDivData = $("#karyoGetTaskItemdiv").data("karyoGetTaskItemGrid");
		document.getElementById('karyoGetTaskItemJson').value = commonGetModifyRecords(karyoGetTaskItemDivData);
	    var karyoGetTaskTemplateDivData = $("#karyoGetTaskTemplatediv").data("karyoGetTaskTemplateGrid");
		document.getElementById('karyoGetTaskTemplateJson').value = commonGetModifyRecords(karyoGetTaskTemplateDivData);
	    var karyoGetTaskReagentDivData = $("#karyoGetTaskReagentdiv").data("karyoGetTaskReagentGrid");
		document.getElementById('karyoGetTaskReagentJson').value = commonGetModifyRecords(karyoGetTaskReagentDivData);
	    var karyoGetTaskCosDivData = $("#karyoGetTaskCosdiv").data("karyoGetTaskCosGrid");
		document.getElementById('karyoGetTaskCosJson').value = commonGetModifyRecords(karyoGetTaskCosDivData);
	    var karyoGetTaskResultDivData = $("#karyoGetTaskResultdiv").data("karyoGetTaskResultGrid");
		document.getElementById('karyoGetTaskResultJson').value = commonGetModifyRecords(karyoGetTaskResultDivData);
//	    var karyoGetTaskTempDivData = $("#karyoGetTaskTempdiv").data("karyoGetTaskTempGrid");
//		document.getElementById('karyoGetTaskTempJson').value = commonGetModifyRecords(karyoGetTaskTempDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/experiment/karyoget/karyoGetTask/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/experiment/karyoget/karyoGetTask/copyKaryoGetTask.action?id=' + $("#karyoGetTask_id").val();
}

$("#toolbarbutton_status").click(function(){
	if ($("#karyoGetTask_id").val()){
		commonChangeState("formId=" + $("#karyoGetTask_id").val() + "&tableId=KaryoGetTask");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#karyoGetTask_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#karyoGetTask_acceptUser").val());
	nsc.push("实验组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.sampleHarvest,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/experiment/karyoget/karyoGetTask/showKaryoGetTaskItemList.action", {
				id : $("#karyoGetTask_id").val()
			}, "#karyoGetTaskItempage");
load("/experiment/karyoget/karyoGetTask/showKaryoGetTaskTemplateList.action", {
				id : $("#karyoGetTask_id").val()
			}, "#karyoGetTaskTemplatepage");
load("/experiment/karyoget/karyoGetTask/showKaryoGetTaskReagentList.action", {
				id : $("#karyoGetTask_id").val()
			}, "#karyoGetTaskReagentpage");
load("/experiment/karyoget/karyoGetTask/showKaryoGetTaskCosList.action", {
				id : $("#karyoGetTask_id").val()
			}, "#karyoGetTaskCospage");
load("/experiment/karyoget/karyoGetTask/showKaryoGetTaskResultList.action", {
				id : $("#karyoGetTask_id").val()
			}, "#karyoGetTaskResultpage");
load("/experiment/karyoget/karyoGetTask/showKaryoGetTaskTempList.action", {
				id : $("#karyoGetTask_id").val()
			}, "#karyoGetTaskTemppage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

var item = menu.add({
			    	text: '复制'
					});
item.on('click', editCopy);


/**
 * 调用模板
 */
function TemplateFun(){
	var type="doCkaGet";
	var win = Ext.getCmp('TemplateFun');
	if (win) {win.close();}
	var TemplateFun= new Ext.Window({
	id:'TemplateFun',modal:true,title:'选择模板',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/system/template/template/templateSelectByType.action?flag=TemplateFun&type="+type+"' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 TemplateFun.close(); }  }]  }); 
	 TemplateFun.show(); 
}
function setTemplateFun(rec){
	//把实验模板中的中间产物类型和数量带入
//	var itemGrid=karyoGetTaskItemGrid.store;
//	if(itemGrid.getCount()>0){
//		for(var i=0;i<itemGrid.getCount();i++){
//			itemGrid.getAt(i).set("dicSampleType-id",rec.get('dicSampleType-id'));
//			itemGrid.getAt(i).set("dicSampleType-name",rec.get('dicSampleType-name'));
//			itemGrid.getAt(i).set("productNum",rec.get('productNum'));
//		}
//	}
	document.getElementById('karyoGetTask_acceptUser').value=rec.get('acceptUser-id');
	document.getElementById('karyoGetTask_acceptUser_name').value=rec.get('acceptUser-name');
	var code=$("#karyoGetTask_template").val();
	if(code==""){
	document.getElementById('karyoGetTask_template').value=rec.get('id');
	document.getElementById('karyoGetTask_template_name').value=rec.get('name');
	var win = Ext.getCmp('TemplateFun');
	if(win){win.close();}
	var id=rec.get('id');
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoGetTaskTemplateGrid.getStore().recordType;
				karyoGetTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("stepNum",obj.code);
					p.set("stepDescribe",obj.name);
					p.set("note",obj.note);
					karyoGetTaskTemplateGrid.getStore().add(p);							
					karyoGetTaskTemplateGrid.startEditing(0, 0);		
				});
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoGetTaskReagentGrid.getStore().recordType;
				karyoGetTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					karyoGetTaskReagentGrid.getStore().add(p);							
					karyoGetTaskReagentGrid.startEditing(0, 0);		
				});
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoGetTaskCosGrid.getStore().recordType;
				karyoGetTaskCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("instrumentCode",obj.code);
					p.set("instrumentName",obj.name);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					karyoGetTaskCosGrid.getStore().add(p);							
					karyoGetTaskCosGrid.startEditing(0, 0);		
				});			
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
	}else{
		if(rec.get('id')==code){
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
	}else{
		var ob1 = karyoGetTaskTemplateGrid.store;
		if (ob1.getCount() > 0) {
			for(var j=0;j<ob1.getCount();j++){
				var oldv = ob1.getAt(j).get("id");
				//根据ID删除
				if(oldv!=null){
					ajax("post", "/experiment/karyoget/karyoGetTask/delKaryoGetTaskTemplateOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null);
				}else{								
					karyoGetTaskTemplateGrid.store.removeAll();
				}
			}
			karyoGetTaskTemplateGrid.store.removeAll();
		}
		var ob2 = karyoGetTaskReagentGrid.store;
		if (ob2.getCount() > 0) {
			for(var j=0;j<ob2.getCount();j++){
				var oldv = ob2.getAt(j).get("id");
				if(oldv!=null){
					ajax("post", "/experiment/karyoget/karyoGetTask/delKaryoGetTaskReagentOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null); 
				}else{
					karyoGetTaskReagentGrid.store.removeAll();
				}
			}
			karyoGetTaskReagentGrid.store.removeAll();
		}
		var ob3 = karyoGetTaskCosGrid.store;
		if (ob3.getCount() > 0) {
			for(var j=0;j<ob3.getCount();j++){
				var oldv = ob3.getAt(j).get("id");
				if(oldv!=null){
					ajax("post", "/experiment/karyoget/karyoGetTask/delKaryoGetTaskCosOne.action", {
						ids : oldv
					}, function(data) {
						if (data.success) {
							message("删除成功！");
						} else {
							message("删除失败！");
						}
					}, null); 
				}else{
					karyoGetTaskCosGrid.store.removeAll();
				}
			}
			karyoGetTaskCosGrid.store.removeAll();
		}
		document.getElementById('karyoGetTask_template').value=rec.get('id');
		document.getElementById('karyoGetTask_template_name').value=rec.get('name');
		var win = Ext.getCmp('TemplateFun');
		if(win){win.close();}
		var id = rec.get('id');
		ajax("post", "/system/template/template/setTemplateItem.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoGetTaskTemplateGrid.getStore().recordType;
					karyoGetTaskTemplateGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tItem",obj.id);
						p.set("stepNum",obj.code);
						p.set("stepDescribe",obj.name);
						p.set("note",obj.note);
						karyoGetTaskTemplateGrid.getStore().add(p);							
						karyoGetTaskTemplateGrid.startEditing(0, 0);		
					});
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateReagent.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoGetTaskReagentGrid.getStore().recordType;
					karyoGetTaskReagentGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tReagent",obj.id);
						p.set("agentiaCode",obj.code);
						p.set("agentiaName",obj.name);
						p.set("batch",obj.batch);
						p.set("isCheck",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("singleDosage",obj.num);
						p.set("note",obj.note);
						p.set("sn",obj.sn);
						karyoGetTaskReagentGrid.getStore().add(p);							
						karyoGetTaskReagentGrid.startEditing(0, 0);		
					});
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
			ajax("post", "/system/template/template/setTemplateCos.action", {
			code : id,
			}, function(data) {
				if (data.success) {	
					var ob = karyoGetTaskCosGrid.getStore().recordType;
					karyoGetTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						p.set("tCos",obj.id);
						p.set("instrumentCode",obj.code);
						p.set("instrumentName",obj.name);
						p.set("isCheck",obj.isGood);
						p.set("itemId",obj.itemId);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("time",obj.time);
						p.set("note",obj.note);
						karyoGetTaskCosGrid.getStore().add(p);							
						karyoGetTaskCosGrid.startEditing(0, 0);		
					});			
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null); 
		}
	}
}

/**
 * 按条件加载原辅料
 */
function showReagent(){
	//获取全部数据
	var allRcords=karyoGetTaskTemplateGrid.store;
	var flag=true;
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag=false;
		}
	}
	if(!flag){
		message("请先保存执行单数据!");
		return;
	}
	//获取选择的数据
	var selectRcords=karyoGetTaskTemplateGrid.getSelectionModel().getSelections();	
	//选中的数量
	var length1=selectRcords.length;
	//全部数据量
	var length2=allRcords.getCount();
	
	var tid=$("#karyoGetTask_id").val();
	if(length1==length2 || length1==0){
		load("/experiment/karyoget/karyoGetTask/showkaryoGetTaskReagentList.action", {
			id : $("#karyoGetTask_id").val()
		}, "#karyoGetTaskReagentpage");
	}else if(length1==1){
		karyoGetTaskReagentGrid.store.removeAll();
		$.each(selectRcords, function(i, obj) {
		var code=obj.get("code");
		ajax("post", "/experiment/karyoget/karyoGetTask/setReagent.action", {
			tid:tid,code : code
		}, function(data) {
			
			if (data.success) {	
				var ob = karyoGetTaskReagentGrid.getStore().recordType;
				karyoGetTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("id",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("sn",obj.sn);
					p.set("reactionDosage",obj.reactionDosage);
					p.set("note",obj.note);
					p.set("isGood",obj.isGood);
					p.set("dosage",obj.dosage);
					p.set("tReagent",obj.tReagent);
					p.set("karyoGetTask-id",obj.tId);
					p.set("karyoGetTask-name",obj.tName);
					
					karyoGetTaskReagentGrid.getStore().add(p);							
				});
				karyoGetTaskReagentGrid.startEditing(0, 0);		
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
		});		
	}else{
		message("请选择一条数据!");
		return;
	}
	
}

/**
 * 按条件加载设备
 */
function showCos(){
	//获取全部数据
	var allRcords=karyoGetTaskTemplateGrid.store;
	var flag="1";
	for(var h=0;h<allRcords.getCount();h++){
		var ida = allRcords.getAt(h).get("id");
		if(ida==undefined){
			flag="0";
		}else{
			flag="1";
		}
	}
	if(flag=="0"){
		message("请先保存执行单数据!");
		return;
	}else{
		//获取选择的数据
		var selectRcords=karyoGetTaskTemplateGrid.getSelectionModel().getSelections();
		//选中的数量
		var length1=selectRcords.length;
		//全部数据量
		var length2=allRcords.getCount();
		var tid=$("#karyoGetTask_id").val();
		if(length1==length2 || length1==0){
			load("/experiment/karyoget/karyoGetTask/showkaryoGetTaskCosList.action", {
				id : $("#karyoGetTask_id").val()
			}, "#karyoGetTaskCospage");
		}else if(length1==1){
			karyoGetTaskCosGrid.store.removeAll();
			$.each(selectRcords, function(i, obj) {
			var code=obj.get("code");
			ajax("post", "/experiment/karyoget/karyoGetTask/setCos.action", {
				tid:tid,code : code
			}, function(data) {
				
				if (data.success) {	
					var ob = karyoGetTaskCosGrid.getStore().recordType;
					karyoGetTaskCosGrid.stopEditing();
					$.each(data.data, function(i, obj) {
						var p = new ob({});
						p.isNew = true;
						
						p.set("id",obj.id);
						p.set("instrumentCode",obj.instrumentCode);
						p.set("instrumentName",obj.instrumentName);
						p.set("temperature",obj.temperature);
						p.set("speed",obj.speed);
						p.set("sampleNum",obj.sampleNum);
						p.set("time",obj.time);
						p.set("isCheck",obj.isCheck);
						p.set("itemId",obj.itemId);
						p.set("tCos",obj.tCos);
						p.set("karyoGetTask-id",obj.tId);
						p.set("karyoGetTask-name",obj.tName);
						p.set("note",obj.note);
						karyoGetTaskCosGrid.getStore().add(p);							
					});
					karyoGetTaskCosGrid.startEditing(0, 0);		
				} else {
					message("获取明细数据时发生错误！");
				}
			}, null);
			});		
		}else{
			message("请选择一条数据!");
			return;
		}
	}
}

//查询样本类型
function loadTestDicSampleType(){
	 var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				if (selectRecord.length > 0) {
					$.each(selectRecord, function(a, b) {
						$("#karyoGetTask_sampleType").val(b.get("id"));
						$("#karyoGetTask_sampleType_name").val(b.get("name"));
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '回滚'
		});
	item.on('click', ckcrk);
	
	});
function ckcrk(){
	
	Ext.MessageBox.confirm("提示", "是否初始化该任务单？", function(button, text) {
		if (button == "yes") {
			var selRecord = karyoGetTaskResultGrid.store;
			for(var j=0;j<selRecord.getCount();j++){
				var submit = selRecord.getAt(j).get("isCommit");
				if(submit==""){
					message("有样本未提交，不能初始化！");
					return;
				}
			}

			for(var j=0;j<selRecord.getCount();j++){
				var code = selRecord.getAt(j).get("code");
				var nextFlowId = selRecord.getAt(j).get("nextFlowId");
				if(nextFlowId!=null){
					ajax("post", "/system/nextFlow/nextFlow/RollBack.action", {
						code : code, nextFlowId : nextFlowId
					}, function(data) {
						if (data.success) {	
							message("回滚成功！");
							selRecord.getAt(j).set("isCommit","");
							save();
						} else {
							message("回滚失败！");
						}
					}, null);
				}
				
			}
		}
	});
}	
Ext.onReady(function(){
	var item = menu.add({
	    	text: '保存'
		});
	item.on('click', ckcrk2);
	
	});
function ckcrk2(){
	save();
}
Ext.onReady(function(){
	var item = menu.add({
	    	text: '办理回滚结果'
		});
	item.on('click', ckcrk3);
	
	});
function ckcrk3(){
	Ext.MessageBox.show({ msg: '正在办理回滚,请等待...', progressText: '办理中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	ajax("post", "/system/nextFlow/nextFlow/handleRollBack.action", {
		model : "KaryoGetTask",id : $("#karyoGetTask_id").val()
	}, function(data) {
		if (data.success) {	
			message("办理回滚成功！");
		} else {
			message("办理回滚失败！");
		}
	}, null);
}

//加载模板明细
function loadTemplate(id){
	ajax("post", "/system/template/template/setTemplateItem.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoGetTaskTemplateGrid.getStore().recordType;
				karyoGetTaskTemplateGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tItem",obj.id);
					p.set("stepNum",obj.code);
					p.set("stepDescribe",obj.name);
					p.set("note",obj.note);
					karyoGetTaskTemplateGrid.getStore().add(p);							
					karyoGetTaskTemplateGrid.startEditing(0, 0);		
				});
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateReagent.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoGetTaskReagentGrid.getStore().recordType;
				karyoGetTaskReagentGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tReagent",obj.id);
					p.set("agentiaCode",obj.code);
					p.set("agentiaName",obj.name);
					p.set("batch",obj.batch);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("singleDosage",obj.num);
					p.set("note",obj.note);
					p.set("sn",obj.sn);
					karyoGetTaskReagentGrid.getStore().add(p);							
					karyoGetTaskReagentGrid.startEditing(0, 0);		
				});
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null); 
		ajax("post", "/system/template/template/setTemplateCos.action", {
		code : id,
		}, function(data) {
			if (data.success) {
				var ob = karyoGetTaskCosGrid.getStore().recordType;
				karyoGetTaskCosGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					p.set("tCos",obj.id);
					p.set("instrumentCode",obj.code);
					p.set("instrumentName",obj.name);
					p.set("isCheck",obj.isGood);
					p.set("itemId",obj.itemId);
					p.set("temperature",obj.temperature);
					p.set("speed",obj.speed);
					p.set("time",obj.time);
					p.set("note",obj.note);
					karyoGetTaskCosGrid.getStore().add(p);							
					karyoGetTaskCosGrid.startEditing(0, 0);		
				});			
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
}