$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'actionName',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-id',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-tableName',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-entitypackage',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-entityname',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-type',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-orderNumber',
		type : "Integer"
	});
	fields.push({
		name : 'classPath',
		type : "string"
	});
	fields.push({
		name : 'modifyAttribute',
		type : "string"
	});
	fields.push({
		name : 'modifyValue',
		type : "string"
	});
	fields.push({
		name : 'exeFuncPath',
		type : "string"
	});
	fields.push({
		name : 'stateValue',
		type : "string"
	});
	fields.push({
		name : 'actionMethod',
		type : "string"
	});
	fields.push({
		name : 'type',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	fields.push({
		name : 'orderNumber',
		type : "string"
	});
	cols.fields = fields;
	var storeTypeCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '', '请选择' ],[ '0', '主表' ], [ '1', '页签' ] ]
	});
	var typeCob = new Ext.form.ComboBox({
		store : storeTypeCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'actionName',
		header : '事件名称',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	/*cm.push({
		dataIndex : 'pathName',
		header : '路径',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});*/
	var appType = new Ext.form.TextField({
		allowBlank : true
	});
	appType.on('focus', function() {
		var option = {};
		option.width = 475;
		option.height = 525;
		var url = "/app/applicationTypeTable/showDialogApplicationTypeTableList.action";
		var selApplicationTypeActionDialog = loadDialogPage(null, "选择表单类型", url, {
			"确定" : function() {
				var selGrid=$("#show_dialog_applicationtypetable_grid_div").data("waitApplicationtypeApplyGrid");
				var scRecord = selGrid.getSelectRecord();
				if (scRecord && scRecord[0]) {
					applicationTypeActionGrid.stopEditing();
					var myRecord=applicationTypeActionGrid.getSelectRecord();
					myRecord[0].set("applicationTypeTable-id", scRecord[0].get('id'));
					applicationTypeActionGrid.startEditing(0, 0);
				}
				$(selApplicationTypeActionDialog).dialog("close");
			}
		}, true, option);
	});
	cm.push({
		dataIndex : 'applicationTypeTable-id',
		header : '所属应用表单类型',
		width : 120,
		sortable : true,
		editor : appType
	});
	cm.push({
		dataIndex : 'applicationTypeTable-tableName',
		header : '表名',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'applicationTypeTable-entitypackage',
		header : '包名',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'applicationTypeTable-entityname',
		header : '实体名',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'applicationTypeTable-type',
		header : '类型',
		width : 120,
		sortable : true,
		editor : typeCob,
		renderer :Ext.util.Format.comboRenderer(typeCob)
	});
	cm.push({
		dataIndex : 'applicationTypeTable-orderNumber',
		header : '排序号',
		width : 120,
		sortable : true,
		editor : new Ext.form.NumberField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'classPath',
		header : '表单对应实体的类路径',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'modifyAttribute',
		header : '需要修改的属性',
		width : 120,
		sortable : true,
		hidden : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'modifyValue',
		header : '需要修改的值',
		width : 120,
		sortable : true,
		hidden : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'exeFuncPath',
		header : '需要修改的值的路径',
		width : 120,
		sortable : true,
		hidden : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'stateValue',
		header : '需要修改的状态值',
		width : 120,
		hidden : true,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'actionMethod',
		header : '0.修改状态 1.执行程序 2.修改属性',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'type',
		header : '类型',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		header : '状态',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'orderNumber',
		header : '排序号',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/app/applicationTypeAction/showApplicationTypeActionListJson.action";
	var opts = {};
	opts.title = "动作详细信息";
	opts.height = document.body.clientHeight-20;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/app/applicationTypeAction/delApplicationTypeAction.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				applicationTypeActionGrid.getStore().commitChanges();
				applicationTypeReceiveGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '保存',
		handler : function() {
			var result = {};
			if (result) {
				ajax("post", "/app/applicationTypeAction/saveApplicationTypeActionList.action", {
					itemDataJson : getItemData()
				}, function(data) {
					if (data.success) {
						applicationTypeActionGrid.getStore().commitChanges();
						applicationTypeActionGrid.getStore().reload();
						message("保存成功！");
					} else {
						message("保存失败！");
					}
				}, null);
			}
		}
	});
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_typeAction_div"),"批量上传",null,{
				"确定":function(){
					goInExcel1();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	var idTmr = "";
	function goInExcel1(){
		var file = document.getElementById("file-uploada").files[0];  
		var n = 0;
		var ob = applicationTypeReceiveGrid.getStore().recordType;
		var reader = new FileReader();  
		//将文件以文本形式读入页面  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[1]){
                			var p = new ob({});
                			p.isNew = true;				
                			p.set("id",this[0]);
                			p.set("name",this[1]);
							p.set("tableName",this[2]);
							p.set("entitypackage",this[3]);
							p.set("entityname",this[4]);
							p.set("type",this[5]);
							p.set("orderNumber",this[6]);
							p.set("pathName",this[7]);
							p.set("applicationType-id", this[8]);
							p.set("classPath",this[9]);
							p.set("workflowUserColumn",this[10]);
							p.set("state",this[11]);
							applicationTypeReceiveGrid.getStore().insert(0, p);
                		}
                	}
                    n = n +1;
               });
		}
	}
	applicationTypeActionGrid = gridEditTable("show_applicationtypeaction_grid_div", cols, loadParam, opts);
	var getItemData = function() {
		var itemDataJson = [];
		var modifyRecord = applicationTypeActionGrid.getModifyRecord();
		if (modifyRecord && modifyRecord.length > 0) {
			$.each(modifyRecord, function(i, obj) {
				var data = {};
				data.id = obj.get("id");
				data.actionName = obj.get("actionName");
				data['applicationTypeTable-id'] = obj.get("applicationTypeTable-id");
				data['applicationTypeTable-tableName'] = obj.get("applicationTypeTable-tableName");
				data['applicationTypeTable-entitypackage'] = obj.get("applicationTypeTable-entitypackage");
				data['applicationTypeTable-entityname'] = obj.get("applicationTypeTable-entityname");
				data['applicationTypeTable-type'] = obj.get("applicationTypeTable-type");
				data['applicationTypeTable-orderNumber'] = obj.get("applicationTypeTable-orderNumber");
				data.classPath = obj.get("classPath");
				data.modifyAttribute = obj.get("modifyAttribute");
				data.modifyValue = obj.get("modifyValue");
				data.exeFuncPath = obj.get("exeFuncPath");
				data.stateValue = obj.get("stateValue");
				data.actionMethod = obj.get("actionMethod");
				data.type = obj.get("type");
				data.state = obj.get("state");
				data.orderNumber=obj.get("orderNumber");
				itemDataJson.push(data);
			});
		}
		return itemDataJson.length > 0 ? JSON.stringify(itemDataJson) : "";
	};
});
