$(function() {
	$("td label").css("font-size", "16px");
	$("div[align='left']").css("font-size","16px");
	console.log($("input[type='radio']"));
	$("input[type='radio']").iCheck({
		 radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
	
});

function check(browser) {
	document.getElementById("applicationTypeActionId").value = browser;
}

function agree() {
	var params = "applicationTypeActionId:'" + document.getElementById("applicationTypeActionId").value + "',formId:'" + document.getElementById("formId").value + "'";
	params = '({' + params + '})';
	params = eval(params);
	Ext.MessageBox.confirm(biolims.common.prompt, biolims.common.approve, function(btn) {
		if(btn == "yes") {
			var myMask = new Ext.LoadMask(Ext.getBody(), {
				msg: "Please wait..."
			});
			myMask.show();
			Ext.Ajax.request({
				url: window.ctx + '/applicationTypeAction/exeFun.action',
				method: 'POST',
				params: params,
				success: function(response) {
					myMask.hide();
					var respText = Ext.util.JSON.decode(response.responseText);
					if(respText.message == '') {
						parent.location.reload();
					} else {
						message(respText.message);
					}
				},
				failure: function() {}
			});
		}
	});
}