var stepsQueryTable;
var sjbb = [];
$(function() {

	$("#startDate").datepicker({
		format : 'yyyy-mm-dd',
		autoclose : true,
		language : 'zh-CN',
	});
	$("#endDate").datepicker({
		format : 'yyyy-mm-dd',
		autoclose : true,
		language : 'zh-CN',
	});
	
	
	var colOpts = [];
	colOpts.push({
		"data" : 'warningLine',
		"title" : "警戒线",
		
	});
	colOpts.push({
		"data" : 'name',
		"title" : "步骤名称",
	});
	
	var stepsName = $.trim($("#productHideFields").val());
	$.ajax({
		type : "post",
		url : ctx + "/stepstoquery/stepsQuery/chaxun.action",
		data : {
			kssj : $("#startDate").val(),
			jssj : $("#endDate").val(),
			stepsName : $("#productHideFields").val(),
		},
		async : false,
		datatype : "json",
		success : function(data) {
			var obj = JSON.parse(data);
			if (obj["success"]) {
				if (obj["sj"] != "") {
					$.each(obj["sj"], function(j, k) {
						colOpts.push({
							"data" : obj["sj"][j]["productStartTime"],
							"title" : obj["sj"][j]["productStartTime"]
						});
						sjbb.push(obj["sj"][j]["productStartTime"]);
					});
				}
			} else {
				// top.layer.msg("验证失败！");
			}
		}
	});

	var tbarOpts = [];
	var options = table(true, "",
			"/stepstoquery/stepsQuery/showStepsNameListJson.action?sjbb="
					+ sjbb, colOpts, tbarOpts);
	stepsQueryTable = renderData($("#main"), options);

});

// 查询
function chaxun1() {
	var oldTable = $('#main').dataTable();
    oldTable.fnClearTable(); // 清空一下table
    oldTable.fnDestroy(); // 还原初始化了的dataTable
    $('#main').empty();
	var colOpts1 = [];
	colOpts1.push({
		"data" : 'warningLine',
		"title" : "警戒线",
		"width":"50px"
		
	});
	colOpts1.push({
		"data" : 'name',
		"title" : "步骤名称",
		"width":"100px"
	});

	var stepsName = $.trim($("#productHideFields").val());
	console.log(stepsName)
	$.ajax({
		type : "post",
		url : ctx + "/stepstoquery/stepsQuery/chaxun.action",
		data : {
			kssj : $("#startDate").val(),
			jssj : $("#endDate").val(),
			stepsName : stepsName,
		},
		async : false,
		datatype : "json",
		success : function(data) {
			var obj = JSON.parse(data);
			if (obj["success"]) {
				if (obj["sj"] != "") {
					
					$.each(obj["sj"], function(j, k) {
						colOpts1.push({
							"data" : obj["sj"][j]["productStartTime"],
							"title" : obj["sj"][j]["productStartTime"],
							"width" : "100px"
						});
						sjbb.push(obj["sj"][j]["productStartTime"]);
					});
				}
			} else {
				// top.layer.msg("验证失败！");
			}
		}
	});
	var tbarOpts1 = [];
	var options1 = table(true, "",
			"/stepstoquery/stepsQuery/showStepsNameListJson.action?sjbb="
					+ sjbb+ "&stepsName=" + $("#productHideFieldIds").val(), colOpts1, tbarOpts1);
	stepsQueryTable = renderData($("#main"), options1);
}
// 步骤名称查询
function showSteps() {
	top.layer.open({
		title : biolims.common.pleaseChoose,
		type : 2,
		anim : 1,
		area : [ "550px", "300px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/dic/type/dicTypeSelectTable.action?flag=bzcx",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#sysCode").val();
			var stepIds=[];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .selected").each(function(i, v) {
				stepIds.push($(v).children("td").find("input[type='checkbox']").val());
				});
			
			top.layer.close(index)
			$("#productHideFields").val(name);
			$("#productHideFieldIds").val(stepIds);
			
		},
	})

}

// 日期格式化
function dateFtt(fmt,date) { // author: meizz
 var o = { 
 "M+" : date.getMonth()+1,     // 月份
 "d+" : date.getDate(),     // 日
 "h+" : date.getHours(),     // 小时
 "m+" : date.getMinutes(),     // 分
 "s+" : date.getSeconds(),     // 秒
 "q+" : Math.floor((date.getMonth()+3)/3), // 季度
 "S" : date.getMilliseconds()    // 毫秒
 }; 
 if(/(y+)/.test(fmt)) 
 fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length)); 
 for(var k in o) 
 if(new RegExp("("+ k +")").test(fmt)) 
 fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length))); 
 return fmt; 
}

