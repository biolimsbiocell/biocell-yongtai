$(function() {
	var cols = {};
	cols.sm = false;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});

	fields.push({
		name : 'state',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : '编号',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		header : '应用类型名称',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex : 'state',
		header : '状态',
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/applicationType/showApplicationTypeListJson.action";
	var opts = {};
	opts.height = 430;
	applicationTypeGrid = gridTable("show_dialog_applicationtype_grid_div", cols, loadParam, opts);
	$("#show_dialog_applicationtype_grid_div").data("waitApplicationtypeApplyGrid", applicationTypeGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
});
