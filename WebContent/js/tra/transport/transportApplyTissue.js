﻿
var transportApplyTissueGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'sampleNum',
		type:"string"
	});
	   fields.push({
		   name:'animalType',
		   type:"string"
	   });
	   fields.push({
		   name:'name',
		   type:"string"
	   });
	    fields.push({
		name:'animalMain-id',
		type:"string"
	});
	    fields.push({
		name:'animalMain-name',
		type:"string"
	});
    fields.push({
		name:'animalItem-id',
		type:"string"
	});
	   fields.push({
		name:'gender',
		type:"string"
	});
   fields.push({
		name:'animalItem-inNum',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'genetype',
		type:"string"
	});
	   fields.push({
		name:'animalItem-birthDate',
		type:"string"
//		type:"date",
//		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'way',
		type:"string"
	});
	    fields.push({
		name:'transportApply-id',
		type:"string"
	});
	    fields.push({
		name:'transportApply-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'sampleNum',
		hidden : false,
		header:'样品编号',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'组织名称',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'animalMain-name',
		hidden : true,
		header:'动物名称',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'animalMain-id',
		hidden : false,
		header:'动物编号',
		width:15*10
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'animalType',
		hidden : true,
		header:'品系',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'animalItem-id',
		hidden : true,
		header:'明细编号',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'animalName',
//		hidden : false,
//		header:'动物名称',
//		width:20*6,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'gender',
		hidden : false,
		header:'性别',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'animalItem-inNum',
		hidden : true,
		header:'在库数量',
		width:20*6
	});
	cm.push({
		dataIndex:'num',
		hidden : true,
		header:'数量 ',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals: false, // 不允许小数点 
			allowNegative: false, // 不允许负数 
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'genetype',
		hidden : false,
		header:'基因型',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'animalItem-birthDate',
		hidden : false,
		header:'出生日期',
		width:20*6
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'way',
		hidden : true,
		header:'运输方式',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportApply-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportApply-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/transport/transportApply/showTransportApplyTissueListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="产品运输(组织)信息";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/tra/transport/transportApply/delTransportApplyTissue.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	
	if($("#transportApply_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
		 } else{
					opts.tbar.push({
							text : '选择动物',
							handler : selectAnimalMainFun
						});
					opts.tbar.push({
						text : '显示可编辑列',
						handler : null
					});
					opts.tbar.push({
						text : '取消选中',
						handler : null
					});
					opts.tbar.push({
						text : '填加明细',
						handler : null
					});
					
					
					opts.tbar.push({
						text : "批量上传（CSV文件）",
						handler : function() {
							var options = {};
							options.width = 350;
							options.height = 200;
							loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
								"确定":function(){
									goInExcelcsv();
									$(this).dialog("close");
								}
							},true,options);
						}
					});
		 }

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = transportApplyTissueGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							transportApplyTissueGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	transportApplyTissueGrid=gridEditTable("transportApplyTissuediv",cols,loadParam,opts);
	$("#transportApplyTissuediv").data("transportApplyTissueGrid", transportApplyTissueGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
//获取选中的数据ID
//选择动物主数据
function selectAnimalMainFun(){
			var options = {};
			options.width = 800;
			options.height = 660;
			var url="/ani/animal/animalMain/animalMainSelect.action";
			loadDialogPage(null, "选择明细", url, {
				 "确定": function() {	
					 addAnimal();
					 options.close();
				}
			}, true, options);
		}
var d="";
var name=""; 
function setAnimalMainFun(rec){	
	d = rec.get('id');
	name= rec.get('name');
	var selectRecord = animalMainDialogGrid.getSelectionModel();
	var selRecord = animalItemDialogGrid.store;
	if (selectRecord.getSelections().length > 0) {
		if (selRecord && selRecord.getCount() > 0) {
			for(var i=0;i<selectRecord.getSelections().length;i++){
				var newv = selectRecord.getSelections()[i].get("id");
				for(var j=0;j<selRecord.getCount();j++){
					var oldv = selRecord.getAt(j).get("main-id");
					if( oldv == newv ){
						message("数据重复，请重新选择！");
						return ;
					}
				}
			}
		}
	ajax("post", "/tra/transport/transportApply/showAnimalItemList.action", {
		animalId : d
		}, function(data) {
			if (data.success) {			
				var ob = animalItemDialogGrid.getStore().recordType;
				animalItemDialogGrid.stopEditing();
				$.each(data.data, function(i, obj) {
					var p = new ob({});
					p.isNew = true;
					
					p.set("main-id", d);				
					p.set("main-name", name);
					p.set("item-id", obj.id);
					p.set("batch", obj.bctch);
					p.set("buyWeekAge", obj.buyWeekAge);
					//p.set("type-id", obj.typeId);
					//p.set("type-name", obj.typeName);
					p.set("birthDate", obj.birthDate);
					p.set("realWeekAge", obj.realWeekAge);
					p.set("inNum", obj.inNum);
					//p.set("strain", obj.strain);
					//p.set("cageCode", obj.cageCode);
					//p.set("note", obj.note);
					
					animalItemDialogGrid.getStore().add(p);
					
				});
				animalItemDialogGrid.startEditing(0, 0);
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
	}
}
//添加到明细表

function addAnimal(){
	//var selectRecord = animalItemDialogGrid.getSelectionModel();
	//var selRecord = transportApplyTissueGrid.store;
	//if (selectRecord.length > 0) {
	//	$.each(selectRecord, function(i, obj) {
	var selectRecord = animalItemDialogGrid.getSelectionModel();
	var selRecord = transportApplyTissueGrid.store;
	if (selectRecord.getSelections().length > 0) {
//		if (selRecord && selRecord.getCount() > 0) {
//			for(var i=0;i<selectRecord.getSelections().length;i++){
//				var newv = selectRecord.getSelections()[i].get("item-id");
//				for(var j=0;j<selRecord.getCount();j++){
//					var oldv = selRecord.getAt(j).get("animalItem-id");
//					if( oldv == newv ){
//						message("有重复的数据，请重新选择！");
//						return ;
//					}
//				}
//			}
//		}	
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRepeat = false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("animalItem-id");
				if(oldv == obj.get("item-id")){
					isRepeat = true;
					break;
				}
			}
			if(!isRepeat){
			var ob = transportApplyTissueGrid.getStore().recordType;
			transportApplyTissueGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;	
			
			p.set("animalMain-id", obj.get("main-id"));
			p.set("animalMain-name", obj.get("main-name"));
			p.set("animalItem-id", obj.get("item-id"));
			p.set("animalItem-birthDate",obj.get("birthDate"));
			p.set("animalItem-inNum", obj.get("inNum"));
			transportApplyTissueGrid.getStore().add(p);
			}
		});
		
		transportApplyTissueGrid.startEditing(0, 0);
	}
}
//function selectanimalMainFun(){
//	var win = Ext.getCmp('selectanimalMain');
//	if (win) {win.close();}
//	var selectanimalMain= new Ext.Window({
//	id:'selectanimalMain',modal:true,title:'选择编号',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/AnimalMainSelect.action?flag=animalMain' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectanimalMain.close(); }  }]  });     selectanimalMain.show(); }
//	function setanimalMain(id,name){
//		var gridGrid = $("#transportApplyTissuediv").data("transportApplyTissueGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('animalMain-id',id);
//			obj.set('animalMain-name',name);
//		});
//		var win = Ext.getCmp('selectanimalMain')
//		if(win){
//			win.close();
//		}
//	}
//	
//function selecttransportApplyFun(){
//	var win = Ext.getCmp('selecttransportApply');
//	if (win) {win.close();}
//	var selecttransportApply= new Ext.Window({
//	id:'selecttransportApply',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TransportApplySelect.action?flag=transportApply' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selecttransportApply.close(); }  }]  });     selecttransportApply.show(); }
//	function settransportApply(id,name){
//		var gridGrid = $("#transportApplyTissuediv").data("transportApplyTissueGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('transportApply-id',id);
//			obj.set('transportApply-name',name);
//		});
//		var win = Ext.getCmp('selecttransportApply')
//		if(win){
//			win.close();
//		}
//	}
	
