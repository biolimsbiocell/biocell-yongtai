function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/tra/transport/transportOrder/editTransportOrder.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/tra/transport/transportOrder/viewTransportOrder.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : '编码',
		dataIndex : 'id',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '描述',
		dataIndex : 'name',
		width:50*6,
		hidden:false
	}, 
	
	    {
		header : '运输申请',
		dataIndex : 'transportApply-name',
		width:15*6,
		hidden:false
	}, 
	    {
		header : '分类',
		dataIndex : 'type-name',
		width:15*6,
		hidden:false
	}, 
	    {
		header : '创建人',
		dataIndex : 'createUser-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : '创建日期',
		dataIndex : 'createDate',
		width:15*6,
		hidden:false
	}, 
	
	    {
		header : '批准人',
		dataIndex : 'confirmUser-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : '批准日期',
		dataIndex : 'confirmDate',
		width:15*6,
		hidden:false
	}, 
	
	    {
		header : '运送人',
		dataIndex : 'transUser-name',
		width:15*6,
		hidden:false
	}, 
	   
	{
		header : '承运公司',
		dataIndex : 'transCompany',
		width:40*6,
		hidden:false
	}, 
	
	   
	{
		header : '运送单号',
		dataIndex : 'transCode',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '联系人',
		dataIndex : 'linkman',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '联系电话',
		dataIndex : 'phone',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '接收单位',
		dataIndex : 'receiveCompany',
		width:40*6,
		hidden:false
	}, 
	
	   
	{
		header : '接收地址',
		dataIndex : 'address',
		width:40*6,
		hidden:false
	}, 
	
	   
	{
		header : '邮箱',
		dataIndex : 'emall',
		width:40*6,
		hidden:false
	}, 
	
	   
	{
		header : '航班号',
		dataIndex : 'flightCode',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : '预计送达日期',
		dataIndex : 'willDate',
		width:15*6,
		hidden:false
	}, 
	
	   
	{
		header : '目的地',
		dataIndex : 'arrived',
		width:40*6,
		hidden:false
	}, 
	
	   
	{
		header : '出发日期',
		dataIndex : 'fromDate',
		width:15*6,
		hidden:false
	}, 
	
	   
	{
		header : '签收日期',
		dataIndex : 'receiveDate',
		width:15*6,
		hidden:false
	}, 
	
	   
	{
		header : '运费',
		dataIndex : 'transFee',
		width:40*6,
		hidden:false
	}, 
	
	   
	{
		header : '工作流状态',
		dataIndex : 'state',
		width:40*6,
		hidden:true
	}, 
	
	   
	{
		header : '工作流状态',
		dataIndex : 'stateName',
		width:40*6,
		hidden:false
	}, 
	
	   
	{
		header : 'content1',
		dataIndex : 'content1',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content2',
		dataIndex : 'content2',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content3',
		dataIndex : 'content3',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content4',
		dataIndex : 'content4',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content5',
		dataIndex : 'content5',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content6',
		dataIndex : 'content6',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content7',
		dataIndex : 'content7',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content8',
		dataIndex : 'content8',
		width:20*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content9',
		dataIndex : 'content9',
		width:15*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content10',
		dataIndex : 'content10',
		width:15*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content11',
		dataIndex : 'content11',
		width:15*6,
		hidden:true
	}, 
	
	   
	{
		header : 'content12',
		dataIndex : 'content12',
		width:15*6,
		hidden:true
	}, 
	
			
			
			
			{
				header : '上级编码',
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#transportOrderTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
