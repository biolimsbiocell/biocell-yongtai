var transportApplyGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'project-id',
		type:"string"
	});
	    fields.push({
		name:'project-name',
		type:"string"
	});
	    fields.push({
			name:'project-projectId',
			type:"string"
	});
	    fields.push({
		name:'type-id',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
//	    fields.push({
//		name:'objType-id',
//		type:"string"
//	});
	    fields.push({
		name:'objType',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'applyDepartment',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-id',
		type:"string"
	});
	    fields.push({
		name:'confirmUser-name',
		type:"string"
	});
	    fields.push({
		name:'confirmDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'content1',
		type:"string"
	});
	    fields.push({
		name:'content2',
		type:"string"
	});
	    fields.push({
		name:'content3',
		type:"string"
	});
	    fields.push({
		name:'content4',
		type:"string"
	});
	    fields.push({
		name:'content5',
		type:"string"
	});
	    fields.push({
		name:'content6',
		type:"string"
	});
	    fields.push({
		name:'content7',
		type:"string"
	});
	    fields.push({
		name:'content8',
		type:"string"
	});
	    fields.push({
		name:'content9',
		type:"string"
	});
	    fields.push({
		name:'content10',
		type:"string"
	});
	    fields.push({
		name:'content11',
		type:"string"
	});
	    fields.push({
		name:'content12',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:30*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'project-id',
		hidden:true,
		header:'项目ID',
		width:15*10,
		sortable:true
	});
		cm.push({
		dataIndex:'project-name',
		header:'项目名称',
		hidden:true,
		width:15*10,
		sortable:true
	});
	cm.push({
			dataIndex:'project-projectId',
			header:'项目编号',
			
			width:15*10,
			sortable:true
	});
	cm.push({
		dataIndex:'type-id',
		hidden:true,
		header:'分类ID',
		width:10*10,
		sortable:true
	});
		cm.push({
		dataIndex:'type-name',
		header:'分类',
		
		width:10*10,
		sortable:true
	});
//		cm.push({
//		dataIndex:'objType-id',
//		hidden:true,
//		header:'物品类型ID',
//		width:15*10,
//		sortable:true
//		});
		var typestore = new Ext.data.ArrayStore({
			fields : [ 'id', 'name' ],
			data : [ [ '0', '活体' ], [ '1', '质粒/菌液' ], [ '2', '细胞' ],[ '3', '组织' ]]
		});
		
		var typeComboxFun = new Ext.form.ComboBox({
			store : typestore,
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			emptyText : '',
			selectOnFocus : true
		});
		cm.push({
		dataIndex:'objType',
		header:'物品类型',
		
		width:10*10,
		renderer: Ext.util.Format.comboRenderer(typeComboxFun),	
		sortable:true
		});
//		cm.push({
//			dataIndex:'type',
//			header:'类型',
//			width:20*6,
//			
//			renderer: Ext.util.Format.comboRenderer(typeComboxFun),				
//			sortable:true
//		});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'申请人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'申请人',
		
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'申请日期',
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'applyDepartment',
		header:'申请部门',
		width:30*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'confirmUser-id',
		hidden:true,
		header:'批准人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'confirmUser-name',
		header:'批准人',
		
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'confirmDate',
		header:'批准日期',
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'工作流状态ID',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:15*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'content1',
		header:'content1',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content2',
		header:'content2',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content3',
		header:'content3',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content4',
		header:'content4',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content5',
		header:'content5',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content6',
		header:'content6',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content7',
		header:'content7',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content8',
		header:'content8',
		width:20*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content9',
		header:'content9',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content10',
		header:'content10',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content11',
		header:'content11',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'content12',
		header:'content12',
		width:15*6,
		hidden:true,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/transport/transportApply/showTransportApplyListJson.action";
	var opts={};
	opts.title="运输申请";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	transportApplyGrid=gridTable("show_transportApply_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/tra/transport/transportApply/editTransportApply.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/tra/transport/transportApply/editTransportApply.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/tra/transport/transportApply/viewTransportApply.action?id=' + id;
}
function exportexcel() {
	transportApplyGrid.title = '导出列表';
	var vExportContent = transportApplyGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 842;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				if (($("#startconfirmDate").val() != undefined) && ($("#startconfirmDate").val() != '')) {
					var startconfirmDatestr = ">=##@@##" + $("#startconfirmDate").val();
					$("#confirmDate1").val(startconfirmDatestr);
				}
				if (($("#endconfirmDate").val() != undefined) && ($("#endconfirmDate").val() != '')) {
					var endconfirmDatestr = "<=##@@##" + $("#endconfirmDate").val();

					$("#confirmDate2").val(endconfirmDatestr);

				}
				
				if (($("#startcontent9").val() != undefined) && ($("#startcontent9").val() != '')) {
					var startcontent9str = ">=##@@##" + $("#startcontent9").val();
					$("#content91").val(startcontent9str);
				}
				if (($("#endcontent9").val() != undefined) && ($("#endcontent9").val() != '')) {
					var endcontent9str = "<=##@@##" + $("#endcontent9").val();

					$("#content92").val(endcontent9str);

				}
				
				if (($("#startcontent10").val() != undefined) && ($("#startcontent10").val() != '')) {
					var startcontent10str = ">=##@@##" + $("#startcontent10").val();
					$("#content101").val(startcontent10str);
				}
				if (($("#endcontent10").val() != undefined) && ($("#endcontent10").val() != '')) {
					var endcontent10str = "<=##@@##" + $("#endcontent10").val();

					$("#content102").val(endcontent10str);

				}
				
				if (($("#startcontent11").val() != undefined) && ($("#startcontent11").val() != '')) {
					var startcontent11str = ">=##@@##" + $("#startcontent11").val();
					$("#content111").val(startcontent11str);
				}
				if (($("#endcontent11").val() != undefined) && ($("#endcontent11").val() != '')) {
					var endcontent11str = "<=##@@##" + $("#endcontent11").val();

					$("#content112").val(endcontent11str);

				}
				
				if (($("#startcontent12").val() != undefined) && ($("#startcontent12").val() != '')) {
					var startcontent12str = ">=##@@##" + $("#startcontent12").val();
					$("#content121").val(startcontent12str);
				}
				if (($("#endcontent12").val() != undefined) && ($("#endcontent12").val() != '')) {
					var endcontent12str = "<=##@@##" + $("#endcontent12").val();

					$("#content122").val(endcontent12str);

				}
				
				
				commonSearchAction(transportApplyGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
