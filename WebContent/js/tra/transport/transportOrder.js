	var transportOrderListTable;
$(function(){
	
	var colData = [
		{
			"data": "id",
			"title": biolims.common.id,
		}, {
			"data": "name",
			"title": biolims.common.name
		}, {
			"data": "transportApply-id",
			"title": biolims.common.transportApplyID,
			"visible":false
		}, 
		{
			"data": "transportDate",
			"title": "回输日期"
		},
		
//		{
//			"data": "transportApply-name",
//			"title": biolims.common.transportApply
//		}, 
//		{
//			"data": "objType",
//			"title": biolims.master.productType
//		}, 
		{
			"data": "ysfs-name",
			"title": biolims.common.transportType,
		}, {
			"data": "type-id",
			"title": biolims.storage.studyType,
			"visible":false
		}, 
//		{
//			"data": "type-name",
//			"title": biolims.storage.studyType
//		}, 
		{
			"data": "createUser-id",
			"title": biolims.tStorage.userCreateUserId,
			"visible":false
		}, {
			"data": "createUser-name",
			"title": biolims.tStorage.userCreateUserId
		}, {
			"data": "createDate",
			"title": biolims.tStorage.createDate
		}, {
			"data": "confirmUser-id",
			"title": biolims.common.confirmUser,
			"visible":false
		}, 
//		{
//			"data": "confirmUser-name",
//			"title": biolims.common.confirmUser
//		}, 
//		{
//			"data": "confirmDate",
//			"title": biolims.common.confirmDate
//		}, 
//		{
//			"data": "boxes",
//			"title": biolims.common.PCS
//		}, 
//		{
//			"data": "transUser",
//			"title": biolims.common.transportUser
//		}, 
		{
			"data": "transCompany-name",
			"title": biolims.common.transportCompany
		}, 
//		{
//			"data": "transCode",
//			"title": biolims.common.trackingNumber
//		}, 
//		{
//			"data": "linkman",
//			"title": biolims.common.linkmanName
//		}, 
//		{
//			"data": "phone",
//			"title": biolims.common.linkMan_telNumber
//		}, 
//		{
//			"data": "receiveCompany",
//			"title": biolims.common.receiveCompany
//		}, {
//			"data": "address",
//			"title": biolims.crmCustomer.street
//		}, {
//			"data": "emall",
//			"title": biolims.crmCustomer.email
//		}, {
//			"data": "flightCode",
//			"title": biolims.common.flightCode
//		}, 
//		{
//			"data": "expectedDeliveryDate",
//			"title": biolims.goods.predictTransportDate
//		}, 
//		{
//			"data": "willDate",
//			"title": biolims.goods.predictDeliveryDate
//		}, 
//		{
//			"data": "fromDate",
//			"title": biolims.goods.departDate
//		}, 
//		{
//			"data": "receiveDate",
//			"title": biolims.goods.receiveDate
//		}, 
//		{
//			"data": "transFee",
//			"title": biolims.common.transportCosts
//		}, 
		{
			"data": "state",
			"title": biolims.common.state,
			"visible":false
		}, {
			"data": "stateName",
			"title": biolims.common.state
		}, {
			"data": "zuoState",
			"title": "是否作废",
			"visible":false
		}];
	
//	$.ajax({
//		type:"post",
//		url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
//		async:false,
//		data:{
//			moduleValue : "TransportOrder"
//		},
//		success:function(data){
//			var objData = JSON.parse(data);
//			if(objData.success){
//				$.each(objData.data, function(i,n) {
//					var str = {
//						"data" : n.fieldName,
//						"title" : n.label
//					}
//					colData.push(str);
//				});
//				
//			}else{
//				top.layer.msg(biolims.tInstrumentBorrowDetail.tableError);
//			}
//		}
//	});
	
	var options = table(true, "","/tra/transport/transportOrder/showTransportOrderListJson.action",colData , null)
		transportOrderListTable = renderRememberData($("#main"), options);
		//恢复之前查询的状态
//		$('#main').on('init.dt', function() {
//			recoverSearchContent(transportOrderListTable);
//		})
})

// 新建
function add() {
	window.location = window.ctx + "/tra/transport/transportOrder/editTransportOrder.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/tra/transport/transportOrder/editTransportOrder.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/tra/transport/transportOrder/toViewTransportOrder.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	
	return [
//		{
//			"txt": biolims.common.id,
//			"type": "input",
//			"searchName": "id"
//		},
		{
			"txt": biolims.common.name,
			"type": "input",
			"searchName": "name"
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},{
			"txt" : biolims.sample.createDateEnd,
			  "type" : "dataTime",
			  "searchName" : "createDate##@@##2",
			  "mark" : "e##@@##",
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser-name"
		},
		{
			"type": "table",
			"table": transportOrderListTable
		}
	];
	
//	var fields=[];
//	   fields.push({
//		    "searchName":"id",
//			"type":"input",
//			"txt": biolims.common.id
//		});
//	   fields.push({
//		    "searchName":"name",
//			"type":"input",
//			"txt": biolims.common.name
//		});
//	   fields.push({
//		    "searchName":"createUser-name",
//			"type":"input",
//			"txt": biolims.sample.createUserName
//		});
//	   fields.push({
//		   "searchName": "createDate##@@##",
//			"mark": "##@@##",
//			"type":"dataTime",
//			"txt": biolims.sample.createDateStart
//		});
//	   fields.push({
//		    "searchName":"stateName",
//			"type":"input",
//			"txt": biolims.common.state
//		});
//	fields.push({
//		"type":"table",
//		"table":transportOrderListTable
//	});
//	return fields;
}