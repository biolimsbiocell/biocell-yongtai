var TransportSampleStateTab;
var TransportSampleStateChangeLog;
$(function() {
	// 加载子表
	var id = $("#transportOrder_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": "ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false
	});
	colOpts.push({
		"data":"shippingNo",
		"title": "运输单号",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "shippingNo");
			$(td).attr("transportOrder-id", rowData['transportOrder-id']);
	    },
	    "className":"edit"
	});
	colOpts.push({
		"data":"signatory",
		"title": "签收人",
		"createdCell": function(td) {
			$(td).attr("saveName", "signatory");
	    },
	    "className":"edit"
	});
	colOpts.push({
		"data":"signatureDate",
		"title": "签收日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "signatureDate");
	    },
		"className": "date"
	});
	colOpts.push({
		"data":"confirmationUser",
		"title": "签收确认人",
		"createdCell": function(td) {
			$(td).attr("saveName", "confirmationUser");
	    },
	    "className":"edit"
	});
	colOpts.push({
		"data":"confirmationDate",
		"title": "确认日期",
		"createdCell": function(td) {
			$(td).attr("saveName", "confirmationDate");
	    },
		"className": "date"
	});
	colOpts.push({
		"data":"freight",
		"title": "运输费",
		"createdCell": function(td) {
			$(td).attr("saveName", "freight");
	    },
	    "className":"edit"
	});
	colOpts.push({
		"data":"note",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
	    "className":"edit"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view" && $("#transportOrder_stateName").val()!="完成") {
//	tbarOpts.push({
//		text: biolims.master.selectOrder,
//		action: function() {
//			selectOrder($("#transportSampleState"))
//	}
//	});
	tbarOpts.push({
		text: "添加明细",
		action: function() {
			addItem($("#transportSampleState"));
		}
	});
	tbarOpts.push({
		text: "保 存",
		action: function() {
			saveState($("#transportSampleState"));
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#transportSampleState"),
				"/tra/transport/transportOrder/delTransportSampleState.action");
		}
	});
	}
	
	var TransportSampleStateTabOptions = table(true, id,
		'/tra/transport/transportOrder/showTransportStateSampleTableJson.action', colOpts, tbarOpts)
	TransportSampleStateTab = renderData($("#transportSampleState"), TransportSampleStateTabOptions);
	TransportSampleStateTab.on('draw', function() {
		TransportSampleStateChangeLog = TransportSampleStateTab.ajax.json();
	});
});

//选择订单
//function selectOrder(ele){
//		top.layer.open({
//			title: biolims.common.pleaseChoose,
//			type: 2,
//			area: ["650px", "400px"],
//			btn: biolims.common.confirmSelected,
//			content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
//			yes: function(index, layer) {
//				var id=[];
//				$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .selected").each(function(i, v) {
//					id.push($(v).children("td").eq(1).text());
//				});
//				var flg=checkSelected(id);
//				if(flg==true){
//					top.layer.msg("与表格中数据重复，请仔细选择");
//					return false;
//				}
//				var len=id.length;
//				top.layer.close(index);
//				addl($("#transportSampleState"),len);
//				addInfo(id);
//			},
//		})
//}
function addInfo(id){
	var flag=false;
	var blindex=0;
	$("#transportSampleState tbody tr").each(function(i,v){
		if(blindex<=id.length){
			$(this).addClass("editagain");
			$(this).find("td[savename='orderNo']").attr("sampleOrder-id",id[blindex]).text(id[blindex]);
		}
		blindex++;
		flag=true;
	})
}
function addl(ele,len){
	$(".dataTables_scrollHead th").off();
	//禁用固定列
	$("#fixdeLeft2").hide();
	//禁用列显示隐藏
	$(".colvis").hide();
	//清除没有内容选项
	ele.find(".dataTables_empty").parent("tr").remove();
	//添加明细
	for(var inin=0;inin<len;inin++){
	var ths = ele.find("th");
	var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
	tr.height(32);
	for(var i = 1; i < ths.length; i++) {
		var edit = $(ths[i]).attr("key");
		var saveName = $(ths[i]).attr("saveName");
		if(edit == "select") {
			var selectOpt = $(ths[i]).attr("selectopt");
			tr.addClass("editagain");
			tr.append("<td class=" + edit + " saveName=" + saveName + " selectopt=" + selectOpt + "></td>");
		} else {
			tr.addClass("editagain");
			tr.append("<td class=" + edit + " saveName=" + saveName + "></td>");
		}
	}
	ele.find("tbody").prepend(tr);
	checkall(ele);
	}
}
function checkSelected(id){
	var flag=false;
	$("#transportSampleState tr").each(function(i,v){
		var thisId=$(this).find("td[savename='orderNo']").text();
		if(thisId!=""&&thisId!=null){
			for(var ia=0;ia<id.length;ia++){
				if(thisId==id[ia]){
					flag=true;
				}
			}
		}
	});
	return flag;
}
// 保存
function saveState(ele) {
	if($("#transportOrder_id").val()=="NEW"){
		top.layer.msg("请先保存主表");
		return false;
	}
	var data = saveSampleTransportStatejson(ele);
	var ele=$("#transportSampleState");
	var changeLog = "样本运输：";
	changeLog = getSampleTransportStateChangeLog(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/tra/transport/transportOrder/saveTransportSampleStateTable.action',
		data: {
			id: $("#transportOrder_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				TransportSampleStateTab.ajax.reload();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveSampleTransportStatejson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "shippingNo") {
				json["transportOrder-id"] = $(tds[j]).attr("transportOrder-id");
			}
			
//			if(k == "createUser-name") {
//				json["createUser-id"] = $(tds[j]).attr("createUser-id");
//				continue;
//			}
//			
//			if(k == "orderNo") {
//				json["sampleOrder-id"] = $(tds[j]).attr("sampleOrder-id");
//			}
//			
//			if(k == "invalidState") {
//				var result = $(tds[j]).text();
//				if(result == "是") {
//					json[k] = "1";
//				} else {
//					json[k] = "0";
//				}
//				continue;
//			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getSampleTransportStateChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		TransportSampleStateChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function invalidSampleOrder(){
	var rows=$("#transportSampleState .selected");
	if(!rows.length){
		top.layer.msg("请选择数据！");
		return false;
	}
	var ids=[];
	$("#transportSampleState .selected").each(function(i,v){
		ids.push($(this).find("input[type='checkbox']").val());
	})
	$.ajax({
		type:"post",
		url:ctx+"/tra/sampletransport/sampleTransport/invalidSampleOrder.action",
		data:{
			id:ids,
		},
		success:function(dataBack){
			var data=JSON.parse(dataBack);
			if(data.success){
				top.layer.msg("订单作废成功！");
				TransportSampleStateTab.ajax.reload();
			}
		}
	});
}
function transportStateRefush(){
	TransportSampleStateTab.ajax.reload();
}