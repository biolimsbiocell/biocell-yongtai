﻿var transportOrderBacteriaAllGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'objName',
		type:"string"
	});
	   fields.push({
		name:'bacteriaName',
		type:"string"
	});
	   fields.push({
		name:'projectName',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'antibody',
		type:"string"
	});
	   fields.push({
		name:'od260',
		type:"string"
	});
	   fields.push({
		name:'od230',
		type:"string"
	});
	   fields.push({
		name:'od600',
		type:"string"
	});   
	   fields.push({
		name:'resistance',
		type:"string"
	});
	   fields.push({
		name:'transportTemperature',
		type:"string"
	});
	   fields.push({
		name:'note',
		type:"string"
	});
	    fields.push({
		name:'transportOrder-id',
		type:"string"
	});
	    fields.push({
		name:'transportOrder-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6
	});
//	var typestore = new Ext.data.ArrayStore({
//		fields : [ 'id', 'name' ],
//		data : [ [ '0', '活体' ], [ '1', '质粒/菌液' ], [ '2', '细胞' ],[ '3', '组织' ]]
//	});
//	var typeComboxFun = new Ext.form.ComboBox({
//		store : typestore,
//		displayField : 'name',
//		valueField : 'id',
//		typeAhead : true,
//		mode : 'local',
//		forceSelection : true,
//		triggerAction : 'all',
//		emptyText : '',
//		selectOnFocus : true
//	});
//	cm.push({
//		dataIndex:'objName',
//		hidden : false,
//		header:'物品类型',
//		width:20*6,
//		
//		renderer: Ext.util.Format.comboRenderer(typeComboxFun),	
//		sortable:true
//	});
	cm.push({
		dataIndex:'projectName',
		hidden : false,
		header:'项目名称',
		width:20*6
	});
//	cm.push({
//		dataIndex:'concentration',
//		hidden : false,
//		header:'浓度',
//		width:20*6
////		
////		editor : new Ext.form.TextField({
////			allowBlank : true
////		})
//	});
//	cm.push({
//		dataIndex:'volume',
//		hidden : false,
//		header:'体积',
//		width:20*6
////		
////		editor : new Ext.form.TextField({
////			allowBlank : true
////		})
//	});
//	cm.push({
//		dataIndex:'antibody',
//		hidden : false,
//		header:'抗体',
//		width:40*6
////		
////		editor : new Ext.form.TextField({
////			allowBlank : true
////		})
//	});
//	cm.push({
//		dataIndex:'od260',
//		hidden : false,
//		header:'260/280',
//		width:20*6
////		
////		editor : new Ext.form.TextField({
////			allowBlank : true
////		})
//	});
//	cm.push({
//		dataIndex:'od230',
//		hidden : false,
//		header:'260/230',
//		width:20*6
////		
////		editor : new Ext.form.TextField({
////			allowBlank : true
////		})
//	});
	cm.push({
		dataIndex:'bacteriaName',
		hidden : false,
		header:'菌液名称',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'od600',
		hidden : false,
		header:'OD600',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积(μl)',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'resistance',
		hidden : false,
		header:'抗性',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportTemperature',
		hidden : false,
		header:'运输温度',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportOrder-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportOrder-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/transport/transportOrder/showTransportOrderBacteriaListJson.action";
	var opts={};
	opts.title="产品运输（菌液）信息";
	opts.height =  document.body.clientHeight;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/tra/transport/transportOrder/delTransportOrderBacteria.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selecttransportOrderFun
//		});
//	
	
//	opts.tbar.push({
//		text : "批量上传（CSV文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});
	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = transportOrderBacteriaGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							transportOrderBacteriaGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	opts.tbar.push({
		text : '列表',
		handler : list
	});
	opts.tbar.push({
		text : '删除选中',
		handler : null
	});
//	opts.tbar.push({
//		text : '填加明细',
//		handler : null
//	});
	
	transportOrderBacteriaAllGrid=gridEditTable("transportOrderBacteriaAlldiv",cols,loadParam,opts);
	$("#transportOrderBacteriaAlldiv").data("transportOrderBacteriaAllGrid", transportOrderBacteriaAllGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//function selecttransportOrderFun(){
//	var win = Ext.getCmp('selecttransportOrder');
//	if (win) {win.close();}
//	var selecttransportOrder= new Ext.Window({
//	id:'selecttransportOrder',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TransportOrderSelect.action?flag=transportOrder' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selecttransportOrder.close(); }  }]  });     selecttransportOrder.show(); }
//	function settransportOrder(id,name){
//		var gridGrid = $("#transportOrderBacteriadiv").data("transportOrderBacteriaGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('transportOrder-id',id);
//			obj.set('transportOrder-name',name);
//		});
//		var win = Ext.getCmp('selecttransportOrder')
//		if(win){
//			win.close();
//		}
//	}
	function list() {
		window.location = window.ctx + '/tra/transport/transportOrder/showTransportOrderList.action';
	}
