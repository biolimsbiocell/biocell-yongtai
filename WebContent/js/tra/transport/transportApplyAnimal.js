﻿var transportApplyAnimalGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
    fields.push({
		name:'animalId',
		type:"string"
	});
	
	   fields.push({
		name:'mark',
		type:"string"
	});
	   fields.push({
		name:'genetype',
		type:"string"
	});
	   fields.push({
		name:'birthDate',
//		type:"string"
		type:"date",
		dateFormat:"Y-m-d"
	});
	   fields.push({
		name:'gender',
		type:"string"
	});
	   fields.push({
		name:'clean',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'transportApply-id',
		type:"string"
	});
	    fields.push({
		name:'transportApply-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'animalId',
		hidden : false,
		header:'鼠ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex:'mark',
		hidden : false,
		header:'标记',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'genetype',
		hidden : false,
		header:'基因型',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'birthDate',
		hidden : false,
		header:'出生日期',
		width:15*6,	
		renderer: formatDate,
		editor: new Ext.form.DateField({format: 'Y-m-d'})
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	var gendercob = new Ext.form.ComboBox({
		store : new Ext.data.JsonStore({
			fields : [ 'id', 'name' ],
			data : [ {
				id : '1',
				name : '雄'
			}, {
				id : '0',
				name : '雌'
			} ]
		}),
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'gender',
		hidden : false,
		header:'性别',
		width:20*6,
		
//		renderer: Ext.util.Format.comboRenderer(gendercob),editor: gendercob
	});
	cm.push({
		dataIndex:'clean',
		hidden : false,
		header:'清洁度',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex:'num',
		hidden : true,
		header:'数量 *',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals: false, // 不允许小数点 
			allowNegative: false, // 不允许负数 
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportApply-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportApply-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/transport/transportApply/showTransportApplyAnimalListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="产品运输(动物)信息";
	opts.height =  document.body.clientHeight-100;
	
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/tra/transport/transportApply/delTransportApplyAnimal.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	if($("#transportApply_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
		 } else{
				opts.tbar.push({
						text : '选择基因动物',
						handler : selectcodeFun
				});
				opts.tbar.push({
					text : '选择野生动物',
					handler : selectAnimalwildFun
				});
				opts.tbar.push({
					text : "批量录入微生物学等级",
					handler : function() {
						var records = transportApplyAnimalGrid.getSelectionModel().getSelections();
						if (records && records.length > 0) {
						var options = {};
						options.width = 400;
						options.height = 300;
						loadDialogPage($("#batccc_data_div"), "批量结果", null, {
							"确定" : function() {
									var result = $("#clean").val();
									$.each(records, function(i, obj) {
//										if(obj.get("id")!=null || obj.get("id")!=undefined){
											obj.set("clean", result);
//										}
									});
								$(this).dialog("close");
							}
						}, true, options);
						}else{
							message("请选择样本！");
						}
					}
				});
//				opts.tbar.push({
//						text : '选择项目编号',
//						//handler : selectprojectFun
//						handler : null
//					});
			    
				/*opts.tbar.push({
						text : '选择相关主表',
						handler : selecttransportApplyFun
					});*/
				
				opts.tbar.push({
					text : '显示可编辑列',
					handler : null
				});
				opts.tbar.push({
					text : '取消选中',
					handler : null
				});
				opts.tbar.push({
					text : '填加明细',
					handler : null
//					iconCls : 'add',
//					handler : function() {
//						var ob = transportApplyAnimalGrid.getStore().recordType;
//						var p = new ob({});
//						p.isNew = true;
//						p.set("project-id", $("#transportApply_project").val());
//						transportApplyAnimalGrid.stopEditing();
//						transportApplyAnimalGrid.getStore().insert(0, p);
//						transportApplyAnimalGrid.startEditing(0, 0);
//					}
				});
				
				opts.tbar.push({
					text : "批量上传（CSV文件）",
					handler : function() {
						var options = {};
						options.width = 350;
						options.height = 200;
						loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
							"确定":function(){
								goInExcelcsv();
								$(this).dialog("close");
							}
						},true,options);
					}
				});
		 }

	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = transportApplyAnimalGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 9-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							transportApplyAnimalGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	transportApplyAnimalGrid=gridEditTable("transportApplyAnimaldiv",cols,loadParam,opts);
	$("#transportApplyAnimaldiv").data("transportApplyAnimalGrid", transportApplyAnimalGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
//获取选中的数据ID
//选择动物主数据
function selectcodeFun(){
	
		var win = Ext.getCmp('selectAnimalMain');
		if (win) {win.close();}
		var selectAnimalMain= new Ext.Window({
			id:'selectAnimalMain', modal:true, title:'选择基因动物', layout:'fit', width:800, height:600, closeAction:'close',
			plain:true, bodyStyle:'padding:5px;', buttonAlign:'right',
			collapsible: true, maximizable:true,
			items:new Ext.BoxComponent({id:'maincontent', region:'center',
			html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/ani/animal/animalMain/animalMainSelect.action' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			          { text: '确定',
			        	  handler: function(){
			        		  window.frames[0].collectElements();
			        		  selectAnimalMain.close(); }  },
			          { text: '取消',
			        	  handler: function(){
			        		  selectAnimalMain.close(); }  }
			        		  								]  });     
		selectAnimalMain.show(); 
}
function addAnimalMain(data){
	var ob = transportApplyAnimalGrid.getStore().recordType;
	transportApplyAnimalGrid.stopEditing();
	for ( var i = 0; i < data.length; i++) {
		var p = new ob({});
		p.isNew = true;
		p.set("animalId", data[i].get("id"));
		p.set("mark", data[i].get("mark"));
		p.set("genetype", data[i].get("geneType"));
		p.set("birthDate",new Date(data[i].get("birthDate")));
		p.set("gender", data[i].get("gender"));
		
		transportApplyAnimalGrid.getStore().add(p);
		
	};
	transportApplyAnimalGrid.startEditing(0, 0);
}
//查询野生动物生成数据
function selectAnimalwildFun(){
//	var selRecords = injectIvfNewItemGrid.getSelectionModel().getSelections();
//	if(selRecords && selRecords.length>0){
		var win = Ext.getCmp('selectAnimalWild');
		if (win) {win.close();}
		var selectAnimalWild= new Ext.Window({
			id:'selectAnimalWild', modal:true, title:'选择野生动物', layout:'fit', width:800, height:600, closeAction:'close',
			plain:true, bodyStyle:'padding:5px;', buttonAlign:'right',
			collapsible: true, maximizable:true,
			items:new Ext.BoxComponent({id:'maincontent', region:'center',
			html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/ani/animal/animalMain/animalMainWildSelect.action' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			          { text: '确定',
			        	  handler: function(){
			        		  window.frames[0].collectElements();
			        		  selectAnimalWild.close(); }  },
			          { text: '取消',
			        	  handler: function(){
			        		  selectAnimalWild.close(); }  }
			]  });     
		selectAnimalWild.show(); 
//	}
//	else {
//		message("请先选中数据！");
//		return ;
//	}

}
//添加野生鼠
function addAnimalWild(data){
	var flag = false;
	for ( var i = 0; i < data.length; i++) {
		gender = data[i].get("gender");
			var ob = transportApplyAnimalGrid.getStore().recordType;
			transportApplyAnimalGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;
			p.set("animalId", data[i].get("code"));
			if(gender==1){
				p.set("gender", "雄");
			}else if(gender==0){
				p.set("gender", "雌");
			}
			
			p.set("birthDate",new Date(data[i].get("birthDate")) );
			p.set("genetype", data[i].get("geneType"));
			transportApplyAnimalGrid.getStore().add(p);
			transportApplyAnimalGrid.startEditing(0, 0);
			flag =true;
	}
				
	if (flag == true) {//填到表里的数据
			//标记选中的对象
			for ( var j = 0; j < data.length; j++) {
				ajax("post", "/ani/animal/animalOut/signAnimalItemList.action", {
					animalItemId : data[j].get("id")
				}, function(data) {
				}, null);
		}
	}
}