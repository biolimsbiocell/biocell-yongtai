﻿
$(function() {
//	$("#tabs").tabs({
//		select : function(event, ui) {
//		}
//	});
	showType();
	
})	
function add() {
	window.location = window.ctx + "/tra/transport/transportApply/editTransportApply.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/tra/transport/transportApply/showTransportApplyList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var id=$("#transportApply_id").val();
	if (!id) {
		message("编码不能为空！");
		return;
	}
	var project = $("#transportApply_project").val();
	if (!project) {
		message("项目不能为空！");
		return;
	}
	save();
});	

$("#toolbarbutton_tjsp").click(function() {
				if ($("#transportApply_confirmUser_name").val() == "") {
					message("第一审核人未填写");
					return;
				}
				if ($("#transportApply_confirmUserTwo_name").val() == "") {
					message("第二审核人未填写");
					return;
				}
				
				submitWorkflow("TransportApply", {
					userId : userId,
					userName : userName,
					formId : $("#transportApply_id").val(),
					title : $("#transportApply_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#transportApply_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
});

function change(){
	var selected = $('#transportApply_objType option:selected') .val();
	if(selected=="0"){
		$("#transportApplyAnimalpage").css("display", "");
		$("#transportApplyBacteriapage").css("display", "none");
		$("#transportApplyCellpage").css("display", "none");
		$("#transportApplyTissuepage").css("display", "none");
	}else if(selected=="1"){
		$("#transportApplyAnimalpage").css("display", "none");
		$("#transportApplyBacteriapage").css("display", "");
		$("#transportApplyCellpage").css("display", "none");
		$("#transportApplyTissuepage").css("display", "none");
	}else if(selected=="2"){
		$("#transportApplyAnimalpage").css("display", "none");
		$("#transportApplyBacteriapage").css("display", "none");
		$("#transportApplyCellpage").css("display", "");
		$("#transportApplyTissuepage").css("display", "none");
	}else{
		$("#transportApplyAnimalpage").css("display", "none");
		$("#transportApplyBacteriapage").css("display", "none");
		$("#transportApplyCellpage").css("display", "none");
		$("#transportApplyTissuepage").css("display", "");
	}
	showType();
}


function save() {
	if(remain()==false){
		message("运输数量不能超过在库数量，请重新输入！");
		return ;
	}
if(checkSubmit()==true){
	var type=$("#transportApply_objType").val();
	if(type=="0"){
	    //var transportApplyAnimalDivData = $("#transportApplyAnimaldiv").data("transportApplyAnimalGrid");
		var transportApplyAnimalDivData = transportApplyAnimalGrid;
		document.getElementById('transportApplyAnimalJson').value = commonGetModifyRecords(transportApplyAnimalDivData);
	}else if(type=="1"){
		//var transportApplyBacteriaDivData = $("#transportApplyBacteriadiv").data("transportApplyBacteriaGrid");
		var transportApplyBacteriaDivData = transportApplyBacteriaGrid;
		document.getElementById('transportApplyBacteriaJson').value = commonGetModifyRecords(transportApplyBacteriaDivData);
	}else if(type=="2"){
		//var transportApplyCellDivData = $("#transportApplyCelldiv").data("transportApplyCellGrid");
		var transportApplyCellDivData = transportApplyCellGrid;
		document.getElementById('transportApplyCellJson').value = commonGetModifyRecords(transportApplyCellDivData);
	}else{
		//var transportApplyTissueDivData = $("#transportApplyTissuediv").data("transportApplyTissueGrid");
		var transportApplyTissueDivData = transportApplyTissueGrid;
		document.getElementById('transportApplyTissueJson').value = commonGetModifyRecords(transportApplyTissueDivData);
	}
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/tra/transport/transportApply/save.action";
		form1.submit();	
		}
}	
function remain(){
	var type=$("#transportApply_objType").val();
	if(type=="0"){
		var selRecord = transportApplyAnimalGrid.store.getModifiedRecords();
		if (selRecord && selRecord.length > 0) {
			var store10 = transportApplyAnimalGrid.store;
			var gridCount10 = store10.getCount();
			for(var i=0;i<gridCount10;i++){
				var remainNum = store10.getAt(i).get("animalItem-inNum");
				var num = store10.getAt(i).get("num");
				if(num!=""){
					var remain = remainNum - num;
					if(remain < 0){
						remain = -1;
						return false;
					}
				}
			}
		return true;
		}
	}else if(type=="2"){
		var selRecord = transportApplyCellGrid.store.getModifiedRecords();
		if (selRecord && selRecord.length > 0) {
			var store10 = transportApplyCellGrid.store;
			var gridCount10 = store10.getCount();
			for(var i=0;i<gridCount10;i++){
				var remainNum = store10.getAt(i).get("materailsMainCell-num");
				var num = store10.getAt(i).get("num");
				if(num!=""){
					var remain = remainNum - num;
					if(remain < 0){
						remain = -1;
						return false;
					}
				}
			}
		return true;
		}
	}else if(type=="3"){
		var selRecord = transportApplyTissueGrid.store.getModifiedRecords();
		if (selRecord && selRecord.length > 0) {
			var store10 = transportApplyTissueGrid.store;
			var gridCount10 = store10.getCount();
			for(var i=0;i<gridCount10;i++){
				var remainNum = store10.getAt(i).get("animalItem-inNum");
				var num = store10.getAt(i).get("num");
				if(num!=""){
					var remain = remainNum - num;
					if(remain < 0){
						remain = -1;
						return false;
					}
				}
			}
			return true;
		}
	}
}
function editCopy() {
	window.location = window.ctx + '/tra/transport/transportApply/copyTransportApply.action?id=' + $("#transportApply_id").val();
}
$("#toolbarbutton_status").click(function() {
		if($("#transportApply_id").val())
			commonChangeState("formId=" + $("#transportApply_id").val() + "&tableId=TransportApply");
	
});
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#transportApply_id").val());
	fs.push($("#transportApply_project").val());
	fs.push($("#transportApply_objType").val());
	nsc.push("编码不能为空！");
	nsc.push("项目编号不能为空！");
	nsc.push("物品类型不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'运输申请',
	    	   contentEl:'markup'
	       } ]
	   });
});

var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
function showType(){
	var type=$("#transportApply_objType").val();
	if(type=="0"){
			load("/tra/transport/transportApply/showTransportApplyAnimalList.action", {
				id : $("#transportApply_id").val()
			}, "#transportApplyAnimalpage");
	}else if(type=="1"){
			load("/tra/transport/transportApply/showTransportApplyBacteriaList.action", {
				id : $("#transportApply_id").val()
			}, "#transportApplyBacteriapage");
	}else if(type=="2"){
			load("/tra/transport/transportApply/showTransportApplyCellList.action", {
				id : $("#transportApply_id").val()
			}, "#transportApplyCellpage");
	}else{
			load("/tra/transport/transportApply/showTransportApplyTissueList.action", {
				id : $("#transportApply_id").val()
			}, "#transportApplyTissuepage");
	}
}
	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
	function selectnextStepNameTable() {
		var title = '';
		var url = '';
		title = "选择后续实验模块";
		url = "/exp/project/project/showDialogApplicationTypeTableList.action";
		var option = {};
		option.width = 500;
		option.height = 500;
		loadDialogPage(null, title, url, {
			"确定" : function() {
				selVal(this);
				}
			}, true, option);
	}
	var selVal = function(win) {
	var operGrid = $("#show_dialog_applicationtypetable_grid_div").data("waitApplicationtypeApplyGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
	var str = "";
	var strName = "";
		if (selectRecord.length > 0) {
			$.each(selectRecord, function(i, obj) {
				str = str + obj.get("id")+",";
				strName = strName + obj.get("name")+",";
			});
			document.getElementById("transportApply_nextStepName_id").value =str;
			document.getElementById("transportApply_nextStepName").value =strName;
			$(win).dialog("close");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
	
	//审核材料归还时先检测归还数量是否正常
	function checkNum(){
		var type=$("#transportApply_objType").val();
		var grid = null;
		if(type=="0"){
			grid = transportApplyAnimalGrid.store;
		}else if(type=="1"){
			grid = transportApplyBacteriaGrid.store;
		}else if(type=="2"){
			grid = transportApplyCellGrid.store;
		}else if(type=="3"){
			grid = transportApplyTissueGrid.store;
		}
		if(type=="1"){
			for(var i=0;i<grid.getCount();i++){
				var num = grid.getAt(i).get("volume");
				if(num==null||num==""||parseInt(num)<=0)
					return false;
			}
		}else{
			for(var i=0;i<grid.getCount();i++){
				var num = grid.getAt(i).get("num");
				if(num==null||num==""||parseInt(num)<=0)
					return false;
			}
		}
		
		return true;
	}