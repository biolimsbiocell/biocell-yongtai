﻿var transportApplyTemp;
$(function() {
	var colOpts=[]
	colOpts.push({
		"data": "id",
		"visible": false,
		"title": biolims.common.id,
	})
	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
	})
	colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
	})
	colOpts.push({
		"data": "sampleOrder-id",
		"title": biolims.common.orderCode,
	})
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
	})
	colOpts.push({
		"data": "pronoun",
		"title": biolims.common.pronoun,
	})
	colOpts.push({
		"data": "sampleType",
		"title": biolims.common.sampleType,
	})
	colOpts.push({
		"data": "sampleOrder-crmCustomer-name",
		"title": "医院",
	})
	colOpts.push({
		"data": "sampleOrder-medicalInstitutionsPhone",
		"title": "医院电话",
	})
	colOpts.push({
		"data": "sampleInfo-id",
		"visible":false,
		"title": biolims.sample.sampleMasterData,
	})
	colOpts.push({
		"data": "productId",
		"visible":false,
		"title":"产品编号",
	})
	var tbarOpts=[]
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search()
		}
	});
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action:function(){
			var rows = $("#transportApplyTemp .selected");
			if(!rows.length){
				top.layer.msg("请选择数据!");
				return false;
			}
			var sampleId = [];
			$("#transportApplyTemp .selected").each(function(i, val) {
				sampleId.push($(val).children("td").eq(0).find("input").val());
			});
			var mainJson = JSON.stringify($("#form1").serializeObject());
			$.ajax({
				type: 'post',
				url: '/tra/transport/transportApply/addTransportOrderMakeUp.action',
				data: {
					main: mainJson,
					ids: sampleId
				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						transportApplyTemp.ajax.reload();
						transportApplyItemTable.ajax.reload();
//						$("#transportApply_id").val(data.data);
//						var param = {
//							id: data.data
//						};
//						transportOrderItemTable.settings()[0].ajax.data = param;
					} else {
						top.layer.msg(biolims.common.addToDetailFailed);
					};
				}
			})
		}
	});
	var options = table(true,null,"/tra/transport/transportApply/showTransportApplyTempTableJson.action",colOpts, tbarOpts)
	//渲染样本的table
	transportApplyTemp=renderData($("#transportApplyTemp"),options);
})

$.fn.serializeObject = function(){
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"txt":biolims.common.projectId,
		"type": "input",
		"searchName": "sampleInfo-project-id",
	}, {
		"txt":biolims.common.orderCode,
		"type": "input",
		"searchName": "sampleOrder-id",
	}, {
		"txt":biolims.common.sampleType,
		"type": "input",
		"searchName": "sampleType",
	}, {
		"txt":biolims.common.productName,
		"type": "input",
		"searchName": "productName",
	},  {
		"type" : "table",
		"table" : transportApplyTemp
	} ];
}
//核对编码
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layero) {
			var arr = $("#checkCode").val().split("\n");
			cellPassageAllotTab.settings()[0].ajax.data = {"codes":arr};
			cellPassageAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});

}