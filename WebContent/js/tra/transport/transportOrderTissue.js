﻿var transportOrderTissueGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
			name:'name',
			type:"string"
		});
		    fields.push({
			name:'animalMain-id',
			type:"string"
		});
		    fields.push({
			name:'animalMain-name',
			type:"string"
		});
	    fields.push({
			name:'animalItem-id',
			type:"string"
		});
		   fields.push({
			name:'animalName',
			type:"string"
		});
		   fields.push({
			name:'gender',
			type:"string"
		});
	   fields.push({
			name:'inNum',
			type:"string"
		});
		   fields.push({
			name:'num',
			type:"string"
		});
		   fields.push({
			name:'genetype',
			type:"string"
		});
		   fields.push({
			name:'animalItem-birthDate',
			type:"string"
//			type:"date",
//			dateFormat:"Y-m-d"
		});
		   fields.push({
			name:'way',
			type:"string"
		});
//	    fields.push({
//		name:'transportOrder-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'transportOrder-name',
//		type:"string"
//	});
//	   fields.push({
//		name:'birthDate',
//		type:"string"
//	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:'组织名称',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'animalMain-id',
		hidden : false,
		header:'编号',
		width:15*10
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'animalMain-name',
		hidden : false,
		header:'动物名称',
		width:15*10
	});
	cm.push({
		dataIndex:'animalMItem-id',
		hidden : true,
		header:'明细编号',
		width:15*10
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'gender',
		hidden : false,
		header:'性别',
		width:10*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
//	cm.push({
//		dataIndex:'inNum',
//		hidden : false,
//		header:'在库数量',
//		width:20*6
//	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'数量 *',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals: false, // 不允许小数点 
			allowNegative: false, // 不允许负数 
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'genetype',
		hidden : false,
		header:'基因型',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'animalItem-birthDate',
		hidden : false,
		header:'出生日期',
		width:15*6
//		
//		renderer: formatDate,
//		editor: new Ext.form.DateField({format: 'Y-m-d'})
	});
	cm.push({
		dataIndex:'way',
		hidden : false,
		header:'运输方式',
		width:20*6
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
//	cm.push({
//		dataIndex:'transportOrder-id',
//		hidden : true,
//		header:'相关主表ID',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'transportOrder-name',
//		hidden : false,
//		header:'相关主表',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'birthDate',
//		hidden : false,
//		header:'出生日期',
//		width:20*6,
//		
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/transport/transportOrder/showTransportOrderTissueListJson.action?id="+ $("#id_parent_hidden").val()+"&wid="+$("#applyId").val();
	var opts={};
	opts.title="产品运输（组织）信息";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/tra/transport/transportOrder/delTransportOrderTissue.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
//    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selecttransportOrderFun
//		});
	
	
	
	
	
	
	
//	
//	opts.tbar.push({
//		text : "批量上传（CSV文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = transportOrderTissueGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 8-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							transportOrderTissueGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	};
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
//	opts.tbar.push({
//		text : '填加明细',
//		handler : null
//	});
	transportOrderTissueGrid=gridEditTable("transportOrderTissuediv",cols,loadParam,opts);
	$("#transportOrderTissuediv").data("transportOrderTissueGrid", transportOrderTissueGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

//function selecttransportOrderFun(){
//	var win = Ext.getCmp('selecttransportOrder');
//	if (win) {win.close();}
//	var selecttransportOrder= new Ext.Window({
//	id:'selecttransportOrder',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TransportOrderSelect.action?flag=transportOrder' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selecttransportOrder.close(); }  }]  });     selecttransportOrder.show(); }
//	function settransportOrder(id,name){
//		var gridGrid = $("#transportOrderTissuediv").data("transportOrderTissueGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('transportOrder-id',id);
//			obj.set('transportOrder-name',name);
//		});
//		var win = Ext.getCmp('selecttransportOrder')
//		if(win){
//			win.close();
//		}
//	}
	
