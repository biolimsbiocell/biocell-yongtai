﻿var transportOrderItemTable;
$(function(){
	//日期格式化
	$(".formatDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	
	var stateName = $("#transportApply_state").attr("state");
	var handlemethod=$("#handlemethod").val();
	if(stateName == "1"||handlemethod=="view") {
		settextreadonly();
		$("#leftDiv").hide();
		$("#rightDiv").attr("class", "col-md-12 col-xs-12");
	}
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'transportApply', $("#transportApply_id").val());
	
	//ai图片识别
	$("#aiBtn").click(function() {
		$("#AiPicture").click();
	});
	$("#aiBtn2").click(function() {
		$("#AiPicture2").click();
	});
	
})


function list() {
	window.location = window.ctx + '/tra/transport/transportApply/showTransportApplyList.action';
}

function add() {
	window.location = window.ctx + '/tra/transport/transportApply/editTransportOrder.action';
}

function save(){
	//loading
	top.layer.load(4, {
		shade: 0.3
	});
	var itemDataJson = saveTransportOrderItemjson($("#transportApplyItem"));
	//必填验证
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	//日志
	var changeLog = "";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	var changeLogItem = "产品运输计划明细"+":";
	changeLogItem = getChangeLog(itemDataJson, $("#main"), changeLogItem);
	//页面数据
	var jsonn = {};
	$("#form1 input").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	$("#form1 select").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	data = {
		mainData : JSON.stringify(jsonn),
		changeLog : changeLog,
		itemData : itemDataJson,
		changeLogItem : changeLogItem
	}
	$.ajax({
		url : ctx + '/tra/transport/transportApply/save.action',
		type : 'post',
		data : data,
		success : function(data){
			var data = JSON.parse(data);
			if(data.success){//成功
				top.layer.closeAll();
				var url = "/tra/transport/transportApply/editTransportApply.action?id="
					+ data.id;
				window.location.href = url;
				top.layer.msg(biolims.common.saveSuccess);
			}else{//失败
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed);
			}
		}
	})
}
function save(){
	//自定义字段
	//拼自定义字段（实验记录）
	var inputs = $("#fieldItemDiv input");
	var options = $("#fieldItemDiv option");
	var contentData = {};
	var checkboxArr = [];
	$("#fieldItemDiv .checkboxs").each(function(i, v) {
		$(v).find("input").each(function(ii, inp) {
			var k = inp.name;
			if(inp.checked == true) {
				checkboxArr.push(inp.value);
				contentData[k] = checkboxArr;
			}
		});
	});
	inputs.each(function(i, inp) {
		var k = inp.name;
		if(inp.type != "checkbox") {
			contentData[k] = inp.value;
		}
	});
	options.each(function(i, opt) {
		if(opt.selected == true) {
			var k = opt.getAttribute("name");
			contentData[k] = opt.value;
		}
	});
	//document.getElementById("fieldContent").value = JSON.stringify(contentData);
	
	var changeLog = "";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	var jsonStr = JSON.stringify($("#form1").serializeObject());  
	$.ajax({
		url: ctx + '/tra/transport/transportApply/saveNew.action',
		dataType: 'json',
		type: 'post',
		data: {
			dataValue: jsonStr,
			changeLog: changeLog,
			//sampleTransportItemJson : saveSampleTransportItemjson($("#sampleTransportItemTable")),
			//sampleTrackingConditionJson : saveSampleTrackingConditionjson($("#sampleTrackingConditionTable")),
			bpmTaskId : $("#bpmTaskId").val()
		},
		success: function(data) {
//			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				var url = "/tra/transport/transportApply/editTransportApply.action?id="+data.id;
				
				if(data.bpmTaskId){
					url = url +"&bpmTaskId="+data.bpmTaskId;
				}
				window.location.href=url;
				top.layer.msg(biolims.common.saveSuccess);
			}else{
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed);
			}
			}
	});
}
function checkSubmit() {
	if($("#transportApply_id").val() == null || $("#transportApply_id").val() == "") {
		top.layer.msg(biolims.common.codeNotEmpty);
		return false;
	};
	return true;
}