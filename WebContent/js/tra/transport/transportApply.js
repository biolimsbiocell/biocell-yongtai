var transportOrderListTable;
$(function(){
	var colData = [{
			"data": "id",
			"title": biolims.common.id
		}, {
			"data": "name",
			"title": "描述"
		}, {
			"data": "state",
			"title": biolims.common.state,
			"visible":false
		}, {
			"data": "stateName",
			"title": biolims.common.state
		}];
	
	var options = table(true, "","/tra/transport/transportApply/showTransportApplyListJson.action",colData , null)
	transportOrderListTable = renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(transportOrderListTable);
	})
})

// 新建
function add() {
	window.location = window.ctx + "/tra/transport/transportApply/editTransportApply.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/tra/transport/transportApply/editTransportApply.action?id=' + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		'/tra/transport/transportOrder/toViewTransportOrder.action?id=' + id;
}
//弹框模糊查询参数
function searchOptions() {
	return [{
			"txt": biolims.common.id,
			"type": "input",
			"searchName": "id"
		},
		{
			"txt": biolims.common.name,
			"type": "input",
			"searchName": "name"
		},
		{
			"txt": biolims.sample.createUserName,
			"type": "input",
			"searchName": "createUser"
		},
		{
			"txt": biolims.sample.createDateStart,
			"type": "dataTime",
			"searchName": "createDate##@@##",
			"mark": "##@@##",
		},
		{
			"txt": biolims.common.state,
			"type": "input",
			"searchName": "stateName",
		},
		{
			"type": "table",
			"table": transportOrderListTable
		}
	];
}