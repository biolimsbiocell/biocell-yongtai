﻿var transportOrderCellGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
			name:'code',
			type:"string"
		});
	   fields.push({
			name:'sampleCode',
			type:"string"
		});
	   fields.push({
			name:'pronoun',
			type:"string"
		});
	   fields.push({
			name:'dicSampleType-id',
			type:"string"
		});
	   fields.push({
			name:'dicSampleType-name',
			type:"string"
		});
//	   fields.push({
//		name:'materailsMain-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'materailsMain-name',
//		type:"string"
//	});
//	    fields.push({
//		name:'materailsMainCell-id',
//		type:"string"
//	});
//	   fields.push({
//		name:'cloneCode',
//		type:"string"
//	});
//	   fields.push({
//		name:'materailsMainCell-generation',
//		type:"string"
//	});
//	   fields.push({
//		name:'background',
//		type:"string"
//	});
//	   fields.push({
//	   name:'num',
//	   type:"string"
//    });
//	   	fields.push({
//		name:'way',
//		type:"string"
//	});
	    fields.push({
		name:'transportOrder-id',
		type:"string"
	});
	    fields.push({
		name:'transportOrder-name',
		type:"string"
	});
//	    fields.push({
//		name:'projectName',
//		type:"string"
//	});
//	   fields.push({
//		name:'genus',
//		type:"string"
//	});
//	   fields.push({
//		name:'electricTransTime',
//		type:"string"
//	});
//	   fields.push({
//		name:'generation',
//		type:"string"
//	});
//	   fields.push({
//		name:'strain',
//		type:"string"
//	});
//	   fields.push({
//		name:'genoType',
//		type:"string"
//	}); 
	   fields.push({
		name:'numTubes',
		type:"string"
	}); 
//	   fields.push({
//		name:'detectionMycoplasma',
//		type:"string"
//	}); 
	   fields.push({
		name:'transportTemperature',
		type:"string"
	}); 
//	   fields.push({
//		name:'note',
//		type:"string"
//	}); 
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6
	});
	cm.push({
		dataIndex:'code',
		hidden : false,
		header:'样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'sampleCode',
		hidden : false,
		header:'原始样本编号',
		width:20*6
	});
	cm.push({
		dataIndex:'pronoun',
		hidden : false,
		header:'代次',
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-id',
		hidden : true,
		header:'样本类型id',
		width:20*6
	});
	cm.push({
		dataIndex:'dicSampleType-name',
		hidden : false,
		header:'样本类型',
		width:20*6
	});
//	cm.push({
//		dataIndex:'materailsMain-id',
//		hidden : true,
//		header:'主数据ID',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'materailsMain-name',
//		hidden : true,
//		header:'主数据名称',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'projectName',
//		hidden : false,
//		header:'项目名称',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'materailsMainCell-id',
//		hidden : true,
//		header:'细胞ID',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'cellName',
//		hidden : false,
//		header:'细胞名称',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'materailsMainCell-name',
//		hidden : true,
//		header:'细胞名称',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'genus',
//		hidden : false,
//		header:'种属',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'cloneCode',
//		hidden : false,
//		header:'克隆号',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'electricTransTime',
//		hidden : false,
//		header:'电转次数',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'generation',
//		hidden : false,
//		header:'代次',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'strain',
//		hidden : false,
//		header:'品系',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'genoType',
//		hidden : false,
//		header:'基因型',
//		width:15*10
//	});
	cm.push({
		dataIndex:'numTubes',
		hidden : false,
		header:'细胞数/管',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'detectionMycoplasma',
//		hidden : false,
//		header:'支原体检测（有/无）',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
	cm.push({
		dataIndex:'transportTemperature',
		hidden : false,
		header:'运输温度',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'materailsMainCell-cloneCode',
//		hidden : true,
//		header:'克隆号',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'materailsMainCell-generation',
//		hidden : false,
//		header:'代次',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'materailsMainCell-background',
//		hidden : false,
//		header:'细胞背景',
//		width:15*10
//	});
//	cm.push({
//		dataIndex:'num',
//		hidden : false,
//		header:'管数 *',
//		width:40*6,
//		editor : new Ext.form.NumberField({
//			allowDecimals: false, // 不允许小数点 
//			allowNegative: false, // 不允许负数 
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'way',
//		hidden : false,
//		header:'运输方式',
//		width:20*6
//	});
	cm.push({
		dataIndex:'transportOrder-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportOrder-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/transport/transportOrder/showTransportOrderCellListJson.action?id="+ $("#id_parent_hidden").val()+"&wid="+$("#applyId").val();
	var opts={};
	opts.title="产品运输（细胞）信息";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/tra/transport/transportOrder/delTransportOrderCell.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selecttransportOrderFun
//		});
//	
//	
//	opts.tbar.push({
//		text : "批量上传（CSV文件）",
//		handler : function() {
//			var options = {};
//			options.width = 350;
//			options.height = 200;
//			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
//				"确定":function(){
//					goInExcelcsv();
//					$(this).dialog("close");
//				}
//			},true,options);
//		}
//	});

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = transportOrderCellGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							transportOrderCellGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
//	opts.tbar.push({
//		text : '填加明细',
//		handler : null
//	});
	transportOrderCellGrid=gridEditTable("transportOrderCelldiv",cols,loadParam,opts);
	$("#transportOrderCelldiv").data("transportOrderCellGrid", transportOrderCellGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

//function selecttransportOrderFun(){
//	var win = Ext.getCmp('selecttransportOrder');
//	if (win) {win.close();}
//	var selecttransportOrder= new Ext.Window({
//	id:'selecttransportOrder',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TransportOrderSelect.action?flag=transportOrder' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selecttransportOrder.close(); }  }]  });     selecttransportOrder.show(); }
//	function settransportOrder(id,name){
//		var gridGrid = $("#transportOrderCelldiv").data("transportOrderCellGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('transportOrder-id',id);
//			obj.set('transportOrder-name',name);
//		});
//		var win = Ext.getCmp('selecttransportOrder')
//		if(win){
//			win.close();
//		}
//	}
	
