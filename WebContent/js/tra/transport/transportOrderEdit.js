﻿var transportOrderItemTable;
$(function(){
	//日期格式化
	$(".formatDate").datepicker({
		language: "zh-TW",
		autoclose: true, //选中之后自动隐藏日期选择框
		format: "yyyy-mm-dd" //日期格式，详见 
	});
	
	$("#transportOrder_transportDate").datepicker({
		language : "zh-TW",
		autoclose : true, // 选中之后自动隐藏日期选择框
		format : "yyyy-mm-dd " // 日期格式，详见
	});
	//$("#transportOrder_transportDate").bind('click',function(event){timePacker($(this),event)});
	
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'transportOrder', $("#transportOrder_id").val());
	
	//自定义字段
	//fieldCustomFun();
	//ai图片识别
	$("#aiBtn").click(function() {
		$("#AiPicture").click();
	});
	$("#aiBtn2").click(function() {
		$("#AiPicture2").click();
	});
	stepChange();
})



// 点击上一步下一步切换
function stepChange() {
	var bpmTaskId = $("#bpmTaskId").val();
	var handlemethod = $("#handlemethod").val();
	if(bpmTaskId != "null" || handlemethod == "view") {
		$(".wizard_steps .step").removeClass("selected").removeClass("disabled").addClass("done");
		$("#next").hide();
		$("#pre").hide();
	}
	$("#next").unbind("click").click(
		function() {
			$("#pre").attr("disabled", false);
			var index = $(".wizard_steps .step").index(
				$(".wizard_steps .selected"));
			if(index == $(".wizard_steps .step").length - 1) {
				$(this).attr("disabled", true);
				return false;
			}
			$(".HideShowPanel").eq(index + 1).slideDown().siblings(
				'.HideShowPanel').slideUp();
			$(".wizard_steps .step").eq(index).removeClass("selected")
				.addClass("done");
			var nextStep = $(".wizard_steps .step").eq(index + 1);
			if(nextStep.hasClass("disabled")) {
				nextStep.removeClass("disabled").addClass("selected");
			} else {
				nextStep.removeClass("done").addClass("selected");
			}
			//table刷新和操作显示
			$(".box-tools").show();
			// 回到顶部
			$('body,html').animate({
				scrollTop: 0
			}, 500, function() {
				transportOrderTempTable.ajax.reload();
				transportOrderItemTable.ajax.reload();
			});
		});
	$("#pre").click(
		function() {
			$("#next").attr("disabled", false);
			var index = $(".wizard_steps .step").index(
				$(".wizard_steps .selected"));
			if(index == 0) {
				$(this).attr("disabled", true);
				//table刷新和操作隐藏
				$(".box-tools").hide();
				return false;
			}
			$(".HideShowPanel").eq(index - 1).slideDown().siblings(
				'.HideShowPanel').slideUp();
			$(".wizard_steps .step").eq(index).removeClass("selected")
				.addClass("done");
			$(".wizard_steps .step").eq(index - 1).removeClass("done")
				.addClass("selected");
			$('body,html').animate({
				scrollTop: 0
			}, 500);
		});
	$(".step").click(
		function() {
			if($(this).hasClass("done")) {
				var index = $(".wizard_steps .step").index($(this));
				$(".wizard_steps .selected").removeClass("selected")
					.addClass("done");
				$(this).removeClass("done").addClass("selected");
				$(".HideShowPanel").eq(index).slideDown().siblings(
					'.HideShowPanel').slideUp();
				//table刷新和操作隐藏
				if(index == 0) {
					$(".box-tools").hide();
				} else {
					$(".box-tools").show();
					transportOrderTempTable.ajax.reload();
					transportOrderItemTable.ajax.reload();
				}

			} else if($(this).hasClass("disabled")) {
				top.layer.msg(biolims.order.finishPre);
			}
		});
}

function list() {
	window.location = window.ctx + '/tra/transport/transportOrder/showTransportOrderList.action';

}

function add() {
	window.location = window.ctx + '/tra/transport/transportOrder/editTransportOrder.action';
}

function save(){
	//电话
	var reg3 = /^[1][3,4,5,7,8][0-9]{9}$/;
	var lc = /^[0-9]*$/;
	var lc1 = true;
	var lc2 = true;
	var lc3 = true;
	var lc4 = true;
	var lc5 = true;
	var lc6 = true;
	$("#transportOrderItem tbody tr").each(function(i,val){
    	var tds = $(val).find("td[savename='address']").text();
    	if(!lc.test(tds) && tds!=""){
    		top.layer.msg("请输入正确的楼层(数字)!");
    		lc1 = false;
    		return false;
    	}
    });
	if(lc1 == false){
		top.layer.msg("请输入正确的楼层(数字)!");
		return false;
	}
	$("#transportOrderItem tbody tr").each(function(i,val){
		var tds = $(val).find("td[savename='numTubes']").text();
		if(!lc.test(tds) && tds!=""){
			top.layer.msg("请输入正确的管/袋数(数字)!");
			lc2 = false;
			return false;
		}
	});
	if(lc2 == false){
		top.layer.msg("请输入正确的管/袋数(数字)!");
		return false;
	}
	var ysdh = /^\w+$/;
	$("#transportOrderItem tbody tr").each(function(i,val){
		var tds = $(val).find("td[savename='oddNumber']").text();
		if(!ysdh.test(tds) && tds!=""){
			top.layer.msg("请输入正确的运输单号(数字,字母)!");
			lc3 = false;
			return false;
		}
	});
	if(lc3 == false){
		top.layer.msg("请输入正确的运输单号(数字,字母)!");
		return false;
	}
	//联系人电话
	$("#transportOrderItem tbody tr").each(function(i,val){
		var tds = $(val).find("td[savename='linkmanPhone']").text();
		if(!reg3.test(tds) && tds!=""){
			top.layer.msg("联系人电话不是完整的11位手机号或者正确的手机号!");
			lc4 = false;
			return false;
		}
	});
	if(lc4 == false){
		top.layer.msg("联系人电话不是完整的11位手机号或者正确的手机号!");
		return false;
	}
	//医生电话
	$("#transportOrderItem tbody tr").each(function(i,val){
		var tds = $(val).find("td[savename='doctorTel']").text();
		if(!reg3.test(tds) && tds!=""){
			top.layer.msg("医生电话不是完整的11位手机号或者正确的手机号!");
			lc5 = false;
			return false;
		}
	});
	if(lc5 == false){
		top.layer.msg("医生电话不是完整的11位手机号或者正确的手机号!");
		return false;
	}
	$("#transportOrderItem tbody tr").each(function(i,val){
		var tds = $(val).find("td[savename='phoneNumber']").text();
		if(!reg3.test(tds) && tds!=""){
			top.layer.msg("联系方式不是完整的11位手机号或者正确的手机号!");
			lc6 = false;
			return false;
		}
	});
	if(lc6 == false){
		top.layer.msg("联系方式不是完整的11位手机号或者正确的手机号!");
		return false;
	}
	//loading
	top.layer.load(4, {
		shade: 0.3
	});
	var itemDataJson = saveTransportOrderItemjson($("#transportOrderItem"));
	//必填验证
	var requiredField=requiredFilter();
	if(!requiredField){
		return false;
	}
	//日志
	var changeLog = "";
	$('input[class="form-control"]').each(
			function(i, v) {
				var valnew = $(v).val();
				var val = $(v).attr("changelog");
				if (val !== valnew) {
					changeLog += $(v).prev("span").text() + ':由"' + val
							+ '"变为"' + valnew + '";';
				}
			});
	var changeLogItem = "产品运输明细"+":";
	changeLogItem = getChangeLog(itemDataJson, $("#main"), changeLogItem);
	var transportSample=saveSampleTransportItemjson($("#transportSampleItem"));
	var transportSampleChangeLog="样本运输明细:";
	transportSampleChangeLog=getSampleTransportChangeLog(transportSample,$("#transportSampleItem"),transportSampleChangeLog);
	
	var sampleTransportState=saveSampleTransportStatejson($("#transportSampleState"));
	var sampleTransportStateChangeLog="签收信息:";
	sampleTransportStateChangeLog=getSampleTransportStateChangeLog(sampleTransportState,$("#transportSampleState"),sampleTransportStateChangeLog);
	//页面数据
	var jsonn = {};
	$("#form1 input").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	$("#form1 select").each(function(i, v) {
		var k = v.name;
		jsonn[k] = v.value;
	});
	data = {
		mainData : JSON.stringify(jsonn),
		changeLog : changeLog,
		itemData : itemDataJson,
		changeLogItem : changeLogItem,
		transportSampleItem:transportSample,
		transportSampleChangeLog:transportSampleChangeLog,
		sampleTransportState:sampleTransportState,
		sampleTransportStateChangeLog:sampleTransportStateChangeLog,
	}
	$.ajax({
		url : ctx + '/tra/transport/transportOrder/save.action',
		type : 'post',
		data : data,
		success : function(data){
			var data = JSON.parse(data);
			if(data.success){//成功
				top.layer.closeAll();
				var url = "/tra/transport/transportOrder/editTransportOrder.action?id="
					+ data.id;
				window.location.href = url;
				top.layer.msg(biolims.common.saveSuccess);
			}else{//失败
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed);
			}
		}
	})
}
//选择运输负责人
function selectTransportUser(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#transportOrder_transportResponsibleUser").val(id);
			$("#transportOrder_transportResponsibleUser_name").val(name)
		},
	})
}
//选择签收确认人
function selectConfirmUser(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/user/selectUserTable.action?groupId=admin", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserTable .chosed").children("td").eq(
					0).text();
			top.layer.close(index)
			$("#transportOrder_signConfirmationUser").val(id);
			$("#transportOrder_signConfirmationUser_name").val(name)
		},
	})
}

//选择产物实验单
function selectProductResultOrder(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/experiment/processing/productProcessing/showStateCompleteInfoDialogList.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addStateCompleteInfo .chosed").
					children("td").eq(0).text();
			top.layer.closeAll();
			$("#transportOrder_outOrder").val(id);
			$.ajax({
				url : ctx + '/experiment/processing/productProcessing/addtoTransportOrderItemTable.action',
				type : 'post',
				data : {
					id : id
				},
				success : function(data){
					var data = JSON.parse(data);
					if(data.success){//成功
						//清除没有内容选项
						$("#main").find(".dataTables_empty").parent("tr").remove();
						var list = data.list;
						for(var i=0;i<list.length;i++){
							var code = list[i].code;
							var sampleCode = list[i].sampleCode;
							var sampleType = list[i].sampleType;
							var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
							var tds="<td savename='code'>"+code+"</td><td savename='sampleCode'>"+sampleCode+"</td>" +
								"<td savename='pronoun'></td><td savename='sampleType'>"+sampleType+"</td>" +
								"<td class='edit' savename='numTubes'></td><td class='edit' savename='transportTemperature'></td>";
							tr.height(32);
							$("#main").find("tbody").append(tr.append(tds));
						}
						checkall($("#main"));
					}
				}
			})
		},
	})
}

//上传附件
function fileUp() {
    var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
$("#tableFileLoad").html(str);
    var id = $("#transportOrder_id").val();
    if(id==""||id=="NEW"){
	        top.layer.msg("请先保存数据，再上传附件！");
	        return;
	}
	    

	$("#uploadFileValx").fileinput({
	        uploadUrl: ctx + "/attachmentUpload?useType=misc&modelType=transportOrder&contentId=" + id,
	        uploadAsync: true,
	        language: 'zh',
	        showPreview: true,
	        enctype: 'multipart/form-data',
	        elErrorContainer: '#kartik-file-errors',
	});

    //  if($("#uploadFile .fileinput-remove").length){
    //      $("#uploadFile .fileinput-remove").eq(0).click();
    //  }
	$("#uploadFilex").modal("show");
    //var cfileInput = fileInput('1','samplingTaskItem', id);
}
//附件查看
function fileView() {
    var id = $("#transportOrder_id").val();
    top.layer.open({
        title: biolims.common.attachment,
        type: 2,
        skin: 'layui-layer-lan',
        area: [600,400],
        content: window.ctx + "/operfile/initFileList.action?flag=misc&modelType=transportOrder&id=" + id,
        cancel: function(index, layero) {
            top.layer.close(index)
		}
	});
}