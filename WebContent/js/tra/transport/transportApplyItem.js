﻿var transportApplyItemTable;
var oldTransportOrderItemChangelog;
$(function(){
	var id = $("#transportApply_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "num",
		"title" : "数量",
		"createdCell" : function(td) {
			$(td).attr("saveName", "num");
		}
	});
	colOpts.push({
		"data" : "way-id",
		"title" : "运输方式Id",
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "way-id");
		}
	});
	colOpts.push({
		"data" : "way-name",
		"title" : "运输方式",
		"createdCell" : function(td) {
			$(td).attr("saveName", "way-name");
		}
	});
	colOpts.push({
		"data" : "sampleOrder-id",
		"title" : "关联订单",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-id");
		}
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		
		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				removeData($("#transportApplyItem"));
			}
		});
	}
	
	var options = table(true, id,
			'/tra/transport/transportApply/showTransportApplyItemListJson.action', colOpts, tbarOpts)
	transportApplyItemTable = renderData($("#transportApplyItem"), options);
	transportApplyItemTable.on('draw', function() {
		oldTransportOrderItemChangelog = transportApplyItemTable.ajax.json();
	});
})

function removeData(ele){
	var ids = [];
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.confirm(biolims.common.confirm2Del + length + biolims.common.record,
			{
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	},function(index) {
		top.layer.close(index);
		rows.each(function(i, val) {
			var id = $(val).find("input[type=checkbox]").val();
			if(id) {
				ids.push(id);
			} else {
				$(val).remove();
			}
		});
		if(ids.length) {
			$.ajax({
				type: "post",
				url: ctx + "/tra/transport/transportApply/delTransportApplyCell.action",
				data: {
					ids: ids
				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						top.layer.msg(biolims.common.deleteSuccess);
						transportApplyTemp.ajax.reload();
						//transportApplyItemTable.settings()[0].ajax.data ={id: $("#transportOrder_id").val()};
						transportApplyItemTable.ajax.reload();
					}
				}
			});
		}
	});
}

// 获得保存时的json数据
function saveTransportOrderItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();

		}
		data.push(json);
	});
	return JSON.stringify(data);
}
//日志
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '运输明细编号为"' + v.code + '":';
		oldTransportOrderItemChangelog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}