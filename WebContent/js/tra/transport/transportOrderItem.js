﻿﻿﻿var transportOrderItemTable;
var oldTransportOrderItemChangelog;
$(function(){
	var id = $("#transportOrder_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	
	
//	colOpts.push({
//		"data" : 'planState',
//		"title" : "是否放行审核通过",
//		//"className" : "select",
//		"name" : "" + "|" + "是" + "|" + "否",
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "planState");
//			$(td).attr("selectOpt","" + "|" + "是" + "|" + "否");
//		},
//		"render" : function(data, type, full, meta) {
//			if (data == '1') {
//				return "是";
//			} else if (data == '0') {
//				return "否";
//			} else {
//				return "";
//			}
//		}
//	});
	
	

	
//	 colOpts.push({ 
//		 "data" : "dicSampleType-name", 
//		 "title" : "样本类型",
//		 "createdCell" : function(td) { 
//			 $(td).attr("saveName","dicSampleType-name"); } 
//	 });
	 
	colOpts.push({
		"data" : "code",
		"title" : "细胞编号",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "code");
			$(td).attr("tempId", rowData['tempId']);
			$(td).attr("sampleOrder-id", rowData['sampleOrder-id']);
			$(td).attr("sampleCode", rowData['sampleCode']);
		}
	});
//	colOpts.push({
//		"data" : "sampleOrder-barcode",
//		"title" : "产品批号",
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("saveName", "sampleOrder-barcode");
//		}
//	});
	colOpts.push({
		"data" : "sampleOrder-name",
		"title" : "姓名",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-name");
		}
	});
	colOpts.push({
		"data" : "sampleOrder-crmCustomer-name",
		"title" : "医院",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-crmCustomer-name");
		}
	});
	colOpts.push({
		"data" : "hospitalDepartment",
		"title" : "科室",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospitalDepartment");
		}
	});
	
	colOpts.push({
		"data" : "address",
		"title" : "送达楼层",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "address");
		}
	});
	colOpts.push({
		"data": "reinfusionPlanDate",
		"title":"预计回输日期",
		"createdCell" : function(td) {
			$(td).attr("saveName", "reinfusionPlanDate");
		}
	})
	colOpts.push({
		"data" : "transportDate",
		"title" : "出发时间",
		"width":"150px",
		"className":"newhour",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "transportDate");
			$(td).attr("reinfusionPlanDate", rowData['reinfusionPlanDate']);
		}
	});
	colOpts.push({
		"data" : "transportDate2",
		"title" : "预计到达时间",
		"width":"150px",
		"className":"newhour",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "transportDate2");
			$(td).attr("reinfusionPlanDate", rowData['reinfusionPlanDate']);
		}
	});
	colOpts.push({
		"data" : 'lowTemperature',
		"title" : "最低温度",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "lowTemperature");
		}
	});
	colOpts.push({
		"data" : 'highTemperature',
		"title" : "最高温度",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "highTemperature");
		}
	});
	colOpts.push({
		"data" : 'avgTemperature',
		"title" : "平均温度",
		"className" : "edit",
		"createdCell" : function(td){
			$(td).attr("saveName", "avgTemperature");
		}
	});
	colOpts.push({
        "data":"transportTemperature",
        "title": "温度要求℃",
        "className":"select",
        "name":"|15~25℃|2~8℃",
        "createdCell": function(td) {
			$(td).attr("savename","transportTemperature");
			$(td).attr("selectOpt","|15~25℃|2~8℃");
	     },
	    "render" : function(data, type, full, meta) {
			if (data == "0") {
				return "15~25℃";
			} else if (data == "1") {
				return "2~8℃";
			} else {
				return "";
			}
		}
	
	});
	colOpts.push({
		"data" : 'equimentNumber',
		"title" : "保温箱型号",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "equimentNumber");
		}
	});
	colOpts.push({
		"data" : 'recorder',
		"title" : "温度计型号",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "recorder");
		}
	});
	colOpts.push({
		"data" : 'personFollowCar',
		"title" : "跟车人员",
		"className" : "edit",
		"createdCell" : function(td){
			$(td).attr("saveName", "personFollowCar");
		}
	});
	colOpts.push({
		"data" : 'linkman',
		"title" : "联系人",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "linkman");
		}
	});
	colOpts.push({
		"data" : 'linkmanPhone',
		"title" : "联系人电话",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "linkmanPhone");
		}
	});
	colOpts.push({
		"data" : 'notification',
		"title" : '是否通知',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "notification");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : 'affirm',
		"title" : '是否确认',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "affirm");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	
	colOpts.push({
		"data" : 'sampleOrder-filtrateCode',
		"title" : "筛选号",
//		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-filtrateCode");
		}
	});
	colOpts.push({
		"data" : 'sampleOrder-round',
		"title" : "轮次",
//		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-round");
		}
	});
	colOpts.push({
		"data" : 'sampleOrder-barcode',
		"title" : "产品批号",
//		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-barcode");
		}
	});
	colOpts.push({
		"data" : "numTubes",
		"title" : "管/袋数",
//		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "numTubes");
		},
		"className":"edit"
	});
	
	colOpts.push({
		"data" : 'bloodSampling',
		"title" : '采血物品',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodSampling");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		        "data":"transfusion",
		        "title": "输血器",
		        "className":"select",
		        "name":"|单头|双头|否",
		        "createdCell": function(td) {
					$(td).attr("savename","transfusion");
					$(td).attr("selectOpt","|单头|双头|否");
				 },
				"render" : function(data, type, full, meta) {
					if (data == "0") {
						return "单头";
					} else if (data == "1") {
						return "双头";
					} else if(data =="2"){
						return "否";
					}else{
						return "";
					}
				}
			});
				  
		    colOpts.push({
		        "data" : 'saline',
		        "title" : '氯化钠注射液',
		        "className" : "select",
		        "name" : "|是|否",
		        "createdCell" : function(td) {
				$(td).attr("savename","saline");
				$(td).attr("selectOpt","|是|否");
			        },
			    "render" : function(data, type, full, meta) {
					if (data == "0") {
						return "否";
					} else if (data == "1") {
						return "是";
					} else {
						return "";
					}
				}
			});
				colOpts.push({
					"data" : "oddNumber",
					"title" : "运输单号",
					"className":"edit",
					"createdCell" : function(td,data,rowData) {
						$(td).attr("saveName", "oddNumber");
						$(td).attr("transportOrder-id", rowData['transportOrder-id']);
					}
				});
				
				colOpts.push({
					"data" : 'submitData',
					"title" : "是否提交",
			// "className" : "select",
					"name" : "是" + "|" + "否",
					"createdCell" : function(td) {
						$(td).attr("saveName", "submitData");
						$(td).attr("selectOpt", "是" + "|" + "否");
					},
					"render" : function(data, type, full, meta) {
						if (data == '是') {
							return "是";
						} else if (data == '否') {
							return "否";
						} else {
							return "";
						}
					}
				});
				
				colOpts.push({
					"data" : 'efficacious',
					"title" : "是否有效",
			// "className" : "select",
					"name" : "有效" + "|" + "无效",
					"createdCell" : function(td) {
						$(td).attr("saveName", "efficacious");
						$(td).attr("selectOpt", "有效" + "|" + "无效");
					},
					"render" : function(data, type, full, meta) {
						if (data == '有效') {
							return "有效";
						} else if (data == '无效') {
							return "无效";
						} else {
							return "";
						}
					}
				});
				colOpts.push({
					"data" : "doctor",
					"title" : "医生",
					"className":"edit",
					"createdCell" : function(td) {
						$(td).attr("saveName", "doctor");
					}
				});
				colOpts.push({
					"data" : "doctorTel",
					"title" : "医生电话",
					"className":"edit",
					"createdCell" : function(td) {
						$(td).attr("saveName", "doctorTel");
					}
				});
				
				colOpts.push({
					"data" : 'doctorNotice',
					"title" : '是否通知到医生'
						+ '<img src="/images/required.gif"/>',
					"className" : "select",
					"name" : "|是|否",
					"visible" : false,
					"createdCell" : function(td) {
						$(td).attr("saveName", "doctorNotice");
						$(td).attr("selectOpt", "|是|否");
					},
					"render" : function(data, type, full, meta) {
						if (data == "0") {
							return "否";
						} else if (data == "1") {
							return "是";
						} else {
							return "";
						}
					}
					,
				});
				colOpts.push({
					"data" : 'doctorReply',
					"title" : '医生回复'
						+ '<img src="/images/required.gif"/>',
					"className" : "select",
					"name" : "|是|否",
					"visible" : false,
					"createdCell" : function(td) {
						$(td).attr("saveName", "doctorReply");
						$(td).attr("selectOpt", "|是|否");
					},
					"render" : function(data, type, full, meta) {
						if (data == "0") {
							return "否";
						} else if (data == "1") {
							return "是";
						} else {
							return "";
						}
					}
					,
				});
	
	colOpts.push({
		"data" : 'patientNotice',
		"title" : '是否通知到患者'
			+ '<img src="/images/required.gif"/>',
		"className" : "select",
		"name" : "|是|否",
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "patientNotice");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : 'patientReply',
		"title" : '患者回复'
			+ '<img src="/images/required.gif"/>',
		"className" : "select",
		"name" : "|是|否",
		"visible" : false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "patientReply");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data":"state",
		"title": "生产安排状态"+ '<img src="/images/required.gif"/>',
// "className":"select",
		"name":"|未安排|已安排",
		
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
			$(td).attr("selectOpt", "|未安排|已安排");
	    },
	    "render": function(data, type, full, meta) {
			if(data == "2") {
				return "未安排";
			}if(data == "1") {
				return "已安排";
			}else{
				return "";
			}
		}
	});
	
	colOpts.push({
		"data" : "harvestDate",
		"title" : "收获开始时间",
		"visible":false,
		width:"150px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "harvestDate");
		}
	});
	
	colOpts.push({
		"data" : "endHarvestDate",
		"title" : "收获结束时间",
		"visible":false,
		width:"150px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "endHarvestDate");
		}
	});
	colOpts.push({
		"data" : "cellDate",
		"title" : "出细胞时间",
		"visible":false,
		width:"150px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "cellDate");
		}
	});
	colOpts.push({
		"data" : "sampleCode",
		"title" : "原始样本编号",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleCode");
		}
	});
	colOpts.push({
		"data" : "cellula",
		"title" : "细胞数量",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "cellula");
		}
	});
	
	colOpts.push({
		"data":"shippingNo",
		"title": "运输单号",
		"visible":false,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "shippingNo");
			$(td).attr("transportOrder-id", rowData['transportOrder-id']);
	    },
	    "className":"edit"
	});
//	colOpts.push({
//		"data":"signatory",
//		"title": "签收人",
//		"className":"edit",
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("saveName", "signatory");
//			if(rowData.transportState=="1"){
//				
//			}else{
//				$(td).removeClass("edit");
//			}
//		},
//	});
//	colOpts.push({
//		"data":"signatureDate",
//		"title": "签收日期",
//		"className": "date"
//		"createdCell" : function(td, data, rowData) {
//			$(td).attr("saveName", "signatureDate");
//			if(rowData.transportState=="1"){
//				
//			}else{
//				$(td).removeClass("date");
//			}
//		},
//	});
	colOpts.push({
		"data":"confirmationUser",
		"title": "实际签收人",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "confirmationUser");
			if(rowData.transportState=="1"){
				
			}else{
				$(td).removeClass("edit");
			}
		},
	    "className":"edit"
	});
	colOpts.push({
		"data" : "phoneNumber",
		"title" : "联系方式",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "phoneNumber");
		}
	});
	
	colOpts.push({
		"data" : "transportCompany",
		"title" : "运输公司",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "transportCompany");
		},
	});
	colOpts.push({
		"data":"confirmationDate",
		"title": "实际签收日期",
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "confirmationDate");
			if(rowData.transportState=="1"){
				
			}else{
				$(td).removeClass("date");
			}
	    },
		"className": "date"
	});
	colOpts.push({
		"data":"freight",
		"title": "运输费",
		"createdCell": function(td) {
			$(td).attr("saveName", "freight");
	    },
	    "className":"edit"
	});
	
// colOpts.push({
// "data" : "pronoun",
// "title" : "代次",
// "createdCell" : function(td) {
// $(td).attr("saveName", "pronoun");
// }
// });
	
	
	
	
	
	/*
	 * colOpts.push({ "data" : "dicSampleType-id", "title" : "样本类型ID",
	 * "createdCell" : function(td) { $(td).attr("saveName",
	 * "dicSampleType-id"); }, "visible" : false }); colOpts.push({ "data" :
	 * "dicSampleType-name", "title" : "样本类型", "createdCell" : function(td) {
	 * $(td).attr("saveName", "dicSampleType-name"); } });
	 */
	

	colOpts.push({
		"data" : 'transportState',
		"title" : '是否运输交接'
			+ '<img src="/images/required.gif"/>',
// "className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "transportState");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	
	colOpts.push({
		"data" : "hospital",
		"title" : "医院",
		"className":"edit",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospital");
		}
	});

	
	

	colOpts.push({
		"data" : "hospitalTel",
		"title" : "医院电话",
		"className":"edit",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "hospitalTel");
		}
	});
	
	
	colOpts.push({
		"data":"note",
		"title": "备注",
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
	    "className":"edit"
	});
	
	colOpts.push({ 
        "title": "上传附件",
        "width": "100px",
        "data": null,
        "createdCell": function(td, data) {},
        "render": function() {
            return '<input type="button" value="上传附件" onClick="javascript:fileUp3(this);" >' +
                '<input type="button" value="查看附件" onClick="javascript:fileView3(this);">'
		}
		       
	});
		   
	

	
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
		tbarOpts.push({
			text : biolims.common.delSelected,
			action : function() {
				/*
				 * removeChecked($("#transportOrderItem"),
				 * "/tra/transport/transportOrder/delTransportOrderCell.action");
				 */
				removeData($("#transportOrderItem"));
			}
		});
		tbarOpts.push({
			text: "查看质检结果",
			action:function(){
				showQualityTest1();
			}
		});
		
		tbarOpts.push({
			text: "运输扫码",
			action:function(){
				xianshisaomiao();
			}
		});

		
		tbarOpts.push({
			text: "提交生产安排",
			action:function(){
				tijiao();
			}
				
			});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#transportOrderItem"))
			}
		});

//		tbarOpts.push({
//			text: "选择运输公司",
//			action:function(){
//				showexpressCompany();
//
//			}
//		});
	}
	
	var options = table(true, id,
			'/tra/transport/transportOrder/showTransportOrderItemListJson.action', colOpts, tbarOpts)
	transportOrderItemTable = renderData($("#transportOrderItem"), options);
	transportOrderItemTable.on('draw', function() {
		oldTransportOrderItemChangelog = transportOrderItemTable.ajax.json();

	});
});

//运输方式
function yunshufs() {
	$("#form1").data("changed",true); 
	top.layer.open({
		title : biolims.order.ysfs,
		type : 2,
		area : [ "650px", "400px" ],
		btn : biolims.common.selected,
		content : [
				window.ctx + "/dic/type/dicTypeSelectTable.action?flag=ysfs",
				'' ],
		yes : function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addDicTypeTable .chosed").children("td")
					.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe")
					.contents().find("#addDicTypeTable .chosed").children("td")
					.eq(0).text();
			top.layer.close(index)
			$("#transportOrder_ysfs_id").val(id)
			$("#transportOrder_ysfs_name").val(name)
//			if($("#transportOrder_ysfs_name").val()=="常温"){
//				$("#transportOrder_temperature").val ("15-25℃")
//			}else if($("#sampleReceive_ysfs_name").val()=="蓝冰"){
//				$("#transportOrder_temperature").val ("2-8℃");
//			}else{
//				$("#transportOrder_temperature").val ("");
//			}
		},
	})
}


//判断出发时间是否填写
function tiaojiaopanduan(ele) {
	
	var ele = $("#transportOrderItem");
	 var trs = ele.find("tbody").children(".selected");
	 var trs1 = ele.find("tbody").children(".editagain");
	 var flag = true;
	 
	 if(trs1.length>0){
			top.layer.msg("保存后提交！");
			flag =  false;
		}
	 trs.each(function(i, val) {
	  var json = {};
	  var tds = $(val).children("td");
	  json["id"] = $(tds[0]).find("input").val();
	  for (var j = 1; j < tds.length; j++) {
	   var k = $(tds[j]).attr("savename");
	   json[k] = $(tds[j]).text();
	   if(k == "transportDate") {
	    if (!json[k]) {
	     console.log(json[k]);
	     flag = false;
	     top.layer.msg("出发时间未填写！");
	     break;
	    }
	   }
	  }
	 });
	 return flag;
	}

//提交数据
function tijiao() {
	
	var dataT = true;
	dataT = tiaojiaopanduan(ele);
	
	
	
	
	if(dataT){
		if($("#submitData").val()=="是"){
			top.layer.msg("已提交数据，不需再提交！");
			return false;
		}else{
			var ele = $("#transportOrderItem");
			var trs = ele.find("tbody").children(".editagain");
			if(trs.length>1){
				top.layer.msg("请选择数据！");
				return false;
			}else{
				var rows = $("#transportOrderItem .selected");
				var length = rows.length;
				if(!length) {
					top.layer.msg(biolims.common.pleaseSelect);
					return false;
				}
				var data = [];
				
				rows.each(function(i, val) {
					var tds = $(val).children("td");
					data.push($(tds[0]).find("input").val());
				});
				$.ajax({
					type : 'post',
					url : '/tra/transport/transportOrder/submitFunction.action',
					data : {
						id : data
					},
					success : function(datas) {
						var data = JSON.parse(datas);
						if (data.success) {
							if(data.panduan){
								top.layer.msg("提交成功");
								transportOrderItemTable.ajax.reload();
							}else{
								top.layer.msg("存在提交过样本，请重新选择");
							}
							
						} else {
							top.layer.msg("提交失败");
						};
					}
				});
			}
		}
	}
	
}

function xianshisaomiao(){
	$("#sampleReceiveModal").modal("show");
	$('#sampleReceiveModal').on('shown.bs.modal', function () {
		    //初始化模态框高度为 屏幕高度/2
		    var modalHeight=$(window).height() / 2.5;
		    
		    //将上面的高度设置到 modal-dialog中的margin-top属性中
		    $(this).find('.modal-dialog').css({
		        'margin-top': modalHeight
		    });
		    })
	// 选择临床样本或者科研样本
	$("#modal-body .col-xs-12").hide();
	$("#modal-body #scancode").show();
	scanCode();
}
// 扫码
function scanCode() {
	$("#scanInput").keypress(function(e) {
		var e = e || window.event;
		if (e.keyCode == "13") {
			var sampleCode = this.value;
			if (sampleCode) {
				$.ajax({
					type : "post",
					async : false,
					data : {
						sampleCode : sampleCode,
					},
					url : ctx + "/tra/transport/transportOrder/reinfusionTongguo.action",
					success : function(data2) {
						var data3 = JSON.parse(data2);
						if(data3.success){
							if(data3.sfcz){
								$.ajax({
									type : "post",
									async : false,
									data : {
										id : $("#transportOrder_id").val(),
										sampleCode : sampleCode,
									},
									url : ctx + "/tra/transport/transportOrder/scanCodeInsertpanduan.action",
									success : function(data2) {
										var data3 = JSON.parse(data2);
										if(data3.success){
											if(data3.sfcz){
												if(data3.sfjj){
													top.layer.msg("样本已经交接过！");
												}else{
													top.layer.msg("样本交接成功！");
													$("#scanInput").val("");
													transportOrderItemTable.ajax.reload();
												}
											}else{
												top.layer.msg("样本不在该计划中！");
											};
										}else{
											top.layer.msg("样本查询错误！");
										}
									}
								});
							}else{
								top.layer.msg("该样本放行审核没通过！");
							};
						}else{
							top.layer.msg("查询放行审核数据错误！");
						}
					}
				});
			};
		};
	});
}
function removeData(ele){
 var flag=true;
	var ids = [];
	var rows = ele.find("tbody .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	rows.each(function(i,j,k){
		var submitData=$(this).find("td[savename='submitData']").text();
		if(submitData=="是"){
			top.layer.msg("存在已提交到生产安排的数据不可以删除！");
			flag=false;
			return;
		}
	})
	if(flag){
	
	top.layer.confirm(biolims.common.confirm2Del + length + biolims.common.record,
			{
		icon: 3,
		title: biolims.common.prompt,
		btn:biolims.common.selected
	},function(index) {
		top.layer.close(index);
		rows.each(function(i, val) {
			var id = $(val).find("input[type=checkbox]").val();
			if(id) {
				ids.push(id);
			} else {
				$(val).remove();
			}
		});
		if(ids.length) {
			$.ajax({
				type: "post",
				url: ctx + "/tra/transport/transportOrder/delTransportOrderCell.action",
				data: {
					ids: ids
				},
				success: function(data) {
					var data = JSON.parse(data);
					if(data.success) {
						top.layer.msg(biolims.common.deleteSuccess);
						transportOrderTempTable.ajax.reload();
						transportOrderItemTable.settings()[0].ajax.data ={id: $("#transportOrder_id").val()};
						transportOrderItemTable.ajax.reload();
					}
				}
			});
		}
	});
	
	}
}

// 获得保存时的json数据
function saveTransportOrderItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			json[k] = $(tds[j]).text();
			
			if(k == "code") {
				json["tempId"] = $(tds[j]).attr("tempId");
				json["sampleOrder-id"] = $(tds[j]).attr("sampleOrder-id");
			}
			
			//是否通知
			if(k == "notification") {
				var notification = $(tds[j]).text();
				if(notification == "否") {
					json[k] = "0";
				} else if(notification == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			//是否确认
			if(k == "affirm") {
				var affirm = $(tds[j]).text();
				if(affirm == "否") {
					json[k] = "0";
				} else if(affirm == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			//采血物品
			
			if(k == "bloodSampling") {
				var bloodSampling = $(tds[j]).text();
				if(bloodSampling == "否") {
					json[k] = "0";
				} else if(bloodSampling == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			if(k == "doctorNotice") {
				var doctorNotice = $(tds[j]).text();
				if(doctorNotice == "否") {
					json[k] = "0";
				} else if(doctorNotice == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			//运输温度
			if(k == "transportTemperature") {
				var transportTemperature = $(tds[j]).text();
				if(transportTemperature == "15~25℃") {
					json[k] = "0";
				} else if(transportTemperature == "2~8℃") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			//输血器
			if(k == "transfusion") {
				var transfusion = $(tds[j]).text();
				if(transfusion == "单头") {
					json[k] = "0";
				} else if(transfusion == "双头") {
					json[k] = "1";
				}else if(transfusion == "否"){
					json[k] = "2";
				}else{
					json[k] = "";
				}
				continue;
			}
			//生理盐水
			if(k == "saline") {
				var saline = $(tds[j]).text();
				if(saline == "否") {
					json[k] = "0";
				} else if(saline == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			if(k == "doctorReply") {
				var doctorReply = $(tds[j]).text();
				if(doctorReply == "否") {
					json[k] = "0";
				} else if(doctorReply == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			if(k == "patientNotice") {
				var patientNotice = $(tds[j]).text();
				if(patientNotice == "否") {
					json[k] = "0";
				} else if(patientNotice == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			if(k == "patientReply") {
				var patientReply = $(tds[j]).text();
				if(patientReply == "否") {
					json[k] = "0";
				} else if(patientReply == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			if(k == "state") {
				var result = $(tds[j]).text();
				if(result == "未安排") {
					json[k] = "2";
				}else if(result == "已安排") {
					json[k] = "1";
				} else {
					json[k] = "";
				}
				continue;
			}
			
			
			if(k == "transportState") {
				var transportState = $(tds[j]).text();
				if(transportState == "否") {
					json[k] = "0";
				}else if(transportState == "是") {
					json[k] = "1";
				} else {
					json[k] = "";
				}
				continue;
			}
			
			if(k == "planState") {
				var planState = $(tds[j]).text();
				if(planState == "否") {
					json[k] = "0";
				}else if(transportState == "是") {
					json[k] = "1";
				} else {
					json[k] = "";
				}
				continue;
			}
			
			if(k == "transportDate") {
				var transportDate = $(tds[j]).text();
				var reinfusionPlanDate = $(tds[j]).attr("reinfusionPlanDate");
				if(transportDate!=""){
					transportDate = reinfusionPlanDate+" "+transportDate;
					json[k] = transportDate;
				}else{
					json[k] = reinfusionPlanDate;
				}
				continue;
			}
			
			if(k == "transportDate2") {
				var transportDate2 = $(tds[j]).text();
				var reinfusionPlanDate = $(tds[j]).attr("reinfusionPlanDate");
				if(transportDate!=""){
					transportDate2 = reinfusionPlanDate+" "+transportDate2;
					json[k] = transportDate2;
				}else{
					json[k] = reinfusionPlanDate;
				}
				continue;
			}
			
			
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
// 日志
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '运输明细编号为"' + v.code + '":';
		oldTransportOrderItemChangelog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function showQualityTest1(){
	var rows=$("#transportOrderItem .selected");
	if(rows.length!=1){
		top.layer.msg("请选择一条数据！");
		return false;
	}
	var code=rows.find("td[savename='code']").text();
	top.layer.open({
		title: "质检结果表",
		type: 2,
		area: [document.body.clientWidth-50,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content: [window.ctx +"/tra/transport/transportOrder/showQualityTestTable.action?code="+code,''],
		yes: function(index, layer) {
			top.layer.close(index)	
		},
	});
}
//选择运输公司
function showexpressCompany() {
//	var rows = $("#transportOrderItem .selected");
//	var length = rows.length;
//	if (!length) {
//		top.layer.msg(biolims.common.pleaseSelectData);
//		return false;
//	}
	top.layer
			.open({
				title : biolims.goods.selectExpress,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.selected,
				content : [
						window.ctx
								+ "/system/express/expressCompany/expressCompanySelectTable.action",
						'' ],
				yes : function(index, layer) {
					var name = $('.layui-layer-iframe', parent.document).find(
							"iframe").contents().find(
							"#addExpressCompanyTable .chosed").children("td")
							.eq(1).text();
					var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addExpressCompanyTable .chosed").children("td")
							.eq(0).text();
						top.layer.close(index)
						
				$("#transportOrder_transCompany").val(id);
				$("#transportOrder_transCompany_name").val(name)
					
					
//					var rows=$("#transportOrderItem");
//					rows.addClass("editagain");
//					rows.find("td[savename='transportCompany']").text(name);
					top.layer.close(index)
				},
			})
}
//上传附件
function fileUp3(that) {
    var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
$("#tableFileLoad").html(str);
    var id = $(that).parents("tr").children("td").eq(0).find("input").val();
    if(id==""){
        top.layer.msg("请先保存数据，再上传附件！");
        return;
}
    

$("#uploadFileValx").fileinput({
        uploadUrl: ctx + "/attachmentUpload?useType=misc&modelType=transportOrderItem&contentId=" + id,
        uploadAsync: true,
        language: 'zh',
        showPreview: true,
        enctype: 'multipart/form-data',
        elErrorContainer: '#kartik-file-errors',
});

    //  if($("#uploadFile .fileinput-remove").length){
    //      $("#uploadFile .fileinput-remove").eq(0).click();
    //  }
$("#uploadFilex").modal("show");
    //var cfileInput = fileInput('1','samplingTaskItem', id);
}
//附件查看
function fileView3(that) {
    var id = $(that).parents("tr").children("td").eq(0).find("input").val();
    top.layer.open({
        title: biolims.common.attachment,
        type: 2,
        skin: 'layui-layer-lan',
        area: [600,400],
        content: window.ctx + "/operfile/initFileList.action?flag=misc&modelType=transportOrderItem&id=" + id,
        cancel: function(index, layero) {
            top.layer.close(index)
		}
	});
}