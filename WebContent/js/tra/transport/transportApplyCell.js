﻿var transportApplyCellGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'materailsMain-id',
		type:"string"
	});
	    fields.push({
		name:'materailsMain-name',
		type:"string"
	});
	    fields.push({
		name:'materailsMainCell-id',
		type:"string"
	});
	    fields.push({
		name:'materailsMainCell-name',
		type:"string"
	});
	   fields.push({
		name:'materailsMainCell-cloneCode',
		type:"string"
	});
	   fields.push({
		name:'materailsMainCell-generation',
		type:"string"
	});
	   fields.push({
		name:'materailsMainCell-background',
		type:"string"
	});
	   fields.push({
		name:'materailsMainCell-num',
		type:"string"
	});
	   fields.push({
		name:'num',
		type:"string"
	});
	   fields.push({
		name:'materailsMainCell-strain',
		type:"string"
	});
	   fields.push({
		name:'materailsMainCell-frozenLocation',
		type:"string"
	});
	   fields.push({
		name:'way-id',
		type:"string"
	});
	   fields.push({
	   name:'way-name',
	   type:"string"
   });
	    fields.push({
		name:'transportApply-id',
		type:"string"
	});
	    fields.push({
		name:'transportApply-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'materailsMain-id',
		hidden : true,
		header:'主数据ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'materailsMain-name',
		hidden : false,
		header:'主数据名称',
		width:15*10
	});
	cm.push({
		dataIndex:'materailsMainCell-id',
		hidden : true,
		header:'细胞ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'materailsMainCell-name',
		hidden : false,
		header:'细胞名称',
		width:15*10
	});

	cm.push({
		dataIndex:'materailsMainCell-cloneCode',
		hidden : false,
		header:'克隆号',
		width:15*10
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	cm.push({
		dataIndex:'materailsMainCell-generation',
		hidden : false,
		header:'代次',
		width:15*10
	});
	cm.push({
		dataIndex:'materailsMainCell-strain-name',
		hidden : false,
		header:'品系',
		width:20*6
	});
	cm.push({
		dataIndex:'materailsMainCell-background',
		hidden : true,
		header:'细胞背景',
		width:15*10
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
	});
	
	cm.push({
		dataIndex:'materailsMainCell-num',
		hidden : true,
		header:'在库数量',
		width:20*6
	});
//	cm.push({
//		dataIndex:'materailsMainCell-frozenLocation',
//		hidden : true,
//		header:'位置',
//		width:20*6
//	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:'数量(管) *',
		width:20*6,
		
		editor : new Ext.form.NumberField({
			allowDecimals: false, // 不允许小数点 
			allowNegative: false, // 不允许负数 
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'way-id',
		hidden : true,
		header:'运输方式ID',
		width:40*6
	});
	cm.push({
		dataIndex:'way-name',
		hidden : false,
		header:'运输方式',
		width:40*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportApply-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportApply-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/transport/transportApply/showTransportApplyCellListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="产品运输(细胞)信息";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/tra/transport/transportApply/delTransportApplyCell.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	
	if($("#transportApply_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
		 } else{
				opts.tbar.push({
						text : '选择细胞',
						handler : selectmaterailsMainFun
					});
			    
				/*opts.tbar.push({
						text : '选择相关主表',
						handler : selecttransportApplyFun
					});*/
				opts.tbar.push({
						text : '选择运输方式',
						handler : selectYsfs
				});
				opts.tbar.push({
					text : '显示可编辑列',
					handler : null
				});
				opts.tbar.push({
					text : '取消选中',
					handler : null
				});
				opts.tbar.push({
					text : '填加明细',
					handler : null
				});
				opts.tbar.push({
					text : "批量上传（CSV文件）",
					handler : function() {
						var options = {};
						options.width = 350;
						options.height = 200;
						loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
							"确定":function(){
								goInExcelcsv();
								$(this).dialog("close");
							}
						},true,options);
					}
				});
		 }

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = transportApplyCellGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							transportApplyCellGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	transportApplyCellGrid=gridEditTable("transportApplyCelldiv",cols,loadParam,opts);
	$("#transportApplyCelldiv").data("transportApplyCellGrid", transportApplyCellGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
var d="";
var name=""; 
function setMaterailsMainFun(rec){	
	d = rec.get('id');
	name= rec.get('name');
	var selectRecord = materailsMainDialogGrid.getSelectionModel();
	var selRecord = materailsMainCellDialogGrid.store;
	if (selectRecord.getSelections().length > 0) {
		if (selRecord && selRecord.getCount() > 0) {
			for(var i=0;i<selectRecord.getSelections().length;i++){
				var newv = selectRecord.getSelections()[i].get("id");
				for(var j=0;j<selRecord.getCount();j++){
					var oldv = selRecord.getAt(j).get("materailsMainId");
					if( oldv == newv ){
						message("有重复的数据，请重新选择！");
						return ;
					}
				}
			}
		}
	ajax("post", "/tra/transport/transportApply/showMainCellList.action", {
		mainId : d
		}, function(data) {
			if (data.success) {
				var ob = materailsMainCellDialogGrid.getStore().recordType;
				materailsMainCellDialogGrid.stopEditing();
				$.each(data.data, function(i, obj) {	
					var p = new ob({});
					p.isNew = true;

					p.set("materailsMainId", d);				
					p.set("materailsMainName", name);
					p.set("cellName", obj.name);
					p.set("cellId", obj.id);
					p.set("isMy", obj.isMy);
					p.set("generation", obj.generation);
					p.set("size", obj.size);
					p.set("num", obj.num);
					p.set("strain-name", obj.strain);
					p.set("frozenLocation", obj.strain);
					p.set("note", obj.note);
					p.set("frozenUser-name", obj.frozenUserName);
					p.set("frozenCellsNum", obj.frozenCellsNum);
					
					materailsMainCellDialogGrid.getStore().add(p);
					
				});
				materailsMainCellDialogGrid.startEditing(0, 0);
			} else {
				message("获取明细数据时发生错误！");
			}
		}, null);
	}
}
//添加到明细表
//获取选中的数据ID
//选择动物主数据
function selectmaterailsMainFun(){
			var options = {};
			options.width = 800;
			options.height = 660;
			var url="/tra/materials/materailsMain/materailsMainSelect.action?typeId="+0;//?typeId="+$("#materailsReturn_type").val();
			loadDialogPage(null, "选择明细", url, {
				 "确定": function() {	
					 addCell();
					 options.close();
				}
			}, true, options);
		}
function addCell(){
	var selectRecord = materailsMainCellDialogGrid.getSelectionModel();
	var selRecord = transportApplyCellGrid.store;
	if (selectRecord.getSelections().length > 0) {	
		$.each(selectRecord.getSelections(), function(i, obj) {
			var isRe=false;
			for(var j=0;j<selRecord.getCount();j++){
				var oldv = selRecord.getAt(j).get("materailsMainCell-id");
				if( oldv == obj.get("cellId")){
					isRe=true;
					break;
				}
			}
			if(!isRe){
			var ob = transportApplyCellGrid.getStore().recordType;
			transportApplyCellGrid.stopEditing();
			var p = new ob({});
			p.isNew = true;	
			p.set("materailsMain-id", obj.get("materailsMainId"));
			p.set("materailsMain-name", obj.get("materailsMainName"));
			p.set("materailsMainCell-id", obj.get("cellId"));
		    p.set("materailsMainCell-name", obj.get("cellName"));
		    p.set("materailsMainCell-strain-name", obj.get("strain-name"));
			//p.set("isMy", obj.get("materailsMainCell-isMy"));
			p.set("materailsMainCell-generation", obj.get(""));
			p.set("materailsMainCell-background", obj.get(""));
			p.set("materailsMainCell-cloneCode", obj.get(""));
			//p.set("size", obj.get("materailsMainCell-size"));
			//p.set("frozenUser-name", obj.get("materailsMainCell-frozenUser-name"));
			//p.set("frozenCellsNum", obj.get("materailsMainCell-frozenCellsNum"));
			//p.set("num", obj.get("materailsBorrowCell-num"));
			
			transportApplyCellGrid.getStore().add(p);
			}
		});
		
		transportApplyCellGrid.startEditing(0, 0);
	}
}
//选择运输方式
function selectYsfs(){
	var win = Ext.getCmp('selectmethod');
	if (win) {win.close();}
	var selectmethod= new Ext.Window({
	id:'selectmethod',modal:true,title:'选择运输方式',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/dic/type/dicTypeSelect.action?flag=ystj' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectmethod.close(); }  }]  });     selectmethod.show(); }
	function setystj(id,name){
		var gridGrid =transportApplyCellGrid;
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('way-id',id);
			obj.set('way-name',name);
		});
		var win = Ext.getCmp('selectmethod')
		if(win){
			win.close();
		}
	}
/*function selectmaterailsMainFun(){
	var win = Ext.getCmp('selectmaterailsMain');
	if (win) {win.close();}
	var selectmaterailsMain= new Ext.Window({
	id:'selectmaterailsMain',modal:true,title:'选择细胞',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/MaterailsMainSelect.action?flag=materailsMain' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectmaterailsMain.close(); }  }]  });     selectmaterailsMain.show(); }
	function setmaterailsMain(id,name){
		var gridGrid = $("#transportApplyCelldiv").data("transportApplyCellGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('materailsMain-id',id);
			obj.set('materailsMain-name',name);
		});
		var win = Ext.getCmp('selectmaterailsMain')
		if(win){
			win.close();
		}
	}
	
function selecttransportApplyFun(){
	var win = Ext.getCmp('selecttransportApply');
	if (win) {win.close();}
	var selecttransportApply= new Ext.Window({
	id:'selecttransportApply',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TransportApplySelect.action?flag=transportApply' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selecttransportApply.close(); }  }]  });     selecttransportApply.show(); }
	function settransportApply(id,name){
		var gridGrid = $("#transportApplyCelldiv").data("transportApplyCellGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('transportApply-id',id);
			obj.set('transportApply-name',name);
		});
		var win = Ext.getCmp('selecttransportApply')
		if(win){
			win.close();
		}
	}*/
	
