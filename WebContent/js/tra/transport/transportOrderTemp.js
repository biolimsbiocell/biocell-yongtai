﻿//生产细胞运输上
var transportOrderTempTable;
$(function() {
	var colOpts=[]
	colOpts.push({
		"data": "id",
		"visible": false,
		"title": biolims.common.id,
	})
/*	colOpts.push({
		"data": "code",
		"title": biolims.common.code,
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})*/
	/*colOpts.push({
		"data": "sampleCode",
		"title": biolims.common.sampleCode,
	})*/
	colOpts.push({
		"data": "productName",
		"title": biolims.master.product,
	})
//	colOpts.push({
//		"data": "pronoun",
//		"title": biolims.common.pronoun,
//	})
//	colOpts.push({
//		"data": "orderCode",
//		"title": biolims.common.orderCode,
//	})
//	colOpts.push({
//		"data": "sampleType",
//		"title": biolims.common.sampleType,
//	})
	colOpts.push({
		"data" : "sampleOrder-name",
		"title" : "姓名",
	});
	colOpts.push({
		"data": "sampleOrder-crmCustomer-name",
		"title": "医院",
	})
	colOpts.push({
		"data": "sampleOrder-inspectionDepartment-name",
		"title": "科室",
	})
	colOpts.push({
		"data": "sampleOrder-attendingDoctor",
		"title": "医生",
	})
	colOpts.push({
		"data": "sampleOrder-attendingDoctorPhone",
		"title": "医生电话",
	})
	colOpts.push({
		"data": "note",
		"title": "备注",
	})
	colOpts.push({
		"data": "code",
		"title": "细胞编号",
		"createdCell": function(td) {
			$(td).attr("saveName", "code");
		}
	})
	colOpts.push({
		"data": "sampleOrder-barcode",
		"title": "产品批号",
		"createdCell": function(td) {
			$(td).attr("saveName", "sampleOrder-barcode");
		}
	})
	/*colOpts.push({
		"data": "sampleOrder-medicalInstitutionsPhone",
		"title": "医院电话",
	})*/
	colOpts.push({
		"data": "sampleInfo-id",
		"visible":false,
		"title": biolims.sample.sampleMasterData,
	})
	colOpts.push({
		"data": "productId",
		"visible":false,
		"title":"产品编号",
	})
	colOpts.push({
		"data": "reinfusionPlanDate",
		"title":"预计回输日期",
	})
	
//	colOpts.push({
//		"data": "scopeName",
//		"title": biolims.purchase.costCenter,
//		"createdCell": function(td,data,rowdata) {
//			var typeValue=rowdata["sampleInfo-changeType"];
//			if(typeValue=="0"){
//				$(td).parent("tr").children("td").eq(2).css('background-color','EAB9A8');
//			}else if(typeValue=="1"){
//				$(td).parent("tr").children("td").eq(2).css('background-color','30B573');
//			}else if(typeValue=="2"){
//				$(td).parent("tr").children("td").eq(2).css('background-color','8FAAC6');
//			}
//		}
//	});
//	colOpts.push({
//		"data": "sampleInfo-changeType",
//		"title": biolims.purchase.costCenter,
//		"visible":false
//	});
//	colOpts.push({
//		"data": "planState",
//		"title": "放行审核是否通过",
////		"className": "select",
//		"name": "是|否",
//		"createdCell": function(td) {
//			$(td).attr("saveName", "planState");
//			$(td).attr("selectOpt", "是|否");
//		},
//		"render": function(data, type, full, meta) {
//			if(data == "0") {
//				return "否";
//			}
//			if(data == "1") {
//				return "是";
//			} else {
//				return '';
//			}
//		}
//	});
	var tbarOpts=[];
	/*tbarOpts.push({
		text: '<i class="fa fa-exchange"></i> ' + biolims.common.checkCode,
		action: function() {
			checkCode();
		}
	});*/
	tbarOpts.push({
		text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
		action:function(){
			search();
		}
	});
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action:function(){
			var rows = $("#transportOrderTemp .selected");
			if(!rows.length){
				top.layer.msg("请选择数据!");
				return false;
			}
			var sampleId = [];
			var panduan = false;
			$("#transportOrderTemp .selected").each(function(i, val) {
				sampleId.push($(val).children("td").eq(0).find("input").val());
//				if($(val).find("td[savename='planState']").text()!="是"){
//					panduan = true;
//				}
			});
			if(panduan){
				top.layer.msg("选择数据中存在放行审核未通过数据，请重新选择!");
			}else{
				var mainJson = JSON.stringify($("#form1").serializeObject());
				$.ajax({
					type: 'post',
					url: '/tra/transport/transportOrder/addTransportOrderMakeUp.action',
					data: {
						main: mainJson,
						ids: sampleId
					},
					success: function(data) {
						var data = JSON.parse(data);
						if(data.success) {
							var param = {
								id: data.data
							};
							$("#transportOrder_id").val(data.data);
							transportOrderTempTable.ajax.reload();
							transportOrderItemTable.settings()[0].ajax.data = param;
							transportOrderItemTable.ajax.reload();
						} else {
							top.layer.msg(biolims.common.addToDetailFailed);
						};
					}
				});
			}
			
		}
	});
	tbarOpts.push({
		text: "查看质检结果",
		action:function(){
			showQualityTest();
		}
	});
	var options = table(true,null,"/tra/transport/transportOrder/showTransportOrderTempTableJson.action",colOpts, tbarOpts)
	//渲染样本的table
	transportOrderTempTable=renderData($("#transportOrderTemp"),options);
})

$.fn.serializeObject = function(){
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};
//弹框模糊查询参数
function searchOptions() {
	return [{
		"txt" : biolims.common.sampleCode,
		"type" : "input",
		"searchName" : "sampleCode",
	},{
		"txt" : biolims.common.code,
		"type" : "input",
		"searchName" : "code",
	}, {
		"txt":biolims.common.projectId,
		"type": "input",
		"searchName": "sampleInfo-project-id",
	}, {
		"txt":"项目",
		"type": "input",
		"searchName": "sampleInfo-project-productName",
	}, {
		"txt":biolims.common.orderCode,
		"type": "input",
		"searchName": "orderCode",
	}, {
		"txt":biolims.common.sampleType,
		"type": "input",
		"searchName": "sampleType",
	}, {
		"txt":biolims.common.productName,
		"type": "input",
		"searchName": "productName",
	}, {
		"txt":"回输时间",
		"type": "dataTime",
		"searchName": "reinfusionPlanDate",
	},{
		"txt":"医院",
		"type": "input",
		"searchName": "sampleOrder-crmCustomer-name",
	},{
		"txt":"医生",
		"type": "input",
		"searchName": "sampleOrder-attendingDoctor",
	},
	 {
		"type" : "table",
		"table" : transportOrderTempTable,
	},];
}
//核对编码
function checkCode() {
	top.layer.open({
		title: biolims.common.checkCode,
		type: 1,
		area: ["30%", "60%"],
		btn: biolims.common.selected,
		btnAlign: 'c',
		content: '<textarea id="checkCode" class="form-control" style="width: 99%;height: 99%;"></textarea>',
		yes: function(index, layero) {
			var arr = $("#checkCode").val().split("\n");
			cellPassageAllotTab.settings()[0].ajax.data = {"codes":arr};
			cellPassageAllotTab.ajax.reload();
			top.layer.close(index);
		},
	});
}
function showQualityTest(){
	var rows=$("#transportOrderTemp .selected");
	if(rows.length!=1){
		top.layer.msg("请选择一条数据！");
		return false;
	}
	var code=rows.find("td[savename='sampleOrder-barcode']").text();
	top.layer.open({
		title: "质检结果表",
		type: 2,
		area: [document.body.clientWidth-50,document.body.clientHeight-100],
		btn: biolims.common.selected,
		content: [window.ctx +"/tra/transport/transportOrder/showQualityTestTable.action?code="+code,''],
		yes: function(index, layer) {
			top.layer.close(index)	
		},
	});
}