var TransportSampleItemTab;
var TransportSampleItemChangeLog;
$(function() {
	// 加载子表
	var id = $("#transportOrder_id").val();
	var tbarOpts = [];
	var colOpts = [];
	colOpts.push({
		"data" : "id",
		"title" : "ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "sequenceNumber",
		"title" : "序号",
		"className" : "edit",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "sequenceNumber");
		}
	});
	colOpts.push({
		"data" : "orderNo",
		"title" : biolims.common.orderCode,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "orderNo");
			$(td).attr("sampleOrder-id", rowData['sampleOrder-id']);
		}
	});
	colOpts.push({
		"data" : "name",
		"title" : biolims.common.name,
		"createdCell" : function(td) {
			$(td).attr("saveName", "name");
		},
		"className" : "edit",
		"visible" : false,
	});
	colOpts.push({
		"data" : "createUser-id",
		"title" : "创建人ID",
		"createdCell" : function(td) {
			$(td).attr("saveName", "createUser-id");
		},
		"visible" : false
	});
	colOpts.push({
		"data" : "createUser-name",
		"title" : biolims.sample.createUserName,
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUserr-id']);
		},
		"visible" : false,
	});
	colOpts.push({
		"data" : "createDate",
		"title" : biolims.sample.createDate,
		width : "100px",
		"createdCell" : function(td) {
			$(td).attr("saveName", "createDate");
		},
		"className" : "date",
		"visible" : false,
	});

	colOpts.push({
		"data" : "sampleOrder-name",
		"title" : "姓名",
		"visible" : false,
	});
	colOpts.push({
		"data" : "sampleOrder-crmCustomer-name",
		"title" : "医院",
//		"visible":false,
	})
	colOpts.push({
		"data" : "sampleOrder-inspectionDepartment-name",
		"title" : "科室",
	})
	colOpts.push({
		"data" : 'address',
		"title" : "送达楼层",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "address");
		}
	})
	colOpts.push({
		"data" : "planTime",
		"title" : "预计到达时间",
		"createdCell" : function(td) {
			$(td).attr("saveName", "planTime");
		},
		"className" : "newhour"
	});
	colOpts.push({
		        "data":"transportTemperature",
		        "title": "温度要求℃",
		        "className":"select",
		        "name":"|15~25℃|2~8℃",
		        "createdCell": function(td) {
					$(td).attr("savename","transportTemperature");
					$(td).attr("selectOpt","|15~25℃|2~8℃");
			     },
			    "render" : function(data, type, full, meta) {
					if (data == "0") {
						return "15~25℃";
					} else if (data == "1") {
						return "2~8℃";
					} else {
						return "";
					}
				}
			
			});

	colOpts.push({
		"data" : 'linkman',
		"title" : "联系人",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "linkman");
		}
	});
	colOpts.push({
		"data" : 'linkmanPhone',
		"title" : "联系人电话",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "linkmanPhone");
		}
	});
	colOpts.push({
		"data" : 'notification',
		"title" : '是否通知',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "notification");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : 'affirm',
		"title" : '是否确认',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "affirm");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
		"data" : 'sampleOrder-filtrateCode',
		"title" : "筛选号",
//		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-filtrateCode");
		}
	});
	colOpts.push({
		"data" : 'sampleOrder-round',
		"title" : "轮次",
//		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-round");
		}
	});
	colOpts.push({
	"data" : "sampleOrder-heparinTube",
	"title" : "管/袋数",
//	"className" : "edit"
});
	colOpts.push({
		"data" : "sampleOrder-lymphoidCellSeries",
		"title" : "淋巴细胞绝对值",
//		"className" : "edit"
	});
	colOpts.push({
		"data" : 'bloodSampling',
		"title" : '采血物品',
		"className" : "select",
		"name" : "|是|否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodSampling");
			$(td).attr("selectOpt", "|是|否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
		,
	});
	colOpts.push({
	        "data":"transfusion",
	        "title": "输血器",
	        "className":"select",
	        "name":"|单头|双头|否",
	        "createdCell": function(td) {
				$(td).attr("savename","transfusion");
				$(td).attr("selectOpt","|单头|双头|否");
			 },
			"render" : function(data, type, full, meta) {
				if (data == "0") {
					return "单头";
				} else if (data == "1") {
					return "双头";
				} else if(data =="2"){
					return "否";
				}else{
					return "";
				}
			}
		});
				  
    	colOpts.push({
	        "data" : 'saline',
	        "title" : '氯化钠注射液',
	        "className" : "select",
	        "name" : "|是|否",
	        "createdCell" : function(td) {
			$(td).attr("savename","saline");
			$(td).attr("selectOpt","|是|否");
		        },
	    	"render" : function(data, type, full, meta) {
			if (data == "0") {
				return "否";
			} else if (data == "1") {
				return "是";
			} else {
				return "";
			}
		}
	});
	colOpts.push({
		"data" : "oddNumber",
		"title" : "运输单号",
		"className":"edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "oddNumber");
		}
	});
	
	colOpts.push({
		"data" : "sampleType-name",
		"title" : "样本类型",
		"createdCell" : function(td, data, rowData) {
			$(td).attr("saveName", "sampleType-name");
			$(td).attr("sampleType-id", rowData['sampleType-id']);
		},
	});

	// colOpts.push({
	// "data": 'floor',
	// "title": "楼层",
	// "className": "edit",
	//			
	// "createdCell": function(td) {
	// $(td).attr("saveName", "floor");
	// }
	// });
	colOpts.push({
		"data" : 'sampleOrder-attendingDoctor',
		"title" : "医生",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "sampleOrder-attendingDoctor");
		}
	});
	
	colOpts.push({
		"data" : 'bloodCollection',
		"title" : "取血人",
		"className" : "edit",
		"createdCell" : function(td) {
			$(td).attr("saveName", "bloodCollection");
		}
	});
	colOpts.push({
		"data" : 'drawBloodTime',
		"title" : "预计采血时间",
//		"className" : "date",
		"createdCell" : function(td) {
			$(td).attr("saveName", "drawBloodTime");
		}
	});
	

	colOpts.push({
		"data" : "state",
		"title" : biolims.ufTask.state,
		"createdCell" : function(td) {
			$(td).attr("saveName", "state");
		},
		"visible" : false
	});
	
	// colOpts.push( {
	// "data": "createUser-name",
	// "title": biolims.sample.createUserName,
	// "createdCell": function(td, data, rowData) {
	// $(td).attr("saveName", "createUser-name");
	// $(td).attr("createUser-id", rowData['createUserr-id']);
	// }
	// });

	// colOpts.push({
	// "data":"stateName",
	// "title": biolims.ufTask.stateName,
	// "createdCell": function(td) {
	// $(td).attr("saveName", "stateName");
	// },
	// });
	colOpts.push({
		"data" : "note",
		"title" : biolims.ufTask.note,
		"createdCell" : function(td) {
			$(td).attr("saveName", "note");
		},
		"className" : "textarea",
		"visible" : false,
	});
	
	colOpts.push({
		"data" : "expressCompany-id",
		"title" : "运输公司ID",
		"visible":false,
		"createdCell" : function(td,data) {
			$(td).attr("saveName", "expressCompany-id");
		},
	});
	colOpts.push({
		"data" : "expressCompany-name",
		"title" : "运输公司",
		"visible":false,
		"createdCell" : function(td,data,rowData) {
			$(td).attr("saveName", "expressCompany-name");
			$(td).attr("expressCompany-id", rowData['expressCompany-id']);
		},
	});

//	colOpts.push({
//		"data" : "createUser-name",
//		"title" : "签收人",
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "createUser-name");
//		},
////		"className" : "edit"
//	});
//	colOpts.push({
//		"data" : "acceptDate",
//		"title" : "签收日期",
//		"createdCell" : function(td) {
//			$(td).attr("saveName", "acceptDate");
//		},
////		"className" : "date"
//	});
	colOpts.push({
		"data" : "approvalUser-id",
		"title" : "签收确认人",
		"visible" :false,
		"createdCell" : function(td,data) {
			$(td).attr("saveName", "approvalUser-id");
		},
//		"className" : "edit"
	});
	colOpts.push({
		"data" : "phoneNumber",
		"title" : "联系方式",
		"className":"edit",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "phoneNumber");
		}
	});
	colOpts.push({
		"data" : "confirmationDate",
		"title" : "确认日期",
		"visible":false,
		"createdCell" : function(td) {
			$(td).attr("saveName", "confirmationDate");
		},
		"className" : "date"
	});
	colOpts.push({
		"data" : "freight",
		"title" : "运输费",
		"createdCell" : function(td) {
			$(td).attr("saveName", "freight");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "actualTime",
		"title" : biolims.common.actualTime,
		"createdCell" : function(td) {
			$(td).attr("saveName", "actualTime");
		},
		"className" : "edit",
		"visible" : false,
	});
	colOpts.push({
		"data" : "samplingDate",
		"title" : "采血日期",
		"createdCell" : function(td) {
			$(td).attr("saveName", "samplingDate");
		},
		"className" : "date",
		"visible" : false,
	});
	colOpts.push({
		"data" : "personFollowCar",
		"title" : "跟车人员",
		"createdCell" : function(td) {
			$(td).attr("saveName", "personFollowCar");
		},
		"className" : "edit"
	});
	colOpts.push({
		"data" : "invalidState",
		"visible" : false,
		"title" : "作废状态",
		"className" : "select",
		"name" : "是" + "|" + "否",
		"createdCell" : function(td) {
			$(td).attr("saveName", "invalidState");
			$(td).attr("selectOpt", "是" + "|" + "否");
		},
		"render" : function(data, type, full, meta) {
			if (data == "1") {
				return "是";
			} else {
				return "否";
			}
		}
	});
	 colOpts.push({ 
			"title": "上传附件",
			"width": "100px",
			"data": null,
			"createdCell": function(td, data) {},
			"render": function() {
				return '<input type="button" value="上传附件" onClick="javascript:fileUp4(this);" >' +
					'<input type="button" value="查看附件" onClick="javascript:fileView4(this);">'

			},

		});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view" && $("#transportOrder_stateName").val() != "完成") {
		tbarOpts.push({
			text : biolims.master.selectOrder,
			action : function() {
				selectOrder($("#transportSampleItem"))
			}
		});
		tbarOpts.push({
			text : "保 存",
			action : function() {
				saveItem($("#transportSampleItem"));
			}
		});
//		tbarOpts.push({
//			text: '<i class="fa fa-ioxhost"></i>'+biolims.common.search,
//			action : function() {
//				search()
//			}
//		});
		// tbarOpts.push({
		// text: biolims.common.fillDetail,
		// action: function() {
		// addItem($("#transportSampleItem"))
		// }
		// });
		// tbarOpts.push({
		// text: biolims.common.addwindow,
		// action: function() {
		// addItemLayer($("#transportSampleItem"))
		// }
		// });
		// tbarOpts.push({
		// text: biolims.common.editwindow,
		// action: function() {
		// editItemLayer($("#transportSampleItem"))
		// }
		// });
		/*
		 * tbarOpts.push({ text: biolims.common.batchUpload, action: function() {
		 * $("#uploadCsv").modal("show"); $(".fileinput-remove").click(); var
		 * csvFileInput = fileInputCsv("");
		 * csvFileInput.off("fileuploaded").on("fileuploaded", function(event,
		 * data, previewId, index) { $.ajax({ type: "post", data: { id:
		 * $("#sampleTransportItem_id").val(), fileId: data.response.fileId },
		 * url: ctx +
		 * "/tra/sampletransport/sampleTransport/uploadCsvFile.action", success:
		 * function(data) { var data = JSON.parse(data) if(data.success) {
		 * sampleTransportItemTable.ajax.reload(); } else {
		 * layer.msg(biolims.common.uploadFailed) } } }); }); } });
		 */
		tbarOpts
				.push({
					text : biolims.common.delSelected,
					action : function() {
						removeChecked($("#transportSampleItem"),
								"/tra/transport/transportOrder/delTransportSampleItem.action");
					}
				});
		tbarOpts.push({
			text : biolims.common.editwindow,
			action : function() {
				editItemLayer($("#transportSampleItem"))
			}
		});
	}

	var TransportSampleItemTabOptions = table(
			true,
			null,
			'/tra/transport/transportOrder/showTransportItemSampleTableJson.action?id='+id,
			colOpts, tbarOpts)
	TransportSampleItemTab = renderData($("#transportSampleItem"),
			TransportSampleItemTabOptions);
	TransportSampleItemTab.on('draw', function() {
		TransportSampleItemChangeLog = TransportSampleItemTab.ajax.json();
	});
});


//上传附件
function fileUp4(that) {
	var str = '<div class="modal fade" id="uploadFilex" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg" role="document"><div class="modal-content"><div class="modal-header"><h3 class="modal-title" id="exampleModalLabel"><fmt:message key="biolims.common.uploadFiles"/><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span> </button></h3></div><div class="modal-body"><div class="file-loading"><input id="uploadFileValx" name="excelFile" multiple type="file"></div><div id="kartik-file-errors"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></div></div></div></div>';
	$("#tableFileLoad").html(str);
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	if(id==""){
		top.layer.msg("请先保存数据，再上传附件！");
		return;
	}
	$("#uploadFileValx").fileinput({
		uploadUrl: ctx + "/attachmentUpload?useType=misc2&modelType=sampleTransportItem&contentId=" + id,
		uploadAsync: true,
		language: 'zh',
		showPreview: true,
		enctype: 'multipart/form-data',
		elErrorContainer: '#kartik-file-errors',
	});
	//	if($("#uploadFile .fileinput-remove").length){
	//		$("#uploadFile .fileinput-remove").eq(0).click();
	//	}
	$("#uploadFilex").modal("show");
	//var cfileInput = fileInput('1','samplingTaskItem', id);
}
//附件查看
function fileView4(that) {
	var id = $(that).parents("tr").children("td").eq(0).find("input").val();
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-layer-lan',
		area: [600,400],
		content: window.ctx + "/operfile/initFileList.action?flag=misc2&modelType=sampleTransportItem&id=" + id,
		cancel: function(index, layero) {
			top.layer.close(index)
		}
	})
}
// 选择订单
function selectOrder(ele) {
	top.layer
			.open({
				title : biolims.common.pleaseChoose,
				type : 2,
				area : [ "650px", "400px" ],
				btn : biolims.common.confirmSelected,
				content : [
						window.ctx
								+ "/system/sample/sampleOrder/showSampleOrderDialogList.action",
						'' ],
				yes : function(index, layer) {
					var id = [];
					$('.layui-layer-iframe', parent.document).find("iframe")
							.contents().find("#addSampleOrder .selected").each(
									function(i, v) {
										id.push($(v).children("td").eq(1)
												.text());
									});
					var flg = checkSelected(id);
					if (flg == true) {
						top.layer.msg("与表格中数据重复，请仔细选择");
						return false;
					}
					var len = id.length;
					top.layer.close(index);
					addl($("#transportSampleItem"), len);
					addInfo1(id);
				},
			})
}
function addInfo1(id) {
	var flag = false;
	var blindex = 0;
	$("#transportSampleItem tbody tr").each(
			function(i, v) {
				if (blindex <= id.length) {
					$(v).addClass("editagain");
					$(v).find("td[savename='orderNo']").attr(
							"sampleOrder-id", id[blindex]).text(id[blindex]);
					//选择订单带入QA审核人,签收日期,运输公司
					$.ajax({
						type : 'post',
						url : '/tra/transport/transportOrder/showDateAndUser.action',
						async:false,
						data : {
							id : id[blindex],
						},
						success : function(dataB) {
							var data = JSON.parse(dataB);
							if (data.success) {
								$(v).find("td[savename='approvalUser-id']").text(data.user);
								$(v).find("td[savename='acceptDate']").text(data.date);
								$(v).find("td[savename='expressCompany-name']").attr(
										"expressCompany-id", data.companyId).text(data.company);
							}
						}
					})
				}
				blindex++;
				flag = true;
			})
}
function addl(ele, len) {
	$(".dataTables_scrollHead th").off();
	// 禁用固定列
	$("#fixdeLeft2").hide();
	// 禁用列显示隐藏
	$(".colvis").hide();
	// 清除没有内容选项
	ele.find(".dataTables_empty").parent("tr").remove();
	// 添加明细
	for (var inin = 0; inin < len; inin++) {
		var ths = ele.find("th");
		var tr = $("<tr><td><input type='checkbox' class='icheck' value=''/></td></tr>");
		tr.height(32);
		for (var i = 1; i < ths.length; i++) {
			var edit = $(ths[i]).attr("key");
			var saveName = $(ths[i]).attr("saveName");
			if (edit == "select") {
				var selectOpt = $(ths[i]).attr("selectopt");
				tr.addClass("editagain");
				tr.append("<td class=" + edit + " saveName=" + saveName
						+ " selectopt=" + selectOpt + "></td>");
			} else {
				tr.addClass("editagain");
				tr.append("<td class=" + edit + " saveName=" + saveName
						+ "></td>");
			}
		}
		ele.find("tbody").prepend(tr);
		checkall(ele);
	}
}
function checkSelected(id) {
	var flag = false;
	$("#transportSampleItem tbody tr").each(function(i, v) {
		var thisId = $(this).find("td[savename='orderNo']").text();
		if (thisId != "" && thisId != null) {
			for (var ia = 0; ia < id.length; ia++) {
				if (thisId == id[ia]) {
					flag = true;
				}
			}
		}
	});
	return flag;
}
// 保存
function saveItem(ele) {
	if ($("#transportOrder_id").val() == "NEW") {
		top.layer.msg("请先保存主表");
		return false;
	}
	var data = saveSampleTransportItemjson(ele);
	var ele = $("#transportSampleItem");
	var changeLog = "样本运输：";
	changeLog = getSampleTransportChangeLog(data, ele, changeLog);

	$
			.ajax({
				type : 'post',
				url : '/tra/transport/transportOrder/saveTransportSampleItemTable.action',
				data : {
					id : $("#transportOrder_id").val(),
					dataJson : data,
					changeLog : changeLog
				},
				success : function(data) {
					var data = JSON.parse(data)
					if (data.success) {
						top.layer.msg(biolims.common.saveSuccess);
						TransportSampleItemTab.ajax.reload();
					} else {
						top.layer.msg(biolims.common.saveFailed)
					}
					;
				}
			})
}
// 获得保存时的json数据
function saveSampleTransportItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for (var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			//采血物品
			if(k == "bloodSampling") {
				var bloodSampling = $(tds[j]).text();
				if(bloodSampling == "否") {
					json[k] = "0";
				} else if(bloodSampling == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			//输血器
			if(k == "transfusion") {
				var transfusion = $(tds[j]).text();
				if(transfusion == "单头") {
					json[k] = "0";
				} else if(transfusion == "双头") {
					json[k] = "1";
				}else if(transfusion == "否"){
					json[k] = "2";
				}else{
					json[k] = "";
				}
				continue;
			}
			//氯化钠
			if(k == "saline") {
				var saline = $(tds[j]).text();
				if(saline == "否") {
					json[k] = "0";
				} else if(saline == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			//是否通知
			if(k == "notification") {
				var notification = $(tds[j]).text();
				if(notification == "否") {
					json[k] = "0";
				} else if(notification == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			//是否确认
			if(k == "affirm") {
				var affirm = $(tds[j]).text();
				if(affirm == "否") {
					json[k] = "0";
				} else if(affirm == "是") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}

			//运输温度
			if(k == "transportTemperature") {
				var transportTemperature = $(tds[j]).text();
				if(transportTemperature == "15~25℃") {
					json[k] = "0";
				} else if(transportTemperature == "2~8℃") {
					json[k] = "1";
				}else{
					json[k] = "";
				}
				continue;
			}
			
			if (k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}

			if (k == "orderNo") {
				json["sampleOrder-id"] = $(tds[j]).attr("sampleOrder-id");
			}
			//获取运输公司的json数据
			if(k == "expressCompany-name") {
				json["expressCompany-id"] = $(tds[j]).attr("expressCompany-id");
				continue;
			}

			if (k == "invalidState") {
				var result = $(tds[j]).text();
				if (result == "是") {
					json[k] = "1";
				} else {
					json[k] = "0";
				}
				continue;
			}
			
			if (k == "sampleType-name") {
				json["sampleType-id"] = $(tds[j]).attr("sampleType-id");
			}
			
			var hsrq = $("#transportOrder_transportDate").val();
			if(k == "planTime") {
				var planTime = $(tds[j]).text();
				if(planTime!=""){
					planTime = hsrq+" "+planTime;
					json[k] = planTime;
				}else{
					json[k] = hsrq;
				}
				console.log(planTime)
				continue;
			}

			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getSampleTransportChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		TransportSampleItemChangeLog.data.forEach(function(vv, ii) {
			if (vv.id == id) {
				for ( var k in v) {
					if (v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"'
								+ v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function invalidSampleOrder() {
	var rows = $("#transportSampleItem .selected");
	if (!rows.length) {
		top.layer.msg("请选择数据！");
		return false;
	}
	var ids = [];
	$("#transportSampleItem .selected").each(function(i, v) {
		ids.push($(this).find("input[type='checkbox']").val());
	})
	$
			.ajax({
				type : "post",
				url : ctx
						+ "/tra/sampletransport/sampleTransport/invalidSampleOrder.action",
				data : {
					id : ids,
				},
				success : function(dataBack) {
					var data = JSON.parse(dataBack);
					if (data.success) {
						top.layer.msg("订单作废成功！");
						TransportSampleItemTab.ajax.reload();
					}
				}
			});
}
//function searchOptions() {
//	
//	return [{
//				"txt" : "订单编号",
//				"type" : "input",
//				"searchName" : "orderNo",
//			},{
//				"txt" : "预计采血时间",
//				"type" : "dataTime",
//				"searchName" : "drawBloodTime##@@##1",
//				"mark" : "s##@@##"
//			},{
//				"type" : "table",
//				"table" : TransportSampleItemTab
//			}];
//}
function transportRefush() {
	TransportSampleItemTab.ajax.reload();
}
