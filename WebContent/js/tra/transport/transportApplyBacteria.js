﻿
var transportApplyBacteriaGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
//	    fields.push({
//		name:'materailsMain-id',
//		type:"string"
//	});
//	    fields.push({
//		name:'materailsMain-name',
//		type:"string"
//	});
   fields.push({
		name:'objName',
		type:"string"
	});
    fields.push({
		name:'projectName',
		type:"string"
	});
	   fields.push({
		name:'concentration',
		type:"string"
	});
	   fields.push({
		name:'volume',
		type:"string"
	});
	   fields.push({
		name:'antibody',
		type:"string"
	});
	   fields.push({
		name:'od260',
		type:"string"
	});
	   fields.push({
		name:'od230',
		type:"string"
	});
	   fields.push({
		   name:'note',
		   type:"string"
	   });
	    fields.push({
		name:'transportApply-id',
		type:"string"
	});
	    fields.push({
		name:'transportApply-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编码',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
//	cm.push({
//		dataIndex:'materailsMain-id',
//		hidden : true,
//		header:'物品类型ID',
//		width:15*10,
//		editor : new Ext.form.TextField({
//			allowBlank : true
//		})
//	});
//	cm.push({
//		dataIndex:'materailsMain-name',
//		hidden : false,
//		header:'物品类型',
//		width:15*10
//	});
	var typestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', '活体' ], [ '1', '质粒/菌液' ], [ '2', '细胞' ],[ '3', '组织' ]]
	});
	
	var typeComboxFun = new Ext.form.ComboBox({
		store : typestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'objName',
		hidden : false,
		header:'产品类型',
		width:15*10,
		renderer: Ext.util.Format.comboRenderer(typeComboxFun),	
		sortable:true
	});
	cm.push({
		dataIndex:'projectName',
		hidden : false,
		header:'项目名称',
		width:15*10
	});
	cm.push({
		dataIndex:'concentration',
		hidden : false,
		header:'浓度(ng/μl)',
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'volume',
		hidden : false,
		header:'体积 *(μl/管)',
		width:20*6,
		editor : new Ext.form.NumberField({
			allowDecimals: false, // 不允许小数点 
			allowNegative: false, // 不允许负数 
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'antibody',
		hidden : false,
		header:'抗体',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'od260',
		hidden : false,
		header:'260/280',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'note',
		hidden : false,
		header:'备注',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'od230',
		hidden : false,
		header:'260/230',
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportApply-id',
		hidden : true,
		header:'相关主表ID',
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'transportApply-name',
		hidden : true,
		header:'相关主表',
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/tra/transport/transportApply/showTransportApplyBacteriaListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="产品运输(质粒/菌液)信息。";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/tra/transport/transportApply/delTransportApplyBacteria.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
    
	
	if($("#transportApply_state").val()==1){
//		   alert("工作流状态已完成，页面不可编辑。");
		 } else{
//				opts.tbar.push({
//						text : '选择物品类型',
//						//handler : selectmaterailsMainFun
//						handler : null
//					});
			    
				/*opts.tbar.push({
						text : '选择相关主表',
						handler : selecttransportApplyFun
					});*/
				
				
				
				opts.tbar.push({
					text : '显示可编辑列',
					handler : null
				});
				opts.tbar.push({
					text : '取消选中',
					handler : null
				});
				opts.tbar.push({
					text : '填加明细',
					iconCls : 'add',
					handler : function() {
						var ob = transportApplyBacteriaGrid.getStore().recordType;
						var p = new ob({});
						p.isNew = true;
						p.set("projectName", $("#transportApply_project_name").val());
						p.set("objName", $("#transportApply_objType").val());
						transportApplyBacteriaGrid.stopEditing();
						transportApplyBacteriaGrid.getStore().insert(0, p);
						transportApplyBacteriaGrid.startEditing(0, 0);
					}
				});
								
				opts.tbar.push({
					text : "批量上传（CSV文件）",
					handler : function() {
						var options = {};
						options.width = 350;
						options.height = 200;
						loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
							"确定":function(){
								goInExcelcsv();
								$(this).dialog("close");
							}
						},true,options);
					}
				});
		 }

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = transportApplyBacteriaGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 6-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 7-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							transportApplyBacteriaGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	
	transportApplyBacteriaGrid=gridEditTable("transportApplyBacteriadiv",cols,loadParam,opts);
	$("#transportApplyBacteriadiv").data("transportApplyBacteriaGrid", transportApplyBacteriaGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

//function selectmaterailsMainFun(){
//	var win = Ext.getCmp('selectmaterailsMain');
//	if (win) {win.close();}
//	var selectmaterailsMain= new Ext.Window({
//	id:'selectmaterailsMain',modal:true,title:'选择物品类型',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/MaterailsMainSelect.action?flag=materailsMain' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selectmaterailsMain.close(); }  }]  });     selectmaterailsMain.show(); }
//	function setmaterailsMain(id,name){
//		var gridGrid = $("#transportApplyBacteriadiv").data("transportApplyBacteriaGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('materailsMain-id',id);
//			obj.set('materailsMain-name',name);
//		});
//		var win = Ext.getCmp('selectmaterailsMain')
//		if(win){
//			win.close();
//		}
//	}
//	
//function selecttransportApplyFun(){
//	var win = Ext.getCmp('selecttransportApply');
//	if (win) {win.close();}
//	var selecttransportApply= new Ext.Window({
//	id:'selecttransportApply',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
//	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
//	collapsible: true,maximizable: true,
//	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
//	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/TransportApplySelect.action?flag=transportApply' frameborder='0' width='100%' height='100%' ></iframe>"}),
//	buttons: [
//	{ text: '关闭',
//	 handler: function(){
//		 selecttransportApply.close(); }  }]  });     selecttransportApply.show(); }
//	function settransportApply(id,name){
//		var gridGrid = $("#transportApplyBacteriadiv").data("transportApplyBacteriaGrid");
//		var selRecords = gridGrid.getSelectionModel().getSelections(); 
//		$.each(selRecords, function(i, obj) {
//			obj.set('transportApply-id',id);
//			obj.set('transportApply-name',name);
//		});
//		var win = Ext.getCmp('selecttransportApply')
//		if(win){
//			win.close();
//		}
//	}
	
