$(document).ready(function() {


var fields = [];
	   
	fields.push({
		title : "编号",
		field : "id"
	});
	
	   
	fields.push({
		title : "描述",
		field : "name"
	});
	
	  fields.push({
		title : "创建人",
		field : "createUser.name"
	});
	   
	fields.push({
		title : "提交时间",
		field : "createDate"
	});
	
	   
	fields.push({
		title : "状态id",
		field : "state"
	});
	
	   
	fields.push({
		title : "状态",
		field : "stateName"
	});
	
	   
	fields.push({
		title : "备注",
		field : "note"
	});
	
	  fields.push({
		title : "运输人",
		field : "transportUser.name"
	});
	   
	fields.push({
		title : "运输公司",
		field : "transportCompany"
	});
	
	   
	fields.push({
		title : "运输单号",
		field : "trackingNumber"
	});
	
	   
	fields.push({
		title : "开始时间",
		field : "beginDate"
	});
	
	   
	fields.push({
		title : "结束时间",
		field : "endDate"
	});
	
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/tra/sampletransport/sampleTransport/showSampleTransportListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: fields
	});

});
// 新建
function add() {
	window.location = window.ctx +
		"/tra/sampletransport/sampleTransport/editSampleTransport.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/tra/sampletransport/sampleTransport/editSampleTransport.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/tra/sampletransport/sampleTransport/viewSampleTransport.action?id=" + id;
}

// 弹框模糊查询参数
function searchOptions() {

var fields = [];
	   
	fields.push({
		"txt" : "编号",
		"searchName" : 'id',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "描述",
		"searchName" : 'name',
		"type" : "input"
	});
	
	   fields.push({
		header : '创建人',
		"searchName" : 'createUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "提交时间",
		"searchName" : 'createDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态id",
		"searchName" : 'state',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "状态",
		"searchName" : 'stateName',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "备注",
		"searchName" : 'note',
		"type" : "input"
	});
	
	   fields.push({
		header : '运输人',
		"searchName" : 'transportUser.name',
		"type" : "input"
	});
	   
	fields.push({
		"txt" : "运输公司",
		"searchName" : 'transportCompany',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "运输单号",
		"searchName" : 'trackingNumber',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "开始时间",
		"searchName" : 'beginDate',
		"type" : "input"
	});
	
	   
	fields.push({
		"txt" : "结束时间",
		"searchName" : 'endDate',
		"type" : "input"
	});
	

	fields.push({
			"type" : "table",
			"table" : $('#mytreeGrid'),
			"reloadUrl":ctx+"/com/biolims/system/product/showProductListJson.action"
		});





	return fields;
}

