var sampleTrackingConditionTable;
var oldsampleTrackingConditionChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#sampleTransport_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": "ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.common.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
	    "className": "edit"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": "创建人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		},
		"visible": false
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.sample.createUserName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUserr-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.sample.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.ufTask.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
	    "visible": false
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.ufTask.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.common.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"temperature",
		"title": biolims.common.temperature,
		"createdCell": function(td) {
			$(td).attr("saveName", "temperature");
	    },
	});
	   colOpts.push({
		"data":"changeTime",
		"title": biolims.common.changeTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "changeTime");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"changeSite",
		"title": biolims.common.locationTracking,
		"createdCell": function(td) {
			$(td).attr("saveName", "changeSite");
	    },
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view" && $("#sampleTransport_stateName").val()!="完成") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#sampleTrackingConditionTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#sampleTrackingConditionTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#sampleTrackingConditionTable"))
		}
	});
	/*tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#sampleTrackingCondition_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/tra/sampletransport/sampleTransport/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 sampleTrackingConditionTable.ajax.reload();
						} else {
							layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});*/
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveSampleTrackingCondition($("#sampleTrackingConditionTable"));
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#sampleTransportItemTable"),
				"/tra/sampletransport/sampleTransport/delSampleTrackingCondition.action");
		}
	});
	}
	
	var sampleTrackingConditionOptions = table(true,
		id,
		'/tra/sampletransport/sampleTransport/showSampleTrackingConditionTableJson.action', colOpts, tbarOpts)
	sampleTrackingConditionTable = renderData($("#sampleTrackingConditionTable"), sampleTrackingConditionOptions);
	sampleTrackingConditionTable.on('draw', function() {
		oldsampleTrackingConditionChangeLog = sampleTrackingConditionTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveSampleTrackingCondition(ele) {
	var data = saveSampleTrackingConditionjson(ele);
	var ele=$("#sampleTrackingConditionTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/tra/sampletransport/sampleTransport/saveSampleTrackingConditionTable.action',
		data: {
			id: $("#sampleTransport_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveSampleTrackingConditionjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldsampleTrackingConditionChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
