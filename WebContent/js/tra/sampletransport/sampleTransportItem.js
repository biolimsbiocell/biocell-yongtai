var sampleTransportItemTable;
var oldsampleTransportItemChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表
	var id = $("#sampleTransport_id").val();
	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": "ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"visible": false
	});
	   colOpts.push({
		"data":"orderNo",
		"title": biolims.common.orderCode,
		"createdCell": function(td) {
			$(td).attr("saveName", "orderNo");
	    }
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.common.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
	    "className":"edit"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": "创建人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		},
		"visible": false
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.sample.createUserName,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUserr-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.sample.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.ufTask.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"visible": false
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.ufTask.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
	});
	   colOpts.push({
		"data":"note",
		"title":biolims.ufTask.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	   colOpts.push({
		"data":"planTime",
		"title": biolims.common.planTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "planTime");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"actualTime",
		"title": biolims.common.actualTime,
		"createdCell": function(td) {
			$(td).attr("saveName", "actualTime");
	    },
		"className": "edit"
	});
	   colOpts.push({
			"data":"samplingDate",
			"title": "采血日期",
			"createdCell": function(td) {
				$(td).attr("saveName", "samplingDate");
		    },
			"className": "date"
		});
	   colOpts.push({
			"data":"invalidState",
			"title": "作废状态",
			"className":"select",
			"name":"是"+"|"+"否",
			"createdCell": function(td) {
				$(td).attr("saveName", "invalidState");
				$(td).attr("selectOpt", "是"+"|"+"否");
		    },
		    "render": function(data, type, full, meta) {
				if(data == "1") {
					return "是";
				}else{
					return "否";
				}
			}
		}); 
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view" && $("#sampleTransport_stateName").val()!="完成") {
		
	tbarOpts.push({
		text: biolims.master.selectOrder,
		action: function() {
			selectOrder($("#sampleTransportItemTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#sampleTransportItemTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#sampleTransportItemTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#sampleTransportItemTable"))
		}
	});
	/*tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#sampleTransportItem_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/tra/sampletransport/sampleTransport/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 sampleTransportItemTable.ajax.reload();
						} else {
							layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});*/
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveSampleTransportItem($("#sampleTransportItemTable"));
		}
	});
	tbarOpts.push({
		text: biolims.common.delSelected,
		action: function() {
			removeChecked($("#sampleTransportItemTable"),
				"/tra/sampletransport/sampleTransport/delSampleTransportItem.action");
		}
	});
	}
	
	var sampleTransportItemOptions = table(true,
		id,
		'/tra/sampletransport/sampleTransport/showSampleTransportItemTableJson.action', colOpts, tbarOpts)
	sampleTransportItemTable = renderData($("#sampleTransportItemTable"), sampleTransportItemOptions);
	sampleTransportItemTable.on('draw', function() {
		oldsampleTransportItemChangeLog = sampleTransportItemTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view" || $("#sampleTransport_stateName").val()=="完成") {
		settextreadonly();
		$("#btn_save").hide();
		$("#btn_changeState").hide();
		$("#btn_submit").hide();
	}
});

//选择订单
function selectOrder(ele){
	var rows = ele.find(".selected");
	var length = rows.length;
	if(!length){
		top.layer.msg(biolims.common.pleaseSelectAPieceOfData);
	}else if(length>1){
		top.layer.msg("只能选择一条数据");
	}else{
		top.layer.open({
			title: biolims.common.pleaseChoose,
			type: 2,
			area: ["650px", "400px"],
			btn: biolims.common.confirmSelected,
			content: [window.ctx + "/system/sample/sampleOrder/showSampleOrderDialogList.action", ''],
			yes: function(index, layer) {
				var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addSampleOrder .chosed").children("td")
				.eq(0).text();
				rows.addClass("editagain");
				var flag=true;
				$("#sampleTransportItemTable tr").each(function(i,v){
					if($(this).find("td[saveName='orderNo']").text()==id){
						flag=false;
					}
				})
				if(flag==true){
					rows.find("td[saveName=orderNo]").text(id);
				}
				top.layer.closeAll();
			},
		})
	}
}
// 保存
function saveSampleTransportItem(ele) {
	var data = saveSampleTransportItemjson(ele);
	var ele=$("#sampleTransportItemTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	
	$.ajax({
		type: 'post',
		url: '/tra/sampletransport/sampleTransport/saveSampleTransportItemTable.action',
		data: {
			id: $("#sampleTransport_id").val(),
			dataJson: data,
			changeLog: changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveSampleTransportItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
			if(k == "invalidState") {
				var result = $(tds[j]).text();
				if(result == "是") {
					json[k] = "1";
				} else {
					json[k] = "0";
				}
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldsampleTransportItemChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
function invalidSampleOrder(){
	var rows=$("#sampleTransportItemTable .selected");
	if(!rows.length){
		top.layer.msg("请选择数据！");
		return false;
	}
	var ids=[];
	$("#sampleTransportItemTable .selected").each(function(i,v){
		ids.push($(this).find("input[type='checkbox']").val());
	})
	$.ajax({
		type:"post",
		url:ctx+"/tra/sampletransport/sampleTransport/invalidSampleOrder.action",
		data:{
			id:ids,
		},
		success:function(dataBack){
			var data=JSON.parse(dataBack);
			if(data.success){
				top.layer.msg("订单作废成功！");
				sampleTransportItemTable.ajax.reload();
			}
		}
	});
}