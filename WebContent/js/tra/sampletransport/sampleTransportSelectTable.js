var sampleTransportTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.sampleTransport.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.sampleTransport.name,
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":"创建人ID"
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.sampleTransport.createUser
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.sampleTransport.createDate,
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.sampleTransport.state,
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.sampleTransport.stateName,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.sampleTransport.note,
	});
	
	    fields.push({
		"data":"transportUser-id",
		"title":"运输人ID"
	});
	    fields.push({
		"data":"transportUser-name",
		"title":biolims.sampleTransport.transportUser
	});
	
	    fields.push({
		"data":"transportCompany",
		"title":biolims.sampleTransport.transportCompany,
	});
	
	    fields.push({
		"data":"trackingNumber",
		"title":biolims.sampleTransport.trackingNumber,
	});
	
	    fields.push({
		"data":"beginDate",
		"title":biolims.sampleTransport.beginDate,
	});
	
	    fields.push({
		"data":"endDate",
		"title":biolims.sampleTransport.endDate,
	});
	
	var options = table(true, "","/tra/sampletransport/sampleTransport/showSampleTransportTableJson.action",
	 fields, null)
	sampleTransportTable = renderData($("#addSampleTransportTable"), options);
	$('#addSampleTransportTable').on('init.dt', function() {
		recoverSearchContent(sampleTransportTable);
	})
});

//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.sampleTransport.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.sampleTransport.name
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"创建人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.sampleTransport.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.sampleTransport.createDate
		});
	fields.push({
			"txt": "提交时间(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "提交时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.sampleTransport.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.sampleTransport.stateName
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.sampleTransport.note
		});
	fields.push({
	    "type":"input",
		"searchName":"transportUser.id",
		"txt":"运输人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"transportUser.name",
		"txt":biolims.sampleTransport.transportUser
	});
	   fields.push({
		    "searchName":"transportCompany",
			"type":"input",
			"txt":biolims.sampleTransport.transportCompany
		});
	   fields.push({
		    "searchName":"trackingNumber",
			"type":"input",
			"txt":biolims.sampleTransport.trackingNumber
		});
	   fields.push({
		    "searchName":"beginDate",
			"type":"input",
			"txt":biolims.sampleTransport.beginDate
		});
	fields.push({
			"txt": "开始时间(Start)",
			"type": "dataTime",
			"searchName": "beginDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "开始时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "beginDate##@@##2"
		});
	   fields.push({
		    "searchName":"endDate",
			"type":"input",
			"txt":biolims.sampleTransport.endDate
		});
	fields.push({
			"txt": "结束时间(Start)",
			"type": "dataTime",
			"searchName": "endDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "结束时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "endDate##@@##2"
		});
	
	fields.push({
		"type":"table",
		"table":sampleTransportTable
	});
	return fields;
}
