var sampleTransportTable;
$(function() {
	var fields=[];
	    fields.push({
		"data":"id",
		"title":biolims.common.id,
	});
	
	    fields.push({
		"data":"name",
		"title":biolims.common.id,
		"visible":false
	});
	
	    fields.push({
		"data":"createUser-id",
		"title":"创建人ID",
		"visible":false
	});
	    fields.push({
		"data":"createUser-name",
		"title":biolims.common.id,
		"visible":false
	});
	
	    fields.push({
		"data":"createDate",
		"title":biolims.tStorage.createDate,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"state",
		"title":biolims.tProject.state,
		"visible":false
	});
	
	    fields.push({
		"data":"stateName",
		"title":biolims.tProject.state,
	});
	
	    fields.push({
		"data":"note",
		"title":biolims.tInstrumentRepair.note,
	});
	
	    /*fields.push({
		"data":"transportUser-id",
		"title":"运输人ID",
		"visible":false
	});*/
	    fields.push({
		"data":"transportUser",
		"title":biolims.common.transportUser,
	});
	
	    fields.push({
		"data":"transportCompany",
		"title":biolims.common.transportCompany,
	});
	
	    fields.push({
		"data":"trackingNumber",
		"title":biolims.common.trackingNumber,
	});
	
	    fields.push({
		"data":"beginDate",
		"title":biolims.common.startTime,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	
	    fields.push({
		"data":"endDate",
		"title":biolims.common.endTime,
		"render": function(data) {
				if(data) {
					return parent.moment(data).format('YYYY-MM-DD');
				} else {
					return "";
				}
			}
	});
	

		$.ajax({
			type:"post",
			url:window.ctx + "/system/customfields/findFieldByModuleValue.action",
			async:false,
			data:{
				moduleValue : "SampleTransport"
			},
			success:function(data){
				var objData = JSON.parse(data);
				if(objData.success){
					$.each(objData.data, function(i,n) {
						var str = {
							"data" : n.fieldName,
							"title" : n.label
						}
						colData.push(str);
					});
					
				}else{
					layer.msg("表格解析错误，请重新刷新界面！");
				}
			}
		});

	var options = table(true, "","/tra/sampletransport/sampleTransport/showSampleTransportTableJson.action",
	 fields, null)
	sampleTransportTable = renderData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(sampleTransportTable);
	})
});
// 新建
function add() {
	window.location = window.ctx +
		"/tra/sampletransport/sampleTransport/editSampleTransport.action";
}
// 编辑
function edit() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/tra/sampletransport/sampleTransport/editSampleTransport.action?id=" + id;
}
// 查看
function view() {
	var id = $(".selected").find("input").val();
	if(id == "" || id == undefined) {
		layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx +
		"/tra/sampletransport/sampleTransport/viewSampleTransport.action?id=" + id;
}
//弹框模糊查询参数
function searchOptions() {
var fields=[];
	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt":biolims.sampleTransport.id
		});
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt":biolims.sampleTransport.name
		});
	fields.push({
	    "type":"input",
		"searchName":"createUser.id",
		"txt":"创建人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"createUser.name",
		"txt":biolims.sampleTransport.createUser
	});
	   fields.push({
		    "searchName":"createDate",
			"type":"input",
			"txt":biolims.sampleTransport.createDate
		});
	fields.push({
			"txt": "提交时间(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "提交时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "createDate##@@##2"
		});
	   fields.push({
		    "searchName":"state",
			"type":"input",
			"txt":biolims.sampleTransport.state
		});
	   fields.push({
		    "searchName":"stateName",
			"type":"input",
			"txt":biolims.sampleTransport.stateName
		});
	   fields.push({
		    "searchName":"note",
			"type":"input",
			"txt":biolims.sampleTransport.note
		});
	fields.push({
	    "type":"input",
		"searchName":"transportUser.id",
		"txt":"运输人ID"
	});
	fields.push({
	    "type":"input",
		"searchName":"transportUser.name",
		"txt":biolims.sampleTransport.transportUser
	});
	   fields.push({
		    "searchName":"transportCompany",
			"type":"input",
			"txt":biolims.sampleTransport.transportCompany
		});
	   fields.push({
		    "searchName":"trackingNumber",
			"type":"input",
			"txt":biolims.sampleTransport.trackingNumber
		});
	   fields.push({
		    "searchName":"beginDate",
			"type":"input",
			"txt":biolims.sampleTransport.beginDate
		});
	fields.push({
			"txt": "开始时间(Start)",
			"type": "dataTime",
			"searchName": "beginDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "开始时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "beginDate##@@##2"
		});
	   fields.push({
		    "searchName":"endDate",
			"type":"input",
			"txt":biolims.sampleTransport.endDate
		});
	fields.push({
			"txt": "结束时间(Start)",
			"type": "dataTime",
			"searchName": "endDate##@@##1",
			"mark": "s##@@##",
		},
		{
			"txt": "结束时间(End)",
			"type": "dataTime",
			"mark": "e##@@##",
			"searchName": "endDate##@@##2"
		});
	
	fields.push({
		"type":"table",
		"table":sampleTransportTable
	});
	return fields;
}
