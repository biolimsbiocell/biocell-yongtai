var sampleTransportTable;
var oldsampleTransportChangeLog;
/**
 * 列参数：data: 取值key title:表头 orderable：是否排序 visible:列显示或隐藏
 * createdCell：设置保存时发给后台的键 classname: edit:行内编辑 select：行内下拉框选择 date：行内日期选择
 */
$(function() {
	// 加载子表

	var tbarOpts = [];
	var colOpts = [];
	   colOpts.push({
		"data":"id",
		"title": biolims.sampleTransport.id,
		"createdCell": function(td) {
			$(td).attr("saveName", "id");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"name",
		"title": biolims.sampleTransport.name,
		"createdCell": function(td) {
			$(td).attr("saveName", "name");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "createUser-id",
		"title": "创建人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "createUser-id");
		}
	});
	colOpts.push( {
		"data": "createUser-name",
		"title": biolims.sampleTransport.createUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "createUser-name");
			$(td).attr("createUser-id", rowData['createUser-id']);
		}
	});
	   colOpts.push({
		"data":"createDate",
		"title": biolims.sampleTransport.createDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "createDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"state",
		"title": biolims.sampleTransport.state,
		"createdCell": function(td) {
			$(td).attr("saveName", "state");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"stateName",
		"title": biolims.sampleTransport.stateName,
		"createdCell": function(td) {
			$(td).attr("saveName", "stateName");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"note",
		"title": biolims.sampleTransport.note,
		"createdCell": function(td) {
			$(td).attr("saveName", "note");
	    },
		"className": "textarea"
	});
	colOpts.push( {
		"data": "transportUser-id",
		"title": "运输人ID",
		"createdCell": function(td) {
			$(td).attr("saveName", "transportUser-id");
		}
	});
	colOpts.push( {
		"data": "transportUser-name",
		"title": biolims.sampleTransport.transportUser,
		"createdCell": function(td, data, rowData) {
			$(td).attr("saveName", "transportUser-name");
			$(td).attr("transportUser-id", rowData['transportUser-id']);
		}
	});
	   colOpts.push({
		"data":"transportCompany",
		"title": biolims.sampleTransport.transportCompany,
		"createdCell": function(td) {
			$(td).attr("saveName", "transportCompany");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"trackingNumber",
		"title": biolims.sampleTransport.trackingNumber,
		"createdCell": function(td) {
			$(td).attr("saveName", "trackingNumber");
	    },
		"className": "edit"
	});
	   colOpts.push({
		"data":"beginDate",
		"title": biolims.sampleTransport.beginDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "beginDate");
	    },
		"className": "date"
	});
	   colOpts.push({
		"data":"endDate",
		"title": biolims.sampleTransport.endDate,
		"createdCell": function(td) {
			$(td).attr("saveName", "endDate");
	    },
		"className": "date"
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod != "view") {
	tbarOpts.push({
		text: biolims.common.fillDetail,
		action: function() {
			addItem($("#sampleTransportTable"))
	}
	});
	tbarOpts.push({
		text: biolims.common.addwindow,
		action: function() {
			addItemLayer($("#sampleTransportTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.editwindow,
		action: function() {
			editItemLayer($("#sampleTransportTable"))
		}
	});
	tbarOpts.push({
		text: biolims.common.batchUpload,
		action: function() {
			$("#uploadCsv").modal("show");
			$(".fileinput-remove").click();
			var csvFileInput = fileInputCsv("");
				csvFileInput.off("fileuploaded").on("fileuploaded", function(event, data, previewId, index) {
				$.ajax({
					type: "post",
					data: {
						id: $("#sampleTransport_id").val(),
						fileId: data.response.fileId
					},
					url: ctx + "/tra/sampletransport/sampleTransport/uploadCsvFile.action",
					success: function(data) {
						var data = JSON.parse(data)
						if(data.success) {
							 sampleTransportTable.ajax.reload();
						} else {
							layer.msg(biolims.common.uploadFailed)
						}
		
					}
				});
			});
			
		}
		});
	
	tbarOpts.push({
		text: biolims.common.save,
		action: function() {
			saveSampleTransport($("#sampleTransportTable"));
		}
	});
	}
	
	var sampleTransportOptions = 
	table(true, "","/tra/sampletransport/sampleTransport/showSampleTransportTableJson.action",
	 colOpts, tbarOpts)
	
	
	
	sampleTransportTable = renderData($("#sampleTransportTable"), sampleTransportOptions);
	sampleTransportTable.on('draw', function() {
		oldsampleTransportChangeLog = sampleTransportTable.ajax.json();
	});
	// 上一步下一步，子表打开注释一次
	//stepViewChange();
});

// 保存
function saveSampleTransport(ele) {
	var data = saveSampleTransportjson(ele);
	var ele=$("#sampleTransportTable");
	var changeLog = "实验结果：";
	changeLog = getChangeLog(data, ele, changeLog);
	$.ajax({
		type: 'post',
		url: '/tra/sampletransport/sampleTransport/saveSampleTransportTable.action',
		data: {
			dataJson: data,
			changeLog:changeLog
		},
		success: function(data) {
			var data = JSON.parse(data)
			if(data.success) {
				layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				layer.msg(biolims.common.saveFailed)
			};
		}
	})
}
// 获得保存时的json数据
function saveSampleTransportjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			// 判断并转换为数字
			
			if(k == "createUser-name") {
				json["createUser-id"] = $(tds[j]).attr("createUser-id");
				continue;
			}
			
			if(k == "transportUser-name") {
				json["transportUser-id"] = $(tds[j]).attr("transportUser-id");
				continue;
			}
		
			json[k] = $(tds[j]).text();
		}
		data.push(json);
	});
	return JSON.stringify(data);
}
function getChangeLog(data, ele, changeLog) {
	var saveJson = JSON.parse(data);
	saveJson.forEach(function(v, i) {
		var id = v.id;
		changeLog += '样本编号为"' + v.code + '":';
		oldsampleTransportChangeLog.data.forEach(function(vv, ii) {
			if(vv.id == id) {
				for(var k in v) {
					if(v[k] != vv[k]) {
						var title = ele.find("th[savename=" + k + "]").text();
						changeLog += '"' + title + '"由"' + vv[k] + '"变为"' + v[k] + '";';
					}
				}
				return false;
			}
		});
	});
	return changeLog;
}
