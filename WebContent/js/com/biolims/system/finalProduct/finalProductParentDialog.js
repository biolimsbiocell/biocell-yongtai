var productParentDialogGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'testTime',
		type:"string"
	});
	    fields.push({
		name:'productFee',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'personName',
		type:"string"
	});
	    fields.push({
		name:'parent-id',
		type:"string"
	});
	    fields.push({
		name:'parent-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'testTime',
		header:biolims.master.testTime,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'productFee',
		header:biolims.master.money,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowStateName,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'personName',
		header:biolims.master.personName,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'parent-id',
		header:biolims.master.parentId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'parent-name',
		header:biolims.master.parentName,
		width:15*10,
		sortable:true
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/com/biolims/system/finalProduct/showDialogProductParentListJson.action";
	var opts={};
	opts.title=biolims.master.productMasterData;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setProductFun(rec);
	};
	productParentDialogGrid=gridEditTable("show_dialog_product_parent_div",cols,loadParam,opts);
	$("#show_dialog_product_parent_div").data("productParentDialogGrid",productParentDialogGrid);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(productParentDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
