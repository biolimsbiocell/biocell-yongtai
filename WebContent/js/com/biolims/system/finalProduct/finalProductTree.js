function add(){
	window.location=window.ctx+'/com/biolims/system/finalProduct/editProduct.action';
}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/com/biolims/system/finalProduct/editProduct.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/com/biolims/system/finalProduct/viewProduct.action?id=' + id;
}

	Ext.onReady(function() {

		Ext.BLANK_IMAGE_URL = window.ctx+'/images/s.gif';
			Ext.QuickTips.init();
			function reloadtree() {
				var node = treeGrid.getSelectionModel().getSelectedNode();
				if(node==null){
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.getRootNode().expand(true);
					}, this);
				}else{
					var path = treeGrid.getSelectionModel().getSelectedNode().getPath('id');
					treeGrid.getLoader().load(treeGrid.getRootNode(), function(treeNode) {
						treeGrid.expandPath(path, 'id', function(bSucess, oLastNode) {
							treeGrid.getSelectionModel().select(oLastNode);
						});
					}, this);
				}
			}
			col = [ 
			
	   
	{
		header : biolims.common.id,
		dataIndex : 'id',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.name,
		dataIndex : 'name',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.master.testTime,
		dataIndex : 'testTime',
		width:20*6,
		hidden:false
	}, 
	
	   /* {
		header : '检测方法',
		dataIndex : 'workType-name',
		width:15*6,
		hidden:false
	}, */
	   
	{
		header : biolims.master.productFee,
		dataIndex : 'productFee',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.sample.createUserName,
		dataIndex : 'createUser-name',
		width:15*6,
		hidden:false
	}, 
	   /* {
		header : '创建日期',
		dataIndex : 'createDate-name',
		width:15*6,
		hidden:false
	}, */
	   
	{
		header : biolims.common.state,
		dataIndex : 'state',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.common.workFlowStateName,
		dataIndex : 'stateName',
		width:20*6,
		hidden:false
	}, 
	
	   
	{
		header : biolims.master.personName,
		dataIndex : 'personName',
		width:20*6,
		hidden:false
	}, 
	
	    {
		header : biolims.master.parentName,
		dataIndex : 'parent-name',
		width:15*6,
		hidden:false
	}, 
			
			
			
			{
				header : biolims.common.upId,
				width : 160,
				dataIndex : 'upId'
			}];
			var tbl = [];
			var treeGrid = new Ext.ux.tree.TreeGrid({
				id:'treeGrid',
				width:parent.document.body.clientWidth-50,
				height: parent.document.body.clientHeight-80,
				renderTo: 'markup',
				enableDD: true,
			
				columnLines:true,
				columns:col,
				root:new Ext.tree.AsyncTreeNode({  
		            id:'0',  
		            loader:new Ext.tree.TreeLoader({  
		                 dataUrl: $("#productTreePath").val(),  
		                 listeners:{  
		                     "beforeload":function(treeloader,node)  
		                     {  
		                        treeloader.baseParams={  
		                        treegrid_id:node.id,  
		                        method:'POST'  
		                        };  
		                     }  
		                 }    
		            })  
		        }),  
		       
				listeners: {
				click: function(node,event)  
	            {  
					document.getElementById("selectId").value=node.attributes.id;
					document.getElementById("leaf").value=node.attributes.leaf;
	                if (node.isLeaf()) {  
	                    event.stopEvent();  
	                } else {  
	                    event.stopEvent();  
	                    node.toggle();  
	                }  
	            }  ,
				dbclick:function(n){
					document.getElementById("selectId").value=n.attributes.id;
					document.getElementById("leaf").value=n.attributes.leaf;
					edit();
				}
			}
		 });
		//	Ext.getCmp('treeGrid').getRootNode().expand(true);
	});
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.master.listModel
		});
	item.on('click', lbms);
	
	});
function lbms(){
	var url = ctx+"/com/biolims/system/finalProduct/showProductList.action";
	location.href = url;
}
Ext.onReady(function(){
	var item1 = menu.add({
	    	text: biolims.master.treeModel
		});
	item1.on('click', szms);
	
	});
function szms(){
	var url = ctx+"/com/biolims/system/finalProduct/showProductTree.action";
	location.href = url;
}
