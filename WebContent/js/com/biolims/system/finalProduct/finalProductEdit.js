﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});

});
function add() {
	window.location = window.ctx
			+ "/com/biolims/system/finalProduct/editProduct.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx
			+ '/com/biolims/system/finalProduct/showProductList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave() {
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});
$("#toolbarbutton_tjsp").click(function() {
	submitWorkflow("#finalProduct", {
		userId : userId,
		userName : userName,
		formId : $("#finalProduct_id").val(),
		title : $("#finalProduct_name").val()
	}, function() {
		window.location.reload();
	});

});
$("#toolbarbutton_sp").click(function() {
	completeTask($("#finalProduct_id").val(), $(this).attr("taskId"), function() {
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
	});
});

function save() {
	if (checkSubmit() == true) {
		Ext.MessageBox.show({
			msg : biolims.common.savingData,
			progressText : biolims.common.saving,
			width : 300,
			wait : true,
			icon : 'ext-mb-download'
		});
		document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/com/biolims/system/finalProduct/save.action";
		form1.submit();

	}
}
function editCopy() {
	window.location = window.ctx
			+ '/com/biolims/system/finalProduct/copyProduct.action?id='
			+ $("#finalProduct_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#finalProduct_id").val() + "&tableId=finalProduct");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#finalProduct_id").val());
	nsc.push(biolims.common.IdEmpty);
//	fs.push($("#finalProduct_acceptUserGroup").val());
//	nsc.push("负责组不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
		message(mess);
		return false;
	}
	return true;
}
$(function() {
	Ext.onReady(function() {
		var tabs = new Ext.TabPanel({
			id : 'tabs11',
			renderTo : 'maintab',
			height : document.body.clientHeight - 30,
			autoWidth : true,
			activeTab : 0,
			margins : '0 0 0 0',
			items : [ {
				title : "终产品主数据",
				contentEl : 'markup'
			} ]
		});
	});
	var handlemethod = $("#handlemethod").val();
	if (handlemethod == "view") {
		settextreadonlyByAll();
	}
});

var item = menu.add({
	text : biolims.common.copy
});
item.on('click', editCopy);

function checkTestMethod() {
	var win = Ext.getCmp('testMethodFun');
	if (win) {
		win.close();
	}
	var testMethodFun = new Ext.Window(
			{
				id : 'testMethodFun',
				modal : true,
				title : biolims.sample.selectSequenceMethod,
				layout : 'fit',
				width : 500,
				height : 500,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='"
									+ window.ctx
									+ "/dic/type/dicTypeSelect.action?flag=test' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						testMethodFun.close();
					}
				} ]
			});
	testMethodFun.show();
}

function checkOrderBlock(){
//	var win = Ext.getCmp('orderBlockFun');
//	if (win) {
//		win.close();
//	}
//	var orderBlockFun= new Ext.Window({
//		id : 'orderBlockFun',
//		modal : true,
//		title : biolims.sample.selectOrderBlock,
//		layout : 'fit',
//		width : 500,
//		height : 500,
//		closeAction : 'close',
//		plain : true,
//		bodyStyle : 'padding:5px;',
//		buttonAlign : 'center',
//		collapsible : true,
//		maximizable : true,
//		items : new Ext.BoxComponent(
//				{
//					id : 'maincontent',
//					region : 'center',
//					html : "<iframe scrolling='no' name='maincontentframe' src='"
//							+ window.ctx
//							+ "/dic/type/dicTypeSelect.action?flag=orderBlock' frameborder='0' width='100%' height='100%' ></iframe>"
//				}),
//		buttons : [ {
//			text : biolims.common.close,
//			handler : function() {
//				orderBlockFun.close();
//			}
//		} ]
//	});
//	orderBlockFun.show();
	var win = Ext.getCmp('orderBlockFun');
	if (win) {
		win.close();
	}
	var orderBlockFun = new Ext.Window(
			{
				id : 'orderBlockFun',
				modal : true,
				title : biolims.sample.selectOrderBlock,
				layout : 'fit',
				width : 600,
				height : 600,
				closeAction : 'close',
				plain : true,
				bodyStyle : 'padding:5px;',
				buttonAlign : 'center',
				collapsible : true,
				maximizable : true,
				items : new Ext.BoxComponent(
						{
							id : 'maincontent',
							region : 'center',
							html : "<iframe scrolling='no' name='maincontentframe' src='/com/biolims/system/finalProduct/showProductOrderBlockTree.action?flag=OrderBlock' frameborder='0' width='100%' height='100%' ></iframe>"
						}),
				buttons : [ {
					text : biolims.common.close,
					handler : function() {
						orderBlockFun.close();
					}
				}]
			});
	orderBlockFun.show();
}

function setorderBlock(id,name){
	document.getElementById("finalProduct.orderBlockId").value = id;
	document.getElementById("finalProduct_orderBlock").value = name;
	var win = Ext.getCmp('orderBlockFun');
	if (win) {
		win.close();
	}
}

function settest(id, name) {
	document.getElementById("finalProduct_testMethod").value = id;
	document.getElementById("finalProduct_testMethod_name").value = name;
	var win = Ext.getCmp('testMethodFun');
	if (win) {
		win.close();
	}
}


var loadDicSampleType;
//查询样本类型
function loadTestDicSampleType(){
	 var options = {};
		options.width = document.body.clientWidth*0.8;
		options.height = document.body.clientHeight*0.8;
		loadDicSampleType=loadDialogPage(null, biolims.common.sampleType, "/sample/dicSampleType/dicSampleTypeSelect.action", {
			"确定(Confirm)" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
//				var records = sampleReceiveItemGrid.getSelectRecord();
				
				if (selectRecord.length > 0) {
//					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
//							obj.set("dicSampleType-id", b.get("id"));
//							obj.set("dicSampleType-name", b.get("name"));
//							obj.set("nextFlowId", "");
//							obj.set("nextFlow", "");
							document.getElementById("finalProduct_dicSampleType").value = b.get("id");
							document.getElementById("finalProduct_dicSampleType_name").value = b.get("name");
						});
//					});
				}else{
					message(biolims.common.selectYouWant);
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
}
function setDicSampleType(){

	var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
	var selectRecord = operGrid.getSelectionModel().getSelections();
//	var records = sampleReceiveItemGrid.getSelectRecord();
	
	if (selectRecord.length > 0) {
//		$.each(records, function(i, obj) {
			$.each(selectRecord, function(a, b) {
				document.getElementById("finalProduct_dicSampleType").value = b.get("id");
				document.getElementById("finalProduct_dicSampleType_name").value = b.get("name");
//				obj.set("dicSampleType-id", b.get("id"));
//				obj.set("dicSampleType-name", b.get("name"));
//				
//				obj.set("nextFlowId", "");
//				obj.set("nextFlow", "");
			});
//		});
	}else{
		message(biolims.common.selectYouWant);
		return;
	}
	loadDicSampleType.dialog("close");

}