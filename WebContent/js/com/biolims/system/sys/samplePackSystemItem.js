﻿
var samplePackSystemItemGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'name',
		type:"string"
	});
	   fields.push({
			name:'scope',
			type:"string"
		});
	   fields.push({
		name:'num',
		type:"string"
	});
	    fields.push({
		name:'samplePackSystem-id',
		type:"string"
	});
	    fields.push({
		name:'samplePackSystem-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:biolims.common.id,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'name',
		hidden : false,
		header:biolims.common.name,
		width:40*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'num',
		hidden : false,
		header:biolims.common.value,
		width:20*6,
		
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'scope',
		hidden : false,
		header:biolims.master.scope,
		width:20*6,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'samplePackSystem-id',
		hidden : true,
		header:biolims.common.relatedMainTableId,
		width:15*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'samplePackSystem-name',
		hidden : true,
		header:biolims.common.relatedMainTableName,
		width:15*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/com/biolims/system/sys/samplePackSystem/showSamplePackSystemItemListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title=biolims.sample.packagingSystemDetail;
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/com/biolims/system/sys/samplePackSystem/delSamplePackSystemItem.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				samplePackSystemItemGrid.getStore().commitChanges();
				samplePackSystemItemGrid.getStore().reload();
				message(biolims.common.deleteSuccess);
			} else {
				message(biolims.common.deleteFailed);
			}
		}, null);
	};
    
//	opts.tbar.push({
//			text : '选择相关主表',
//			handler : selectsamplePackSystemFun
//		});
	
	
	
	
	
	
	
	

	
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = samplePackSystemItemGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			
						
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			
						
							
							samplePackSystemItemGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	samplePackSystemItemGrid=gridEditTable("samplePackSystemItemdiv",cols,loadParam,opts);
	$("#samplePackSystemItemdiv").data("samplePackSystemItemGrid", samplePackSystemItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})

function selectsamplePackSystemFun(){
	var win = Ext.getCmp('selectsamplePackSystem');
	if (win) {win.close();}
	var selectsamplePackSystem= new Ext.Window({
	id:'selectsamplePackSystem',modal:true,title:biolims.common.selectRelevantTable,layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	html:"<iframe scrolling='no' name='maincontentframe' src='"+ctx+"/SamplePackSystemSelect.action?flag=samplePackSystem' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: biolims.common.close,
	 handler: function(){
		 selectsamplePackSystem.close(); }  }]  });     selectsamplePackSystem.show(); }
	function setsamplePackSystem(id,name){
		var gridGrid = $("#samplePackSystemItemdiv").data("samplePackSystemItemGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('samplePackSystem-id',id);
			obj.set('samplePackSystem-name',name);
		});
		var win = Ext.getCmp('selectsamplePackSystem')
		if(win){
			win.close();
		}
	}
	
