﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/com/biolims/system/sys/samplePackSystem/editSamplePackSystem.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/com/biolims/system/sys/samplePackSystem/showSamplePackSystemList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	var productId = $("#samplePackSystem_product").val();
	if (!productId) {
		message(biolims.wk.productIdIsEmpty);
		return;
	}
	
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#samplePackSystem", {
					userId : userId,
					userName : userName,
					formId : $("#samplePackSystem_id").val(),
					title : $("#samplePackSystem_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#samplePackSystem_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    var samplePackSystemItemDivData = $("#samplePackSystemItemdiv").data("samplePackSystemItemGrid");
		document.getElementById('samplePackSystemItemJson').value = commonGetModifyRecords(samplePackSystemItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/com/biolims/system/sys/samplePackSystem/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/com/biolims/system/sys/samplePackSystem/copySamplePackSystem.action?id=' + $("#samplePackSystem_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#samplePackSystem_id").val() + "&tableId=samplePackSystem");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#samplePackSystem_id").val());
	nsc.push(biolims.common.IdEmpty);
	fs.push($("#samplePackSystem_product").val());
	nsc.push(biolims.wk.productIdIsEmpty);
	
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.sample.packagingSystem,
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/com/biolims/system/sys/samplePackSystem/showSamplePackSystemItemList.action", {
				id : $("#samplePackSystem_id").val()
			}, "#samplePackSystemItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);
	
	function showsampleTypeFun(){
		var win = Ext.getCmp('showsampleTypeFun');
		if (win) {win.close();}
		var showsampleTypeFun= new Ext.Window({
		id:'showsampleTypeFun',modal:true,title:biolims.common.selectSampleType,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=yblx' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text:biolims.common.close,
		 handler: function(){
			 showsampleTypeFun.close(); }  }]  });     showsampleTypeFun.show(); }
		 function setyblx(id,name){
		 document.getElementById("samplePackSystem_sampleType").value = id;
		document.getElementById("samplePackSystem_sampleType_name").value = name;
		var win = Ext.getCmp('showsampleTypeFun')
		if(win){win.close();}
		}
	 function showbloodTubeFun(){
			var win = Ext.getCmp('showbloodTubeFun');
			if (win) {win.close();}
			var showbloodTubeFun= new Ext.Window({
			id:'showbloodTubeFun',modal:true,title:biolims.plasma.selectBloodTube,layout:'fit',width:500,height:500,closeAction:'close',
			plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
			collapsible: true,maximizable: true,
			items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
			html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?flag=bloodTube' frameborder='0' width='100%' height='100%' ></iframe>"}),
			buttons: [
			{ text: biolims.common.close,
			 handler: function(){
				 showbloodTubeFun.close(); }  }]  });     showbloodTubeFun.show(); }
			 function setbloodTube(id,name){
			 document.getElementById("samplePackSystem_bloodTube").value = id;
			document.getElementById("samplePackSystem_bloodTube_name").value = name;
			var win = Ext.getCmp('showbloodTubeFun')
			if(win){win.close();}
			}	 
		 
 function ProductFun(){
		var win = Ext.getCmp('productFun');
		if (win) {win.close();}
		var productFun= new Ext.Window({
		id:'productFun',modal:true,title:biolims.pooling.selectBusinessType,layout:'fit',width:500,height:500,closeAction:'close',
		plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
		collapsible: true,maximizable: true,
		items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
		html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
		buttons: [
		{ text: biolims.common.close,
		 handler: function(){
			 productFun.close(); }  }]  });     productFun.show(); }

function setProductFun(id,name){
		var productName = "";
		ajax(
				"post",
				"/com/biolims/system/product/findProductToSample.action",
				{
					code : id,
				},
				function(data) {

					if (data.success) {
						$.each(data.data, function(i, obj) {
							productName += obj.name + ",";
						});
						document.getElementById("samplePackSystem_product").value = id;
						document.getElementById("samplePackSystem_product_name").value = productName;
					}
				}, null);
		var win = Ext.getCmp('productFun');
		if(win){win.close();}
	}

		 
		 
		 
		 
