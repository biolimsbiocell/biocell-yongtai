var samplePackSystemDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'sampleType-id',
		type:"string"
	});
	    fields.push({
		name:'sampleType-name',
		type:"string"
	});
    fields.push({
		name:'bloodTube-id',
		type:"string"
	});
	    fields.push({
		name:'bloodTube-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType-id',
		header:'样本类型ID',
		width:15*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType-name',
		header:biolims.common.sampleType,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'bloodTube-id',
		header:biolims.plasma.bloodTubeId,
		width:15*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'bloodTube-name',
		header:biolims.plasma.bloodTube,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:40*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/com/biolims/system/sys/samplePackSystem/showSamplePackSystemListJson.action";
	var opts={};
	opts.title=biolims.sample.packagingSystem;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setSamplePackSystemFun(rec);
	};
	samplePackSystemDialogGrid=gridTable("show_dialog_samplePackSystem_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(samplePackSystemDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
