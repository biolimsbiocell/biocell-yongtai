/* 
 * 文件描述: 单位组的弹框
 * 
 */
var options;
$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.name,
	});
	colOpts.push({
		"data": "cycle",
		"title": biolims.pooling.transformNum,
	});
	colOpts.push({
		"data": "mark-name",
		"title": "主单位",
	});
	colOpts.push({
		"data": "mark2-name",
		"title": "副单位",
	});
	colOpts.push({
		"data": "state",
		"title": biolims.common.state,
		"name": biolims.master.valid+"|"+biolims.master.invalid,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.master.valid;
			}else if(data == "0") {
				return biolims.master.invalid;
			}else {
				return '';
			}
		}
	});
	var tbarOpts = [];
	var state=$("#state").val();
	var unit=$("#arr").val();
	debugger
	if(state=="state"){
		options = table(false, null,
				'/com/biolims/system/work/unitGroupNew/showWorkTypeDialogListNewJsonByUnit.action?unit='+unit, colOpts, tbarOpts)
	}else{
		options = table(false, null,
				'/com/biolims/system/work/unitGroupNew/showWorkTypeDialogListNewJson.action', colOpts, tbarOpts)
	}
	
	
	var addUnitGroup = renderData($("#addUnitGroup"), options);
	$("#addUnitGroup").on(
		'init.dt',
		function(e, settings) {
			// 清除操作按钮
			$("#addUnitGroup_wrapper .dt-buttons").empty();
			$('#addUnitGroup_wrapper').css({
				"padding": "0 16px"
			});
			var trs = $("#addUnitGroup tbody tr");
			addUnitGroup.ajax.reload();
			addUnitGroup.on('draw', function() {
				trs = $("#addUnitGroup tbody tr");
				trs.click(function() {
					$(this).addClass("chosed").siblings("tr")
						.removeClass("chosed");
				});
			});
			trs.click(function() {
				$(this).addClass("chosed").siblings("tr")
					.removeClass("chosed");
			});
		});

})