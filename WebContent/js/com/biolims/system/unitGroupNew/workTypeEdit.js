﻿
//$(function() {

//	$("#tabs").tabs({
//		select : function(event, ui) {
//		}
//	});
//})	
var mainFileInput=fileInput ('dwz','unitGroupNew',$("#unitGroupNew_id").val().trim());

function add() {
	window.location = window.ctx + "/com/biolims/system/work/unitGroupNew/editWorkType.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/com/biolims/system/work/unitGroupNew/showWorkTypeList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
//$("#toolbarbutton_tjsp").click(function() {
//				submitWorkflow("#workType", {
//					userId : userId,
//					userName : userName,
//					formId : $("#workType_id").val(),
//					title : $("#workType_name").val()
//				}, function() {
//					window.location.reload();
//				});
//				
//});
//$("#toolbarbutton_sp").click(function() {
//		completeTask($("#workType_id").val(), $(this).attr("taskId"), function() {
//			document.getElementById('toolbarSaveButtonFlag').value = 'save';
//			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
//		});
//});






function save() {
	var changeLog = "";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	var myreg = /^[1-9]\d*$/;
	if(!$("#unitGroupNew_cycle").val().match(myreg)){
		top.layer.msg("转换系数只能输入整数！");
		return false;
	}
	
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	document.getElementById("changeLog").value = changeLog;
	
	
	if(checkSubmit() == true) {
		form1.action = window.ctx +
			"/com/biolims/system/work/unitGroupNew/save.action";
		form1.submit();
	}
}		
function editCopy() {
	window.location = window.ctx + '/com/biolims/system/work/workType/copyWorkType.action?id=' + $("#workType_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#workType_id").val() + "&tableId=workType");
}
function checkSubmit() {
	if($("#unitGroupNew_id").val() == null || $("#unitGroupNew_id").val() == "") {
		top.layer.msg(biolims.common.IdEmpty);
		return false;
	};
	return true;
}
$(function() {
//Ext.onReady(function(){
//	var tabs=new Ext.TabPanel({
//		   id:'tabs11',
//	       renderTo:'maintab',
//	       height:document.body.clientHeight-30,
//	       autoWidth:true,
//	       activeTab:0,
//	       margins:'0 0 0 0',
//	       items:[{
//	    	   title:biolims.common.sequencingFun,
//	    	   contentEl:'markup'
//	       } ]
//	   });
//});
//load("/com/biolims/system/work/workType/showWorkTypeItemList.action", {
//				id : $("#workType_id").val()
//			}, "#workTypeItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonly();
}
});
//批量单位1
function unitBatch() {
	top.layer.open({
		title: biolims.common.batchUnit,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/unit/showUnitSelectTable.action", ''],
		yes: function(index, layero) {
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td")
				.eq(0).attr("id");
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td")
			.eq(0).text();
			$("#unitGroupNew_mark_id").val(type);
			$("#unitGroupNew_mark_name").val(name);
			top.layer.close(index)
		},
	})

}

function fileUp(){
	if($("#unitGroupNew_id").val()==""||$("#unitGroupNew_id").val()==null||$("#unitGroupNew_id").val()=="NEW"){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
function fileView(){
	top.layer.open({
		title:biolims.common.attachment,
		type:2,
		skin: 'layui-top.layer-lan',
		area:[document.body.clientWidth-300,document.body.clientHeight-100],
		content:window.ctx+"/operfile/initFileList.action?flag=dwz&modelType=unitGroupNew&id="+$("#unitGroupNew_id").val().trim(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
//批量单位2
function unitBatch2() {
	top.layer.open({
		title: biolims.common.batchUnit,
		type: 2,
		area: [document.body.clientWidth - 300, document.body.clientHeight - 100],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/unit/showUnitSelectTable.action", ''],
		yes: function(index, layero) {
			var type = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td")
				.eq(0).attr("id");
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUnitTable .chosed").children("td")
			.eq(0).text();
			$("#unitGroupNew_mark2_id").val(type);
			$("#unitGroupNew_mark2_name").val(name);
			top.layer.close(index)
		},
	})

}