var techServiceTaskGrid;
$(function() {
	
//	if($("#handlemethod").val() == "view") {
//		settextreadonly();
//		$("#item_input").hide();
//	}
	var options = table(true, "",
			"/com/biolims/system/work/unitGroupNew/showWorkTypeListNewJson.action",
			[ {
				"data" : "id",
				"title" : biolims.common.id,
			}, {
				"data" : "name",
				"title" : biolims.common.name,
			}, {
				"data" : "cycle",
				"title" : biolims.pooling.transformNum,
			}
			, {
				"data" : "mark-name",
				"title" : biolims.unitGroupNew.primaryUnit,
			}, {
				"data" : "mark2-name",
				"title" : biolims.unitGroupNew.deputyUunit,
			}, {
				"data": "state",
				"title": biolims.common.state,
				"name": biolims.master.valid+"|"+biolims.master.invalid,
				"render": function(data, type, full, meta) {
					if(data == "1") {
						return biolims.master.valid;
					}else if(data == "0") {
						return biolims.master.invalid;
					}else {
						return '';
					}
				}
			} ], null)
	techServiceTaskGrid = renderRememberData($("#main"), options);
	$('#main').on('init.dt', function() {
		recoverSearchContent(techServiceTaskGrid);
	})
});

function add() {
	window.location = window.ctx
			+ '/com/biolims/system/work/unitGroupNew/editWorkType.action';
}
function edit() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
			+ '/com/biolims/system/work/unitGroupNew/editWorkType.action?id=' + id;
}
    function view() {
	var id = $(".selected").find("input").val();
	
	if (id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx
	+ '/com/biolims/system/work/unitGroupNew/viewWorkType.action?id=' + id;
    }
//	$("#maincontentframe", window.parent.document)[0].src = window.ctx
//			+ "/technology/wk/techJkServiceTask/viewTechJkServiceTask.action?id="
//			+ id+"&type="+$("#type").val();
//}
// 弹框模糊查询参数
function searchOptions() {
	return [ {
		"txt" : biolims.common.id,
		"type" : "input",
		"searchName" : "id",
	}, {
		"txt" : biolims.common.name,
		"type" : "input",
		"searchName" : "name",
	},  {
		"searchName" : "mark.name",
		"type" : "input",
		"txt" : biolims.unitGroupNew.primaryUnit,
	}, {
		"searchName" : "mark2.name",
		"type" : "input",
		"txt" : biolims.unitGroupNew.deputyUunit,
	}, {
		"type" : "table",
		"table" : techServiceTaskGrid
	} ];
}
