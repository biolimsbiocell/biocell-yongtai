﻿var applicationTypeGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'pathName',
		type : "string"
	});
	fields.push({
		name : 'applicationType-id',
		type : "string"
	});
	fields.push({
		name : 'classPath',
		type : "string"
	});
	fields.push({
		name : 'workflowUserColumn',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.master.formName,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'pathName',
		header : biolims.common.path,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'applicationType-id',
		header : biolims.master.applicationTypeId,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		}) 
	});
	cm.push({
		dataIndex : 'classPath',
		header : biolims.master.classPath,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'workflowUserColumn',
		header : biolims.master.workflowUserColumn,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'state',
		header : biolims.common.state,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/com/biolims/system/work/workOrder/showApplicationTypeTableListJson.action";
	var opts = {};
	opts.width = 550;
	opts.height = 490;
	applicationTypeGrid = gridTable("show_dialog_applicationtypetable_grid_div", cols, loadParam, opts);
	$("#show_dialog_applicationtypetable_grid_div").data("waitApplicationtypeApplyGrid", applicationTypeGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
});

function sc(){
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"), biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(applicationTypeGrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();
		}
	}, true, option);
}
