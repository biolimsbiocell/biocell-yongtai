﻿var workOrderItemGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'nextStepId',
		type : "string"
	});
	fields.push({
		name : 'nextStepName',
		type : "string"
	});
	fields.push({
		name : 'note',
		type : "string"
	});
	fields.push({
		name : 'workOrder-id',
		type : "string"
	});
	fields.push({
		name : 'workOrder-name',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-id',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-name',
		type : "string"
	});
	fields.push({
		name : 'sort',
		type : "string"
	});
	fields.push({
		name : 'dnextId',
		type : "string"
	});
	fields.push({
		name : 'dnextName',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		hidden : true,
		header : biolims.common.taskDetailId,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		hidden : false,
		header : biolims.common.testName,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'note',
		hidden : false,
		header : biolims.common.note,
		width : 20 * 6,

		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	cm.push({
		dataIndex : 'applicationTypeTable-id',
		hidden : true,
		header : biolims.common.experimentModuleId,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'applicationTypeTable-name',
		hidden : false,
		header : biolims.common.experimentModule,
		width : 200
	});

	cm.push({
		dataIndex : 'nextStepId',
		hidden : true,
		header : biolims.common.nextStepId,
		width : 150
	});
	cm.push({
		dataIndex : 'nextStepName',
		hidden : false,
		header : biolims.common.nextStepName,
		width : 500
	});

	cm.push({
		dataIndex : 'dnextId',
		hidden : true,
		header : biolims.sample.dnextId,
		width : 150
	});
	cm.push({
		dataIndex : 'dnextName',
		hidden : false,
		header : biolims.sample.dnextName,
		width : 20 * 6
	});

	cm.push({
		dataIndex : 'workOrder-id',
		hidden : true,
		header : biolims.common.relatedMainTableId,
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'workOrder-name',
		hidden : true,
		header : biolims.common.relatedMainTableName,
		width : 50
	});
	cm.push({
		dataIndex : 'sort',
		hidden : false,
		header : biolims.common.orderNumber,
		width : 50,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx
			+ "/com/biolims/system/work/workOrder/showWorkOrderItemListJson.action?id="
			+ $("#id_parent_hidden").val();
	var opts = {};
	opts.title = biolims.common.commonTemplate;
	opts.height = document.body.clientHeight - 100;
	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post",
				"/com/biolims/system/work/workOrder/delWorkOrderItem.action", {
					ids : ids
				}, function(data) {
					if (data.success) {
						workOrderItemGrid.getStore().commitChanges();
						workOrderItemGrid.getStore().reload();
						message(biolims.common.deleteSuccess);
					} else {
						message(biolims.common.deleteFailed);
					}
				}, null);
	};

	opts.tbar.push({
		text : biolims.common.selectTest,
		handler : selectTable
	});

	opts.tbar.push({
		text : biolims.common.selectNextFlow,
		handler : selectnextStepNameTable
	});

	opts.tbar.push({
		text : biolims.sample.selectNextStep,
		handler : selectdnext
	});

	opts.tbar.push({
		text : biolims.common.editableColAppear,
		handler : null
	});
	opts.tbar.push({
		text : biolims.common.uncheck,
		handler : null
	});
	workOrderItemGrid = gridEditTable("workOrderItemdiv", cols, loadParam, opts);
	$("#workOrderItemdiv").data("workOrderItemGrid", workOrderItemGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
});

function selectTable() {
	var title = '';
	var url = '';
	title = biolims.common.selectExperimentModule;
	url = "/com/biolims/system/work/workOrder/showDialogApplicationTypeTableList.action";
	var option = {};
	option.width = 600;
	option.height = 600;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			selVal(this);
		}
	}, true, option);
}
var selVal = function(win) {
	var operGrid = $("#show_dialog_applicationtypetable_grid_div").data(
			"waitApplicationtypeApplyGrid");
	var selRecords = operGrid.getSelectionModel().getSelections();
	var id = "";
	var name = "";
	$.each(selRecords, function(i, obj) {
		id = obj.get("id");
		name = obj.get("name");
	});

	var selectRecord = workOrderItemGrid.getSelectionModel().getSelections();

	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {

			obj.set('applicationTypeTable-id', id);
			obj.set('applicationTypeTable-name', name);

		});
		$(win).dialog("close");
	} else {
		message(biolims.common.selectYouWant);
		return;
	}
};

function selectnextStepNameTable() {
	var title = '';
	var url = '';
	title = biolims.common.selectNextFlow;
	var pId = [];
	var selectRecord = workOrderItemGrid.getSelectionModel().getSelections();
	$.each(selectRecord, function(i, obj) {
		pId.push(obj.get('nextStepId'));
		// pId += obj.get('nextStepId') +",";

	});

	url = "/system/nextFlow/nextFlow/showDialogWorkOrderNextFlowList.action?pId="
			+ pId;
	var option = {};
	option.width = 650;
	option.height = 560;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			selVal1(this);
		}
	}, true, option);
}
var selVal1 = function(win) {
	var operGrid = $("#show_dialog_nextflow_grid_div").data("showNextFlowGrid");
	var selRecords = operGrid.getSelectionModel().getSelections();
	var id = [];
	var name = [];
	$.each(selRecords, function(i, obj) {
		id.push(obj.get("id"));
		name.push(obj.get("name"));
		// id = id + obj.get("id") +"," ;
		// name = name + obj.get("name") + ",";
	});

	var selectRecord = workOrderItemGrid.getSelectionModel().getSelections();
	if (selectRecord.length > 0) {
		$.each(selectRecord, function(i, obj) {

			obj.set('nextStepId', id.toString());
			obj.set('nextStepName', name.toString());

		});
		$(win).dialog("close");
	} else {
		message(biolims.common.selectYouWant);
		return;
	}
};

// 选择默认下一步流向
function selectdnext() {
	var title = '';
	var url = '';
	title = biolims.sample.selectNextStep;
	url = "/system/nextFlow/nextFlow/showDialogWorkOrderNextFlowList.action";
	var option = {};
	option.width = 650;
	option.height = 560;
	loadDialogPage(null, title, url, {
		"Confirm" : function() {
			selVal2(this);
		}
	}, true, option);
}
var selVal2 = function(win) {
	var operGrid = $("#show_dialog_nextflow_grid_div").data("showNextFlowGrid");
	var selRecords = operGrid.getSelectionModel().getSelections();
	if(selRecords.length>0){
		var id=[];
		var name=[];
		$.each(selRecords, function(i, obj) {
			id.push(obj.get("id"));
			name.push(obj.get("name"));
		});
		
		var selectRecord = workOrderItemGrid.getSelectionModel().getSelections();
			if (selectRecord.length > 0) {
				$.each(selectRecord, function(i, obj) {
					obj.set('dnextId',id.toString());
					obj.set('dnextName',name.toString());
					
				});
				$(win).dialog("close");
			} else {
				message(biolims.common.selectYouWant);
				return;
			}
	}else{
		message("请选择数据！");
	}

};
