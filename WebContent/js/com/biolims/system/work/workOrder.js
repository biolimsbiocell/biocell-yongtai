var workOrderGrid;
$(function(){
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
		"visible": false,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.common.name,
	});
	colOpts.push({
		"data": "createUser-id",
		"title": biolims.sample.createUserId,
		"visible": true,
	});
	colOpts.push({
		"data": "createUser-name",
		"title": biolims.sample.createUserName,
	});
	colOpts.push({
		"data": "createDate",
		"title": biolims.sample.createDate,
		"visible": true,
	});
	colOpts.push({
		"data": "productId",
		"title": biolims.wk.productId,
	});
	colOpts.push({
		"data": "productName",
		"title": biolims.wk.productName,
	});
	colOpts.push({
		"data": "stateName",
		"title": biolims.common.workFlowStateName,
	});
	var options = table(true, null, "/com/biolims/system/work/workOrder/showWorkOrderListJson.action", colOpts, null);
	workOrderTab = renderRememberData($("#main"), options);
});
function add(){
		window.location=window.ctx+'/com/biolims/system/work/workOrder/editWorkOrder.action';
	}
function edit(){
	var id = $(".selected").find("input").val();
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/com/biolims/system/work/workOrder/editWorkOrder.action?id=' + id;
}
function view() {
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/com/biolims/system/work/workOrder/viewWorkOrder.action?id=' + id;
}

//弹框模糊查询参数
function searchOptions() {
var fields=[];
/*	   fields.push({
		    "searchName":"id",
			"type":"input",
			"txt": biolims.common.id
		});*/
	   fields.push({
		    "searchName":"name",
			"type":"input",
			"txt": biolims.common.name
		});
	   fields.push({
		    "searchName":"createUser-name",
			"type":"input",
			"txt": biolims.sample.createUserName
		});
	   fields.push({
		    "searchName":"productName",
			"type":"input",
			"txt": biolims.wk.productName,
		});
	   fields.push({
			"txt": biolims.tStorage.createDate+"(Start)",
			"type": "dataTime",
			"searchName": "createDate##@@##1",
			"mark": "s##@@##",
		});
	   fields.push({
			"txt": biolims.tStorage.createDate+"(End)",
			"type": "dataTime",
			"searchName": "createDate##@@##2",
			"mark": "e##@@##",
		});
	fields.push({
		"type":"table",
		"table":workOrderTab
	});
	return fields;
}