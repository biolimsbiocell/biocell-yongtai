var workTypeGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'cycle-id',
		type:"string"
	});
	    fields.push({
		name:'cycle',
		type:"string"
	});
	    fields.push({
		name:'parent-id',
		type:"string"
	});
	    fields.push({
		name:'parent-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
//	    fields.push({
//			name:'state-name',
//			type:"string"
//		});
	    fields.push({
			name:'person-id',
			type:"string"
		});
		    fields.push({
			name:'person-name',
			type:"string"
		});
		    fields.push({
			name:'mark',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:30*6,
		
		sortable:true
	});
//		cm.push({
//		dataIndex:'cycle-id',
//		hidden:true,
//		header:'检测周期ID',
//		width:20*10,
//		sortable:true
//		});
	cm.push({
		dataIndex:'cycle',
		header:biolims.master.testTime,
		
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'parent-id',
		hidden:true,
		header:biolims.master.parentIdId,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'parent-name',
		header:biolims.master.parentId,
		
		width:15*10,
		sortable:true
	});

	cm.push({
		dataIndex:'person-id',
		hidden:true,
		header:'负责人ID',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'person-name',
		header:biolims.master.personName,
		
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'mark',
		header:'标识码',
		width:20*6,
		
		sortable:true
	});
	var statestore = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '1', biolims.master.valid ], [ '0', biolims.master.invalid]]
	});
	
	var stateComboxFun = new Ext.form.ComboBox({
		store : statestore,
		displayField : 'name',
		valueField : 'id',
		typeAhead : true,
		mode : 'local',
		forceSelection : true,
		triggerAction : 'all',
		emptyText : '',
		selectOnFocus : true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*6,
		
		renderer: Ext.util.Format.comboRenderer(stateComboxFun),				
		sortable:true
	});
//	cm.push({
//		dataIndex:'state',
//		header:'状态',
//		width:40*6,
//		
//		sortable:true
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/com/biolims/system/work/workType/showWorkTypeListJson.action";
	var opts={};
	opts.title=biolims.wk.productName;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	workTypeGrid=gridTable("show_workType_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/com/biolims/system/work/workType/editWorkType.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/com/biolims/system/work/workType/editWorkType.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/com/biolims/system/work/workType/viewWorkType.action?id=' + id;
}
function exportexcel() {
	workTypeGrid.title = biolims.common.exportList;
	var vExportContent = workTypeGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				
				commonSearchAction(workTypeGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.master.listModel
		});
	item.on('click', lbms);
	
	});
function lbms(){
	var url = ctx+"/com/biolims/system/work/workType/showWorkTypeList.action";
	location.href = url;
}
Ext.onReady(function(){
	var item1 = menu.add({
	    	text: biolims.master.treeModel
		});
	item1.on('click', szms);
	});
function szms(){
	var url = ctx+"/com/biolims/system/work/workType/showWorkTypeTree.action";
	location.href = url;
}
