﻿var showNextFlowGrid;
$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];
	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'name',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-id',
		type : "string"
	});
	fields.push({
		name : 'applicationTypeTable-name',
		type : "string"
	});
	cols.fields = fields;
	var cm = [];
	cm.push({
		dataIndex : 'id',
		header : biolims.common.id,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'name',
		header : biolims.common.nextFlow,
		width : 120,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	
	cm.push({
		dataIndex : 'applicationTypeTable-id',
		header : biolims.master.relatedTableId,
		width : 150,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		}) 
	});
	
	cm.push({
		dataIndex : 'applicationTypeTable-name',
		header : biolims.master.relatedTableName,
		width : 150,
		sortable : true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/system/nextFlow/nextFlow/showDialogWorkOrderNextFlowListJson.action";
	loadParam.limit=999;
	var opts = {};
	opts.width = 600;
	opts.height = 430;
	showNextFlowGrid = gridTable("show_dialog_nextflow_grid_div", cols, loadParam, opts);
	$("#show_dialog_nextflow_grid_div").data("showNextFlowGrid", showNextFlowGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	
	setTimeout(function() {
		var store1 = showNextFlowGrid.store;
		var gridCount1 = store1.getCount();
		for ( var ij1 = 0; ij1 < gridCount1; ij1++) {
			var record1 = store1.getAt(ij1);
			if($("#selectIds").val().indexOf(record1.get('id'))>=0){
				showNextFlowGrid.getSelectionModel().selectRow(ij1,true);	
			}
			
		}

		},1000);


});

function sc(){
	var option = {};
	option.width = 542;
	option.height = 417;
	loadDialogPage($("#jstj"),biolims.common.search, null, {
		"开始检索(Start retrieve)" : function() {
			commonSearchAction(showNextFlowGrid);
			$(this).dialog("close");
		},
		"清空(Empty)" : function() {
			form_reset();
		}
	}, true, option);
}
