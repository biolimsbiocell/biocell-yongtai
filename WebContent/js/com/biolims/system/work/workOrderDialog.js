$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.user.eduName1,
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/com/biolims/system/work/workOrder/showDialogWorkOrderListJson.action',colOpts , tbarOpts)
		var workOrderTable = renderData($("#addWorkOrder"), options);
	$("#addWorkOrder").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addWorkOrder_wrapper .dt-buttons").empty();
		$('#addWorkOrder_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addWorkOrder tbody tr");
	workOrderTable.ajax.reload();
	workOrderTable.on('draw', function() {
		trs = $("#addWorkOrder tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

