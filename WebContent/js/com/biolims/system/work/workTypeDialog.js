var workTypeDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'cycle-id',
		type:"string"
	});
	    fields.push({
		name:'cycle-name',
		type:"string"
	});
	    fields.push({
		name:'parent-id',
		type:"string"
	});
	    fields.push({
		name:'parent-name',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
			name:'person-id',
			type:"string"
		});
		    fields.push({
			name:'person-name',
			type:"string"
		});
		    fields.push({
			name:'mark',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
		cm.push({
		dataIndex:'cycle-id',
		header:biolims.master.testTimeId,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'cycle-name',
		header:biolims.master.testTime,
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'parent-id',
		header:biolims.master.parentIdId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'parent-name',
		header:biolims.master.parentId,
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'person-id',
		hidden:true,
		header:biolims.master.personId,
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'person-name',
		header:biolims.master.personName,
		
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'mark',
		header:biolims.master.mark,
		width:40*6,
		
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/com/biolims/system/work/workType/showWorkTypeListJson.action";
	var opts={};
	opts.title=biolims.common.sequencingFun;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setWorkTypeFun(rec);
	};
	workTypeDialogGrid=gridTable("show_dialog_workType_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(workTypeDialogGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
