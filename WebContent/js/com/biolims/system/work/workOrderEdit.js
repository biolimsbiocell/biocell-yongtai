﻿var workOrderItemGrid
var colOpts = [];
colOpts.push({
	"data": "id",
	"title": biolims.common.taskDetailId,
	"visible": false,
	"createdCell": function(td) {
		$(td).attr("saveName", "id");
	}
})
colOpts.push({
	"data": "name",
	"title": biolims.common.stepName,
	"createdCell": function(td) {
		$(td).attr("saveName", "name");
	}
})
colOpts.push({
	"data": "note",
	"title": biolims.common.note,
	"width": "120px",
	"createdCell": function(td, data) {
		$(td).attr("saveName", "note");
	},
})
colOpts.push({
	"data": "applicationTypeTable-id",
	"title": biolims.common.experimentModuleId,
	"createdCell": function(td, data, rowData) {
		$(td).attr("saveName", "applicationTypeTable-id");
	}
})
colOpts.push({
	"data": "applicationTypeTable-name",
	"title": biolims.common.experimentModule,
	"createdCell": function(td, data, rowData) {
		$(td).attr("saveName", "applicationTypeTable-name");
		$(td).attr("applicationTypeTable-id", rowData['applicationTypeTable-id']);
	}
})
colOpts.push({
	"data": "nextStepId",
	"title": biolims.common.nextStepId,
	"createdCell": function(td, data) {
		$(td).attr("saveName", "nextStepId");
	},
})
colOpts.push({
	"data": "dnextId",
	"title": biolims.sample.dnextId,
	"createdCell": function(td, data) {
		$(td).attr("saveName", "dnextId");
	},
})
colOpts.push({
	"data": "dnextName",
	"title": biolims.sample.dnextName,
	"createdCell": function(td, data) {
		$(td).attr("saveName", "dnextName");
	},
})
colOpts.push({
	"data": "process",
	"title": "是否过程中",
	"className":"select",
	"name":biolims.common.yes+"|"+biolims.common.no,
	"createdCell": function(td) {
		$(td).attr("saveName", "process");
		$(td).attr("selectOpt", biolims.common.yes+"|"+biolims.common.no);
	},
	"render": function(data, type, full, meta) {
		if(data == "1") {
			return biolims.common.yes;
		}
		if(data == "0") {
			return biolims.common.no;
		}else{
			return "";
		}
	}
})
colOpts.push({
	"data": "workOrder-id",
	"title": biolims.common.relatedMainTableId,
	"visible": false,
	"createdCell": function(td) {
		$(td).attr("saveName", "workOrder-id");
	}
})
var tbarOpts = [];
tbarOpts.push({
	text: biolims.common.lookApprovalProcess,
	action: function() {
		viewWorkflow();
	}
})
tbarOpts.push({
	text: biolims.common.save,
	action: function() {
		saveItem();
	}
})
tbarOpts.push({
	text: biolims.sample.dnextName,
	action: function() {
		chosedNextFlow();
	}
})
var options = table(
	true,
	$("#workOrder_id").text(),
	"/com/biolims/system/work/workOrder/showWorkOrderItemTableJson.action",
	colOpts, tbarOpts);
workOrderItemGrid = renderData($("#workOrderItem"),
		options);
function viewWorkflow(){
	var rows = $("#workOrderItem .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	var tableId="";
	var tableName="";
	$.each(rows, function(j, k) {
		tableId = $(k).find("td[savename='applicationTypeTable-id']").text();
		tableName=$(k).find("td[savename='applicationTypeTable-name']").text();
	});
	$("#maincontentframe", window.parent.document)[0].src = window.ctx + "/workflow/process/viewWorkflowProcess.action?tableId=" + tableId+"&tableName="+tableName;
}
//获得保存时的json数据
function saveItemjson(ele) {
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			// 判断男女并转换为数字
			if(k == "process") {
				var process = $(tds[j]).text();
				if(process == biolims.common.no) {
					json[k] = "0";
				} else if(process == biolims.common.yes) {
					json[k] = "1";
				}else{
					json[k] = "";
				} 
				continue;
			}
			
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

//保存
function saveItem(){
	var itemJson = saveItemjson($("#workOrderItem"));
	top.layer.load(4, {shade:0.3});
	$.ajax({
		type: 'post',
		url: '/com/biolims/system/work/workOrder/saveItem.action',
		data: {
			id: $("#workOrder_id").text(),
			itemJson: itemJson
		},
		success: function(data) {
			var data = JSON.parse(data);
			if(data.success) {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveSuccess);
				tableRefresh();
			} else {
				top.layer.closeAll();
				top.layer.msg(biolims.common.saveFailed)
			};
		}
	})
}

//默认下一步
function chosedNextFlow(){
	var rows = $("#workOrderItem .selected");
	var length = rows.length;
	if(!length) {
		top.layer.msg(biolims.common.pleaseSelect);
		return false;
	}
	top.layer.open({
		title: biolims.master.selectTestType,
		type: 2,
		area: [500,400],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/nextFlow/nextFlow/selectNextFlow.action", ''],
		yes: function(index, layer) {
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(0).text();
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addNextFlow .chosed").children("td").eq(2).text();
			rows.addClass("editagain");
			rows.find("td[savename='dnextName']").text(name);
			rows.find("td[savename='dnextId']").text(id);
			top.layer.close(index)
		},
	})
}