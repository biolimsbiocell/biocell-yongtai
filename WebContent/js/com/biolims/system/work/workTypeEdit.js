﻿
$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/com/biolims/system/work/workType/editWorkType.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/com/biolims/system/work/workType/showWorkTypeList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#workType", {
					userId : userId,
					userName : userName,
					formId : $("#workType_id").val(),
					title : $("#workType_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#workType_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: biolims.common.savingData, progressText: biolims.common.saving, width:300,   wait:true,   icon:'ext-mb-download'  });
	    //var workTypeItemDivData = $("#workTypeItemdiv").data("workTypeItemGrid");
		//document.getElementById('workTypeItemJson').value = commonGetModifyRecords(workTypeItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/com/biolims/system/work/workType/save.action";
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/com/biolims/system/work/workType/copyWorkType.action?id=' + $("#workType_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#workType_id").val() + "&tableId=workType");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#workType_id").val());
	nsc.push(biolims.common.IdEmpty);
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:biolims.common.sequencingFun,
	    	   contentEl:'markup'
	       } ]
	   });
});
//load("/com/biolims/system/work/workType/showWorkTypeItemList.action", {
//				id : $("#workType_id").val()
//			}, "#workTypeItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: biolims.common.copy
						});
	item.on('click', editCopy);