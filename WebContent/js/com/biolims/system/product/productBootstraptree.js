/* 
 * 文件名称 :productBootstraptree.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/02/06
 * 文件描述: 产品主数据的treeGrid操作
 * 
 */
$(document).ready(function() {
	//生成树
	$('#mytreeGrid').treegridData({
		id: 'id',
		parentColumn: 'parent',
		type: "GET", //请求数据的ajax类型
		url: '/com/biolims/system/product/showProductListJson.action', //请求数据的ajax的url
		ajaxParams: {}, //请求数据的ajax的data属性
		expandColumn: null, //在哪一列上面显示展开按钮
		striped: false, //是否各行渐变色
		bordered: true, //是否显示边框
		expandAll: false, //是否全部展开
		columns: [{
				title: biolims.common.id,
				field: 'id'
			},
			{
				title: biolims.common.name,
				field: 'name'
			},
			{
				title: biolims.master.productFee,
				field: 'productFee'
			},
			{
				title: biolims.sample.createUserName,
				field: 'createUser.name'
			},
			{
				title: biolims.common.state,
				field: 'stateName'
			},
			{
			/*	title: biolims.user.personnelGroup,
				field: 'acceptUserGroup.name'
			},
			{*/
				title: biolims.master.parentName,
				field: 'parentName'
			},
			{
				title: biolims.common.upId,
				field: 'parent'
			}
		]
	});

});
//新建
function add() {
	window.location = window.ctx + '/com/biolims/system/product/editProduct.action'
}
//编辑
function edit() {
	var id = $(".chosed").children("td").eq(1).text();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/com/biolims/system/product/editProduct.action?id=' + id;
}
//查看
function view() {
	var id = $(".chosed").children("td").eq(1).text();
	if(id == "" || id == undefined) {
		top.layer.msg(biolims.common.selectRecord);
		return false;
	}
	$("#maincontentframe", window.parent.document)[0].src = window.ctx +
		"/com/biolims/system/product/viewProduct.action?id=" +
		id;
}

// 弹框模糊查询参数
function searchOptions() {
	return [{
		"txt": biolims.common.id,
		"type": "input",
		"searchName": "id",
	}, {
		"txt": biolims.common.name,
		"type": "input",
		"searchName": "name",
	}, {
		"txt": "项目定价",
		"type": "input",
		"searchName": "productFee",
	}, {
		"txt": biolims.sample.createUserName,
		"type": "input",
		"searchName": "createUser.name",
	},/* {
		"txt": biolims.tInstrumentMaintain.createDate+"(Start)",
		"type": "dataTime",
		"searchName": "createDate##@@##1",
		"mark": "s##@@##",
	}, {
		"txt": biolims.tInstrumentMaintain.createDate+"(End)",
		"type": "dataTime",
		"mark": "e##@@##",
		"searchName": "createDate##@@##2"
	},*/ {
		"txt": biolims.common.state,
		"type": "input",
		"searchName": "stateName"
	}, {
		"txt": biolims.user.personnelGroup,
		"type": "top.layer",
		"searchName": "acceptUserGroup",
		"action": "acceptUserGroup()"
	}, {
		"type": "table",
		"table": $('#mytreeGrid'),
		"reloadUrl": ctx + "/com/biolims/system/product/showProductListJson.action"
	}];
}

function acceptUserGroup() {
	top.layer.open({
		title: biolims.common.selectRyGroup,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/userGroup/userGroupSelTable.action"],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserGroup .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserGroup .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$(".layui-top.layer-content #searchItemtop.layer").find("input[searchname='acceptUserGroup']").attr("sid", id);
			$(".layui-top.layer-content #searchItemtop.layer").find("input[searchname='acceptUserGroup']").val(name)
		},
	})

}