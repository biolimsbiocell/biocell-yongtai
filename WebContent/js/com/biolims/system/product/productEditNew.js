/* 
 * 文件名称 :productNew.js
 * 创建者 : 郭恒开
 * 创建日期: 2018/02/06
 * 文件描述: 新建产品数据操作函数
 * 
 */
$(function() {
	var handlemethod = $("#handlemethod").val();
	if(handlemethod == "view") {
		settextreadonly();
	}
	if(handlemethod == "modify") {
		$("#product_id").prop("readonly", "readonly");
	}
	// 上传附件(1.useType,2.modelType,3.id)
	var mainFileInput = fileInput('1', 'Product', $("#product_id").val());
	
	showProductItems($("#workOrderId").val());
})

function save() {
   //日志 dwb 2018-05-12 14:50:32
	var changeLog = "产品主数据-";
	$('input[class="form-control"]').each(function(i, v) {
		var valnew = $(v).val();
		var val = $(v).attr("changelog");
		if(val !== valnew) {
			changeLog += $(v).prev("span").text() + ':由"' + val + '"变为"' + valnew + '";';
		}
	});
	if(changeLog != "产品主数据-"){
		 document.getElementById("changeLog").value = changeLog;
	}
	
	var handlemethod = $("#handlemethod").val();
	//必填验证
	var requiredField=requiredFilter();
		if(!requiredField){
			return false;
		}
	var itemJson = saveProductItemJson($("#productItemTable"));
	$("#productItemJson").val(itemJson);
	console.log(itemJson);
	if(handlemethod == "modify") {
		top.layer.load(4, {shade:0.3}); 
		/*$.ajax({
			type: "post",
			url: ctx + '/com/biolims/system/product/save.action',
			data: {
				itemJson:itemJson
			},
			success: function(data) {
				var data = JSON.parse(data);
			}
		});*/
		$("#form").attr("action", "/com/biolims/system/product/save.action");
		$("#form").submit();
		top.layer.closeAll();
	} else {
		$.ajax({
			type: "post",
			url: ctx + '/common/hasId.action',
			data: {
				id: $("#product_id").val(),
				obj: 'Product'
			},
			success: function(data) {
				var data = JSON.parse(data);
				if(data.message) {
					top.layer.msg(data.message);
				} else {
					$("#form").attr("action", "/com/biolims/system/product/save.action");
					$("#form").submit();
					top.layer.closeAll();
				}
			}
		});
	}
}

function add() {
	window.location = window.ctx +
		"/com/biolims/system/product/editProduct.action";
}

function list() {
	window.location = window.ctx +
		'/com/biolims/system/product/showProductList.action';
}
//选择父级
function choseParent() {
	top.layer.open({
		title: biolims.master.parentName,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: window.ctx +
			"/com/biolims/system/product/showProductSelTree.action",
		yes: function(index, layer) {
			var name=[],id=[];
			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#mytreeGrid .chosed").each(function (i,v) {
				name.push($(v).children("td").eq(2).text());
				id.push($(v).children("td").eq(1).text());
			});
			$("#product_parent_name").val(name.join(","));
			$("#product_parent_id").val(id.join(","));
			top.layer.close(index)
		},
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
//选择区块
function choseQK() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=orderBlock", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#orderBlockId").val(id);
			$("#orderBlock").val(name);
		},
	})
}
//选择检测方法
function choseJCFF() {
	top.layer.open({
		title: biolims.sample.selectSequenceMethod,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=test", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#testMethodId").val(id);
			$("#testMethodName").val(name);
		},
	})
}
//选择负责组
function choseUserGroup() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/core/userGroup/userGroupSelTable.action"],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserGroup .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addUserGroup .chosed").children("td").eq(
				0).text();
			top.layer.close(index)
			$("#userGroupId").val(id);
			$("#userGroupName").val(name);
		},
	})
}
//选择ID
function chosesoId(){
	
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/dic/type/dicTypeSelectTable.action?flag=chose_id", ''],
		yes: function(index, layer) {
//			var name = [];
			var name = $('.layui-layer-iframe', parent.document).find(
			"iframe").contents().find("#sysCode").val();
//			$('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .selected").each(function(i, v) {
//				name.push($(v).children("td").eq(3).text());	
//			})

//			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addDicTypeTable .selected").children("td").eq(3).text();
			console.log(name)
		
			top.layer.close(index)
			$("#productHideFields").val(name);
		},
	})
	
}
//上传附件模态框出现
function fileUp() {
	if(!$("#product_id").val()){
		top.layer.msg("请先保存数据再上传附件！")
		return false;
	}
	$("#uploadFile").modal("show");
}
//查看附件
function fileView() {
	top.layer.open({
		title: biolims.common.attachment,
		type: 2,
		skin: 'layui-top.layer-lan',
		area: ["650px", "400px"],
		content: window.ctx + "/operfile/initFileList.action?flag=1&modelType=Product&id=" + $("#product_id").val(),
		cancel: function(index, layer) {
			top.layer.close(index)
		}
	})
}
//改变状态
function changeState (that) {
	var state=$(that).val();
	if(state=="0"){
		$("#stateName").val(biolims.master.invalid);
	}else{
		$("#stateName").val(biolims.master.valid);
	}
}
//选择流程
function choseLC() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/com/biolims/system/work/workOrder/workOrderSelect.action"],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addWorkOrder .chosed").children("td").eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addWorkOrder .chosed").children("td").eq(0).text();
			top.layer.close(index);
			$("#workOrderId").val(id);
			$("#workOrderName").val(name);
			
			showProductItems(id);
		},
	})
}

//展示产品明细信息
function showProductItems(id){
	$.ajax({
		type: "post",
		url: ctx + '/com/biolims/system/work/workOrder/findWorkOrderItemListById.action',
		data: {
			id: id
		},
		success: function(data) {
			var data = JSON.parse(data);
			console.log(data);
			if(data.success){
				$("#productItemTable").find("tbody").empty();
				for(var i=0;i<data.list.length;i++){
					$("#productItemTable").find(".dataTables_empty").parent("tr").remove();
					var tr = $("<tr class='editagain'><td><input type='checkbox' class='icheck' value=''/></td></tr>");
					var tds="<td savename='name'>"+data.list[i].name+"</td><td savename='note'>"+data.list[i].note+"</td>" +
					"<td savename='applicationId'>"+data.list[i].applicationTypeTable.id+
					"<td savename='application'>"+data.list[i].applicationTypeTable.name+
					"<td savename='nextStepId'>"+data.list[i].nextStepId+"</td><td savename='cycle'></td><td savename='template-name'></td>";
					tr.height(32);
					$("#productItemTable").find("tbody").append(tr.append(tds));
				}
				checkall($("#productItemTable"));
			}
		}
	});
}
//查看流程图
function viewLC () {
	var workOrderId=$("#workOrderId").val();
	if(!workOrderId){
		top.layer.msg("请先选择实验流程");
		return false;
	}
	top.layer.open({
		title: biolims.common.checkFlowChart,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/com/biolims/system/product/viewWorkOrder.action?workOrderId="+workOrderId],
		yes: function(index, layer) {
			top.layer.close(index)
		},
	})
}
//自定义字段
function choseCustom() {
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/customfields/showCustomDialog.action"],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcustomFieldTable .selected").children("td").eq(2).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addcustomFieldTable .selected").children("td").eq(1).text();
			top.layer.close(index)
			$("#fieldTemplateId").val(id);
			$("#fieldTemplateName").val(name);
		},
	})
}

function selectTemplate(){
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/template/template/showTemplateDialogList.action?type=1", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addTemplate .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addTemplate .chosed").children("td")
				.eq(0).text();
			$("#product_template_id").val(id);
			$("#product_template_name").val(name);
			top.layer.close(index);
		},
	})
}