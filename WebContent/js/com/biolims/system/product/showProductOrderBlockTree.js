$(function(){
Ext.onReady(function() {
		Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";

		var tree = new Ext.tree.TreePanel({
			renderTo : 'markup',
			title : biolims.master.product,
			height : document.body.clientHeight - 20,
			width : 520,
			useArrows : true,
			checked:true,
			autoScroll : true,
			animate : true,
			enableDD : true,
			containerScroll : true,
			rootVisible : false,
			root : {
				nodeType : 'async',
				checked:true
			},
			sorters : [ {
				property : 'id',
				direction : 'ASC'
			}, {
				property : 'text',
				direction : 'ASC'
			}  ],
			dataUrl :$("#historyTreePath").val(),

			listeners : {
				'checkchange' : function(node, checked) {
					if (checked) {
						if (node.leaf == false) {

							var childnodes = node.childNodes;
							Ext.each(childnodes, function() { // 从节点中取出子节点依次遍历
								var nd = this;

								if (nd.hasChildNodes()) { // 判断子节点下是否存在子节点
									findchildnode(nd); // 如果存在子节点 递归

								} else {

									nd.ui.toggleCheck(checked);
									nd.attributes.checked = checked;

								}
							});

						}
					} else {

						if (node.leaf == false) {

							var childnodes = node.childNodes;
							Ext.each(childnodes, function() { // 从节点中取出子节点依次遍历
								var nd = this;

								if (nd.hasChildNodes()) { // 判断子节点下是否存在子节点
									findchildnode(nd); // 如果存在子节点 递归
								} else {

									nd.ui.toggleCheck(checked);
									nd.attributes.checked = checked;

								}
							});

						}

					}
				}
			},
			buttons : [
			{

				id : 'saveAllModify',
				text : biolims.common.select,
				handler : function() {
					var msgId = '',msgName='', selNodes = tree.getChecked();
					Ext.each(selNodes, function(node) {
						if (msgId != '') {
							msgId += ',';
							msgName+= ',';
							
						}
						if(!node.attributes.text){
							return;
						}else{
							msgId +=  node.id ;
							msgName+=node.text;
						}
					});
					setvalue(msgId,msgName);
				}
			}
		  ]
		});

		tree.getRootNode().expand(true);
	});
	});



