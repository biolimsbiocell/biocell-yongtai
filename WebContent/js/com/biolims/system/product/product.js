var productGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'testTime',
		type:"string"
	});
	    fields.push({
			name:'testMethod-id',
			type:"string"
		});
	    fields.push({
			name:'testMethod-name',
			type:"string"
		});
	   fields.push({
		name:'workType-id',
		type:"string"
	});
	    fields.push({
		name:'workType-name',
		type:"string"
	});
	    fields.push({
		name:'productFee',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'mark',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	    fields.push({
		name:'personName',
		type:"string"
	});
	    fields.push({
		name:'parent-id',
		type:"string"
	});
	    fields.push({
		name:'parent-name',
		type:"string"
	});
	    fields.push({
			name:'money',
			type:"string"
		});
	    fields.push({
			name:'discount',
			type:"string"
		});
	    fields.push({
			name:'way',
			type:"string"
		});
	    
	    fields.push({
			name:'acceptUserGroup-id',
			type:"string"
		});
	    fields.push({
			name:'acceptUserGroup-name',
			type:"string"
		});
	    fields.push({
			name:'notes',
			type:"string"
		});
	    fields.push({
			name:'sampleCodeNum',
			type:"string"
		});
	    fields.push({
			name:'sampleNum',
			type:"string"
		});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:30*6,
		hidden:false,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:20*6,
		
		sortable:true
	});
	cm.push({
		dataIndex:'testTime',
		header:biolims.master.testTime,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'workType-id',
		hidden:true,
		header:biolims.wk.productId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'workType-name',
		header:biolims.wk.productName,
		
		width:15*10,
		sortable:true
		});
		cm.push({
			dataIndex:'testMethod-id',
			hidden:true,
			header:biolims.sample.businessTypeId,
			width:15*10,
			sortable:true
			});
			cm.push({
			dataIndex:'testMethod-name',
			header:biolims.sample.businessTypeName,
			
			width:15*10,
			sortable:true
			});
			cm.push({
				dataIndex:'money',
				header:biolims.master.money,
				width:20*6,
				
				sortable:true
			});
			cm.push({
				dataIndex:'discount',
				header:biolims.master.discount,
				width:20*6,
				
				sortable:true
			});
	cm.push({
		dataIndex:'productFee',
		header:biolims.master.productFee,
		width:20*6,
		
		sortable:true
	});
	var storeGoodCob = new Ext.data.ArrayStore({
		fields : [ 'id', 'name' ],
		data : [ [ '0', biolims.common.pleaseChoose ] ,[ '1', biolims.master.halfMonthFee ],[ '2', biolims.master.MonthFee ],[ '3', biolims.master.quarterFee ], [ '4', biolims.master.cashFee ]]
	});
	var goodCob = new Ext.form.ComboBox({
		store : storeGoodCob,
		displayField : 'name',
		valueField : 'id',
		mode : 'local'
	});
	
	
	cm.push({
		dataIndex:'way',
		hidden : false,
		header:biolims.master.way,
		width:20*6,
		editor : goodCob,
		renderer : Ext.util.Format.comboRenderer(goodCob)
	});
		cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:biolims.sample.createUserId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		
		width:15*10,
		sortable:true
		});
		cm.push({
			dataIndex:'acceptUserGroup-id',
			hidden:true,
			header:"负责组id",
			width:15*10,
			sortable:true
			});
			cm.push({
			dataIndex:'acceptUserGroup-name',
			header:"负责组",
			width:15*10,
			sortable:true
			});
		cm.push({
		dataIndex:'mark',
		header:biolims.master.mark,
		width:20*6,
		
		sortable:true
	});
		cm.push({
			dataIndex:'sampleNum',
			header:"用量",
			width:20*6,
			
			sortable:true
		});
		cm.push({
			dataIndex:'notes',
			header:"备注",
			width:40*6,
			
		});
		var statestore = new Ext.data.ArrayStore({
			fields : [ 'id', 'name' ],
			data : [ [ '1', biolims.master.valid ], [ '0', biolims.master.invalid ]]
		});
		
		var stateComboxFun = new Ext.form.ComboBox({
			store : statestore,
			displayField : 'name',
			valueField : 'id',
			typeAhead : true,
			mode : 'local',
			forceSelection : true,
			triggerAction : 'all',
			emptyText : '',
			selectOnFocus : true
		});
		cm.push({
			dataIndex:'state',
			header:biolims.common.state,
			width:20*6,
			
			renderer: Ext.util.Format.comboRenderer(stateComboxFun),				
			sortable:true
		});
//	cm.push({
//		dataIndex:'stateName',
//		header:'工作流状态',
//		width:20*6,
//		
//		sortable:true
//	});
	cm.push({
		dataIndex:'personName',
		header:biolims.master.personName,
		width:20*6,
		
		sortable:true
	});
		cm.push({
		dataIndex:'parent-id',
		hidden:true,
		header:biolims.master.parentId,
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'parent-name',
		header:biolims.master.parentName,
		
		width:15*10,
		sortable:true
		});
		cm.push({
			dataIndex:'sampleCodeNum',
			header:"关联样本数量",
			width:40*6,
			
		});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/com/biolims/system/product/showProductListJson.action";
	var opts={};
	opts.title=biolims.master.productMasterData;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	productGrid=gridTable("show_product_div",cols,loadParam,opts);
})
function add(){
		window.location=window.ctx+'/com/biolims/system/product/editProduct.action';
	}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/com/biolims/system/product/editProduct.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/com/biolims/system/product/viewProduct.action?id=' + id;
}
function exportexcel() {
	productGrid.title = biolims.common.exportList;
	var vExportContent = productGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
			
				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}
				
				
				commonSearchAction(productGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	});
});
Ext.onReady(function(){
	var item = menu.add({
	    	text: biolims.master.listModel
		});
	item.on('click', lbms);
	
	});
function lbms(){
	var url = ctx+"/com/biolims/system/product/showProductList.action";
	location.href = url;
}
Ext.onReady(function(){
	var item1 = menu.add({
	    	text: biolims.master.treeModel
		});
	item1.on('click', szms);
	});
function szms(){
	var url = ctx+"/com/biolims/system/product/showProductTree.action";
	location.href = url;
}
