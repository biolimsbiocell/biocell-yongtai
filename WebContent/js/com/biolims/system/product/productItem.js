var productItemTable;
var colOpts = [];
colOpts.push({
	"data": "id",
	"title": biolims.common.taskDetailId,
	"visible": false,
	"createdCell": function(td) {
		$(td).attr("saveName", "id");
	}
})
colOpts.push({
	"data": "name",
	"title": biolims.common.stepName,
	"createdCell": function(td) {
		$(td).attr("saveName", "name");
	}
})
colOpts.push({
	"data": "note",
	"title": biolims.common.note,
	"width": "120px",
	"createdCell": function(td, data) {
		$(td).attr("saveName", "note");
	},
})
colOpts.push({
	"data": "applicationId",
	"title": biolims.common.experimentModuleId,
	"createdCell": function(td, data, rowData) {
		$(td).attr("saveName", "applicationId");
	}
})
colOpts.push({
	"data": "application",
	"title": biolims.common.experimentModule,
	"createdCell": function(td, data, rowData) {
		$(td).attr("saveName", "application");
	}
})
colOpts.push({
	"data": "nextStepId",
	"title": biolims.common.nextStepId,
	"createdCell": function(td, data) {
		$(td).attr("saveName", "nextStepId");
	},
})
colOpts.push({
	"data": "cycle",
	"title": "周期",
	"createdCell": function(td, data) {
		$(td).attr("saveName", "cycle");
	},
})
colOpts.push({
	"data": "template-id",
	"title": "模板ID",
	"visible":false,
	"createdCell": function(td, data) {
		$(td).attr("saveName", "template-id");
	},
})
colOpts.push({
	"data": "template-name",
	"title": "模板名称",
	"createdCell": function(td, data,rowdata) {
		$(td).attr("saveName", "template-name");
		$(td).attr("template-id", rowdata["template-id"]);
	},
})
var tbarOpts = [];

tbarOpts.push({
		text: "选择模板",
		action: function() {
			selectTemplate1();
		}
	});
var options = table(true,$("#product_id").val(),
	"/com/biolims/system/product/showProductItemListJson.action",
	colOpts, tbarOpts);
productItemTable = renderData($("#productItemTable"),options);

function saveProductItemJson(ele){
	var trs = ele.find("tbody").children(".editagain");
	var data = [];
	trs.each(function(i, val) {
		var json = {};
		var tds = $(val).children("td");
		json["id"] = $(tds[0]).find("input").val();
		for(var j = 1; j < tds.length; j++) {
			var k = $(tds[j]).attr("savename");
			json[k] = $(tds[j]).text();
			
			if(k == "template-name") {
				json["template-id"] = $(tds[j]).attr("template-id");
				continue;
			}
		}
		data.push(json);
	});
	return JSON.stringify(data);
}

function selectTemplate1(){
	var rows = $("#productItemTable .selected");
	if(!rows.length){
		top.layer.msg(biolims.common.pleaseSelectData);
		return false;
	}
	top.layer.open({
		title: biolims.common.pleaseChoose,
		type: 2,
		area: ["650px", "400px"],
		btn: biolims.common.selected,
		content: [window.ctx + "/system/template/template/showTemplateDialogList.action", ''],
		yes: function(index, layer) {
			var name = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addTemplate .chosed").children("td")
				.eq(1).text();
			var id = $('.layui-layer-iframe', parent.document).find("iframe").contents().find("#addTemplate .chosed").children("td")
				.eq(0).text();
			$("#productItemTable").find("tr").addClass("editagain");
			rows.find("td[savename='template-name']").attr(
				"template-id", id).text(name);
			rows.find("td[savename='template-id']").text(id);

			top.layer.close(index);
		},
	})
}