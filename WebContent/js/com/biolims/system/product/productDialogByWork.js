var productDialogByWorkGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'name',
		type:"string"
	});
	    fields.push({
		name:'testTime',
		type:"string"
	});
	    fields.push({
		name:'workType-id',
		type:"string"
	});
	    fields.push({
		name:'workType-name',
		type:"string"
	});
	    fields.push({
		name:'productFee',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:biolims.common.id,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:biolims.common.name,
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'testTime',
		header:biolims.master.testTime,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'workType-id',
		header:biolims.sample.businessTypeId,
		width:50*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'workType-name',
		header:biolims.sample.businessTypeName,
		width:50*10,
		sortable:true
		});
	cm.push({
		dataIndex:'productFee',
		header:biolims.master.productFee,
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:biolims.sample.createUserId,
		width:20*10,
		hidden:true,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:biolims.sample.createUserName,
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:biolims.sample.createDate,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:biolims.common.state,
		width:20*10,
		hidden:true,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:biolims.common.workFlowState,
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/com/biolims/system/product/showDialogProductListJsonByWork.action?code="+$("#code").val();
	var opts={};
	opts.title=biolims.master.productMasterData;
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setProductFun(rec);
	};
	productDialogByWorkGrid=gridTable("show_dialog_product_div",cols,loadParam,opts);
});
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), biolims.common.search, null, {
			"开始检索(Start retrieve)" : function() {
				commonSearchAction(productDialogByWorkGrid);
				$(this).dialog("close");

			},
			"清空(Empty)" : function() {
				form_reset();

			}
		}, true, option);
	}
