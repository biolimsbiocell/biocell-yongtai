$(function() {
	var cols = {};
	cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});
	fields.push({
		name : 'assignee',
		type : "string"
	});
	fields.push({
		name : 'proxyUserId',
		type : "string"
	});
	fields.push({
		name : 'state',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 120,
		hidden : true
	});
	cm.push({
		dataIndex : 'assignee',
		header : '办理人',
		width : 200,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex : 'proxyUserId',
		header : '代办人',
		width : 150,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});

	var state = new Ext.form.ComboBox({
		transform : "sel_state",
		width : 100,
		triggerAction : 'all',
		lazyRender : true
	});
	cm.push({
		dataIndex : 'state',
		header : '状态',
		width : 100,
		editor : state,
		renderer : Ext.util.Format.comboRenderer(state)
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/proxy/showProxyUserListJson.action";
	var opts = {};
	opts.title = "代办人设置";
	opts.height = document.documentElement.clientHeight;

	opts.tbar = [];
	opts.delSelect = function(ids) {
		ajax("post", "/workflow/proxy/deleteProxyUser.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);

	};
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});

	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});

	opts.tbar.push({
		text : '保存',
		handler : function() {
			var param = {};
			var datas = [];
			var modifyRecord = grid.getAllRecord();
			if (modifyRecord) {
				$.each(modifyRecord, function(i, obj) {
					if (obj.get("assignee") && obj.get("proxyUserId")) {
						var data = {};
						data.id = obj.get("id");
						data.assignee = obj.get("assignee");
						data.proxyUserId = obj.get("proxyUserId");
						data.state = obj.get("state");
						datas.push(data);
					}
				});
			}

			if (datas.length > 0) {
				ajax("post", "/workflow/proxy/saveProxyUser.action", {
					data : JSON.stringify(datas),
				}, function(data) {
					if (data.success) {
						message("保存成功！");
						grid.getStore().reload();
					} else {
						message("保存失败！");
					}
				}, null);
			}
		}
	});

	var grid = gridEditTable("proxy_user_set_div", cols, loadParam, opts);
});