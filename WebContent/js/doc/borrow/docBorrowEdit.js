$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/doc/borrow/docBorrow/editDocBorrow.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/doc/borrow/docBorrow/showDocBorrowList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#docBorrow", {
					userId : userId,
					userName : userName,
					formId : $("#docBorrow_id").val(),
					title : $("#docBorrow_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#docBorrow_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
});






function save() {
if(checkSubmit()==true){
	    var docBorrowItemDivData = $("#docBorrowItemdiv").data("docBorrowItemGrid");
		document.getElementById('docBorrowItemJson').value = commonGetModifyRecords(docBorrowItemDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/doc/borrow/docBorrow/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/doc/borrow/docBorrow/copyDocBorrow.action?id=' + $("#docBorrow_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#docBorrow_id").val() + "&tableId=docBorrow");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#docBorrow_id").val());
	nsc.push("合同编码不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'文档借阅申请',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/doc/borrow/docBorrow/showDocBorrowItemList.action", {
				id : $("#docBorrow_id").val()
			}, "#docBorrowItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);