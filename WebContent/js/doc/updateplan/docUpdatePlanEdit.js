$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	
function add() {
	window.location = window.ctx + "/doc/docUpdatePlan/editDocUpdatePlan.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/doc/docUpdatePlan/showDocUpdatePlanList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#docUpdatePlan", {
					userId : userId,
					userName : userName,
					formId : $("#docUpdatePlan_id").val(),
					title : $("#docUpdatePlan_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#docUpdatePlan_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/dashboard/toDashboard.action';
		});
});






function save() {
if(checkSubmit()==true){
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/doc/docUpdatePlan/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/doc/docUpdatePlan/copyDocUpdatePlan.action?id=' + $("#docUpdatePlan_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#docUpdatePlan_id").val() + "&tableId=docUpdatePlan");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#docUpdatePlan_id").val());
	nsc.push("计划编码不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'文档更新计划',
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);