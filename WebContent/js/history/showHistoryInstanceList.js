$(function() {
	$("button").button();

	var cols = {};
	// cols.sm = true;
	var fields = [];

	fields.push({
		name : 'id',
		type : "string"
	});

	fields.push({
		name : 'processInstanceId',
		type : "string"
	});
	fields.push({
		name : 'applUserId',
		type : "string"
	});
	fields.push({
		name : 'applUserName',
		type : "string"
	});
	fields.push({
		name : 'formName',
		type : "string"
	});
	fields.push({
		name : 'tableName',
		type : "string"
	});
	fields.push({
		name : 'tablePath',
		type : "string"
	});
	fields.push({
		name : 'formTitle',
		type : "string"
	});
	fields.push({
		name : 'startDate',
		type : "string"
	});
	fields.push({
		name : 'endDate',
		type : "string"
	});
	fields.push({
		name : 'businessKey',
		type : "string"
	});

	fields.push({
		name : 'version',
		type : "string"
	});

	fields.push({
		name : 'isEnd',
		type : "string"
	});
	fields.push({
		name : 'isSubProcess',
		type : "string"
	});
	fields.push({
		name : 'parentProcessId',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'id',
		header : 'ID',
		width : 100,
		hidden : true
	});
	cm.push({
		dataIndex : 'processInstanceId',
		header : '流程实例ID',
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'applUserId',
		header : '发起人ID',
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'applUserName',
		header : '发起人',
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'formName',
		header : '表单ID',
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'tableName',
		header : '表单名称',
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'tablePath',
		header : '表单路径',
		width : 120,
		hidden: true,
		sortable : false
	});
	cm.push({
		dataIndex : 'formTitle',
		header : '表单提示',
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'startDate',
		header : '开始时间',
		width : 130,
		sortable : false
	});
	cm.push({
		dataIndex : 'endDate',
		header : '结束时间',
		width : 130,
		sortable : false
	});
	cm.push({
		dataIndex : 'businessKey',
		header : '业务ID',
		width : 130,
		sortable : false
	});

	cm.push({
		dataIndex : 'version',
		header : '流程定义版本',
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'isEnd',
		header : '状态',
		width : 100,
		sortable : false
	});
	cm.push({
		dataIndex : 'isSubProcess',
		header : '是否子流程',
		width : 100,
		sortable : false
	});
	cm.push({
		dataIndex : 'parentProcessId',
		header : '主流程ID',
		width : 100,
		sortable : false
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/history/showHistoryInstanceJson.action";
	var opts = {};
	opts.title = "审批历史";
	opts.height = document.documentElement.clientHeight - 30;
	opts.tbar = [];

	var grid;

	opts.tbar.push({
		text : '审批明细',
		handler : function() {
			var selRocord = grid.getSelectRecord();
			if (selRocord && selRocord.length > 0) {
				var instanceId = selRocord[0].get("processInstanceId");
				var parentProcessId = selRocord[0].get("parentProcessId");
				if (parentProcessId)
					instanceId = parentProcessId;
				var options = {};
				options.width = 600;
				options.height = 447;
				options.data = {
					instanceId : instanceId
				};
				loadDialogPageNo(null, "审批历史", "/workflow/history/showTaskVariablesList.action", null, true, options);

			} else {
				message("请选择一条流程实例！");
			}
		}
	},{
		text : '查看表单',
		handler : function() {
			var selRocord = grid.getSelectRecord();
			if (selRocord && selRocord.length > 0) {
				var tablePath = selRocord[0].get("tablePath");
				openDialogExt(window.ctx+tablePath);  

			} else {
				message("请选择一条流程实例！");
			}
		}
	});

	grid = gridTable("show_history_process_instance_div", cols, loadParam, opts);
	$("#search_btn").click(function() {
		var paramData = {
			sendUser : $.trim($("#send_user").val()),
			formName : $.trim($("#form_name").val()),
			formId : $.trim($("#form_id").val()),
			limit : grid.toolbars[1].pageSize
		};
		grid.getStore().load({
			params : paramData
		});
	});

});