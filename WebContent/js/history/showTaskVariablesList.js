$(function() {
	var cols = {};
	// cols.sm = true;
	var fields = [];

	fields.push({
		name : 'taskName',
		type : "string"
	});

	fields.push({
		name : 'assigneeName',
		type : "string"
	});
	fields.push({
		name : 'modifyTime',
		type : "string"
	});
	fields.push({
		name : 'operResult',
		type : "string"
	});
	fields.push({
		name : 'info',
		type : "string"
	});
	fields.push({
		name : 'proxyInfo',
		type : "string"
	});

	cols.fields = fields;
	var cm = [];

	cm.push({
		dataIndex : 'taskName',
		header : '任务名称',
		width : 100
	});
	cm.push({
		dataIndex : 'assigneeName',
		header : '办理人',
		width : 120,
		sortable : false
	});
	cm.push({
		dataIndex : 'modifyTime',
		header : '办理时间',
		width : 130,
		sortable : false
	});
	cm.push({
		dataIndex : 'operResult',
		header : '办理结果',
		width : 100,
		sortable : false
	});
	cm.push({
		dataIndex : 'info',
		header : '办理意见 ',
		width : 150,
		sortable : false
	});
	cm.push({
		dataIndex : 'proxyInfo',
		header : '代办信息 ',
		width : 150,
		sortable : false
	});

	cols.cm = cm;
	var loadParam = {};
	loadParam.url = ctx + "/workflow/history/showTaskVariablesJson.action?instanceId=" + $("#instance_id").val();
	var opts = {};
	opts.height = 400;
	opts.width = 580;
	gridTable("show_history_task_div", cols, loadParam, opts);
	$(".ui-dialog-content").css("height", "412px");

});