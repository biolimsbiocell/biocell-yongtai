$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	;
function add() {
	window.location = window.ctx + "/kb/zsk/knowledgeZSK/editKnowledgeZSK.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/kb/zsk/knowledgeZSK/showKnowledgeZSKList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#knowledgeZSK", {
					userId : userId,
					userName : userName,
					formId : $("#knowledgeZSK_id").val(),
					title : $("#knowledgeZSK_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#knowledgeZSK_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){
	    var knowledgeJDZSKResultDivData = $("#knowledgeJDZSKResultdiv").data("knowledgeJDZSKResultGrid");
		document.getElementById('knowledgeJDZSKResultJson').value = commonGetModifyRecords(knowledgeJDZSKResultDivData);
	    /*var knowledgeBXZLZSKDivData = $("#knowledgeBXZLZSKdiv").data("knowledgeBXZLZSKGrid");
		document.getElementById('knowledgeBXZLZSKJson').value = commonGetModifyRecords(knowledgeBXZLZSKDivData);*/
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/kb/zsk/knowledgeZSK/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/kb/zsk/knowledgeZSK/copyKnowledgeZSK.action?id=' + $("#knowledgeZSK_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#knowledgeZSK_id").val() + "&tableId=knowledgeZSK");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#knowledgeZSK_id").val());
	nsc.push("检测申请号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return true;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'知识库',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/kb/zsk/knowledgeZSK/showKnowledgeJDZSKResultList.action", {
				id : $("#knowledgeZSK_id").val()
			}, "#knowledgeJDZSKResultpage");
load("/kb/zsk/knowledgeZSK/showKnowledgeBXZLZSKList.action", {
				id : $("#knowledgeZSK_id").val()
			}, "#knowledgeBXZLZSKpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);