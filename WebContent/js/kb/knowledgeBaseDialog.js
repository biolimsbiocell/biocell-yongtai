var knowledgeBaseDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'mutationGenes',
		type:"string"
	});
	    fields.push({
		name:'type-id',
		type:"string"
	});
	    fields.push({
		name:'type-name',
		type:"string"
	});
	    fields.push({
		name:'original',
		type:"string"
	});
	    fields.push({
		name:'otherName',
		type:"string"
	});
	    fields.push({
		name:'daodu',
		type:"string"
	});
	    fields.push({
		name:'biologySpecific',
		type:"string"
	});
	    fields.push({
		name:'antistop',
		type:"string"
	});
	    fields.push({
		name:'commonMutaionCancer',
		type:"string"
	});
	    fields.push({
		name:'noopen',
		type:"string"
	});
	    fields.push({
		name:'state-id',
		type:"string"
	});
	    fields.push({
		name:'state-name',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编码',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'mutationGenes',
		header:'突变基因',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'otherName',
		header:'其他名称',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'biologySpecific',
		header:'生物学特定功能',
		width:50*10,
		sortable:true
	});
	cm.push({
		dataIndex:'commonMutaionCancer',
		header:'常见突变与癌症',
		width:15*10,
		sortable:true
	});
		cm.push({
		dataIndex:'type-id',
		header:'选择类别ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'type-name',
		header:'选择类别',
		width:15*10,
		sortable:true
		});
	cm.push({
		dataIndex:'original',
		header:'是否原创',
		width:15*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'daodu',
		header:'导读',
		width:15*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'antistop',
		header:'关键词',
		width:15*10,
		sortable:true
	});
	
	cm.push({
		dataIndex:'noopen',
		header:'不公开',
		width:15*10,
		sortable:true
	});
		cm.push({
		dataIndex:'state-id',
		header:'状态ID',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'state-name',
		header:'状态',
		width:15*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-id',
		header:'创建人ID',
		width:10*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'创建人',
		width:10*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'创建日期',
		width:12*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/kb/showKnowledgeBaseListJson.action";
	var opts={};
	opts.title="知识库";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setKnowledgeBaseFun(rec);
	};
	knowledgeBaseDialogGrid=gridTable("show_dialog_knowledgeBase_div",cols,loadParam,opts);
	$("#show_dialog_knowledgeBase_div").data("knowledgeBaseDialogGrid", knowledgeBaseDialogGrid);

})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(knowledgeBaseDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
