var knowledgeGrid;
$(function(){
	var cols={};
	var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'patientId',
		type:"string"
	});
	fields.push({
		name:'sampleCode',
		type:"string"
	});
	fields.push({
		name:'createUser-id',
		type:"string"
	});
	fields.push({
		name:'createUser-name',
		type:"string"
	});
	fields.push({
		name:'createDate',
		type:"string"
	});
	fields.push({
		name:'sampleType-id',
		type:"string"
	});
	fields.push({
		name:'sampleType-name',
		type:"string"
	});
	fields.push({
		name:'path',
		type:"string"
	});
	fields.push({
		name:'productId',
		type:"string"
	});
	fields.push({
		name:'productName',
		type:"string"
	});
	fields.push({
		name:'joinlab',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*6,
		sortable:true
	});
	cm.push({
		dataIndex:'patientId',
		header:'病案号',
		width:20*6,

		sortable:true
	});
	cm.push({
		dataIndex:'sampleCode',
		header:'样本编号',
		width:20*6,

		sortable:true
	});
	cm.push({
		dataIndex:'createUser-id',
		hidden:true,
		header:'上传人ID',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createUser-name',
		header:'上传人',

		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:'上传时间',
		width:20*6,

		sortable:true
	});
	cm.push({
		dataIndex:'sampleType-id',
		hidden:true,
		header:'样本类型ID',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'sampleType-name',
		header:'样本类型',

		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'path',
		header:'附件地址',
		hidden:true,
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'productId',
		hidden:true,
		header:'检测项目ID',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'productName',
		header:'检测项目',

		width:20*10,
		sortable:true
	});
//	cm.push({
//		dataIndex:'joinlab',
//		header:'合作实验室',
//		width:20*6,
//
//		sortable:true
//	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/kb/knowle/knowledge/showKnowledgeListJson.action";
	var opts={};
	opts.title="知识库";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id){
		$('#selectId').val(id);
		edit();
	};
	knowledgeGrid=gridTable("show_knowledge_div",cols,loadParam,opts);
})
function add(){
	window.location=window.ctx+'/kb/knowle/knowledge/editKnowledge.action';
}
function edit(){
	var id="";
	id=document.getElementById("selectId").value;
	if (id==""||id==undefined){
		message("请选择一条记录!");
		return false;
	}
	window.location=window.ctx+'/kb/knowle/knowledge/editKnowledge.action?id=' + id;
}
function view() {
	var id = "";
	id = document.getElementById("selectId").value;
	if (id == "" || id == undefined) {
		message("请选择一条记录!");
		return false;
	}
	window.location = window.ctx + '/kb/knowle/knowledge/viewKnowledge.action?id=' + id;
}
function exportexcel() {
	knowledgeGrid.title = '导出列表';
	var vExportContent = knowledgeGrid.getExcelXml();
	var x = document.getElementById('gridhtm');
	x.value = vExportContent;
	document.excelfrm.submit();
}
$(function() {
	$("#opensearch").click(function() {
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {

				if (($("#startcreateDate").val() != undefined) && ($("#startcreateDate").val() != '')) {
					var startcreateDatestr = ">=##@@##" + $("#startcreateDate").val();
					$("#createDate1").val(startcreateDatestr);
				}
				if (($("#endcreateDate").val() != undefined) && ($("#endcreateDate").val() != '')) {
					var endcreateDatestr = "<=##@@##" + $("#endcreateDate").val();

					$("#createDate2").val(endcreateDatestr);

				}


				commonSearchAction(knowledgeGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	});
});
