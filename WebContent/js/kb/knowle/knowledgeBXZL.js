var knowledgeBXZLGrid;
$(function(){
	var cols={};
	cols.sm = true;
    var fields=[];
	   fields.push({
		name:'id',
		type:"string"
	});
	   fields.push({
		name:'ywName',
		type:"string"
	});
	   fields.push({
		name:'spName',
		type:"string"
	});
	   fields.push({
		name:'spot',
		type:"string"
	});
	   fields.push({
		name:'state',
		type:"string"
	});
	    fields.push({
		name:'knowledge-id',
		type:"string"
	});
	    fields.push({
		name:'knowledge-name',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		hidden : true,
		header:'编号',
		width:20*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'ywName',
		hidden : false,
		header:'药物名称',
		width:30*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'spName',
		hidden : false,
		header:'商品名',
		width:30*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'spot',
		hidden : false,
		header:'靶点/原理',
		width:30*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'state',
		hidden : false,
		header:'审批状态/临床试验状态',
		width:30*6,
		sortable:true,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'knowledge-id',
		hidden : true,
		header:'相关主表ID',
		width:20*10,
		editor : new Ext.form.TextField({
			allowBlank : true
		})
	});
	cm.push({
		dataIndex:'knowledge-name',
		hidden : true,
		header:'相关主表',
		width:20*10
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/kb/knowle/knowledge/showKnowledgeBXZLListJson.action?id="+ $("#id_parent_hidden").val();
	var opts={};
	opts.title="靶向治疗药物";
	opts.height =  document.body.clientHeight-100;
	opts.tbar = [];
       opts.delSelect = function(ids) {
		ajax("post", "/kb/knowle/knowledge/delKnowledgeBXZL.action", {
			ids : ids
		}, function(data) {
			if (data.success) {
				scpProToGrid.getStore().commitChanges();
				scpProToGrid.getStore().reload();
				message("删除成功！");
			} else {
				message("删除失败！");
			}
		}, null);
	};
	/*opts.tbar.push({
			text : '选择相关主表',
				handler : selectknowledgeDialogFun
		});*/
	opts.tbar.push({
		text : "批量上传（csv文件）",
		handler : function() {
			var options = {};
			options.width = 350;
			options.height = 200;
			loadDialogPage($("#bat_uploadcsv_div"),"批量上传",null,{
				"确定":function(){
					goInExcelcsv();
					$(this).dialog("close");
				}
			},true,options);
		}
	});
	function goInExcelcsv(){
		var file = document.getElementById("file-uploadcsv").files[0];  
		var n = 0;
		var ob = knowledgeBXZLGrid.getStore().recordType;
		var reader = new FileReader();  
		reader.readAsText(file,'GB2312');  
		reader.onload=function(f){  
			var csv_data = $.simple_csv(this.result);
			$(csv_data).each(function() {
                	if(n>0){
                		if(this[0]){
                			var p = new ob({});
                			p.isNew = true;				
                			var o;
                			o= 0-1;
                			p.set("po.fieldName",this[o]);
                			o= 1-1;
                			p.set("po.fieldName",this[o]);
                			o= 2-1;
                			p.set("po.fieldName",this[o]);
                			o= 3-1;
                			p.set("po.fieldName",this[o]);
                			o= 4-1;
                			p.set("po.fieldName",this[o]);
                			o= 5-1;
                			p.set("po.fieldName",this[o]);
							knowledgeBXZLGrid.getStore().insert(0, p);
                		}
                	}
                     n = n +1;
                	
                });
    	}
	}
	opts.tbar.push({
		text : '显示可编辑列',
		handler : null
	});
	opts.tbar.push({
		text : '取消选中',
		handler : null
	});
	knowledgeBXZLGrid=gridEditTable("knowledgeBXZLdiv",cols,loadParam,opts);
	$("#knowledgeBXZLdiv").data("knowledgeBXZLGrid", knowledgeBXZLGrid);
	$(".x-panel-bbar,.x-toolbar").css("width", "100%");
	$(".x-panel-tbar").css("width", "100%");
})
function selectknowledgeFun(){
	var win = Ext.getCmp('selectknowledge');
	if (win) {win.close();}
	var selectknowledge= new Ext.Window({
	id:'selectknowledge',modal:true,title:'选择相关主表',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center',
	buttons: [
	{ text: '关闭',
	 handler: function(){
		 selectknowledge.close(); }  }]  }) });  
    selectknowledge.show(); }
	function setknowledge(rec){
		var gridGrid = $("#knowledgeBXZLdiv").data("knowledgeBXZLGrid");
		var selRecords = gridGrid.getSelectionModel().getSelections(); 
		$.each(selRecords, function(i, obj) {
			obj.set('knowledge-id',rec.get('id'));
			obj.set('knowledge-name',rec.get('name'));
		});
		var win = Ext.getCmp('selectknowledge')
		if(win){
			win.close();
		}
	}
	function selectknowledgeDialogFun(){
			var title = '';
			var url = '';
			title = "选择相关主表";
			url = ctx + "/kb/knowle/knowledge/KnowledgeSelect.action?flag=knowledge";
			var option = {};
			option.width = document.body.clientWidth-30;
			option.height = document.body.clientHeight-160;
			loadDialogPage(null, title, url, {
				"确定" : function() {
						selknowledgeVal(this);
				}
			}, true, option);
		}
	var selknowledgeVal = function(win) {
		var operGrid = knowledgeDialogGrid;
		var selectRecord = operGrid.getSelectionModel().getSelections();
		if (selectRecord.length > 0) {
			var gridGrid = $("#knowledgeBXZLdiv").data("knowledgeBXZLGrid");
			var selRecords = gridGrid.getSelectionModel().getSelections(); 
			$.each(selRecords, function(i, obj) {
				obj.set('knowledge-id',rec.get('id'));
				obj.set('knowledge-name',rec.get('name'));
			});
			$(win).dialog("close");
			$(win).dialog("remove");
		} else {
			message("请选择您要选择的数据");
			return;
		}
	};
