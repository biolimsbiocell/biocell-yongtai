var knowledgeDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	    fields.push({
		name:'id',
		type:"string"
	});
	    fields.push({
		name:'patientId',
		type:"string"
	});
	    fields.push({
		name:'createUser-id',
		type:"string"
	});
	    fields.push({
		name:'createUser-name',
		type:"string"
	});
	    fields.push({
		name:'createDate',
		type:"string"
	});
	    fields.push({
		name:'sampleType-id',
		type:"string"
	});
	    fields.push({
		name:'sampleType-name',
		type:"string"
	});
	    fields.push({
		name:'product-id',
		type:"string"
	});
	    fields.push({
		name:'product-name',
		type:"string"
	});
	    fields.push({
		name:'joinlab',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'检测申请号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'patientId',
		header:'病案号',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'createUser-id',
		header:'上传人ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'createUser-name',
		header:'上传人',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'createDate',
		header:'上传时间',
		width:20*10,
		sortable:true
	});
		cm.push({
		dataIndex:'sampleType-id',
		header:'样本类型ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'sampleType-name',
		header:'样本类型',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'product-id',
		header:'检测项目ID',
		width:20*10,
		sortable:true
		});
		cm.push({
		dataIndex:'product-name',
		header:'检测项目',
		width:20*10,
		sortable:true
		});
	cm.push({
		dataIndex:'joinlab',
		header:'合作实验室',
		width:20*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/kb/knowle/knowledge/showKnowledgeListJson.action";
	var opts={};
	opts.title="知识库";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setKnowledgeFun(rec);
	};
	knowledgeDialogGrid=gridTable("show_dialog_knowledge_div",cols,loadParam,opts);
})
function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(knowledgeDialogGrid);
				$(this).dialog("close");

			},
			"清空" : function() {
				form_reset();

			}
		}, true, option);
	}
