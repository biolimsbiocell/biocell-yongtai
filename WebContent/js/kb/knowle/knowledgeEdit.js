$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
})	;
function add() {
	window.location = window.ctx + "/kb/knowle/knowledge/editKnowledge.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/kb/knowle/knowledge/showKnowledgeList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	save();
});	
$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#knowledge", {
					userId : userId,
					userName : userName,
					formId : $("#knowledge_id").val(),
					title : $("#knowledge_name").val()
				}, function() {
					window.location.reload();
				});
				
});
$("#toolbarbutton_sp").click(function() {
		completeTask($("#knowledge_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});






function save() {
if(checkSubmit()==true){
	    var knowledgeJYTBResultDivData = $("#knowledgeJYTBResultdiv").data("knowledgeJYTBResultGrid");
		document.getElementById('knowledgeJYTBResultJson').value = commonGetModifyRecords(knowledgeJYTBResultDivData);
	    var knowledgeJYCPResultDivData = $("#knowledgeJYCPResultdiv").data("knowledgeJYCPResultGrid");
		document.getElementById('knowledgeJYCPResultJson').value = commonGetModifyRecords(knowledgeJYCPResultDivData);
	    var knowledgeKBSJResultDivData = $("#knowledgeKBSJResultdiv").data("knowledgeKBSJResultGrid");
		document.getElementById('knowledgeKBSJResultJson').value = commonGetModifyRecords(knowledgeKBSJResultDivData);
	    var knowledgeJDResultDivData = $("#knowledgeJDResultdiv").data("knowledgeJDResultGrid");
		document.getElementById('knowledgeJDResultJson').value = commonGetModifyRecords(knowledgeJDResultDivData);
	    var knowledgeBXZLDivData = $("#knowledgeBXZLdiv").data("knowledgeBXZLGrid");
		document.getElementById('knowledgeBXZLJson').value = commonGetModifyRecords(knowledgeBXZLDivData);
	document.getElementById('toolbarSaveButtonFlag').value = 'save';
	form1.action = window.ctx + "/kb/knowle/knowledge/save.action";
	form1.submit();
	var loadMarsk = new Ext.LoadMask(Ext.getBody(),
		{
		       msg : '正在处理，请稍候。。。。。。',
		       removeMask : true// 完成后移除
		 });
	loadMarsk.show();	
	}
}		
function editCopy() {
	window.location = window.ctx + '/kb/knowle/knowledge/copyKnowledge.action?id=' + $("#knowledge_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#knowledge_id").val() + "&tableId=knowledge");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#knowledge_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'知识库',
	    	   contentEl:'markup'
	       } ]
	   });
});
load("/kb/knowle/knowledge/showKnowledgeJYTBResultList.action", {
				id : $("#knowledge_id").val()
			}, "#knowledgeJYTBResultpage");
load("/kb/knowle/knowledge/showKnowledgeJYCPResultList.action", {
				id : $("#knowledge_id").val()
			}, "#knowledgeJYCPResultpage");
load("/kb/knowle/knowledge/showKnowledgeKBSJResultList.action", {
				id : $("#knowledge_id").val()
			}, "#knowledgeKBSJResultpage");
load("/kb/knowle/knowledge/showKnowledgeJDResultList.action", {
				id : $("#knowledge_id").val()
			}, "#knowledgeJDResultpage");
load("/kb/knowle/knowledge/showKnowledgeBXZLList.action", {
				id : $("#knowledge_id").val()
			}, "#knowledgeBXZLpage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});

	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);
	
	
	function uploadFile1() {
		var isUpload = true;

		load("/operfile/toCommonUpload.action", { // 是否修改
			isUpload : isUpload
		}, null, function() {
			$("#upload_file_div").data("callback", function(data) {

				$("#knowledge_path").val(data.fileId);

			});
		});
	}
	function downFile1() {
		var id = $("#knowledge_path").val();
		window.open(window.ctx + '/operfile/downloadById.action?id='+id,'','');
	}
	function loadTestDicSampleType(){
		var options = {};
		options.width = document.body.clientWidth-800;
		options.height = document.body.clientHeight-40;
		loadDialogPage(null, "样本类型", "/sample/dicSampleType/dicSampleTypeSelect.action", {
			"确定" : function() {
				var operGrid = $("#show_dialog_dicSampleType_div").data("dicSampleTypeDialogGrid");
				var selectRecord = operGrid.getSelectionModel().getSelections();
				var records = sampleReceiveItemGrid.getSelectRecord();

				if (selectRecord.length > 0) {
					$.each(records, function(i, obj) {
						$.each(selectRecord, function(a, b) {
							obj.set("dicSampleType-id", b.get("id"));
							obj.set("dicSampleType-name", b.get("name"));

							obj.set("nextFlowId", "");
							obj.set("nextFlow", "");
						});
					});
				}else{
					message("请选择您要选择的数据");
					return;
				}
				$(this).dialog("close");
			}
		}, true, options);
	}
	// 选择检查项目
	function voucherProductFun() {
		var win = Ext.getCmp('voucherProductFun');
		if (win) {
			win.close();
		}
		var voucherProductFun = new Ext.Window(
				{
					id : 'voucherProductFun',
					modal : true,
					title : '选择项目',
					layout : 'fit',
					width : 600,
					height : 600,
					closeAction : 'close',
					plain : true,
					bodyStyle : 'padding:5px;',
					buttonAlign : 'center',
					collapsible : true,
					maximizable : true,
					items : new Ext.BoxComponent(
							{
								id : 'maincontent',
								region : 'center',
								html : "<iframe scrolling='no' name='maincontentframe' src='"+window.ctx+"/com/biolims/system/product/showProductSelectTree.action?flag=ProductFun' frameborder='0' width='100%' height='100%' ></iframe>"
							}),
					buttons : [ {
						text: biolims.common.close,
						handler : function() {
							voucherProductFun.close();
						}
					} ]
				});
		voucherProductFun.show();
	}
	function setProductFun(id, name) {
		var productName = "";
		ajax(
				"post",
				"/com/biolims/system/product/findProductToSample.action",
				{
					code : id,
				},
				function(data) {

					if (data.success) {
						$.each(data.data, function(i, obj) {
							productName += obj.name + ",";
						});
						document.getElementById("knowledge_product_id").value = id;
						document.getElementById("knowledge_product_name").value = productName;
					}
				}, null);
		var win = Ext.getCmp('voucherProductFun');
		if (win) {
			win.close();
		}
	}