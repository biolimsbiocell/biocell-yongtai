$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
});	
function add() {
	window.location = window.ctx + "/kb/editKnowledgeBase.action";
}
$("#toolbarbutton_add").click(function() {
	add();
});
function list() {
	window.location = window.ctx + '/kb/showKnowledgeBaseList.action';
}
$("#toolbarbutton_list").click(function() {
	list();
});
function newSave(){
	save();
}
$("#toolbarbutton_save").click(function() {
	
	save();
});	
function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/kb/save.action";
		$("#toolbarbutton_save").hide();
		form1.submit();
	
		}
}		
function editCopy() {
	window.location = window.ctx + '/kb/copyKnowledgeBase.action?id=' + $("#knowledgeBase_id").val();
}
function changeState() {
	commonChangeState("formId=" + $("#knowledgeBase_id").val() + "&tableId=knowledgeBase");
}
function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}
$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'知识库',
	    	   contentEl:'markup'
	       } ]
	   });
});
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
function uploadFile1() {
	var isUpload = true;
	
	load("/operfile/toCommonUpload.action", { // 是否修改
		isUpload : isUpload
	}, null, function() {
		$("#upload_file_div").data("callback", function(data) {
			
			$("#knowledgeBase_path").val(data.fileId);
			
		});
	});
}
function downFile1() {
var id = $("#knowledgeBase_path").val();	
window.open(window.ctx + '/operfile/downloadById.action?id='+id,'','');
}
Ext.onReady(function() {
	 Ext.QuickTips.init();
	Ext.BLANK_IMAGE_URL = window.ctx + "/images/s.gif";
	var showtype = Ext.get('showtype');
	showtype.on('click', 
	 function showtypeFun(){
	var win = Ext.getCmp('showtypeFun');
	if (win) {win.close();}
	var showtypeFun= new Ext.Window({
	id:'showtypeFun',modal:true,title:'选择类别',layout:'fit',width:500,height:500,closeAction:'close',
	plain : true,bodyStyle : 'padding:5px;',buttonAlign : 'center',
	collapsible: true,maximizable: true,
	items: new Ext.BoxComponent({id:'maincontent', region: 'center', 
	html:"<iframe scrolling='no' name='maincontentframe' src='"+window.ctx +"/dic/type/dicTypeSelect.action?funName=showtypeFun&flag=sampleCheckItem,sampleCheckItem1,dt,else,generation&flag=showtypeFun' frameborder='0' width='100%' height='100%' ></iframe>"}),
	buttons: [
	{ text: '关闭',
	 handler: function(){
	 showtypeFun.close(); }  }]  });     showtypeFun.show(); }
	);
	});
	 function setshowtypeFun(id,name){
	 document.getElementById("knowledgeBase_type").value = id;
	document.getElementById("knowledgeBase_type_name").value = name;
	var win = Ext.getCmp('showtypeFun')
	if(win){win.close();}
	}
