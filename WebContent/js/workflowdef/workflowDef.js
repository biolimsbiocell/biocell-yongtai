var flg=false;
var flg2=false;
$(function() {
	if($("#lan").val()=="zh_CN"){
		console.log($("#lan").val())
		flg=true;
	}
	if($("#lan").val()=="en"){
		flg2=true;
	}
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"visible":false,
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"visible":flg,
		"title": biolims.user.eduName1,
	});
	colOpts.push({
		"data": "enName",
		"visible":flg2,
		"title": biolims.user.eduName1,
	});
	colOpts.push({
		"data": "cls",
		"title": biolims.common.classPath,
	});
	colOpts.push({
		"data": "oper",
		"title": biolims.common.itPassThrough,
		"name":biolims.common.pass+"|"+biolims.common.noPass,
		"render": function(data, type, full, meta) {
			if(data == "1") {
				return biolims.common.pass;
			}else if(data == "0") {
				return biolims.common.noPass;
			}else {
				return "";
			}
		}
		
	});
	colOpts.push({
		"data": "orderBlockName",
		"visible":flg,
		"title": biolims.common.applicationName ,
	});
	colOpts.push({
		"data": "orderBlockEnName",
		"visible":flg2,
		"title": biolims.common.applicationName ,
	});
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
	});
	var tbarOpts = [];
	var options = table(true,"",
		"/workflow/def/showWorkflowDefTableJson.action",colOpts, tbarOpts)
	myTable= renderRememberData($("#main"), options);
	//恢复之前查询的状态
	$('#main').on('init.dt', function() {
		recoverSearchContent(myTable);
	})
});
function add(){
		window.location=window.ctx+'/workflow/def/editWorkflowDef.action';
	}
function edit(){
	var id="";
	var id = $(".selected").find("input").val();
	if (id==""||id==undefined){
		message(biolims.common.selectRecord);
		return false;
	}
	window.location=window.ctx+'/workflow/def/editWorkflowDef.action?id=' + id ;
}
function view() {
	var id = "";
	var id = $(".selected").find("input").val();
	if (id == "" || id == undefined) {
		message(biolims.common.selectRecord);
		return false;
	}
	window.location = window.ctx + '/workflow/def/viewWorkflowDef.action?id=' + id;
}

//弹框模糊查询参数
function searchOptions() {
	return [/*{
			"txt": biolims.master.sampleBloodRecriveId,
			"type": "input",
			"searchName": "id"
		},*/
		{
			"txt": "名称",
			"type": "input",
			"searchName": "name"
		},
		{
			"txt": "模块名称",
			"type": "input",
			"searchName": "orderBlockName"
		},
//		{
//			"txt": "biolims.receive.receiveDateStart",
//			"type": "dataTime",
//			"searchName": "acceptDate##@@##1",
//			"mark": "s##@@##",
//		},
//		{
//			"txt": "biolims.receive.receiveDateEnd",
//			"type": "dataTime",
//			"searchName": "acceptDate##@@##2",
//			"mark": "e##@@##",
//		},
		{
			"type": "table",
			"table": myTable
		}
	];
}
