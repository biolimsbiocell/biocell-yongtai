$(function() {
	var colOpts = [];
	colOpts.push({
		"data": "id",
		"visible":false,
		"title": biolims.common.id,
	});
	colOpts.push({
		"data": "name",
		"title": biolims.user.eduName1 ,
	});
	colOpts.push({
		"data": "cls",
		"title": biolims.common.classPath,
	});
	colOpts.push({
		"data": "oper",
		"title": biolims.common.itPassThrough,
	});
	colOpts.push({
		"data": "state",
		"title": biolims.common.state,
	});
	colOpts.push({
		"data": "stateName",
		"title": biolims.common.stateName,
	});
	colOpts.push({
		"data": "orderBlock",
		"title": biolims.common.modular,
	});
	colOpts.push({
		"data": "note",
		"title": biolims.common.note,
	});
	var tbarOpts = [];
	var options = table(false, null,
			'/workflow/def/selectWorkflowDefTable.action?orderBlock='+$("#orderBlock").val(),colOpts , tbarOpts)
		var workflowDefTable = renderData($("#addWorkflowDef"), options);
	$("#addWorkflowDef").on(
	'init.dt',
	function(e, settings) {
		// 清除操作按钮
		$("#addWorkflowDef_wrapper .dt-buttons").empty();
		$('#addWorkflowDef_wrapper').css({
			"padding": "0 16px"
		});
	var trs = $("#addWorkflowDef tbody tr");
	workflowDefTable.ajax.reload();
	workflowDefTable.on('draw', function() {
		trs = $("#addWorkflowDef tbody tr");
		trs.click(function() {
			$(this).addClass("chosed").siblings("tr")
				.removeClass("chosed");
		});
	});
	trs.click(function() {
		$(this).addClass("chosed").siblings("tr")
			.removeClass("chosed");
	});
	});

})

