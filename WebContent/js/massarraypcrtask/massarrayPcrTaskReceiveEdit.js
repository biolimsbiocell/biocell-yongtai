﻿$(function() {
	$("#tabs").tabs({
		select : function(event, ui) {
		}
	});
	var id=$("#massarrayPcrTaskReceive_id").val();
	var state = $("#massarrayPcrTaskReceive_state").val();
	if(id=="" || state == "3"){
		var type="2";
		load("/experiment/massarrayPcrTask/massarrayPcrTaskReceive/showMassarrayPcrTaskReceiveItemListTo.action", {type:type}, "#MassarrayPcrTaskReceviceLeftPage");
		$("#markup").css("width","75%");
	}
});

function add() {
	window.location = window.ctx + "/experiment/massarrayPcrTask/massarrayPcrTaskReceive/editMassarrayPcrTaskReceive.action";
}

$("#toolbarbutton_add").click(function() {
	add();
});

function list() {
	window.location = window.ctx + '/experiment/massarrayPcrTask/massarrayPcrTaskReceive/showMassarrayPcrTaskReceiveList.action';
}

$("#toolbarbutton_list").click(function() {
	list();
});

function newSave(){
	save();
}

$("#toolbarbutton_save").click(function() {
	save();
});

$("#toolbarbutton_tjsp").click(function() {
				submitWorkflow("#massarrayPcrTaskReceive", {
					userId : userId,
					userName : userName,
					formId : $("#massarrayPcrTaskReceive_id").val(),
					title : $("#massarrayPcrTaskReceive_name").val()
				}, function() {
					window.location.reload();
				});				
});

$("#toolbarbutton_sp").click(function() {
		completeTask($("#massarrayPcrTaskReceive_id").val(), $(this).attr("taskId"), function() {
			document.getElementById('toolbarSaveButtonFlag').value = 'save';
			location.href = window.ctx + '/lims/pages/dashboard/dashboard.jsp';
		});
});

function save() {
if(checkSubmit()==true){    Ext.MessageBox.show({ msg: '正在保存数据,请等待...', progressText: '保存中...', width:300,   wait:true,   icon:'ext-mb-download'  });
	    var massarrayPcrTaskReceiveItemDivData = $("#massarrayPcrTaskReceiveItemdiv").data("massarrayPcrTaskReceiveItemGrid");
		document.getElementById('massarrayPcrTaskReceiveItemJson').value = commonGetModifyRecords(massarrayPcrTaskReceiveItemDivData);
	    document.getElementById('toolbarSaveButtonFlag').value = 'save';
		form1.action = window.ctx + "/experiment/massarrayPcrTask/massarrayPcrTaskReceive/save.action";
		form1.submit();	
		}
}	

function editCopy() {
	window.location = window.ctx + '/experiment/massarrayPcrTask/massarrayPcrTaskReceive/copyMassarrayPcrTaskReceive.action?id=' + $("#massarrayPcrTaskReceive_id").val();
}

$("#toolbarbutton_status").click(function(){
	var selRecord = massarrayPcrTaskReceiveItemGrid.store;
	for(var j=0;j<selRecord.getCount();j++){
		var result = selRecord.getAt(j).get("method");
		if(result==""){
			message("处理结果不能为空！");
			return;
		}
	}
	if ($("#massarrayPcrTaskReceive_id").val()){
		commonChangeState("formId=" + $("#massarrayPcrTaskReceive_id").val() + "&tableId=MassarrayPcrTaskReceive");
	}	
});

function checkSubmit() {
	var mess = "";
	var fs = new Array();
	var nsc = new Array();
	fs.push($("#massarrayPcrTaskReceive_id").val());
	nsc.push("编号不能为空！");
	fs.push($("#massarrayPcrTaskReceive_id").val());
	nsc.push("编号不能为空！");
	mess = commonFieldsNotNullVerify(fs, nsc);
	if (mess != "") {
			message(mess);
			return false;
		}
		return true;
	}

$(function() {
Ext.onReady(function(){
	var tabs=new Ext.TabPanel({
		   id:'tabs11',
	       renderTo:'maintab',
	       height:document.body.clientHeight-30,
	       autoWidth:true,
	       activeTab:0,
	       margins:'0 0 0 0',
	       items:[{
	    	   title:'样本接收',
	    	   contentEl:'markup'
	       } ]
	   });
});

load("/experiment/massarrayPcrTask/massarrayPcrTaskReceive/showMassarrayPcrTaskReceiveItemList.action", {
				id : $("#massarrayPcrTaskReceive_id").val()
			}, "#massarrayPcrTaskReceiveItempage");
var handlemethod = $("#handlemethod").val();
if (handlemethod == "view") {
	settextreadonlyByAll();
}
});
	var item = menu.add({
				    	text: '复制'
						});
	item.on('click', editCopy);