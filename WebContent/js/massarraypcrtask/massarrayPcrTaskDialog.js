var massarrayPcrTaskDialogGrid;
$(function(){
	var cols={};
    var fields=[];
	fields.push({
		name:'id',
		type:"string"
	});
	fields.push({
		name:'name',
		type:"string"
	});
	fields.push({
		name:'createUser-id',
		type:"string"
	});
	fields.push({
		name:'createUser-name',
		type:"string"
	});
	fields.push({
		name:'createDate',
		type:"string"
	});
    fields.push({
		name:'testUser-id',
		type:"string"
	});
	fields.push({
		name:'testUser-name',
		type:"string"
	});
	fields.push({
		name:'receiveDate',
		type:"string"
	});
	fields.push({
		name:'state',
		type:"string"
	});
	fields.push({
		name:'stateName',
		type:"string"
	});
	cols.fields=fields;
	var cm=[];
	cm.push({
		dataIndex:'id',
		header:'编号',
		width:20*10,
		sortable:true
	});
	cm.push({
		dataIndex:'name',
		header:'描述',
		width:50*10,
		sortable:true
	});
	cm.push({
	    dataIndex:'createUser-id',
	    header:'下达人ID',
	    width:15*10,
	    sortable:true
	});
	cm.push({
		dataIndex:'createUser-name',
		header:'下达人',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'createDate',
		header:'下达日期',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'testUser-id',
		header:'实验员ID',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'testUser-name',
		header:'实验员',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'receiveDate',
		header:'接收日期',
		width:15*10,
		sortable:true
	});
	cm.push({
		dataIndex:'state',
		header:'工作流状态',
		width:40*10,
		sortable:true
	});
	cm.push({
		dataIndex:'stateName',
		header:'工作流状态',
		width:40*10,
		sortable:true
	});
	cols.cm=cm;
	var loadParam={};
	loadParam.url=ctx+"/experiment/massarrayPcrTask/massarrayPcrTask/showMassarrayPcrTaskListJson.action";
	var opts={};
	opts.title="MassarrayPCR引物任务";
	opts.height=document.body.clientHeight-34;
	opts.rowselect=function(id){
		$("#selectId").val(id);
	};
	opts.rowdblclick=function(id,rec){
		$('#selectId').val(id);
		window.parent.setMassarrayPcrTaskFun(rec);
	};
	massarrayPcrTaskDialogGrid=gridTable("show_dialog_massarrayPcrTask_div",cols,loadParam,opts);
});

function sc(){
		var option = {};
		option.width = 542;
		option.height = 417;
		loadDialogPage($("#jstj"), "搜索", null, {
			"开始检索" : function() {
				commonSearchAction(massarrayPcrTaskDialogGrid);
				$(this).dialog("close");
			},
			"清空" : function() {
				form_reset();
			}
		}, true, option);
}
