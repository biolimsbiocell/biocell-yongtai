/*/获取正常的月/*/
function getMonth(month){
	var m=month;
	if(Number(month)<10){
		m="0"+month;
	}
	return m;
}
/*/获取参数年之前的前十年/*/
function getYears(curentYear){
	var currentYear=Number(curentYear);//当前年
	var years=[];
	var length=10;
	for(var i=0;i<=length;i++){
		years.push((currentYear-i).toString()); 
	}
	return years;
}

//为x轴每个点添加数据(两个页签通用)
	function getXdata(dateType, date){
		var xData = [];
		switch(dateType){
			case "month" :
				var num = new Date(date.split("-")[0], date.split("-")[1], 0).getDate();
				for(var i = 0; i < num; i++){
					xData[i] = i + 1;
				}
				break;
			case "year" :
				for(var i = 0; i < 12; i++){
					xData[i] = (i + 1) + '<liferay-ui:message key="MD.Transfer.Month" />';
				}
				break;
		}
		return xData;
	}
function bindRemovePopAndPanel(){
	$(".back").on("click",function(){
		 var tmp = $("html");
		    tmp.find(".popup, .panel, .panel-overlay").remove();
	});
}	

